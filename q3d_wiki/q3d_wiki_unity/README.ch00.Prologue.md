## 《Unity 新手村日记》-00-自学自研项目（Q3D）简介

<br>

#### 说明

---

- 项目文档记录了笔者工作积累的云原生微服务技术笔记，以及元宇宙、数字孪生、程序化建模工作学习笔记；
- 项目包含一套元宇宙建模方法论和工程，采用 QGIS -> Blender / Houdini -> Unreal / Unity 的方式实现数字孪生虚拟城市；
- 项目包含一个 Unreal 客户端工程和一个 Unity 客户端工程（建设中）；
- 项目包含一个 Cesium 网页端工程，实现 WebGIS BI 展示；
- 项目包含一个 Vue.js / ElementPlus / TS 网页端工程，实现 Admin 管理后台；
- 项目包含一个 Java / SpringBoot 微服务工程和一个 C# / DotNet 微服务工程（建设中），共用网关和服务注册中心（Nacos），可以协同工作；
- 项目包含一个 Publish 部署工程，采用 Docker / Kubernetes / Jenkins 方式进行服务部署；
- 程序语言涉及 C++ / Java / C# / Python / TS / Shell / VEX；
- 错漏之处恳请读者不吝指正，转载望注明出处：<https://gitee.com/yexiang841/q3d_project/>；
- Copyright 叶湘 ( weixin:yexiang841, email:yexiang841@qq.com ), Shitouren Studio. All Rights Reserved.

<br>

#### Unity 工作学习笔记

---

- `>>>>` [《Unity 新手村日记》-00-自学自研项目（Q3D）简介](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unity/README.ch00.Prologue.md)
- [《Unity 新手村日记》-01-编辑器（Editor）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unity/README.ch01.Editor.md)
- [《Unity 新手村日记》-03-框架（GamePlay）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unity/README.ch03.GamePlay.md)
- [《Unity 新手村日记》-04-材质（Material）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unity/README.ch04.Material.md)
- [《Unity 新手村日记》-05-场景编辑（Landscape）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unity/README.ch05.Landscape.md)
- [《Unity 新手村日记》-06-UI 界面（UMG）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unity/README.ch06.UMG.md)
- [《Unity 新手村日记》-07-物理模拟/碰撞（Physics/Collision）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unity/README.ch07.Physics.md)
- [《Unity 新手村日记》-08-骨骼和动画（Skeletal/Animation）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unity/README.ch08.Skeletal.md)
- [《Unity 新手村日记》-09-粒子系统（ParticleSystem/Niagara）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unity/README.ch09.Niagara.md)
- [《Unity 新手村日记》-10-C#](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unity/README.ch10.CSharp.md)
- [《Unity 新手村日记》-11-项目设置和打包（Project/Packing）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unity/README.ch11.Project.md)
- [《Unity 新手村日记》-12-流体（Flip）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unity/README.ch12.Flip.md)
- [《Unity 新手村日记》-13-毛发系统（Groom）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unity/README.ch13.Groom.md)
- [《Unity 新手村日记》-14-混沌物理系统（Chaos）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unity/README.ch14.Chaos.md)
- [《Unity 新手村日记》-15-AI](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unity/README.ch15.AI.md)
- [《Unity 新手村日记》-17-技术美工和程序化建模（TA）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unity/README.ch17.TA.md)

<br>

#### 为什么选择 Unity

---

1.  实时渲染；
2.  程序化建模；
3.  结构良好的 C#；
4.  风格化渲染更好做；
5.  移动端支持更好；

<br>

#### Unity 的缺点（个人认为）

---

- 飞行模式（FlyMode）下，触摸板得双击按压才能使用 ASDW，而 UE4 只需要双指触碰就行了；

<br>

#### 版本选择

---

1.  必须选择 LTS 版本；
2.  当前（2022.2）
    - 占有率：2018.3 > 2019.4 > 2020.3；
    - 趋势：2020.3 > 2019.4 > 2018.3；
3.  笔者选择**2020.3**，原因：
    1.  新手没有历史包袱；
    2.  新版的文档也是全的；
    3.  houdini 最新版可以使用新版；
    4.  新版的植被 TA 更好；

<br>

#### 安装引擎

---

1.  先装 UnityHub，再装 Unity 引擎；
2.  安装 Mono，官网：<https://www.mono-project.com/download/stable/>；
    - 验证：终端里输入 `mono --version`；
    - 在 Edit->Project Settings->Player -> Other Settings -> Configuration -> Scripting Backend 选 Mono，API Compatibility Level 选.NET 4.X；
3.  安装

<br>

#### MacOS 下 VSCode 开发环境配置

---

1.  参考：<https://code.visualstudio.com/docs/other/unity>；

2.  设置 Unity 的编辑器：在 Unity->Preferences 中找到 External Tools，设置为 VSCode；

3.  Unity 不需要像 UE4 那样在创建项目时选择是否启用 C++，而是任何时候都能添加脚本组件；

4.  安装 CodeLLDB，解决 macOS Catalina 不支持 lldb 的问题；

5.  C# 核心插件：

    - 安装 C# 支持；
    - 安装 C# Extensions 扩展；
    - 安装 Auto-Using for C#，自动添加 Using；

6.  Mono 核心插件：

    - 安装 Mono Debug，用于 C# 编译时调试；

7.  Unity 核心插件：

    - 安装 Unity Tools；
    - 安装 Unity Code Snippets；
    - 安装 Unity Snippets Modified；
    - 安装 Unity Snippets；

8.  项目插件：

    - 安装 Project Manager 项目管理插件，快速切换不同项目；

9.  编辑插件：

    - 安装 Chinese (Simplified) Language Pack for Visual Studio Code，官方中文包，全局搜索 Configure Display Language 切换，但笔者建议用英文；
    - 安装 Prettier - Code formatter 代码规范插件，alt+shift+f 规整代码；
    - 安装 Bracket Pair Colorizer 2，让嵌套代码块用不同颜色标识，安装后自动生效；
    - 安装 Vim 插件，基础技能不要忘，通过插件的 enable 和 disable 来开关；
    - 安装 TODO Heighlight 和 Todo Tree，用以管理 Todo 列表；
    - 安装 YAML 插件，用于自动格排列 Yaml 格式；
    - 安装 Format Files 插件，可以在 Explorer 区对文件按目录进行格式化（建议先在 Git 中 Commit 备份）；

10. Code Runner，各种语言快速运行助手，配置如下：

    - 调整运行设置，全局搜索 Preferences: Open Settings (JSON)，添加：
      - "code-runner.clearPreviousOutput": true,//每次运行之前清一下之前的输出；
      - "code-runner.runInTerminal": true,//打开终端执行；
      - "code-runner.saveAllFilesBeforeRun": true,//运行之前保存所有文；
    - 调整 C++选项编译选项，全局搜索 Preferences: Open Workspace Settings (JSON)，添加：
      - "code-runner.executorMap":{"Cache": "cd $dir && g++ $fileName -o $fileNameWithoutExt.out -std=c++17 -Wall -O0 -g && $dir$fileNameWithoutExt.out"}；

11. VSCode 快捷键（Vim 之外的）：

    - 文档：<https://code.visualstudio.com/shortcuts/keyboard-shortcuts-windows.pdf>；
    - cmd+,：打开设置；
    - cmd+shift+p：全局搜索设置；
    - cmd+p：全局搜索文件；
    - alt+shift+f：用 Prettier 插件规整代码；
    - alt+o：在.h 和.cpp 之间切换；
    - f5：执行；
    - cmd+shift+b：运行编译任务；

12. 配置 Omnisharp （不配置也可以开发，但是类型无法识别）

    - Code -> Preferences -> Settings，检索 mono
      - 设置 Omnisharop:Use Global Mono 为 Always
      - 某些环境下有必要同时设置 MonoPath，在 settings.json 中添加：
        ```
        "omnisharp.monoPath": "/Library/Frameworks/Mono.framework/Versions/Current/Commands/mono",
        ```

<br>

#### 官网常用链接

---

1.  Unity 中文官网：<https://unity.cn/>；
2.  Unity 中文文档(2020.3)：<https://docs.unity.cn/cn/2020.3/Manual/UnityManual.html>；
3.  Unity API 文档(2020.3)：<https://docs.unity.cn/cn/2020.3/ScriptReference/index.html>；

<br>

#### 学习资源整理

---

1.  图形学：

    - B 站闫令琪：<https://www.bilibili.com/video/BV1X7411F744>；

2.  学习目录：

3.  入门：

    - B 站邵发：<https://www.bilibili.com/video/BV1WK411V7dn>；

4.  进阶：

5.  材质：

6.  推荐的 B 站 UP 主：

7.  设计模式参考：

    - <https://www.runoob.com/design-pattern/design-pattern-intro.html>；
    - <https://www.cnblogs.com/shiroe/p/14845752.html>；

<br>

#### 编码规范

---

- TODO:
