## 《Unreal 新手村日记》-00-自学自研项目（Q3D）简介

<br>

#### 说明

---

- 项目文档记录了笔者工作积累的云原生微服务技术笔记，以及元宇宙、数字孪生、程序化建模工作学习笔记；
- 项目包含一套元宇宙建模方法论和工程，采用 QGIS -> Blender / Houdini -> Unreal / Unity 的方式实现数字孪生虚拟城市；
- 项目包含一个 Unreal 客户端工程和一个 Unity 客户端工程（建设中）；
- 项目包含一个 Cesium 网页端工程，实现 WebGIS BI 展示；
- 项目包含一个 Vue.js / ElementPlus / TS 网页端工程，实现 Admin 管理后台；
- 项目包含一个 Java / SpringBoot 微服务工程和一个 C# / DotNet 微服务工程（建设中），共用网关和服务注册中心（Nacos），可以协同工作；
- 项目包含一个 Publish 部署工程，采用 Docker / Kubernetes / Jenkins 方式进行服务部署；
- 程序语言涉及 C++ / Java / C# / Python / TS / Shell / VEX；
- 错漏之处恳请读者不吝指正，转载望注明出处：<https://gitee.com/yexiang841/q3d_project/>；
- Copyright 叶湘 ( weixin:yexiang841, email:yexiang841@qq.com ), Shitouren Studio. All Rights Reserved.

<br>

#### Unreal 工作学习笔记

---

- `>>>>` [《Unreal 新手村日记》-00-自学自研项目（Q3D）简介](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch00.Prologue.md)
- [《Unreal 新手村日记》-01-编辑器（Editor）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch01.Editor.md)
- [《Unreal 新手村日记》-02-蓝图（Blueprint）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch02.Blueprint.md)
- [《Unreal 新手村日记》-03-框架（GamePlay）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch03.GamePlay.md)
- [《Unreal 新手村日记》-04-材质（Material）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch04.Material.md)
- [《Unreal 新手村日记》-05-场景编辑（Landscape）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch05.Landscape.md)
- [《Unreal 新手村日记》-06-UI 界面（UMG）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch06.UMG.md)
- [《Unreal 新手村日记》-07-物理模拟/碰撞（Physics/Collision）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch07.Physics.md)
- [《Unreal 新手村日记》-08-骨骼和动画（Skeletal/Animation）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch08.Skeletal.md)
- [《Unreal 新手村日记》-09-粒子系统（ParticleSystem/Niagara）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch09.Niagara.md)
- [《Unreal 新手村日记》-10-C++](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch10.CPP.md)
- [《Unreal 新手村日记》-11-项目设置和打包（Project/Packing）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch11.Project.md)
- [《Unreal 新手村日记》-12-流体（Flip）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch12.Flip.md)
- [《Unreal 新手村日记》-13-毛发系统（Groom）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch13.Groom.md)
- [《Unreal 新手村日记》-14-混沌物理系统（Chaos）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch14.Chaos.md)
- [《Unreal 新手村日记》-15-AI](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch15.AI.md)
- [《Unreal 新手村日记》-16-UE5](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch16.UE5.md)
- [《Unreal 新手村日记》-17-技术美工和程序化建模（TA/PM）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch17.TA.md)

<br>

#### 为什么选择 Unreal

---

1.  仅次于电影级的实时渲染；
2.  较熟悉 C++；
3.  视图操作对 Mac 系统和触摸板比 Unity 友好；
4.  能支撑 Houdini 的程序化建模；

<br>

#### Unreal 的缺点（个人认为）

---

1.  风格化渲染难度略高；
2.  对 2D 支持不十分到位；
3.  C++ 是魔改的，应该叫 U++，使用了大量宏，增加了复杂度；
4.  编译运行不是那么爽快；
5.  在 MacOS 下对 VSCode 支持不是特别好，尤其是代码补齐功能上；

<br>

#### 版本选择

---

1.  必须选择 LTS 版本；
2.  在没有历史包袱的情况下，尽量选择 Unreal5；
3.  如果用 Unreal4，建议选择 4.27；

<br>

#### 安装引擎

---

1.  先装 EpicGames Launcher，再装 Unreal 引擎；
2.  Unreal 是可以离线运行的，除了安装过程之外，日常打开不依赖 EpicGames Launcher；

<br>

#### MacOS 下安装 XCode

---

1.  Unreal 在 MacOS 下的编译依赖于 XCode 工具进行编译，因此必须安装 XCode（不论使用 XCode 还是 VSCode 作为 IDE）；
2.  有时候更新太新的 XCode 版本会出现底层编译错误（涉及/Engine/Source 的错误），可能会需要安装旧版本 XCode；
3.  旧版本 XCode 下载页面：<https://xcodereleases.com>；

<br>

#### MacOS 下 VSCode 开发环境配置

---

1.  参考：<https://github.com/botman99/ue4-xcode-vscode-mac>；

2.  设置 UE4 的编辑器：全局设置中搜 Source Code Editor，设置为 VSCode，然后再创建项目；

3.  创建项目时选择 C++项目，会自动生成 VSCode 工程文件；

4.  如果创建时没有按以上顺序，可手动(Shell)创建 VSCode 工程文件：

    ```
    /Users/Shared/Epic\ Games/UE_4.26/Engine/Build/BatchFiles/Mac/GenerateProjectFiles.sh -project=“[路径/工程.uproject]" -game -vscode
    ```

5.  安装 CodeLLDB，解决 macOS Catalina 不支持 lldb 的问题；

6.  C++ 核心插件：

    - 安装 C++ 支持；
    - 安装 CodeLLDB，解决 macOS Catalina 不支持 lldb 的问题；
    - 安装 C++ Intellisense，用于自动代码补全，实时错误检查，代码改进建议；

7.  Unreal 核心插件：

    - 安装 Unreal Engine 4 Snippets；
    - 安装 UE Intellisense Fixes，UE4 智能提醒修复器：<https://github.com/boocs/ue4-intellisense-fixes/releases/>；

8.  项目插件：

    - 安装 Project Manager 项目管理插件，快速切换不同项目；

9.  编辑插件：

    - 安装 Chinese (Simplified) Language Pack for Visual Studio Code，官方中文包，全局搜索 Configure Display Language 切换，但笔者建议用英文；
    - 安装 Prettier - Code formatter 代码规范插件，alt+shift+f 规整代码；
    - 安装 Bracket Pair Colorizer 2，让嵌套代码块用不同颜色标识，安装后自动生效；
    - 安装 Vim 插件，基础技能不要忘，通过插件的 enable 和 disable 来开关；
    - 安装 TODO Heighlight 和 Todo Tree，用以管理 Todo 列表；
    - 安装 YAML 插件，用于自动格排列 Yaml 格式；
    - 安装 Format Files 插件，可以在 Explorer 区对文件按目录进行格式化（建议先在 Git 中 Commit 备份）；

10. Code Runner，各种语言快速运行助手，配置如下：

    - 调整运行设置，全局搜索 Preferences: Open Settings (JSON)，添加：
      - "code-runner.clearPreviousOutput": true,//每次运行之前清一下之前的输出；
      - "code-runner.runInTerminal": true,//打开终端执行；
      - "code-runner.saveAllFilesBeforeRun": true,//运行之前保存所有文；
    - 调整 C++选项编译选项，全局搜索 Preferences: Open Workspace Settings (JSON)，添加：
      - "code-runner.executorMap":{"Cache": "cd $dir && g++ $fileName -o $fileNameWithoutExt.out -std=c++17 -Wall -O0 -g && $dir$fileNameWithoutExt.out"}；

11. VSCode 快捷键（Vim 之外的）：

    - 文档：<https://code.visualstudio.com/shortcuts/keyboard-shortcuts-windows.pdf>；
    - cmd+,：打开设置；
    - cmd+shift+p：全局搜索设置；
    - cmd+p：全局搜索文件；
    - alt+shift+f：用 Prettier 插件规整代码；
    - alt+o：在.h 和.cpp 之间切换；
    - f5：执行；
    - cmd+shift+b：运行编译任务；

12. 在.vscode.launch.json 的对应 debug 项中（比如 development）加上"stopOnEntry": false；

    - 此时运行和按 F5 就能从 VSCode 打开 Unreal 编辑器；

13. 若要在 VSCode 中只编译（确保代码可用），则需要使用 VSCode 的 Task 系统；

    - 在 Terminal -> RunTask 中可选择项目的 Run/Build/Clean；

14. 修改 c_cpp_properties.json：<https://www.alexkissijr.com/unreal/c_cpp_properties.json>；

<br>

#### 官网常用链接

---

1. Unreal 官网：<https://www.unrealengine.com/zh-CN/>；
2. Unreal 中文文档：<https://docs.unrealengine.com/zh-CN/index.html>；
3. Unreal API 文档：<https://docs.unrealengine.com/4.27/en-US/API/>；

<br>

#### 学习资源整理

---

1.  图形学：

    - B 站闫令琪：<https://www.bilibili.com/video/BV1X7411F744>；

2.  学习目录：

    - 别人整理总结的：<https://zhuanlan.zhihu.com/p/36675543>；（看完就是大佬了）

3.  入门：

    - 油管搬运：<https://www.bilibili.com/video/BV1Yt411d77A>；（字幕很烂，基于英文，案例很多）

4.  进阶：

    - 【蓝图】B 站 Developer 殿堂：<https://www.bilibili.com/video/BV1NJ411y7e1>；
    - 【蓝图】B 站 CG 学习笔记：<https://www.bilibili.com/video/BV18e411p74F>；（有小地图实现）
    - 【蓝图】B 站谌嘉诚：<https://www.bilibili.com/video/BV164411Y732>；（新手推荐）
    - 【蓝图】B 站 TT 脑思：<https://www.bilibili.com/video/BV1Za4y1s7ti>；（个人推荐）
    - 【材质】B 站 TT 脑思：<https://www.bilibili.com/video/BV19t4y1e7Dw>；
    - 【材质】B 站阿棍儿：<https://www.bilibili.com/video/BV1GJ411j7d4>；
    - 【材质】B 站谌嘉诚的材质频道：<https://space.bilibili.com/31898841/favlist?fid=920627041&ftype=create>；
    - 【材质】油管搬运材质大师课：<https://www.bilibili.com/video/BV1Kp4y1B7Ro>；
      - 上面这则大师课的学习笔记：<https://blog.csdn.net/weixin_43803133/category_10752697.html>；
    - 【材质】油管搬运：<https://www.bilibili.com/video/BV1H5411n7eG>；（讲原理）
    - 【旧版粒子】B 站 TT 脑思：<https://www.bilibili.com/video/BV1B54y167cj>；
    - 【新版粒子】B 站 TT 脑思：<https://www.bilibili.com/video/BV1iA41137My>；
    - 【运动】B 站五谷延年：<https://www.bilibili.com/video/BV12f4y1r71N>；
    - 【运动】B 站 TGOC：<https://www.bilibili.com/video/BV1XK411K756>；
    - 【场景】油管搬运：<https://www.bilibili.com/video/BV12i4y1t7BK>；
    - 【GamePlay】B 站吉叶子：<https://www.bilibili.com/video/BV1Bf4y1D7AT>；
    - 【UMG】B 站午歌：<https://www.bilibili.com/video/BV1Qi4y1u7kh>；（噪音警告）
    - 【C++】B 站大钊：<https://www.bilibili.com/video/BV1C7411F7RF>；（只讲大纲）
    - 【C++】B 站天空游荡的鱼：<https://www.bilibili.com/video/BV1Xk4y1B7Q4>；（讲干货）
    - 【C++】知乎大钊《InsideUE4》：<https://zhuanlan.zhihu.com/insideue4>；（顶级精华帖）
    - 【C++】知乎孤傲雕：<https://zhuanlan.zhihu.com/p/69367495>；
    - 【C++】CSDN-RollingTune：<https://blog.csdn.net/qq_33500238/article/details/99674576>；
    - 【布料】B 站 TGOC：<https://www.bilibili.com/video/BV1k5411b7XF>；
    - 【混沌】油管搬运：<https://www.bilibili.com/video/BV1S7411G72o>；
    - 【碰撞】官方教程：<https://docs.unrealengine.com/4.26/zh-CN/InteractiveExperiences/Physics/Collision/>；
    - 【碰撞】两种设置碰撞方法的区别：<http://www.v5xy.com/?p=467>；
    - 【碰撞】物理碰撞的源码解析：<https://blog.csdn.net/u012999985/article/details/78242493>；
    - 【小地图】B 站小地图实现：<https://www.bilibili.com/video/BV1Zv4y1f7D9>；

5.  材质：

    - B 站水墨材质案例：<https://www.bilibili.com/video/BV1HU4y1p7aE>；
    - B 站皮肤材质案例：<https://www.bilibili.com/video/BV1Q54y127Qp>；
    - B 站冰材质案例：<https://www.bilibili.com/video/BV1bE411w7vK>；
    - B 站雪材质案例：
      - <https://www.bilibili.com/video/BV1TJ411p7U9>；
      - <https://www.bilibili.com/video/BV1M7411L77s>；
      - <https://www.bilibili.com/video/BV1wt411m7L7>；
    - B 站地形材质案例：<https://www.bilibili.com/video/BV1Mb4y197TA>；
    - B 站水材质案例：
      - <https://www.bilibili.com/video/BV1Zf4y1U7g9>；
      - <https://www.bilibili.com/video/BV1cJ41167bT>；
    - B 站卡通材质案例：<https://www.bilibili.com/video/BV1Ai4y177bZ>；
    - B 站自动材质和多层材质案例：<https://www.bilibili.com/video/BV1Kb411i74s>；

6.  个人推荐的 B 站 Up 主：

    - B 站午歌的主页：<https://space.bilibili.com/73820604>；
    - B 站墨鱼驴肉汤主页：<https://space.bilibili.com/6306173>；
    - B 站 MARX-Workshop 主页：<https://space.bilibili.com/160061749>；
    - B 站十一弦主页：<https://space.bilibili.com/23560321>；
    - B 站黑鸟云端主页：<https://www.bilibili.com/video/BV1B4411h7po>；

7.  设计模式参考：

    - <https://www.runoob.com/design-pattern/design-pattern-intro.html>；
    - <https://www.cnblogs.com/shiroe/p/14845752.html>；

<br>

#### 素材资源整理（持续收集中）

---

1. 虚幻商城

   - 素材推荐：
     - 植物：tamperate Vegetation 系列；
     - 植物：Megascans Meadow Pack；
     - 山体：Landscape Backgrounds；

2. QuixelMegascans（影视级扫描库）

   - 官网：<https://quixel.com/megascans>；
   - B 站 QuixelBridge 和 QuixelMixer 的使用：<https://www.bilibili.com/video/av585007190>；

3. Youtube 搜 unreal free download；

<br>

#### 编码规范

---

1.  命名是修为，日志是气质，注释是品行；
2.  Unreal 官方代码规范：<https://docs.unrealengine.com/4.26/zh-CN/ProductionPipelines/DevelopmentSetup/CodingStandard/>；
3.  另一个很值得参考的代码规范：<https://github.com/skylens-inc/ue4-style-guide/blob/master/Doc/README.md>；
4.  笔者是 Unreal 新手，但有开放平台的研发经验，所以并不完全遵循官方代码规范，而是些微改动，糅合个人多年习惯和开放平台标准，尝试呈现 SDK 标准，在这一点上请读者审慎参考。
5.  本项目的命名规范会带有项目前缀，尽量简短。读者若在客户端用过友盟等第三方 API，应该见过 UM 之类的前缀，本项目用 Q3D，表示该资源隶属于本项目（而不是第三方引用），有利于溯源查错查重。
6.  只要严格使用项目前缀，就不太必要使用命名空间（namespace），UHT 也不支持命名空间，笔者只在特定接口使用命名空间进行版本划分；
7.  笔者的命名习惯是：追求规范、不介意长，属于强迫症，由（被毒打的）社会经验累积而成；
8.  文件和类型资源命名举例
    - Q3D_BPU_Hit.h 定义蓝图类，U 表示源自 UObject；
    - Q3D_CPPU_Hit.h 定义 C++类 UQ3D_CPPU_Hit，U 表示源自 UObject；
    - Q3D_BPA_Pawn 定义蓝图类，A 表示源自 AActor；
    - Q3D_CPPA_Pawn.h 定义类 AQ3D_CPPA_Pawn，A 表示源自 AActor；
    - Q3D_BPI_Shooter 定义蓝图接口，I 表示源自 UInterface；
    - Q3D_CPPI_Shooter.h 定义 C++接口 IQ3D_CPPI_Shooter，I 表示源自 UInterface；
    - Q3D_BPS_HP 定义蓝图结构体，S 表示基类来体自 UScriptStruct；
    - Q3D_CPPE_HitType.h 定义 C++枚举 EQ3D_CPPE_HitType，E 表示源自 UEnum；
    - Q3D_CPPC_PawnState.h 定义 C++组件 UQ3D_CPPC_PawnState，C 表示源自 UActorComponent；
    - Q3D_BPWC_FloatingLive 定义蓝图 UI 组件，WC 表示源自 UWidgetComponent；
    - Q3D_BPW_FloatingLive 定义蓝图 UI，W 表示源自 UUserWidget；
    - Q3D_CPPFL_PrintLog.h 定义 C++函数库 UQ3D_CPPFL_PrintLog，FL 表示源自 UBlueprintFunctionLibrary；
    - Q3D_BPFL_PrintLog 定义蓝图函数库（Function Libarary）；
    - Q3D_BPML_PrintLog 定义蓝图宏库（Macro Libarary）；
    - 依此类推；
9.  纯蓝图资产命名
    - 这些资源似乎一般都在编辑器实现，所以暂时没有用 BP 和 CPP 去区分（节省长度）；
    - Q3D_DT_PropertyInit 表示 DataTable 资源，类型是 UDataTable；
    - Q3D_N_FireBlack_S01 表示 Niagara 类型的特效资源，S 表示 Style，该命名表示一种黑色火焰特效，样式风格是 S01；
    - Q3D_NE_Smoke_S01_Black 表示 Niagara Emitter，该命名表示组成黑色火焰样式 S01 的其中一个发射器，专门发射黑烟；
    - Q3D_NMS_ClampRadius 表示 Niagara Module Script，该命名表示一个用来限制半径的模块脚本；
    - Q3D_M_Metal_Rusty_S01 表示 Material，该命名表示一种有划痕的金属材质，划痕样式风格是 01；
    - Q3D_MI_Metal_Rusty_S01_P01 表示材质实例（Material Instance），P 表示 Parameter，即对上一行材质使用了一组参数 P01 进行实例化；
    - Q3D_MF_Noise_S01 表示材质函数，该命名表示一种噪声生成器，样式风格是 S01；
    - Q3D_MAP_TestPawn_L01 表示 Map，L 表示关卡（Level），看名字就知道是 TestPawn 系列地图的第一关；
    - 有朝一日若成大佬，开始用 C++写特效和材质，就把*N*和*M*变成*BPN*和*BPM*，以区分*CPPN*和*CPPM*，想来似乎没毛病；
10. 类和接口中尽量使用 Category
    - 笔者习惯用简化类/接口名作为 Category，如：Q3D_Hit / Q3D_I_HitTarget（去掉 CPP/BP 标识）；
    - 继承的类用 Category 分辨函数/成员是来自基类还是哪个子类；
    - 组合的类用 Category 区分组合模块来源；
    - 接口中用 Category 标识每一个函数，当一个 Actor 继承实现多个接口时，每个接口函数自动归类到自己的 Category 下，是一种很舒服的体验；
11. 函数命名
    - Q3D_F_PrintLog( ... ) 定义一个公有函数；
    - \_F_PrintLog( ... ) 定义一个私有函数；
    - Q3D_FD_PrintLog( ... ) 定义一个委托函数，D 代表 Delegate；
    - Q3D_FE_PrintLog( ... ) 定义一个回调函数，E 代表 Event；
    - Q3D_IF_PrintLog( ... ) 定义接口中的一个函数，I 代表 Interface；
    - Q3D_IFD_PrintLog( ... ) 定义接口中的一个委托函数；
    - Q3D_IFE_PrintLog( ... ) 定义接口中的一个回调函数；
12. 以上文件、资源、类名、函数名都需要加项目前缀（Q3D\_），除去前缀的部分，是大小写驼峰型，至于什么时候用下划线连接两个单词？这是分类学（玄学的一种），无法言表，只能细品。以下是变量命名，不带项目前缀；
13. 基本数据变量命名
    - UE 官方的规范是全部大小写驼峰型，完全不使用下划线，用简化字母在前面标识类型，如 bIsActive；
    - 个人规范与 UE 官方略微不同，使用完整类型名，引入小写，引入下划线，用于区分成员变量（Bool_IsActive）、函数参数（bool_is_active）和临时变量（bool_is_active_tmp）；
    - 事实证明是可行的，在 C++中定义的小写变量，以及蓝图中定义的参数/返回值/临时变量等小写变量，到了蓝图节点使用时，都会自动显示成大小写驼峰型并忽略所有下划线，所以 Bool_IsActive 和 bool_is_active 最终都会显示成 Bool Is Active。但如果小写不拆，比如 bool_isactive 最终会显示 Bool Isactive，略微不完美，这就是 is_active 中下划线的作用；
    - Int_HitCount 定义一个 int32/Integer 类型的 Value，大写表示该数据是类的成员变量；
    - int_hit_count 小写表示该数据是函数的参数（属于个人风格，请慎重参考）；
    - int_hit_count_tmp 小写+tmp 表示函数中定义的临时变量，（属于个人风格，请慎重参考），如果需要两个，那就是 tmp01 和 tmp02；
    - float_value_in / float_value_out 当传入的参数（float_value）与成员变量（Float_Value）重名时（UE4 此时不区分大小写），在参数后面加\_in 和\_out，\_out 指的是引用方式传值的输出；
    - Enum_HitType / enum_hit_type 枚举也算基本类型，用值传递（指针或引用传递没必要）；
    - Ptr_S_HitValue / ptr_s_hit_value 结构体可以使用指针；
    - Struct_HitValue / struct_hit_value 结构体不使用指针；
14. 对象变量命名
    - Ptr_A_SomeObj 声明一个指针，大小写驼峰型表示该指针是成员变量，如果是本项目定义的类，那就一定是 AQ3D_CPPA_SomeObj 或者 AQ3D_BPA_SomeObj 类型，\_A\*表示它源自 AActor；
    - ptr_a_some_obj 是上述类型的小写版，表示该指针是函数参数或临时变量。（属于个人风格，请慎重参考）；
    - 类似的，Ptr_DT_PropertyData 声明一个 DataTable 型的成员变量，看名字可知类型大概率是 Q3D_DT_PropertyData；
    - 同一个 UQ3D_CPPU_Hit 需要实例化两个对象怎么命名？靠下划线（分类玄学）：Ptr_U_Hit_Damage 和 Ptr_U_Hit_Recover，
15. 容器变量命名
    - Array_Int_Value 定义一个 Int 数组 TArray；
    - Map_Name_To_Ptr_U_Hit 定义一个 TMap，Key 是 FName 类型，Value 是 UQ3D_CPPU_Hit 类型的对象指针；
16. 回车与空行规范
    - 没事不留空行；
    - 大事之间，留一行空行；
    - 天大的事，也只留一行空行；
    - 小技巧：提交之前通过 Shell 指令实现（适用于 Mac）：
      ```
      gsed -i ':a;N;$!ba;s/\n\n\n/\n\n/g' *.h
      gsed -i ':a;N;$!ba;s/\n\n\n/\n\n/g' *.cpp
      ```
17. 头文件依赖
    - `#pragma once`用于保护头文件只被引用一次，应该默认加上；
    - 出于职业习惯，.h 中还是要尽量减少 include，多使用前置声明，相关的 include 都放在.cpp；
18. 注释
    - 编码阶段不写注释（写完不还得改么...）；
    - Debug 阶段发现容易错的地方写注释；
    - Review 阶段发现容易忘的地方写注释；
    - Unreal 的 C++ 反射宏大多数都有 `meta = ( Tooltip = "" )` 选项，供蓝图显示简介，因此大多数注释都写在这里，不要另开/\* \*/；
    - 函数参数备注使用/\*换行\*/；
    - 代码流程注释统一用//，不要超过一行，不为注释前后留空行；

<br>

#### 架构思考

---

1.  C++还是蓝图？

    - 看过大钊（虚幻社区经理？）安利 C++的视频，结合自己的实践总结了几点；
      - 首先，【会用 C++ 的都会用蓝图】，所以，不要跳过蓝图；
      - 然后，【会用 C++ 的最后会主要使用 C++】，所以，蓝图只是重要的辅助手段；
      - 最后，【团队项目/大项目会是 C++ 结合蓝图】，所以，不要二选一，成年人什么都要；
    - 蓝图优点 1：更像流程图，比 C++ 更容易看懂，适合做原型，又快又能演示，或许策划刚讲完需求，Demo 就做好了；
    - 蓝图缺点 1 / C++ 优点 1：蓝图节点是二进制资源，无法使用各种高效的文本管理手段：
      - 比如把工程中所有的 `LogError(...)` 改成 `LogWarning(...)`，C++ 文件可以用 IDE 全局替换，也可以一个 sed 指令搞定，而在蓝图中你得一边哭一边改，一个一个节点删，一个一个节点新建，一条线一条线重新连。
      - 就算只改参数（类型/名），蓝图节点的修改也只能依赖其内部的 Reference，而 Reference 是语义层面，不可能比改单词更彻底（虽然改单词容易犯错）；
    - 蓝图缺点 2 / C++ 优点 2：蓝图节点是二进制资源，很难在 Git/Svn 中精细管理（例如无法 diff 操作），还很浪费空间，一个.uasset 文件动则几百 K，随便改一下就加几百 K，长此以往，Github/Gitee 直呼受不了。相比之下，C++改个大点的.cpp 文件也就十几 K；
    - 蓝图缺点 3：易崩溃，难修复。虽然高版本原来越来越稳定，但还是有很多易崩溃的场景，关键是有时候崩溃了还运行不起来，于是没有修复的机会，只能又一次迁移（或者用 IDE 做 Debug）；
    - 蓝图缺点 4：Bug 有点多。遇到过很多次.uasset 明明出现在文件系统中，但编辑器中就是显示不出来的状况。有时候在编辑器中移动资源就常出状况，甚至令笔者怀疑自己有什么重要知识点没学到位；
    - C++ 缺点 1：对编程基础有较高的要求，你是高手当然可以含笑不语，但你的 BOSS 和 HR 正在招聘网站上垂死挣扎；
    - C++ 缺点 2：容易造成崩溃（蓝图缺点 3 的根源之一）。举其中一个例子：比如说 C++定义一个类 CPPA，然后蓝图 BPA 继承 CPPA，然后在 C++中把 CPPA 改成 CPPB，编辑器一开，BPA 直接找不到基类了，并且还不能 Re-Parent 成 CPPB，最要紧的是 BPA 中所有基于 CPPA 重写的函数都消失了...失了...了...要是没有 Git 找回，会哭很久的。如果蓝图别的地方多处依赖这些消失的函数/节点，就会引发灾难，这时就算程序不崩溃而你崩溃了，那也算崩溃；
    - C++ 缺点 3（疑似）：有可能是笔者没学精细，在 MacOS 上 HotReload 总是失效，经常需要重新运行编辑器；

2.  C++ 结合蓝图

    - 首先要相信终极方案是【C++结合蓝图】，按这个方向设计工作流程；
    - 新建蓝图类时，一律先新建 C++基类然后继承它。例如：以前创建蓝图 BPA_MyActor 可能直接继承自 AActor，现在先创建一个 CPPA_MyActor（继承自 AActor），然后 BPA_MyActor 继承 CPPA_MyActor；
    - 对外营业的是 BPA_MyActor，而不是 CPPA_MyActor，因此代码写在这两个中都是可用的；
    - 做原型用 BPA_MyActor，待流程测通了，找个好欺负的新人，让他把功能全移到 CPPA_MyActor，实现无缝切换；

3.  组件（Component）的使用

    - 比如说要为 Pawn 添加一种功能叫 Property，那么定义四个类：
      - 【数据】负责最终干活（增删改查/绑定/广播等）；
      - 【数据容器】负责数据同步（初始化/序列化/网络同步/关卡同步/读写 DB 等）；
      - 【组件】负责沟通 Owner（处理生命周期/响应事件/注销等）；
      - 【接口】负责让功能模块与 Owner 的类型解耦；
    - 第一步：定义【数据】U_Property（Bean？）
      - 可能会有个干活函数：`FString& F_ToString() const`；
    - 第二步：定义【数据容器】U_PropertyState（BeanManager？）；
      - 通常会有一个 Map_Name_To_Ptr_U_Property 或者 Array_Ptr_U_Property 用来存储【数据】Property（一个或多个）；
      - 可能会有一两个函数：
        - 查【数据】：`U_Property* F_Property_GetByName( FName name_id );`；
        - 增【数据】：`void F_Property_SetByName( FName name_id, U_Property* ptr_u_property );`；
    - 第三步：定义【组件】C_PropertyState（注意字母 C，是个 UActorComponent）；
      - 【组件】会持有【数据容器】：`U_PropertyState* Ptr_U_PropertyState`；
    - 第四步：定义【接口】I_PropertyState（注意字母 I，是个 UInterface）；
      - 通常会有一个接口函数：`U_PropertyState* IF_PropertyState_Get()`用于返回【数据容器】；
      - Pawn 继承并实现【接口】，将自己【组件】C_PropertyState 中的【数据容器】U_PropertyState 暴露出去：`return Ptr_C_Property->Ptr_U_PropertyState`；
      - 也可以有别的接口函数，Pawn 可以直接转发给自己【组件】C_PropertyState 中的【数据容器】U_PropertyState 去处理，比如统计一下【数据】Property 的个数和总和；
    - 第五步：修改 MyPawn 定义：
      - MyPawn 添加组件：`C_PropertyState* Ptr_C_PropertyState`：
      - MyPawn 继承接口：`class MyPawn : public APawn, public I_PropertyState`
      - MyPawn 实现接口：`virtual U_PropertyState* IF_PropertyState_Get_Implementation() const { return Ptr_C_PropertyState->Ptr_U_PropertyState; }`
    - 第六步：Property 的初始化（一个或多个）：
      - 如果由组件自动初始化，就在组件（C_PropertyState）的 BeginPlay 中 GetOwner 取相关数据（或者读网络/文件/DB 数据），创建 Ptr_U_Property（一个或多个），存入`this->Ptr_U_PropertyState`；
      - 如果由 MyPawn 主动初始化，就在 MyPawn 的 BeginPlay 中生成 Ptr_U_Property（一个或多个），存入【组件】C_PropertyState 中的【数据容器】U_PropertyState；
    - 最后，MyPawn 新特性的使用方法如下：
      - 将 MyPawn 转换成对应接口：`I_PropertyState* ptr_i_property_state = Cast<I_PropertyState>( ptr_a_pawn );`
      - 接口判断：`if( ! ptr_i_property_state ){ UE_LOG( Warning, "Invalid_Ptr_I_PropertyState" ); return; };`
      - 通过接口取数据容器：`U_PropertyState* ptr_u_property_state = ptr_i_property_state->Execute_IF_PropertyState_Get( ptr_a_pawn );`
      - 从数据容器中取数据：`U_Property* ptr_u_property = ptr_u_property_state->F_Property_GetByName( TEXT("name_id") );`
      - 让数据正经干活：`FString string_property_tmp = ptr_u_property->F_ToString();`
    - 这是【面向接口编程】的套路，缺点是类型繁多，优点是层级良好，笔者以前都是这么干的，不知道大厂会怎么做，盼高手指教。

4.  子系统（SubSystem）的使用

    - TODO

<br>

#### Q3D 工程代码解析

---

1.  日志

    - UE_LOG 只是基础工具，为了扩展性，封装一个日志宏是必需的；
    - 本项目封装了 Q3D_LOG，是项目开始的第一个模块；
    - Q3D_LOG 宏设计成 `Q3D_LOG( Warning, "Content" )`，而不是 `Q3D_LogWarning()` 和 `Q3D_LogError()`，完全是因为：在蓝图中改参数比换节点容易得多；
    - Q3D_LOG 目前简单实现了：
      - 封装蓝图日志；
      - 静态配置日志显示等级和屏幕显示颜色；
      - 静态配置是否打印到屏幕；
      - 静态配置是否打印到 Output；
      - 静态配置是否打印到文件；
    - Q3D_LOG 的 TODO List：
      - 动态配置是否打印到文件；
      - 找一个第三方的审计日志和日志上传模块，或者自己实现一个；
      - 动态配置日志上传；

2.  PawnState

    - 或许是我没学精细，Unreal 的 GamePlay 框架中，一个 PlayerState 对应的是一个 Player，那如果一个 Player 想要拥有多个角色怎么办？初步理解还是要在 PlayerState 之下再加一层 PawnState，所以就这么做了；
    - 根据上一节关于组件的使用，这里也定义了三个类：
      - 【数据】暂时没想到要存什么数据所以留空；
      - 【数据容器】U_PawnState 负责数据同步（初始化/序列化/网络同步/关卡同步/读写 DB 等）；
      - 【组件】C_PawnState 负责沟通 Owner（处理生命周期/响应事件/注销等）；
      - 【接口】I_PawnState 负责让 PawnState 功能模块与 Owner 的类型解耦；
    - 之后所有 C++的角色都继承 Q3D_CPPA_Character / Q3D_CPPA_Pawn，所有蓝图的角色继承它的子类 Q3D_BPA_Character / Q3D_BPA_Pawn；

3.  Property

    - 基本框架就如上一节所说，不再赘述。但我们探索更多的内容：属性有没有可以提炼封装的共同特性？
    - 我们看入门者学习视频，发现里边的案例，不管是子弹击中也好，燃烧伤害也好，都是在 Actor 上挂一个 int/float，就成了 HP，然后通过 Damage 传递事件，HP 减一下就算完成了掉血，这真的好吗？
    - 显然不好！虽然我没做过游戏那我也玩过游戏，照我看，可以提炼封装的特性有很多，比如说：
      - 自动阈值处理：自动处理属性减到 0 之后不再往下减的情况，同样的还有设置一个 max，超过 max 只能是 max 的处理；
      - 自动事件处理：属性触底之后，能否默认制造一个广播，让感兴趣的 Actor 去订阅和响应呢？HP 到了 0，宿主要狗带，要播放特效，总不能每个 HP 写一段触发逻辑，所以 HP 应该封装在 Property 中；
      - 光环：打过魔兽都知道，HP 等于 100 并不是永久等于 100，你可能会受到光环的影响暂时变成了 200，随后又变回 100，这段逻辑，也应该存在 Property 中，这样一来，所有 Property 都会有一个光环机制，从而实现策划的各种奇思妙想，比如靠近柱子令角色速度变快；
      - 光环带来了一些附加问题：比如你的 HP，当前值是 70，最大值是 100，策划要求当角色靠近一根柱子时，血量翻倍，此时 70/100 要等比变成 140/200 该如何实现呢？光环撤去之时，或许角色受了伤，140 变成了 120，那么等比从 120/200 变回 60/100 又如何实现呢？
    - 本项目就上述问题，封装了 Property，在处理 HP 的时候，笔者思考了一个问题，HP_NOW 值和 HP_MAX 值，应该放在一个 Property 中，还是两个 Property 中呢？倘若放在一个 Property 中，那么对一个属性的修改，粒度就会变细，从而破坏了封装特性；
    - 本项目的解决方案是：每个 Property 会包含 min、value、max 三个值。但人物的 HP，会分成 Property(HP_NOW)和 Property(HP_MAX)两个属性，而 Property(HP_NOW)中的 max，其实是订阅了 Property(HP_MAX)的 value，这个流程完全符合封装特性；
    - 以上特性伴随了另一个功能模块：伤害和恢复；

4.  Hit

    - 当笔者编码实现伤害和恢复时，发现他们除了一个加一个减之外，其它特性几乎一样，不用写两份代码，所以将它们统一定义为【命中】Hit，Hit 用一个枚举 EQ3D_CPPE_HitType 区分它是【伤害】Damage 还是【恢复】Recover；
    - 接下来是 Hit 的目标【属性】，上节我们已经把 Property 封装起来了，那么速度也好，血量也罢，都是能被 Hit 影响的属性名字而已，那就用枚举类型 EQ3D_CPPE_PropertyName 定义【属性名】吧，Property 存一个 Name，Hit 存一个 Name，它们一对上，就能对属性制造一次【命中】；
    - 学习视频中有个案例，火焰用 Timer 实现了单次或持续的燃烧伤害，当然这只是面向初学者的教程，个人认为 Timer 不应该在火焰本体中实现，而应该在火焰制造的 Hit(Damage)中实现（不然 Timer 要一个个去处理 Actor），火焰应该只有一种 Hit，对每一个 Target 发送一个 Hit 实例；
    - 为了让火焰能够轻松产生 Hit，我们要定义命中的模式：它可以是单次命中、多次命中、持续命中、限时光环和持续光环。它们的工作方式是不同的，单次扣血，多次扣血，持续扣血，临时性或持久性地让属性变化，都可以用 Hit(Mode)来定义；
    - 接下来是命中所造成的伤害值/治疗值，它也许是个数字，也可能是个比例，比如子弹一次打出 100 焦耳，是个固定值。倘若火焰能让任何角色 10 秒就烧死，那就意味着它是一个比例值，每秒扣血 10%，还有一种特殊情况就是依次减半或者不停翻倍，为了处理这种情况，我们用 Hit(Value)来处理；
    - 现在我们可以发动火焰伤害了，火焰制造一个 Hit(HP)(Damage)(Hit_Multiple)(PercentByNow)，然后将 Hit 发往与它碰撞的目标；
    - 作为对比，刚才策划喜欢的那根柱子，可以制造一个 Hit(SPD)(Recover)(Halo_Infinite)(PercentByMax)发往靠近它的物体，以对 SPD 这个速度属性施加一个持续的恢复光环，恢复的值是当前值的倍数；
    - 特别要提到，【命中】的【发起者】不应该知道【目标】对象的类型，所以我们需要一个 I_HitTarget 接口，所有能够接受 Hit 的目标类型，都只要实现这个接口就好，这个就是 Hit 传递给 Target 的接口函数；
    - Hit 在接口中传递，也要在接口中被重算，一个 Hit 最后的产生的值，要有由 Hit 的来源 Maker、媒介 Causer、以及目标 Target 共同决定，比如大法师向手无寸铁的农民打了一个大火球，最终的伤害要由法师、火球、农民三者共同决定，所以 Hit 每次接口传递都应该在接口中被重算；
    - 过程就是：大法师制造一个 Hit(HP)(Damage)(Hit_Once)(Value)，它预期对 Target 的 HP 制造一个单次伤害，每次伤害 Value。法师将 Hit 赋予 Causer 火球，火球将 Value 翻倍输出，火球飞行之后碰到 Target 并传达 Hit，农民发现自己没有护甲，再将 Value 翻倍，并执行伤害；
    - 有了 Hit 和 Property 这两个结构，我们几乎能把魔兽争霸的伤害系统做出来。笔者推演了一遍，发现大多数都能实现：
      - 比如泉水制造了两个持续的固定治疗值：Hit(HP)(Recover)(Hit_Infinite)(Value)和 Hit(MP)(Recover)(Infinite)(Value)；
      - 巫妖的 Nova 制造了一个针对 HP 的单次伤害 Hit(HP)(Damage)(Hit_Once)(Value)和一个针对速度的计时光环 Hit(SPD)(Damage)(Halo_Timed)(PercentByNow)；
    - 至此，Hit/Property 基本能覆盖大多数场景，使用上也相对解耦，但要求每个 Target 重算各种 Hit，也是繁琐的。但我们知道许多重算是有规则的，那就是相性系统，比如说穿刺攻击对某防御属性有特效，这个逻辑完全不用一个个写，直接用一个 Component 去实现就可以了；
    - C_HitTarget 就是这么一个 Component，它把相性克制的规则一次性写好，然后挂接到 Actor 上，Actor 不是已经通过接口 I_HitTarget 接收到伤害了吗，现在 Actor 把伤害转发给自己的 C_HitTarget 组件就可以了；
    - C_HitTarget 组件拥有计算规则，那它也需要取 Owner 的其它属性才能算出结果，我们又不想让它认识每一种 Owner 的类型，怎么办？还记得 Owner 实现了 I_PropertyState 接口吗？C_HitTarget 用这个接口就能取到 Owner 的任何属性，传一个 Enum_PropertyName 就行。

5.  以上就是 Hit 和 Property 共同作用的机制，他们是互相关联的，所以我们的底层 A_Pawn 和 A_Character 在 C_PawnState / I_PawnState 之外，又封装了：C_PropertyState / I_PropertyState / C_HitTarget / I_HitTarget，目前一共三套基础组件；

<br>

#### Q3D_Lib 插件的使用

---

1.  选择 4.27 及以上的版本，新建目标项目；

    - 需要在新建时选择 C++模式；
    - 本插件包含了 FirstPersonCharacter/ThirdPersonCharacter/StartContent 的部分素材；
    - 建议调整编辑器设置(EditorPreference)的 ScrollGesture 为 Standard；

2.  复制本项目 Plugins/下整个 Q3D_Lib 到目标项目，在 UE4 插件的约定机制下，本插件会自动加入编译列表；
3.  重启项目，编译启动之后即可生效；
4.  在 WorldSetting 中选用 Q3D_BPA_GameMode；

    - Q3D_Lib 中的 ActionMapping 和 AxisMapping 会因 Q3D_BPA_GameMode 中读取 DataTable 配置而启用；

5.  在 ProjectSetting->Maps&Modes 中使用 Q3D_BPA_GameMode、Q3D_BPU_GameInstance 和 Q3D_MAP_TestGamePlay_Level01；
6.  重启项目；

<br>

#### 渲染优化

---

1.  在非必要的时候，关闭天空球的 CastShadow；
2.  在非必要的时候，关闭 StaticMesh 的 CastShadow；

<br>

#### (未完待续)

---
