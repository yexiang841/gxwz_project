## 《Unreal 新手村日记》-02-蓝图（Blueprint）

<br>

#### 说明

---

- 项目文档记录了笔者工作积累的云原生微服务技术笔记，以及元宇宙、数字孪生、程序化建模工作学习笔记；
- 项目包含一套元宇宙建模方法论和工程，采用 QGIS -> Blender / Houdini -> Unreal / Unity 的方式实现数字孪生虚拟城市；
- 项目包含一个 Unreal 客户端工程和一个 Unity 客户端工程（建设中）；
- 项目包含一个 Cesium 网页端工程，实现 WebGIS BI 展示；
- 项目包含一个 Vue.js / ElementPlus / TS 网页端工程，实现 Admin 管理后台；
- 项目包含一个 Java / SpringBoot 微服务工程和一个 C# / DotNet 微服务工程（建设中），共用网关和服务注册中心（Nacos），可以协同工作；
- 项目包含一个 Publish 部署工程，采用 Docker / Kubernetes / Jenkins 方式进行服务部署；
- 程序语言涉及 C++ / Java / C# / Python / TS / Shell / VEX；
- 错漏之处恳请读者不吝指正，转载望注明出处：<https://gitee.com/yexiang841/q3d_project/>；
- Copyright 叶湘 ( weixin:yexiang841, email:yexiang841@qq.com ), Shitouren Studio. All Rights Reserved.

<br>

#### Unreal 工作学习笔记

---

- [《Unreal 新手村日记》-00-自学自研项目（Q3D）简介](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch00.Prologue.md)
- [《Unreal 新手村日记》-01-编辑器（Editor）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch01.Editor.md)
- `>>>>` [《Unreal 新手村日记》-02-蓝图（Blueprint）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch02.Blueprint.md)
- [《Unreal 新手村日记》-03-框架（GamePlay）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch03.GamePlay.md)
- [《Unreal 新手村日记》-04-材质（Material）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch04.Material.md)
- [《Unreal 新手村日记》-05-场景编辑（Landscape）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch05.Landscape.md)
- [《Unreal 新手村日记》-06-UI 界面（UMG）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch06.UMG.md)
- [《Unreal 新手村日记》-07-物理模拟/碰撞（Physics/Collision）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch07.Physics.md)
- [《Unreal 新手村日记》-08-骨骼和动画（Skeletal/Animation）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch08.Skeletal.md)
- [《Unreal 新手村日记》-09-粒子系统（ParticleSystem/Niagara）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch09.Niagara.md)
- [《Unreal 新手村日记》-10-C++](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch10.CPP.md)
- [《Unreal 新手村日记》-11-项目设置和打包（Project/Packing）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch11.Project.md)
- [《Unreal 新手村日记》-12-流体（Flip）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch12.Flip.md)
- [《Unreal 新手村日记》-13-毛发系统（Groom）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch13.Groom.md)
- [《Unreal 新手村日记》-14-混沌物理系统（Chaos）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch14.Chaos.md)
- [《Unreal 新手村日记》-15-AI](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch15.AI.md)
- [《Unreal 新手村日记》-16-UE5](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch16.UE5.md)
- [《Unreal 新手村日记》-17-技术美工和程序化建模（TA/PM）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch17.TA.md)

<br>

#### 蓝图基础

---

1. 组件（Component）的添加：

   - 先添加静态网格，再从右侧选择相应的模型；
   - 移动性（Movable）：要打开移动性才能进行动画操作；
   - Scene 组件可以用来调整轴中心点偏移的问题；
   - 组件有很多类型，如网格体组件，粒子组件，UMG 组件等；

2. 成员变量：

   - 公有变量：把变量右边的小眼睛打开即可在外部编辑，注意，变量的编辑可以实时生效，但是要应用到场景中，需要依赖事件和构造函数；
   - 变量的分类（Category）：相当于 Bean，结构体，可以整理变量分类；

3. 纯函数：

   - 没有执行流的函数，只提供数据（其实相当于插入执行流）；
   - 纯函数不能修改类属性；

4. 事件（Event）：

   - 事件的发生需要设定目标和连接执行流；
   - 默认事件：
     - EventBeginPlay（快捷键 P）：在点 Play 的时候会调用的一个事件；
   - 碰撞事件（通常来自触发盒子 TriggerBox）：
     - OnActionBeginOverlap；
     - OnActionEndOverlap；
   - 世界事件：
     - 键盘事件：注意，键盘事件不是默认打开，需要由 EnableInput 节点开启，或者在 ClassDefault 中修改 AutoReceiveInput 为当前玩家；
   - 自定义事件：
     - 可以 CE_XXX 进行命名，生成一个自定义事件，系统就会自动在 CallFunction 中新建一个调用时间的函数；
     - 自定义事件作用一：实现多线程；
     - 自定义事件作用二：实现跨组件调用；
     - 自定义事件调用一：直接在 CallFunction 调用事件；
     - 自定义事件调用二：控制台命令（ConsoleCommand）
       - 通过节点 ExecuteConsoleCommand 调用；
       - 有固定格式：ce CE_XXX；
   - 蓝图类调用关卡蓝图的事件：通过节点 ExecuteConsoleCommand 调用；

5. 事件调度（EventDispatchers）就是代理机制的一种应用（订阅）
6. 实例：

   - 蓝图类需要拖入场景中以生成实例，拖入即执行构造函数（ConstructionScript）；
   - 编辑器面板的 Details 中修改的是实例而不是类，修改 Details 会触发构造函数（ConstructionScript）；

7. 接口（Interface）

   - 当接口没有入参也没有返回时，实现类只能通过 Event_XXX 来实现接口函数；
   - 当接口有入参或者返回值时，可以在左侧双击编辑接口函数，也可以拖入编辑面板中编辑；
   - 调用：
     - 要调用一个实例的接口函数，需要先 CastToXXX，然后才能进行调用；

8. 宏：

   - 宏库必需继承基类，具有相同基类的蓝图才能使用该宏；
   - 宏可以有多个执行流输出（函数不行）；

9. 调试过滤器：设定之后可以实时查看运行线路图；
10. 情景关联：打钩之后，就只有当前对象能用的节点会显示出来；

<br>

#### 常用节点

---

1. MakeLiteralXXX：创造一个值，但只是用于编辑器复用，不存为局部变量；
2. TimeLine：时间轴，设定自定义数值和时间的关系；

   - Play / PlayFromStart / Stop / Reverse / ReverseFromEnd；
   - 要同时连接 Update 和轨迹才有效果；
   - Update 会持续触发；

3. Gate：就是 Enable 和 Disable 的意思，要连三个输入才有效果；
4. FlipFlop：执行翻转；
5. Lerp：将 0 和 1 映射到所有数值；
6. Select：多元表达式，例如将枚举的不同值变成对应的 int；
7. Switch On：就是 switch case，根据不同数值走不同的逻辑；
8. Branch：条件分支；
9. DoOnce：只触发一次，除非被 Reset；
10. DoN：可触发 N 次，被 Reset 之后回复剩余 N 次；
11. GetAllActorOfClass：获取所有的 Actor 类实例，可手动指定 Actor 子类型，然后；

    - 通过 Get 数组的方法获得其中的实例；
    - 通过 ForEachLoop 去遍历每一个实例，加上 Equals 节点和 Branch 节点可用实例名进行筛选；

12. Delay：延时节点，让事件延时执行；
13. MultiGate：让运行依次让从不同的出口输出，可以随机、循环、重置；
14. GetWorldDeltaSecond：获取世界时间，以秒为单位；
15. FindLookAtRotation：找两个物体之间连线的方向；

<br>

#### Actor 常用节点

---

1. SetActorRotation / SetActorLocation / SetActor：组件运动，可以分割结构体引脚进行单个坐标的运动；
2. SetRelativeRotation / SetRelativeLocation / SetRelative：相对运动；
3. SetWorldRotation / SetWorldLocation / SetWorld：绝对运动；
4. AddActorLocalRotation / SetActorLocalOffset：增量运动；
5. Destroy：销毁 Actor；

<br>

#### StaticMeshComponent 常用节点

---

1. SetPhysicsLinearVelocity：设置物理线性速度，只是加速度，不考虑质量，只能用于 StaticMeshComponent；
2. AddImpulse：加冲量速度乘以质量，也就是说质量越大，加的速度越小，只能用于 StaticMeshComponent；
3. SetMaterail：变换材质，只能用于 StaticMeshComponent；
4. SetWorldLocation：设置位置，瞬移；
5. SetVisibility/ToggleVisibility：设置/切换可视性；

<br>

#### Pawn 配置

---

1. AutoPossessAI 用于在 Pawn 生成时自动新增一个 AIController；

   - AIControllerClass 用于指定 AIController 的子类；

2. AutoPossessPlayer 用于设置 Pawn 被某个 Player（的 PlayerController）自动 Possess；
3. 注意当以上两者都设定时，Pawn 会先由 AIController 控制（Possess），然后会 Unpossess，再转交给 PlayerController 控制，所以：

   - 在第一次 Possess 事件中，可以保存一下 AIController 的引用；
   - 在 Unpossess 的事件中，可以手动让之前保存的 AIController 继续接管该 Pawn；
   - 否则这个 Pawn 就会是无控制状态；

<br>

#### 射线检测

---

1. LineTrace：根据线段的路径检测碰撞，通常检测鼠标射线的方法是：

   - 把 Start 设定为鼠标的世界坐标（先 ShowMouseCursor，然后 GetPlayerController.ConvertMouseLocationToWorldSpace）；
   - 把 End 设定为鼠标世界坐标加上方向向量乘以一个距离值；

2. 项目设置 -> Collision -> TraceChannel；

   - TODO:

<br>

#### 音效（Audio）

---

1. 在蓝图中直接拖入音效视频，编辑视频详情，打开重载衰减（），编辑衰减距离；

<br>

#### 过场动画/关卡序列（Sequencer）

---

1. 序列建议以 SEQ_XXX 命名；
2. Ctrl+滚轮可缩放编辑区；
3. 添加摄像机

   - 光圈、焦距、对焦距离、Transform 都可以设定关键帧；
   - 添加多个摄像机时的镜头切换；

4. 世界大纲中的物体可以拖到过场动画中进行参数设定；
5. 可以新增音频频道；
6. 可以直接导出视频；

<br>

#### 后期（PostProcessVolume）

---

参考：<https://zhuanlan.zhihu.com/p/403439442>；
参考：<https://zhuanlan.zhihu.com/p/537853695>；

<br>

#### 视频合成 - 方法一

---

教程：<https://www.bilibili.com/video/BV1Yv4y1A7Pm>；

视频合成需要两个插件：

- Movie Render Queue：影片渲染队列插件；
- Composure：实时合成插件；

视频合成需要打开一个项目设置：

- Project Setting -> Postproccessing -> Enable alhpa channel suport in post processing 设置为 Linear color space only；

需新建两种资源：

- Img Media Source，用来加载序列帧；
- Media Player，用来播放序列帧，这一步某些环境下容易出错，可以选择方法二；

#### 视频合成 - 方法二

---

教程：<https://www.bilibili.com/video/BV17t4y1q74f>；

<br>
