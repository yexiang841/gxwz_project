## 《Unreal 新手村日记》-11-项目设置和打包（Project/Packing）

<br>

#### 说明

---

- 项目文档记录了笔者工作积累的云原生微服务技术笔记，以及元宇宙、数字孪生、程序化建模工作学习笔记；
- 项目包含一套元宇宙建模方法论和工程，采用 QGIS -> Blender / Houdini -> Unreal / Unity 的方式实现数字孪生虚拟城市；
- 项目包含一个 Unreal 客户端工程和一个 Unity 客户端工程（建设中）；
- 项目包含一个 Cesium 网页端工程，实现 WebGIS BI 展示；
- 项目包含一个 Vue.js / ElementPlus / TS 网页端工程，实现 Admin 管理后台；
- 项目包含一个 Java / SpringBoot 微服务工程和一个 C# / DotNet 微服务工程（建设中），共用网关和服务注册中心（Nacos），可以协同工作；
- 项目包含一个 Publish 部署工程，采用 Docker / Kubernetes / Jenkins 方式进行服务部署；
- 程序语言涉及 C++ / Java / C# / Python / TS / Shell / VEX；
- 错漏之处恳请读者不吝指正，转载望注明出处：<https://gitee.com/yexiang841/q3d_project/>；
- Copyright 叶湘 ( weixin:yexiang841, email:yexiang841@qq.com ), Shitouren Studio. All Rights Reserved.

<br>

#### Unreal 工作学习笔记

---

- [《Unreal 新手村日记》-00-自学自研项目（Q3D）简介](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch00.Prologue.md)
- [《Unreal 新手村日记》-01-编辑器（Editor）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch01.Editor.md)
- [《Unreal 新手村日记》-02-蓝图（Blueprint）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch02.Blueprint.md)
- [《Unreal 新手村日记》-03-框架（GamePlay）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch03.GamePlay.md)
- [《Unreal 新手村日记》-04-材质（Material）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch04.Material.md)
- [《Unreal 新手村日记》-05-场景编辑（Landscape）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch05.Landscape.md)
- [《Unreal 新手村日记》-06-UI 界面（UMG）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch06.UMG.md)
- [《Unreal 新手村日记》-07-物理模拟/碰撞（Physics/Collision）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch07.Physics.md)
- [《Unreal 新手村日记》-08-骨骼和动画（Skeletal/Animation）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch08.Skeletal.md)
- [《Unreal 新手村日记》-09-粒子系统（ParticleSystem/Niagara）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch09.Niagara.md)
- [《Unreal 新手村日记》-10-C++](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch10.CPP.md)
- `>>>>` [《Unreal 新手村日记》-11-项目设置和打包（Project/Packing）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch11.Project.md)
- [《Unreal 新手村日记》-12-流体（Flip）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch12.Flip.md)
- [《Unreal 新手村日记》-13-毛发系统（Groom）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch13.Groom.md)
- [《Unreal 新手村日记》-14-混沌物理系统（Chaos）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch14.Chaos.md)
- [《Unreal 新手村日记》-15-AI](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch15.AI.md)
- [《Unreal 新手村日记》-16-UE5](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch16.UE5.md)
- [《Unreal 新手村日记》-17-技术美工和程序化建模（TA/PM）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch17.TA.md)

<br>

#### 系统选择

---

1. 强烈建议使用 Windows 平台。MacOS 下没有 VS 支持，VSCode 支持得也不够好，XCode 更是废材一样的存在；
2. 如果一定要用 Mac，安装引擎时把 Epic Game 中间的空格去掉，避免跟其它软件共用时出现问题；

<br>

#### 目录定义

---

1. Binaries：编译出的可执行文件，不进代码库；
2. Config：工程全局配置文件，核心；
3. Content：大部分内容，核心；
4. DerivedDataCache：缓存，不进代码库；
5. Intermediate：编译中间文件，不进代码库；
6. Saved：不想转变为 uasset 的资源，可以放在 Saved 目录下；
   - Autosaved 目录是编辑器自动保存路径，不进代码库；
7. Source：引擎源码，核心；
8. [工程名].sln，项目文件，非必需，可以从 [工程名].uproject 中生成；
9. [工程名].uproject，ue 工程文件，核心；

<br>

#### 全局配置

---

1. ProjectSetting：

   - 配置 GameInstance；
   - 配置编辑器初始地图（EditorStartupMap）和游戏初始地图（GameDefaultMap）；

2. WorldSetting：

   - 配置 GameMode
   - 配置 DefaultPawnClass - 在出生点默认生产的 Pawn；
   - 配置 HudClass - 待研究；
   - 配置 PlayerControllerClass - 自定义玩家控制器；
   - 配置 GameStatClass - 自定义游戏状态存储；
   - 配置 PlayerStateClass - 自定义玩家状态存储；
   - 配置 SpectatorClass - 自定义观众模式的 Pawn；

3. 外部配置文件：

   - 教程 YouTuber-TheLifeofJevins：<https://www.youtube.com/watch?v=QvN8rNCjDko>；
   - 日志配置：<https://unreal.gg-labs.com/wiki-archives/common-pitfalls/logs-printing-messages-to-yourself-during-runtime>；

<br>

#### 分辨率基础

---

1. 首先 PPI 是物理像素密度，表达的是显示质量，与设计无关；
2. 安卓的设计会用到 DPI 的概念；
3. 2021.6 安卓主流分辨率和长宽比：

   - 低端：1280 x 720 px，16 : 9；（标准 720P，HD，仍有一定的保有量）
   - 中端：1920 x 1080 px，16 : 9；（标准 1080P，FHD，正在退出主流）
   - 高端：2560 x 1440 px，16 : 9；（标准 2K，QHD，正在成为主流）
   - 中端加长：2400 x 1080 px，20 : 9；（FHD+，全面屏）
   - 高端加长：3200 x 1440 px，20 : 9；（WQHD，全面屏）
   - 事实上加长版存在多种分辨率，但宽是固定的，比例都保持在 16:9 到 20:9 之间；
     - 比如小米 10s 是 2340 x 1080 px；
     - 比如三星 s9 是 2960 x 1440 px；
   - 加长版都需要考虑水滴/刘海摄像头、底部虚拟按钮专用空间；
   - 摄像头区域一般都会包含在背景区域之内，所以设计上要求安全区不占用横屏左侧；
   - 通常安卓手机都可以设定底部虚拟按键是否悬浮，如果设置成悬浮，那么背景区域还是要占满横屏右侧；

4. iPhone 分辨率：

   - 6//7/8/S/Plus 等：近似 720P/1080P（没一个标准的），以 667 x 375 pt 为主，略大于 16 : 9；（无刘海）
   - XR：1792 × 828 px，896 x 414 pt，19.478 : 9；（刘海占？）
   - X/XS：2436 × 1125 px，812 x 375 pt，19.488 : 9；（刘海占？）
   - XSMAX：2688 × 1242 px，896 x 414 pt，19.478 : 9；（刘海占？）
   - 11：1792 x 828 px，，896 x 414 pt，19.478 : 9；
   - 11Pro：2436 x 1125 px，812 x 375 pt，19.488 : 9；
   - 11ProMax：2688 x 1242 px，896 x 414 pt，19.478 : 9；
   - 12Mini：2340 x 1080 px，780 x 360 pt，19.5 : 9；（刘海占？）
   - 12/12Pro：2532 x 1170 px，844 x 390 pt，19.477 : 9；（刘海占？）
   - 12ProMax：2778 x 1284 px，926 x 428 pt，19.472 : 9；（刘海占？）
   - 任性得令人发指，详细数据：<https://www.jianshu.com/p/f94724d235b0>；

5. 整体设计前就应该定好 UI 规范，例如：

   - 横屏；
   - 横屏高度始终充满；
   - 横屏宽度可冗余；
   - 背景区比例 20:9，安全区比例 16:9，左右各留出横屏高度 2/9 的设计冗余；
   - 核心控件只放在安全区；

6. 需要考虑安卓和 iOS 适配问题；

<br>

#### 分辨率适配方案

---

1. 如果使用物理像素，那么同样 100x100px 的方块在多种机型中的显示就是天差地别的，所以要用逻辑像素；
2. iPhone 的逻辑像素 pt=1/163inch，每一种机型都设定差不太多的 pt 值，设计时统一用 pt 单位；

   - 比如刘海屏，顶部状态栏占用 44pt 高度，其中摄像头部分占 30pt 高度；
   - 底部操作区占用 34pt 高度；

3. 安卓的逻辑像素 dp=1/160inch，根据屏幕像素密度（dpi），计算缩放倍数；

   - 参考：<https://www.jianshu.com/p/00af3fdf0472>；
   - 适配方案一：RelativeLayout+LinearLayout+Weight；
   - 适配方案二：计算 Density；

4. UE 的适配方案叫 DPIScaling，可以自定义适配规则（ScaleRule），也可以使用四种默认策略：

   - ShortestSide：；
   - LongestSide：；
   - Horizontal：；
   - Vertical：；
   - Unreal 提供安全区组件（SafeZone）；
   - 通常可监听 GameInstance 中的屏幕旋转事件（ApplicationRecievedScreenOrientationChangedNotificationDelegate），通过 SwitchOnEScreenOrientation 进行分支处理，并应用到 SafeZone 的 SetSidesToPad；

<br>

#### 打包设置

---

1. 选择地图列表；

<br>

#### 安卓打包

---

1. 需要下载 NDK，注意版本，根据 Unreal 官方文档 ReleaseNotes 一节下载对应的 NDK；

<br>

#### iOS 打包

---

1. TODO:
