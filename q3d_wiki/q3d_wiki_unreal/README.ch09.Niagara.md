## 《Unreal 新手村日记》-09-粒子系统（ParticleSystem/Niagara）

<br>

#### 说明

---

- 项目文档记录了笔者工作积累的云原生微服务技术笔记，以及元宇宙、数字孪生、程序化建模工作学习笔记；
- 项目包含一套元宇宙建模方法论和工程，采用 QGIS -> Blender / Houdini -> Unreal / Unity 的方式实现数字孪生虚拟城市；
- 项目包含一个 Unreal 客户端工程和一个 Unity 客户端工程（建设中）；
- 项目包含一个 Cesium 网页端工程，实现 WebGIS BI 展示；
- 项目包含一个 Vue.js / ElementPlus / TS 网页端工程，实现 Admin 管理后台；
- 项目包含一个 Java / SpringBoot 微服务工程和一个 C# / DotNet 微服务工程（建设中），共用网关和服务注册中心（Nacos），可以协同工作；
- 项目包含一个 Publish 部署工程，采用 Docker / Kubernetes / Jenkins 方式进行服务部署；
- 程序语言涉及 C++ / Java / C# / Python / TS / Shell / VEX；
- 错漏之处恳请读者不吝指正，转载望注明出处：<https://gitee.com/yexiang841/q3d_project/>；
- Copyright 叶湘 ( weixin:yexiang841, email:yexiang841@qq.com ), Shitouren Studio. All Rights Reserved.

<br>

#### Unreal 工作学习笔记

---

- [《Unreal 新手村日记》-00-自学自研项目（Q3D）简介](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch00.Prologue.md)
- [《Unreal 新手村日记》-01-编辑器（Editor）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch01.Editor.md)
- [《Unreal 新手村日记》-02-蓝图（Blueprint）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch02.Blueprint.md)
- [《Unreal 新手村日记》-03-框架（GamePlay）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch03.GamePlay.md)
- [《Unreal 新手村日记》-04-材质（Material）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch04.Material.md)
- [《Unreal 新手村日记》-05-场景编辑（Landscape）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch05.Landscape.md)
- [《Unreal 新手村日记》-06-UI 界面（UMG）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch06.UMG.md)
- [《Unreal 新手村日记》-07-物理模拟/碰撞（Physics/Collision）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch07.Physics.md)
- [《Unreal 新手村日记》-08-骨骼和动画（Skeletal/Animation）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch08.Skeletal.md)
- `>>>>` [《Unreal 新手村日记》-09-粒子系统（ParticleSystem/Niagara）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch09.Niagara.md)
- [《Unreal 新手村日记》-10-C++](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch10.CPP.md)
- [《Unreal 新手村日记》-11-项目设置和打包（Project/Packing）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch11.Project.md)
- [《Unreal 新手村日记》-12-流体（Flip）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch12.Flip.md)
- [《Unreal 新手村日记》-13-毛发系统（Groom）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch13.Groom.md)
- [《Unreal 新手村日记》-14-混沌物理系统（Chaos）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch14.Chaos.md)
- [《Unreal 新手村日记》-15-AI](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch15.AI.md)
- [《Unreal 新手村日记》-16-UE5](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch16.UE5.md)
- [《Unreal 新手村日记》-17-技术美工和程序化建模（TA/PM）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch17.TA.md)

<br>

#### 粒子系统

---

1. Unreal 中有两个粒子系统：

   - 旧版粒子系统；
   - FX 中的 Niagara；

2. 该如何选择？

   - 旧版粒子系统相当于模板，拿来就用，调调参数就行，易学易用；
   - Niagara 是可编程的粒子系统，跟材质、蓝图类似结构，定制程度更高，需要代码能力；
   - 当然推荐学新的；

<br>

#### 旧版粒子系统

---

1. 粒子材质：见下回；
2. 发射源（）：每个发射源是一种规则的粒子，可以混在一起；
3. 发射原点（）：主要调整位置和旋转轴向；
4. 粒子个数区间：在生成（）中调整发射比率（）；
5. 粒子大小区间：在初始大小（）中调整比率（），还有一个比率缩放（）可以直接乘上倍数；
6. 粒子角度/速度区间：在初始速度（）中调整向量，向量即是角度，也是速度；

   - 在初始数读中还可调节发布向量模式，可以选择常量或者区间，常量的话通常用于定向集束；

7. 粒子生命周期区间：在生命周期（）中设置秒数区间；
8. 粒子的颜色：

   - 可使用初始颜色（）
   - 可使用生命周期内颜色（），调整常量曲线（）中两个点的颜色，使其线性变化；

9. 粒子力场：右键添加加速（）->恒加速度（），比如向下的重力；
10. 粒子碰撞：右键添加碰撞，主要调整回弹力；

    - 如果在响应（）中选择停止，粒子碰撞之后就不会弹跳，适合用于模拟雪；

11. 发射源形状：右键添加位置->各种形态，比如可以用圆柱体下雪；
12. 光照：右键添加光照，使粒子对外发光；
13. 粒子速度随生命周期变化：右键添加速度->速度/生命；
14. 边界：在通用设置上（单击空白处），设定边界（）范围；
15. 外部吸引：右键添加吸引（）->点重力（）就可以制造一个点，在设定的影响范围内，粒子都会被吸过去；
16. 类型数据：在右键->类型数据（）中添加；

    - GPUSprites：；不开这个就是纯 CPU 模拟，打开这个才能呈现更多的粒子效果；
    - 条带数据：TODO:

<br>

#### 粒子材质

---

1. 做火焰或者其它动画时，可能会需要用到动画材质

   - 用 Moho 制作动画帧；
   - 用 PS 对动画帧制作 UV 序列（文件->自动->联系表），然后导入 Unreal 作为材质使用；

2. 材质子 UV

   - TODO:

<br>

#### 粒子事件

---

1. 用于在粒子的生命周期中产生事件，从而触发其它发射源的开始（传递时间和位置到新粒子）；

<br>

#### Niagara

---

1. 发射器和 Niagara 系统：一个 Niagara 系统可以组合多个发射器，发射器可以复用；
2. 粒子的类型：

   - 材质粒子（Sprite）：没有网格体，只是显示一个材质；
   - 网格体粒子（Mesh）：需要设置网格体，需要控制面数；
   - 条带粒子（Beam）：用来制作闪电、拖尾等；

3. 发射器七组属性：要注意节点放入不同属性会有不同效果；

   - 发射器设置（EmitterSetting）：；
     - 切换 CPU 模拟或者 GPU 模拟，GPU 模拟需要设定固定边界（暂时不理解）；
     - 粒子和条带用 GPU 模拟可以极大解约空间，烟雾本机实测用 CPU 模拟效果较好；
   - 发射器生成：；
   - 发射器更新：
     - 循环模式（LoopBehavior）：；
     - 发生体（SpawnBurstInstantaneous）：比较精确地控制发生个数，一般较少时使用；
     - 发生率（SpawnRate）：每秒生成的粒子个数，会作用于每一个发生体；
   - 粒子生成：；
     - 初始粒子（InitializeParticle）：基础生存时间，基础颜色，基础粒子大小，粒子大小模式；
     - 初始速度（AddVelocity）：用简单的速度使粒子运动；
     - 发射源形态（Location）：；
       - 圆形/方形/环形/筒形/锥形生成源：Sphere/Box/Torus/Cylinder/Cone Location；
         - 要注意条带粒子使用这些形态会不太规则，尽量避免；
       - 静态网格体（StaticMeshLocation）：导入一个静态网格体作为粒子发生源的形状；
       - 骨骼网格体（SkeletalMeshLocation）：；
     - （InitializeMeshReproductionSprite）：类似网格体生成源；
   - 粒子更新：；
     - 颜色变化（ScaleColor）：通过时间曲线改变粒子颜色；
     - 颜色覆盖（Color）：可直接覆盖基础颜色，同时提供更灵活的控制（比如曲线、参数等）；
     - 尺寸变化（ScaleSize）：通过时间曲线改变粒子尺寸；
     - 线性力场（LinearForce）：位置、范围、力度；
     - 点吸引力（PointAttractionForce）：位置、范围、力度、力向量；
     - 点排斥力（PointForce）：；
     - 卷曲噪声力（CurlNoiseForce）：模拟自然界的一些自然干扰，适合用来模拟流体、烟雾的随机扰动；
     - 旋涡力（VortexForce）：制造旋涡力场；
     - 重力（GravityForce）：简单的重力场；
     - 摩擦力（Drag）：用来模拟粒子粘滞或者说受空气阻力影响很大的感觉；
     - 碰撞（Collision）：；
     - 力场与速度解算（SolveForcesAndVelocity）：注意，此项对顺序有要求，必须放在所有力场和加速度的后面；
     - 子 UV 动画（SubUVAnimation）：用来逐帧播放子 UV 材质；
     - 修改 Mesh 朝向（OrientMeshToVector）：；
     - 产生事件：
       - 产生事件的前提是在发射器设置打开本地空间（用以产生位置参数）和持久 ID；
       - 碰撞事件（GenerateCollisionEvent）：粒子与外部碰撞时产生事件；
       - 位置事件（GenerateLocationEvent）：每个粒子所达的位置产生一个事件；
       - 结束事件（GenerateDeathEvent）：每个粒子生命周期结束时产生一个事件；
   - 事件处理器：；
     - 事件处理器属性：在此监听自己活着其它发射器产生的事件，用以触发操作；
     - 执行模式：
     - 每帧最大事件：
     - 生成数量：
     - 随机生成数量：
   - 渲染：
     - Sprite 渲染器：用来设置图片材质；
     - 光线渲染器：使粒子真正发光（可以照亮周围）；

4. 值模型：

   - 常数（含 Vector）：设置一个常数；
   - 单值控制（FromFloat）：用一个值控制 Vector 中多个值；
   - 随机范围（RandomRange）：在一个范围内随机；
   - 单值随机（RandomVector）：一个值控制 Vector 三个值的随机；
   - 时间曲线（FromCurve）：根据时间曲线赋值；
   - 距离中心点位置（NormalizeDistanceRange）：设定一个距离，让粒子距离中心点的位置归一到 0 和 1 之间；
   - 四则运算（Multiply/Divide/Add/Sub）：；

5. 参数栏：

   - 粒子属性：可在粒子属性栏添加变量，以控制基础粒子属性，比如 ScaleSize；
   - 发射器属性（）：
     - 粒子属性阅读器（）：；

6. 暂存区：相当于临时的模块脚本；
7. 模块脚本（ModuleScript）：

   - 粒子属性：；
   - 模块属性：
     - 音频示波器；

8. 蓝图与 Niagara 系统通信：

   - 用户公开：设置可被蓝图调用的参数；
