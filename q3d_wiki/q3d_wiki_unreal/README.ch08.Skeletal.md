## 《Unreal 新手村日记》-08-骨骼和动画（Skeletal/Animation）

<br>

#### 说明

---

- 项目文档记录了笔者工作积累的云原生微服务技术笔记，以及元宇宙、数字孪生、程序化建模工作学习笔记；
- 项目包含一套元宇宙建模方法论和工程，采用 QGIS -> Blender / Houdini -> Unreal / Unity 的方式实现数字孪生虚拟城市；
- 项目包含一个 Unreal 客户端工程和一个 Unity 客户端工程（建设中）；
- 项目包含一个 Cesium 网页端工程，实现 WebGIS BI 展示；
- 项目包含一个 Vue.js / ElementPlus / TS 网页端工程，实现 Admin 管理后台；
- 项目包含一个 Java / SpringBoot 微服务工程和一个 C# / DotNet 微服务工程（建设中），共用网关和服务注册中心（Nacos），可以协同工作；
- 项目包含一个 Publish 部署工程，采用 Docker / Kubernetes / Jenkins 方式进行服务部署；
- 程序语言涉及 C++ / Java / C# / Python / TS / Shell / VEX；
- 错漏之处恳请读者不吝指正，转载望注明出处：<https://gitee.com/yexiang841/q3d_project/>；
- Copyright 叶湘 ( weixin:yexiang841, email:yexiang841@qq.com ), Shitouren Studio. All Rights Reserved.

<br>

#### Unreal 工作学习笔记

---

- [《Unreal 新手村日记》-00-自学自研项目（Q3D）简介](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch00.Prologue.md)
- [《Unreal 新手村日记》-01-编辑器（Editor）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch01.Editor.md)
- [《Unreal 新手村日记》-02-蓝图（Blueprint）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch02.Blueprint.md)
- [《Unreal 新手村日记》-03-框架（GamePlay）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch03.GamePlay.md)
- [《Unreal 新手村日记》-04-材质（Material）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch04.Material.md)
- [《Unreal 新手村日记》-05-场景编辑（Landscape）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch05.Landscape.md)
- [《Unreal 新手村日记》-06-UI 界面（UMG）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch06.UMG.md)
- [《Unreal 新手村日记》-07-物理模拟/碰撞（Physics/Collision）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch07.Physics.md)
- `>>>>` [《Unreal 新手村日记》-08-骨骼和动画（Skeletal/Animation）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch08.Skeletal.md)
- [《Unreal 新手村日记》-09-粒子系统（ParticleSystem/Niagara）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch09.Niagara.md)
- [《Unreal 新手村日记》-10-C++](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch10.CPP.md)
- [《Unreal 新手村日记》-11-项目设置和打包（Project/Packing）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch11.Project.md)
- [《Unreal 新手村日记》-12-流体（Flip）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch12.Flip.md)
- [《Unreal 新手村日记》-13-毛发系统（Groom）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch13.Groom.md)
- [《Unreal 新手村日记》-14-混沌物理系统（Chaos）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch14.Chaos.md)
- [《Unreal 新手村日记》-15-AI](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch15.AI.md)
- [《Unreal 新手村日记》-16-UE5](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch16.UE5.md)
- [《Unreal 新手村日记》-17-技术美工和程序化建模（TA/PM）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch17.TA.md)

<br>

#### 骨架/骨骼树（Skeleton）

---

1. 骨架包括一组用树状结构组织在一起的骨头（Bone）和插槽（Socket）；
2. 骨架和蒙皮是姿势和运动的建模基础，骨骼带动蒙皮运动，极大降低了多面体操控的复杂度；
3. 骨架和蒙皮是在别的建模软件中创建的，Unreal 不提供骨骼建模能力；
4. 插槽（Socket）：用来附加物体的位置点

   - 右键在骨骼树中添加插槽；
   - 可以在插槽上右键添加预览物，但仅仅是预览，不会真实应用；
   - 在角色蓝图中新建物体，置于骨骼网格体组件之下；
   - 在 ParentSocket 选项之下选择插槽；

5. FK 骨骼结构：正向运动学，肩膀和手肘决定手腕；
6. IK 骨骼结构：逆向运动学，肩膀和手腕决定手肘；
7. 姿势（Pose）

   - 骨架中每一个骨骼的位置、旋转、位移组合就是姿势；
   - 在 Unreal 中，姿势存储为姿势资产（PoseAsset）；
   - 姿势有权重，表示：

<br>

#### 骨骼网格体（SkeletalMesh）

---

1. Unreal 中，模型（蒙皮）附加骨架之后，存储为骨骼网格体；
2. 人形的骨骼网格体通常命名为 SK_Mannequin；
3. 骨骼网格体常用蓝图节点：

   - PlayAnimation：播放动画；
   - Montage：播放蒙太奇动画：

<br>

#### 动画（Animation）

---

1. 连贯的一组姿势形成动画片段，在 Unreal 中存储为运动资产（MovementAsset）；
2. 动画绑定到骨架，骨架绑定到模型；

   - 所以，模型换皮不换骨架，也可以做一样的动画
   - 反过来说，模型也可以给骨架换不同的动画（只要匹配）；

3. 通知（Notifies）

   - 动画界面中可以编辑通知；
   - 通知就是事件，可添加声音或特效等触发资源；

4. 曲线（Curves）；

   - 动画界面中可以编辑通知（Notifies）

5. 右侧可导入的动画资产包括动画序列（AnimationSequence）和混合空间（BlendSpace）；
6. RateScale：动画播放的速度；
7. 叠加动画：

   - 先选择一个参考姿势，然后设置姿势增量，即可在原来姿势上叠加为新的姿势，叠加的过程是为叠加动画
   - 在附加设置中将 Additive 设置，就设置为叠加动画，保存的是姿势增量：

8. 混合空间（BlendSpace）

   - 在两个动画之间进行插值，比如蹲和站立，可以插值为半蹲；
   - BlendSpace1D 指的是只有一个控制维度（所有姿势和动画都进行插值）；
   - 不带 1D 其实是 2D，X 轴和 Y 轴的含义可以根据动作自定义；
   - 参数解析：
     - 网格分区数量，表示插值动作的个数；
     - 插值时间，表示动画过渡所用的时间；
     - 最小值和最大值，值定义和区间应该取可理解的含义，比如转向可以从-180 到 180，速度可以从 0 到 600；
   - 同步组：
     - 当两个动作频率不一样时，混合起来就会出现不自然；
     - Unreal 可设定领路者和跟随者，跟随者依据领路者的频率调整自己的时间比例；
     - 同步标记：可以在动作中设置同步标记比如 Left 和 Right，相同的标记会协调同步组中的不同动作进行同步；
       - 同一组的每个动作中都拥有的同步标记标记才会有效果；

9. 瞄准偏移（AimOffsetBlendSpace）

   - 瞄准偏移是混合空间的一个子类，混合了多种姿势来实现“瞄准”动作的左右上下偏移；

10. 动画蒙太奇（Montage）

    - 蒙太奇是由动画片段组成的动作剪辑
    - 播放蒙太奇不会扰乱状态机；
    - 播放蒙太奇需要在动作中预留插槽（Slot）；
    - 插槽（Slot）

11. 动画曲线：

    - TODO:

<br>

#### 动画蓝图（AnimBP）

---

1. 建立动画蓝图需要绑定一副骨架；
2. 动画蓝图会比普通蓝图多出一种动画图表 AnimationGraph；
3. 动画图表的作用是建立状态机（StateMachine）和输出最终动画姿势（FinalAnimationPose）；
4. 状态机：

   - 状态机的每一个状态，输出是姿势或者动画，输出可以由姿势、动画、混合空间连接；
   - 动画也可右键转换为姿势；
   - 过渡规则：是一个布尔变量，表示当前是否可以从 A 状态转移到 B 状态；

5. 应用动画蓝图：在 Charactor 中选择 Mesh，然后在细节面板的动画类中选择动画蓝图；
6. 动画蓝图常用节点：

   - CalculateDirection：计算角色方向；
   - LayeredBlendPerBone：；

<br>

#### 高级运动系统

---

1. <https://blog.csdn.net/UWA4D/article/details/106121913>；
