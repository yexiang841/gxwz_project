## 《Unreal 新手村日记》-06-UI 界面（UMG）

<br>

#### 说明

---

- 项目文档记录了笔者工作积累的云原生微服务技术笔记，以及元宇宙、数字孪生、程序化建模工作学习笔记；
- 项目包含一套元宇宙建模方法论和工程，采用 QGIS -> Blender / Houdini -> Unreal / Unity 的方式实现数字孪生虚拟城市；
- 项目包含一个 Unreal 客户端工程和一个 Unity 客户端工程（建设中）；
- 项目包含一个 Cesium 网页端工程，实现 WebGIS BI 展示；
- 项目包含一个 Vue.js / ElementPlus / TS 网页端工程，实现 Admin 管理后台；
- 项目包含一个 Java / SpringBoot 微服务工程和一个 C# / DotNet 微服务工程（建设中），共用网关和服务注册中心（Nacos），可以协同工作；
- 项目包含一个 Publish 部署工程，采用 Docker / Kubernetes / Jenkins 方式进行服务部署；
- 程序语言涉及 C++ / Java / C# / Python / TS / Shell / VEX；
- 错漏之处恳请读者不吝指正，转载望注明出处：<https://gitee.com/yexiang841/q3d_project/>；
- Copyright 叶湘 ( weixin:yexiang841, email:yexiang841@qq.com ), Shitouren Studio. All Rights Reserved.

<br>

#### Unreal 工作学习笔记

---

- [《Unreal 新手村日记》-00-自学自研项目（Q3D）简介](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch00.Prologue.md)
- [《Unreal 新手村日记》-01-编辑器（Editor）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch01.Editor.md)
- [《Unreal 新手村日记》-02-蓝图（Blueprint）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch02.Blueprint.md)
- [《Unreal 新手村日记》-03-框架（GamePlay）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch03.GamePlay.md)
- [《Unreal 新手村日记》-04-材质（Material）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch04.Material.md)
- [《Unreal 新手村日记》-05-场景编辑（Landscape）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch05.Landscape.md)
- `>>>>` [《Unreal 新手村日记》-06-UI 界面（UMG）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch06.UMG.md)
- [《Unreal 新手村日记》-07-物理模拟/碰撞（Physics/Collision）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch07.Physics.md)
- [《Unreal 新手村日记》-08-骨骼和动画（Skeletal/Animation）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch08.Skeletal.md)
- [《Unreal 新手村日记》-09-粒子系统（ParticleSystem/Niagara）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch09.Niagara.md)
- [《Unreal 新手村日记》-10-C++](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch10.CPP.md)
- [《Unreal 新手村日记》-11-项目设置和打包（Project/Packing）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch11.Project.md)
- [《Unreal 新手村日记》-12-流体（Flip）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch12.Flip.md)
- [《Unreal 新手村日记》-13-毛发系统（Groom）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch13.Groom.md)
- [《Unreal 新手村日记》-14-混沌物理系统（Chaos）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch14.Chaos.md)
- [《Unreal 新手村日记》-15-AI](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch15.AI.md)
- [《Unreal 新手村日记》-16-UE5](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch16.UE5.md)
- [《Unreal 新手村日记》-17-技术美工和程序化建模（TA/PM）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch17.TA.md)

<br>

#### UMG 基础概念

---

1. 通常用 UI 目录存储界面；
2. 创建 UI->WidgetBlueprint，可以用 W_XXX 命名 UI 蓝图类；
3. 在组件蓝图类中 CreateWidget，在 Class 中选择以上创建的 UI 蓝图类；
4. 连出 AddToViewViewport，将 UI 显示到界面中；
5. 通常也应该建立一个 W_XXX 类型的实例变量，将 UI 界面存起来，以便之后引用；
6. W_XXX 类型的实例变量可以如下操作：

   - SetInputMode：有三种模式，只控制 UI，只控制场景，两者皆控制；

7. 控件右侧的 bind 是指将值和函数返回值绑定，调用频率是帧调用，所以要注意性能；
8. 可视化：

   - Visible：可见；
   - Collapsed：塌陷，隐藏并且从空间移除；
   - Hidden：隐藏但留在空间中；
   - Not Hit-Testable：可见但无交互，可选是否应用到子控件；

9. ToolTipWidget：类似于鼠标停留时跳出来的详情面板；
10. Alignment：设定控件的坐标原点，注意是 0~1 的范围，是个比例值；
11. 锚点：

    - 点锚点：；
    - 线锚点：通常用于拉伸，比如进度条等；

12. UI 控件：

    - CanvasPanel：基础面板，用于整个屏幕；
      - 如果控件不是针对整个屏幕的话，可以不要这个控件（并且使用 Desired 工作区）；
    - BackgroundBlur：背景模糊控件；
    - Image：图片
    - ProcessBar：进度条；
    - SizeBox：用于强制调整子控件的大小，只能有一个子控件；
      - 常用的子控件有 HorizontalBox 和 VerticalBox；
    - ScrollBox：滚动控件，可出现滚动条；
    - Spacer：空间；
    - Border：背景；
    - Overlay：层级；
    - InputKeySelector：按键选择器；

13. UI 控件的使用：

    - 用 Border 框定范围
    - 里边放 SizeBox / VerticalBox / HorizontalBox 进行模块布局；
    - 用 Spacer / Overlay 进行控件布局；
    - 将控件选为变量，就可以在蓝图中调用；

14. UI 事件：

    - 需要在 UI 蓝图中使用继承的方式复写系统事件；
    - 返回值可使用 handle 或者 unhandle，决定了事件是否向子控件继续传递；
    - 要接受交互事件，首先要把顶层 UI 的 IsFocusable 打开，然后在 Widget 的 EventConstruct 中使用 SetFocus 节点；
    - 默认事件：
      - EventOnInitialized：类似于 Actor 蓝图中的 EventBeginPlay；

15. UI 资源导入，有两个设置比较重要：

    - CompressionSetting 应设置为 UserInterface2D；
    - TextureGroup 应设置为 UI；

16. UI 动画：

    - 先添加一个 Animation，再添加一个 Track，然后添加跟踪的属性，就可以进行 K 帧操作了；
    - 完成动画编辑之后，从蓝图事件中使用 PlayAnimation 节点；
    - 当 PlayerAnimation 节点的 NumLoopsToPlay 设定为 0 时，就是无限循环播放，此时就需要 StopAnimation 节点来停止动画；

17. 数值绑定：

    - 绑定函数：比较灵活，但是每一帧都在计算，资源消耗大；
    - 绑定变量；
    - 主动设置控件的值；
    - 使用事件调度器；

18. 在世界中渲染 UI：

    - 在 Actor 中直接新建；

19. UI 蓝图类：

    - Operation 类：例如 DragDropOperation（拖拽操作）；

<br>

#### 架构设计原则

---

1. MVC：

   - V 不应该认识 C，V 只需要认识 M；
   - V 只负责触发自己的事件分发器，至于事件的绑定，由它的创建者去做；
   - V 的创建者可能是上级 V 或者 C：
   - 如果是上级 V，应继续传播事件（也可以顺便处理自己层级的响应）；
   - 如果是 C，则可以绑定处理者和处理函数；
   - V 对 M 的数据绑定：
   - 如果数据实时性要求不高，V 不应该直接绑定 M 的引用，以避免 M 引用失效造成 V 崩溃；
   - 推荐的做法是 V 持有并绑定 M 的数据副本，然后监听 M 的数据变化，根据自己需要（频率）同步本地数据副本；
   - 如果数据实时性要求高，应绑定 M 的引用，但需要架构确保 M 的有效性；
   - V 的基本流程应该是：
   - Model_Set，在 ModelSet 中绑定事件，ModelSet 应该由它的创建者或者构造函数去调用；
   - Model_Reload，用于同步控件绑定的数据到自己的副本；
   - 以上步骤，在初始化时，应先 Set，再 Reload；
