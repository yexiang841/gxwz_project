## 《Unreal 新手村日记》-01-编辑器（Editor）

<br>

#### 说明

---

- 项目文档记录了笔者工作积累的云原生微服务技术笔记，以及元宇宙、数字孪生、程序化建模工作学习笔记；
- 项目包含一套元宇宙建模方法论和工程，采用 QGIS -> Blender / Houdini -> Unreal / Unity 的方式实现数字孪生虚拟城市；
- 项目包含一个 Unreal 客户端工程和一个 Unity 客户端工程（建设中）；
- 项目包含一个 Cesium 网页端工程，实现 WebGIS BI 展示；
- 项目包含一个 Vue.js / ElementPlus / TS 网页端工程，实现 Admin 管理后台；
- 项目包含一个 Java / SpringBoot 微服务工程和一个 C# / DotNet 微服务工程（建设中），共用网关和服务注册中心（Nacos），可以协同工作；
- 项目包含一个 Publish 部署工程，采用 Docker / Kubernetes / Jenkins 方式进行服务部署；
- 程序语言涉及 C++ / Java / C# / Python / TS / Shell / VEX；
- 错漏之处恳请读者不吝指正，转载望注明出处：<https://gitee.com/yexiang841/q3d_project/>；
- Copyright 叶湘 ( weixin:yexiang841, email:yexiang841@qq.com ), Shitouren Studio. All Rights Reserved.

<br>

#### Unreal 工作学习笔记

---

- [《Unreal 新手村日记》-00-自学自研项目（Q3D）简介](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch00.Prologue.md)
- `>>>>` [《Unreal 新手村日记》-01-编辑器（Editor）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch01.Editor.md)
- [《Unreal 新手村日记》-02-蓝图（Blueprint）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch02.Blueprint.md)
- [《Unreal 新手村日记》-03-框架（GamePlay）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch03.GamePlay.md)
- [《Unreal 新手村日记》-04-材质（Material）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch04.Material.md)
- [《Unreal 新手村日记》-05-场景编辑（Landscape）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch05.Landscape.md)
- [《Unreal 新手村日记》-06-UI 界面（UMG）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch06.UMG.md)
- [《Unreal 新手村日记》-07-物理模拟/碰撞（Physics/Collision）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch07.Physics.md)
- [《Unreal 新手村日记》-08-骨骼和动画（Skeletal/Animation）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch08.Skeletal.md)
- [《Unreal 新手村日记》-09-粒子系统（ParticleSystem/Niagara）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch09.Niagara.md)
- [《Unreal 新手村日记》-10-C++](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch10.CPP.md)
- [《Unreal 新手村日记》-11-项目设置和打包（Project/Packing）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch11.Project.md)
- [《Unreal 新手村日记》-12-流体（Flip）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch12.Flip.md)
- [《Unreal 新手村日记》-13-毛发系统（Groom）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch13.Groom.md)
- [《Unreal 新手村日记》-14-混沌物理系统（Chaos）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch14.Chaos.md)
- [《Unreal 新手村日记》-15-AI](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch15.AI.md)
- [《Unreal 新手村日记》-16-UE5](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch16.UE5.md)
- [《Unreal 新手村日记》-17-技术美工和程序化建模（TA/PM）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch17.TA.md)

<br>

#### 资源基本操作

---

1. 导入：

   - 在编辑器外迁移：把文件夹拖入 Content，再重新打开，打开时会重新识别；
   - 在编辑器内迁移：
     - 打开源工程，选择资源，如果是目录则右键迁移（），如果是资源则右键资源操作->迁移；
     - 然后选择目标工程的 Content 目录；

2. 引用查看器（ReferenceViewer）：查看一个资源用到的其它资源，常用于理清依赖关系；
3. 工程目录结构：<https://blog.csdn.net/a359877454/article/details/53382539>；
4. 项目迁移时：Binary / Intermediate / Saved 目录都可以删除；

<br>

#### 视图操作

---

1.  视口平移/环绕/远近：鼠标右键+asdw，用于找角度和位置；
2.  视口前后平移（微调）：鼠标左键点击移动；
3.  视口上下左右平移（微调）：鼠标右键触摸移动；
4.  视口上下左右旋转（微调）：鼠标右键点击移动；
5.  物体移动/旋转/缩放：w/e/r；
    - 避坑警告：在场景中双击静态网格体之后再编辑，有可能会修改到子组件的相对位置；
6.  分组：Ctrl+G / Shift+G；
7.  快速复制：Ctrl+W；
8.  保存所有：Ctrl+Shift+S；
9.  运行游戏：Alt+P；
10. 模拟运行：Alt+S；
11. 快速复制：Alt+变换；
12. 让物体放置到地面：End（在 Mac 上是 Fn+右方向），注意，准确地说不是放到地面，是放到下方碰撞体之上；
13. 显示对齐点：V+变换；
14. 快捷键：
    - 聚焦选中的物体：F；
    - 显示和隐藏不属于场景的物体：G；
    - 显示和隐藏选中的物体：H / Ctrl+H；
    - 显示所有碰撞：Alt+C；
    - 显示场景中的雾：Alt+F；
    - 选择半透明物体的开关：T；
    - 测距：鼠标中键；
    - 快速打开物体的蓝图类：Ctrl+E；
    - 在内容浏览器中快速找到物体的类：Ctrl+B；

<br>

#### 画刷（BSP）

---

1.  添加型：正常放置物体；
2.  减去型：布尔减操作；
3.  调整画刷 xyz 尺寸不会拉伸材质，但是拉伸工具会拉伸材质；
4.  Hollow：允许内部结构（材质），打开 Hollow 之后就可以调整 WallThickness；
5.  固体性：
    - 固体：有阻挡效果，可以是减去型；
    - 半固体：有阻挡效果，但不可以是减去型；
    - 非固体：无法跟其它物体互动；
6.  导出为静态网格体（StaticMesh）：选中 BSP -> 详情 -> 画刷设置 -> 高级选项 -> 创建静态网格体；
7.  画刷（BSP）的材质操作：
    - 在【几何体】中选择匹配的画刷可以给物品赋予统一材质，不然就会一个个面地赋予材质；
    - 先点材质，再添加 BSP 会直接赋予统一材质；
    - 在【表面属性】中可以微调材质的平移缩放旋转等；

<br>

#### 静态网格体（StaticMesh）

---

1.  静态网格体合并：选中多个 Actor -> 窗口 -> 开发者工具 -> 合并 Actor；
2.  碰撞和物理模拟见后章；
