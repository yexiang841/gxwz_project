## 《Unreal 新手村日记》-10-C++

<br>

#### 说明

---

- 项目文档记录了笔者工作积累的云原生微服务技术笔记，以及元宇宙、数字孪生、程序化建模工作学习笔记；
- 项目包含一套元宇宙建模方法论和工程，采用 QGIS -> Blender / Houdini -> Unreal / Unity 的方式实现数字孪生虚拟城市；
- 项目包含一个 Unreal 客户端工程和一个 Unity 客户端工程（建设中）；
- 项目包含一个 Cesium 网页端工程，实现 WebGIS BI 展示；
- 项目包含一个 Vue.js / ElementPlus / TS 网页端工程，实现 Admin 管理后台；
- 项目包含一个 Java / SpringBoot 微服务工程和一个 C# / DotNet 微服务工程（建设中），共用网关和服务注册中心（Nacos），可以协同工作；
- 项目包含一个 Publish 部署工程，采用 Docker / Kubernetes / Jenkins 方式进行服务部署；
- 程序语言涉及 C++ / Java / C# / Python / TS / Shell / VEX；
- 错漏之处恳请读者不吝指正，转载望注明出处：<https://gitee.com/yexiang841/q3d_project/>；
- Copyright 叶湘 ( weixin:yexiang841, email:yexiang841@qq.com ), Shitouren Studio. All Rights Reserved.

<br>

#### Unreal 工作学习笔记

---

- [《Unreal 新手村日记》-00-自学自研项目（Q3D）简介](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch00.Prologue.md)
- [《Unreal 新手村日记》-01-编辑器（Editor）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch01.Editor.md)
- [《Unreal 新手村日记》-02-蓝图（Blueprint）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch02.Blueprint.md)
- [《Unreal 新手村日记》-03-框架（GamePlay）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch03.GamePlay.md)
- [《Unreal 新手村日记》-04-材质（Material）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch04.Material.md)
- [《Unreal 新手村日记》-05-场景编辑（Landscape）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch05.Landscape.md)
- [《Unreal 新手村日记》-06-UI 界面（UMG）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch06.UMG.md)
- [《Unreal 新手村日记》-07-物理模拟/碰撞（Physics/Collision）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch07.Physics.md)
- [《Unreal 新手村日记》-08-骨骼和动画（Skeletal/Animation）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch08.Skeletal.md)
- [《Unreal 新手村日记》-09-粒子系统（ParticleSystem/Niagara）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch09.Niagara.md)
- `>>>>` [《Unreal 新手村日记》-10-C++](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch10.CPP.md)
- [《Unreal 新手村日记》-11-项目设置和打包（Project/Packing）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch11.Project.md)
- [《Unreal 新手村日记》-12-流体（Flip）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch12.Flip.md)
- [《Unreal 新手村日记》-13-毛发系统（Groom）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch13.Groom.md)
- [《Unreal 新手村日记》-14-混沌物理系统（Chaos）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch14.Chaos.md)
- [《Unreal 新手村日记》-15-AI](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch15.AI.md)
- [《Unreal 新手村日记》-16-UE5](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch16.UE5.md)
- [《Unreal 新手村日记》-17-技术美工和程序化建模（TA/PM）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch17.TA.md)

<br>

#### 常用源码位置

---

1. Source/Runtime/Core/CoreUObject：UObject 所在地；
2. Source/Runtime/Core/Container：各种常用容器，如 FString / TArray / TSet / TMap 等；
3. Source/Runtime/Engine/Classes/GameFramework：GamePlay 架构

<br>

#### 新建 C++ 类

---

1. 编辑器中右键新建即可；

<br>

#### 删除 C++ 类

---

1. 关闭 Unreal 编辑器；
2. 在代码编辑器中删除文件（只是删除了文件索引）；
3. 在文件系统中删除.h 文件、.cpp 文件、Binaries 目录；
4. 在代码编辑器中重新生成解决方案（亲测非必须）；
5. 重新打开 Unreal 编辑器；

<br>

#### MyClass.generated.h

---

1. 由 UHT 工具生成的头文件，用来链接生成 UHT 生成反射的代码文件；

<br>

#### UCLASS()：类声明宏定义

---

1. 需要在 class 和类名之间加 MYPROJECT_API；
2. 类定义的下一行必须是 GENERATED_BODY()或者 GENERATED_UCLASS_BODY()；

   - GENERATED_UCLASS_BODY 会多声明构造函数，属于旧版，已废弃；
   - 在 GENERATED_UCLASS_BODY()之后定义的成员变量是 private 的；

3. UCLASS( Abstract )表示抽象类，不能被实例化，只能被继承；
4. UCLASS( Blueprintable )：可在蓝图中使用（实例化和继承）；
5. UCLASS( config = Game )：将配置信息保存到哪个配置文件，Game 表示的是文件名；

<br>

#### UPROPERTY()：属性声明宏定义

---

1. 编辑器可见性：

   - UPROPERTY( EditAnywhere )：在编辑器中可见可编辑（CDO 和 Instance）；
   - UPROPERTY( EditDefaultsOnly )：只能在 CDO 中编辑；
   - UPROPERTY( EditInstanceOnly )：只有丢进场景中时（生成了实例），才可以在 Detail 面板中编辑；
   - UPROPERTY( VisibleAnywhere )：在编辑器中可见（CDO 和 Instance）；
   - UPROPERTY( VisibleDefaultsOnly )：只在 CDO 中可见；
   - UPROPERTY( VisibleInstanceOnly )：只有丢进场景中时（生成了实例），才在 Detail 面板中可见；

2. 引用对象的编辑：

   - UPROPERTY( EditInline )：在编辑器中可直接编辑数组、其它对象的属性等；

3. 蓝图读写特性：

   - UPROPERTY( BlueprintReadOnly )：蓝图只读；
   - UPROPERTY( BlueprintReadWrite )：蓝图读写；
   - 以上两个默认只能用于 Public 属性，否则编译报错，但标记 meta = ( AllowPrivateAccess = true )可以规避，类似 C++中的“友元”；

4. 元数据说明符 meta：

   - 格式类似这样：UPROPERTY( meta = ( ClampMin = “1”, ClampMax = “100” ) )；
   - 在蓝图中点击变量之后，底下的属性，都能应用到 meta 中
   - 例如 meta = ( InstanceEditable = true, ExposeOnSpawn=“true” )
     - 当使用了 ExposeOnSpawn 时，构造函数见下节；

5.

<br>

#### UFUNCTION( )：函数声明宏定义

---

1. 蓝图调用特性：

   - UFUNCTION( BlueprintCallable )：蓝图可执行；
   - UFUNCTION( BlueprintPure )：蓝图可执行的纯函数，记得带上 const；

2. 重写特性：

   - UFUNCTION( BlueprintImplementableEvent )：虚函数，重载只能实现为事件，不能实现为函数；
   - UFUNCTION( BlueprintNativeEvent )：虚函数，可以在蓝图中重写；
     - 必须在 void Test();之后再声明一个 void Test_Implementation();
     - C++中【必须要有】void Test_Implementation();的实现体；
     - 使用宏标记之后（声明给蓝图使用的虚函数），void Test();可以不加 virtual
     - 但如果纯 C++中也需要重写该函数的话，应该在 void Test_Implementation()前面也加上 virtual；

3. 返回值命名：

   - 将返回值设为 void；
   - 需要返回的数值通过引用参数传入；
   - .h 文件中在引用参数前加：UPARAM(DisplayName = “返回值命名”)；（.cpp 中不用加）
   - 在函数实现中对引用参数赋值，然后 return 空即可；

4. 纯虚函数：<https://www.brightdevelopers.com/unreal-engine-4-c-polymorphism-overview/>；

<br>

#### UENUM()：枚举声明宏定义

---

1. 括号中添加 BluprintType 才可被蓝图使用；
2. 建议使用 enum class 而不是 enum

   - enum class 使用中必须是 EType::EValue，而 enum 可以是 EValue，但会导致不同的 EType 中值冲突，不好；
   - enum class 有值，但不能隐式转换，比如 int a = EType::EValue 会编译报错，只能 int a = int( EType:Evalue )；
   - 综上，enum class 是真正的强类型，推荐使用；

3. UMETA( DisplayName = “别名" )：蓝图中显示别名；
4. UMETA( Tooltip = “描述" )：蓝图中的描述；

<br>

#### USTRUCT()：结构体声明宏定义

---

1. 括号中添加 BluprinntType 才可被蓝图使用；
2. 类定义的下一行必须是 GENERATED_USTRUCT_BODY()；
3. ；

<br>

#### UINTERFACE():接口生命宏定义

---

1. 文档：<https://docs.unrealengine.com/4.27/zh-CN/ProgrammingAndScripting/GameplayArchitecture/Interfaces/>；
2. 编辑器会生成一个 UXXX 接口和一个 IXXX 接口，前者只是用于反射，不改也不要用，后者才是接口函数声明之处；
3. 手打的话：

   - #include “UObject/Interface”
   - 定义一个 UXXX 类继承 UInterface（不需要 MYPROJECT_API）；
   - 定义一个 IXXX 类（要有 MYPROJECT_API）；
   - 以上 XXX 要相同；

4. 声明一个可在蓝图中实现的接口：

   - UXXX 类需要用 UINTERFACE(Blueprintable)修饰符；
   - IXXX 类中的函数不需要 virtual 修饰符；
   - IXXX 类中的函数需要用 UFUNCTION 修饰符：
     - BlueprintCallable：可被蓝图调用（似乎接口不允许函数标记为 BlueprintPure）；
     - BlueprintImplementableEvent：只能在蓝图中实现；
     - BlueprintNativeEvent：能在 C++或者蓝图中实现；
     - 注意，接口函数不能声明为 BlueprintPure；

5. 声明一个只需要 C++实现的接口：

   - UXXX 类不需要 UINTERFACE(Blueprintable)修饰符；
   - IXXX 类中的函数需要用 virtual 修饰符；
   - IXXX 类中的函数不需要 UFUNCTION 修饰符；

6. 接口实现：

   - 若接口定义的是 void Test()，那么其它类的实现应该是 virtual void Test_Implement() override，而不是 void Test()；
   - 根据 C++语法，.cpp 中要把 virtual 和 override 去掉；

7. 判断是否实现接口：USomeObject->Implements<ISomeInterface>()；
8. 直接转换接口（省去判断）：ISomeInterface\* interface = Cast<ISomeInterface>(USomeObject)；
9. 接口调用：interface->Execute_Test( this, 其它参数 )；注意 this 并不总是 this，而是原目标类型的指针；

<br>

#### 常用类型

---

1. TODO

<br>

#### 字符串

---

1. 文档：<https://docs.unrealengine.com/4.27/zh-CN/ProgrammingAndScripting/ProgrammingWithCPP/UnrealArchitecture/StringHandling/FString/>；
2. 补充枚举转字符串：StaticEnum<EType>()->GetNameStringByValue( (int64)enumvar )；

<br>

#### TArray / TMap

---

1. 参考：<https://blog.csdn.net/yangxuan0261/article/details/52078303>；
2. 文档：<https://docs.unrealengine.com/4.27/zh-CN/ProgrammingAndScripting/ProgrammingWithCPP/UnrealArchitecture/TMap/>；

<br>

#### FDataTable

---

1. 见代码（来不及记录就写好了）
2. 转换 Json 和 CSV 参考：<https://zhuanlan.zhihu.com/p/158714495>；

<br>

#### 类型操作

---

1. 类型转换：T\* o2 = Cast<T>( o1 )；
2. 获取对象的 UClass：UClass c = o->GetClass()；
3. 获取类的 UClass：UClass c = T::StaticClass()；
4. 判断对象是否属于某类：bool b = o->IsA<T>()；
5. 判断对象是否是某个类的子类：o->GetClass()->IsChildOf( c )；
6. UClass 转换为字符串：c->GetName()；

<br>

#### 智能指针

---

1. 参考：<https://blog.csdn.net/github_38111866/article/details/107712692>；
2. TSharedPtr：

   - 仿自 C++11 的 shared_ptr，拥有引用计数，计数为 0 时销毁；
   - 两个 TSharedPtr 互相赋值会导致循环引用，内存泄露；

3. TWeakPtr：

   - 仿自 C++11 的 weak_ptr，不能防止被垃圾回收，若引用的对象在其他地方被销毁，则 TWeakObjectPtr 内部的指针自动将被置为 NULL，TWeakObjectPtr::IsValid()会返回 false；
   - 用于不影响 GC 的引用，比如说代理中指向对象的就是弱指针，对象有可能被 GC，会导致代理不生效，但不会崩；

<br>

#### 函数指针

---

1. int32 AAA()；
2. typedef int32 (_P)()；或者 using P = int32 (_) ()；
3. P = &AAA，P 就是函数指针；

<br>

#### Lamda 表达式

---

1. 无返回：auto LamdaFun = [v1,&v2,this] (p1,const &p2) {…}；
2. 有返回：auto LamdaFun = [v1,&v2,this] (p1,const &p2) -> 返回类型 {…; return ...}；
3. [v1,&v2,this]：捕获列表，分别表示值传递、引用传递、this 传递；
4. (p1,const &p2)：参数；
5. {}：函数体，默认不可以修改捕获列表的值，除非使用 mutable 标记

   - auto LamdaFun = [v1,&v2,this] (p1,const &p2) mutable -> 返回类型 {…; return ...}；

6. 执行：ret = LamdaFun(p1,p2)；
7.

<br>

#### 代理（Delegate）

---

1. 参考：<https://www.cnblogs.com/kekec/p/10678905.html>；
2. 文档：<https://docs.unrealengine.com/4.27/en-US/ProgrammingAndScripting/ProgrammingWithCPP/UnrealArchitecture/Delegates/Multicast/>；
3. 动态代理（DynamicDelegate）和非动态代理：

   - 动态代理可被蓝图使用
   - 动态单播代理可用于 UFUNCTION( BlueprintCallable )声明的函数中作为参数；
   - 动态多播代理声明时需要 UPROPERTY( BlueprintAssignable )修饰；
   - 动态多播代理不能带参数（Payload），也就是在【绑定】的时候，函数名后面不能跟附加参数列表；
   - 非动态代理只能用于 C++，宏定义时只需要参数类型不需要参数名
   - 动态多播代理就是蓝图中的 EventDispatcher；
   - 所以以下只记录动态代理，非动态代理把 DYNAMIC 去掉即可；

4. 单播代理宏定义：

   - DECLARE_DYNAMIC_DELEGATE( 代理名 )；
   - DECLARE_DYNAMIC_DELEGATE_OneParam( 代理名, 参数类型 参数名)；
   - DECLARE_DYNAMIC_DELEGATE_TwoParams( 代理名, 参数类型 参数名,参数类型 参数名)；
   - DECLARE_DYNAMIC_DELEGATE_RetVal( 返回类型, 代理名 )；
   - DECLARE_DYNAMIC_DELEGATE_RetVal_OneParam( 返回类型, 代理名, 参数类型 参数名)；
   - DECLARE_DYNAMIC_DELEGATE_RetVal_TwoParams( 返回类型, 代理名, 参数类型 参数名, 参数类型 参数名)；
   - 注意：代理名请用 F 开头；

5. 多播代理宏定义：

   - DECLARE_DYNAMIC_MULTICAST_DELEGATE( 代理名 )；
   - DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam( 代理名, 参数类型 参数名)；
   - DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams( 代理名, 参数类型 参数名, 参数类型 参数名 )；
   - 注意：多播代理没有返回值，可以绑定多次，但执行是无序的；

6. 代理定义：

   - 代理名 代理对象；

7. 单播代理绑定：

   - 绑定 UObject 派生类对象的函数（注意“&”）：代理对象.BindDynamic( 对象, &类::函数 )；
   - 绑定通过 UFUNCTION 标注的函数：代理对象.BindDynamicUFunction( 对象, STATIC_FUNCTION_FNAME(TEXT("类::函数")) )；
   - 绑定匿名函数（Lamda 表达式）：代理对象.BindDynamicLamda( [](){} )；
   - 绑定原生 C++类对象方法（注意“&”）：代理对象.BindDynamicRaw( &对象, &类::函数 )；
   - 绑定静态函数：代理对象.BindDynamicStatic( 类::函数 )；
   - 绑定智能指针对象方法：
     - 代理对象.BindDynamicSP()；
     - 代理对象.BindDynamicThreadSafeSP()；
   - TODO:

8. 多播代理绑定：把上述单播绑定函数中的【Bind】改成【Add】；
9. 绑定判断：

   - 单播：
     - 代理对象.IsBound()；
   - 多播：
     - 代理对象.IsBound()；存在一个以上即为 True；
     - 代理对象.IsBoundToObject( 对象 )；
     - 代理对象.IsAlreadyBound( 对象, &类::函数 )；（注意“&”）

10. 解除绑定：

    - 单播：
      - 代理对象.UnBound()；
    - 多播：
      - 解除所有代理：代理对象.Clear()；
      - 解除某个 UObject 上的所有代理：代理对象.RemoveAll( 对象 )；
      - 解除单个代理：
        - 代理对象.Remove( FDelegateHandle )；需要在 Add 的时候保存返回的 FDelegateHandle 对象；
        - 代理对象.Remove( 对象, STATIC_FUNCTION_FNAME(TEXT("类::函数")) )；
        - 代理对象.RemoveDynamic( 对象, &类::函数 )；（注意“&”）

11. 广播

    - 单播广播：
      - 有参代理：代理对象.Execute( 参数… )；
      - 无参代理：代理对象.ExecuteIfBound()；
    - 多播广播：多播代理对象.BroadCast(参数…)；

<br>

#### 事件（Event）

---

1. 文档：<https://docs.unrealengine.com/4.27/en-US/ProgrammingAndScripting/ProgrammingWithCPP/UnrealArchitecture/Delegates/Events/>；
2. 本质上就是动态多播；
3. 多播代理和事件的区别：

   - 论坛：<https://answers.unrealengine.com/questions/533323/what-difference-between-delegates.html>；
   - While any class can bind events, only the class that declares the event may invoke the event’s Broadcast, IsBound, and Clear functions. This means event objects can be exposed in a public interface without worrying about giving external classes access to these sensitive functions.
   - If you don't need more than one class to do the broadcast, use an event.

4. 就是说，事件声明放在哪个类里，哪个类就能对代理对象进行操作：
5. 事件宏定义：

   - DECLARE_EVENT( 类名, 代理名 )；
   - DECLARE_EVENT_OneParam( 类名, 代理名, 参数类型, 参数名)；
   - DECLARE_EVENT_TwoParams( 类名, 代理名, 参数类型, 参数名, 参数类型, 参数名)；
   - 注意：多播代理没有返回值，可以绑定多次，但执行是无序的；

<br>

#### 子系统（USubsystem）

---

1. 是一个 4.24 的新特性：<https://zhuanlan.zhihu.com/p/158717151>；
2. 继承自 UObject；
3. Subsystem 是 GamePlay 级别的 Component，类似于 Actor 中的 Component；
4. 应用场景：仅当需要全局管理器的时候应该使用，本质上是判断数据是否全局性；
5. 有什么好处：

   - 自动单例；
   - 自动生命周期；
   - 自动蓝图提示；
   - 逻辑分层；

6. 有 5 种 Subsystem：

   - 引擎子系统（UEngineSubsystem）：；
   - 编辑器子系统（UEditorSubsystem）：；
   - 游戏实例子系统（UGameInstanceSubsystem）：；
   - 世界场景子系统（UWorldSubsystem）：；
   - （ULocalPlayerSubsystem）：；

7. 区别于蓝图类中的函数，Subsystem 是有状态的；
8. 灵魂拷问：全局的游戏逻辑功能应该写在哪？

   - 答案一：写在扩展的 MyGameInstance/GameMode/PlayerController 中
     - 为了解决粒度过大冗余过多的问题，通常会写一些 ManagerActor，在需要的时候拖入场景中生成实例并使用；
   - 答案二：写在 Subsystem 中；
     - 多 Subsystem 的方式天然能解决冗余过多的问题；
     - 如果想要在蓝图中使用，可以用 C++写个基类，然后在蓝图中继承它，类似这样：
       - UCLASS(abstract, Blueprintable, BlueprintType)
       - class HELLO_API UMyGameInstanceSubsystemBase : public UGameInstanceSubsystem { }

9. 如何用好 Subsystem：

   - 理解 5 类 Outer 的生命周期（因为 5 种 Subsystem 依附于 5 种 Outer 生存）：
     - UEngine：代表引擎，数量 1，进程启动创建，进程退出销毁；
     - UEditorEngine：代表编辑器，数量 1，编辑器启动创建，编辑器退出销毁；
     - UGameInstance：代表一场游戏，数量 1，游戏启动创建，游戏结束销毁；
     - UWorld：代表一个世界，数量可能>1，生命周期同 GameMode；
     - ULocalPlayer：代表本地玩家，数量可能>1，生命周期可以等同于 GameInstance（一开始就创建好所有玩家），也可以是在游戏中动态增减的；
   - 重写 Initialize、DeInitialize（并理解它们被调用的声明周期）；
   - Subsystem 也可以定义自己的基类；
   - 可以学习官方的 Subsystem；

10. 生命周期：

    - ShouldCreateSubsystem(UObject\* Outer)：是否可以创建子系统；
    - Initialize(FSystemCollectionBase& Collection)：初始化；
    - Deinitialize()：结束；

<br>

#### 自定义模块

---

1. 文档：<https://docs.unrealengine.com/4.27/zh-CN/ProgrammingAndScripting/GameplayArchitecture/Gameplay/>；
2. 模块只能包含代码，不包含有内容资源
3. 新建模块：

   - 在 Source 中新建模块名目录；
   - 添加：Public/[模块名].h，声明 class F 模块名 : public IModuleInterface；
   - 添加：Private/[模块名].cpp，实现 F 模块名，主要是 void StartupModule()和 void ShutdownModule()；
   - 添加：[模块名].Build.cs；
     - 指定依赖库，PublicDependency 和 PrivateDependency 的区别是；
     - 指定 Public 路径和 Private 路径；
   - TODO:

4. 使用模块：

   - 整个模块复制到 Source/；
   - 修改[工程].uproject，依葫芦画瓢加个模块名（注意语法，逗号不要多也不要少）；
   - 修改[工程].Target.cs，在 ExtraModuleName 中加个模块名（注意语法，逗号不要多也不要少）；
   - 修改[工程]Editor.Target.cs，在 ExtraModuleName 中加个模块名（注意语法，逗号不要多也不要少）；
   - 在 IDE 中编译通过；
   - 在 Unreal 编辑器中刷新 vs 工程文件；
   - 重启 Unreal；

<br>

#### 自定义插件

---

1. 插件可以包含模块（Module）、内容资源（Content）、配置（Config）
2. 文档：<https://docs.unrealengine.com/4.27/zh-CN/ProductionPipelines/Plugins/#plugindescriptorfiles>；
3. 新建插件：

   - 在 Unreal 编辑器内新建插件，会在 Plugins 目录之下新建插件名目录；
     - 也可手动在 Plugins 下新建插件名目录；
   - 自动生成或手动添加：[插件名].uplugin 文件，描述文件见文档；
   - 自动生成或手动添加：Public/[模块名].h，作用与上述模块相同；
   - 自动生成或手动添加：Private/[模块名].cpp，作用与上述模块相同；
   - 会比模块多一个：Public/[模块名]BPLibarary.h 和 Private/[模块名]BPLibarary.cpp，以支撑 BP 系统；
   - 随便新建一个 C++类，系统会自动生成[模块名].Build.cs，
     - 也可手动在插件根目录添加，但系统生成的格式会完整一些；
     - 跟模块一样，这里得自己指定依赖库，指定 Public 和 Private 路径，才能编译通过；

4. 使用插件：

   - .uplugin 文件会自动包含类似模块在.uproject 中的声明，所以使用插件并不需要修改工程文件；

<br>

#### Runtime.CoreUObject.Public.UObject.Object 解析

---

1. 参考：<https://zhuanlan.zhihu.com/p/22813908>；
2. 反射：

   - 反射是指：用代码声明定义出来的类型，在编译时会用一种数据结构描述，并在运行时再得到；
   - 反射是最终表象，运行时类型系统（RTTI）才是底层支撑；
   - C++自带的类型系统能力只有：
     - typeid：获得类型信息 type_info
     - dynamic_cast：判断看基类指针是否指向子类对象；
   - 必须通过宏、模板、编译等手段实现反射，其实就是在函数和成员变量之上封装一层；
   - Unreal 使用 UHT（UnrealHeaderTool）方案，在编码时要求在 class、function、menber、enum 等结构前添加一些宏标记（类似 C#的 Attribute 语法），在编译时分析源码宏标记并生成代码，由 A.h 生成 A.generated.h，再修改 A.h，将 A.generated.h 包含进来。
   - 为了跨平台一致性，UHT 生成的依旧是代码而不是数据文件，由各平台自己编译成二进制；
   - Unreal 使用 UBT（UnrealBuildTool）进行编译配置管理，UBT 使用了 C#，UBT 的作用有两个：
     - 编译的时候寻找代码文件；
     - 运行的时候寻找链接文件；

3. 类型系统：

   - UObject 作为基类；
   - UObject 派生 UMetaData（用于 UPackage 映射）、UField（会聚合 UMeta）、UInterface（接口，本质也是 UClass）；
   - UField 派生 UProperty（字段定义）、UStruct（聚合类定义，会聚合 UProperty）、UEnum（枚举）；
   - UStruct 派生 UFunction（普通函数）、UClass（普通类）、UScriptStruct（普通结构）；

4. 创建：

   - 简要参考：<https://www.52vr.com//extDoc/ue4/CHN/Programming/UnrealArchitecture/Objects/Creation/index.html>；
   - 简要对比：<https://stackoverflow.com/questions/60021804/unreal-engine-4-different-ways-to-instantiate-the-object>；
   - 使用 NewObject<TClassType>()方式进行对象创建；
   - 使用 NewObject<TClassType>( UObject\* outer )方式进行对象创建并指定 Outer；
   - NewObject：
     - 注意这里不需要 GetWorld()
     - UMyObject\* object = NewObject<UMyObject>()；
     - UMyObject* object = NewObject<UMyObject>( UObject* outer )；
     - UMyObject* object = NewObject<UMyObject>( UObject* outer, FName name )；
     - 文档：<https://docs.unrealengine.com/4.27/zh-CN/ProgrammingAndScripting/ProgrammingWithCPP/UnrealArchitecture/Objects/Creation/>；
     - 论坛：<https://answers.unrealengine.com/questions/241656/constructobject-deprecated.html>；
   - NewObjectDeferred（不存在）：；
     - 当 UMyObject 中有属性声明了 ExposeOnSpawn 时，【并】【没】【有】类似 SpawnActorDeferred，所以还是在生成之后一个一个填吧；
   - 如果是在构造函数中创建对象，使用 CreateDefaultSubobject()；

5. GC：

   - 如果不想被 GC，使用 AddToRoot()进行规避，此指令会将对象添加到 GC 根，一般用于各种管理单例；
     - 如果想要释放，则使用 RemoveFromRoot()从 GC 根移除，然后就会自动回收；
     - 出于优秀习惯，应该将打算回收的对象指针置为 nullptr；
   - 如果想要强制回收，使用 object->MarkPendingKill()；然后将 object = nullptr；
   - 原生 C++类如果需要使用 Unreal 的 GC 系统，可以继承 FGCObject；

6. 序列化：从内存到磁盘教序列化，从磁盘到内存教反序列化；
7. CDO：在对象有很多时，只存储模板对象和成员变量非默认值，可以节省大量的存储空间和存取档的速度；
8. 生命周期：

   - Init() -> Shutdown()；
   - TODO:

<br>

#### Runtime.Engine.Classes.GameFramework.Actor 解析

---

1. 动态创建：SpawnActor：

   - AMyActor\* actor = GetWorld()->SpawnActor<AMyActor>( FVector location, FRotation rotation )；
   - AMyActor* actor = GetWorld()->SpawnActor<AMyActor>( FVector location, FRotation rotation, AActor* owner, APawn instigator, bool collision )；
   - 论坛：<https://docs.unrealengine.com/4.27/zh-CN/ProgrammingAndScripting/ProgrammingWithCPP/UnrealArchitecture/Actors/Spawning/>；

2. 动态创建时赋值：SpawnActorDeferred：；

   - 当 AMyActor 中有属性声明了 ExposeOnSpawn 时可以考虑使用此函数；
   - 论坛：<https://answers.unrealengine.com/questions/292074/spawn-actor-with-expose-on-spawn-variables-in-c.html>；
   - 论坛：<https://answers.unrealengine.com/questions/417150/spawn-actor-with-dynamic-data.html>；

3. 销毁：actor->Destroy()；

<br>

#### Runtime.Engine.Classes.Component.ActorComponent 解析

---

1. 特别提醒，不要用 UPROPERTY( VisibleAnywhere, BlueprintReadOnly, Category = XXX )声明组件；
2. 参考 1：

   - 参考：<https://dawnarc.com/2016/05/ue4constructor%E4%B8%8Eruntime%E4%B8%A4%E7%A7%8D%E7%8E%AF%E5%A2%83%E4%B8%8B%E5%88%9B%E5%BB%BAcomponent%E5%B9%B6attach%E7%9A%84%E6%96%B9%E5%BC%8F/>；
   - 参考：<https://blog.csdn.net/qq_29523119/article/details/84934202>；

3. 构造和附加：

   - ActorComponent 的几个子类：SceneComponent、MovementComponent、UIComponent 等；
   - 拥有场景信息的是 SceneComponent，需要附加；
   - 其它类型的 Component 以及纯 ActorComponent 是没有场景信息的，无需附加；
   - 在 Actor 构造函数实现定义和附加：
     - XXXComponent = CreateDefaultSubobject<UXXXComponent>( TEXT(“XXXComponent”) )；
     - XXXComponent->SetupAttachment( RootComponent, FAttachmentTransformRules::KeepRelativeTransfor )；（Actor.h 中已定义 RootComponent）
     - 文档：<https://docs.unrealengine.com/4.27/en-US/API/Runtime/Engine/Components/USceneComponent/SetupAttachment/>；
   - 在 Runtime 构造和附加：
     - XXXComponent = NewObject<UXXXComponent>( Object* owner, Actor* a )；
     - XXXComponent->RegistComponent()；
     - XXXComponent->AttachToComponent( a->GetRootComponent(), FAttachmentTransformRules::KeepRelativeTransform )；
     - 文档：<https://docs.unrealengine.com/4.27/en-US/API/Runtime/Engine/Components/USceneComponent/AttachToComponent/>；

4. 手动 GC：MyComponent->DestroyComponent()；MyComponent = nullptr；

<br>

#### Character 创建

---

1. USprintArmComponent：弹簧臂组件；
2. UCameraComponent：摄像机组件；
3. UCapsuleComponent：胶囊体组件；

   - 注意这是个 SceneComponent，需要 SetupAttachment / AttachToComponent；

4. USkeletalMesh：骨骼模型；
5. USkeletalMeshComponent：骨骼组件；
6. UPlayerInputComponent：输入响应组件；

   - 在 SetupPlayerInputComponent( UInputComponent\* )函数中绑定事件；
   - TODO:

7. UMovementComponent：运动组件；
