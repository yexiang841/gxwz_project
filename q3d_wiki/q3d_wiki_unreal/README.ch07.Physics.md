## 《Unreal 新手村日记》-07-物理模拟/碰撞（Physics/Collision）

<br>

#### 说明

---

- 项目文档记录了笔者工作积累的云原生微服务技术笔记，以及元宇宙、数字孪生、程序化建模工作学习笔记；
- 项目包含一套元宇宙建模方法论和工程，采用 QGIS -> Blender / Houdini -> Unreal / Unity 的方式实现数字孪生虚拟城市；
- 项目包含一个 Unreal 客户端工程和一个 Unity 客户端工程（建设中）；
- 项目包含一个 Cesium 网页端工程，实现 WebGIS BI 展示；
- 项目包含一个 Vue.js / ElementPlus / TS 网页端工程，实现 Admin 管理后台；
- 项目包含一个 Java / SpringBoot 微服务工程和一个 C# / DotNet 微服务工程（建设中），共用网关和服务注册中心（Nacos），可以协同工作；
- 项目包含一个 Publish 部署工程，采用 Docker / Kubernetes / Jenkins 方式进行服务部署；
- 程序语言涉及 C++ / Java / C# / Python / TS / Shell / VEX；
- 错漏之处恳请读者不吝指正，转载望注明出处：<https://gitee.com/yexiang841/q3d_project/>；
- Copyright 叶湘 ( weixin:yexiang841, email:yexiang841@qq.com ), Shitouren Studio. All Rights Reserved.

<br>

#### Unreal 工作学习笔记

---

- [《Unreal 新手村日记》-00-自学自研项目（Q3D）简介](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch00.Prologue.md)
- [《Unreal 新手村日记》-01-编辑器（Editor）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch01.Editor.md)
- [《Unreal 新手村日记》-02-蓝图（Blueprint）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch02.Blueprint.md)
- [《Unreal 新手村日记》-03-框架（GamePlay）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch03.GamePlay.md)
- [《Unreal 新手村日记》-04-材质（Material）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch04.Material.md)
- [《Unreal 新手村日记》-05-场景编辑（Landscape）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch05.Landscape.md)
- [《Unreal 新手村日记》-06-UI 界面（UMG）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch06.UMG.md)
- `>>>>` [《Unreal 新手村日记》-07-物理模拟/碰撞（Physics/Collision）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch07.Physics.md)
- [《Unreal 新手村日记》-08-骨骼和动画（Skeletal/Animation）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch08.Skeletal.md)
- [《Unreal 新手村日记》-09-粒子系统（ParticleSystem/Niagara）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch09.Niagara.md)
- [《Unreal 新手村日记》-10-C++](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch10.CPP.md)
- [《Unreal 新手村日记》-11-项目设置和打包（Project/Packing）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch11.Project.md)
- [《Unreal 新手村日记》-12-流体（Flip）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch12.Flip.md)
- [《Unreal 新手村日记》-13-毛发系统（Groom）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch13.Groom.md)
- [《Unreal 新手村日记》-14-混沌物理系统（Chaos）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch14.Chaos.md)
- [《Unreal 新手村日记》-15-AI](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch15.AI.md)
- [《Unreal 新手村日记》-16-UE5](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch16.UE5.md)
- [《Unreal 新手村日记》-17-技术美工和程序化建模（TA/PM）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch17.TA.md)

<br>

#### 可设置碰撞的物体

---

1. 静态网格体（StaticMesh）；
2. 骨骼体（SkeletalMesh）：因为开销较大，所以一般在需要时（比如精准打击）才加载，不需要时只使用简单的胶囊碰撞；
3. 地形（Landscape）；
4. 物理体积（PhysicsVolume）；

<br>

#### 碰撞的编辑器操作

---

1. 设置碰撞体，会产生阻挡效果和碰撞事件

   - 但是要开启物理模拟才会有力作用效果；
   - 碰撞事件放在包含它的 Actor 中处理；

2. 碰撞作用在静态网格体层面或者 Actor 的组件层面，在静态网格体编辑器中，从菜单栏【碰撞】添加各种类型碰撞；
3. 物体可以同时设置简单和复杂的碰撞：

   - 简单碰撞用于外部作用，如 Player 走动碰撞；
   - 复杂碰撞用于自体作用，如子弹射中 Player 身上的部位，例如可用于 LineTraceByChannel 中的 TraceComplex 端口；

4. 简单碰撞：

   - 圆形（Sphere）、胶囊体（Capsule）、盒碰撞体（Box）；
   - 钻石面碰撞体（DOP）：简化网格模型到指定面数，作为碰撞体；

5. 复杂碰撞：

   - 自动凸包碰撞：分析网格体凸面，自动生成复杂碰撞体；
   - 点自欧东凸包碰撞会打开凸包分解设置页，设定分解参数后应用，才会生成碰撞体；

6. 在菜单栏碰撞中可以一键删除已存在的碰撞体；
7. 碰撞复杂性（CollisionComplexity）：在详情 -> 碰撞 -> 碰撞复杂性中选择几种选项；

   - 简单碰撞和复杂碰撞分别设置；
   - 将简单碰撞用于复杂碰撞；
   - 将复杂碰撞用于简单碰撞；

<br>

#### 物理模拟（SimulatePhysics）

---

1. 打开物理模拟，才会有力作用效果；
2. 物理模拟是在静态网格层，Actor 如果有多个静态网格，可以为每个静态网格单独设置物理模拟；
3. 主要设置质量、摩擦力、角摩擦力等；

<br>

#### 碰撞通道（CollisionChannel）

---

1. 每个 Actor 都有一个属于自己的碰撞通道；
2. 静止 StaticMesh 归属于 WorldStatic；
3. 运动的球归属于 WorldDynamic；

<br>

#### 碰撞预设（CollisionPreset）

---

1. 参考：https://docs.unrealengine.com/4.26/zh-CN/InteractiveExperiences/Physics/Collision/Reference/
2. Ignore：忽略；
3. Block：阻挡，可用于产生 Hit 事件；
4. Overlap：可穿透，可重叠，用于虚拟碰撞，但如果不开启生成命中事件的话，事实上等于 Ignore；

<br>

#### 两个物体碰撞的条件

---

1. 互相对对方的碰撞通道设置了 Block；
2. 至少有一个开启了物理模拟（SimulatePhysics）；
3. 两者都要有碰撞盒子（对于 StaticMesh）或碰撞组件（对于 Actor）；

<br>

#### 事件产生的条件

---

1. TODO:
