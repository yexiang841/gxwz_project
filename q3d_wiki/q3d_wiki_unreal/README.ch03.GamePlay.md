## 《Unreal 新手村日记》-03-框架（GamePlay）

<br>

#### 说明

---

- 项目文档记录了笔者工作积累的云原生微服务技术笔记，以及元宇宙、数字孪生、程序化建模工作学习笔记；
- 项目包含一套元宇宙建模方法论和工程，采用 QGIS -> Blender / Houdini -> Unreal / Unity 的方式实现数字孪生虚拟城市；
- 项目包含一个 Unreal 客户端工程和一个 Unity 客户端工程（建设中）；
- 项目包含一个 Cesium 网页端工程，实现 WebGIS BI 展示；
- 项目包含一个 Vue.js / ElementPlus / TS 网页端工程，实现 Admin 管理后台；
- 项目包含一个 Java / SpringBoot 微服务工程和一个 C# / DotNet 微服务工程（建设中），共用网关和服务注册中心（Nacos），可以协同工作；
- 项目包含一个 Publish 部署工程，采用 Docker / Kubernetes / Jenkins 方式进行服务部署；
- 程序语言涉及 C++ / Java / C# / Python / TS / Shell / VEX；
- 错漏之处恳请读者不吝指正，转载望注明出处：<https://gitee.com/yexiang841/q3d_project/>；
- Copyright 叶湘 ( weixin:yexiang841, email:yexiang841@qq.com ), Shitouren Studio. All Rights Reserved.

<br>

#### Unreal 工作学习笔记

---

- [《Unreal 新手村日记》-00-自学自研项目（Q3D）简介](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch00.Prologue.md)
- [《Unreal 新手村日记》-01-编辑器（Editor）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch01.Editor.md)
- [《Unreal 新手村日记》-02-蓝图（Blueprint）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch02.Blueprint.md)
- `>>>>` [《Unreal 新手村日记》-03-框架（GamePlay）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch03.GamePlay.md)
- [《Unreal 新手村日记》-04-材质（Material）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch04.Material.md)
- [《Unreal 新手村日记》-05-场景编辑（Landscape）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch05.Landscape.md)
- [《Unreal 新手村日记》-06-UI 界面（UMG）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch06.UMG.md)
- [《Unreal 新手村日记》-07-物理模拟/碰撞（Physics/Collision）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch07.Physics.md)
- [《Unreal 新手村日记》-08-骨骼和动画（Skeletal/Animation）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch08.Skeletal.md)
- [《Unreal 新手村日记》-09-粒子系统（ParticleSystem/Niagara）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch09.Niagara.md)
- [《Unreal 新手村日记》-10-C++](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch10.CPP.md)
- [《Unreal 新手村日记》-11-项目设置和打包（Project/Packing）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch11.Project.md)
- [《Unreal 新手村日记》-12-流体（Flip）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch12.Flip.md)
- [《Unreal 新手村日记》-13-毛发系统（Groom）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch13.Groom.md)
- [《Unreal 新手村日记》-14-混沌物理系统（Chaos）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch14.Chaos.md)
- [《Unreal 新手村日记》-15-AI](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch15.AI.md)
- [《Unreal 新手村日记》-16-UE5](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch16.UE5.md)
- [《Unreal 新手村日记》-17-技术美工和程序化建模（TA/PM）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch17.TA.md)

<br>

#### GamePlay 架构

---

4. 万物皆 UObject，然后有 Actor，Actor 管理 Replication、Spawn、Tick

   - UObject 拥有基础的类注册，反射、序列化、GC 等机制；

5. 一图胜千言：<http://static.zybuluo.com/fjz13/c6ke7bxnu7ni2ktdpwhdgcnk/StructureEasy.jpg>；
6. 实体（Actor）

   - 继承自 UObject；
   - 基本属性：可放置于场景、具有移动性、可添加碰撞等；
   - 注意：Actor 不一定场景可见，如 AInfo 下派生的 AWorldSettings、AGameMode、APlayState 等；
   - 在 OOP 思想中，Actor 功能不宜过繁，所以用 Component 封装功能，聚合使用；
   - 一个 Actor 通常会拥有很多 Component，其中 SceneComponent 用作场景放置，常作为 RootComponent；

7. 组件（UActorComponent）

   - 继承自 UObject；
   - Component 就是 Actor 的 Details 面板中的各种参数定义和赋值；
   - USceneComponent 继承自 UActorComponent，增加了 Transfor 来表示其位置，所以常用于有形组件；

8. 世界（UWorld）

   - 都继承自 UObject；
   - UWorld 中除了 ULevel 数组，还会有几个指针：
     - PersistentLevel（入口关卡）；
     - CurrentLevel（当前关卡）；
     - StreamingLevels 数组（后续动态加载关卡）；
   - UWorld 中聚合 APlayerController 数组和 APawn 数组，也就是说游戏中的各种 Actor、Pawn 和 Controller 本体不在关卡（Level）中，而是在世界中，只是用 Level 进行批量管理而已；
   - UWorld 有几种类型：
     - 比如编辑器（PIE）和游戏预览（Preview），都是 World；
     - 不同的 UWorld 以 FWorldContext 进行上下文切换；
     - FWorldContext 是在 GameInstance 中管理的；

9. 关卡（ULevel）

   - 继承自 UObject；
   - 有时候 World 太大，装不下所有的 Actor，所以拆分成关卡（Level）进行管理，多关卡组合成 World 可以有两种方式：
     - SubLevel；
     - WorldComposition：拼接世界地图可以用这种方式；
   - Level 管理光照、物理等属性；
   - Level 也存储各种 Actor，包括显示的和不显示的（如 AWorldSettings）；
   - Level 还有一个特化的聚合：关卡蓝图（ALevelScriptActor），这也是个 Actor；
     - 基本操作：
       - 从蓝图 -> 关卡蓝图中打开；
       - 通常从 EventBeginPlay 开始（快捷键 P）；
       - 关卡蓝图中引用实例：先选中实例，然后在关卡蓝图中右键，CreateAReferenceToXXX；
     - 哪些逻辑适合写在这里：
       - 场景生成；
       - 跟场景有关的表现层逻辑，比如区域重力变化，光照等；
       - 本关卡过场动画、音乐控制、粒子系统的激活等；
   - 关卡的迁移（Travel）：
     - 参考：<https://zhuanlan.zhihu.com/p/36731312>；
     - 简单的迁移：使用打开关卡（）节点即可；
     - TODO:

10. 角色（Pawn）

    - 继承自 Actor，扩展出 Character（人形的 Pawn），在逻辑上定义为需要与玩家互动的 Actor；
    - 互动包括：可被 Controller 控制、可进行物理表示（如碰撞）、可响应输入进行变换；
    - 会引用一个 PlayerState 和一个 Controller；
    - 注意：Actor 级别已经能够响应诸如 WASD 等输入，但是到了 Pawn 层才会定义他们是用来前进后退的逻辑；
    - Pawn 的三个重要子类：
      - DefaultPawn：想自己开发 Pawn，就从这里开始，它带上了 Movement、Collision、StaticMesh 三大 Component；
      - SpectatorPawn：扩展自 DefaultPawn，就加了个无重力漫游 Component：USpectatorPawnMovement；
      - Charactor：人形角色，主要就是带上了人形骨骼（SkeletalMesh）；
    - 绑定控制器步骤：
      - 首先要添加蓝图模板：添加功能或内容包（AddNew） -> 蓝图功能 -> 第三人称游戏/第一人称游戏；
      - 绑定控制器方法一（拖入式）：
        - 从内容（Content） -> ThirdPersonBP/FirstPersonBP -> Blueprints 中获取人物拖入场景；
        - 选中场景中的角色；
        - 右侧细节面板（Details） -> ThirdPersionCharacter -> Pawn -> 自动玩家控制（AutoPossessPlayer），设置为玩家 0；
      - 绑定控制器方法二（出生点）：
        - 游戏模式设定（前提必须添加蓝图模板）：右侧世界场景设置面板（WorldSetting） -> GameModeOverride 选择蓝图模板 ThirdPersonGameMode/FirstPersonGameMode；
        - 放置一个出生点到场景中；
        - 播放之后，就会从出生点开始控制小人；
      - 以上两种方法是冲突的，方法一会覆盖方法二；
      - 方法二中，GameModeOverride 也可以是自定义，然后紧接着在选中的游戏模式（SelectedGameMode）中定义详细的 Pawn；

11. 控制器（AController）

    - 继承自 Actor，为了控制 Pawn，需要许多与 Actor 中的能力，重点增加了操纵 Pawn 的能力（Possess）；
    - 会引用一个 PlayerState 和一个 Pawn/Charactor；
    - 为了工程上的简约，AController 只实现一对一的 Pawn 操作，所以在面对多 Pawn 时需要显式切换 Possess；
      - 因此在研发即时战略游戏（RTS）时需要自己实现 Controller；
    - 尽量让 Pawn 只接受指令去完成，让 Controller 统筹管理逻辑；
      - 比如：如果 Pawn 具有“重生”能力，那么重生之后的 Pawn 应该具有哪些以前的数据，应该由 Controller 来管理；

12. 玩家控制器（APlayerController）和 AI 控制器（AAIController）

    - 继承自 AController；
    - 只存在于客户端，从 PlayerState 数据中生成和重建；
    - APlayerController 主要是增加了 UPlayer 的关联特性，多了诸如摄像机管理、输入逻辑、UMG 显示、声音、动画等控制接口，如：
      - ConvertMouseLocationToWorldSpace：鼠标的屏幕坐标；
      - EnableInput / DisableInput：允许/关闭键盘输入；
      - ShowMouseCursor：允许/关闭鼠标显示；
      - SetInputMode：当前输入模式；
    - 用节点 GetPlayerController（全局函数）可获取当前的玩家控制器；
    - AAIController 则没有摄像机等，但会有导航、行为树等 Component；
    - 哪些逻辑适合写在这里：跟角色行为有关的逻辑；

13. 游戏实例（UGameInstance）

    - 继承自 UObject；
    - GameInstance 从逻辑上比 World 更高一级，相当于进程；
    - 保存这一个重要的上下文（WorldContext），很多时候需要从这里取；
    - 主要职责是：
      - 引擎初始化；
      - 管理玩家；
      - GameMode 的重载修改；
      - 建立联网机制；
    - 哪些逻辑适合在这里：
      - 加载 World，切换 Level；
      - 动态增删 Players；
      - 全局配置，ConsoleCommand 等；
    - 如果继承并自定义了 GameInstance，将 ProjectSettings 中的 GameInstance 配置改为自定义的 GameInstance；

14. 游戏模式（AGameMode）

    - 继承自 AInfo；
    - 可理解为 WorldController，所以只应存在于服务端，从 GameState 数据中生成和重建；
    - GameMode 应该是跨关卡的存在，通常没有太多的数据变化，也没有客户端需要了解的瞬态数据；
    - GameMode 只应该存在于 Server 中（单机游戏也是 Server）；
    - GameMode 应该是一套玩法逻辑，而 GameInstance 则有可能对 GameMode 进行切换，比如说大游戏中的分支小游戏；
    - 哪些逻辑适合在这里：
      - 每个关卡都需要的功能，比如创建 AIRobot；
      - 处理游戏开始、暂停、结束等进度逻辑；
      - 播放游戏开场动画和处理 Level 载入；
      - 多人游戏的同步；
    - 如果继承并自定义了 GameMode，将 WordSettings 中 GameMode 配置改为自定义的 GameMode；
    - GameMode 和 GameState 是多人联机的默认实现（实现了一些 Match 的功能），如果想要简单的基类，应该从 GameModeBase 何 GameStateBase 继承，然后自己用 MatchState 去实现 Match；

15. 游戏状态（AGameState）

    - 继承自 AInfo；
    - 应在服务端和客户端保持同步；
    - 用于存储和恢复整个游戏除了角色之外的数据，默认拥有一个 PlayerArray 数组；
    - 那些逻辑适合在这里：让所有玩家都知道的信息；

16. 玩家（UPlayer）

    - 继承自 UObject；
    - UPlayer 原生存储在 GameInstance 中；
    - 不是具体的角色，而是一个“操控者”，一个 ID，即使是一个发送指令的网络连接，也可以是 UPlayer；
    - 一个 UPlayer 通过 PlayerController 和 PlayerState 来控制角色（Pawn）
    - UPlayer 的两个重要子类是：本地玩家（ULocalPlayer）和网络连接（UNetConnection）；
      - ULocalPlayer 比 UPlayer 多出了 ViewPort 相关功能；

17. 玩家状态（APlayerState）

    - 继承自 AInfo；
    - 应在服务端和客户端保持同步；
    - APlayerState 跟 Pawn、Controller 平级，都是存储在 World 中，由 Level 管理；
    - APlayerState 只为 UPlayer 存在（对应），而不为 AI 生成，但是既然从 Controller 中独立出来，就是为了给 AI 读取用；
    - 应用场景：当网络掉线时，服务端的 Controller 代理可能会被释放，服务器就只存 PlayerState，以便上线时恢复；

18. 游戏存档（USaveGame）

    - 继承自 UObject；
    - 使用 UObject 的序列化能力进行文件存储；

<br>

#### 全局配置

---

1. ProjectSetting：

   - 配置 GameInstance；
   - 配置编辑器初始地图（EditorStartupMap）和游戏初始地图（GameDefaultMap）；

2. WorldSetting：

   - 配置 GameMode
   - 配置 DefaultPawnClass - 在出生点默认生产的 Pawn；
   - 配置 HudClass - 待研究；
   - 配置 PlayerControllerClass - 自定义玩家控制器；
   - 配置 GameStatClass - 自定义游戏状态存储；
   - 配置 PlayerStateClass - 自定义玩家状态存储；
   - 配置 SpectatorClass - 自定义观众模式的 Pawn；

3. 外部配置文件：

   - 教程：YouTuber-TheLifeofJevins：<https://www.youtube.com/watch?v=QvN8rNCjDko>；
   - 日志配置：<https://unreal.gg-labs.com/wiki-archives/common-pitfalls/logs-printing-messages-to-yourself-during-runtime>；

4. Pawn 配置

   - AutoPossessAI 用于在 Pawn 生成时自动新增一个 AIController；
     - AIControllerClass 用于指定 AIController 的子类；
   - AutoPossessPlayer 用于设置 Pawn 被某个 Player（的 PlayerController）自动 Possess；
   - 注意当以上两者都设定时，Pawn 会先由 AIController 控制（Possess），然后会 Unpossess，再转交给 PlayerController 控制，所以：
     - 在第一次 Possess 事件中，可以保存一下 AIController 的引用；
     - 在 Unpossess 的事件中，可以手动让之前保存的 AIController 继续接管该 Pawn；
     - 否则这个 Pawn 就会是无控制状态；

<br>

#### 生命周期

---

1. GamePlay

   - GameMode->BeginPlay
   - GameState->BeginPlay
   - PlayerController->BeginPlay
   - TODO:

2. Actor 和 Component

   - Actor->ConstructScript（放置、出生，初始化）
   - ActorComponent->EventBeginPlay
   - Actor->EventBeginPlay
   - WidgetComponent->EventConstruct；

3. 子类的 Event 会覆盖基类的 Event，需要在子类 Event 上手动 CallParentEvent；

<br>

#### GamePlay 初始化逻辑

---

1. GameInstance 创建 LocalPlayer(Player)，再创建出相对应的 PlayerController，Player 和 PlayerController 互相引用；
2. PlayerController 创建过程中会创建一个 PlayerState；
