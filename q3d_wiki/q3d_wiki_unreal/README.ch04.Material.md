## 《Unreal 新手村日记》-04-材质（Material）

<br>

#### 说明

---

- 项目文档记录了笔者工作积累的云原生微服务技术笔记，以及元宇宙、数字孪生、程序化建模工作学习笔记；
- 项目包含一套元宇宙建模方法论和工程，采用 QGIS -> Blender / Houdini -> Unreal / Unity 的方式实现数字孪生虚拟城市；
- 项目包含一个 Unreal 客户端工程和一个 Unity 客户端工程（建设中）；
- 项目包含一个 Cesium 网页端工程，实现 WebGIS BI 展示；
- 项目包含一个 Vue.js / ElementPlus / TS 网页端工程，实现 Admin 管理后台；
- 项目包含一个 Java / SpringBoot 微服务工程和一个 C# / DotNet 微服务工程（建设中），共用网关和服务注册中心（Nacos），可以协同工作；
- 项目包含一个 Publish 部署工程，采用 Docker / Kubernetes / Jenkins 方式进行服务部署；
- 程序语言涉及 C++ / Java / C# / Python / TS / Shell / VEX；
- 错漏之处恳请读者不吝指正，转载望注明出处：<https://gitee.com/yexiang841/q3d_project/>；
- Copyright 叶湘 ( weixin:yexiang841, email:yexiang841@qq.com ), Shitouren Studio. All Rights Reserved.

<br>

#### Unreal 工作学习笔记

---

- [《Unreal 新手村日记》-00-自学自研项目（Q3D）简介](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch00.Prologue.md)
- [《Unreal 新手村日记》-01-编辑器（Editor）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch01.Editor.md)
- [《Unreal 新手村日记》-02-蓝图（Blueprint）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch02.Blueprint.md)
- [《Unreal 新手村日记》-03-框架（GamePlay）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch03.GamePlay.md)
- `>>>>` [《Unreal 新手村日记》-04-材质（Material）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch04.Material.md)
- [《Unreal 新手村日记》-05-场景编辑（Landscape）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch05.Landscape.md)
- [《Unreal 新手村日记》-06-UI 界面（UMG）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch06.UMG.md)
- [《Unreal 新手村日记》-07-物理模拟/碰撞（Physics/Collision）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch07.Physics.md)
- [《Unreal 新手村日记》-08-骨骼和动画（Skeletal/Animation）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch08.Skeletal.md)
- [《Unreal 新手村日记》-09-粒子系统（ParticleSystem/Niagara）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch09.Niagara.md)
- [《Unreal 新手村日记》-10-C++](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch10.CPP.md)
- [《Unreal 新手村日记》-11-项目设置和打包（Project/Packing）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch11.Project.md)
- [《Unreal 新手村日记》-12-流体（Flip）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch12.Flip.md)
- [《Unreal 新手村日记》-13-毛发系统（Groom）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch13.Groom.md)
- [《Unreal 新手村日记》-14-混沌物理系统（Chaos）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch14.Chaos.md)
- [《Unreal 新手村日记》-15-AI](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch15.AI.md)
- [《Unreal 新手村日记》-16-UE5](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch16.UE5.md)
- [《Unreal 新手村日记》-17-技术美工和程序化建模（TA/PM）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch17.TA.md)

<br>

#### 材质基本操作

---

1. 通常新建 Material 目录存储材质；
2. 材质类通常以 M_XXX 命名，材质实例通常以 MI_XXX 命名，材质函数以 MF_XXX 命名；
3. 外部材质贴图导入 Unreal 之后会存储为 Unreal 专用的 uasset 格式，相对于 jpg 会增加很多的存储空间，需要规划导入量；

<br>

#### 材质参数（MaterialParam）和材质实例（MaterialInstance）

---

1. 做材质时可将变量提升为参数，用来接收外部数据；
2. 常量右键可转换为参数（Param）[S]：然后就能从外部引入参数；
3. 直接搜索 Parameter 可以找出各种类型的参数节点；
4. 参数建议设定默认值；
5. Unreal 会自动处理参数关联性，比如当 Switch 的数值和布尔值都是参数时，会自动处理成“布尔值打钩之后才显示数值”；
6. 材质参数的第一种用法：蓝图传参给材质，调用 StaticMesh 的 SetXXXParameterValueOnMaterial；
7. 材质参数的第二种用法：在材质上右键创建材质实例，材质实例也可以赋予 Mesh，细节面板中可以给材质参数赋值；

<br>

#### 材质参数集（MaterialParamCollection）

---

1. 材质参数实体，跟材质实例一样，数值可用于材质，不同的是它还可以被蓝图、序列修改，可用于做动画；
2. 相当于全局结构体变量；

<br>

#### 材质输出节点

---

1. 混合模型（BlendMode）最重要的选项：

   - 不透明（Opaque）：用于大多数不透明物体；
     - 也包括次表面（Subsurface）：类似于皮肤、蜡烛等模型（其它引擎常称为 SSS），需要将着色模型（ShadingMode）设定为次表面（Subsurface），次表面通常要设定小于 1 的 Opaque 值，然后赋予表面颜色和次表面颜色两种颜色；；
   - 半透明（Translucent）：用于玻璃等物体；
     - 此模式默认没有金属度、高光、粗糙度的，可以在半透明选项（Translucency）中调整光照模式（LightingMode）为表面透明（SurfaceTranslucent）恢复出来，但此时计算量会很大；
   - 叠加（Additive）：把自己变成蒙板，可以透过自己看到后面背后的物体，可以用来实现一些科幻效果；
   - 遮罩（Masked）：会多出一个 OpacityMask 选项，用 0 和 1 来决定是否显示某个部分；

2. 自发光颜色可以大于 1，1 到 10 之间比较常用；
3. 用黑白颜色材质贴图连到透明度上，可以实现纹理雕刻；
4. 法线贴图（NormalMap），可以对光照进行重算，从而实现凹凸效果，模型本质上没有变化，仅计算光照效果，降低模型面数等于降低了计算量；
5. 曲面细分乘数（Tesellation）打开之后，就可以加上乘数接口，从而对表面的点进行更细的操作（同时增加负载）；

<br>

#### 贴图采样（TextureSample）

---

1. 常用的贴图采样节点：TODO；

<br>

#### 材质函数（MaterailFunction）

---

1. 材质函数可以有多个输出，类似于蓝图中的宏；
2. 输入和输出的接口顺序是通过优先级显示定义的，0 在前；
3. 常用材质函数：

   - TODO:

4. 参数命名约定：

   - s\_：表示 Scalar，1 维；
   - t2d\_：表示 Texture；
   - b\_：表示 StaticBool；
   - v2*/v3*/v4\_：；

<br>

#### 数值运算

---

1. 数值常量（Constant）[1]：一维浮点数值，可右键转化为参数；
2. 数值加法（Add）[A]：可用于数值或多维矩阵运算，黑加啥是啥，白加啥都白；
3. 数值乘法（Multiply）[M]：可用于数值或多维矩阵运算

   - 白乘啥是啥，黑乘啥都黑；
   - 颜色向量相乘相当于颜色叠加（正片叠底），很常用，比如加颜色、加划痕、加污渍等；

4. 数值除法（Divide）[D]：

   - 被除数是 0 的话会自动当做 1；

5. 1 内反转（OneMinus）[O]：1-x；
6. 绝对值（Abs）；
7. 取模（Fmod）；
8. 乘方（Power）[E]；
9. 取小数（Frac）：从时间节点中取小数，可以让时间映射到 0~1 之间（循环），比较常用；
10. 向上/下取整（Ceil/Floor），注意负数情况下，是向坐标正负方向取整，而不是远离/接近 0；
11. 插值运算（Lerp）[L]：以 0 到 1 之间的值为比例混合 A 和 B 通道；
12. 数值约束（Clamp），把阈值之外的变为最大最小值，阈值之内的不变（区别于映射）；

<br>

#### 分量运算

---

1. 多维分量常数（ConstantVector）[2/3/4]：2 维用于 uv，3 维用于向量/颜色，4 维加透明度，可右键转化为参数；
2. 分量拆分（BreakOut2/3/4Component）可以获得多维数的分量；
3. 遮罩（ComponentMask）跟分量拆分差不多，但是干净一点；
4. 分量组合（MakeFloat2/3/4）可以合并分量为多维数；
5. 分量追加（Append）从低维拼成高维；

<br>

#### 向量（3 维分量）运算

---

1. 向量点乘（Dot）：a(->) . b(->) = ||a(->)|| _ ||b(->)|| _ cosθ，夹角越小，乘数越大，角度一致时 cosθ=1；
2. 向量叉乘（Cross）：结果是得到两个向量构成的平面的法线，符合右手螺旋法则，法线向量长度为：||a x b|| = ||a|| _ ||b|| _ sinθ；
3. 镜头向量（CameraVector）镜头指向的方向，可以用来做一些镜头跟随计算

   - 比如说用 CameraVector 和 VertexNormalWS 点乘，可制作菲涅尔效应；

4. 顶点法向量（VertexNormalWS）

   - 物体有多少个顶点，就有多少个法向量；
   - 法线向量长度定义为 1；

5. 像素法向量（PixelNormalWS）

   - 物体在着色时用多少个像素，就有多少个法向量，通常比顶点数多得多（负载也会大）；
   - 当物体顶点数远小于材质分辨率时，如果要对材质配合法向量运算，可以用这个；

<br>

#### 布尔运算

---

1. 布尔常数（StaticBool）：布尔值，可右键转化为参数;
2. 判断（if）[I]，二值开关，根据条件取不同源输出；
3. 开关（Switch）相当于多远运算符，根据条件取值；

<br>

#### UV 运算

---

1. 因为不同 3D 软件用的 XYZ 坐标系不同，所以用 UV 来表示会更合理，在平面中，U 表示横向，V 表示纵向；
2. 材质坐标（TexCoord）[U]节点：贴图采样（TextureSample）节点的 UVs 接口最常用的输入；
3. UV 平移：对材质坐标（TexCoord）用 Add 节点与二维向量做加法，二维向量可以用 Append 组合 X 和 Y；
4. UV 缩放：

   - 对材质坐标（TexCoord）用 Multiply 节点与二维向量做乘法，二维向量可以用 Append 组合 X 和 Y；
   - 直接调材质坐标（TexCoord）的参数对 UV 进行拉伸；
   - 数值越大密度越大；

5. UV 平移（Panner）[P]：作用于材质坐标（TexCoord），随时间平移 UV；
6. UV 旋转（Rotator）：作用于材质坐标（TexCoord），随时间旋转 UV；
7. UV 贴图：在模型制作时通常会做 UV 贴图，在 Unreal 的材质中，如果想要对模型不同部位使用不同材质，可使用混合材质属性节点（BlendMaterialAttributes），用 UV 贴图（黑白图）做通道，用不同的材质输出节点做 AB 输出；
8. UV 贴图的制作：

   - 在模型创作时，美术通常会分面创作材质，然后整体烘焙生成模型的 UV 贴图；
   - 对真实物体进行材质扫描时，烘焙生成的 UV 贴图，也是基于特定模型；

9. UV 修复：

   - 普通纹理材质的 UV 贴图，都是从以上方法获得，所以用于其它模型时，常常会出现拉伸、模糊等问题；
   - 为了材质能被别的模型套用，就不得不进行 UV 修复，用算法模拟（负载）实现通用性（便捷）；
   - UV 修复方案一：三向贴图（Triplanar），适合解决峭壁渲染的问题，但弯曲面会有一定程度的材质混合；

10. 三向贴图的实现：

    - 贴图采样（TextureSample）节点的 UVs 的输入不使用 TexCoord，而使用绝对世界坐标（AbsoluteWorldPosition）的其中两个分量；
    - 只取 XY 轴，则 Z 方向上会拉伸，其它面同理；
    - 将世界坐标适当压缩（乘除法）之后，分别选择三个面，连入贴图采样节点；
    - 将三个方向的贴图采样节点按照法线长度（的平方）进行混合；

<br>

#### 法线运算

---

1. 纯蓝色（0,0,1）可以冒充没有起伏的法线贴图，可以用来和法线贴图做插值（Lerp）调整强度；
2. 法线混合/叠加（BlendAngleCorrectedNormals）：将两种法线叠加，比如用于木材纹理，增加凹凸噪音；
3. 法线增强：将法线贴图与（x,y,1）相乘，可以增强法线，x,y>1，但蓝色通道必须等于 1；
4. 法线减弱：将法线贴图与（0,0,1）做插值（Lerp），可降低法线贴图强度；

<br>

#### 时间节点

---

1. 时间节点（Time）可以带来材质随时间变化的效果

   - 通常可以跟随函数，例如正弦函数（SineRemapped）；
   - 让事件变快或者变慢，可以在 Time 节点后面使用乘法（变快）或者除法（变慢）；

2. 透明度渐变（DepthFade）可以制作渐变效果；

<br>

#### 位置节点

---

1. 绝对世界位置（AbsoluteWorldPosition）：每一个点都获取自己的坐标位置；
2. 物体位置（ObjectPosition）：每一个点都使用物体的锚点坐标位置；
3. 屏幕位置（ScreenPosition）：每一个点都使用其在显示平面中的坐标位置；

   - 屏幕左上角是(0,0)，屏幕右下角是(1,1)，所以，如果屏幕不是等比的话，uv 也不等比；

4. 摄像头位置（CameraPosition）：是将摄像头作为一个点的向量值；
5. 到最近的表面的距离（DistanceToNearestSurface）；
6. 像素深度（PixelDepth）每个点距离摄像机的距离，可以用来做 LOD 等；
7. 凹凸偏移（BumpOffset）[B]：利用灰度图（而不是法线图）制作类似法线贴图的凹凸效果（同样是视觉欺骗）；
8. 与最近物体的距离（DistanceToNearestSurface）：这个属性有助于将物体间的距离赋予材质使用；

   - 需要在项目设置里打开距离场：引擎->渲染->光照->生成网格体距离场；

9. 像素深度（PixelDepth）：每一个点到摄像机的距离；

<br>

#### 层节点

---

1. 层混合（LayerBlend）多图层混合，混合的模式常用的有权重重叠和高度重叠；
2. 距离混合（DistanceBlend）；

<br>

#### 地形材质节点

---

1. 地形层混合（LandscapeLayerBlend）：需要手动增加层数；
2. 地形 UV（LandscapeCoords）：平移/旋转/缩放地形材质；
3. 地形材质分支（LandscapeLayerSwitch）：对于地图是否使用了某个材质层应用不同通道；
4. 地形透明模板（LandscapeVisibilityMask）：

   - 将材质输出节点的 BlendMode 改为 Mask；
   - 将此节点连到材质输出的透明度模板（OpacityMask）上；
   - 就可以在雕刻笔刷中使用可见性笔刷，将地形刷为透明；

<br>

#### 金属材质

---

1. 主要调：反射贴图、金属度（较高）、粗糙度、法线贴图；
2. 污渍、划痕、腐蚀等细节贴图可以通过乘法（Multiply）添加到原图，用插值（Lerp）控制强度；
3. 污渍、划痕、腐蚀等细节的粗糙度可以通过插值（Lerp）添加到原图，就是脏的地方应该很粗糙的意思；

<br>

#### 字体材质

---

1. 蓝图中的 TextRender 无法显示中文，需要创建字体解决；
2. 在 UserWidget 类别中创建字体；

   - FontCacheType 选择 Offline，然后可以选择一种字体；
   - 把 ImportOption 中的 AlphaOnly 勾选；
   - 在 Chars 中填入所有想要用到的中文；
   - 菜单栏 -> 资源(Asset) -> 重新导入(Reimport)；

3. 创建一个材质，BlendMode 选择为 Masked；

   - 添加一个 FontSampleParameter 节点连接 OpacityMask；

4. 在 TextRender 编辑界面中使用自定义字体；

<br>

#### 常用材质算法

---

1. 参考：<https://zhuanlan.zhihu.com/p/100155057?from_voters_page=true>；
2. 距离映射算法：Distance -> Divide -> Clamp -> 1-X -> 立方；
3. 地形材质动态比例：LandscapeCoords -> PixelDepth -> Divide -> Clamp -> Lerp；

<br>

#### 风格化渲染

---

- 三体：<链接已失效>；
- 人类遗迹（厚重感）：<链接已失效>；
- 钢铁 + 森林（蒸汽朋克的 / 末日宁静的）：<链接已失效>；
- 钢铁生物（蒸汽朋克的 / 炫酷的）：<链接已失效>；
- 黑暗西游：<链接已失效>；

<br>
