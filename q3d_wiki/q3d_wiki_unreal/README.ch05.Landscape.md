## 《Unreal 新手村日记》-05-场景编辑（Landscape）

<br>

#### 说明

---

- 项目文档记录了笔者工作积累的云原生微服务技术笔记，以及元宇宙、数字孪生、程序化建模工作学习笔记；
- 项目包含一套元宇宙建模方法论和工程，采用 QGIS -> Blender / Houdini -> Unreal / Unity 的方式实现数字孪生虚拟城市；
- 项目包含一个 Unreal 客户端工程和一个 Unity 客户端工程（建设中）；
- 项目包含一个 Cesium 网页端工程，实现 WebGIS BI 展示；
- 项目包含一个 Vue.js / ElementPlus / TS 网页端工程，实现 Admin 管理后台；
- 项目包含一个 Java / SpringBoot 微服务工程和一个 C# / DotNet 微服务工程（建设中），共用网关和服务注册中心（Nacos），可以协同工作；
- 项目包含一个 Publish 部署工程，采用 Docker / Kubernetes / Jenkins 方式进行服务部署；
- 程序语言涉及 C++ / Java / C# / Python / TS / Shell / VEX；
- 错漏之处恳请读者不吝指正，转载望注明出处：<https://gitee.com/yexiang841/q3d_project/>；
- Copyright 叶湘 ( weixin:yexiang841, email:yexiang841@qq.com ), Shitouren Studio. All Rights Reserved.

<br>

#### Unreal 工作学习笔记

---

- [《Unreal 新手村日记》-00-自学自研项目（Q3D）简介](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch00.Prologue.md)
- [《Unreal 新手村日记》-01-编辑器（Editor）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch01.Editor.md)
- [《Unreal 新手村日记》-02-蓝图（Blueprint）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch02.Blueprint.md)
- [《Unreal 新手村日记》-03-框架（GamePlay）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch03.GamePlay.md)
- [《Unreal 新手村日记》-04-材质（Material）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch04.Material.md)
- `>>>>` [《Unreal 新手村日记》-05-场景编辑（Landscape）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch05.Landscape.md)
- [《Unreal 新手村日记》-06-UI 界面（UMG）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch06.UMG.md)
- [《Unreal 新手村日记》-07-物理模拟/碰撞（Physics/Collision）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch07.Physics.md)
- [《Unreal 新手村日记》-08-骨骼和动画（Skeletal/Animation）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch08.Skeletal.md)
- [《Unreal 新手村日记》-09-粒子系统（ParticleSystem/Niagara）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch09.Niagara.md)
- [《Unreal 新手村日记》-10-C++](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch10.CPP.md)
- [《Unreal 新手村日记》-11-项目设置和打包（Project/Packing）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch11.Project.md)
- [《Unreal 新手村日记》-12-流体（Flip）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch12.Flip.md)
- [《Unreal 新手村日记》-13-毛发系统（Groom）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch13.Groom.md)
- [《Unreal 新手村日记》-14-混沌物理系统（Chaos）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch14.Chaos.md)
- [《Unreal 新手村日记》-15-AI](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch15.AI.md)
- [《Unreal 新手村日记》-16-UE5](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch16.UE5.md)
- [《Unreal 新手村日记》-17-技术美工和程序化建模（TA/PM）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_unreal/README.ch17.TA.md)

<br>

#### 光照（Lighting）

---

1. 基础概念：

   - 光束遮挡：丁达尔效应；
   - 同时发生折射和反射：菲涅尔效应；
   - 泛光：直视光源时产生的过曝模糊；
   - 体积散射强度：可以对体积雾产生丁达尔效果；

2. 定向/平行光源（DirectionalLight）：类似太阳光，主要调强度色温方向；
3. 点光源（PointLight）：即灯泡，主要调强度色温衰减半径；
4. 聚光灯（SpotLight）：即射灯，主要调强度色温衰减半径内径外径；
5. 矩形光源（RectLight）：模拟人造灯带，主要调强度色温衰减半径；
6. 天光（SkyLight）：天空漫反射，使白天室外环境更真实；

<br>

#### 打光技巧

---

1. 辅光在主光 1/3 左右，去掉阴影；

<br>

#### 雾（Fog）

---

1. 指数级高度雾（ExponentialHeightFog）：常用的雾气

   - 越靠近地面雾越浓（指数级渐变）；
   - 打开体积雾属性（VolumetricFog），就会有丁达尔效应（阳光在雾中漫反射形成光线）；

2. 大气雾（AtmosphericFog）：注意大气雾和天空大气只能二选一；

<br>

#### 天空（Sky）

---

1. 天空大气（SkyAtmosphere）：基础的天空着色，注意大气雾和天空大气只能二选一；
2. 天空球（SkySphere）：以蓝图形式存在的天空，可编程和配置，可关联事件；

<br>

#### 曝光

---

1. 自动曝光，可以在设置->项目设置->渲染中定义是否需要自动曝光；

<br>

#### 后期盒子（PostProcessVolume）

---

1. 对一个区域单独进行环境设置；
2. 如果打开无限范围（InfiniteExtent），则全局环境都会应用后期盒子的设置；

<br>

#### 地形（Landscape）

---

1. 创建（Create）：由几个指标进行尺寸控制：

   - 参考：<https://zhuanlan.zhihu.com/p/36731312>；
   - 包含关系：地形 Actor = 整体大小 > 地形组件 > 地形分段 > 四边形方块
   - 整体大小（OverallSize）：在 Scale 之前的最终大小，也代表顶点数；
   - 地形组件（Component）：渲染单元、可视化单元、碰撞单元，每个组件都是一个 CPU 处理成本，组件越少，计算越快，Unreal 建议最大的地形不要超过 1024(32x32)个组件；
   - 地形分段（Section）：分段作用于 LOD，；
   - 四边形方块（Quad）：决定了地形网格最后的顶点数，也就是分辨率，；
   - 建议的尺寸搭配（在最大化面积的同时最小化地形组件的数量）：https://docs.unrealengine.com/4.26/zh-CN/BuildingWorlds/Landscape/TechnicalGuide/
   - 分段大小（）：；
   - 每个组件的分段数（）：；
   - 组件数（NumberOfComponent）：
     - 地形重复多少块，
     - 每个组件都是一个 CPU 处理成本，Unreal 建议最大的地形不要超过 1024 块；
   - 整体分辨率（）：；
   - 缩放（Scale）：整体面积缩放，因为组件数不变，所以缩放会影响细节精度；
   - 材质：只是赋予材质资产，材质中可能会有很多材质层，要分别绘制上去才会有视觉效果；

2. 管理（Manage）：调整组件、分段、四方块等配置；
3. 雕刻（Sculpt）：拔高/平滑地形，主要调尺寸、力度、衰减
4. 绘制（Painnt）：绘制地形的材质；

   - 笔刷分层：
     - 在材质中创建地形分层（LandscapeLayerBlend）；
     - 将具有分层的材质应用到地形材质（LandscapeMaterial）；
     - 在笔刷中会自动识别出分层（Layer）；
     - 为笔刷中的每个分层新建权重混合层（Weighted-Blended Layer）；
     - 然后就可以用各层笔刷单独刷上材质，各层会有权重混合的效果；
     - 在自定义材质中使用 LandscapeCoords 可平移/旋转/缩放不同的地形层；

5. 高程图（Heightmap）：

   - Unreal 只支持 16 位的灰度图，要求 png 格式，需要在 ps 中适配好再导入 Unreal；

<br>

#### 植被（Foliage）

---

1. 手动添加：在植被工具中，先添加多种模型，再调 Density、缩放范围、法线方向、随机旋转、阴影、碰撞等，然后手动刷；
2. 程序化设置：新建 Miscellaneous -> FoliageType，在 Mesh 中添加模型，其余配置可程序化设置；
3. 植被的碰撞设置，首先需要模型本身设置碰撞检测，然后才能加上植被碰撞；
