## 《微服务工作笔记》-- 数据持久化（Persistence）

<br>

#### 介绍

---

- 项目文档记录了笔者工作积累的云原生微服务技术笔记，以及元宇宙、数字孪生、程序化建模工作学习笔记；
- 项目包含一套元宇宙建模方法论和工程，采用 QGIS -> Blender / Houdini -> Unreal / Unity 的方式实现数字孪生虚拟城市；
- 项目包含一个 Unreal 客户端工程和一个 Unity 客户端工程（建设中）；
- 项目包含一个 Cesium 网页端工程，实现 WebGIS BI 展示；
- 项目包含一个 Vue.js / ElementPlus / TS 网页端工程，实现 Admin 管理后台；
- 项目包含一个 Java / SpringBoot 微服务工程和一个 C# / DotNet 微服务工程（建设中），共用网关和服务注册中心（Nacos），可以协同工作；
- 项目包含一个 Publish 部署工程，采用 Docker / Kubernetes / Jenkins 方式进行服务部署；
- 程序语言涉及 C++ / Java / C# / Python / TS / Shell / VEX；
- 错漏之处恳请读者不吝指正，转载望注明出处：<https://gitee.com/yexiang841/q3d_project/>；
- Copyright 叶湘 ( weixin:yexiang841, email:yexiang841@qq.com ), Shitouren Studio. All Rights Reserved.

<br>

#### 微服务工作学习笔记

---

- [《微服务工作笔记》-- 部署说明（Deploy）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_service/README.Deploy.md)
- [《微服务工作笔记》-- 架构选型（Selection）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_service/README.Selection.md)
- [《微服务工作笔记》-- 数据接口（Interface）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_service/README.Interface.md)
- `>>>>` [《微服务工作笔记》-- 数据持久化（Persistence）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_service/README.Database.md)
- [《微服务工作笔记》-- 分布式组件（Distributed）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_service/README.Distributed.md)
- [《微服务工作笔记》-- 云原生（CloudNative）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_service/README.CloudNative.md)
- [《微服务工作笔记》-- 持续集成部署（DevOps）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_service/README.DevOps.md)
- [《微服务工作笔记》-- 系统安全（Security）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_service/README.Security.md)
- [《微服务工作笔记》-- 数据治理（Governance）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_service/README.Governance.md)
- [《微服务工作笔记》-- Java 微服务（SpringBoot）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_service/README.SpringBoot.md)
- [《微服务工作笔记》-- DotNet 微服务（DotNet）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_service/README.DotNet.md)
- [《微服务工作笔记》-- 网页端工程（Frontend）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_service/README.Frontend.md)

<br>

#### 本章摘要

---

1.  更新状态：持续整理上传中；
2.  最后更新：2021.10.20；
3.  本章记录常用数据库的基本操作；

<br>

#### Server 端微服务架构中的组件解析

---

- 数据库管理工具：开发测试运维常用工具；
- 数据库：持久层存储，主要是关系型和非关系型，其它类型还有图数据库、时序数据库等；
- 数据持久化模型：ORM（Object Relational Mapping）将存储结构和读写过程映射到框架对象中；
- 数据库连接池：读写数据库的连接管理器，为连接优化和监控等提供入口；
- 分表分库：海量高并发情况下，单表单库很容易达到压测极限，分表分库是解决数据库压力过大的手段之一；
- 缓存：缓存也是解决海量高并发情况下数据库压力过大的手段之一；
- 搜索引擎：搜索引擎通常是采用另一种持久层存储（Lucene 索引文档），目前主流的搜索引擎都基于 Lucene，除了 Apache Solr 之外，一般不使用关系数据库；
- 分布式文件存储：在云平台的助推下，现在已经叫做对象存储了；

<br>

#### 数据库命名规范

---

1.  引用：<https://mp.weixin.qq.com/s/cBo9drTiSUrbTkjJzG9E_g>；
    - 库名：`dev_q3d_svc`，`dev_`区分环境；
    - 正式表名：`t_hello_world`，`t_`显式声明为表；
    - 临时表名：`temp_t_hello_world`，`temp_`显式声明为临时表；
    - 备份表名：`bak_t_hello_world_20220724`，`bak_`显式声明为备份表，日期全取 8 位；
    - 字段名：`world_nickname`，注意 `NickName` 可不用拆开为 `nick_name`；

<br>

#### MySQL

---

1.  引用：
    - 官网：<https://dev.mysql.com/downloads/mysql/>；
    - 基本数据类型对应表：<https://wenku.baidu.com/view/9e92db1d84c24028915f804d2b160b4e767f816e.html>；
    - MySQL 中的 VARCHAR 和 TEXT：<https://blog.csdn.net/weixin_43198122/article/details/123847819>；
    - MySQL 中的 Key：<https://blog.csdn.net/xiaole060901/article/details/113114635>；
2.  摘要：
    - MySQL 是最流行的非商业数据库，但不开源，且需要一定的商业授权；
    - MariaDB 是 MySQL 的完全开源版；
3.  部署：
    - MacOS 开发环境安装：
      ```
      brew install mysql@5.7
      brew link --force mysql@5.7
      ```
    - TODO:Windows 开发环境安装：；
    - TODO:Linux 开发环境安装：；
    - Docker-Compose 生产环境安装配置：[Demo]()；
    - 默认端口：
      - 3306：数据端口；
    - 初始化前应准备好（或链接）配置文件，如：
      - 配置文件：/usr/local/etc/my.cnf
      - 数据目录：/usr/local/var/mysql
      - 日志目录：/usr/local/log/mysql
    - 初始化运行配置：
      ```
      mysqld --initialize-insecure --user=$(whoami) --basedir=/usr/local/opt/mysql@5.7 --datadir=/usr/local/var/mysql --tmpdir=/tmp
      ```
      - --basedir：软件目录；
      - --datadir：数据目录；
    - 启动 mysql 服务(见下节)
    - 初始化密码等配置：
      ```
      mysql_secure_installation
      ```
      - 其中包括设定 root 密码和配置等，依据提示操作；
      - 如：root 密码：DEVroot123456
    - 远程登录权限配置：
      - 第一层控制：my.cnf 中的 bind-address
        - 127.0.0.1 表示只能本地登录，0.0.0.0 表示所有 ip 都能远程登录，建议用 0.0.0.0，mysql.user 表去控制；
      - 第二层控制：mysql.user 表的 Host 字段
        - localhost 表示只能本地登录，%表示可以远程登录，IP 表示白名单；
    - 首次登录管理员账户，建议新建非管理员账户/密码/权限：
      ```
      # 新建非管理员账户/密码
      mysql -h127.0.0.1 -P3306 -uroot -pDEVroot123456 -Dmysql -e "CREATE USER IF NOT EXISTS 'DEVuser'@'%' IDENTIFIED by 'DEVuser123456'";
      ```
4.  使用：

    - MacOS 开发环境启动/关闭服务：
      ```
      brew services start mysql@5.7
      brew services stop mysql@5.7
      ```
    - TODO:Windows 开发环境启动/关闭服务：；
    - TODO:Linux 开发环境启动/关闭服务：；
    - 常用指令：
      ```
      mysql -h127.0.0.1 -uDEVuser -pDEVuser123456 # 登录 MySQL Shell
      quit # 退出
      ```
    - 账户操作：
      ```
      # 新增账户方法一：严格安全模式（my.cnf 中 sql_mode=NO_ENGINE_SUBSTITUTION,DEVICT_TRANS_TABLES）
      CREATE USER 'DEVuser'@'%' IDENTIFIED by 'DEVuser123456';
      # 新增账户方法二（不建议）：非严格安全模式（my.cnf 中 sql_mode=NO_ENGINE_SUBSTITUTION）
      INSERT INTO mysql.user(Host,User,authentication_string) values("%","DEVuser",password("DEVuser123456"));
      # 删除账户
      DELETE FROM mysql.user WHERE User='DEVuser' and Host=‘%';
      # 更改账户
      UPDATE mysql.user SET authentication_string=password("DEVuser123456") WHERE User='DEVuser' and Host='%';
      ```
    - 库操作：
      ```
      SHOW DATABASES # 列出数据库
      USE test # 进入数据库
      CREATE DATABASE test # 新建库
      DROP DATABASE test # 删除库
      ```
    - 权限操作：

      ```
      SHOW GRANTS; # 查询当前用户权限
      SHOW GRANTS FOR 'DEVuser'@'%'; # 查询特定用户权限；
      GRANT {权限} ON {库名}.{表名} TO {用户名}@{登录主机}; # 授权
      REVOKE {权限} ON {库名}.{表名} FROM {用户名}@{登录主机}; # 撤销授权

      ```

      - 例子：`GRANT create,alter,drop,insert,select,update,delete ON dev_user.* To 'DEVuser'@'%';`
      - 权限包括：
        - all privileges：所有权限；
        - select,delete,create,update,drop 等；
      - 主机包括：
        - localhost：本机访问；
        - %：远程访问；

    - 账户/库/权限操作执行之后需要 flush privileges 才能即时生效；

<br>

#### PostgreSQL

---

1.  引用：
    - 官网：<https://www.postgresql.org/>；
2.  TODO:摘要：
3.  部署：
    - MacOS 开发环境安装：
      ```
      brew install postgresql
      ```
    - TODO:Windows 开发环境安装：；
    - TODO:Linux 开发环境安装：；
    - Docker-Compose 生产环境安装配置：[Demo]()；
    - 默认端口：
      - 5432: 数据端口；
    - 初始化数据库：
      ```
      initdb --locale=zh_CN -E UTF-8 -D /usr/local/var/postgres
      ```
      - --local：默认是 C，实测 C 和 en_US 之类的对中文排序是根据 ASCII 码，还是用 zh_CN 才能解决中文问题；
      - -E UTF-8：通用编码；
      - -D：数据目录（同时也是 PGDATA 环境变量）；
    - 初始化之后就会生成相关的数据库根路径及配置文件，如：
      - 数据目录：/usr/local/var/postgres
      - 配置文件：/usr/local/var/postgres/postgresql.conf
    - 此时应修改（或链接）配置文件，包括：
      - postgresql.conf：端口等；
      - pg_hba.conf：日志、白名单等；
    - 首次登录，配置管理员账户/密码，建议新建非管理员账户/密码/权限：
      ```
      # 登录 PostgreSQL Shell（首次登录无账户密码）
      psql -h 127.0.0.1 -p 5432 -d postgres
      # 列出所有角色和账户
      \du
      # 修改管理员密码，注意：PostgreSQL 的管理员账户不是 root，而是当前系统用户：$(whoami)；
      ALTER USER {whoami} WITH password 'DEVroot123456';
      # 新建 DEVuser 账户
      CREATE USER DEVuser WITH password 'DEVuser123456';
      ```
4.  使用：
    - MacOS 开发环境启动/关闭服务：
      ```
      brew services start postgresql
      brew services stop postgresql
      ```
    - TODO:Windows 开发环境启动/关闭服务：；
    - TODO:Linux 开发环境启动/关闭服务：；
    - 常用指令：
      ```
      psql -h 127.0.0.1 -p 5432 -U DEVuser -W DEVuser123456 -d kong # 登录 PostgreSQL Shell
      \q # 退出
      \du # 列出当前用户具有所有权限；
      ```
    - 库操作：
      ```
      \l # 列出数据库；
      CREATE DATABASE kong OWNER DEVkong; # 为 DEVkong 用户创建数据库 kong；
      DROP DATABASE kong; # 删除数据库 kong；
      \c kong # 进入数据库；
      ```
    - 权限操作：
      - 赋予权限：GRANT {权限} ON DATABASE {库名} TO {账户}；
        - 权限可以是：SELECT,INSERT,UPDATE,DELETE,RULE,ALL；
      - 撤销权限：REVOKE {权限} ON DATABASE {库名} FROM {账户}；

<br>

#### MongoDB

---

1.  引用：
    - 官网：<https://www.mongodb.com/try#community>；
    - 配置：<https://www.jianshu.com/p/8f0c55dbfba1>；
2.  摘要：
    - Zookeeper 有两种模式：
      - Standalone：单机部署，单进程；
      - Cluster：集群部署，进程数必须是单数，因为节点要选举 Master；
3.  部署：
    - MacOS 开发环境安装：
      ```
      brew tap mongodb/brew
      brew install mongodb/brew/mongodb-community@4.2
      brew link --force mongodb-community@4.2
      ```
    - TODO:Windows 开发环境安装：；
    - TODO:Linux 开发环境安装：；
    - Docker-Compose 生产环境安装配置：[Demo]()；
    - 默认端口：
      - 27017: mongod（非分片或配置服务器）或 mongos 的数据端口；
      - 27018: 当 mongod 作为分片服务器时的数据端口；
      - 27019: 当 mongod 作为配置服务器时的数据端口；
    - 首次登录，配置管理员账户/密码，建议新建非管理员账户/密码/权限：
      ```
      # 登录 MongoDB Shell（首次登录无账户密码）
      mongo --host 127.0.0.1 --port 27017
      # 选择管理者库
      use admin
      # 配置 root 账户密码
      db.createUser({user:"root",pwd:"DEVroot123456", roles:["root"]})
      # 验证效果
      show users
      # 先退出
      quit()
      # 重新登录 MongoDB Shell（使用刚配置的 root 账户密码）
      mongo --host 127.0.0.1 --port 27017 -u root -p DEVroot123456
      # 选择管理者库
      use admin
      # 新建 DEVuser 账户/密码/角色(权限)，角色(权限)说明见下节
      db.createUser({user:"DEVuser",pwd:"DEVuser123456", roles:["userAdminAnyDatabase","dbAdminAnyDatabase","readWriteAnyDatabase"]})
      # 验证效果
      show users
      # 退出
      quit()
      ```
4.  使用：
    - MacOS 开发环境启动/关闭服务：
      ```
      brew services start mongodb-community@4.2
      brew services stop mongodb-community@4.2
      ```
    - TODO:Windows 开发环境启动/关闭服务：；
    - TODO:Linux 开发环境启动/关闭服务：；
    - 常用指令：
      ```
      mongo --host 127.0.0.1 --port 27017 -u DEVuser -p DEVuser123456 # 登录 Mongo Shell
      quit() # 退出
      ```
    - TODO:库操作：；
    - TODO:权限操作：；

<br>

#### TODO:Druid

---

1.  数据库连接池

<br>

#### JPA

---

1.  引用：
    - 主键生成策略：<https://www.jianshu.com/p/3b384e873232>；
    - 逆向工程工具 hibernate-tools-maven-plugin：
      - Github：<https://github.com/stadler/hibernate-tools-maven-plugin>；
      - 文档：<https://docs.jboss.org/tools/latest/en/hibernatetools/html_single/>；
    - 逆向工程工具 smartnews/jpa-entity-generator：
      - Github：<https://github.com/smartnews/jpa-entity-generator>；
    - @Transactional 事务：<https://blog.csdn.net/jiangyu1013/article/details/84397366>；
2.  摘要：
    - 主键生成策略：
      - @GeneratedValue(strategy = GenerationType.IDENTITY) // ID 生成策略：自增 ID
      - @GeneratedValue(strategy = GenerationType.TABLE) // ID 生成策略：使用专门的 ID 表生成
      - @GeneratedValue(strategy = GenerationType.SEQUENCE) // ID 生成策略：由的库序列生成
      - @GeneratedValue(strategy = GenerationType.AUTO) // ID 生成策略：自定义 ID
      - 常见数据库支持
        | | MySQL | Oracle | PostgreSQL |
        | :-- |:-- | :---- | :--------- |
        | IDENTITY | 是 | 否 | 是 |
        | SEQUENCE | 否 | 是 | 是 |
        | TABLE | 是 | 是 | 是 |
        | AUTO| 是 | 是 | 是 |
    - JpaRepository 支持接口规范方法名查询，意思是如果在接口中定义的查询方法符合它的命名规则，就可以不用写实现，目前支持的关键字如下：
      | Keyword | Sample |JPQL snippet |
      | :-- |:-- | :---- | :--------- |
      | And | findByNameAndPwd | where name= ? and pwd =? |
      | Or | findByNameOrSex | where name= ? or sex=? |
      | Is,Equals | findById,findByIdEquals | where id= ? |
      | Between | findByIdBetween | where id between ? and ? |
      | LessThan | findByIdLessThan | where id < ? |
      | LessThanEquals | findByIdLessThanEquals | where id <= ? |
      | GreaterThan | findByIdGreaterThan | where id > ? |
      | GreaterThanEquals | findByIdGreaterThanEquals | where id > = ? |
      | After | findByIdAfter | where id > ? |
      | Before | findByIdBefore | where id < ? |
      | IsNull | findByNameIsNull | where name is null |
      | isNotNull,NotNull | findByNameNotNull | where name is not null |
      | Like | findByNameLike | where name like ? |
      | NotLike | findByNameNotLike | where name not like ? |
      | StartingWith | findByNameStartingWith | where name like ‘?%’ |
      | EndingWith | findByNameEndingWith | where name like ‘%?’ |
      | Containing | findByNameContaining | where name like ‘%?%’ |
      | OrderBy | findByIdOrderByXDesc | where id=? order by x desc |
      | Not | findByNameNot | where name <> ? |
      | In | findByIdIn(Collection<?> c) | where id in (?) |
      | NotIn | findByIdNotIn(Collection<?> c) | where id not in (?) |
      | True | findByAaaTrue | where aaa = true |
      | False | findByAaaFalse | where aaa = false |
      | IgnoreCase | findByNameIgnoreCase | where UPPER(name)=UPPER(?) |
      | top | findTop10 | top 10/where ROWNUM <=10 |
3.  部署：
    - ；
4.  使用：
    - ；
