## 《微服务工作笔记》-- 部署说明（Deploy）

<br>

#### 介绍

---

- 项目文档记录了笔者工作积累的云原生微服务技术笔记，以及元宇宙、数字孪生、程序化建模工作学习笔记；
- 项目包含一套元宇宙建模方法论和工程，采用 QGIS -> Blender / Houdini -> Unreal / Unity 的方式实现数字孪生虚拟城市；
- 项目包含一个 Unreal 客户端工程和一个 Unity 客户端工程（建设中）；
- 项目包含一个 Cesium 网页端工程，实现 WebGIS BI 展示；
- 项目包含一个 Vue.js / ElementPlus / TS 网页端工程，实现 Admin 管理后台；
- 项目包含一个 Java / SpringBoot 微服务工程和一个 C# / DotNet 微服务工程（建设中），共用网关和服务注册中心（Nacos），可以协同工作；
- 项目包含一个 Publish 部署工程，采用 Docker / Kubernetes / Jenkins 方式进行服务部署；
- 程序语言涉及 C++ / Java / C# / Python / TS / Shell / VEX；
- 错漏之处恳请读者不吝指正，转载望注明出处：<https://gitee.com/yexiang841/q3d_project/>；
- Copyright 叶湘 ( weixin:yexiang841, email:yexiang841@qq.com ), Shitouren Studio. All Rights Reserved.

<br>

#### 微服务工作学习笔记

---

- `>>>>` [《微服务工作笔记》-- 部署说明（Deploy）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_service/README.Deploy.md)
- [《微服务工作笔记》-- 架构选型（Selection）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_service/README.Selection.md)
- [《微服务工作笔记》-- 数据接口（Interface）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_service/README.Interface.md)
- [《微服务工作笔记》-- 数据持久化（Persistence）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_service/README.Database.md)
- [《微服务工作笔记》-- 分布式组件（Distributed）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_service/README.Distributed.md)
- [《微服务工作笔记》-- 云原生（CloudNative）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_service/README.CloudNative.md)
- [《微服务工作笔记》-- 持续集成部署（DevOps）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_service/README.DevOps.md)
- [《微服务工作笔记》-- 系统安全（Security）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_service/README.Security.md)
- [《微服务工作笔记》-- 数据治理（Governance）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_service/README.Governance.md)
- [《微服务工作笔记》-- Java 微服务（SpringBoot）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_service/README.SpringBoot.md)
- [《微服务工作笔记》-- DotNet 微服务（DotNet）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_service/README.DotNet.md)
- [《微服务工作笔记》-- 网页端工程（Frontend）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_service/README.Frontend.md)

<br>

#### 测试机

---

1.  主机地址：127.0.0.1
    - 账户和密码作为敏感信息已隐去；
2.  测试环境：CentOS Linux release 7.9.2009 (Core)

<br>

#### 域名分配

---

测试环境：

```
127.0.0.1 shitouren.test # 缺省访问主域
127.0.0.1 www.q3d.test # 带 www 访问主域

127.0.0.1 gitlab.q3d.test # 研发组 - Gitlab 管理页
127.0.0.1 rancher.q3d.test # 研发组 - Rancher 管理页
127.0.0.1 k8s.q3d.test # 研发组 - K8S 管理页
127.0.0.1 jenkins.q3d.test # 研发组 - Jenkins 管理页
127.0.0.1 sonarqube.q3d.test # 研发组 - SonarQube 管理页

127.0.0.1 nacos.q3d.test # 业务组 - Nacos 注册/配置中心管理页
127.0.0.1 svc.q3d.test # 业务组 - 微服务业务端口

127.0.0.1 es.svc.q3d.test # 业务组 - 搜索系统 ElasticSearch 数据端口
127.0.0.1 cerebro.svc.q3d.test # 业务组 - 搜索系统 Cerebro 管理页
127.0.0.1 kibana.svc.q3d.test # 业务组 - 搜索系统 Kibana 管理页

127.0.0.1 es-head.q3d.test # 运维组 - ES-Head 管理页

127.0.0.1 es.log.q3d.test # 运维组 - 日志 ELK 系统 ElasticSearch 数据端口
127.0.0.1 cerebro.log.q3d.test # 运维组 - 日志 ELK 系统 Cerebro 管理页
127.0.0.1 kibana.log.q3d.test # 运维组 - 日志 ELK 系统 Kibana 管理页
127.0.0.1 zoonav.log.q3d.test # 运维组 - 日志 ELK 系统 ZooNavigator 管理页
127.0.0.1 kafka.log.q3d.test # 运维组 - 日志 ELK 系统 Kafka 管理页

127.0.0.1 cloudbeaver.q3d.test # 数据组 - CloudBeaver (DBeaver Web 版)

127.0.0.1 openldap.q3d.test # 权限组 - OpenLdap 管理页
127.0.0.1 phpldapadmin.q3d.test # 权限组 - PHPLdapAdmin 管理页
```

TODO:预生产环境域名配置；

TODO:生产环境域名配置；

<br>

#### 部署代码库

---

本项目使用 wwww.gitee.com 作为托管平台，上线部署将自建代码仓库；

部署代码库地址：https://gitee.com/yexiang841/publish.git

多人协作采用两种授权方式：

1.  开启企业账号，对员工账号进行授权（通常用于企业）；
2.  添加合作者账号，对不同的 Git 账号进行授权（通常用于独立开发者合作）；

<br>

#### 部署路径说明

---

本项目支持非固定部署路径，意味着在主机任何位置都可以进行部署，因为：

- 在容器化部署的时候采用的相对路径映射，参考各软件和微服务的 docker-compose.yml 中的 volumes 配置；
- 运行脚本一律采用相对路径的方式（参见各软件和微服务 bin 目录下的 .sh 脚本）；

多人协作测试的场景下，可以采用以下两种部署路径管理方式：

- 公用 root 账户：/opt/user1/publish、/opt/user2/publish、...
- 独立非 root 账户：/home/user1/publish、/home/user2/publish、...
- 基于 Docker 的多账户协作场景下，只需要将别人生成的镜像删除，然后重新在自己的 publish 目录下生成镜像（docker-compose-up.sh）即可在容器中挂载自己的文件；
- 容器镜像只能映射到一个主机路径（publish），因此在多人协作场景下，协作者需要沟通协调，按时间段分配使用权：

<br>

#### 容器映射说明

---

在可配置的情况下，容器内部的配置文件从主机映射：

- `/opt/q3d_lib_publish/{服务}/etc/{配置路径}`；
- `/opt/q3d_lib_publish/{服务}/etc/{配置文件}`；

在可配置的情况下，容器中内部的数据和日志都映射到主机：

- 数据路径指向：`/opt/q3d_lib_publish/{服务}/data`
- 插件路径指向：`/opt/q3d_lib_publish/{服务}/plugin`
- 日志路径指向：`/opt/q3d_lib_publish/{服务}/log`

（参考各软件和微服务的 docker-compose.yml 中的 volumes 配置）

因此所有映射到容器的运行脚本，都采用绝对路径，参考 q3d_lib_publish/mysql5.7/bin/docker-init.sh；

脚本和配置文件前带有`docker-`前缀，表示应用于 Docker 容器内；

<br>

#### CentOS 服务器环境搭建

---

1.  保持 sshd 工作连接：

    - 修改/etc/ssh/sshd_config：
      ```
      ClientAliveInterval 60 # 客户端存活检测间隔
      ClientAliveCountMax 60 # 客户端存活检测次数
      ```
    - 重启 sshd：
      ```
      systemctl restart sshd
      ```

2.  修改 ulimit

    - 编辑 /etc/security/limits.conf 追加：

      ```
      * soft nofile 204800
      * hard nofile 204800
      * soft nproc 204800
      * hard nproc 204800

      * soft memlock unlimited
      * hard memlock unlimited
      ```

    - 编辑 /etc/sysctl.conf 追加：

      ```
      vm.max_map_count=262144
      ```

    - 执行使其生效

      ```
      sysctl -p
      ```

3.  安装 Git 2.x 以上的版本：
    ```
    # yum install -y git # 本指令只会安装 Git 1.x 的版本，注意避坑
    #
    # 用 yum + ius 源安装 Git 2.x 版本
    curl https://setup.ius.io | sh # 添加 ius 源
    yum search git # 查看 git2xx 的具体版本
    yum install -y git222 # 安装 Git 2.22
    #
    # IUS 源也比较慢，实在不行就源码安装：
    curl -O https://github.com/git/git/archive/refs/tags/v2.30.2.zip
    unzip git-2.30.2.zip
    cd git-2.30.2/
    yum -y install autoconf # configure 依赖 autoconf 工具
    yum install -y zlib zlib-devel # make 依赖 zlib 库
    yum -y install asciidoc xmlto # Git 文档依赖库
    yum -y install libcurl-devel # Git 远程连接依赖库（不装这个也能成功安装 Git 但 clone 会出错）
    make configure
    ./configure --prefix=/usr
    make all doc
    make install install-doc install-html
    #
    # 验证安装结果，查看版本
    git version
    ```
4.  安装 Docker 最新稳定版本：
    ```
    # 安装辅助工具
    yum install -y yum-utils device-mapper-persistent-data lvm2
    # 添加官方源
    yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
    # 安装 Docker
    yum install docker-ce docker-ce-cli containerd.io
    # 验证安装结果，查看版本
    docker -v
    # 启动 Docker
    systemctl start docker
    # 验证 Docker 服务是否已经在后台运行
    ps aux | grep docker
    # 运行一个 hello-world 验证 Docker 是否正常可用
    docker run hello-world
    ```
5.  安装 Docker-Compose 最新稳定版本：
    ```
    # yum install epel-release # 安装辅助工具
    # yum -y install python-pip # 安装 pip
    # pip install --upgrade pip # 升级 pip 到最新版本
    # pip install -U docker-compose # 用 pip 工具安装 Docker-Compose
    #
    # 根据系统实际情况，本项目手动安装 Docker-Compose
    curl -L "https://github.com/docker/compose/releases/download/1.29.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose # 下载
    chmod +x /usr/local/bin/docker-compose # 打开执行权限
    ln -s /usr/local/bin/docker-compose /usr/bin/docker-compose # 软连接到默认指令集
    ```
6.  安装 OpenJDK11，用于开发而非部署：
    ```
    yum search java-11-openjdk # 搜索可用版本
    yum install java-11-openjdk-devel.x86_64 # 安装
    java --version # 验证版本
    ```

<br>

#### 拉取源码

---

根据上节部署路径说明和容器映射说明，可以在公用测试主机任意位置拉取代码库进行部署，多人协作应合理规划各自的工作路径；

为简化演示，假设使用 yexiang841 账户，在 ~/Desktop/ 拉取代码库进行部署；

演示主机的设定是多用户公用测试机，因此使用 https 协议拉取代码库进行部署；

多用户协作场景下，在链接地址的 `gitee.com` 之前标注 Gitee 账户 `yexiang841@`，详细分析请参阅《持续集成部署（DevOps）》一章；

拉取源码：

```
cd ~/Desktop/
git clone https://yexiang841@gitee.com/yexiang841/q3d_project.git
cd q3d_project/
```

配置本项目（非全局）的提交署名：

```
cd q3d_project
git config user.name "yexiang841"
git config user.email "yexiang841@qq.com"
```

<br>

#### 依赖关系说明

---

本服务有以下必选依赖：

- MySQL
  - Q3D 服务依赖的数据存储；
  - Nacos 服务依赖的数据存储；
- Nacos
  - Q3D 服务依赖的配置中心 + 服务注册中心；
- Elasticsearch
  - SkyWalking 服务依赖的数据存储；
- SkyWalking
  - Q3D 服务依赖的调用链监控 + 简易日志查询；
- Redis
  - Q3D 服务依赖的数据缓存；

还有以下可选依赖：

- zookeeper
- mongodb
- postgres

<br>

#### DEV / TEST 环境部署 - 构建本地环境

---

bin 路径下有两个用于本地部署的初始化脚本：

- bin/mac-clear.sh # 清理部署环境所有数据，注意，这里会连 mysql 数据库数据一起清除
- bin/mac-ln.sh # 创建标准工作路径链接 /opt/q3d_lib_publish/，注意：这个操作会覆盖已有的 /opt/q3d_lib_publish

首次运行，可先清理数据，再建立公共工作区链接：

```
cd q3d_project/q3d_lib_publish/
sh bin/mac-clear.sh
sh bin/mac-ln.sh
ls -l /opt/ # 检验是否链接到了 /opt/q3d_lib_publish
```

（MacOS 环境）安装 MySQL：

```
brew install mysql@5.7
```

初始化 MySQL：

```
cd q3d_project/q3d_lib_publish/mysql_5.7/bin
sh mac-clear.sh # 清理当前数据
sh mac-ln.sh # 链接当前目录到 HomeBrew 安装的 MySQL 指定路径
sh mac-init.sh # 初始化数据库
sh mac-init-dev.sh # 初始化 DEV 环境（账户密码等）
sh mac-init-test.sh # 初始化 TEST 环境（账户密码等）
```

启动 MySQL：

```
cd q3d_project/q3d_lib_publish/mysql_5.7/bin
sh mac-start.sh # 启动 MySQL 服务，本质上是运行 brew start mysql@5.7
```

初始化 Nacos：

```
cd q3d_project/q3d_lib_publish/nacos_2.1/bin
sh mac-clear.sh # 清理当前数据
sh mac-ln.sh # 链接当前目录到 HomeBrew 安装的 MySQL 指定路径
sh mac-init.sh # 使用刚才新建的 MySQL root 账号执行 ../etc/mac/nacos_mysql_dev.sql，并检验结果
```

补全 Nacos 执行文件：

- Nacos 本地执行文件较大（100M+），因此没有传到 Gitee 仓库，在运行时需要从百度云下载和补全执行文件；
- 百度云地址：<>；
- 下载解压 q3d_project_trunk 库，复制同名目录（q3d_project_trunk/q3d_lib_publish/nacos_2.1/）下的所有文件合并到 q3d_project/q3d_lib_publish/nacos_2.1/；

启动 Nacos：

```
cd q3d_project/q3d_lib_publish/nacos_2.1/bin
sh mac-start.sh # 启动 Nacos 服务，本质上是运行 startup.sh -m standalone
```

验证 Nacos：<http://127.0.0.1:8848/nacos>；

（MacOS 环境）安装 ElasticSearch：

```
brew install elasticsearch
```

初始化 ElasticSearch：

```
cd q3d_project/q3d_lib_publish/elasticsearch_7.10/bin
sh mac-clear.sh # 清理当前数据
sh mac-ln.sh # 链接当前目录到 HomeBrew 安装的 ElasticSeartch 指定路径
```

启动 ElasticSearch：

```
cd q3d_project/q3d_lib_publish/elasticsearch_7.10/bin
sh mac-start.sh # 启动 ElasticSearch 服务，本质上是运行 brew start elasticsearch
```

初始化 SkyWalking-Agent：

```
cd q3d_project/q3d_lib_publish/skywalking-agent_8.11/bin
sh mac-clear.sh # 清理当前数据
sh mac-init.sh # 将本项目用到的 jar 包考入指定目录
```

初始化 SkyWalking-APM：

```
cd q3d_project/q3d_lib_publish/skywalking-apm_9.1/bin
sh mac-clear.sh # 清理当前数据
# sh mac-init.sh # 若使用 MySQL 作为存储，则需要运行脚本初始化对应账户和表，但现在使用 ElasticSearch，无需配置
```

补全 SkyWalking 执行文件：

- SkyWalking 本地执行文件较大（100M+），因此没有传到 Gitee 仓库，在运行时需要从百度云下载和补全执行文件；
- 百度云地址：<>；
- 下载解压 q3d*project_trunk 库，复制两个同名目录（q3d_project_trunk/q3d_lib_publish/skywalking-*/）下的所有文件合并到 q3d*project/q3d_lib_publish/skywalking-*/；

启动 SkyWalking-APM：

```
cd q3d_project/q3d_lib_publish/skywalking-apm_9.1/bin
sh mac-start.sh # 启动 SkyWalking 服务
```

> 注意：
> SkyWalking-APM 偶尔会因为 ElasticSearch 尚未准备好而启动失败，有时需要重启；

验证 SkyWalking：<http://127.0.0.1:9070>；

（MacOS 环境）安装 Redis：

```
brew install redis
```

初始化 Redis：

```
cd q3d_project/q3d_lib_publish/redis_7.0/bin
sh mac-clear.sh # 清理当前数据
sh mac-ln.sh # 链接当前目录到 HomeBrew 安装的 MySQL 指定路径
sh mac-init.sh # 初始化数据库
sh mac-init-dev.sh # 初始化 DEV 环境（账户密码等）
sh mac-init-test.sh # 初始化 TEST 环境（账户密码等）
```

启动 Redis：

```
cd q3d_project/q3d_lib_publish/redis_7.0/bin
sh mac-start.sh # 启动 Redis 服务，本质上是运行 brew start redis
```

TODO:至此各项依赖安装完毕，可启动服务：

导入配置到 Nacos：

- 访问 Nacos：<http://127.0.0.1:8848/nacos>；
- 在【配置管理】->【配置列表】->【导入配置】中，依次上传各微服务的 Nacos 配置，目前各微服务的配置在：
  - q3d_project/q3d_lib_publish/q3d_lib_service_spring/admin-svc-v1/etc/nacos_config/group-admin-svc-v1.zip
  - q3d_project/q3d_lib_publish/q3d_lib_service_spring/auth-svc-v1/etc/nacos_config/group-auth-svc-v1.zip
  - q3d_project/q3d_lib_publish/q3d_lib_service_spring/hello-svc-v1/etc/nacos_config/group-hello-svc-v1.zip
  - q3d_project/q3d_lib_publish/q3d_lib_service_spring/user-svc-v1/etc/nacos_config/group-user-svc-v1.zip

查看调用链跟踪和关键日志：

- 访问 SkyWalking：<http://127.0.0.1:9070>；
- TODO:查看关键调用链跟踪；
- TODO:查看关键日志；

启动批处理：

```
cd q3d_project/q3d_lib_publish/bin
sh mac-start-dev.sh # 启动上述所有服务
```

停止批处理：

```
cd q3d_project/q3d_lib_publish/bin
sh mac-stop-dev.sh # 停止上述所有服务
```

<br>

#### UAT/ PROD 环境部署 - Docker 方式部署

---

本项目每一个软件和微服务都配备有自己的 docker-compose.yml，支持持续集成和持续部署（ CI / CD ）；

创建容器内部网络：

```
docker network list # 查看是否有 n-dev 网络
docker network create n-dev
docker network list # 再次确认 n-dev 网络已建立
```

bin 路径下有三个用于部署的脚本：

- docker-compose-up-d.sh：以 docker-compose up -d 的方式，按顺序启动所有软件和微服务；
- docker-compose-down.sh：停止所有软件和微服务，并删除所有容器；
- docker-clear.sh：清理数据，下一次运行 docker-compose-up-d.sh 时，相当于第一次运行；

首次运行，先清理数据，(docker 方式不需要建立公共工作区链接）：

```
cd q3d_project/q3d_lib_publish/
sh bin/docker-clear.sh # 注意，这里会连 mysql 数据库数据一起清除
```

Docker-Compose 快速部署：

```
cd q3d_project/q3d_lib_publish/
sh bin/docker-compose-up-d.sh
```

<br>

#### s-nginx 部署说明

---

1.  部署服务器只开放 80 端口，因此配置一个 Nginx 监听 80 端口，所有请求通过 Nginx 代理转发；
2.  Nginx 的配置在 q3d_lib_publish/nginx_1.19/etc/c-nginx.conf；
3.  Nginx 的转发规则在 q3d_lib_publish/nginx_1.19/etc/conf.d/c-nginx.{域名}.conf；
4.  服务和端口分配表：

| 分组   | 本地端口        | 容器:端口                  | 服务          | 描述                                      | 连接地址                             |
| :----- | :-------------- | :------------------------- | :------------ | :---------------------------------------- | :----------------------------------- |
| 业务组 | 127.0.0.1:80    | c-nginx:80                 | s-nginx       | Nginx 监听端口                            | <http://www.q3d.test/>               |
| 数据组 | 127.0.0.1:3306  | c-mysql:3306               | s-mysql       | MySql 数据端口                            |                                      |
| 数据组 | 127.0.0.1:27017 | c-mongodb:27017            | s-mongodb     | MongoDB 数据端口                          |                                      |
| 数据组 | 127.0.0.1:5432  | c-postgres:5432            | s-postgres    | PostgreSQL 数据端口                       |                                      |
| 数据组 | 127.0.0.1:6379  | c-redis:6379               | s-redis       | Redis 数据端口                            |                                      |
| 数据组 | 127.0.0.1:26379 | c-redis:26379              | s-redis       | Redis 集群数据同步端口                    |                                      |
| 数据组 | 127.0.0.1:2181  | c-zookeeper:2181           | s-zookeeper   | Zookeeper 数据端口                        |                                      |
| 数据组 | 127.0.0.1:2888  | c-zookeeper:2888           | s-zookeeper   | Zookeeper 集群数据同步端口                |                                      |
| 数据组 | 127.0.0.1:3888  | c-zookeeper:3888           | s-zookeeper   | Zookeeper 集群 Master 选举端口            |                                      |
| 数据组 | 127.0.0.1:9092  | c-kafka:9092               | s-kafka       | Kafka 数据端口                            |                                      |
| 数据组 | 127.0.0.1:5672  | c-rabbitmq:5672            | s-rabbitmq    | RabbitMQ 数据端口                         |                                      |
| 数据组 | 127.0.0.1:11800 | c-skywalking:11800         | s-skywalking  | SkyWalking gRPC 数据端口                  |                                      |
| 数据组 | 127.0.0.1:12800 | c-skywalking:12800         | s-skywalking  | SkyWalking HTTP 数据端口                  |                                      |
| 数据组 | 不做本地映射    | c-elk-log:9200             | s-elk-log     | 日志 ELK 系统 ElasticSearch 数据端口      | <http://es.log.q3d.test/>            |
| 数据组 | 不做本地映射    | c-elk-log:9300             | s-elk-log     | 日志 ELK 系统 ElasticSearch 集群同步端口  |                                      |
| 数据组 | 不做本地映射    | c-elk-log:9600             | s-elk-log     | 日志 ELK 系统 Logstash Beat 数据端口      |                                      |
| 数据组 | 不做本地映射    | c-elk-log:5044             | s-elk-log     | 日志 ELK 系统 Logstash 数据端口           |                                      |
| 数据组 | 不做本地映射    | c-elk-svc:9200             | s-elk-svc     | 搜索系统 ElasticSearch 数据端口           | <http://es.svc.q3d.test/>            |
| 数据组 | 不做本地映射    | c-elk-svc:9300             | s-elk-svc     | 搜索系统 ElasticSearch 集群同步端口       |                                      |
|        |                 |                            |               |                                           |                                      |
| 运维组 | 不做本地映射    | c-cloudbeaver:8978         | s-cloudbeaver | CloudBeaver (DBeaver Web 版)              | <http://cloudbeaver.q3d.test/>       |
| 运维组 | 不做本地映射    | c-zookeeper:9060           | s-zookeeper   | Zookeeper 管理页                          | <http://zookeeper.q3d.test/commands> |
| 运维组 | 不做本地映射    | c-rabbitmq:15672           | s-rabbitmq    | RabbitMQ 管理页                           | <http://rabbitmq.q3d.test/>          |
| 运维组 | 不做本地映射    | c-skywalking:9070          | s-skywalking  | SkyWalking 管理页                         | <http://skywalking.q3d.test/>        |
| 运维组 | 不做本地映射    | c-es-head:9100             | s-es-head     | ES-Head 管理页                            | <http://es-head.q3d.test/>           |
| 运维组 | 不做本地映射    | c-cerebro-log:9000         | s-cerebro-log | 日志 ELK 系统 Cerebro 管理页              | <http://cerebro.log.q3d.test/>       |
| 运维组 | 不做本地映射    | c-elk-log:5601             | s-elk-log     | 日志 ELK 系统 Kibana 管理页               | <http://kibana.log.q3d.test/>        |
| 运维组 | 不做本地映射    | c-cerebro-svc:9000         | s-cerebro-svc | 搜索系统 Cerebro 管理页                   | <http://cerebro.svc.q3d.test/>       |
| 运维组 | 不做本地映射    | c-elk-svc:5601             | s-elk-svc     | 搜索系统 Kibana 管理页                    | <http://kibana.svc.q3d.test/>        |
|        |                 |                            |               |                                           |                                      |
| 业务组 | 127.0.0.1:8848  | c-nacos:8848               | s-nacos       | Nacos 服务注册中心、配置中心、消息总线    | <http://svc.q3d.test/nacos>          |
| 业务组 | 不做本地映射    | c-gateway:9010             | s-gateway     | SpringCloud Gateway 网关                  | <http://svc.q3d.test/gateway>        |
| 业务组 | 不做本地映射    | c-sba-svc:9020             | s-sba-svc     | SpringBoot Admin 服务监控                 | <http://svc.q3d.test/sba>            |
| 业务组 | 不做本地映射    | c-eureka-svc:9030          | s-eureka-svc  | SpringCloud Eureka 服务注册中心（已停用） | <http://svc.q3d.test/eureka>         |
| 业务组 | 不做本地映射    | c-config-svc:9040          | s-config-svc  | SpringCloud Config 配置中心（已停用）     | <http://svc.q3d.test/config>         |
| 业务组 | 不做本地映射    | c-bus-svc:9050             | s-bus-svc     | SpringCloud Bus 消息总线（已停用）        | <http://svc.q3d.test/bus>            |
| 业务组 | 不做本地映射    | c-id-svc:801x              | s-id-svc      | 分布式 ID 生成器微服务                    | <http://svc.q3d.test/id-svc/>        |
| 业务组 | 不做本地映射    | c-hello-svc:802x/702x/602x | s-hello-svc   | 测试与示例微服务(HTTP/gRPC/Dubbo 接口)    | <http://svc.q3d.test/hello-svc/>     |
| 业务组 | 不做本地映射    | c-admin-svc:803x/703x/603x | s-admin-svc   | 管理后台微服务(HTTP/gRPC/Dubbo 接口)      | <http://svc.q3d.test/admin-svc/>     |

<br>

#### 账户密码规则（测试环境）

---

1.  用户名格式：{环境}{服务名}
    - 环境指的是 profile 配置：dev / test / uat / prod；
2.  密码格式：{环境}{服务名}{识别码}

<br>

#### s-cloudbeaver / s-mysql 说明

---

1.  s-cloudbeaver 是容器化的 CloudBeaver，是 DBeaver 的 Web 私有部署版本；
2.  访问 CloudBeaver
    - 入口：<http://cloudbeaver.q3d.test/>
3.  第一次登录需要设定账户密码，测试环境遵循上节所述规则；
4.  s-mysql 服务
    - 在 CloudBeaver 新建 mysql 连接，CloudBeaver 会在容器网络 n-dev 中自动检索 3306 端口，应该会自动找到；
    - s-mysql 服务 的 root 密码在`publish/mysql_5.7/etc/c-env-root.dev`中定义，在`publish/mysql_5.7/docker-compose.yml`中写入；

<br>

#### s-nacos 说明

---

1.  使用 Nacos
    - 入口：<http://svc.q3d.test/nacos>
2.  Nacos 配置 s-mysql 服务：
    - 数据库配置（包括 Nacos 管理员账号和密码）在初始化脚本中定义：`publish/nacos_2.0/etc/c-nacos-mysql-test-create.sql`
    - 数据库权限（包括 Nacos 新建的数据库账号和密码）在初始化脚本中定义：`publish/nacos_2.0/etc/c-nacos-mysql-test-grant.sql.dev`
    - 在 Mysql 的容器脚本中执行：`publish/mysql_5.7/docker-compose.yml`
3.  Nacos 连接 s-mysql 服务：
    - 数据库连接配置在初始化脚本中定义：`publish/nacos_2.0/etc/c-env-mysql.test`
    - 在 Nacos 的容器脚本中执行：`publish/nacos_2.0/docker-compose.yml`

<br>

#### s-elk-log 说明

---

1.  s-elk-log 是用于收集日志的 ELK 集群
2.  拓扑图
    ![拓扑图](https://images.gitee.com/uploads/images/2021/1012/163656_1a40d1e5_7377815.jpeg "拓扑图.jpg")
3.  时序图
    ![时序图](https://images.gitee.com/uploads/images/2021/1012/163808_2e133271_7377815.png "时序图.png")
4.  访问 ES-Head
    - 入口：<http://es-head.q3d.test/>
    - 连接：<http://es.log.q3d.test/>
5.  访问 Cerebro
    - 入口：<http://cerebro.log.q3d.test/>
    - Cerebro 会自动扫描 ES，手动可以：连接：<http://s-elk-log:9200>
6.  访问 Kibana
    - 入口：<http://kibana.log.q3d.test/>

<br>

#### s-elk-svc 说明

---

1.  s-elk-svc 是用于搜索的 ELK 集群，基本上会通过 api 读写，不会使用 Logstash 端口
2.  使用 ES-Head
    - 入口：<http://es-head.q3d.test/>
    - 连接：<http://es.svc.q3d.test/>
3.  使用 Cerebro
    - 入口：<http://cerebro.svc.q3d.test/>
    - Cerebro 会自动扫描 ES，手动可以：连接：<http://s-elk-log:9200>
4.  使用 Kibana
    - 入口：<http://kibana.svc.q3d.test/>
