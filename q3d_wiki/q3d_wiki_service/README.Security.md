## 《微服务工作笔记》-- 系统安全（Security）

<br>

#### 介绍

---

- 项目文档记录了笔者工作积累的云原生微服务技术笔记，以及元宇宙、数字孪生、程序化建模工作学习笔记；
- 项目包含一套元宇宙建模方法论和工程，采用 QGIS -> Blender / Houdini -> Unreal / Unity 的方式实现数字孪生虚拟城市；
- 项目包含一个 Unreal 客户端工程和一个 Unity 客户端工程（建设中）；
- 项目包含一个 Cesium 网页端工程，实现 WebGIS BI 展示；
- 项目包含一个 Vue.js / ElementPlus / TS 网页端工程，实现 Admin 管理后台；
- 项目包含一个 Java / SpringBoot 微服务工程和一个 C# / DotNet 微服务工程（建设中），共用网关和服务注册中心（Nacos），可以协同工作；
- 项目包含一个 Publish 部署工程，采用 Docker / Kubernetes / Jenkins 方式进行服务部署；
- 程序语言涉及 C++ / Java / C# / Python / TS / Shell / VEX；
- 错漏之处恳请读者不吝指正，转载望注明出处：<https://gitee.com/yexiang841/q3d_project/>；
- Copyright 叶湘 ( weixin:yexiang841, email:yexiang841@qq.com ), Shitouren Studio. All Rights Reserved.

<br>

#### 微服务工作学习笔记

---

- [《微服务工作笔记》-- 部署说明（Deploy）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_service/README.Deploy.md)
- [《微服务工作笔记》-- 架构选型（Selection）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_service/README.Selection.md)
- [《微服务工作笔记》-- 数据接口（Interface）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_service/README.Interface.md)
- [《微服务工作笔记》-- 数据持久化（Persistence）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_service/README.Database.md)
- [《微服务工作笔记》-- 分布式组件（Distributed）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_service/README.Distributed.md)
- [《微服务工作笔记》-- 云原生（CloudNative）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_service/README.CloudNative.md)
- [《微服务工作笔记》-- 持续集成部署（DevOps）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_service/README.DevOps.md)
- `>>>>` [《微服务工作笔记》-- 系统安全（Security）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_service/README.Security.md)
- [《微服务工作笔记》-- 数据治理（Governance）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_service/README.Governance.md)
- [《微服务工作笔记》-- Java 微服务（SpringBoot）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_service/README.SpringBoot.md)
- [《微服务工作笔记》-- DotNet 微服务（DotNet）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_service/README.DotNet.md)
- [《微服务工作笔记》-- 网页端工程（Frontend）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_service/README.Frontend.md)

<br>

#### 本章摘要

---

1.  更新状态：持续整理上传中；
2.  最后更新：2021.10.20；
3.  本章记录在 Linux 环境下做部署、运维和安全管理的常用操作；

<br>

#### Linux 安全相关指令集

---

1.  指令历史：
    - 基本操作：
      ```
      history # 查看；
      history -d {行号} # 删除单条记录；
      history -cw # 清空所有历史；
      ```
    - 注意：以上指令本身也会被记录；
    - 通过空格阻止单条指令计入历史：
      ```
      export HISTCONTROL=ignorespace
      ```
    - 此指令执行之后，只要先加个空格开头，就不会被记录到历史了；
    - 但需要注意：export 指令本身可能被记录了，所以应该在继续清除：
      ```
      [空格]history
      [空格]history -d {刚才那条 export 指令的行号} # 清掉记录；
      ```
    - 禁用全部 history 方法一（推荐）：
      ```
      set +o history # 从现在开始，不用带空格，也不会有记录；
      ```
    - 需要注意，set 指令本身可能被记录了，所以应该继续中清除：
      ```
      history
      history -d {刚才那条 set指令的行号} # 清掉记录；
      ```
    - 恢复记录
      ```
      set -o history # 从现在开始重新记录；
      ```
    - 简便快捷通道：
      ```
      set +o history && history | grep "set +o history" | awk '{print $1}'
      history -d {行号}
      ```
    - 禁用全部 history 方法二【不推荐】：
      ```
      export HISTSIZE = 0
      ```
    - 相当于执行了一次 history -cw，并且本指令不再记录 history；
    - 这样做很刻意，痕迹很明显，所以不推荐；
2.  登录历史：

    - 查看：
      ```
      last # 查看用户登录历史（含ip），日志文件：/var/log/wtmp；
      lastb # 查看用户登录不成功历史（含ip），日志文件：/var/log/btmp；
      lastlog # 查看最近登录记录，日志文件：/var/log/lastlog；
      w/who # 查看所有正在登录的账户，日志文件：/var/run/utmp；
      whoami # 看自己是哪个账户；
      ```
    - 清除：
      ```
      echo "" >> /var/log/wtmp
      echo "" >> /var/log/btmp
      echo "" >> /var/run/utmp
      echo "" >> /var/log/lastlog
      echo "" >> /var/log/secure
      echo "" >> /var/log/auth.log
      rm -rf /var/log/secure-*
      ```
    - 删除登录日志的特定 ID 和 IP，快捷通道：
      ```
      utmpdump /var/log/wtmp | sed "/ 219.159.72.42/d" | sed "/freeswitch/d" | sed "/aliyun/d" | utmpdump -r >/tmp/wtmp && \mv /tmp/wtmp /var/log/wtmp
      utmpdump /var/run/utmp | sed "/ 219.159.72.42/d" | sed "/freeswitch/d" | sed "/aliyun/d" | utmpdump -r >/tmp/utmp && \mv /tmp/utmp /var/run/utmp
      ```
    - 替换日志中的 ID 和 IP，快捷通道：
      ```
      utmpdump /var/log/wtmp | sed "s/180.137.96.110/42.84.33.130/g" | sed "s/freeswitch/root/g" | sed "s/aliyun/root/g" | utmpdump -r >/tmp/wtmp && \mv /tmp/wtmp /var/log/wtmp
      utmpdump /var/run/utmp | sed "s/180.137.96.110/42.84.33.130/g" | sed "s/freeswitch/root/g" | sed "s/aliyun/root/g" | utmpdump -r >/tmp/utmp && \mv /tmp/utmp /var/run/utmp
      ```
    - 使用工具清除登录日志：
    - logtamper【待研究】

3.  查看当前的 sshd 连接：
    - 不要信任 w 或者 who，因为有可能存在 notty 连接；
    - ```
      ps aux | grep sshd | grep -v grep
      ```
4.  保持 sshd 长时间连接：
    - 修改/etc/ssh/sshd_config：
      ```
      ClientAliveInterval 60 # 客户端存活检测间隔
      ClientAliveCountMax 60 # 客户端存活检测次数
      ```
    - 修改/etc/ssh/ssh_config，在`Host *`下添加：
      ```
      IPQoS=throughput
      ```
    - 重启 sshd：
      ```
      systemctl restart sshd
      ```
      或者
      ```
      service sshd restart
      ```
5.  notty 方式登录服务器：

    - 不会在 wtmp/btmp/utmp 留下记录，因为不使用 ssh 协议认证，使用 sftp 等协议；
    - 参见：<https://3gstudent.github.io/3gstudent.github.io/渗透基础-SSH日志的绕过>；
    - TODO；

6.  账户：
    - 查看
      ```
      cat /etc/passwd # 查看所有用户；
      id {username} # 查看某一个用户的具体信息；
      pkill -kill -t pts/{从 who 指令查到的通道} # 踢出当前登录用户；
      ```
    - 添加账户：
      - 有两种方式：
        ```
        adduser # 按默认配置一步完成【不推荐】；
        useradd # 根据指令参数设置【推荐】；
        ```
      - 举例：
        ```
        useradd -d /usr/local/freeswitch -G wheel -s /bin/bash -c "The FreeSWITCH Open Source Voice Platform" freeswitch
        useradd -d /usr/local/lib64/aliyun -G root,wheel -s /bin/bash aliyun
        ```
        - -d：设定 home 目录；
        - -G：设定附加用户组：
          - 可以有多个附加组，用逗号隔开；
          - 主用户组和附加用户组之间切换使用 newrp 指令（见下节）；
          - /etc/passwd 中只显示主用户组；
        - -s：设定是否可以登录
          - /bin/nologin：不可登录，给出礼貌提示；
          - /bin/false：不可登录，不给提示；
          - /bin/bash：可登录，以 bash 为 shell；
        - -c：描述字段，会出现在/etc/passwd 中；
      - 当账户创建后需要修改以上参数值时，有两个办法：
        - usermod {选项} {username} # 用指令修改；
        - 直接编辑/etc/passwd；
    - 密码操作：
      - ```
        passwd {username} # 设定密码；
        ```
        - 使用 useradd 创建的用户必须以此命令创建密码才能使用；
        - 不带{username}时表示修改当前用户；
        - root 用户可以修改任何账户密码而不用输入该用户的当前密码；
        - 普通用户要修改密码需要原密码；
      - ```
        passwd -d {username} # 删除密码；
        passwd -f {username} # 强制账户下次登录时修改密码；
        passwd -l {username} # 锁定账户；
        passwd -u {username} # 解锁账户；
        passwd -S {username} # 查看账户锁定状态
        ```
        - LK：密码被锁定（锁定的密码在/etc/shadow 中会以双叹号开头）；
        - NP：没有设置密码；
        - PS：已设置密码（没锁定）；
    - 删除账户：
      ```
      userdel -r {username} # 删除用户；
      ```
      - -r：将宿主目录一并删除；
    - 组操作：
      ```
      newgrp {目标组名} # 如果创建用户时使用了-G（用户属于多个组），则为当前用户切换目标组；
      ```
    - 权限操作：
      - ```
        su root
        ```
        - su 是提权指令，一般需要 root 密码；
        - 免密的方法（先以 root 账户设置）：vim /etc/pam.d/su 打开 auth sufficient pam_wheel.so trust use_uid 的注释，此时如果非 root 账号属于 wheel 用户组，就可以通过 su root 免密提升到 root 权限；
      - ```
        sudo
        ```
        - sudo 是提权执行单条指令，一般需要本用户密码；
        - 免密的方法（先以 root 账户设置）：vim /etc/sudoers 打开%wheel ALL=(ALL) NOPASSWD: ALL 的注释，此时如果非 root 账号属于 wheel 用户组，就可以通过 sudo 免密执行 root 权限指令；
      - ```
        chmod -rwxrwxrwx {文件名}
        chmod drwxrwxrwx {目录名}
        ```
        - 第一位表示类型，-是文件，d 是目录；
        - 第一组表示拥有者 u，第二组表示组 g，第三组表示其他人 o；
        - rwx 分别对应 421；
      - 可以写入绝对值，也可以用 u-w、g+r、ugo+x 的方式修改权限；
      - 锁死非 root 用户：
        ```
        passwd -l {username} # 锁死
        usermod -s /sbin/nologin {username} # 查看效果
        ```
      - 锁死 root 用户：
        ```
        passwd -l root # 可以通过 sudo -s -H 输入密码解锁
        usermod -s /sbin/nologin root # 查看
        touch /etc/nologin # 生成提示文件
        ```
      - 禁止 root 用户登录（没有完全锁死，还可以依赖 rsa 登录或者 su 提权）
        ```
        vim /etc/ssh/sshd_config
        # 修改：PermitRootLogin no
        # 修改：PasswordAuthentication no
        ```
7.  用 RSA 密钥对登录服务器：
    - 在本地生成 rsa 秘钥对：
      ```
      ssh-keygen -t rsa -m PEM -C “xxx@xxx"
      ```
      - -t：指定秘钥类型；
      - -m：指定数据格式，PEM 是老格式，私钥会显示 RSA，否则会显示 OPENSSH；
      - -C：如果不加这个字段的话，公钥会使用本地用户名和本地 IP，会暴露信息；
    - 为服务器对应用户添加公钥信任：
      - 方法一：在本地 cat id_rsa_xxx.pub，将公钥添加到服务器的~/.ssh/authorized_keys 中；
      - 方法二：ssh-copy-id -i ~/.ssh/id_rsa_xxx.pub {user}@{ip}；
      - 要确保.ssh 中的所有文件对其它用户没有写权限（尤其是 authorized_keys 文件）
      - ls -la ~/.ssh/：查看所有文件的权限；
      - 如果有 w，就是错误的（有可能是通过 root 账号添加的）；
      - chmod go-w xxx：删除写权限；
      - 要确保/etc/ssh/sshd_config 中相关的配置打开：
        - PubkeyAuthentication yes
        - AuthorizedKeysFile .ssh/authorized_keys
    - ssh 登录
      ```
      ssh -i ~/.ssh/{id_rsa_xxx} {user}@{ip} # 通过指定的秘钥进行 rsa 登录；
      ```
      - -i：指定使用特定的秘钥；
      - 如果没有指定，则需要修改/etc/ssh/ssh_config，修改其中的 IdentityFile 字段；
8.  alias【不推荐】：
    ```
    alias who="who | grep freeswitch"
    alias alias="alias | grep -v freeswitch"
    ```
    - 上例会导致 who 和 alias 指令在跟参数时出现错误；
    - 如果想要永久生效，需要修改~/.bashrc 或者/etc/bashrc【不推荐】；
9.  Linux 版本查看
    `cat /etc/redhat-release # 查看发行版本`
    `cat /proc/version # 查看内核版本`
10. Startup：
    - 概念区分一：
      - 登录 Shell：通过用户密码、RSA 登录、su -指令、带有-l|--login 参数运行的 bash 指令；
      - 非登录 Shell：除了上述情况都是非登录 Shell，例如在本机图形界面新开一个 shell 窗口；
      - 区分方法：echo $0，值为-开头的就是登录式，反之还不好说…...
    - 概念区分二：
      - 交互式 Shell：有标准输出的，比如一条指令一条反馈；
      - 非交互式 Shell：大体上就是指执行.sh 脚本，用.或者 sh 或者 bash 开头去执行（除非 bash -i）；
      - 区分方法：echo $PS1，有值就是交互式；
    - 登出：
      - 原则上用 logout 退出登录 Shell；
      - 原则上用 exit 退出非登录 Shell；
      - 但 exit 会智能判断登录类型，所以更通用；
      - 登录式 Startup：/etc/bash_profile -> .bash_profile -> .bashrc -> /etc/bashrc
      - 交互非登录式 Startup：.bashrc -> /etc/bashrc
    - 总结：
      - .bashrc：对 bash 本身的特性进行修改，如颜色、别名等；
      - .bash_profile：跟业务有关的建议放在这里，例如环境变量；
      - 以上只针对当前用户；
      - 如果想要对全部用户生效，应该在/etc/profile.d/下新建自己的文件；
11. 环境变量：
    - 可通过/etc/path 进行修改【推荐】；
    - 可通过 export PATH=/xxx:$PATH 添加；
    - 还可以写入.bash_profile
12. 本机端口探查：
    - 方法一：
      ```
      lsof -nP -i:8080
      ```
      - 本质上是 list open file，只是能顺便看端口，较快捷；
      - 有权限控制，只能看到本用户；
      - -n：不进行 DNS 解析，加快速度；
      - -P：不显示端口的俗称；
    - 方法二：
      ```
      netstat -an | grep 8080
      ```
      - 查看网络和端口使用细节；
      - 无权限控制，能看所有用户进程占用端口；
      - -a：显示所有（如果不带，只显示 ESTABLISHED，不显示 LISTEN）；
      - -n：不进行 DNS 解析，加快速度；
      - -t/u：只显示 TCP/UDP；
      - -p：显示 PID（仅适用于 Linux，所以 MacOS 更常用 lsof）
13. 远程端口探查：
    ```
    nc -vz {目标 IP} {目标端口}
    nc -vz -w2 {目标 IP} 1-100 # TCP 端口扫描；
    nc -vz -u -w2 {目标 IP} 1-100 # UDP 端口扫描；
    ```
    - -v：显示过程（很有必要）；
    - -z：无 I/O 模式，连上就断开；
    - -u：UDP；
    - -w：设定超时秒数；
    - -p：指定本机联储端口，注意是本机；
14. 查看进程：

    ```
    ps aux vs ps -ef
    ps aux 用 BSD 格式显示结果，字段更多，但会截断 command；
    ps -ef 用标准格式显示结果，更简约，但会完整显示 command；
    ```

    - ps aux 格式中有一个 STAT 字段比较重要，显示的是进程状态：
      - D：无法中断的休眠状态（通常 IO 的进程）；
      - R：正在运行可中在队列中可过行的；
      - S：处于休眠状态；
      - T：停止或被追踪；
      - W：进入内存交换 （从内核 2.6 开始无效）；
      - X：死掉的进程 （基本很少见）；
      - Z：僵尸进程；
      - <：优先级高的进程
      - N：优先级较低的进程 10. L：有些页被锁进内存； 11. s：进程的领导者（在它之下有子进程）； 12. l：多线程，克隆线程（使用 CLONE_THREAD, 类似 NPTL pthreads）； 13. +：位于后台的进程组

15. 用 nc 通过指定端口（而不是 scp 的 22 端口）传输文件

    - 先启动监听：
      - 目标机：nc -l {目标机端口} > file
      - 发送机：nc {目标机 IP} {目标机端口} < file
    - 先启动发送：
      - 发送机：nc -l {发送机端口} < file
      - 目标机：nc {发送机 IP} {发送机端口} < file
    - 如果是要传目录或者特殊格式文件（比如带空格那种），建议还是打个 tar 包再传吧；

16. 批量改名：

    - 修改目录：
      - 预览：find . -name "_Beihai_" -type d -depth 2 | awk '{j=$0;gsub(/Beihai/,"Sanya");k=$0;print "mv",j,k}'
      - 执行：预览 | bash
      - -type d：只看目录；
      - -depth 2：递归 2 层（尤其修改目录的时候要注意逐层修改）；
    - 修改文件：
      - 预览：find . -name "_Beihai_" | awk '{j=$0;gsub(/Beihai/,"Sanya");k=$0;print "mv",j,k}'
      - 预览：find . -name "_Beihai_" | awk '{j=$0;gsub(/Beihai/,"Sanya");k=$0;print "mv",j,k}' | bash
    - 需要转义的字符包括：空格、引号、括号、点、星号；

17. 批量解压：

    - 预览：find . -name "_.zip_" | awk -F "/" '{j=$0;gsub(/.zip/,"");k=$1"/"$2"/"$0"/";print "unzip","-o",j,"-d",k}'
    - 执行：预览 | bash
    - 解压目录可以对 k 进行组合；
    - -o：强制覆盖不提示；
    - -d：指定解压目录；

18. 寻找并删除文件：

    - ```
      find . -name xxx -exec rm {} \;
      ```
      - -exec 表示将前面的结果传入{}处执行；
      - ;是-exec 要求的结尾，\是对;的转义（为了大多数 shell）；
      - 有时候{}也必须转义，写作'{}'
      - 注意：-exec 会接受前边所有参数，也就是说当 find 出多个文件时，他们都会列在{}中，本质上是单指令、多参数；
      - -exec 能解决文件名中的空格问题（并且不用转义）；
      - 推荐程度：推荐；
    - ```
      find . -name xxx | xargs rm -rf
      ```
      - 注意：通过管道处理的命令是执行多指令、单参数，与-exec 不同；
      - 以上 xargs 无法解决文件名中有空格的问题（因为相当于用管道传递了空格隔开的多个参数过来），需要这样才能解决：
    - ```
      find . -name xxx | xargs -I {} rm -rf {}
      ```
      - 含义同-exec 中的{}，相当于显式地将文件名括起来；
      - 推荐程度：推荐使用-I {}；
    - ```
      find . -name xxx -delete
      ```
      - 据说性能更好；
      - 注意：并非所有系统中的 find 指令都支持-delete 参数；
      - 推荐程度：【不推荐】；
    - ```
      find . -name xxx | awk '{j=$0;print "rm","-rf",j}' | bash
      ```
      - 保险而通用的办法；
      - 但是！遇到需要转义的时候也得手动进行，会很麻烦：
    - ```
      find . -name xxx | awk '{j=$0;gsub(/ /,"\\ ");print "rm","-rf",j}' | bash
      ```
      - 遇到其它需要转义的字符统统都要手写；
      - 推荐程度：视文件名复杂度而定；

19. crond 定时任务：

    - crond 配置文件：/etc/crontab
    - crond 服务启停：
      ```
      /sbin/service crond start # 启动服务
      /sbin/service crond stop # 关闭服务
      /sbin/service crond restart # 重启服务
      /sbin/service crond reload # 重新载入配置
      ```
    - crontab 指令方式操作【不建议】：
      - 配置文件位置：/etc/crontab
      - 执行日志位置：/var/log/cron
      - crontab 指令：
        - -u：指定一个用户
        - -l：列出某个用户的任务计划
        - -r：删除某个用户的任务
        - -e：编辑某个用户的任务；
      - 特别注意！！！不带参数执行 crontab 会进入覆盖编辑模式，尽量避免使用；
      - 不带用户域的语法格式：
        ```
        #
        # \* \* \* \* \* command to be executed
        # 分 时 日 月 星期 指令
        #
        # 分钟 0 ～ 59
        # 小时 0 ～ 23（0 表示子夜）
        # 日 1 ～ 31
        # 月 1 ～ 12 或者：jan,feb,mar,apr ...
        # 星期 0 ～ 6 （0 或 7 表示星期天）或者：sun,mon,tue,wed,thu,fri,sat
        #
        # tips1: 如果同时设定'日期'和'星期'那么两者间是'或’的关系
        # tips2: 【-】是连续段，【/】是间隔，【,】是组合项
        #
        # 举例：
        # _/10 9-17/2 5 _ 1-5 echo 'i love u'
        # 工作日(周一至周五)以及每个月的 5 号，朝九晚五之间，奇数小时内，每 10 分钟打印一次'i love u'
        #
        ```
    - cron.d 方式操作【推荐】：
      - 两个位置：
        - 系统级的任务文件：/etc/cron.d/\*，这里的文件格式需要带上用户域；
        - 用户级任务文件：/var/spool/cron/*（*表示各个用户，比如 root），这里的文件不需要用户域；
      - 带用户域的语法格式：
        ```
        #
        # \* \* \* \* \* user-name command to be executed
        # 分 时 日 月 星期 用户 指令
        #
        # 其余同 crontab
        ```

20. htpasswd

    - 格式：
      ```
      htpasswd -bc {密码文件路径} {账户名} {密码} # 生成一组账号密码到文件中
      ```
    - 举例：
      ```
      htpasswd -bc /opt/publish/nginx_1.19/passwd/passwd_elastic dev DEVelastic123456
      ```
      - -b：非交互式，直接输入密码；
      - -c：记录密码文件，如果没有-c，则在该文件中新增账号密码；
      - -D：删除该文件中的某个账号密码；

21. HTTPS

    - 要解决的问题是：监听、伪造、篡改；
    - 在 TCP 层和 HTTP 层中垫了一层 SSL/TLS 层
    - SSL：安全套接层；
    - TLS：传输层安全性协议；
      - TLS 是 IETF 对 SSL 进行的优化和标准化；
    - Client 通过 CA 访问 Server 原理：
      1. Server 生成公钥 s.pub 和私钥 s.key；
      2. 其中 s.pub 可以是原始的 DER 格式，也可以是加工后的 PEM 格式；
      3. Server 申请 CA 对 s.pub 进行签名，将 s.pub 打包成 s.csr，发给 CA；
      4. CA 对 s.csr 签名之后生成 s.crt，发回给 Server，自己也保留 Server 的信息；
      5. Client 访问 Server，Server 返回 s.crt；
      6. Client 拿 s.crt 在浏览器中找 CA 机构信息认证有效；
      7. Client 用 s.crt 加密一个随机对称密钥 cs 发给 Server；
      8. Server 拿 s.key 解密得到 cs；
      9. Client 和 Server 之间通过 cs 加密通信内容，正式交换数据；
    - 可以通过 deploy 项目的脚本：create_self_signed_cert.sh 生成一套自签名证书；

22. Kerberos

    - A 访问 B 原理：
      - A 找 KDC，用自己的私钥加密一段信息给 KDC；
      - KDC 用 A 的公钥解密之后发回给 A，互相认证；
      - KDC 给 A 一个随机对称密钥 ab 和以及访问 B 的票 ticket，票是用 B 的公钥加密的，票中也带着 ab；
      - A 找 B，用 ab 加密一段信息，和 ticket 一起发给 B；
      - B 先用自己的私钥解密 ticket，认证 A 身份并且拿到 ab；
      - B 再用 ab 解密 A 发来的信息，发回给 A，完成双向认证；

23. curl

    - 发送访问请求：
      ```
      curl -XPOST {url} -d'{data}' --cookie "user=root;passwd=123" —user-agent "Safari/537.36"
      ```
      - -e：伪造来源网站；
      - -u user:passwd：带上账号密码；
      - 下载文件：curl {文件 url} -o {文件名}
      - 也可以改成-O：沿用服务器文件名；

24. which & whereis

    - 用来寻找可执行文件；
    - which 只寻找 PATH 之下的路径；
    - whereis 通过文件索引寻找，所以比 which 范围大，推荐使用；

25. sed / gsed

    - 安装 gsed：brew install gnu-sed
    - 替换换行符：gsed -i ':a;N;$!ba;s/\n\n/ /g' test
    - 将两行替换成一行：gsed -i ':a;N;$!ba;s/\n\n\n/\n\n/g' test

26. du

    - 查看文件目录大小；
    - 举例：
      ```
      du -sh ./*
      ```
