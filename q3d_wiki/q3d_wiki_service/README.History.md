## 《微服务工作笔记》-- 架构选型（Selection）

<br>

#### 介绍

---

- 项目文档记录了笔者工作积累的云原生微服务技术笔记，以及元宇宙、数字孪生、程序化建模工作学习笔记；
- 项目包含一套元宇宙建模方法论和工程，采用 QGIS -> Blender / Houdini -> Unreal / Unity 的方式实现数字孪生虚拟城市；
- 项目包含一个 Unreal 客户端工程和一个 Unity 客户端工程（建设中）；
- 项目包含一个 Cesium 网页端工程，实现 WebGIS BI 展示；
- 项目包含一个 Vue.js / ElementPlus / TS 网页端工程，实现 Admin 管理后台；
- 项目包含一个 Java / SpringBoot 微服务工程和一个 C# / DotNet 微服务工程（建设中），共用网关和服务注册中心（Nacos），可以协同工作；
- 项目包含一个 Publish 部署工程，采用 Docker / Kubernetes / Jenkins 方式进行服务部署；
- 程序语言涉及 C++ / Java / C# / Python / TS / Shell / VEX；
- 错漏之处恳请读者不吝指正，转载望注明出处：<https://gitee.com/yexiang841/q3d_project/>；
- Copyright 叶湘 ( weixin:yexiang841, email:yexiang841@qq.com ), Shitouren Studio. All Rights Reserved.

<br>

#### 微服务工作学习笔记

---

- [《微服务工作笔记》-- 部署说明（Deploy）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_service/README.Deploy.md)
- `>>>>` [《微服务工作笔记》-- 架构选型（Selection）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_service/README.Selection.md)
- [《微服务工作笔记》-- 数据接口（Interface）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_service/README.Interface.md)
- [《微服务工作笔记》-- 数据持久化（Persistence）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_service/README.Database.md)
- [《微服务工作笔记》-- 分布式组件（Distributed）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_service/README.Distributed.md)
- [《微服务工作笔记》-- 云原生（CloudNative）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_service/README.CloudNative.md)
- [《微服务工作笔记》-- 持续集成部署（DevOps）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_service/README.DevOps.md)
- [《微服务工作笔记》-- 系统安全（Security）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_service/README.Security.md)
- [《微服务工作笔记》-- 数据治理（Governance）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_service/README.Governance.md)
- [《微服务工作笔记》-- Java 微服务（SpringBoot）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_service/README.SpringBoot.md)
- [《微服务工作笔记》-- DotNet 微服务（DotNet）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_service/README.DotNet.md)
- [《微服务工作笔记》-- 网页端工程（Frontend）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_service/README.Frontend.md)

<br>

#### 服务端技术架构发展史

---

1.  为了解释架构选型的驱动力，首先就架构发展的历史背景进行简要梳理；
2.  以下几节将对 IT 架构中 C/S 模式和 B/S 模式的发展历程做个回顾；

<br>

#### 互联网出现之前的 C 端应用 (Client) 架构

---

1.  关键事件：Dos、Windows95；
2.  举例：
    - Dos 软件：UCDOS、WPS；
    - Dos 游戏：大航海时代；
    - Window 软件：Photoshop；
    - Window 游戏：仙剑奇侠传；
3.  数据存储在 Client；

<br>

#### 互联网出现之后的早期 C/S (Client/Server) 架构

---

1.  关键事件：以太网、MFC；
2.  举例：
    - 专用的联网客户端：银行取款机；
3.  技术要点： 要安装，要联网，没有用 Http 协议，单进程多线程；
4.  数据存储在 Server；

<br>

#### 另类 C/S 架构 - 局域网对战游戏

---

1.  关键事件：交换机、局域网；
2.  举例：
    - 局域网对战游戏：红警、CS；
3.  技术要点： 要安装，要联网，没有用 Http 协议，单进程多线程；
4.  番外：C/S 架构的前身 - 局域网对战游戏；
    - 每个 Client 自带一个 Server，用来响应局域网中别的 Client；
    - 游戏是推动技术进步的核心因素之一；
5.  数据存储在 Client；

<br>

#### 早期的 B/S (Browser/Server) 架构

---

1.  关键事件：IE、HTTP、HTML；
2.  举例：
    - 门户新闻网站：新浪、搜狐；
3.  网速也是推动技术进步的核心因素之一；
4.  数据全部存储在 Server，页面框架和数据都是每次加载；

<br>

#### 如今的 B/S (Browser/Server) 架构

---

1.  关键事件：HTML5/CSS3/ES6、Chrome、MVVM；
2.  举例：
    - 深度交互网站：微博、FaceBook；
    - 网页式 ERP：Vue-Element-Admin；
    - 移动端 H5 页面：；
3.  数据全部存储在 Server，页面框架加载一次，数据动态加载；

<br>

#### C/S 技术架构图

---

1.  关键事件：WebSocket；
2.  举例：
    - 联网游戏：传奇、魔兽世界；
    - 联网软件工具：金山毒霸、QQ；
3.  C/S 架构的固有缺陷：
    - 客户端多版本兼容问题；
    - 客户端漏洞难以修复问题；

<br>

#### 特殊 C/S 架构 - P2P、区块链、Web3、边缘计算

---

1.  举例：
    - 点到点下载：BT、迅雷；
    - 区块链应用：比特币、溯源系统；
2.  为何始终不是主流：
    - 无序经营的监管问题；
    - 资源无法统一规划和使用；

<br>

#### Client 框架发展史

---

1.  第一代 GUI：
    - Xerox Alto，非个人系统；
    - Apple Lisa，第一个个人系统；
    - X Window System，运行于 UNIX，麻省理工开发；
2.  MS-DOS 时代：
    - WPS、UCDOS、大航海时代；
    - 16 位；
    - 主要操作靠 CLI（命令行界面），程序手写 GUI（图形用户界面）；
3.  Windows3.x、Windows95、Windows98、Windows2000、Windows7、Windows10、Windows11：
    - 应用程序大爆发；
    - 16 位 -> 32 位 -> 64 位；
    - Windows API -> Microsoft Foundation Class（MFC） -> Windows Form -> Windows Presentation Foundation（WPF）；
    - WPF：地球人都在等你跨平台（基于 C#/.Net）；
4.  PC 跨平台：由系统多元化带来革新：
    - Linux 两强：
      - Qt，基于 C/C++，案例有 KDE 桌面系统和 WPS 软件，是目前 PC 跨平台的主流技术；
      - Gtk+，也基于 C/C++，案例有 Gnome 桌面系统；
    - iOS/MacOS：Cocoa；
    - 伟大的 Java 虚拟机 + Swing/AWT
    - DotNet 的跨平台尝试：Mono；
    - PyQt，基于 Qt 进行 Python 扩展，引入了 Web 框架；
    - Electron：Github 背书，基于 Javascript，完全就是 Web 框架；
5.  移动端：
    - iOS：Objective-C、Swift；
    - Android：Java、Kotlin；
6.  移动端跨平台：
    - PhoneGap：第一代移动跨平台框架，目前死在沙滩上；
    - ReactNative：FaceBook 背书，接近瓶颈，因为 UI 开发方面还是平台相关的；
    - Flutter：谷歌背书，值得期待，在 UI 开发上，已经做到平台无关，但是使用了新语言 Dart；
    - Uni-App：从 Vue 发展而来的国产精品，值得期待，在 UI 开发上，依赖于社区开发的第三方库；
    - 以上都是通过 Web 框架实现跨平台的，唯一一个使用底层库实现跨移动平台的框架是 Xamarin（Mono 的分支项目）；
7.  跨全平台（PC 和移动端） GUI 格局：
    - Electron：值得期待；
    - Unity：基于 C#，通过 Mono 框架实现跨平台；
    - Unreal：基于 C++ 魔改，行业称 U++，通过底层 C++ 库实现跨平台；
8.  一个经常被忽略的惊人事实：二个游戏引擎都实现了跨全平台（包括 Windows、Linux、MacOS、iOS、Android）

<br>

#### Browser 网页端框架发展史

---

1.  一些基本事实：
    - 不管是 PC 还是 Androi/iOS，除了 Top100 之外，大部分客户端应用都是基于信息流的简单界面；
      - 基于信息流；
      - 基于短连接的简单交互，不需要长连接；
      - 除了鉴权信息之外，基本不需要太多的客户端存储；
    - 随着 HTML5/CSS6/ECMAScript6 的发展，Web 应用已经能完成大部分客户端的功能；
    - 随着浏览器标准的统一，Web 框架成为了一种跨平台的框架；
2.  B 比 C 好在哪？
    - 真正的跨平台（一套代码多处运行）；
    - 轻客户端，无需担忧更新问题和漏洞修复问题；
3.  MVC 时代：
    - 网页端基本等于 MVC 中的 V，应用 Thymeleaf、Freemarker 等模板引擎，Controller 组装 Model 到 View 中再吐出来；
    - 解决了：
      - PHP 等语言的过程式页面生成原始方式；
    - 未解决：
      - 前后端分离（数据访问解耦、后端语言解耦）；
      - 页面逻辑解耦（页面表现层与后端逻辑深度绑定）；
4.  SPA（Single Page Application）时代：
    - HTML 和 CSS 从静态服务器或者 CDN 获取，业务数据通过 AJAX 从后端获取；
    - 解决了：
      - 前后端分离（数据访问解耦、后端语言解耦）；
    - 未解决：
      - 页面逻辑解耦（页面表现层与 API 接口深度绑定）；
5.  MVVM（Model View ViewModel）时代：
    - 在页面表现层与 API 之间增加再 ViewModel，而 ViewModel 与页面表现层中的数据双向绑定；
    - 解决了：
      - 前后端分离（数据访问解耦、后端语言解耦）；
      - 页面逻辑解耦；
    - 三种 MVVM 的实现：谷歌的 AngularJS（模块化）、脸书的 React.js（虚拟 Dom）、尤雨溪的 Vue.js（模块化 + 虚拟 Dom）；

<br>

#### Server 框架发展史

---

1.  单一应用架构：
    当网站流量很小时，只需一个应用，将所有功能都部署在一起，以减少部署节点和成本。此时，用于简化增删改查工作量的数据访问框架（ORM）是关键；

2.  垂直应用架构：
    当访问量逐渐增大，单一应用增加机器带来的加速度越来越小，将应用拆成互不相干的几个应用，以提升效率。此时，用于加速网页端页面开发的 Web 框架（MVC）是关键；

3.  分布式服务架构：
    当垂直应用越来越多，应用之间交互不可避免，将核心业务抽取出来，作为独立的服务，逐渐形成稳定的服务中心，使网页端应用能更快速的响应多变的市场需求。此时，用于提高业务复用及整合的分布式服务框架（RPC）是关键；

4.  流动计算架构：
    当服务越来越多，容量的评估，小服务资源的浪费等问题逐渐显现，此时需增加一个调度中心基于访问压力实时管理集群容量，提高集群利用率。此时，用于提高机器利用率的资源调度和治理中心（SOA）是关键；

<br>

#### TODO:容器化和云原生发展史

---

1.  虚拟机；
2.  容器；
3.  容器网络；
4.  容器治理；
