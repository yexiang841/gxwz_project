## 《微服务工作笔记》-- DotNet 微服务（DotNet）

<br>

#### 介绍

---

- 项目文档记录了笔者工作积累的云原生微服务技术笔记，以及元宇宙、数字孪生、程序化建模工作学习笔记；
- 项目包含一套元宇宙建模方法论和工程，采用 QGIS -> Blender / Houdini -> Unreal / Unity 的方式实现数字孪生虚拟城市；
- 项目包含一个 Unreal 客户端工程和一个 Unity 客户端工程（建设中）；
- 项目包含一个 Cesium 网页端工程，实现 WebGIS BI 展示；
- 项目包含一个 Vue.js / ElementPlus / TS 网页端工程，实现 Admin 管理后台；
- 项目包含一个 Java / SpringBoot 微服务工程和一个 C# / DotNet 微服务工程（建设中），共用网关和服务注册中心（Nacos），可以协同工作；
- 项目包含一个 Publish 部署工程，采用 Docker / Kubernetes / Jenkins 方式进行服务部署；
- 程序语言涉及 C++ / Java / C# / Python / TS / Shell / VEX；
- 错漏之处恳请读者不吝指正，转载望注明出处：<https://gitee.com/yexiang841/q3d_project/>；
- Copyright 叶湘 ( weixin:yexiang841, email:yexiang841@qq.com ), Shitouren Studio. All Rights Reserved.

<br>

#### 微服务工作学习笔记

---

- [《微服务工作笔记》-- 部署说明（Deploy）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_service/README.Deploy.md)
- [《微服务工作笔记》-- 架构选型（Selection）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_service/README.Selection.md)
- [《微服务工作笔记》-- 数据接口（Interface）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_service/README.Interface.md)
- [《微服务工作笔记》-- 数据持久化（Persistence）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_service/README.Database.md)
- [《微服务工作笔记》-- 分布式组件（Distributed）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_service/README.Distributed.md)
- [《微服务工作笔记》-- 云原生（CloudNative）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_service/README.CloudNative.md)
- [《微服务工作笔记》-- 持续集成部署（DevOps）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_service/README.DevOps.md)
- [《微服务工作笔记》-- 系统安全（Security）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_service/README.Security.md)
- [《微服务工作笔记》-- 数据治理（Governance）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_service/README.Governance.md)
- [《微服务工作笔记》-- Java 微服务（SpringBoot）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_service/README.SpringBoot.md)
- `>>>>` [《微服务工作笔记》-- DotNet 微服务（DotNet）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_service/README.DotNet.md)
- [《微服务工作笔记》-- 网页端工程（Frontend）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_service/README.Frontend.md)

<br>

#### 本章摘要

---

1.  更新状态：持续更新中；
2.  最后更新：2021.10.20；
3.  微服务脚手架 C#/DotNet6/React.js 版；
4.  代码位置：<https://gitee.com/yexiang841/publish/blob/master/microservices_dotnet>

<br>

#### DotNet Core

---

1.  引用：
    - [参考]<>；
2.  摘要：
    - ；
3.  部署：
    - 使用 System.Drawing.Common 程序集的 DotNet 应用程序（操作 Image,Bitmap 类型，实现生成验证码、二维码，图片操作等功能）要求安装 libgdiplus
      - brew install mono-libgdiplus；
    - 然后安装官方 pkg（默认路径是：/usr/local/share/dotnet）；
      - 官网：<https://dotnet.microsoft.com/en-us/download/dotnet>；
    - 验证：
      - 重开终端；
      - dotnet --list-sdks
      - dotnet --list-runtimes
    - 卸载：
      ```
      dotnet --list-sdks
      sdk_version="5.0.100"
      sudo rm -rf /usr/local/share/dotnet/sdk/$sdk_version
      sudo rm -rf /usr/local/share/dotnet/shared/Microsoft.NETCore.App/$sdk_version
      sudo rm -rf /usr/local/share/dotnet/shared/Microsoft.AspNetCore.All/$sdk_version
      sudo rm -rf /usr/local/share/dotnet/shared/Microsoft.AspNetCore.App/$sdk_version
      sudo rm -rf /usr/local/share/dotnet/host/fxr/$sdk_version
      dotnet --list-runtimes
      runtime_version="5.0.0"
      sudo rm -rf /usr/local/share/dotnet/shared/Microsoft.AspNetCore.App/$runtime_version
      sudo rm -rf /usr/local/share/dotnet/shared/Microsoft.NETCore.App/$runtime_version
      ```
4.  使用：
    ```
    cd dotnet6_grpc_nacos
    dotnet new sln -n q3d-service # 建立一个解决方案 q3d-service 缺省 -n 会使用目录名
    dotnet new webapi -o hello-svc --no-https # 新建一个 webapi 类型的应用，且不使用 HTTPS
    dotnet new webapi -o admin-svc --no-https # 新建一个 webapi 类型的应用，且不使用 HTTPS
    dotnet sln add hello-svc # 将新建的 hello-svc 加入解决方案中
    dotnet sln add admin-svc # 将新建的 admin-svc 加入解决方案中
    cd hello-svc # 进入刚新建的 webapi 项目 hello-svc
    dotnet run # 运行 hello-svc
    ```

<br>

#### Dapr

---

1.  引用：
    - 官网：<https://dapr.io/>；
    - 参考：<https://blog.csdn.net/sD7O95O/article/details/117639004>；
2.  摘要：
    - ；
3.  部署：
4.  使用：
