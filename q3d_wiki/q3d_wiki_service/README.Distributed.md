## 《微服务工作笔记》-- 分布式组件（Distributed）

<br>

#### 介绍

---

- 项目文档记录了笔者工作积累的云原生微服务技术笔记，以及元宇宙、数字孪生、程序化建模工作学习笔记；
- 项目包含一套元宇宙建模方法论和工程，采用 QGIS -> Blender / Houdini -> Unreal / Unity 的方式实现数字孪生虚拟城市；
- 项目包含一个 Unreal 客户端工程和一个 Unity 客户端工程（建设中）；
- 项目包含一个 Cesium 网页端工程，实现 WebGIS BI 展示；
- 项目包含一个 Vue.js / ElementPlus / TS 网页端工程，实现 Admin 管理后台；
- 项目包含一个 Java / SpringBoot 微服务工程和一个 C# / DotNet 微服务工程（建设中），共用网关和服务注册中心（Nacos），可以协同工作；
- 项目包含一个 Publish 部署工程，采用 Docker / Kubernetes / Jenkins 方式进行服务部署；
- 程序语言涉及 C++ / Java / C# / Python / TS / Shell / VEX；
- 错漏之处恳请读者不吝指正，转载望注明出处：<https://gitee.com/yexiang841/q3d_project/>；
- Copyright 叶湘 ( weixin:yexiang841, email:yexiang841@qq.com ), Shitouren Studio. All Rights Reserved.

<br>

#### 微服务工作学习笔记

---

- [《微服务工作笔记》-- 部署说明（Deploy）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_service/README.Deploy.md)
- [《微服务工作笔记》-- 架构选型（Selection）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_service/README.Selection.md)
- [《微服务工作笔记》-- 数据接口（Interface）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_service/README.Interface.md)
- [《微服务工作笔记》-- 数据持久化（Persistence）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_service/README.Database.md)
- `>>>>` [《微服务工作笔记》-- 分布式组件（Distributed）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_service/README.Distributed.md)
- [《微服务工作笔记》-- 云原生（CloudNative）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_service/README.CloudNative.md)
- [《微服务工作笔记》-- 持续集成部署（DevOps）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_service/README.DevOps.md)
- [《微服务工作笔记》-- 系统安全（Security）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_service/README.Security.md)
- [《微服务工作笔记》-- 数据治理（Governance）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_service/README.Governance.md)
- [《微服务工作笔记》-- Java 微服务（SpringBoot）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_service/README.SpringBoot.md)
- [《微服务工作笔记》-- DotNet 微服务（DotNet）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_service/README.DotNet.md)
- [《微服务工作笔记》-- 网页端工程（Frontend）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_service/README.Frontend.md)

<br>

#### 本章摘要

---

1.  更新状态：持续整理上传中；
2.  最后更新：2021.10.20；
3.  本章记录微服务分布式组件的概念和用法；

<br>

#### Server 端微服务架构中的组件解析

---

- 入口代理：第一道工序，用于解析域名，根据域名或者路径映射到服务进程；
- 服务注册中心：让服务可以不理会 IP 和 Port，直接用服务名互相访问，因此可以支持动态部署，实现线性扩容和服务热备；
- 配置中心：实现不停服更新配置；
- 消息总线：实现全局的消息订阅机制；
- 消息队列/异步通信：将任务事件排入队列中异步执行，或用于通信，或用于分布式事务的实现；
- 分布式事务：当一个业务链路需要跨越多个微服务节点时，出现调用中断怎么办？事务如何回滚？就是需要重点解决的问题；
- 智能网关：实现 BFF（Backend for Frontend）；
  - 负载均衡：当服务线性扩容时，如何将流量按需分配到；
- 服务接口：从单体进程到微服务，第一要务就是解决服务间通信的问题；
- 接口文档：从手写文档，到自动导出文档，时代一直在变；
- 认证：在微服务架构上，认证可以放在每个微服务，但更好的选择是放在网关；
- ID 生成器：一个号的 ID 有几个指标，数据库自增 ID 和 UUID 都无法完全满足，因此市面上出现了几个流行的 ID 生成器；

<br>

#### Redis

---

1. 官网：<https://redis.io>；
2. HomeBrew 方式安装：
   - brew install redis
3. 此时应修改（或链接）配置文件，初始路径如下：
   - 配置文件： /usr/local/etc/redis.conf
   - 日志路径：/usr/local/var/log/mongodb/
   - 数据路径：/usr/local/var/db/redis/
4. 启动/关闭服务：brew services start/stop redis
5. 默认端口：6379
6. redis-cli 登录：redis-cli -h 127.0.0.1 -p 6379
7. redis-cli 退出：quit
8. 增删库：
   - ；

<br>

#### Zookeeper

---

1.  引用：
    - 官网：<https://zookeeper.apache.org/>；
    - 参考：<https://www.runoob.com/w3cnote/zookeeper-tutorial.html>；
    - 参考：<https://zhuanlan.zhihu.com/p/62526102>；
    - 指令详情：<http://www.wjhsh.net/yangzhenlong-p-8271151.html>；
2.  摘要：
    - Zookeeper 有两种模式：
      - Standalone：单机部署，单进程；
      - Cluster：集群部署，进程数必须是单数，因为节点要竞争 Master；
3.  部署：
    - MacOS 开发环境安装：
      ```
      brew install zookeeper
      ```
      - 若安装 Kafka，Zookeeper 会作为依赖被自动安装：
    - TODO:Windows 开发环境安装：；
    - TODO:Linux 开发环境安装：；
    - Docker-Compose 生产环境安装配置：[Demo]()；
    - 默认端口：
      - 2181：数据端口；
      - 2888：集群数据同步端口 ；
      - 3888：集群 Master 选举端口；
      - 8080：自带管理页端口（常被占用）；
4.  使用：
    - MacOS 开发环境启动/关闭服务：
      ```
      brew services start zookeeper
      brew services stop zookeeper
      ```
    - TODO:Windows 开发环境启动/关闭服务：；
    - TODO:Linux 开发环境启动/关闭服务：；
    - 常用指令：
      ```
      zkServer status # 使用自带服务端指令查看集群 zookeeper 服务状态
      zkCli -server 127.0.0.1:2181 # 登录 Zookeeper Shell
      ```
    - Zookeeper Shell 常用指令：
      ```
      h # 帮助列表
      version # 查看 Zookeeper 版本
      ls / # 列出根节点下的所有子节点
      create -e /test 132 # 创建临时节点 /test 上存储 123
      create /test 132 # 创建永久节点 /test 上存储 123
      ls /test # 列出节点 /test 下的直接子节点
      stat /test # 查看某个节点的详细信息
      get /test # 获取节点 /test 上的数据
      set /test 123 # 设置节点 /test 存储数据为 123
      quit # 退出
      ```

<br>

#### Kafka

---

1.  引用：
    - 官网：<https://kafka.apache.org/>；
2.  摘要：
    - Kafka 的运行依赖 Zookeeper 服务的运行；
3.  部署：
    - MacOS 开发环境安装：
      ```
      brew install kafka
      ```
    - TODO:Windows 开发环境安装：；
    - TODO:Linux 开发环境安装：；
    - Docker-Compose 生产环境安装配置：[Demo]()；
    - 默认端口：
      - 9092：数据端口；
4.  使用：
    - MacOS 开发环境启动/关闭服务：
      ```
      brew services start kafka
      brew services stop kafka
      ```
    - TODO:Windows 开发环境启动/关闭服务：；
    - TODO:Linux 开发环境启动/关闭服务：；

<br>

#### RabbitMQ

---

---

1.  引用：
    - 官网：<https://www.rabbitmq.com/>；
2.  摘要：
    - RabbitMQ 的依赖 Erlang；
3.  部署：
    - MacOS 开发环境安装：
      ```
      brew install rabbitmq
      ```
    - TODO:Windows 开发环境安装：；
    - TODO:Linux 开发环境安装：；
    - Docker-Compose 生产环境安装配置：[Demo]()；
    - 默认端口：
      - 5672：数据端口；
      - 15672：自带管理页端口；
    - 权限配置：
      - 管理页初始化默认账号密码： guest / guest ；
4.  使用：
    - MacOS 开发环境启动/关闭服务：
      ```
      brew services start rabbitmq
      brew services stop rabbitmq
      ```
    - TODO:Windows 开发环境启动/关闭服务：；
    - TODO:Linux 开发环境启动/关闭服务：；
    - 查看当前使用的有效配置：
      ```
      rabbitmqctl environment
      ```

<br>

#### Nacos

---

1.  引用：
    - 官网：<https://nacos.io/zh-cn/index.html>；
    - 下载：<https://github.com/alibaba/nacos/releases>；
2.  摘要：
    - Nacos 的配置可导出为 zip，迁移时可直接导入；
3.  部署：
    - MacOS / Linux / Windows 开发环境安装：从 Github 下载源码编译或者二进制版本，解压之后即可；
    - Docker-Compose 生产环境安装配置：[Demo]()；
    - 默认端口：
      - 8848：自带管理页端口；
4.  使用：
    - MacOS /Linux 开发环境启动/关闭服务：
      ```
      sh startup.sh -m standalone
      ```
    - Windows 开发环境启动/关闭服务：
      ```
      startup.cmd -m standalone
      ```

<br>

#### Sentinel

---

1.  引用：
    - 官网：<https://sentinelguard.io/zh-cn/index.html>；
2.  摘要：
    - ；
3.  部署：
    - MacOS 开发环境安装：
      ```
      ```
    - TODO:Windows 开发环境安装：；
    - TODO:Linux 开发环境安装：；
    - Docker-Compose 生产环境安装配置：[Demo]()；
    - 默认端口：
      - ：数据端口；
4.  使用：
    - MacOS 开发环境启动/关闭服务：
      ```
      ```
    - TODO:Windows 开发环境启动/关闭服务：；
    - TODO:Linux 开发环境启动/关闭服务：；

<br>

#### SkyWalking

---

1.  引用：
    - 官网：<https://skywalking.apache.org/>；
2.  摘要：
    - 配置详解：<https://blog.csdn.net/lizz861109/article/details/114578826>；
    - 使用详解：<https://www.jianshu.com/p/b69bc629f476>；
    - 日志详解：<https://www.jianshu.com/p/c93e55b1c011>；
3.  部署：
    - TODO:MacOS 开发环境安装：
    - TODO:Windows 开发环境安装：；
    - TODO:Linux 开发环境安装：；
    - Docker-Compose 生产环境安装配置：[Demo]()；
    - 默认端口：
      - 11800：和 Skywalking 通信的 gRPC 端口；
      - 12800：和 Skywalking 通信的 HTTP 端口；
      - 8080：管理后台端口；
4.  使用：
    - TODO:MacOS 开发环境启动/关闭服务：
    - TODO:Windows 开发环境启动/关闭服务：；
    - TODO:Linux 开发环境启动/关闭服务：；

<br>

#### Seata

---

1.  引用：
    - [文档]<>；
    - [Github]<>；
    - [文档]<>；
2.  摘要：
    - 阿里开源的数据库连接池组件；
3.  部署：
    - MacOS 开发环境安装：
      ```
      brew install kafka
      ```
    - TODO:Windows 开发环境安装：；
    - TODO:Linux 开发环境安装：；
4.  使用：
    - `pom.xml`：[Demo]()；
    - `application.yml`：[Demo]()；
    - `Application.java`：[Demo]()；
    - 监控`pom.xml`：[Demo]()；

<br>

#### 安全

---

1. 概念：<http://www.freebuf.com/articles/web/39234.html>；
2. DDOS：分布式饱和攻击（找很多肉鸡一起发动流量轰炸），防御原则：
   - 日志要打 ip 和 user agent；
   - iptables 将 ip 加入黑名单；
   - nginx 将 ip 加黑名单；
   - 服务器前顶一个 CDN，后端使用弹性 IP；
3. XSS：跨域脚本攻击（提交 js 脚本做坏事），参考：<https://segmentfault.com/a/1190000022348467>，防御原则：
   - 将能被转为 html 片段的内容，在转换时使用 innerText 而非 innerHTML；
   - 不相信用户的输入，执行严格的内容过滤（如 apache 的 commons-text、jsoup）；
   - 服务器端种 cookie 的时候要加上 cookie.setHttpOnly(true)；
4. CSRF：跨域请求伪造（坏网站利用别人的 cookie 访问别人的接口），用 XSS 来实现 CSRF 的叫做 XSRF（连坏网站都不用建了），防御原则：
   - 严格执行 RESTful 提交规范；
   - token 验证或者双重 cookie 验证以及配合 samesite cookie；
   - spring security4 自带 csrf token
   - 开启 CSP 策略（TODO），参考：<https://developer.mozilla.org/zh-CN/docs/Web/HTTP/CSP>；
   - 参考：<https://zhuanlan.zhihu.com/p/46592479>；

<br>

#### 鉴权系统的演化

---

1. 演化过程：本地 session -> 粘性 session -> 集中式 session -> 分布式 token -> 网关校验 token -> JWT；

<br>

#### SSO

---

1. 从集中式 session 开始，就能天然实现 SSO（但是依赖于域名配置）；

<br>

#### JWT

---

1. 参考：<https://zhuanlan.zhihu.com/p/64233144>；
   1. 优点：简单可依赖性，防篡改，减轻 auth 服务的压力；
   2. 缺点：无保密性（仅简单 base64），传输开销大（若流量敏感慎选），需要服务端引入秘钥，无法单独修改过期时间；
   3. 建议使用 RSA 方式加密，这样只有 auth 微服务持有私钥；
2. JWT 格式：Base64UrlEncode(Header).Base64UrlEncode(Payload).Base64UrlEncode(Signature)，各模块及 Claims 的含义： 1. Header 格式类似：{ 2. "typ":"JWT",//类型；
   "alg":"HS256"//校验算法；
   } 3. Payload 格式类似：{ 4. "iss":"<https://auth.company.com"//颁发令牌的站点>；
   "iat":"1234567890"//签发时间；
   "exp":"1234567890"//过期时间；
   "aud":"dev.company.com"//令牌目标站点；
   "sub":"myself@company.com"//用户名；
   "nbf":"1234567890"//NotBefore，在这个时间点之前无效；
   "jti":"xxxxxxxx"//JWT 唯一 ID；
   "xxx":"xxxx"//其它自定义字段；
   } 5. Signature=HMACSHA256(Base64UrlEncode(Header)+"."+Base64UrlEncode(Payload)+"."+secret)

<br>

#### OAuth2.0

---

1. 主流标准的第三方校验机制，用高权限账户密码生成低权限令牌供第三方使用，
   1. 概念：<http://www.ruanyifeng.com/blog/2019/04/oauth_design.html>；
   2. 进阶：<https://zhuanlan.zhihu.com/p/29345083>；
   3. 一般平台都会设定第三方白名单（client id 列表），也就是需要授权过程，所以第三方一般要先请求授权码再请求令牌；
   4. 客户端拿到令牌之后的使用方式是在请求头加入令牌，例如：curl -H “Authorization: Bearer [令牌]” <https://api.company.com>；
   5. 常用的做法是发放一个 refresh token，用来控制客户端自动更新令牌的权限；
2. 鉴权：RBAC（Role Base Access Control）：

<br>

#### LDAP

---

1. 作为企业用户，内部会有很多需要验证的后台系统、管理平台，LDAP 很适合解决员工入职离职升迁等问题；
2. OpenLDAP；

<br>

#### 微服务架构下的认证系统设计

---

1.  原理说明，参考：<http://blueskykong.com/2017/10/19/security1/>；
2.  Spring Security / Shiro
    - Security 在 Springboot 上下文中有更便捷的表现（比如 csrf token）；
    - Shiro 简单易上手，控制粒度比 Security 稍大；
    - 本例选用 Spring Security;

<br>

#### 集中日志监控

---

1. ELK、EFK
   - 本例选用 Fluentd -> Kafka -> Logstash -> ElasticSearch -> Kibana；
   - 拓扑选择的思考：<https://blog.csdn.net/weixin_39972996/article/details/111204895>；
2. Docker 方式安装：
   - Docker-Compose 文件见 Deploy 工程

<br>

#### Java 系统中的日志

---

1. 运行日志：
   - slf4j 和 commons-logging 是抽象接口，Logging/Log4j/Logback 是实现框架，Logging 是 JDK 自带；
   - Log4j / LogBack
     - Log4j 和 Logback 是同一个作者，后者是性能升级版；
     - 本例选用 slf4j+Logback，其余选项是 slf4j+log4j 或者 commons-logging+log4j；
2. 审计日志：
   - 审计日志建议使用结构化日志；
   - StructLog4j（TODO）；

<br>

#### Elasticsearch

---

1. 官网：<https://www.elastic.co/cn/>；
2. 文档：<https://www.elastic.co/guide/cn/elasticsearch/guide/current/index.html>；
3. 参考：<https://www.bootwiki.com/elasticsearch/elasticsearch-getting-start.html>；
4. Brew 方式安装：
   - brew tap elastic/tap
   - brew install elastic/tap/elasticsearch-full
5. 此时应修改（或链接）配置文件，初始路径如下：
   - 数据路径：/usr/local/var/lib/elasticsearch/
   - 日志路径：/usr/local/var/log/elasticsearch/
   - 插件路径：/usr/local/var/elasticsearch/plugins/
   - 配置路径：/usr/local/etc/elasticsearch/
6. 启动/关闭服务：brew services start/stop elasticsearch
7. 配置集群：<https://www.cnblogs.com/aqicheng/p/14262484.html>；
8. 安装 elasticsearch-head：
   - Github：<https://github.com/mobz/elasticsearch-head>；
   - cd elasticsearch-head && vim Grunfile.js
   - 配置合适的端口号比如 9003；
   - npm install
   - npm run start（前台启动）
   - 此时就能在本地浏览器访问了：<http://localhost:9003/>；
   - 首先连接到 elasticsearch，然后可以进行查询；
9. 权限管理：<https://zhuanlan.zhihu.com/p/91821035>；
10. 数据访问：
    - RESTful：http://localhost:{es 端口}/{index}/{type}/[{id}]
    - 其中 index、type 非空，id 如果不提供，系统会自动生成 uuid；
    - 不带 id 的情况应该用 POST
    - 一个 es 实例可以看做数据库，一个 index 可以看做表，一个 document 可以看做记录；
    - PUT 一条数据时会生成\_version:1
    - PUT/POST 进行修改时\_version 会+1
    - PUT 是幂等的全量修改，POST 只修改相关字段；
    - GET 进行查询；
    - DELETE 进行删除；
      - type 不能删除和查询；
11. 搜索：
    - POST：<http://localhost:{es 端口}/\_search/ # 搜索所有 index 和 type>；
    - POST：<http://localhost:{es 端口}/{index}/\_search/ # 搜索某个索引的所有 type>；
    - POST：<http://localhost:{es 端口}/{index}/{type}/\_search/ # 搜索某个 type 的所有文档>；
12. 索引操作；
    - POST：<http://localhost:{es 端口}/{index}/\_close/ # 关闭索引>；
    - POST：<http://localhost:{es 端口}/{index}/\_open/ # 打开索引>；

<br>

#### Kibana

---

1. Brew 方式安装：
   - brew tap elastic/tap
   - brew install elastic/tap/kibana-full
2. 此时应修改（或链接）配置文件，初始路径如下：
   - 数据路径：/usr/local/opt/kibana/data/
   - 插件路径：/usr/local/opt/kibana/plugin/
   - 配置路径：/usr/local/etc/kibana/
3. 启动/关闭服务：brew services start/stop elasticsearch
4. 使用手册：
   - 参考：<https://blog.csdn.net/tian_111222333/article/details/102916998>；
   - 参考：<https://www.cnblogs.com/chenqionghe/p/12503181.html?utm_source=tuicool&utm_medium=referral>；
5. 使用步骤：
   - 建立 Index Pattern；
   - 从 Discovery 查看；
6. 安装插件：TODO；

<br>

#### Zookeeper

---

1. Brew 方式安装：
   ```
   brew install zookeeper
   ```
2. 此时应修改（或链接）配置文件，初始路径如下：
   - 配置路径：/usr/local/etc/zookeeper/
3. 启动/关闭服务：brew services start/stop zookeeper
4. 因为选举机制，zookeeper 集群要配置奇数台；
5. 原理：
   - Zookeeper 可组建 Server 集群，每个 Server 可以管理多个 Client；
   - Zookeeper 启动时从单数的 Server 中选举 Leader（Paxos 协议），其它是 Follower；
   - Client 可以创建节点用以保存数据，有四种节点：
     - PERSISTENT（默认持久节点）：Client 与 Server 断开连接后节点还在；
     - PERSISTENT_SEQUENTIAL（顺序持久节点）：并且有时间编号；
     - EPHEMERAL（临时节点）：Client 与 Server 断开连接后节点自动删除；
     - EPHEMERAL_SEQUENTIAL（临时顺序节点）：删除前有时间编号；
   - 顺序节点适用于分布式锁、分布式选举等场景；
   - 临时节点适用于心跳监控、服务发现等场景；
   - Zookeeper 实现分布式锁使用临时顺序节点，但因为需要增删节点，效率一般；
6. 如果配置文件中开放 admin.serverport，通过 8080 端口可以访问到一个比较基础的 RESTful 管理界面；
7. 集群化配置：
   - 参考：<https://zhuanlan.zhihu.com/p/121728783>；
   - Zookeeper 的 zoo.cfg 配置文件有几处不同：
     - 每个节点分别使用不同 broker.id；
     - 每个节点的 listeners 监听自己的{ip}:{Kafka 端口}；
     - 每个节点的 zookeeper.connect 用都好连接所有节点的{ip}:{Zookeeper 端口}；
8. 命令行：zkCli.sh
   - ls /brokers/ids/ # 查看 broker 的 id；
   - get /brokers/ids/0 # 查看 listener 具体信息；

<br>

#### Kafka

---

1. 参考：<https://www.cnblogs.com/TM0831/p/13355383.html>；
2. Brew 方式安装：
   ```
   brew install kafka
   ```
3. 此时应修改（或链接）配置文件，初始路径如下：
   - 配置路径：/usr/local/etc/kafka/
4. Kafka 依赖 Zookeeper，利用 zookeeper 在 Producer、Consumer、Broker 之间建立关系和连接；
5. Kafka 自带一个 Zookeeper 的配置文件，可用可不用；
6. 启动/关闭服务：
   - 先启动 Zookeeper；
   - brew services start/stop kafka
7. 原理：
   - 一个 Kafka 服务器就是一个 Broker，多个 Broker 组成集群；
   - 消息是带时间戳的 key-value 对；
   - 以 Topic 分类传递消息，逻辑上 Topic 是个队列，物理上是分散在多个 Broker 上的多个 Partition（目录），这是为了实现高可用，方便集群扩展；
   - 单个分区内的消息时有序的；
   - 为了高可用，可以指定 Partition 的多个副本（Replication），副本间使用 zookeeper 的 leader/follower 机制同步；
   - ACK 机制：
     - 每个 Partition 收到消息后要向 Producer 发送 ACK 确认消息收到；
     - 在右 Replica 的情况下，可由用户决定三种策略：
       - ACK=0，Producer 不用等 ACK，可能丢数据；
       - ACK=1，Producer 会等 ACK，Leader 接到数据就发 ACK，Leader 挂掉可能丢数据；
       - ACK=-1，Producer 会等 ACK，Leader 同步完 Follower 之后才发 ACK，Leader 挂掉可能导致数据重复；
8. 使用流程：
   - 启动 server；
   - 创建 Topic，指定 zookeeper 端口地址，使用 1 个副本，1 个分区：
     - kafka-topics --zookeeper localhost:52040 --create --replication-factor 1 --partitions 1 --topic mytopic
   - 删除 Topic，指定 zookeeper 端口地址，使用 1 个副本，1 个分区：
     - kafka-topics --zookeeper localhost:52040 --delete --topic test
   - 查看 Topic 列表：
     - kafka-topics --zookeeper localhost:52040 --list
   - 使用命令行生产者造数据：
     - kafka-console-producer --broker-list localhost:52051,localhost:52052,localhost:52053 --topic test
   - 使用命令行消费者取数据：
     - kafka-console-consumer --bootstrap-server localhost:52051,localhost:52052,localhost:52053 --topic test --from-beginning
9. 集群化配置：
   - 在奇数节点上分别启动完全一样的 Zookeeper；
   - Kafka 的 server.properties 配置文件有几处不同：
     - 每个节点分别使用不同 broker.id；
     - 每个节点的 listeners 监听自己的{ip}:{Kafka 端口}；
     - 每个节点的 zookeeper.connect 用都好连接所有节点的{ip}:{Zookeeper 端口}；
   - 建立 topoic 的时候可充分使用集群高可用性：
     - kafka-topics --zookeeper localhost:52041,localhost:52042,localhost:52043 --create --replication-factor 3 --partitions 3 --topic test
10. Kafka Connect：
    - Kafka 提供的与其它系统对接数据的工具，包括导入端 Source 和导出端 Sink；
    - 有 4 个工作组件：
      - Connectors：连接器；
      - Tasks：读写器；
      - Workers：运行 Connectors 和 Workers 的进程；
      - Converters：数据转换器；
    - 有两种工作模式：
      - standalone：单进程；
      - distributed：分布式；
    - Kafka Mirror Maker2：
      - Kafka 的双热备部署工具，基于 Kafka Connect；
      - Kafka2.4 之前有一个版本叫 Kafka Mirror Maker1，缺陷太多没有被推广；

<br>

#### Filebeat

---

1. Brew 方式安装：
   ```
   brew install filebeat
   ```
2. 配置文件解析：<https://www.cnblogs.com/zsql/p/13137833.html>；
3. 此时应修改（或链接）配置文件，初始路径如下：
   - 配置路径：/usr/local/etc/filebeat/
4. 启动/关闭进程：
   - filebeat -e -c /opt/publish/filebeat_7.11/etc/filebeat_nginx_1.19.yml -d "publish"
     - -e：标准输出；
     - -c：指定配置文件；
     - -d：指定调试器，用逗号分隔，如：publish
5. 发往 kafka：
   - 见配置文件；
   - 验证：
     - kafka-topics --list --zookeeper 127.0.0.1:{zk 端口}
     - kafka-console-consumer --bootstrap-server 127.0.0.1:{kafka 端口} --topic nginx --from-beginning

<br>

#### Logstash

---

1. Brew 方式安装：
   ```
   brew install logstash
   ```
2. 此时应修改（或链接）配置文件，初始路径如下：
   - 配置路径：/usr/local/etc/logstash/
     - 注意有时候
3. 配置文件中文参考：
   - 参考：<https://blog.csdn.net/qq_23435961/article/details/108118655>；
   - 参考：<https://blog.csdn.net/u010281174/article/details/97178567>；
   - 参考：<https://www.cnblogs.com/jiashengmei/p/8857053.html>；
   - 如果缺省参数启动（比如 brew services start logstash），会默认读取 pipeline.yml 作为入口配置；
4. 启动/关闭服务：brew services start/stop logstash
5. 原理：
   - 数据处理分为：
     - 多个 Input -> Queue；
     - Queue -> 多个 Pipeline；
       - 每个 Pipeline 包括 Batcher -> Filter -> Output；
     - Queue 可配置使用内存（InMemory）或者持久存储（PQ），建议 PQ；
     4.
   - 以 Pipeline 做为管理三阶段流程，中间数据以 Event 进行封装；

<br>

#### 集中异常监控

---

1. Sentry，类似友盟的在线异常分析；

<br>

#### SkyWalking

---

3. 引用：
   - 官网：<http://skywalking.apache.org>；
   - Github：<https://github.com/apache/incubator-skywalking>；
4. 摘要：
   - 用于实现调用链跟踪和简单的日志跟踪；
5. 部署：

   - 下载 APM 和 Agent（Java/SpringBoot）：官网下载地址：<https://skywalking.apache.org/downloads/>；
   - C#/DotNet 工程下载 DotNet 探针 DotNet 探针地址：Github：<https://github.com/SkyAPM/SkyAPM-dotnet>；
   - TODO:配置 APM：[Demo]()；
   - TODO:运行 APM：[Demo]()；
   - TODO:非侵入式自定义 Tag：
   - 侵入式自定义 Tag：

     ```

     ```

   - 由于 SpringCloudGateway 是基于 WebFlux 来实现的，需要进到 skywalking 的 agent 目录，将 optional-plugins 目录底下的以下两个 jar 包复制到 plugins 目录
     ```
     cp optional-plugins/apm-spring-webflux-5.x-plugin-8.11.0.jar plugins/
     cp optional-plugins/apm-spring-cloud-gateway-3.x-plugin-8.11.0.jar plugins/
     ```

6. 使用：

   - TODO:开发环境启动/关闭服务：[Demo]()；
   - 添加三个启动变量：[Demo]()；
     ```
     -javaagent:{Agent插件完整路径} # 例如 /opt/publish/skywalking-agent_8.11/skywalking-agent.jar
     -Dskywalking.agent.service_name={服务名} # 例如 hello-svc-8020
     -Dskywalking.collector.backend_service={APM的IP:端口} # 例如127.0.0.1:11800
     ```
