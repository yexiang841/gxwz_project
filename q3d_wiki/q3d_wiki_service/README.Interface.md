## 《微服务工作笔记》-- 数据接口（Interface）

<br>

#### 介绍

---

- 项目文档记录了笔者工作积累的云原生微服务技术笔记，以及元宇宙、数字孪生、程序化建模工作学习笔记；
- 项目包含一套元宇宙建模方法论和工程，采用 QGIS -> Blender / Houdini -> Unreal / Unity 的方式实现数字孪生虚拟城市；
- 项目包含一个 Unreal 客户端工程和一个 Unity 客户端工程（建设中）；
- 项目包含一个 Cesium 网页端工程，实现 WebGIS BI 展示；
- 项目包含一个 Vue.js / ElementPlus / TS 网页端工程，实现 Admin 管理后台；
- 项目包含一个 Java / SpringBoot 微服务工程和一个 C# / DotNet 微服务工程（建设中），共用网关和服务注册中心（Nacos），可以协同工作；
- 项目包含一个 Publish 部署工程，采用 Docker / Kubernetes / Jenkins 方式进行服务部署；
- 程序语言涉及 C++ / Java / C# / Python / TS / Shell / VEX；
- 错漏之处恳请读者不吝指正，转载望注明出处：<https://gitee.com/yexiang841/q3d_project/>；
- Copyright 叶湘 ( weixin:yexiang841, email:yexiang841@qq.com ), Shitouren Studio. All Rights Reserved.

<br>

#### 微服务工作学习笔记

---

- [《微服务工作笔记》-- 部署说明（Deploy）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_service/README.Deploy.md)
- [《微服务工作笔记》-- 架构选型（Selection）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_service/README.Selection.md)
- `>>>>` [《微服务工作笔记》-- 数据接口（Interface）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_service/README.Interface.md)
- [《微服务工作笔记》-- 数据持久化（Persistence）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_service/README.Database.md)
- [《微服务工作笔记》-- 分布式组件（Distributed）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_service/README.Distributed.md)
- [《微服务工作笔记》-- 云原生（CloudNative）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_service/README.CloudNative.md)
- [《微服务工作笔记》-- 持续集成部署（DevOps）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_service/README.DevOps.md)
- [《微服务工作笔记》-- 系统安全（Security）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_service/README.Security.md)
- [《微服务工作笔记》-- 数据治理（Governance）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_service/README.Governance.md)
- [《微服务工作笔记》-- Java 微服务（SpringBoot）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_service/README.SpringBoot.md)
- [《微服务工作笔记》-- DotNet 微服务（DotNet）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_service/README.DotNet.md)
- [《微服务工作笔记》-- 网页端工程（Frontend）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_service/README.Frontend.md)

<br>

#### 本章摘要

---

1.  更新状态：持续整理上传中；
2.  最后更新：2021.10.09；
3.  本章记录网关和接口的设计理念；

<br>

#### TODO:Nginx

---

1.  引用：
    - 官网：<http://nginx.org>；
2.  摘要：
    - ；
3.  部署：
    - MacOS 开发环境安装：
      ```
      brew install nginx
      ```
    - TODO:Windows 开发环境安装：；
    - TODO:Linux 开发环境安装：；
4.  使用：
    - MacOS 开发环境启动/关闭服务：
      ```
      brew services start nginx
      brew services stop nginx
      ```
    - TODO:Windows 开发环境启动/关闭服务：；
    - TODO:Linux 开发环境启动/关闭服务：；

<br>

#### TODO:OpenResty

---

1.  引用：
    - 官网：<http://openresty.org/cn/>；
2.  摘要：
    - ；
3.  部署：
    - MacOS 开发环境安装：
      ```
      brew tap openresty/brew
      brew install openresty
      ```
    - TODO:Windows 开发环境安装：；
    - TODO:Linux 开发环境安装：；
4.  使用：
    - MacOS 开发环境启动/关闭服务：
      ```
      brew services start openresty
      brew services stop openresty
      ```
    - TODO:Windows 开发环境启动/关闭服务：；
    - TODO:Linux 开发环境启动/关闭服务：；

<br>

#### Kong

---

1.  引用：
    - 官网：<https://konghq.com/>；
2.  摘要：
    - 网易实践：<https://zhuanlan.zhihu.com/p/242260216>；
    - 教程：<https://www.jianshu.com/p/a68e45bcadb6/>；
3.  部署：
    - MacOS 开发环境安装：
      ```
      brew tap kong/kong
      brew install kong
      ```
    - TODO:Windows 开发环境安装：；
    - TODO:Linux 开发环境安装：；
4.  使用：
    - MacOS 开发环境启动/关闭服务：
      ```
      brew services start kong
      brew services stop kong
      ```
    - TODO:Windows 开发环境启动/关闭服务：；
    - TODO:Linux 开发环境启动/关闭服务：；

<br>

#### Nacos

---

1.  引用：
    - 官网：<https://nacos.io/zh-cn/index.html>；
    - Github：<https://github.com/alibaba/nacos>；
    - 文档：<https://nacos.io/zh-cn/docs/what-is-nacos.html>；
2.  摘要：
    - Nacos 是阿里巴巴推出来的一个新开源项目，是一个更易于构建云原生应用的动态服务发现、配置管理和服务管理平台；
    - Nacos 与 Consul 功能类似，可以完全取代 Eureka / Config / Bus
3.  部署：
    - 定义接口：[Demo]()；
4.  使用：
    - 定义接口：[Demo]()；
    - 定义接口：[Demo]()；

<br>

#### TODO:Traefik

---

1.  引用：
    - 官网：<>；
    - Github：<>；
    - 文档：<>；
2.  摘要：
    - Traefik 部署在 K8S 上分为 DaemonSet 和 Deployment 两种方式，各有优缺点：
    - DaemonSet 能确定有哪些 node 在运行 Traefik，可以确定的知道后端 ip，但是不能方便的伸缩；
    - Deployment 可以更方便的伸缩，但是不能确定有哪些 node 在运行 Traefik，不能确定的知道后端 ip；
    - 所以一般部署两种不同类型的 Traefik:
      - 面向内部(Internal)服务的 Traefik，建议可以使用 Deployment 的方式，如 K3S 自带的 traefik 服务；
      - 面向外部(External)服务的 Traefik，建议可以使用 DaemonSet 的方式，如 K3S 自带的 svclb-traefik 服务；
3.  部署：
    - [Demo]()；
4.  使用：
    - [Demo]()；

<br>

#### TODO:Istio

---

1.  引用：
    - 官网：<https://istio.io/>；
    - 文档：<https://istio.io/zh/docs/concepts/what-is-istio/>；
    - Github：<https://github.com/istio/istio>；
    - 最佳教程：<https://www.servicemesher.com/istio-handbook/>；
    - 最佳教程前传：<https://jimmysong.io/istio-handbook/>；
2.  摘要：
    - Traefik 部署在 K8S 上分为 DaemonSet 和 Deployment 两种方式，各有优缺点：
    - DaemonSet 能确定有哪些 node 在运行 Traefik，可以确定的知道后端 ip，但是不能方便的伸缩；
    - Deployment 可以更方便的伸缩，但是不能确定有哪些 node 在运行 Traefik，不能确定的知道后端 ip；
    - 所以一般部署两种不同类型的 Traefik:
      - 面向内部(Internal)服务的 Traefik，建议可以使用 Deployment 的方式，如 K3S 自带的 traefik 服务；
      - 面向外部(External)服务的 Traefik，建议可以使用 DaemonSet 的方式，如 K3S 自带的 svclb-traefik 服务；
3.  部署：
    - 安装：
      - 1.6 版本以上官方已弃用 Helm 方式安装，原因待考究（TODO）参考：<https://istio.io/zh/docs/setup/install/helm/>；
      - 官方推荐先安装 Istioctl 工具，然后使用 Istioctl 安装 Istio；
4.  使用：
    - [Demo]()；
    - [Demo]()；

<br>

#### RESTful 接口设计

---

1. RESTful 规范：

   - HTTP 方法：
     - GET 查询数据/请求页面（幂等）
     - POST 提交数据（幂等）
     - PUT 全部更新资源（幂等），PUT / PATCH 和 POST 的核心区别是幂等操作；
     - PATCH 部分更新资源（非幂等）
     - DELETE 删除资源操作（幂等）
     - HEAD 仅仅是获得获取资源的部分信息（content-type、content-length），不返回具体资源
     - OPTIONS 用于 url 验证，验证接口服务是否正常
   - URI 规范：用资源名词而非操作动词，名词用复数（这一点往往要很严谨的架构师才能完全实现，比如要把登录抽象成为权限资源）；
   - REST 第三级（level3）Hypermedia
     - 概念：加上 link，里边有 rel（相关操作）、method（http 方法）和 href（其实是 uri），告知可能用到的关联操作的链接；
       - 参考：<https://zhuanlan.zhihu.com/p/30396391>；
       - 参考：<http://www.ruanyifeng.com/blog/2018/10/restful-api-best-practices.html>；
     - 但这会为攻击后台的过程制造便利，所以应该以安全防护到位为前提；
   - 理想状态下 REST 使用的 HTTP 状态码：
     - 1xx：中间状态临时响应，笔者不熟悉，API 也不需要；
     - 2xx：操作成功；
       - GET200（取得数据） / PUT200（更新成功） / PATCH200（部分更新成功）；
       - POST201（已生成了新资源）；
       - POST202（已受理，未立即完成）；
       - DELETE204（删除成功，已无该资源） / GET204（请求成功但无数据返回） / PUT204（请求成功但资源不存在无法更新）；
     - 3xx：重定向；
       - GET301（永久重定向，API 用不到）；
       - GET302 / 307（临时重定向，API 用不到），区别在于 307 不会从 POST 变为 GET；
       - 303（该资源在别的接口上）；
     - 4xx：客户端错误；
       - 400（服务端不理解请求，如果其它 4xx 无法描述，就用 400）；
       - 401（身份验证失败）；
       - 402（资源超期需要续费……）；
       - 403（身份验证成功但是权限不够）；
       - 404（资源不存在）；
       - 405（所使用的的 HTTP 方法不在权限之内）；
       - 410（资源已从这个地址转移）；
       - 415（客户端要求的返回格式不支持）；
       - 422（客户端上传的数据无法处理，通常表示格式没问题，但具体处理的时候语义有问题，例如附件不符合约定）； 10. 423（资源暂时被锁住，比如说订单已发货，不能修改）； 11. 429（客户端请求次数超限）；
     - 5xx：服务器错误；
       - 500（客户端请求有效，但服务器发生了错误）；
       - 501（以后会有这个借口，但现在还没实现…）；
       - 503（服务器暂时不可用，维护中）；
       - 504（服务器网关超时，很有可能是吞吐量的问题）；

2. REST 接口设计常见误区：<https://blog.csdn.net/weixin_44742132/article/details/116773023>；
3. REST 接口的缺陷：<https://www.zhihu.com/question/438825740/answer/1691981151>；

<br>

#### OpenFeign 接口

---

1.  引用：
    - 官网：<https://spring.io/projects/spring-cloud-openfeign>；
    - 参考：<http://events.jianshu.io/p/e4e51e30b111>；
    - 参考：<https://blog.csdn.net/lydms/article/details/124392399>；
2.  摘要：
    - Feign 是一个 HTTP 请求调用的轻量级框架，可以以 Java 接口注解的方式调用 HTTP 请求；
3.  部署：
    - 定义 FeignClient：[demo](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_service/springboot2_grpc_nacos/hello-api/src/main/java/com/q3d/demo/api/hello/client/feign/HelloWorldV1FeignInterface.java)；
    - 做一些配置：[demo](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_service/springboot2_grpc_nacos/hello-api/src/main/java/com/q3d/demo/api/hello/client/feign/HelloWorldV1FeignConfig.java)；
4.  使用：
    - 使用 FeignClient 实现客户端调用：[demo](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_service/springboot2_grpc_nacos/hello-api/src/main/java/com/q3d/demo/api/hello/client/feign/HelloWorldV1ClientFeign.java)；

<br>

#### gRPC 接口

---

1.  引用：
    - SpringBoot Starter：
      - 官网：<https://yidongnan.github.io/grpc-spring-boot-starter/zh-CN/>；
      - 参考：<https://cloud.tencent.com/developer/article/1800263>；
    - ProtoBuf：文档：<https://developers.google.com/protocol-buffers/docs/proto3>；
2.  摘要：
    - ；
3.  部署：
    - 定义 ProtoBuf：[demo](https://gitee.com/yexiang841/q3d_project/tree/master/q3d_service/springboot2_grpc_nacos/hello-api/src/main/java/com/q3d/demo/api/hello/proto)；
    - 使用插件将 ProtoBuf 编译为目标语言：[demo](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_service/springboot2_grpc_nacos/hello-api/pom.xml)；
4.  使用：
    - 继承编译结果，实现服务端接口：[demo](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_service/springboot2_grpc_nacos/hello-svc/src/main/java/com/q3d/demo/svc/hello/grpc/service/HelloWorldV1GrpcService.java)；
    - 继承编译结果，实现客户端调用：[demo](https://gitee.com/yexiang841/q3d_project/tree/master/q3d_service/springboot2_grpc_nacos/hello-api/src/main/java/com/q3d/demo/api/hello/client/grpc)；
    - 命令行工具 grpcurl：
      ```
      brew install grpcurl
      grpcurl -plaintext {host}:{port} list # 探测某个地址端口下可用的服务
      grpcurl -plaintext {host}:{port} list {Service} # 探测某个服务下可用的接口
      grpcurl -plaintext {host}:{port} describe {Service} # 详细描述某个服务下可用的接口
      grpcurl -plaintext {host}:{port} describe {Message} # 详细描述某个数据类型
      ```

<br>

#### Dubbo 接口

---

1.  引用：
    Apache Dubbo
    - 官网：<https://dubbo.incubator.apache.org/zh/>；
    - 文档：<https://dubbo.gitbooks.io/dubbo-user-book/content/>；
    - demo：<https://github.com/apache/dubbo-spring-boot-project>；
    - 参考：<https://www.jianshu.com/p/be534104785b>；
2.  摘要：
    - Dubbo 是阿里巴巴开发的，近几年捐献给了 Apache 基金会继续孵化，因此目前可以选择 Apache Dubbo；
    - Dubbo 目前暂时只支持 Java、Golang 语言，不支持 C#，因此暂时不予选用；
3.  部署：
    - 定义接口：[Demo]()；
4.  使用：
    - 定义接口：[Demo]()；
    - 定义接口：[Demo]()；

<br>

#### GraphQL 接口

---

1.  GraphQL 接口框架：

    - 概念：<https://www.jianshu.com/p/65e28516a1ad>；

<br>

#### SSL 自签名证书和私有 CA 证书

---

1.  用于在开发环境对测试域名测试 <https 请求（如https://www.company.local）>；
2.  参考：<https://blog.csdn.net/nklinsirui/article/details/89432430>；
3.  因本地（集群入口之前）用 Nginx 作为反向代理，所以证书可以存放在 Nginx 目录之下；
4.  cd /opt/publish/nginx && mkdir certs；
5.  生成私钥（密码随便，等下会删掉）：openssl genrsa -des3 -out server.pass.key 2048；
6.  删除私钥中的密码：openssl rsa -in server.pass.key -out server.key；
7.  生成 CSR（证书签名请求）：

    - openssl req -new -key server.key -out server.csr -subj "/C=CN/ST=Guangxi/L=Beihai/O=company/OU=dev/CN=company.local"；
    - C=国家、ST=省、L=城市、O=组织、OU=部门、CN=域名；

8.  生成自签名证书（有效期十年）：openssl x509 -req -days 3650 -in server.csr -signkey server.key -out server.crt；
9.  自签名证书存在安全隐患，黑客可以仿照以上流程伪造证书进行数据劫持，不能用于预生产环境（UAT）和生产环境（PROD）；
10. Rancher 官方将上述指令集成了一个生成证书脚本，参考：<https://rancher2.docs.rancher.cn/docs/installation/options/self-signed-ssl/_index/>；

    - 可直接通过该脚本生成私有 CA 证书：create_self_signed_cert.sh --ssl-domain=www.company.local. --ssl-trusted-domain=localhost --ssl-trusted-ip=127.0.0.1,0.0.0.0 --ssl-size=2048 --ssl-date=3650；
    - cacerts.pem 和 cakey.pem 可用于 Rancher；
    - tls.crt 和 tls.key 用上述私有 CA 进行了签名，可用于 Nginx / Apache / K8S-Dashboard 等；

<br>

#### IPv4

---

1.  地址分类：
    - A 类（1.0.0.0 到 126.255.255.255）：由 ENIC 分配，主要分配给具有大量主机而局域网络数量较少的大型网络；
    - B 类（128.0.0.0 到 191.255.255.255）：由 InterNIC/APNIC 分配，一般用于国际性大公司和政府机构；
    - C 类（192.0.0.0 到 223.255.255.255）：用于一般小公司校园网研究机构等；
    - D 类（224.0.0.0 到 239.255.255.255）：用于特殊用途. 又称做广播地址；
    - E 类（240.0.0.0 到 247.255.255.255）：暂时保留；
    - 特殊地址：
    - 本机：172.0.0.1；
    - 路由器常用：192.168.x.x / 10.0.0.x；

<br>

#### IPv6 技术的意义

---

1.  国家正在大力推进的 IPv6 相关工作，包括 IPv6 技术创新，对发展安全自主的下一代互联网具有重大意义；
2.  IPv6 的提出，最重要的目的就是解决公网 IPv4 耗尽的问题，现代物联网已经进入了高速发展阶段，地址需求非常大，根据预测，2025 年，物联网的连接数将超过 270 亿，迫切需要 IPv6；
3.  IPv6 网络也为 5G、数据中心等新型基础设施建设奠定基础，更为未来发展大规模的工业互联网开拓网络空间，搭建基础环境；
4.  IPv6 协议的设计充分考虑了更高的效率和安全、扩展等方面的支持，可以说，IPv6 是未来网络发展的大趋势；

<br>

#### IPv6 相比 IPv4 的优势

---

1.  IPv4 的 32 位地址已经耗竭，IPv6 采用 128 位的地址长度，拥有更大的地址空间；
2.  IPv6 的地址空间足够大，甚至可以为地球上存在的每一个物件分配 IP 地址，因此 IPv6 协议是实现物联网的基础设施；
3.  IPv6 具有更高的安全性，通过 IPv6 协议的安全机制，可对网络层的数据进行加密，对 IP 报文进行校验，提高了数据的安全性；
4.  IPv6 传输速度更快，IPv6 的地址分配，一开始就遵循聚类的原则，这大大减小了路由器中路由表的长度，提高了路由器转发数据包的速度；
5.  IPv6 加入了对自动配置的支持，这是对 DHCP 协议的改进和扩展，使得网络（尤其是局域网）的管理更加方便和快捷；
6.  IPv6 拥有更好的头部格式，简化和加速了路由选择过程，提高了效率。具体的技术指标包括：
    - IPv6 的报文头部对比起 IPv4，更精简了，字段更少了；
    - IPv6 的报文头部是定长（固定为 40 字节），IPv4 报文头部是变长的。这个意味着，写代码处理 IPv6 数据报文的效率会提高很多；
    - IPv6 的报文头部取消了校验和字段。取消这个字段也是对 IPv4 协议的一个改进。

<br>

#### IPv6 的发展现状

---

1.  据国外权威机构统计数据显示，欧美国家的 IPv6 部署率已超过 50%， Google、Facebook 等全球排名靠前的网站已经全面支持 IPv6；
2.  我国 IPv6 地址申请量保持较快增长，目前我国 IPv6 地址资源总量居全球第一位；
3.  目前我国 IPv6 部署已从主要着力于呼吁进行 IPv6 改造，到了同时重视已完成改造的企业系统或网站在安全防护、加速访问等方面需求的阶段：
4.  所以，现在我国处于 IPv6 改造升级的关键时期，无论现在用的是什么网络，都不可能绕过 IPv6 的部署；
5.  目前在大众生活中还是比较少接触和使用 IPv6，个中原因是非常的复杂：
    - 有技术上障碍，因为 IPv6 和 IPv4 是两个完全不兼容的协议（在极少数的特定场景可以实现兼容），如果要从支持 IPv4 升级到 IPv6，无论是应用程序用客户端、服务器程序端、路由器等等，都要同时支持 IPv6 才能解决问题，这个的升级改造需要花费的成本是较大的。
    - 许多运营商和互联网服务商，需要重视用户的体验，不能强制客户更新换代硬件设备和软件以支持 IPv6，因此会有大量旧系统需要兼容；
    - 许多民营企业更愿意选择利用现有技术降低 IPv4 地址耗尽带来的压力，例如 NAT 的广泛应用，就是 IPv6 推广使用的一个重要的“障碍”。

<br>

#### IPv6 的技术架构

---

1.  图-IPv6 报文格式：
2.  Linux 内核中，IPv6 协议栈与 IPv4 协议栈并行关系。IPv6 和 IPv4 完全是两套不一样的代码实现。IPv6 完整的协议栈逻辑模块包括：
    - 网络层 IPv6，核心逻辑：IPv6 路由子系统；
    - 传输层 TCP/UDP 实现：TCPv6、UDPv6；
    - 控制报文协议 ICMPv6；
    - 邻居子系统的实现：邻居发现协议 NDP（对应于 IPv4 里面的 ARP 协议）；
    - 其他高级实现（IPv6 NAT、IPv6 隧道、iPv6 IPSec 等）；
    - 图-Linux 内核双栈架构

<br>

#### IPv6 的过渡技术

---

1.  由上所述，IPv4 升级到 IPv6 肯定不会是一蹴而就的，需要经历一个十分漫长的过渡阶段。现阶段，就出现了 IPv4 慢慢过渡到 IPv6 的技术（或者叫过渡时期的技术）；
2.  过渡技术要解决最重要的问题就是，如何利用现在大规模的 IPv4 网络进行 IPv6 的通信。 目前主要有三种实现 IPv4 到 IPv6 的过渡技术：

    - 双栈技术；
    - 隧道技术；
    - 转换技术（有一些文献叫做翻译技术）；

3.  双栈技术：

    - 通信节点同时支持 IPv4 和 IPv6 双栈。例如在同一个交换机下面有 2 个 主机节点，2 个节点都是 IPv4/IPv6 双栈，节点间原来使用 IPv4 上的 TCP/UDP 协议通信传输，现在需要升级为 IPv6 上的 TCP/UDP 传输。由于 2 个节点都支持 IPv6，那只要修改应用程序为 IPv6 的 Socket 通信即可；
    - 上面的例子在局域网通信的改造是很容易的。但是在广域网，2 个节点间往往经过多个路由器，按照双栈技术的部署要求，之间的所有节点都要支持 IPv4/IPv6 双栈，并且都要配置了 IPv4 的公网 IP 才能正常工作，这里就无法解决 IPv4 公网地址匮乏的问题。因此，双栈技术一般不会直接部署到网络中，而是配合其他过渡技术一起使用，例如在隧道技术中，在隧道的边界路由器就是双栈的，其他参与通信的节点不要求是双栈的。

4.  隧道技术：

    - 当前的网络是以 IPv4 为主，因此尽可能地充分利用 IPv4 网络进行 IPv6 通信是十分好的手段之一，隧道技术就是这样的一种过渡技术；
    - 隧道将 IPv6 的数据报文封装在 IPv4 的报文头部后面（IPv6 的数据报文是 IPv4 的载荷部分），IPv6 通信节点之间传输的 IPv6 数据包就可以穿越 IPv4 网络进行传输。隧道技术的一个很重要的优点是透明性，通过隧道进行通信的两个 IPv6 节点（或者节点上的应用程序）几乎感觉不到隧道的存在；
    - 根据隧道的出口入口的构成，隧道可以分为路由器-路由器，主机-路由器隧道、路由器-主机、主机-主机隧道等类型。

5.  转换技术（有一些文献叫做：翻译技术）

    - 在很长期一段时间内，IPv4 网络还是主流的情况下，IPv4 到 IPv6 的过渡是十分漫长的，因此也需要解决 IPv6 节点与 IPv4 节点通信的问题，协议转换技术可以用来解决这个问题；
    - 协议转换技术根据协议在网络中位置的不同，分为网络层协议转换、传输层协议转换和应用层协议转换等。协议转换技术的核心思路就是在 IPv4 和 IPv6 通信节点之间部署中间层，将 IPv4 和 IPv6 相互映射转换。

<br>

#### IPv6 过渡技术的实现方案

---

1.  可行性方案一（无需设备）
    - 实现目标：不修改代码，不改造业务，不改造现有设备；
    - 适用方案：云转换服务方案；
    - 实现形式：用户购买云转换服务，原 IPv4 网站不需要改造、不需要修改代码，不影响原有业务正常运行基础上，直接支持 IPv6 连接访问，投资少，见效快。适合升级改造网站规模较小的用户，只需购买服务，并把 DNS 的 A 记录指向云转换平台，即可完成升级改造工作。
2.  可行性方案二（软硬一体）
    - 实现目标：不改变现有配置，实现网络层及应用层 IPv4 与 IPv6 平滑过渡，自由互通；
    - 适用方案：软硬一体化渐进式方案；
    - 实现形式：通过硬件设备和软件系统（包括 IPv6 升级改造设备、IPv4/IPv6 地址转换系统等）的相融，对网站进行渐进式升级：保持现有配置不变，实现应用支持 IPv6 连接访问，并在其首页标示该应用已支持 IPv6；总体规划，分步实施，实现网络层及应用层翻译，在网络演进的后期，支持 IPv4 用户对 IPv6 互联网门户网站的访问。
