## 《微服务工作笔记》-- Java 微服务（SpringBoot）

<br>

#### 介绍

---

- 项目文档记录了笔者工作积累的云原生微服务技术笔记，以及元宇宙、数字孪生、程序化建模工作学习笔记；
- 项目包含一套元宇宙建模方法论和工程，采用 QGIS -> Blender / Houdini -> Unreal / Unity 的方式实现数字孪生虚拟城市；
- 项目包含一个 Unreal 客户端工程和一个 Unity 客户端工程（建设中）；
- 项目包含一个 Cesium 网页端工程，实现 WebGIS BI 展示；
- 项目包含一个 Vue.js / ElementPlus / TS 网页端工程，实现 Admin 管理后台；
- 项目包含一个 Java / SpringBoot 微服务工程和一个 C# / DotNet 微服务工程（建设中），共用网关和服务注册中心（Nacos），可以协同工作；
- 项目包含一个 Publish 部署工程，采用 Docker / Kubernetes / Jenkins 方式进行服务部署；
- 程序语言涉及 C++ / Java / C# / Python / TS / Shell / VEX；
- 错漏之处恳请读者不吝指正，转载望注明出处：<https://gitee.com/yexiang841/q3d_project/>；
- Copyright 叶湘 ( weixin:yexiang841, email:yexiang841@qq.com ), Shitouren Studio. All Rights Reserved.

<br>

#### 微服务工作学习笔记

---

- [《微服务工作笔记》-- 部署说明（Deploy）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_service/README.Deploy.md)
- [《微服务工作笔记》-- 架构选型（Selection）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_service/README.Selection.md)
- [《微服务工作笔记》-- 数据接口（Interface）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_service/README.Interface.md)
- [《微服务工作笔记》-- 数据持久化（Persistence）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_service/README.Database.md)
- [《微服务工作笔记》-- 分布式组件（Distributed）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_service/README.Distributed.md)
- [《微服务工作笔记》-- 云原生（CloudNative）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_service/README.CloudNative.md)
- [《微服务工作笔记》-- 持续集成部署（DevOps）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_service/README.DevOps.md)
- [《微服务工作笔记》-- 系统安全（Security）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_service/README.Security.md)
- [《微服务工作笔记》-- 数据治理（Governance）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_service/README.Governance.md)
- `>>>>` [《微服务工作笔记》-- Java 微服务（SpringBoot）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_service/README.SpringBoot.md)
- [《微服务工作笔记》-- DotNet 微服务（DotNet）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_service/README.DotNet.md)
- [《微服务工作笔记》-- 网页端工程（Frontend）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_service/README.Frontend.md)

<br>

#### 本章摘要

---

1.  更新状态：持续更新中；
2.  最后更新：2021.10.11；
3.  微服务脚手架 Java / SpringBoot / Vue.js 版；

<br>

#### JDK

---

1.  OracleJDK / OpenJDK

    - OracleJDK 和 OpenJDK 都是 Oracle 维护的，区别是商用协议；
    - OracleJDK：官网：<https://www.oracle.com/java/technologies/javase-downloads.html>；
    - OpenJDK：官网：<https://adoptopenjdk.net>，建议测试/预生产/生产环境以 Docker 镜像方式安装；
    - 不论是 OracleJDK 还是 AdoptOpenJDK，都只选 LTS 版本，比如 JDK8 / JDK11 / JDK17；
    - 在 JDK17 之前，因为 OracleJDK 的开放协议存在不确定性，且商用收费，因此建议在部署环境选用 OpenJDK，开发环境无所谓；
    - 在 JDK17 之后，Oracle 宣布 Java 免费商用，因此 OpenJDK 理论上已经退出历史舞台；

2.  Java8 / Java11 / Java17

    - 三个 LTS 版本的差异：<https://cloud.tencent.com/developer/article/1977236>；
    - Java11 新特性：
      - var 类型：自动推断变量类型；
      - 自带 HTTP Client，以后可以不用第三方包了；
      - String 类增强：新增了 isBlank()、strip()、repeat()等有用方法；
      - 函数式编程增强；
      - 直接用 java 运行.java 文件，而不用 javac 编译；
    - Java11 整体感觉没有革命性变化，现实点来说，生态介于 Java8 和 Java17 之间；
    - Java17 新特性：
      - Sealed Classes，表示允许哪些类继承；
      - Vector 向量数据结构；
    - Java17 的整体改动也比较小，加上 Java11 的变化之后，较 Java8 更完善；
    - 大多数软件生态都在强推 Java11/Java17，比如 Idea / Eclipse / VSCode，以后不用 java11 可能会需要在开发工具环节投入大量精力；
    - SpringBoot3 要求支持 Java17 以上；
    - 选型上可以选用:
      - SpringBoot2 + OpenJDK11（当前选择）
      - SpringBoot3 + JDK17（等待 GA 版本）

3.  MacOS 安装 AdopOpenJDK11：

    - Homebrew 安装：
      - 添加路径：`brew tap adoptopenjdk/openjdk`
      - cask 安装：`brew install --cask adoptopenjdk11`
    - MacOS 中使用/usr/libexec/java_home 工具进行 Java 寻址和版本切换；
      - 列出所有已安装的 JDK：`/usr/libexec/java_home -V`
    - JDK 安装之后即具备 javac，可进行编译，JDK 自带 JRE，可执行 java；

4.  环境变量：

    - 系统 PATH：用于查找指令，正常安装 JDK 都会修改配置好：
      - java 指令路径，MacOS 默认是/usr/bin/java；
      - javac 指令路径，MacOS 默认是/usr/bin/javac；
    - JAVA_HOME：用于指定 JDK 资源地址，正常安装 JDK 都会配置好：
      - MacOS 自带 JAVA_HOME 配置在~/.bash_profile，使用了系统自带的/usr/libexec/java_home 工具进行 Java 寻址和版本切换；
      - 若想手动配置可把/usr/libexec/java_home 注释掉，手写：`export JAVA_HOME=/xxx/xxx`；
    - JRE_HOME：一些软件可能会需要单独设置 JRE_HOME，视系统和安装方式而定；
      - 有些情况下 JRE_HOME 设置为 JAVA_HOME 即可，如 MacOS 系统；
      - 如果 echo $JAVA_HOME 看到是 xxx/xxx/jdk，可验证设置 JRE_HOME=xxx/xxx/jre 是否可行；
    - CLASS_PATH：用于在运行时查找二进制文件（TODO）；
      - 通常会包含 JAVA_HOME 下的 lib 目录；

5.  javac 指令

    - 版本检测：
      ```
      javac -version
      ```
    - 编译
      ```
      javac -s ./ -d ./ -cp ./;./lib/ Test.java
      ```
      - -s：源文件路径；
      - -d：目标路径；
      - cp：ClassPath，需要引用的资源路径，用;分隔多个路径；

6.  java 指令：

    - 版本检测：
      ```
      java -version
      ```
    - 执行二进制文件
      ```
      java -DTest=10 Test.class "这里往后都是Main的参数" 0.0 true
      ```
      或者
      ```
      java -Dtest.config.path=Test.xml -jar Test.jar com.test.A "这里往后都是Main的参数" 0.0 true
      ```
      - com.test.A 是指定入口类找 Main 函数；
      - -D<名>=<值>：设置系统属性，在 java 中通过 System.getProperty("my.config.path")获取；
      - cp：ClassPath，需要引用的资源路径，用;分隔多个路径；

7.  jar：

    - 每个 jar 包含一个 META-INF/MANIFEST.MF 文件，罗列文件清单，指定执行入口，其实就是配置 Class-Path 和 Main-Class；
    - jar 指令：略（尽量依赖 Maven/Gradle 工具和其它框架，不提倡手动使用 jar 指令打包）；

8.  war：

    - war 可理解为 web 专用 jar，现主要用于 Tomcat/Jetty 等 Servlet 容器；
      - 注意：Springboot 构建的 jar 包自带 Tomcat，因此不必要打成 war 包；
    - war 包结构：
      ```
      war
      ├── Index.jsp     # 主页面
      └── WEB-INF       # 资源目录
        ├── lib           # 依赖的第三方jar包
        ├── classes       # Java类
        │ └── package       # 包结构
        │   └── A             # 类
        └── web.xml       # 主配置文件
      ```
    - Tomcat 使用 war 包：将 war 包放在 Tomcat 的 webapps/目录下，Tomcat 自动解压执行；

<br>

#### Maven

---

1.  引用：
    - 仓库：<https://mvnrepository.com>；
    - 标签详解：<https://www.cnblogs.com/momo1210/p/7525690.html>；
2.  摘要：
    - 阿里开源的数据库连接池组件；
    - Maven 使用环境变量 Maven Home，但不写入系统 env，通常写入 IDE；
    - Maven 使用本地隐藏目录~/.m2/repository 存储下载的依赖包资源；
    - 包版本类型
      - RELEASE：稳定版本，上线使用；
      - SNAPSHOT：开发中使用，要关注包更新；
    - 标签知识点：
      - SpringBoot 内置的依赖都不用写版本号；
        - SpringBoot 的内置依赖可以在 TODO 中查看；
      - \<scope\>：生命周期作用域。 scope 对应 maven 中三种 classpath：编译，测试，运行。
        - compile：默认范围，编译测试运行都有效；
        - provided：在编译和测试时有效，在运行时由环境提供；
        - runtime：在测试和运行时有效；
        - test：只在测试时有效；
        - system：在编译和测试时有效，与本机系统关联，可移植性差；
        - import：只能用在 dependencyManagement 的 dependencies 中，表示引入上一级父 pom 中对应 dependency 的所有声明（同样仅仅是声明）；
      - \<optional\>true\<optional\>
        - 假设 Project-A 依赖 Project-B, Project-B 依赖 Project-C，如果在 Project-B 中定义：
          ```
          <dependency>
            <groupId>com.example</groupId>
            <artifactId>Project-C</artifactId>
            <optional>true/false</optional>
          </dependency>
          ```
        - optional = false：Projbct-B 强制要求 Project-A 引入 Project-C；
        - optional = true：只有当 Project-A 显式依赖 Project-C 时才真正引入 Project-C；
    - 父级项目配置：[Demo]()；
    - 子级项目配置：[Demo]()；
3.  部署：
    - MacOS 开发环境安装：
      ```
      brew install maven
      ```
    - TODO:Windows 开发环境安装：；
    - TODO:Linux 开发环境安装：；
    - 配置阿里镜像，HomeBrew 安装后，vim /usr/local/Cellar/maven/{version}/libexec/conf/settings.xml，在<mirors>下添加：
      ```
      <mirror>
        <id>alimaven</id>
        <name>aliyun maven</name>
        <url>http://maven.aliyun.com/nexus/content/groups/public/</url>
        <mirrorOf>central</mirrorOf>
      </mirror>
      ```
    - 笔者习惯了科学上网，没有使用上述配置；
4.  使用：
    - 版本检测：
      - `mvn -v`
      - `mvn -version`
    - 创建操作：
      - ```
        # 一次性创建项目：
        mvn archetype:create
          -DgroupId=packageName
          -DartifactId=webappName
          -DarchetypeArtifactId=maven-archetype-webapp
        ```
      - `mvn archetype:generate # 逐步性创建项目`
      - `mvn eclipse:eclipse # 生成eclipse项目`
      - `mvn idea:idea # 生成idea项目`
    - 基础操作：
      - `mvn clean # 清除产生的项目`
      - `mvn compile # 编译源代码`
      - `mvn package # 打包，生成 jar/war`
      - `mvn install # 包含 mvn compile，mvn package，然后上传到本地仓库`
      - `mvn site # 产生 site`
      - `mvn deploy # 上传到私服`
      - `mvn validate # 验证工程是否正确，所有需要的资源是否可用`
      - `mvn verify # 验证包是否有效且达到质量标准`
      - `mvn help:effective-pom # 查看父级 pom`
      - -U 参数：以上指令大部分都可以在最后跟-U 参数强制检查更新，常用于 SNAPSHOT 版本依赖
    - 执行 Java 类：
      - `mvn exec:java -Dexec.mainClass="com.example.demo.AppApplication"`
    - 测试：
      - `mvn test-compile # 编译测试代码`
      - `mvn test # 运行测试`
    - 进阶操作：
      - `mvn source:jar # TODO`
      - `mvn source:jar-no-fork # TODO`
      - `mvn jar:jar # 只打jar包`
      - `mvn test -skipping compile -skipping test-compile # 只测试而不编译，也不测试编译`
      - `mvn dependency:list # 查看当前项目已被解析的依赖`

<br>

#### Gradle

---

1.  官网：<https://gradle.org>；

<br>

#### SpringBoot

---

1.  理论基础：
    - 控制反转（IoC：InverseOfControl）：
      - 所谓反转，就是从主动获取对象，变成被动接受对象，当 A 类里边需要用 B 类的对象时，不要从 A 类里边生成，而是由别人（IoC 容器）填进去；
      - 依赖注入（DI：DependencyInject）：，IoC 的实现方式之一；
    - 面向切面编程（AOP：AspectOrientedProgramming）
      - 与 OOP 互补，各种拦截器的实现思想，常用语日志记录、审计、声明式事务、安全性和缓存等；
      - 一些简单术语：
        - 增强处理（Advice）：“before”、“after”、“after-returning”、“around”、“after-throwing”；
        - 切面（Aspect）：用来组织多个 Advice；
        - 连接点（JoinPoint）：用来切面方法的调用点；
        - 切入点（PointCut）：可以插入 Advice 的 JoinPoint，经常会用正则表达；
      - 一些高级术语：
        - 引入：；
        - 织入（Weaving）：其实就是扩展成子类；
        - AOP 代理；
      - 静态 AOP（如 AspectJ）和动态 AOP（如 Spring 默认）：
        - 静态 AOP：编译阶段修改字节码，高效；
        - 动态 AOP：运行阶段生成 AOP 代理，低效：
          - JDK 动态代理：通过反射实现；
          - CGLIB 动态代理：如果目标类没有实现接口，就会用这种代理；
2.  Spring Initializr 提供自动新建服务，官网：<https://start.spring.io>；
3.  工程结构：
    - 单仓库多模块结构（Mono-Repo/Multi-Module），一个父级 pom 多个子级 pom
      - 对 CI / CD 友好
      - 减少冗余；
      - 需要强大的持续集成工具的支持；
    - 每个微服务占用两个模块，一个接口（api）一个实现（svc），api 模块推送到 maven 仓库以便依赖方调用；
    - common-lib 模块作为通用工具，web-app 模块作为营销站点，config 作为私密配置文件；
4.  各个 starter 的简单介绍：
    - 参考：<https://www.cnblogs.com/yaowen/p/8623902.html>；
    - 参考：<https://www.jianshu.com/p/3a397c305969>；
    - 本例启动过程中用到的自带 starter 有：
      - Lombok：自动代码；
      - Spring Web：支持 http 的 web 服务；
      - Apache Freemarker：著名模板引擎之一，本例会有一些辅助页面会用到；
      - JWT
    - 本例用不到的自带 starter 有：
      - Spring Boot Devtools：修改代码后自动重启；
      - Jersey：基于 jax-rs（区别于 jax-xs/jax-rpc）实现的 RESTful 接口规范，类似于 RESTEasy；
      - Spring Configuration Processor：支持老的 xml 或 properties 格式的配置；
      - Spring Reactive Web：支持 webflux 的 tcp 长连接服务（实现 websocket）；
      - Spring Session：在 spring cloud 环境中，会需要用此框架解耦容器（tomcat/jetty）；
      - Spring Web Services：支持 SOAP 协议的服务；
        - soap 协议包是 xml 格式，http 是纯文本（依赖 json 传递格式化数据）；
        - 依赖于 wsdl4j（web 服务描述语言，xml 格式），使用 xsd 描述服务接口；
        - 常用于需要对外进行标准数据交换的网站，比如气象 / 地图 / 航班等；
      - Rest Repositories：也就是 Spring Data Rest，配合 Spring Data JPA 使用，将@RepositoriesRestResource 数据直接以 level3 开放到 Rest 接口；
        - 简介：<https://www.cnblogs.com/demingblog/p/10599134.html>；
        - 在使用 Spring Data JPA + Spring Data Rest 之后，简单逻辑的数据 CRUD 就不用再手写了，也可以直接省略 Controller，
      - Rest Repositories HAL Browser：基于 hal+json 的 media type 的 API 浏览器，参考：<https://www.jianshu.com/p/84f2bbffb885>；
      - Spring HATEOAS：支持 level3 的 RESTful 约束；
      - Vaadin：某框架，全用 Java 搞定网页端，不用 css/js，适合单兵开发 crm/erp；
      - Mustache：已无人提及的模板引擎；
      - Groovy Template：已无人提及的模板引擎；
      - Spring Security：如果不用 Shiro 的话想必就要用这个了；
      - OAuth2 Client：需要实现 OAuth2 客户端时使用；
      - OAuth2 Resource Server：需要实现 OAuth2 资源服务器时使用；
      - Spring LDAP：
      - Okta：当 OAuth2 使用 OpenID Connect（OIDC）进行身份验证时使用；
5.  工程中的 mvnw/mvnw.cmd 是个版本兼容脚本，若设备没有装 maven 或者版本不一致时，可执行./mvnw clean install 可以自动安装合适的版本并运行；
6.  将 resources/application.properties 修改为 resources/application.yml 以使用更结构化的数据；
7.  SpringBoot 基础概念：
    - Spring / SpringMVC / SpringBoot / SpringCloud 之间的关系：（TODO）
    - IoC（反向控制） / DI（依赖注入） / AOP（面向切面）：（TODO）；
8.  请求映射：
    - 根据 HTTP 的类型可分别使用 @GetMapping @PostMapping @PutMapping @DeleteMapping @PatchMapping；
    - @RequestMapping 完整版
      - `@RequestMapping(value="abc/test.json",consumes="application/json",produces="MediaType.XXX")`
        - value：路径；
        - method：方法；
        - consumes：允许媒体类型；
        - produces：响应媒体类型；
        - params：参数；
        - headers：请求头；
    - @RestController = @Controller + @ResponseBody，加了前者可省略后者（不用再申明响应类型）；
9.  参数
    - 参数映射：
      - @RequestParam：只支持基本数据类型，用于接收 form 表单参数或 url 参数
      - @ModelAttribute：支持基本数据类型和对象类型，用于接收 form 表单参数或 url 参数并绑定到 model 中
      - @RequestBody：支持基本数据类型和对象类型，用于接收 json 格式数据
        - 参数可用 @RequestBody Class class 直接反序列化请求中的 JSON 字符串；
    - JSON 传参：
      - 参考：<https://blog.csdn.net/qq_41753340/article/details/122491453>；
      - 参考：<https://blog.csdn.net/liuchang19950703/article/details/113444856>；
    - 参数校验：
      - SpringBoot 支持 JSR-303、Bean 验证，默认使用 hibernate validator 实现；
      - @NotNull 对象必须不为 null
      - @Null 对象只能为 null
      - @AssertFalse 限制必须为 false
      - @AssertTrue 限制必须为 true
      - @DecimalMax(value) 限制必须为一个不大于指定值的数字
      - @DecimalMin(value) 限制必须为一个不小于指定值的数字
      - @Digits(integer,fraction) 限制必须为一个小数，且整数部分的位数不能超过 integer，小数部分的位数不能超过 fraction
      - @Future 限制必须是一个将来的日期
      - @Max(value) 限制必须为一个不大于指定值的数字
      - @Min(value) 限制必须为一个不小于指定值的数字
      - @Past 限制必须是一个过去的日期
      - @Pattern(value) 限制必须符合指定的正则表达式
      - @Size(max,min) 限制字符长度必须在 min 到 max 之间
      - @Past 验证注解的元素值（日期类型）比当前时间早
      - @NotEmpty 验证注解的元素值不为 null 且不为空（字符串长度不为 0、集合大小不为 0）
      - @NotBlank 验证注解的元素值不为空（不为 null、去除首位空格后长度为 0），不同于@NotEmpty，@NotBlank 只应用于字符串且在比较时会去除字符串的空格
      - @Email 验证注解的元素值是 Email，也可以通过正则表达式和 flag 指定自定义的 email 格式
      - @Null @NotNull @NotBlank @NotEmpty；
      - @Size(min=,max=) @Length；
      - @Min @Max @Digits(integer=,fraction=) @Range(min=,max=)；
      - @Email @Pattern；
      - 用@Validated({主 Class.上下文 Class.class})触发校验，校验结果放在 BindingResult 对象中；
      - group 概念：@NotNull(group={Update.class}) @Null(group={Add.class}) Long id；
10. 响应：
    - 传 Class 返回 String（返回视图地址，JSON 自动反序列化参数生成 Class）
    - 传 Model 返回 String（返回视图地址）；
      - Controller 中带有@ModelAttribute 的方法会被先调用以在 Model 中统一添加内容（如 User）
    - 传 ModelAndView 返 ModelAndView（参数添加数据和视图地址后返回）；
    - @ResponseBody Class 返回序列化后的 JSON 字符串；
    - @ResponseBody String 返回纯文本；
11. 重定向：
    - 重定向响应：
      - 在正常响应的 String 中带"redirect:”；
      - 在正常响应的 ModelAndView 中带"redirect:”；
      - 直接使用 RedirectView 类；
    - Forward 响应：
      - 在 String 中加”forward:”指向别的@RequestMapping；
12. 传文件：
    - 参数：String name，MultipartFile file；
    - MultipartFile 主要方法：getOriginalFilename 文件名，getInputStream 流，getSize 大小；
13. 拦截器 / 过滤器：
    - WebMvcConfigurer.addInterceptors(InterceptorRegistry registry)
    - InterceptorRegistry、HandlerInterceptor、HandlerInterceptorAdapter？（TODO）；
    - HandlerInterceptor 的实现：preHandler / postHandle / afterComplete；
    - Spring Interceptor（拦截器） vs Servlet Filter（过滤器）：
      - 拦截器是基于反射机制，过滤器是函数回调；
      - 拦截器不依赖 Servlet 容器，可以在更广泛的场合运行；
      - 拦截器和过滤器同时存在时，过滤器先执行；
      - 针对 http 请求的预处理和后处理应该放在过滤器；
14. 全局单例：
    - 使用 @Bean、@Component、@Service、@Controller、@Repository 注解声明的类都是单例，用 @AutoWired 装配；
    - 如果 @AutoWired 装配的类有多种实现：
      - 在声明的时候使用 @Primary 让自己成为首选；
      - 声明的时候起个别名比如 @Service("Alias")，注入的时候使用@AutoWired + @Qualifier("Alias”)选取；
      - 不起别名也可以，注入的时候使用 @AutoWired+@Qualifier(“类名变小写开头”)选取；
    - @Bean，@Component，@Service，@Controller，@Repository 之间的关系：
      - @Component 是通用注解，@Service，@Controller，@Repository 是在此之上的扩展；
      - @Service 用于业务逻辑层，@Controller 用于表现层，@Repository 用于持久层；
        - 如果使用了 SpringBoot 的 Rest Repositories，可以将 @Repository 升级成 @RepositoriesRestResource，使其能直通 RESTful 操作；
      - @Bean 比 @Component 更灵活，扫描和装载的东西更少，引用第三方库时只能用 @Bean；
    - 如果某些 Bean 的方法需要在构造方法之后或者注销前执行，因为没有显式的构造方法，所以需要 @PostConstruct 和 @PreDestroy；
15. 全局多例：
    - 使用 @Bean，@Component，@Service，@Controller，@Repository 注解声明类时加上 @Scope("prototype")；
    - 需要定义成员变量的时候才用；
16. 多线程：
    - 在需要多线程的方法上标注 @Async；
    - 如果多线程用于处理请求，线程上下文内容共享，需要通过 ThreadPoolTaskExecutor.setTaskDecorator()实现；
17. 测试用例：
    - 使用 @SpringBootTest 声明一个 TestUnit；
      - 不需要启动 WebController 使用@SpringBootTest(webEnvironment=SpringBootTest.Environment.NONE)，如 Repository 测试；
    - 使用 @RunWith(SpringRunner.class)标注测试类加载数据库 JPA 所需的环境；
    - 使用 @Before 标注方法进行环境初始化，使用 @Test 标注方法进行单元测试，使用@After 标注方法进行环境释放；
    - 使用 @AutoConfigureMockMvc 自动装载 MockMvc；
    - 测试 REST 接口：RestTemplate client = RestTemplateBuilder.build()；

<br>

#### Lombok

---

1.  Lombok 简介：用注解方式提供许多 Kotlin 中才有的语法糖，会在编译期间自动生成代码；
2.  引入 SpringBoot 内置依赖：
    ```
    <dependency>
        <groupId>org.projectlombok</groupId>
        <artifactId>lombok</artifactId>
    </dependency>
    ```
3.  依赖引用后即可使用 Lombok 注解：
    - @Getter / @Setter 一键实现 POJO；
      - 指定非空字段：`@Setter(SomeField={@NonNull})`
    - @ToString 一键实现 toString()；
      - 指定排除在外的字段：`@ToString(exclude={"someField"})`
    - @EqualsAndHashCode 一键实现 equals 和 hashCode；
    - @NoArgsConstructor 一键实现无参构造函数；
    - @AllArgsConstructor 一键实现全参构造函数；
    - @RequiredArgsConstructor 一键实现 final 参构造函数；
    - @Data = @Getter + @Setter + @ToString + @EqualsAndHashCode + @RequiredArgsConstructor；
    - @NonNull 一键实现抛空参异常，建议配置成 IllegalArgumentException，需要在 lombok.config 文件中配置：
      ```
      lombok.nonNull.exceptionType = IllegalArgumentException
      lombok.nonNull.flagUsage = [warning | error]
      ```
    - @Builder 一键实现链式 Builder 模式；
      - @Builder.Default 的作用：
        - 不用@Builder.Default，为成员变量 data 设置默认值，new 出来的对象 data 自带默认值；
        - 启用@Builder.Default，new 出来的对象为空对象，data 不带默认值；
        - 启用@Builder.Default，不设置-> Student.builder().build()，data 为默认值；
        - 启用@Builder.Default，设置-> Student.builder().data(4).build()，data 为设置的值；
        - 参考：<https://www.jianshu.com/p/e60047ff7e53>；
    - @Cleanup 一键实现 InputStream/OutputStream 的 close；
      - 带参指定关闭方法名：`@Cleanup("SomeMethod")`
    - @Slf4j 一键实现 logback 等 factory 实例；
    - @Log 灵活配置不同 Log 框架
    - @Synchronized 一键实现方法锁；

<br>

#### POJO、JavaBean、EJB、Bean、Model/Entity、DAO、DTO、VO/DO/PO/AO/BO

---

1.  POJO（Plain Old Java Object）：
    普通 Java 类，没有继承、没有接口、没有框架接入，就是一个纯粹的 Java 类；

2.  JavaBean：
    - 稍微有点规范的 POJO；
    - 全部属性都是 private 属性，每一个都要有 getter / setter，命名要规范；
    - 必须有一个公共缺省构造函数；
    - 可序列化；
      - implements java.io.Serializable；
      - private static final long serialVersionUID = 1L；
    - 可以有 getter / setter 之外的方法，可以看做包含一定规则的 POJO；
3.  EJB：
    J2EE 四层模型中的概念，业务层规范，指一组实现某些业务功能的 JavaBean 的组合。
    比如经典的 Session_Bean / Entity_Bean / Message_Driven_Bean，它们有不同的功能，由 EJB 容器进行生命周期管理；
4.  Bean：
    SpringBoot 专用名词，特指可自动装配的实体，不限于 JavaBean；
5.  Model/Entity：
    - 一般是跟数据表字段完全对应的 ORM 对象；
    - 其中因为 JPA 使用 @Entity 注解 ORM 对象，因此 XxxEntity.java 通常是 JPA 专用；
    - 而 MyBaits 有时候会特意绕开 Entity，因此常使用 XxxModel.java，或者直接使用 Xxx.java；
6.  DAO：业务层规范，在 Mybatis 中对应的是 Mapper（因此常写成 XxxMapper），在 JPA 中对应的是 Repository（因此常写成 XxxRepo）；
7.  DTO：用于在网络传输的对象，一般不会完全等于 Model/Entity 中的字段（通常是子集），内部字段按需配置，为了避免冗余信息泄露；
    - DTO 常使用注解进行参数校验，用于在 HTTP 请求中自动规范数据范围，可极大缓解边界测试压力，其中包括：
      - @Null 被注释的元素必须为 null
      - @NotNull 被注释的元素必须不为 null
      - @AssertTrue 被注释的元素必须为 true
      - @AssertFalse 被注释的元素必须为 false
      - @Min(value) 被注释的元素必须是一个数字，其值必须大于等于指定的最小值
      - @Max(value) 被注释的元素必须是一个数字，其值必须小于等于指定的最大值
      - @DecimalMin(value) 被注释的元素必须是一个数字，其值必须大于等于指定的最小值
      - @DecimalMax(value) 被注释的元素必须是一个数字，其值必须小于等于指定的最大值
      - @Size(max=, min=) 被注释的元素的大小必须在指定的范围内
      - @Digits (integer, fraction) 被注释的元素必须是一个数字，其值必须在可接受的范围内
      - @Past 被注释的元素必须是一个过去的日期
      - @Future 被注释的元素必须是一个将来的日期
      - @Pattern(regex=,flag=) 被注释的元素必须符合指定的正则表达式
    - Hibernate Validator 附加的 constraint
      - @NotBlank(message =) 验证字符串非 null，且长度必须大于 0
      - @Email 被注释的元素必须是电子邮箱地址
      - @Length(min=,max=) 被注释的字符串的大小必须在指定的范围内
      - @NotEmpty 被注释的字符串的必须非空
      - @Range(min=,max=,message=) 被注释的元素必须在合适的范围内
8.  VO/DO/PO/AO/BO：
    - 分别是 ViewObject(视图对象)/DomainObject(领域对象)/PersistantObject(持久化对象)/ApplicationObject/BusinessObject；
    - 这些是 DDD 领域驱动设计中的概念；

<br>

#### SpringBoot 配置文件

---

1.  bootstrap.yml（.properties）/ application.yml（.properties）
    - bootstrap.yml：用来程序引导时执行，应用于更加早期配置信息读取，如可以使用来配置 application.yml 中使用到参数等；
    - application.yml：应用程序特有配置信息，可以用来配置后续各个模块中需使用的公共参数等；
2.  加载顺序： bootstrap.yml > bootstrap-dev/prod/test.yml application.yml > application-dev/prod/test.yml
3.  覆盖关系： bootstrap.yml < bootstrap-dev/prod/test.yml < application.yml < application-dev/prod/test.yml

<br>

#### SpringBoot Actuator

---

1.  引用：
    - 参考：<https://www.jianshu.com/p/1be43b3de0e7>；
2.  摘要：
    - Actuator 的作用是在运行时对服务进行监视和管理；
    - 常用 endpoints：
      | endpoint | 作用 |
      | :-------- | :--- |
      | info | 展示应用信息 |
      | mappings | 展示所有 @RequestMapping 路径信息 |
      | env | 查看环境变量 |
      | autoconfig | 查看自动配置的使用情况 |
      | configprops | 查看应用中所有 @ConfigurationProperties 列表 |
      | beans | 查看应用中所有 SpringBeans 列表 |
      | health | 查看应用程序健康信息 |
      | httptrace | 展示 HTTP 路径信息 |
      | metrics | 展示 metrics 信息 |
      | loggers | 显示并修改应用程序中日志器的配置 |
      | scheduledtasks | 展示应用程序中的定时任务信息 |
      | auditevents | 查看 audit 事件信息 |
      | dump | 打印线程栈 |
      | threaddump | 执行 Thread Dump |
    - Metrics 列表：<http://localhost:8080/actuator/metrics>；
      - 例如：访问<http://localhost:8080/actuator/metrics/jvm.memory.max>，查看 jvm.memory.max；
3.  使用：
    - `pom.xml`：[demo](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_service/springboot2_grpc_nacos/hello-svc/pom.xml)；
    - `application.yml`：[demo](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_service/springboot2_grpc_nacos/hello-svc/src/main/resources/application.yml)；

<br>

#### SpringBoot Admin

---

1.  引用：
    - 官网：<https://github.com/codecentric/spring-boot-admin>；
    - 文档：<https://consolelog.gitee.io/docs-spring-boot-admin-docs-chinese/>；
2.  摘要：
    - SpringBoot Admin 是一个第三方组件，用于监控 SpringBoot 服务，主要功能有：
      - 显示应用程序的监控状态；
      - 应用程序上下线监控；
      - 查看 JVM，线程信息；
      - 可视化的查看日志以及下载日志文件；
      - 动态切换日志级别；
      - Http 请求信息跟踪；
3.  使用：
    - 服务端`pom.xml`：[demo](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_service/springboot2_grpc_nacos/sba-svc/pom.xml)；
    - 服务端`@EnableAdminServer`：[demo](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_service/springboot2_grpc_nacos/sba-svc/src/main/java/com/q3d/demo/svc/sba/SbaSvcApplication.java)；
    - 客户端`pom.xml`：[demo](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_service/springboot2_grpc_nacos/hello-svc/pom.xml)；
    - 客户端`application.yml`：[demo](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_service/springboot2_grpc_nacos/hello-svc/src/main/resources/application-dev.yml)；

<br>

#### SpringBoot Configuration Processor

---

1.  引用：
    - 官网：<https://github.com/codecentric/spring-boot-admin>；
    - 参考：<https://blog.csdn.net/yuhan_0590/article/details/85100246>；
    - 参考：<http://t.zoukankan.com/felixzh-p-12768603.html>；
2.  摘要：
    - 有时候类对象属性太多可以考虑写在配置文件里，省去一大堆类定义，就可以使用 SpringBoot Configuration Processor 工具；
    - 很多低代码工具和脚手架也是通过配置文件实现自动生成代码；
3.  使用：
    - `pom.xml`：[demo](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_service/springboot2_grpc_nacos/pom.xml)；
    - `@EnableConfigurationProperties`：[demo](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_service/springboot2_grpc_nacos/hello-svc/src/main/java/com/q3d/demo/svc/hello/HelloSvcApplication.java)；
    - `@ConfigurationProperties`：[demo](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_service/springboot2_grpc_nacos/hello-svc/src/main/java/com/q3d/demo/svc/hello/model/RemoteConfigV1Model.java)；
    - `@PropertySource`和`@Value`：[Demo]()；

<br>

#### SpringBoot Test

---

1.  Spring Test 与 JUnit 等其他测试框架结合起来，提供了便捷高效的测试手段。而 Spring Boot Test 是在 Spring Test 之上的再次封装，增加了切片测试，增强了 mock 能力
2.  引入 SpringBoot 内置依赖：
    ```
    <dependency>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-test</artifactId>
        <scope>test</scope>
    </dependency>
    ```
3.  使用测试用例：
    ```
    @RunWith(SpringRunner.class)
    @SpringBootTest
    @Slf4j
    public class MyTests {
        /
        * 使用断言
        */
        @Test
        public void test1() {
            log.info("test 1");
            TestCase.assertEquals(1, 1);
        }
    }
    ```

<br>

#### SpringCloud

---

1.  版本选择：官网：<https://spring.io/projects/spring-cloud#learn>；
    - 当前使用的 SpringBoot 2.5.5 适配的是 SpringCloud 2020.0.x，最新的是 2020.0.4；
2.  2020.0.4 版本中的组件版本：<https://blog.csdn.net/youanyyou/article/details/120499926>；
    - 以下模块得到了更新：
      | 模块 | 版本 |
      | :--- | :--- |
      | SpringCloud Starter Build | 3.0.4 |
      | SpringCloud Netflix | 3.0.4 |
      | SpringCloud Openfeign | 3.0.4 |
      | SpringCloud Gateway | 3.0.4 |
      | SpringCloud Commons | 3.0.4 |
      | SpringCloud Config | 3.0.5
      | SpringCloud Consul | 3.0.4 |
      | SpringCloud Contract | 3.0.4 |
      | SpringCloud Kubernetes | 2.0.4 |
      | SpringCloud Sleuth | 3.0.4 |
      | SpringCloud Vault | 3.0.4 |
      | SpringCloud Zookeeper | 3.0.4 |
      | SpringCloud CircuitBreaker | 2.0.2 |
      | SpringCloud Stream | 3.1.4 |
      | SpringCloud Function | 3.1.4 |

<br>

#### SpringCloud Eureka

---

1.  引用：
    - 官网：<https://spring.io/projects/spring-cloud-netflix>；
    - 参考：<https://blog.csdn.net/xx326664162/article/details/80048704>；
    - 参考：<https://www.cnblogs.com/youngdeng/p/12883967.html>；
    - 高可用集群部署：<https://blog.csdn.net/m0_48481908/article/details/120592942>；
2.  摘要：
    - Eureka 是 Netflix 开源的一个 RESTful 风格的服务，是一个用于注册和发现的中心化的组件；
    - Eureka 稳定但已停止维护，官方建议使用 SpingCloud Consul 替换 Eureka + Config，但 Consul 的公司积极响应美国政府制裁中国，建议选择阿里开源的 Nacos；
    - Eureka 的不足之处：<https://www.sohu.com/a/240906237_355140>；
    - 本例选型使用 Nacos 作为服务注册中心和配置中心，不使用 Eureka；
3.  使用：
    - 服务端`pom.xml`：[demo](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_service/springboot2_grpc_nacos/eureka-svc/pom.xml)；
    - 服务端`application.yml`：[demo](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_service/springboot2_grpc_nacos/eureka-svc/src/main/resources/application-dev.yml)；
    - 服务端`@EnableEurekaServer`：[demo](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_service/springboot2_grpc_nacos/eureka-svc/src/main/java/com/q3d/demo/svc/eureka/EurekaSvcApplication.java)；
    - 客户端`pom.xml`：[demo](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_service/springboot2_grpc_nacos/hello-svc/pom.xml)；
    - 客户端`application.yml`：[demo](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_service/springboot2_grpc_nacos/hello-svc/src/main/resources/application-dev.yml)；
    - 客户端`@EnableDiscoveryClient`：[demo](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_service/springboot2_grpc_nacos/hello-svc/src/main/java/com/q3d/demo/svc/hello/HelloSvcApplication.java)；
    - 客户端寻址调用：[demo](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_service/springboot2_grpc_nacos/hello-svc/src/main/java/com/q3d/demo/svc/hello/service/RemoteIdV1Service.java)；

<br>

#### SpringCloud Config + Bus

---

1.  引用：
    - 官网：<https://spring.io/projects/spring-cloud-config>；
    - 官网：<https://spring.io/projects/spring-cloud-bus>；
    - 参考：<https://zhuanlan.zhihu.com/p/521884723>；
    - 参考：<https://www.cnblogs.com/cg-ww/p/15359042.html>；
    - 参考：<https://www.cnblogs.com/jinshengnianhua/p/15336219.html>；
    - 参考：<https://www.modb.pro/db/170175>；
    - 参考：<https://zhuanlan.zhihu.com/p/521884723>；
    - 原理：<https://blog.csdn.net/LeoHan163/article/details/118739022>；
2.  摘要：
    - Config 是分布式配置中心组件，支持配置文件放在本地或者 Git 仓库；
    - Bus 常用作配置中心自动同步，依赖消息中间件 Kafka/RabbitMQ 同步信息；
    - 官方建议使用 SpingCloud Consul 替换 Eureka + Config，但 Consul 的公司积极响应美国政府制裁中国，建议选择阿里开源的 Nacos；
3.  使用：
    - 服务端`pom.xml`：[demo](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_service/springboot2_grpc_nacos/config-svc/pom.xml)；
    - 服务端`application-native.yml`：[demo](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_service/springboot2_grpc_nacos/config-svc/src/main/resources/application-native.yml)；
    - 服务端`@EnableConfigServer`：[demo](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_service/springboot2_grpc_nacos/config-svc/src/main/java/com/q3d/demo/svc/config/ConfigSvcApplication.java)；
    - 客户端`pom.xml`：[demo](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_service/springboot2_grpc_nacos/hello-svc/pom.xml)；
    - 客户端`bootstrap-dev.yml`：[demo](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_service/springboot2_grpc_nacos/hello-svc/src/main/resources/bootstrap-dev.yml)；
    - 客户端`@EnableConfigurationProperties`：[demo](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_service/springboot2_grpc_nacos/hello-svc/src/main/java/com/q3d/demo/svc/hello/HelloSvcApplication.java)；
    - 客户端`@ConfigurationProperties`：[demo](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_service/springboot2_grpc_nacos/hello-svc/src/main/java/com/q3d/demo/svc/hello/model/RemoteConfigV1Model.java)；

<br>

#### SpringCloud Gateway

---

1.  引用：
    - 官网：<https://spring.io/projects/spring-cloud-gateway>；
    - 参考：<https://www.cnblogs.com/qiantao/p/15750341.html>；
    - 参考：<https://blog.csdn.net/zzy7075/article/details/109238763>；
    - Predicate 规则：<https://blog.csdn.net/crazymakercircle/article/details/125057567>；
2.  摘要：

    - SpringCloud 曾有 Zuul 和 Gateway 两款网关产品，官方目前建议用 Gateway，但同时也在发展 Zuul2；
    - SpringCloud Gateway 可以配合 Consul/Nacos 使用，同时也能接入 DotNet 微服务，是除了自研网关之外的可选网关；
    - Route（路由）：Route 是网关的基础元素，由 ID、目标 URI、断言、过滤器组成。当请求到达网关时，由 Gateway Handler Mapping 通过断言进行路由匹配，当断言为真时，匹配到路由。
    - Predicate（断言）：Predicate 是 Java 8 中提供的一个函数。允许开发人员匹配来自 HTTP 的请求，例如请求头或者请求参数。简单来说它就是匹配条件。
      | 规则 | 实例 | 说明 |
      | :-- | :-- | :-- |
      | Path | - Path=/gate/,/rule/ | 当请求的路径为 gate、rule 开头的时将被转发 |
      | Before | - Before=2017-01-20T17:42:47.789-07:00[America/Denver] | 某个时间之前的请求将被转发 |
      | After | - After=2017-01-20T17:42:47.789-07:00[America/Denver] | 某个时间之后的请求将被转发 |
      | Between | - Between=2017-01-20T17:42:47.789-07:00[America/Denver],2017-01-21T17:42:47.789-07:00[America/Denver] | 某个时间段之间的请求将被转发 |
      | Cookie | - Cookie=chocolate,ch.p | 名为 chocolate 或者满足类似于 chop 正则的请求将被转发 |
      | Header | - Header=X-Request-Id, \d+ | 携带参数 X-Request-Id 或者满足 \d+ 正则的请求将被转发 |
      | Host | - Host=\*\*.baidu.com | 主机名为类似 www.baidu.com 的请求将被转发 |
      | Method | - Method=GET | GET 请求将被转发，还可以限定 POST、PUT 等请求方式 |
      | Query | - Query=id | 参数中有 userid 的请求将被转发 |
      | RemoteAddr | - RemoteAddr=192.168.1.1/24 | 满足某个 IP 段的请求将被转发 |
    - Filter（过滤器）：Filter 是 Gateway 中的过滤器，可以在请求发出前后进行一些业务上的处理。
    - 注意：SpringCloud Gateway 目前还不能直接转发和管理 gRPC 请求，因此只能在 Gateway 服务中做一个 HTTP 请求进行转发；

3.  使用：
    - 服务端`pom.xml`：[demo](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_service/springboot2_grpc_nacos/gateway-svc/pom.xml)；
    - 服务端`application.yml`：[demo](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_service/springboot2_grpc_nacos/gateway-svc/src/main/resources/application.yml)；

<br>

#### SpringCloud Ribbon

---

1.  引用：
    - 官网：<https://ribboncommunications.com/>；
    - 参考：<https://www.cnblogs.com/cg-ww/p/15359053.html>；
2.  摘要：
    - Ribbon 是 Netflix 发布的开源项目，主要功能是提供客户端的软件负载均衡算法和服务调用；
3.  使用：
    - `pom.xml`：[Demo]()；
    - `application.yml`：[Demo]()；
    - `Application.java`：[Demo]()；

<br>

#### SpringCloud Hystrix

---

1.  引用：
    - 官网：<https://www.baeldung.com/spring-cloud-netflix-hystrix>；
    - 雪崩效应的形成的应对：<https://segmentfault.com/a/1190000005988895>；
2.  摘要：
    - Ribbon 是 Netflix 发布的开源项目，主要功能是提供客户端的软件负载均衡算法和服务调用；
    - 目前笔者推荐 Sentinel；
3.  使用：
    - `pom.xml`：[Demo]()；
    - `application.yml`：[Demo]()；
    - `Application.java`：[Demo]()；

<br>

#### SpringCloud Stream

---

1.  引用：
    - 官网：<https://spring.io/projects/spring-cloud-stream>；
    - 参考：<https://zhuanlan.zhihu.com/p/521884723>；
2.  摘要：
    - Stream 解决了开发人员无感知的使用消息中间件的问题，因为 Stream 对消息中间件的进一步封装，可以做 到代码层面对中间件的无感知，甚至于动态的切换中间件；
3.  使用：
    - `pom.xml`：[Demo]()；
    - `application.yml`：[Demo]()；
    - `Application.java`：[Demo]()；

<br>

#### SpringCloud OpenFeign

---

1.  引用：
    - 官网：<https://spring.io/projects/spring-cloud-openfeign>；
    - 参考：<https://blog.csdn.net/weixin_39806100/article/details/113924841>；
2.  摘要：
    - Feign 是一个声明式的伪 RPC 工具，本质是 RESTful 的 Http 请求，但采用 RPC 的工作模式；
3.  使用：
    - `pom.xml`：[Demo]()；
    - `application.yml`：[Demo]()；
    - `Application.java`：[Demo]()；

<br>

#### SpringCloud Sleuth + Zipkin

---

1.  引用：
    - 官网：<https://spring.io/projects/spring-cloud-sleuth>；
    - 参考：<https://www.cnblogs.com/cy0628/p/15195918.html>；
2.  摘要：
    - Sleuth 是链路追踪器，Zipkin 是展示工具；
    - 目前笔者推荐选用 SkyWalking；
3.  使用：
    - `pom.xml`：[Demo]()；
    - `application.yml`：[Demo]()；
    - `Application.java`：[Demo]()；

<br>

#### Swagger

---

1.  引用：
    - 官网：<https://swagger.io/>；
    - 参考：<https://www.cnblogs.com/haola/p/15362814.html>；
2.  摘要：
    - Swagger 是自动生成接口文档的组件；
    - 访问路径：
      - 2.x.x 访问路径：<http://{host}:{port}/context-path/swagger-ui.html>；
      - 3.x.x 访问路径：<http://{host}:{port}/context-path/swagger-ui/index.html>；
    - Swagger 注解：
      - @Api(tags = "描述请求类，会显示在 UI 界面上",value = "自己看的注释，不会显示在 UI 界面上" )
      - @ApiOperation(tags = "描述请求方法，会显示在 UI 界面上",value = "自己看的注释，不会显示在 UI 界面上" )
      - @ApiImplicitParams：用在请求的方法上，表示一组参数说明
        - @ApiImplicitParam：用在@ApiImplicitParams 注解中，指定一个请求参数的各个方面
          - name：参数名
          - value：参数的汉字说明、解释
          - required：参数是否必须传
          - paramType：参数放在哪个地方
            - header --> 请求参数的获取：@RequestHeader
            - query --> 请求参数的获取：@RequestParam
            - path --> 请求参数的获取：@PathVariable
            - body
            - form
          - dataType：参数类型，默认 String，其它值 dataType="Integer"
          - defaultValue：参数的默认值
      - @ApiResponses：用在请求的方法上，表示一组响应
        - @ApiResponse：用在@ApiResponses 中，一般用于表达一个错误的响应信息
          - code：数字，例如 400
          - message：信息，例如"请求参数没填好"
          - response：抛出异常的类
      - @ApiModel：用于响应类上，表示一个返回响应数据的信息（这种一般用在 post 创建的时候，使用@RequestBody 这样的场景，请求参数无法使用@ApiImplicitParam 注解进行描述的时候）
      - @ApiModelProperty：用在属性上，描述响应类的属性
3.  使用：
    - `pom.xml`：[Demo]()；
    - `application.yml`：[Demo]()；
    - `Application.java`：[Demo]()；

<br>

#### 自动拦截日志

---

1.  引用：<https://blog.csdn.net/quyixiao/article/details/115369035>；

<br>

#### MyBatis

---

1.  引用：
    - mybatis-generator 配置：<https://blog.51cto.com/u_12630471/3794048>；
    - mybatis-generator 配置：<https://www.cnblogs.com/xichji/p/12047553.html>；
    - mybatis-generator ModelExample 用法：<https://www.cnblogs.com/xichji/p/12047553.html>；
    - MyBatis 注解版用法：<https://www.zhihu.com/market/paid_column/1167078405261647872/section/1169647796486647808>；
2.  摘要：
    - 阿里开源的数据库连接池组件；
3.  使用：
    - `pom.xml`：[Demo]()；
    - `application.yml`：[Demo]()；
    - `Application.java`：[Demo]()；
    - 监控`pom.xml`：[Demo]()；

<br>

#### MyBatis-Plus

---

1.  引用：
    - 官网：<https://baomidou.com/>；
    - Gitee：<https://gitee.com/baomidou/mybatis-plus>；
    - Github：<https://github.com/baomidou/mybatis-plus>；
2.  摘要：
    - 阿里开源的数据库连接池组件；
3.  使用：
    - `pom.xml`：[Demo]()；
    - `application.yml`：[Demo]()；
    - `Application.java`：[Demo]()；
    - 监控`pom.xml`：[Demo]()；

<br>

#### Druid

---

1.  引用：
    - Github：<https://github.com/alibaba/druid>；
    - 文档：<https://github.com/alibaba/druid/tree/master/druid-spring-boot-starter>；
2.  摘要：
    - 阿里开源的数据库连接池组件；
3.  使用：
    - `pom.xml`：[Demo]()；
    - `application.yml`：[Demo]()；
    - `Application.java`：[Demo]()；
    - 监控`pom.xml`：[Demo]()；

<br>

#### SkyWalking

---

1.  SkyWalking 是一款基于字节码的链路追踪器；
2.  参考：<https://www.cnblogs.com/cy0628/p/15195918.html>；
3.  引入第三方依赖：
    ```
    <!-- SkyWalking -->
    <dependency>
        <groupId>org.springframework.cloud</groupId>
        <artifactId>spring-cloud-starter-sleuth</artifactId>
    </dependency>
    <!-- Zipkin -->
    <dependency>
        <groupId>org.springframework.cloud</groupId>
        <artifactId>spring-cloud-starter-zipkin</artifactId>
    </dependency>
    ```
4.  使用；
    - `pom.xml`：[Demo]()；
    - `application.yml`：[Demo]()；
    - `Application.java`：[Demo]()；

<br>

#### GraalVM

---

1.  TODO:

<br>

#### SpringNative

---

1.  TODO:

<br>

#### JHipster

---

1.  简介：
    - 是一个国外常用的 SpringBoot 脚手架；
    - 是一个流行的 Codeless 框架；
    - 官网：<https://www.jhipster.tech/>；
    - 参考：<https://www.cnblogs.com/demingblog/p/13805484.html>；
2.  ## 技术栈：
    - JPA / Hibernate / DDD；

<br>

#### eladmin

---

1.  简介：
    - 国内常用的 CRM 脚手架之一
    - 官网：<https://el-admin.vip>；
    - 是一套代码模板，不是 Codeless 框架；
2.  ## 技术栈：
    - Shiro / MyBatis；

<br>

#### 若依 Ruoyi

---

1.  简介：
    - 国内常用的微服务脚手架之一
    - 官网：<http://www.ruoyi.vip>；
    - 是一套代码模板，不是 Codeless 框架；
2.  ## 技术栈：
    - Shiro / MyBatis；

<br>

#### Vue 脚手架

---

1.  Vue-Element-Admin：Vue 官方推荐的后台管理平台，很完善，被大部分基于 Vue 的 CRM 系统使用；
