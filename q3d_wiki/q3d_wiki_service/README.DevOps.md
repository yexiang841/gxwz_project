### 《微服务工作笔记》-- 持续集成部署（DevOps）

<br>

#### 介绍

---

- 项目文档记录了笔者工作积累的云原生微服务技术笔记，以及元宇宙、数字孪生、程序化建模工作学习笔记；
- 项目包含一套元宇宙建模方法论和工程，采用 QGIS -> Blender / Houdini -> Unreal / Unity 的方式实现数字孪生虚拟城市；
- 项目包含一个 Unreal 客户端工程和一个 Unity 客户端工程（建设中）；
- 项目包含一个 Cesium 网页端工程，实现 WebGIS BI 展示；
- 项目包含一个 Vue.js / ElementPlus / TS 网页端工程，实现 Admin 管理后台；
- 项目包含一个 Java / SpringBoot 微服务工程和一个 C# / DotNet 微服务工程（建设中），共用网关和服务注册中心（Nacos），可以协同工作；
- 项目包含一个 Publish 部署工程，采用 Docker / Kubernetes / Jenkins 方式进行服务部署；
- 程序语言涉及 C++ / Java / C# / Python / TS / Shell / VEX；
- 错漏之处恳请读者不吝指正，转载望注明出处：<https://gitee.com/yexiang841/q3d_project/>；
- Copyright 叶湘 ( weixin:yexiang841, email:yexiang841@qq.com ), Shitouren Studio. All Rights Reserved.

<br>

#### 微服务工作学习笔记

---

- [《微服务工作笔记》-- 部署说明（Deploy）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_service/README.Deploy.md)
- [《微服务工作笔记》-- 架构选型（Selection）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_service/README.Selection.md)
- [《微服务工作笔记》-- 数据接口（Interface）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_service/README.Interface.md)
- [《微服务工作笔记》-- 数据持久化（Persistence）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_service/README.Database.md)
- [《微服务工作笔记》-- 分布式组件（Distributed）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_service/README.Distributed.md)
- [《微服务工作笔记》-- 云原生（CloudNative）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_service/README.CloudNative.md)
- `>>>>` [《微服务工作笔记》-- 持续集成部署（DevOps）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_service/README.DevOps.md)
- [《微服务工作笔记》-- 系统安全（Security）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_service/README.Security.md)
- [《微服务工作笔记》-- 数据治理（Governance）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_service/README.Governance.md)
- [《微服务工作笔记》-- Java 微服务（SpringBoot）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_service/README.SpringBoot.md)
- [《微服务工作笔记》-- DotNet 微服务（DotNet）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_service/README.DotNet.md)
- [《微服务工作笔记》-- 网页端工程（Frontend）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_service/README.Frontend.md)

<br>

#### 本章摘要

---

1.  更新状态：偶尔补充；
2.  最后更新：2021.10.20；
3.  本章记录常用开发工具/环境的搭建、CI/CD 和 DevOps 的概念和用法；

<br>

#### Server 端微服务架构中的组件解析

---

5.  微服务治理：
    - 日志：无论是否微服务，日志的规范是敏捷运维的基础要素之一；
    - 审计日志：随着日志查询被重视和 ELK 的流行，对日志格式规范的要求提升，于是发展处审计日志；
    - 集中日志监控：即日志的采集、传输、存储、查询/搜索；
    - CI/CD：持续集成和发布，敏捷开发和敏捷运维的基础要素之一；
    - 调用链跟踪：SkyWalking；
    - Metrics 监控：Prometheus / AlertManager / Grafana；

<br>

#### 环境/硬件/系统

---

1.  设定 DEV、TEST、UAT、PROD 四种环境，分别用于开发、测试、产品预发布（线上测试）、产品发布；
2.  开发环境（DEV）和测试环境（TEST）：

    - MacOS Big Sur 11.5.2
    - 4 GHz 四核 Intel Core i7
    - 16 GB 1867 MHz DDR3

3.  预生产环境（UAT）和生产环境（PROD）：

    - 据 Rancher 官方文档说明，因当前 Aliyun EKS 无法对 Rancher 共享 ETCD，所以如果选用 Rancher，目前只能基于 ECS；

<br>

#### 软件安装的原则

---

1.  对于研发环境相关软件，按以下优先级选择安装方式：

    - 优先级一：Yum / HomeBrew / Apt / Zypper 方式安装
      - Fedora / Redhat 系（包括 CentOS）：yum / rpm；
      - Mac：HomeBrew；
      - Ubuntu / Debian 系：apt-get / deb
      - SUSE / OpenSUSE 系：zypper；
    - 优先级二：从官方安装工具安装；
      - 有些软件会提供安装链接，使用 curl | bash 指令方式安装；
      - 笔者习惯在使用将官方安装工具之前看 HomeBrew 库中是否有较新版本，如果版本太老还是建议使用官方推荐方式；
      - 卸载：依据官方文档；
    - 优先级三：release 编译版本安装；
      - 解压到本地，然后将 bin 路径添加到/etc/paths；
      - 卸载：删除 release 文件，手动删除/etc/paths 中的相关变量；
    - 优先级四：从源码编译安装；
      - 通常是./configure && cmake . && make && make install 编译安装；
      - 卸载：通常是在原路径下：
        ```
        make uninstall
        ```
      - 不推荐此方式安装，因为丢失源码后不容易回溯 / 升级 / 卸载 / 修改配置；

2.  笔者使用 MacOS / CentOS，以下会更多记录 MacOS / CentOS 系统的工作；
3.  对于工具软件和桌面应用软件，按以下优先级选择安装方式：

    - 优先级一：官方桌面软件（dmg）；
      - 选择此种方式的重要原因是避免被 brew upgrade 等操作误伤，例如 Docker 等版本敏感型的软件；
      - 卸载：通过 AppCleaner 之类的第三方工具；
        - 有一些软件是在 dmg 里边封装其它格式（常见 pkg），没有在 applications 中呈现，会需要用安装文件来卸载，如 Vagrant；
      - 升级：在软件内部进行，或下载新版本覆盖安装；
    - 优先级二：HomeBrew Cask 安装；
      - 通过 brew install --cask 安装，通过 brew cask uninstall --cask 卸载；
    - 优先级三：官方 pkg 包；
      - MacOS 中双击即可安装；
      - 卸载方式：
        ```
        pkgutil --pkgs #找出包名；
        pkgutil --pkg-info xxx.xxx.xxx #找出相关路径；
        sudo rm -r {上述指令找到的路径}；
        sudo pkgutil --forget com.microsoft.dotnet.xxx.xxx...；
        ```
      - 也可以通过第三方工具 pkg-uninstall 卸载；

4.  对于生产环境中服务运行相关软件，按以下优先级选择软件安装方式：

    - 优先级一：Helm / Rancher 方式安装在 k8s 系统中；
    - 优先级二：Docker 方式安装（特指通过 Docker-Compose 指定配置文件安装）；
    - 优先级三：yum 方式安装；

5.  安装软件前建议 which -a 一下，清除不必要的重复软件以免埋下环境隐患（如 python 等）；
6.  软件、代码、数据在研发设备中的存放位置；

    - 如果是桌面软件或者 HomeBrew 方式安装，建议不要改动安装路径（为共享设备者和未来的自己考虑）；
    - 严格禁止使用中文路径；
    - 尽量禁止使用带空格的路径，降低使用风险；

7.  笔者习惯：

    - 第三方软件安装在/opt 中；
    - 工程堆放在/opt/projects 中；
    - 部署配置、数据、日志堆放在/opt/publish 中，用 git 进行管理；
    - 避坑：在 Linux 系统中可能需要设置此路径为当前非 root 用户所有：
      ```
      sudo chown -R $(whoami):admin /opt
      ```

<br>

#### HomeBrew(其它 Linux 平台请参考各自的软件库工具)

---

1.  引用：
    - 官网：<https://brew.sh>；
2.  摘要：
    - 笔者实操中发现换阿里镜像后卡顿还是经常发生，建议依照后面 Privoxy 一节设置科学护体，避免换镜像；
    - 如果没有科学护体，正常使用需要换镜像（如阿里云等）：
      - 设置镜像：
        ```
        cd $(brew —repo) && git remote -v
        git -C "$(brew --repo)" remote set-url origin https://mirrors.aliyun.com/homebrew/brew.git
        cd "$(brew --repo)/Library/Taps/homebrew/homebrew-core" && git remote -v
        git -C "$(brew --repo)/Library/Taps/homebrew/homebrew-core" remote set-url origin https://mirrors.aliyun.com/homebrew/homebrew-core.git
        ```
      - 恢复默认镜像：
        ```
        git -C "$(brew --repo)" remote set-url origin https://github.com/Homebrew/brew
        git -C "$(brew --repo)/Library/Taps/homebrew/homebrew-core" remote set-url origin https://github.com/Homebrew/homebrew-core
        ```
    - 安装软件之后配置环境变量：
      - `sudo vim /etc/paths` 检查配置确保 `/usr/local/bin` 和 `/usr/local/sbin` 排在 `/usr/bin` 和 `/usr/sbin` 前面；
      - 这是因为 MacOS 默认安装的不可删除软件（比如 git）使用后者路径，而 HomeBrew 安装的软件使用前者路径；
      - 可在 `/etc/paths.d/` 中新建文件单独记录各软件的环境变量；
3.  部署：
    - 根据官方文档安装：
      ```
      /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
      ```
    - HomeBrew 在 MacOS 中依赖 XCode 的 CommandLineTools 组件，在没装 XCode 的情况下需要安装：
      ```
      xcode-select --install
      ```
4.  使用：
    - 清理和升级 HomeBrew 库：
      ```
      brew update # 更新 HomeBrew 库
      brew doctor # 检查本地软件状态并给出升级、卸载建议
      brew cleanup # 清除无用软件
      ```
    - 安装和卸载软件：
      ```
      brew list # 列出已安装软件
      brew info xxx # 查看（版本、路径、配置文件等信息）
      brew install xxx # 安装
      brew uninstall xxx # 卸载
      ```
    - 安装特定的版本：
      - 用 brew info xxx 查看的版本信息中会包括`From: https://github.com/xxx/xxx.rb`
      - 登录这个 github 的 history 找到对应版本的 xxx.rb 地址，然后：
        ```
        brew install <https://github.com/xxx/xxx.rb>；
        ```
    - HomeBrew 安装软件的常用路径：
      ```
      /usr/local/Cellar/ # 软件路径之一
      /usr/local/opt/ # 软件路径之二
      /usr/local/etc/ # 配置文件路径
      /usr/local/var/ # 数据路径之一
      /usr/local/var/lib/ # 数据路径之二
      /usr/local/var/run/ # 数据路径之三
      /usr/local/var/log/ # 日志路径
      ```
    - 安装软件过程中，如果 update 步骤太慢，可以：
      - ctrl + c 跳过；
      - 或者（不推荐）vim ~/.bash_profile 新增一行：
        ```
        export HOMEBREW_NO_AUTO_UPDATE=true；
        ```
    - 管理服务：
      ```
      brew services cleanup # 清除已卸载应用的无用的配置
      brew services list # 查看使用 brew 安装的服务列表
      brew services run {service-name} # 启动服务（仅启动不注册）
      brew services start {service-name} # 启动服务，并注册
      brew services stop {service-name} # 停止服务
      brew services restart {service-name} # 重启服务
      ```

<br>

#### Git

---

1.  引用：
    - 官网：<https://git-scm.com>；
    - 一个很棒的 Git 原理动画：<https://zhuanlan.zhihu.com/p/132573100>；
2.  摘要：
    - git 协议使用 22 端口，可以利用 ~/.ssh/id_rsa.pub 实现免密提交，因此更适合私有的开发环境；
      - 在单主机多用户（比如公用测试机）环境上，频繁修改 ~/.ssh 是无权限或者不方便的，因此更适合使用 <https 协议>；
    - https 协议使用 443 端口
      - <https 协议常用于项目开源，惯例只用于 pull>；
      - <https 协议需要提交的话，要么请求合并，要么在有权限的情况下，用 Git 账户密码进行 push>；
    - https 协议实现免密提交：
      把：
      ```
      https://gitee.com:user/project.git
      ```
      添加 Git 账户和密码变成：
      ```
      https://{account}:{passwd}@gitee.com:user/project.git
      ```
      可实现免密提交；
      但这样会在`git remote -v`指令下暴露个人密码，必须避免，应该只使用 account：
      ```
      https://{account}@gitee.com:user/project.git
      ```
      密码手动输入；
      对于其中的 account，很多 Git 账户都是邮箱，邮箱中的 `@` 会和 `@gitee.com` 中的 `@` 混淆而出错，需要转义为 `%40`；
      例如 `account@qq.com@gitee.com` 应写成 `account%40qq.com@gitee.com`
      若不想暴露密码，又想免密提交，建议改用 git 协议；
    - git 协议实现免密提交
      依赖于 ~/.ssh ：
      首先生成密钥对：
      ```
      ssh-keygen -t rsa -C "me@mymail.com" # 默认会存储为 id_rsa / id_rsa.pub
      ```
      在 Git 托管平台添加 id_rsa.pub 作为信任凭证；
      然后就可以在 git 协议下免密提交；
    - 多账户协作（开发环境，单用户，多账户）
      场景：在自己电脑上，根据配置自动切换个人 git 账户和公司 git 账户；
      因为是个人设备，所以在 git clone 时选择 git 协议；
      因为是单用户开发，所以可以设置全局署名：
      ```
      git config --global user.name "me"
      git config --global user.email "me@mymail.com"
      ```
      如果希望按项目区分署名，可以选择在项目中单独设置署名；
      用个人账户生成密钥对：
      ```
      ssh-keygen -t rsa -C "me@mymail.com" # 存储名为 id_rsa_myself
      ```
      用公司账户生成密钥对
      ```
      ssh-keygen -t rsa -C "me@company.com" # 存储名为 id_rsa_company
      ```
      生成一个配置文件 `~/.ssh/config`，根据 remote 地址自动区分使用哪个账户，大概这样：
      ```
      # myself
      # key from `ssh-keygen -t rsa -C "me@mymail.com"`
      # add by ssh-add ~/.ssh/id_rsa_myself
      #
      Host me.github.com
      HostName github.com
      User git
      PreferredAuthentications publickey
      IdentityFile ~/.ssh/id_rsa_myself
      #
      # to check: ssh -T git@myself.github.com
      #
      # company
      # key from `ssh-keygen -t rsa -C "me@company.com"`
      # add by ssh-add ~/.ssh/id_rsa_company
      #
      Host company.github.com
      HostName github.com
      User git
      PreferredAuthentications publickey
      IdentityFile ~/.ssh/id_rsa_company
      #
      # to check: ssh -T git@company.github.com
      ```
    - 多账户协作（测试环境，多用户，单账户）
      场景：多个开发者共同登录同一台公用的测试主机，每个开发者用自己的账号拉取同一个代码库；
      因为是公用测试机，频繁修改 ~/.ssh 是无权限或者不方便的，所以在 git clone 时选择 <https 协议>；
      因为是多用户开发，所以只能在每个项目中单独设置署名：
      ```
      git config user.name "Name"
      git config user.email "account@company.com"
      ```
      不需要 ~/.ssh，只需要在 git clone 时按上节方式指定 Git 账户即可；
      把：
      ```
      https://gitee.com:user/project.git
      ```
      添加 Git 账户变成：
      ```
      https://account@gitee.com:user/project.git
      ```
      注意不要添加密码，在公共设备上必须保持每次提交都输入密码，否则将危及代码库安全；
    - Fork 和 Branch
      - 注意：Fork 不是 Git 指令，而是 Github 指令，用来克隆好的项目；
      - Fork 出来的源码，如果想要贡献给原项目，用 Pull Request 向原项目发合并申请；
      - Branch 是内部日常使用的流程；
    - 提交日志规范
      ```
      type(功能模块): 标题
      (空行)
      - 说明 1
      - 说明 2
      (空行)
      BREAKING CHANGE：重大变动以及如何变迁（可选）
      ```
      - 其中 type 可以是：
        - feat: 添加新特性
        - fix: 修复 bug
        - docs: 仅仅修改了文档
        - style: 仅仅修改了空格、格式缩进、逗号等等，不改变代码逻辑
        - refactor: 代码重构，没有加新功能或者修复 bug
        - perf: 优化相关，比如提升性能、体验
        - test: 增加测试用例
        - chore: 改变构建流程、或者增加依赖库、工具等
        - revert: 回滚到上一个版本
      - 举例：
        ```
        feat(Nginx): 修正一些配置
        ```
        ```
        docs(README): 增加一些描述
        ```
3.  部署：
    - MacOS 开发环境安装：
      ```
      brew install git
      ```
      - Git 在 MacOS 中依赖 XCode 的 CommandLineTools 组件，在没装 XCode 的情况下需要安装：
        ```
        xcode-select --install
        ```
    - TODO:Windows 开发环境安装：；
    - TODO:Linux 开发环境安装：；
    - 配置署名和邮箱
      ```
      git config --global user.name "me"
      git config --global user.email "me@mail.com"
      ```
      - 有 `--global` 就是本主机全局配置；
      - 没有 `--global` 就是当前工程配置；
    - 检查署名配置
      ```
      git config -l --global # 查看全局配置
      git config -l # 查看局部配置
      ```
4.  使用：
    - 创建/挂接 git 仓库
      ```
      mkdir project
      cd project
      git init # 在本地生成了.git，但还没有挂接远程仓库
      touch README.md
      git add README.md
      git commit -m “initialize on xxxx.xx.xx"
      git remote add origin git@gitee.com:me/project.git
      git push -u origin master # -u：记住当前选择，以后可以省略 origin master
      ```
    - 常用指令：
      - `git remote -v # 查看远程地址`
      - `git log -2 -p # 查看提交历史`
        - -2：只显示两个版本；
        - -p：显示出每个版本的 diff；
      - `git revert {历史版本号} # 用历史版本提交一个新版本`
        - 相当于以某个历史版本作为最新版本并进行一次 commit；
        - 所有版本都将保留；
      - `git reset --soft/hard {历史版本号} # 回退到某一个历史版本，在这个版本之后的版本全部丢弃`
        - 这里的回退主要是对 HEAD 进行回移操作；
        - --soft：当前版本会留在缓冲区，此时依然可以继续提交这个版本；
        - --hard：清空缓冲区，如果想要强行推到仓库，需要 git push -f；
      - `git push -f # 强制让远程分支变成当前版本`
      - `git commit -a # 不用一个个 git add，而是把被跟踪的文件直接 commit`
      - `git commit --amend # 追加提交，内容合并到上一个版本`
        - 注意，如果追加到了已经 push 的版本，则需要 git push -f；
      - `git rm：从缓存区删除文件`
        - 注意危险：默认会把文件从工作区删除；
        - --cached：从缓存区删除，但保留在工作区；
        - -f：不管有没有提交到缓存区，都删掉文件（包括缓存区和工作区）；
      - `git fetch --all # 从远程读取所有数据，但不进行合并操作`
        - 接下来可以用：`git reset --hard origin/master`强制覆盖本地；
        - 注意此操作有丢失本地文件的风险；

- 分支指令：
  ```
  git fetch # 从远程下载新分支与数据；
  git branch # 列出分支；
  git branch {branchname} # 在当前分支创建出分支；
  git branch -d {branchname} # 删除分支；
  git checkout {branchname} # 切换分支；
  git checkout -b {branchname} # 创建并切换到新分支；
  git merge {branchname} # 将别的分支 merge 到当前分支；
  ```

<br>

#### Svn

---

1.  引用：
    - 官网：<https://tortoisesvn.net>；
2.  摘要：
    - 笔者建议使用 Git，Svn 不做详细记录；
3.  部署：
    - 略；
4.  使用：
    - 常用指令：
      ```
      svn co # 检出，带用户名检出的方法有两个
      svn —username={用户名} co # 带用户名检出；
      svn co svn+ssh://
      svn up # 更新；
      svn ci # 提交；
      ```

<br>

#### Privoxy

---

1.  引用：
    - 官网：<https://zookeeper.apache.org/>；
    - 参考：<https://www.cnblogs.com/shihaoyu/p/11126513.html>；
2.  摘要：
    - 这个软件的作用，请读者自行查阅，这里不方便写；
3.  部署：
    - HomeBrew 方式安装
      ```
      brew install privoxy
      ```
    - 打开/usr/local/etc/privoxy/config，找到 listen-address 0.0.0.0:8118，在下一行添加 forward-socks5 / {科学 ip}:{科学 port} .（此处请读者自行解决）；
    - 在 ~/.bash_profile 中添加：
      ```
      function proxy_off(){
          brew services stop privoxy
          unset http_proxy
          unset https_proxy
          git config --global --unset http.proxy
          git config --global --unset https.proxy
          echo -e "proxy off"
      }
      function proxy_on() {
          brew services start privoxy
          export no_proxy="localhost,127.0.0.1"
          export http_proxy="http://127.0.0.1:1087"
          export https_proxy=$http_proxy
          git config --global http.proxy $http_proxy
          git config --global https.proxy $http_proxy
          echo -e "proxy on"
      }
      ```
    - 同时设置了终端和 git 的代理，指向 privoxy 的监听端口，privoxy 再转发到科学界；
    - 如果是使用 zsh，则将上述代码写入 ~/.zshrc，因为 zsh 不使用 ~/.zsh_profile 文件；
4.  使用：
    - 开启：科学护体 -> proxy_on；
    - 关闭：proxy_off -> 回归真理；
    - 检验：
      ```
      curl ip.gs # 查看当前代理是否切换成功；
      curl www.google.com # 检验访问效果；
      ```

<br>

#### SwitchHosts

---

1.  引用：
    - 官网：<https://oldj.github.io/SwitchHosts/>；
    - Github：<https://github.com/oldj/SwitchHosts>；
2.  摘要：
    - 更方便地管理 hosts，可模拟测试域名，相比于采用 127.0.0.1/localhost 进行测试，www.company.local能更接近生产环境；
3.  部署：
    - HomeBrew 方式安装：
      ```
      brew install switchhosts
      ```
    - 添加测试域名。
      - 建议添加 127.0.0.1 {company}.local，或者 127.0.0.1 {company}.test，这样跟生产环境的区别只在于将 local/test 换成 com；
      - 将虚拟域名和 backup 都打开（以免影响其它软件）；
4.  使用：
    - 见官网文档；

<br>

#### Postman

---

1.  引用：
    - 官网：<https://www.postman.com>；
2.  摘要：
    - 用于模拟发送请求；
    - 如果读者使用 VSCode，建议使用 PostCode 插件代替；
3.  部署：
    - 从官网下载 dmg 安装（版本无要求）；
4.  使用：
    - 见官网文档；

<br>

#### DBeaver

---

1.  引用：
    - 官网：<https://dbeaver.io>；
2.  摘要：
    - 目前次好用而且免费的数据库连接工具，可完全取代收费的 Navicat；
3.  部署：
    - 从官网下载 dmg 安装（版本无要求）；
4.  使用：
    - 见官网文档；

<br>

#### ProseccOn

---

1.  引用：
    - 官网：<https://www.processon.com>；
2.  摘要：
    - 用于制作流程图；
3.  部署：
    - 从官网下载 dmg 安装（版本无要求）；
4.  使用：
    - 见官网文档；

<br>

#### JsonEditorOnline

---

1.  引用：
    - 官网：<http://jsoneditoronline.cn>；
2.  摘要：
    - 用于编辑和解析 Json 字符串；
3.  部署：
    - 在线使用；
4.  使用：
    - 见官网；

<br>

#### Glances

---

1.  引用：
    - 官网：<https://nicolargo.github.io/glances/>；
2.  摘要：
    - 用于监控 Linux 系统资源使用率；
    - C/S 模式，参考：<https://zhuanlan.zhihu.com/p/85935725>；
3.  部署：
    - pip 方式安装
      ```
      pip install glances
      ```
4.  使用：
    - 显示字段解释：
      - %CPU：该进程占用的 CPU 使用率
      - %MEM：该进程占用的物理内存和总内存的百分比
      - VIRT：虚拟内存大小
      - RES：进程占用的物理内存值
      - PID：进程 ID 号
      - USER：进程所有者的用户名
      - NI：进程优先级
      - S：进程状态，其中 S 表示休眠，R 表示正在运行，Z 表示僵死状态。
      - TIME+：该进程启动后占用的总的 CPU 时间
      - IO_R 和 IO_W：进程的读写 I/O 速率
      - Command：进程名称
    - 指令集：
      - h ：显示帮助信息
      - q ：离开程序退出
      - c ：按照 CPU 实时负载对系统进程进行排序
      - m ：按照内存使用状况对系统进程排序
      - i ：按照 I/O 使用状况对系统进程排序
      - p ：按照进程名称排序
      - d ：显示磁盘读写状况
      - w ：删除日志文件
      - l ：显示日志
      - s ：显示传感器信息
      - f ：显示系统信息
      - 1 ：轮流显示每个 CPU 内核的使用情况（次选项仅仅使用在多核 CPU 系统）

<br>

#### Vim

---

1.  引用：
    - 官网：<https://www.vim.org/>；
2.  摘要：
    - 流水的对手铁打的 Vim，基础技能不要忘；
    - 入门选手建议买一个 Vim 快捷键鼠标垫；
3.  部署：
    - 在 Shell 终端上，通常默认已安装；
    - 在 IDE 中，通常在插件市场中安装；
4.  使用：
    - TODO:快捷键：；

<br>

#### Oh-My-Zsh

---

1.  引用：
    - Github：<https://github.com/ohmyzsh/ohmyzsh>
2.  摘要：
    - 据说是最好用的 Shell，但据笔者试验，会导致很多破解软件（例如 Adobe 系列）的破解脚本失效，因此不推荐；
      - 号称能完全兼容 `!#/bin/bash` 开头的 Bash 脚本；
      - 但根据笔者实践，很多软件的安装脚本会受到影响，不推荐使用；
    - 对比 Bash 的优点：
      - 两次 Tab：可视化选择；
      - 自动大小写：可以试试`cat readme.md`
3.  部署：
    - curl 方式安装：
      ```
      sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
      ```
4.  使用：
    - 变更系统默认 Shell：
      ```
      chsh -s /bin/zsh
      ```
    - 变更主题：
      ```
      vim ~/.zshrc # 设置 ZSH_THEME="ys"
      ```
    - 配置环境变量：
      ```
      vim ~/.zshrc # 将原来 ~/.bash_profile 中的内容复制过来
      ```
    - 主题可以在 <https://github.com/ohmyzsh/ohmyzsh/wiki/Themes> 找>；

<br>

#### VSCode

---

1.  安装 CodeLLDB，解决 MacOS Catalina 不支持 lldb 的问题；

2.  Java 插件：

    - 安装 Extension Pack for Java，插件包会同时自动安装：
      - Language Support for Java(TM) by Red Hat：Java 语言支持，语法支持，格式化等；
      - Debugger for Java：暂不了解，TODO；
      - Java Test Runner：用于运行测试用例；
      - Maven for Java：Maven 支持；
      - Project Manager for Java：暂不了解，TODO；
      - Visual Code IntelliCode：暂不了解，TODO；
      - 如果是 HomeBrew 安装的话，最新的 VSCode for MacOS 已经不需要手动配置 JAVA_HOME 和 MAVEN_HOME：
    - 安装 Spring Boot Extension Pack，插件包会同时自动安装：
      - Spring Boot Tools：SpringBoot 开发辅助组件；
      - Spring Initializr Java Support：新建项目工具；
      - Spring Boot Dashboard：安装之后会在 Explorer 区多出一个 SPRING BOOT DASHBOARD 面板；
    - 安装 Alibaba Java Coding Guidelines，用于一键检查 Java 代码的 P3C 规范；
    - 安装 Lombok Annotations Support for VSCode，不然会一直提示 Lombok 标签错误；

3.  C++插件：

    - 安装 C/C++ Extension Pack，插件包会同时安装：
      - C/C++ 支持；
      - CMake 工具；
    - 安装 CodeLLDB，解决 MacOS Catalina 不支持 lldb 的问题；
    - 安装 C++ Intellisense，用于自动代码补全，实时错误检查，代码改进建议；

4.  C# 插件：

    - 安装 C# 支持；
    - 安装 Mono Debug，用于 C# 编译时调试；

5.  网页端插件：

    - 安装 ESLint，用于 JS 语法标准格式验证；
    - 如果使用 React，则安装 ES7 React/Redux/GraphQL/React-Native snippets 架构支持；
    - 如果使用 Vue2，则安装 Vetur，以获得语法和编辑器高级支持，在.vue 文件中输入 `vue` 可自动生成模板；
    - 如果使用 Vue3，则安装 Volar (Vue Language Features 和 TypeScript Vue Plugin，以及 Vue 3 Snippets)；
    - 安装 HTML Boilerplate，快速生成最佳的 HTML 页面，安装之后在.html 文件中输入 html 即可弹出 html-boilerplate 选项；

6.  远程插件：

    - 安装 Remote - SSH ，安装后在左侧的 Remote Explorer 中可连接远程主机；
      - Remote - SSH 使用配置文件 ~/.ssh/config
      - 配置格式为：
        ```
        Host {ip}
        HostName {ip}/{hostname}
        User root
        PreferredAuthentications publickey # 这两项是为了免密登录
        IdentityFile ~/.ssh/id_rsa_xxx # 这两项是为了免密登录
        ```
    - 安装 PostCode 插件，模拟 RESTfull 请求进行测试，安装后在左侧会多出 PostCode 栏；

7.  容器插件：

    - 安装 Docker/Kubenetes 支持，安装后左侧会多出 Docker/Kubenetes 模块；
    - TODO:安装 Cloud Code，据说可以很轻松驾驭 Kubernetes；

8.  项目插件：

    - 安装 Chinese (Simplified) Language Pack for Visual Studio Code，官方中文包，全局搜索 Configure Display Language 切换，但笔者建议用英文；
    - 安装 Project Manager 项目管理插件，快速切换不同项目；
    - 安装 TODO Heighlight 和 Todo Tree，用以管理 Todo 列表，左侧栏会多出一棵树；

9.  编辑插件：

    - 安装 Vim 插件，基础技能不能忘，通过插件的 enable 和 disable 来开关；
    - 安装 Prettier - Code formatter 插件，可以作为大部分语言的 Lint，尤其是 Markdown 最优的格式化工具；
      - 如果针对特定语言(比如 JS/TS) VSCode 有自带的 Formatter，那么选择 Formatter 的时候优先使用自带的；
      - 如果针对特定语言(比如 Vue3)有更专业的 Formatter 插件(比如 Volar)，那么选择 Formatter 的时候优先使用专业的；
      - 重新选择 Formatter 的方法：cmd + shift + p，找到 Format Document With 选项重新设置；
    - 安装 any-rule 正则大全，省去宝贵时间，cmd+shift+p，输入 zz 搜索正则表达式；
    - 安装 YAML 插件，用于自动格式化 Yaml 格式；
    - 安装 Format Files 插件，可以在 Explorer 区对文件按目录进行格式化（建议先在 Git 中 Commit 备份）；
    - 安装 Markdown Preview Enhanced 插件，可以在 Markdown 预览中显示流程图(`mermaid`代码段)；
    - 安装 shell-format 插件，用于自动格式化各种 Shell 脚本格式，包括 .sh/.bash/.gitignore/.dockerignore/.properties/.vmoptions 等；

10. Code Runner，各种语言快速运行助手，配置如下：

    - 调整运行设置，全局搜索 Preferences: Open Settings (JSON)，添加：
      - "code-runner.clearPreviousOutput": true,//每次运行之前清一下之前的输出；
      - "code-runner.runInTerminal": true,//打开终端执行；
      - "code-runner.saveAllFilesBeforeRun": true,//运行之前保存所有文；
    - 调整 C++选项编译选项，全局搜索 Preferences: Open Workspace Settings (JSON)，添加：
      - "code-runner.executorMap":{"Cache": "cd $dir && g++ $fileName -o $fileNameWithoutExt.out -std=c++17 -Wall -O0 -g && $dir$fileNameWithoutExt.out"}；

11. VSCode 快捷键（Vim 之外的）：

    - 文档：<https://code.visualstudio.com/shortcuts/keyboard-shortcuts-windows.pdf>；
    - cmd+,：打开设置；
    - cmd+shift+p：全局搜索设置；
    - cmd+shift+e：Explorer 栏；
    - cmd+shift+d：运行栏；
    - cmd+shift+f：查找栏；
    - cmd+shift+h：替换栏；
    - cmd+shift+x：插件栏；
    - cmd+shift+v：预览 MarkDown；
    - cmd+shift+b：运行编译任务；
    - cmd+p：全局搜索文件；
    - alt+shift+f：用 Prettier 插件规整代码；
    - alt+o：在.h 和.cpp 之间切换；
    - f5：执行；

<br>

#### OpenSumi

---

1.  引用：
    - 官网：<https://opensumi.com/>；
    - Github：<https://github.com/opensumi/core>；
2.  TODO:摘要：
    - ；
3.  ## TODO:部署：
4.  TODO:使用：
    - ；

<br>

#### CI/CD

---

1. 笔者推荐参考 Facebook 实践：Jenkins + Git 仓库 + SonarQube + Linter + UnitTest + Phabricator；
2. 参考：<https://blog.csdn.net/xiaolixi199311/article/details/111564824>；

<br>

#### Jenkins

---

1. 官网：<https://www.jenkins.io/zh/>；
2. 前身是 Sun 开发 Hudson，由于 Oracle 的收购，社区启用 Jenkins 开发后续版本；
3. HomeBrew 方式安装

   - 因项目更新快，建议安装 lts 版本；
   - `brew install jenkins-lts`
   - `brew upgrade jenkins-lts # 只更新到 lts 版本`

4. 启动/关闭服务：brew services start/stop jenkins-lts
5. 默认端口：8080
6. 初始访问入口：<http://localhost:8080>；
7. 用默认 admin 账号进行初始化：

   - 访问 http://localhost:8080，等待初始化
   - 如果在这一步卡住很久，你需要看 Privoxy 那章；
   - 根据向导获取初始密码
   - 如果是通过命令行运行的 Jenkins，可以从命令行复制；
   - 如果是服务方式启动，找~/.jenkins/secrets/initialAdminPassword；
   - 当前正在操作的账户是超级管理员 admin，密码一次性失效；

8. 创建管理员用户；

   - 新建管理员账号例如 DEVjenkins；
   - DEVjenkins 密码：DEVjenkins123456

9. 此时会要求填入访问 Jenkins 的固定地址

   - 建议立即配置 nginx 或者 kong 网关；（详见 deploy/nginx 附件）
   - 以后也可以在：系统管理 .JenkinsLocation 中完善；

10. 此时应修改（或链接）配置文件，初始路径如下：

    - 任务文件：$JENKINS_HOME/jobs
    - 日志路径：$JENKINS_HOME/logs
    - 插件路径：$JENKINS_HOME/plugins

11. 常用插件：

    - 建议安装推荐插件；
    - SonarQube Scanner（详见下章）；

<br>

#### SonarQube

---

1. 官网：<https://www.sonarqube.org>；
2. 参考：<https://blog.csdn.net/zuozewei/article/details/84539396>；
3. 由四部分组成：

   - SonarQubeServer；
   - ComputeEngine；
   - SearchServer；
     - 内建 ElasticSearch；
     - 从 6.7 开始官方不建议使用外部 ElasticSearch；
   - WebServer；
   - SonarQubeDatabase；
   - SonarQubePlugins；
   - SonarQubeScanners；

4. 运行四个进程（对应四个日志）：

   - Main process（sonar.log）；
   - Web Server（web.log）；
   - Compute Engine（ce.log）；
   - Elasticsearch（es.log）；

5. HomeBrew 方式安装（因文档更新快，建议安装 8.7 以上的版本）：

   ```
   brew install sonarqube
   ```

6. 此时应修改（或链接）配置文件，初始路径如下：

   - 基础路径：安装路径/libexec/
   - 配置路径：安装路径/libexec/conf/
   - 数据路径：安装路径/libexec/data/
   - 日志路径：安装路径/libexec/logs/

7. 启动/关闭服务：brew services start/stop sonarqube
8. 登录并修改 admin 密码

   - 初始登录账户密码：admin / admin
   - admin 账户新密码：{密码}

9. 插件：

   - 同样，SonarQube 安装插件也常需要 Privoxy 护体；
   - 如果需要中文，Administration.Marketplace.Plugins 中搜索 Chinese Pack 安装并重启； 3.

<br>

#### Linter

---

1. TODO；

<br>

#### Phabricator

---

1. TODO；

<br>

#### 测试

---

1. 可配置的环境逻辑包括：

   - 是否支持 debug；
   - 环境的域名；
   - 是否启用 <https>；
   - 日志打印配置；
   - 是否使用第三方服务（发短信等）；

2. 在 common-lib 中的 EnvConfig 实现 DEV/TEST/UAT/PROD 环境的切换开关；
3. 单元测试（UT/Unit Test）：

   - 依赖隔离、自包含；
   - 用内存数据库测试：H2 等；
   - JUnit5/Mockito；
   - 使用单独的 H2 数据库更方便 Repository 测试；
   - 单元测试之前和之后应做清空操作；

4. 集成测试（IT/Integration Test）：

   - 通常使用 Mock 工具模拟服务依赖；
   - 通常用 In-Memory db 模拟数据存储依赖，如 H2；

5. 组件测试（CT/Component Test）：

   - 内部 Mock：WireMock/Spring-mockito-mock-mockbean；
   - 外部 Mock：HoverFly/mbtest；

6. 契约测试（Contract Test）：

   - Pact/Spring-Cloud-Contract；

7. 端到端测试（E2ET/End to End Test）：

   - Selenium WebDriver/Rest-assured；

8. 测试过程总结：

   - 测试投入的资源金字塔依次从 1 到 5 递减（出于复杂性控制考虑）；
   - 在 application.properties/application.yml 中进行测试环境和变量的预设，通常包括：
   - 测试数据库、账号、密码；
   - 测试内存、账号、密码；
   - 测试 url、测试变量；
   - 日志等级、控制台输出；

9. Mock 和 Spy：（TODO）；
10. 性能测试：

    - JMeter：重交互，门槛低，非研发也可用，但比较难集成；
    - Gatling：重集成 CI/CD，UI 弱一些，报表部分不错；

<br>

#### 性能指标

---

1. 高可用性：TODO；
2. 横向扩展：TODO；
3. 版本兼容：TODO；
4. 安全性：TODO；
5. 调用链跟踪：TODO；
6. 健康检查：TODO；
7. Metrics 监控：TODO；
8. 日志监控：TODO；
9. 配置管理：TODO；
10. 分环境部署：TODO；
11. 环境灰度发布、蓝绿发布：TODO；

<br>

#### 本地构建和测试

---

```
mvn clean package -DskipTests # Maven 打包
docker-compose build && docker images # 生成 Docker 镜像
docker-compose up && docker-compose ps # 测试镜像
docker-compose down # 关闭测试
```

<br>

#### MarkDown

---

```mermaid
graph TD;
A-->B;
A-->X;
A-->C;
B-->D;
C-->D;
```

```mermaid
sequenceDiagram
    participant 企业
    participant 下游
    企业->>移动: 调度
    loop 心跳检测
        移动->>移动: SDK
    end
    Note right of 移动: 详见文档 <br/>资料
    移动-->>企业: 接单
    移动->>下游: 推送
    下游-->>移动: 流程结束
```
