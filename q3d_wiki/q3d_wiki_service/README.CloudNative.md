## 《微服务工作笔记》-- 云原生（CloudNative）

<br>

#### 介绍

---

- 项目文档记录了笔者工作积累的云原生微服务技术笔记，以及元宇宙、数字孪生、程序化建模工作学习笔记；
- 项目包含一套元宇宙建模方法论和工程，采用 QGIS -> Blender / Houdini -> Unreal / Unity 的方式实现数字孪生虚拟城市；
- 项目包含一个 Unreal 客户端工程和一个 Unity 客户端工程（建设中）；
- 项目包含一个 Cesium 网页端工程，实现 WebGIS BI 展示；
- 项目包含一个 Vue.js / ElementPlus / TS 网页端工程，实现 Admin 管理后台；
- 项目包含一个 Java / SpringBoot 微服务工程和一个 C# / DotNet 微服务工程（建设中），共用网关和服务注册中心（Nacos），可以协同工作；
- 项目包含一个 Publish 部署工程，采用 Docker / Kubernetes / Jenkins 方式进行服务部署；
- 程序语言涉及 C++ / Java / C# / Python / TS / Shell / VEX；
- 错漏之处恳请读者不吝指正，转载望注明出处：<https://gitee.com/yexiang841/q3d_project/>；
- Copyright 叶湘 ( weixin:yexiang841, email:yexiang841@qq.com ), Shitouren Studio. All Rights Reserved.

<br>

#### 微服务工作学习笔记

---

- [《微服务工作笔记》-- 部署说明（Deploy）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_service/README.Deploy.md)
- [《微服务工作笔记》-- 架构选型（Selection）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_service/README.Selection.md)
- [《微服务工作笔记》-- 数据接口（Interface）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_service/README.Interface.md)
- [《微服务工作笔记》-- 数据持久化（Persistence）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_service/README.Database.md)
- [《微服务工作笔记》-- 分布式组件（Distributed）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_service/README.Distributed.md)
- `>>>>` [《微服务工作笔记》-- 云原生（CloudNative）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_service/README.CloudNative.md)
- [《微服务工作笔记》-- 持续集成部署（DevOps）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_service/README.DevOps.md)
- [《微服务工作笔记》-- 系统安全（Security）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_service/README.Security.md)
- [《微服务工作笔记》-- 数据治理（Governance）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_service/README.Governance.md)
- [《微服务工作笔记》-- Java 微服务（SpringBoot）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_service/README.SpringBoot.md)
- [《微服务工作笔记》-- DotNet 微服务（DotNet）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_service/README.DotNet.md)
- [《微服务工作笔记》-- 网页端工程（Frontend）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_service/README.Frontend.md)

<br>

#### 本章摘要

---

1.  更新状态：持续整理上传中；
2.  最后更新：2021.10.20；
3.  本章记录云原生/容器化技术的概念和用法；

<br>

#### Server 端微服务架构中的组件解析

---

- 容器：为了规避不同系统的环境带来的影响，解耦环境对进程的影响，可以考虑从开发阶段就进行容器化验证；
- 容器网络：在容器之间建立可通信的网络，告别手动配置服务网络节点；
- 容器部署：基于容器，可以实现一键部署，是敏捷开发和持续集成的重要基石；
- 容器治理：如何拉取、配置和部署公共组件，如果管理服务容器，如何监控容器状态，如何动态扩容；

<br>

#### Docker / Docker-Compose

---

1.  容器化技术是原生微服务架构的基础之一；
2.  官网：<https://www.docker.com>；
3.  仓库：<https://hub.docker.com>；
4.  dmg（软件方式）安装，建议最新版；
5.  在软件中设置共享路径，笔者的软件和工程均放在/opt，需要在 Preference.Resources.FileSharing 中添加/opt；
6.  每个微服务配置一个 Dockerfile；
7.  工程配一个 docker-compose.yml；
8.  约定 Docker-Compose 私密配置从.env 中读取，.env 不提交 Github；
9.  建议依照后面 Privoxy 一节设置科学护体，避免换镜像；
10. 换国内镜像源【不推荐】
    - 笔者此刻搜索记录，不保证长期有效：
    - 国内官方：<https://registry.docker-cn.com>；
    - 163：<http://hub-mirror.c.163.com>；
    - 阿里云 1：<https://3laho3y3.mirror.aliyuncs.com>；
    - 阿里云 2：<https://smyuxec9.mirror.aliyuncs.com>；
    - 道客：<http://f1361db2.m.daocloud.io>；
    - 腾讯云：<https://mirror.ccs.tencentyun.com>；
    - 华中科大：<https://docker.mirrors.ustc.edu.cn>；
    - 中科大：<https://docker.mirrors.ustc.edu.cn>；
11. 将镜像地址添加到软件的 Preferences.DockerEngine.registry-mirrors 中（注意遵守 Json 格式）；
12. 设置容器服务的日志大小；
13. Docker 网络操作：
    - `docker network list # 查看本地网络和类型，类型有三种`
      - host：共享宿主机网络；
      - bridge：创建一个独立的网络（可以让很多容器）
      - none：不使用网络；
    - `docker network inspect {网络名} # 查看网络细节`
    - `docker network rm {网络名} # 删除多余网络`
    - `docker network create --driver bridge --subnet=172.20.0.0/16 --gateway=172.20.0.1 {网络名} # 创建一个网络并指定 ip 地址段`
14. 宿主机在容器中的 IP 地址（bridge 模式）：
    - 如果是 CentOS，执行 ifconfig 会发现系统自动为宿主机在每一个 bridge 网络都定义了 ip 地址，可以在容器内直接使用；
    - 如果是 Windows，可能需要添加一个路由规则（笔者不熟）；
    - 如果是 MacOS，需要用 openvpn 来打通容器内访问宿主机 ip；
      - 引用<https://www.cnblogs.com/freephp/p/14193507.html>；
15. Docker 镜像操作：
    - `docker images # 查看本地镜像`
    - `docker inspect {仓库名}/{镜像名}:{版本号} # 查看镜像元信息（常用于查看版本）`
    - `docker search mysql # 搜索仓库镜像`
    - `docker pull redis # 下载公共镜像`
    - `docker pull nginx:1.19.7 # 下载公共镜像的某个版本`
    - `docker pull {仓库名}/{镜像名}:{版本号} # 下载私有镜像`
    - `docker rmi {仓库名}/{镜像名}:{版本号}或者{镜像 ID} # 删除镜像（建议先停止运行）`
    - `docker pull {仓库名}/{镜像名}:latest # 升级镜像`
    - `docker build -f {Dockerfile 所在路径} -t {仓库名}/{镜像名}:{版本号}或者{镜像 ID} # 自建镜像`
16. Docker 容器操作：
    - `docker ps # 查询运行中的容器`
      - -a：包括未启动的容器；
      - -s：显示运行中的容器的大小；
    - `docker create -it/-d --name {起个好记的名字} {仓库名}/{镜像名}:{版本号} # 装载镜像到容器（但不运行）`
      - -it：以交互模式装载容器以及分配一个伪终端（意思是运行时可以进入容器的 bash）；
      - -d：以守护进程模式装载容器；
    - `docker run -it/-d {容器 ID/Name} # 第一次装载+运行`
      - 参数与 create 相同；
      - 以 docker run -it 方式进入容器之后，如果想退出：
        - exit：退出并关闭容器；
        - ctrl+p+q：退出不关闭容器；
        - -p {宿主端口}:{容器端口}；
    - `docker start/stop/restart/kill {容器 ID/Name} # 运行 / 停止 / 重启 / 强制杀死已存在的容器`
      - `docker stop $(docker ps -aq) # 停止全部运行中的容器`
    - `docker rm {容器 ID/Name} # 删除容器`
      - -f：强制删除运行中的容器；
      - `docker rm $(docker ps -aq) # 删除全部容器`
    - `docker inspect {容器 ID/Name} # 查看容器元信息（常用于查看版本/系统/网络等）`
    - `docker top {容器 ID/Name} # 运行容器中的 top 指令（参数同 bash 的 top）`
    - `docker logs {容器 ID/Name} # 查看容器运行日志`
      - -f：跟踪日志；
      - -t：带时间戳；
      - --tail={整数}：同 bash 的 tail 指令；
    - docker cp # 本地与容器间拷贝文件
      - `docker cp {容器 ID/Name}:/{container_file} {local_path}`
      - `docker cp {local_file} {容器 ID/Name}:/{container_path}/`
      - `docker cp {local_file} {容器 ID/Name}:/{container_file}`
    - `docker attach --sig-proxy=false {容器 ID/Name} # 登录到正在运行的容器`
      - 如果容器正在运行 bash，就会进入 bash;
      - 如果容器正在运行别的进程，则会进入终端查看输出;
      - 不加--sig-proxy=false 就会因为 ctrl+c/d 而直接 stop 容器；
    - `docker exec -it/-d {容器 ID/Name} {bash 指令} # 在正在运行的容器中执行指令`
      - -it：以交互模式装载容器以及分配一个伪终端（意思是运行时可以进入容器的 bash）；
      - 执行指令：`docker exec -it mynginx ls -l /opt`
      - 执行脚本：`docker exec -it mynginx /bin/sh something.sh`
      - 新建终端：`docker exec -it mynginx /bin/bash`
    - `docker container prune # 一键删除全部容器`
    - `docker image prune # 一键删除全部镜像`
    - `docker system prune：一键删除全部容器和镜像`
17. Dockerfile
    - 参考：<https://yeasy.gitbook.io/docker_practice/image/dockerfile>
    - ## CMD、RUN、ENTRYPOINT 的区别；
18. Docker-Compose 版本：

    - 查看版本：可根据 Docker Engine 的版本号，从官方文档查看 docker-compose.yaml 文件中的 version 语法对应的版本号：
      - 文档：<https://docs.docker.com/compose/compose-file/>；

19. Docker-Compose 操作：
    - docker-compose 的操作要在 docker-compose.yml 文件路径下执行；
    - 以下指令都表示针对当前 docker-compose.yml 文件中定义的容器；
    - `docker-compose build # 从 docker-compose.yaml 文件拉取相关镜像和创建镜像`
    - `docker-compose up -d # 装载 docker-compose.yaml 文件中的镜像运行容器`
      - up 指令会探测配置文件变化，如果有变化会删掉之前的容器，重新创建一个；
      - -d（-d 表示后台执行）；
      - --no-create：不重建容器，直接启动；
    - `docker-compose down # 停止（并删除）容器`
    - `docker-compose rm # 删除容器`
    - `docker-compose stop # 停止（不删除）容器`
    - `docker-compose start # 启动容器`
    - `docker-compose restart # 重启容器`
    - `docker-compose run nginx ping www.baidu.com # 在指定容器上执行命令`
    - `docker-compose restart # 重启容器`
    - 其它 docker-compose 操作，参考：<https://www.cnblogs.com/minseo/p/11548177.html>；
20. docker-compose.yml 文件解析：
    - link 参数：将别的服务名和 IP 添加到自己的 host，以便在容器内用服务名直接访问对方；
    - depends_on 参数：等待别的服务启动后才启动自己；

<br>

#### ContainerD

---

1. 因 K8S 在推进解绑 Docker，在此做个简单记录；
2. K3S 默认使用的也不是 Docker 而是 ContainerD；
3. Docker 中的容器引擎也是 ContainerD；
4. 因容器都遵循 OCI（OpenContainerInitiative），并兼容 K8S 的 CRI（ContainerRuntimeInterface），所以 ContainerD 与 Docker 指令集基本一样；
5. 以下记录不同的指令集（从 Docker 迁移到 ContainerD 比较）：
   1. 镜像操作：
      - docker inspect -> crictl inspecti；
6. ContainerD 独有的 Pod 操作：
   1. 查询 Pod 列表：crictl pods；
   2. 查看 Pod 详情：crictl inspectp；
   3. 运行 Pod：crictl runp；
   4. 停止 Pod：crictl stopp；

<br>

#### Kubernetes（以下称 K8S）

---

1. 重要概念：CNCF - 云原生计算基金会，中文社区：<https://www.kubernetes.org.cn/tags/cncf>；
2. 官网：<https://kubernetes.io>；
3. 中文文档：<http://docs.kubernetes.org.cn>；
4. 最佳教程：<https://jimmysong.io/kubernetes-handbook/>；
5. YAML 格式：
   1. yml / yaml 是一种比 xml 简约、比 json 易读，且基本等效的文本格式，常用于配置文件；
   2. 简易教程：<https://www.runoob.com/w3cnote/yaml-intro.html>；
6. 架构：多 Master + 多 Worker；
7. Master Node 四件套：
   1. ETCD：基于 K-V 的元数据存储；
   2. API server：对外操作接口，同时也是 ETCD 的代理；
   3. Scheduler：调度总管；
   4. Controller Manager：确保实际状态和 ETCD 中的预期状态最终保持一致；
8. Worker Node 四件套：
   1. kubelet：监控节点状态的 agent，负责下载镜像和运行容器，确保容器健康，接收 Master Node 中 API server 的指令；
   2. Container Runtime：容器运行环境；
   3. Pod：对容器（目前特指 Docker）的包装；
   4. kube-proxy：负责对 Pod 进行服务发现和负载均衡；
9. Overlay Network：Pod 之间通信；
10. Load Balance：外部访问 K8S；
11. 外围四件套：
    - Storage Service：（TODO）；
    - Monitoring：（TODO）；
    - Logging：（TODO）；
    - Analytics：（TODO）；
12. Pod：基本调度单位，引入 Pod 是为了与容器解耦；
    - 通常一个 Pod 只运行一个容器；
    - 一个 Pod 运行多个容器时，多个容器共享文件系统、IP、端口，而且可以通过 localhost 互相访问，适合需要 Sidecar 的场景；
13. Pause 容器：每个 Pod 中会有一个基础而特殊的 Pause 容器（区别于业务容器），用来为业务容器提供：
    - PID 命名空间：Pod 中不同进程可以互相看到；
    - 网络命名空间：Pod 中多个容器可以访问同一个 IP 和端口范围；
    - IPC 命名空间：Pod 中多个容器可以使用 SystemV IPC 或 POSIX 消息队列进行通信；
    - UTS 命名空间：Pod 中多个容器共享一个主机名，共享 Volume；
14. Deployment：发布控制器，将 Pod 集合分成四种基本发布单元（ReplicaSet / StatfulSet / Job/CronJob），支持滚动/蓝绿发布等高级机制；
    - ReplicaSet：支持无状态应用的发布，可实现 HA，当启动 Pod 的时候，在 yaml 或者 json 配置会指导如何替换和回滚；
    - StatfulSet：支持有状态应用的发布，如 mysql 或者 Redis；
    - Job：支持一次性运行的任务；
    - CronJob：支持周期性运行的任务；
15. Service：外部通过 Service 直接访问服务，屏蔽了寻址和 Pod 负载均衡；
16. ConfigMap：
    - K8S 标准的配置中心；
    - 可以直接作为环境变量传递给服务，也可以以持久卷 Volume 的形式 Mount 到 Pod 当中；
    - 通过 Secret 配置敏感数据；
17. DaemonSet：每个 Worker Node 一个 DaemonSet，用来部署日志采集或者 Prometheus 等；
18. Volume：
    - 往 Pod 中挂载磁盘的原理：Pod=>Volume=>PersistentVolumeClaims=>PersistentVolume=>物理存储；
    - PV 是一个存储的抽象概念，可以让 Pod 对接各种本地或者云存储；
    - PVC 是应用申请 PV 时需要填写的配置规范（磁盘大小、类型等）；
    - PV 和 PVC 结合使用，使得 Volume 和具体物理存储解耦；
19. Label/Selector：Pod 的标签系统；
20. NameSpace：逻辑分类/隔离；
21. Readiness Probe：就绪探针，通过应用健康检查端点判断 Pod 是否可以接入流量；
22. Liveness Probe：存活探针，通过应用的健康检查端点判断 Pod 是否存活，决定是否重启；
23. K8S 的网络：
    - 所有的 Worker Node 在一个 IP 地址空间（节点网络，就像外网一样，是用物理网卡实现的）；
    - 每个 Worker Node 上的所有 Pod 在同一个 IP 地址空间（Pod 网络，形似内网，是用虚拟网卡实现的）；
    - 在应用中，Pod 网络是被 Service 网络屏蔽起来的；
      - Pod 是以集群形式（ReplicaSet/Service）对外提供服务，所以需要屏蔽 Pod 网络；
      - Service 使用 Cluster IP 地址空间，同样是虚拟网卡实现的；
      - 实现自动寻址的组件是 Linux 内核的 Netfilter+Kube-proxy（v1.2 版本以前），Kube-proxy 会根据某种负载均衡规则去选择远端 Service 中的 Pod；
      - 新版本（v1.2 版本以及以后）实现自动寻址的组件是 Netfilter+IPTables/IPVS；
      - Service 会因更新或迁移发生地址变化，只使用服务名寻址，K8S 使用 DNS Service 组件屏蔽 Cluster IP；
    - Kube-proxy+Netfilter 可实现 NodePort：将 Service 暴露在节点网络上；
    - Load Balance 的作用是为暴露在节点网络上的 Service 实现负载均衡；
    - Flannel 网络：让集群中不同节点主机创建的 Docker 容器具有全集群唯一的虚拟 IP 地址；
      - 参考：<https://www.jianshu.com/p/e4c7f83a2a0b>；
      - （TODO）；
    - Ingress 的作用是在 Service 之上做反向代理+网关；
24. K8S 部署配置文件：
    - （TODO）；
25. K8S 在预生产环境（UAT）和生产环境（PROD）的几种部署方案：
    - 使用云服务商在云上提供的 K8S：
      - Hosted Solutions（Serverless 版）：云服务商（如阿里云）负责搭建、管理、运维，用户直接用；
      - Turnkey-Cloud Solutions（托管版）：云服务商负责搭建、管理、运维 Master 节点，用户自定义 Node 节点；
      - Turnkey-On-Premises Solutions（专有版）：云服务商只负责搭建 K8S 基础，Master 节点和 Node 节点均由用户自定义；
        - 阿里云定制版本：
          - K8S 和服务部署到 VPC；
          - 网关部署到 ELB；
          - 数据库部署到 RDS；
    - 在服务器上自行安装的模式：
      - 使用 Kubeadm，Kubeadm 是 CNCF 官方推荐的 K8S 安装工具；
      - 若不使用 Kubeadm，则可以按照官方文档完全自定义安装，对运维基础要求较高；
      - 使用 Rancher 的 RKE 搭建 K8S（见后章）;
    - 本例在预生产环境（UAT）和生产环境（PROD）选用 Rancher/K8S 方案；
26. kubectl 常用指令：
    - 首先安装 kubectl（Kubernetes cli），三种安装方式：
      - （推荐）HomeBrew 安装方式：brew install kubernetes-cli；
      - （不推荐）按照官方文档，同过 curl 下载二进制 kubectl 文件，修改权限后 mv 到/usr/local/bin/；
      - 因 Docker Desktop 安装后默认自带有 kubectl，也可以 kubectl.docker 替代 kubectl 使用；
        - ln -s /Application/Docker.app/Contents/Resource/bin/kubectl /usr/local/bin/kubectl；
        - 重开终端；
    - 集群状态：kubectl cluster-info；
    - 集群节点：kubectl get no；
    - Pod 列表：kubectl get pod；
      - -A：所有名空间；
      - -n：指定名空间，如-n kube-system；
      - -o：指定格式，常用的有 wide / json；
    - 单个 Pod 状态：kubectl get pod {pod-name} -n {namespace}；
    - 单个 Pod 详细信息：kubectl describe pod {pod-name} -n {namespace}；
    - 单个 Pod 的日志：kubectl logs pod {pod-name} -n {namespace}；
      - -f：实时追加查看；
    - 登入运行中的 Pod：kubectl -n kube-system exec -it {pod-name} sh；
    - 查询 Deployment：kubectl get deployment -A；
    - 查询 ReplicaSet：kubectl get rs -A；
    - 查询 Service：kubectl get svc -A；
    - 一次列出全部 Pod / Service / Daemonset / Deployment / ReplicaSet：kubectl get all -A；
    - 打开调试日志：参--v=8，举例：kubectl--v=8 get pods；
    - 清理/删除服务：
      - 先删部署：kubectl delete deployments；（加 -all 删除全部，慎用）
      - 再删服务：kubectl delete services；（加 -all 删除全部，慎用）
      - 再删配置：kubectl delete configmaps；（加 -all 删除全部，慎用）
    - 对 Deployment 扩容（调整 Pod 数）：kubectl scale --replicas={pod-count} deployment/{deployment-name}；
    - 其余对 Deployment、Pod、ReplicaSet 的操作，笔者建议通过部署配置文件而非指令执行；
27. 当研发环境或者生产环境的 K8S（或者 K3S）集群多于一个时，通过 kubeconfig 文件管理集群上下文的切换： 1. kubeconfig 文件可以叫做任何名字，通过 KUBECONFIG 环境变量指向一个 yml 文件即可：export KUBECONFIG={文件路径}； 2. kubeconfig 文件有七个字段： 1. apiVersion / kind / preferences：固定字段，声明文件类型； 2. clusters：集群名数组； 3. users：拥有访问权限的用户数组； 4. contexts：集群上下文数组，每个上下文其实是 cluster+user 的组合； 5. current-context：当前正在使用的上下文； 3. 改写 kubeconfig： 1. 集群创建工具一般会自动写入； 2. 手动修改可以使用 kubectl config 指令，在缺省情况下会操作~/.kube/config 文件，也可通过--kubeconfig 参数指定文件路径； 1. 配置 clusters： 2. kubectl config set-cluster {cluster-name} \
     --server=${API_Server_Addr} \
--certificate-authority=${CA_PATH} \
     --embed-certs=true 3. 配置 users： 4. kubectl config set-credentials {user-name} \
     --client-certificate=${Client_Cert} \
--client-key=${Client_Private_Key} \
     --embed-certs=true 5. 配置 contexts： 6. kubectl config set-contexts {context-name} \
     --cluster={cluster-name} \
     --user={user-name} 7. 切换 current-context： 1. kubectl config use-context {context-name}； 4. 有一个国内开源项目 kubecm 可简化 kubeconfig 操作（其 Github 链接的中文文档需要扫码关注才给看 - -！，不作推荐）； 5. 建议使用 Dashboard 或 Rancher 管理多集群；

<br>

#### Helm

---

1. Helm 是当前 K8S 首选的包管理工具；
2. 官网：<https://helm.sh>；
3. Github：<https://github.com/helm/helm>；
4. 官方仓库 1：<https://hub.helm.sh/>；
5. 官方仓库 2：<https://hub.kubeapps.com/>；
6. Helm 的初衷：将部署一套 K8S 应用需要的 Deployment 和 Service 的 yaml 文件打包在中央仓库里，以方便构建和迁移。
7. Helm 的的软件包单元是 Chart，包含一个应用所有的 Kubernetes manifest 模版，类似 Docker 的 Image；
8. Helm 的部署单元是 Release，Chart 被 Helm 使用一次，就部署一个 Release 到 K8S 集群中，类似 Docker 的 Container；
9. Chart 的仓库是 Repo，Helm Hub 拥有大量的官方 Repo，类似 Docker Hub；
10. 从 Helm3 开始弃用 Tiller，使用 config 文件进行 RBAC 认证；
11. HomeBrew 方式安装：brew install helm；
12. 使用 Helm 的第一步是添加 Repo，首选的 Repo 是官方的 stable：helm repo add stable <https://kubernetes-charts.storage.googleapis.com/>；
13. Helm 常用指令：
    - helm repo add {repo-name} {repo-url} # 添加 Repo
    - helm repo list # 查询已添加的 Repo
    - helm repo update # 更新 Repo 缓存
    - helm repo remove {repo-name} # 删除 Repo
    - helm search repo {repo-name} {chart-name} # 从添加的 Repo 中搜索 Chart
    - helm search hub {chart-name} # 从 Helm Hub 中搜索 Chart（建议安装 stable 下的 Chart）
    - helm install {release-name} {chart-url}/{local-tgz}/{local-path}/{tgz-url} -n {namespace} # 从 Chart 安装 Release
    - helm install --repo {repo-url} {release-name} {chart-name} -n {namespace} # 从 Repo 中指定 Chart 安装 Release
    - helm list -n {namespace} # 查询已安装的 Release
    - helm status {release-name} -n {namespace} # 查看某个 Release 的部署状态
    - helm uninstall {release-name} -n {namespace} # 卸载 Release
    - helm pull {repo-name}/{chart-name} # 下载 Chart（得到 tgz 压缩包）
14. 不同于 Docker 的 Image 和 Container，Helm 的部署环境（K8S）相对复杂，所以按默认配置部署 Release 是大概率不适用的。
15. 更普遍的做法是下载 Chart，解压 tgz 包，修改 Chart.yaml/values.yaml 等配置文件，再通过 helm install {local-path}的方式部署 Release；
16. Chart 包的结构：
    - Chart.yaml：一些前置声明，必须的只有 apiVersion、name、version；
    - values.yaml：参数配置文件，要定制安装主要修改这里；
    - templates 目录：K8S 部署所需要的 yaml 文件；
    - charts 目录：Chart 所依赖的别的 Chart；

<br>

#### K8S 在测试环境（TEST）的几种安装方式

---

1. Minikube：
   1. 官方提供的在单机上运行单节点的版本，可用于学习或测试；
   2. 官网：<https://kubernetes.io/docs/tasks/tools/install-minikube/>；
   3. Github：<https://github.com/AliyunContainerService/minikube>；
2. MicroK8S：
   1. 号称最快最小的单节点 K8S；
   2. 官网：<https://microk8s.io>；
3. KinD：在容器中运行 K8S，此理念受到官方测试环境支持（见后章）；
4. Rancher + K3S + K3D（见后章）；
5. Docker Desktop 自带的 K8S（缺点是不能选择 K8S 版本）；
6. 阿里的 k8s-for-docker-desktop：
   1. 可在网络不科学的条件下使用；
   2. Github：<https://github.com/AliyunContainerService/k8s-for-docker-desktop>；
   3. 执行上述 Github 中的./load_images.sh，下载 K8S 所需镜像；
7. 本例在开发环境和测试环境选用 KinD 方案；

<br>

#### KinD

---

1. KinD 是指 K8S in Docker，让 K8S 在 Docker 容器中运行，也就是说可以利用 Docker 在一台设备上模拟多节点，从而运行多节点的 K8S；
2. Github：<https://github.com/kubernetes-sigs/kind>；
3. 安装： 1. 推荐 HomeBrew 安装方式：brew install kind； 2. 用 go 安装（brew install go）：GO111MODULE="on" go get sigs.k8s.io/kind@v0.8.1； 3. 安装二进制文件： 4. curl -Lo ./kind "https://kind.sigs.k8s.io/dl/v0.8.1/kind-$(uname)-amd64"
   chmod +x ./kind
   mv ./kind /some-dir-in-your-PATH/kind

<br>

#### K3S/K3D

---

1. K3S 是 Rancher 发布的轻量级的 K8S 发行版，跟 K8S 保持一致，减少了文件大小和内存消耗，精简了运维操作；
   1. 官网：<https://k3s.io>；
   2. Github：<https://github.com/rancher/k3s/>；
   3. 参考：<https://zhuanlan.zhihu.com/p/125499493>；
   4. K3S 默认不用额外安装的组件：Kubelet / Kube-Proxy / Docker / ETCD / Ingress（如 Nginx）；
   5. K3S 会内置如下进程：API server / Controller Manager / Scheduler / Kube-Proxy / Flannel；
   6. K3S 甚至能安装到树莓派上，在 IoT 领域前景远大；
   7. K3S 的节点分为 server 和 agent 节点；
2. K3D 让 K3S 能够在 Docker 容器中运行，从而在单机搭建多 master+多 node 集群，比以往的虚拟机方式更优；
   1. 温馨提示：经笔者实测，以及对 github 进行的追踪，K3D 当前版本 bug 太多，在 3.1.0 发布之前慎用！
   2. 官网：<https://k3d.io>；
   3. Github：<https://github.com/rancher/k3d>；
   4. 参考：<https://mp.weixin.qq.com/s/H1TwCTI2sExyh-yHM30gng>；
   5. 在网络不科学的情况下，可以先从前述 Github 中下载对应系统的 k3s 镜像，然后在 k3d create 指令的参数-i 中指定本地镜像；
   6. K3D 所创建的 K3S 版本是 Rancher 官方发布的 latest 版本，若要更改版本，需要在 k3d create 指令的参数-i 中指定镜像版本号；
   7. 安装：
      - 原则上推荐 HomeBrew 安装方式：brew install k3d；
      - 这里有一个例外：当前 brew 只支持到 1.7.0 的版本，而官方 v3 版本存在大量重构（比如多 master），所以笔者选择官方脚本安装；
      - curl -s <https://raw.githubusercontent.com/rancher/k3d/master/install.sh | TAG=v3.0.0-beta.1 bash>；
      - 这种方法的卸载或升级可手动操作/usr/local/bin/k3d，建议读者通过 brew info k3d 关注 HomeBrew 上 K3D 的版本变化；
      - 在安装过程中如果网络不科学，可先到前述 Github 下载对应的镜像，修改 install.sh 脚本中的 download 模块以绕过下载；
   8. K3D 常用指令：
      - 增删查启停集群：k3d create / delete / get / start / stop cluster {cluster-name}；
      - 增删查启停节点：k3d create / delete / get / start / stop node {node-name}；
      - 增加集群参数：
        - -i 指定镜像（可在需要指定镜像版本号的条件下使用）；
        - -v 指定本地镜像（可在网络不科学的条件下使用）；
        - --api-port 指定集群端口，也就是 K3S 集群的 master 对外提供服务的端口，启用多个 k3s 集群应分配不同端口；
        - -p 因为 K3S 集群运行在了容器中，所以需要多做一步：从容器端口映射到主机端口。在本例中只需要映射网关端口即可；
        - --masters 指定 master 节点的数量；
        - --workers 指定 worker 节点的数量；
      - 增加节点参数：
        - -c 指定想要添加的 cluster 名；
        - 举例，往一个已存在的集群 a-cluster 中添加一个 master 节点：k3d create node new-master -c a-cluster --role master；
        - 举例，往一个已存在的集群 a-cluster 中添加一个 worker 节点：k3d create node new-master -c a-cluster --role worker；
      - 切换集群：k3d get kubeconfig {cluster-name} --switch；
   9. 建立一个基于 Docker 容器的 master x 2 + worker x 2 的集群：
      - 查询端口占用情况：lsof -nPi TCP:8000 -sTCP:LISTEN，没有占用则进行下一步，否则更改指令端口；
      - 新建两个集群，admin 集群运行 Rancher，dev 集群运行业务：
        - k3d create cluster admin-k3s-local --api-port 86443 -p 8080:80@loadbalancer -p 8443:443@loadbalancer --masters 1 --workers 2 --volume /opt/publish/rancher/registries.yaml:/etc/rancher/k3s/registries.yaml --volume /opt/publish/etc/localtime:/etc/localtime；
        - k3d create cluster dev-k3s-local --api-port 96443 -p 9080:80@loadbalancer -p 9443:443@loadbalancer --masters 1 --workers 2 --volume /opt/publish/rancher/registries.yaml:/etc/rancher/k3s/registries.yaml --volume /opt/publish/etc/localtime:/etc/localtime；
      - 最后一个参数是将镜像添加到 K3S 集群中，以解决网络不科学的问题；
      - 笔者亲测发现 master 数大于 1 时，重启集群会出错（kubectl 无法使用），在官方 github 提了 issue 之后，官方回应当前版本的 K3S 因为没有使用 ETCD（考虑效率），DQlite 中的某些机制导致了 bug，官方正在研发内嵌式 ETCD，需要等待实现，所以只能暂时考虑单 master 的情况。
      - 此时 k3d get cluster 可看到生成了 admin-k3s-local 集群和 dev-k3s-local 集群；
      - 此时 k3d get node 可看到每个集群生成了 4 个节点：master x 1 + worker x 2 + loadbalancer x 1;
      - 此时 docker images 可看到加载了两个必备镜像：iwilltry42/k3d-proxy 和 rancher/k3s；
      - 此时 docker ps 可看到每个集群运行了 4 个容器：iwilltry42/k3d-proxy x 1 + rancher/k3s x 3（server x 1 + agent x 2）；
      - 在 MacOS 环境中，K3D 会默认写入~/.kube/config 文件，可以通过 k3d get kubeconfig admin-k3s-local 或 k3d get kubeconfig dev-k3s-local 验证；
      - 通过 echo $KUBECONFIG 指令查看此前是否通过 KUBECONFIG 指向别位置的 kubeconfig，如果没有就可以通过~/.kube/config 管理所有集群了；
      - 注意，如果此前已存在 K8S 集群且没有使用~/.kube/config 文件，则需要通过 kubectl config 指令或手动将之前的 kubeconfig 文件合并进来；
      - 根据 K3D 的提示，用指令 export KUBECONFIG=$(k3d get kubeconfig admin-k3s-local)将 k3d 生成的配置文件作为 K8S 的配置；
      - 此时 cat ~/.kube/config 可看到新增了两个 context：k3d-admin-k3s-local 和 k3d-dev-k3s-local；
      - 通过 kubectl config current-context 查看 kubectl 当前指向哪个集群，通过 kubectl config use-context {context-name}进行切换；
      - 执行 kubectl cluster-info 可查询到当前指向的 K3S 集群的信息；
      - 此时 kubectl get no 可以得到当前集群的 node，k3d get node 可以得到所有集群的 node；
      - 也可用 kubectl get pod -A 查询当前 pod 的运行情况，会发现 kube-system 名空间下已经运行了一系列 K3S 内置的 pod，包括；
        - metrics-server；
        - local-path-provisioner；
        - helm-install-traefik（运行结束态）；
        - coredns；
        - svclb-traefik（多个）；

<br>

#### Rancher

---

1. Rancher 简化了部署使用 K8S 的流程，目标是 Run Kubernetes Everywhere；
2. 官网：<https://rancher.com>；
3. 文档：<https://rancher2.docs.rancher.cn>；
4. Rancher 中可方便地运行 Jenkins 容器，以部署持续集成（CI）流水线；
5. （不推荐）通过 Docker/Docker-Compose 安装 Rancher：
   1. 找到/opt/publish/rancher/，（如果读者并非使用/opt 路径请自行调整，下同）这里已有一个 docker-compose.yaml；
   2. 在此执行 docker-compose up -d；
   3. 假设上述 SwitchHosts 设定的测试域名为 company.local，则打开<https://company.local:8443/访问Rancher2>；
   4. 打开安装好的 Rancher 集群管理界面，点击导入，按照平台给出的引导指令执行即可导入；
   5. 这种方法安装的 Rancher 不能做到高可用性；
6. 预生产环境（UAT）和生产环境（PROD）下，官方建议单独建立一个高可用（或单节点）K8S/K3S，然后通过 Helm 安装 Rancher；
   1. 文档：<https://rancher.com/docs/rancher/v2.x/en/installation/k8s-install/>；
   2. 官方中文文档：<https://rancher2.docs.rancher.cn/docs/installation/_index>；
7. 使用 K3D 创建多节点测试环境，能够让 TEST、UAT、PROD 环境统一起来；（当前版本 K3S 还存在多 master 无法重启的 bug）
8. 通过 Helm 在本地 K3S 上安装 Rancher：
   1. 添加 Rancher 的 Repo：helm repo add rancher-latest <https://releases.rancher.com/server-charts/latest>；
   2. 切换到 admin 集群：kubectl config use-context k3d-admin-k3s-local 或者 k3d get kubeconfig k3d-admin-k3s-local --switch；
   3. 为 Rancher 创建 Namespace：kubectl create namespace cattle-system；
   4. 证书选项有两种，一种是通过 cert-manager 管理，一种是使用已有的证书（无论是 CA 机构证书还是自签证书）
   5. 本例测试环境（TEST）使用自签证书，在预生产环境（UAT）和生产环境（PROD）使用 CA 证书；
   6. 创建 tls 类型的 secret：kubectl -n cattle-system create secret tls tls-rancher-ingress --cert=/opt/publish/certs/rancher.company.local/tls.crt --key=/opt/publish/certs/rancher.company.local/tls.key；
   7. 创建 tls-ca 类型的 secret：kubectl -n cattle-system create secret generic tls-ca --from-file=/opt/publish/certs/rancher.company.local/cacerts.pem；
   8. Helm 安装 Rancher：helm install rancher rancher-latest/rancher --namespace cattle-system --set hostname=rancher.company.local --set ingress.tls.source=secret --set privateCA=true；
   9. 此时 kubectl get pods -n cattle-system 确认 Rancher 容器是否已经启动；
   10. 笔者的经验是如果在 k3d create 环节设置了可用的 registries.yaml 就能在节点中顺利下载镜像；
   11. 接下来查看 Ingress：kubectl get ingress -A；会有一个名为 rancher 的 Ingress；
   12. 接下来配置这个 Ingress：kubectl edit ingress rancher -n cattle-system；

<br>

#### Dashboard

---

1. Dashboard 兼容 K8S、K3S 集群
2. 虽然建议 Rancher 管理集群，但生产环境是否使用 Rancher 存在不确定性，所以 Dashboard 也一并安装，以兼容更多环境；
3. 文档：<https://rancher.com/docs/k3s/latest/en/installation/kube-dashboard/>；
4. Github：<https://github.com/kubernetes/dashboard>；
5. 安装 K8S 原装 Dashboard：
6. （不推荐）Docker 方式安装：
   1. 走近科学；
   2. kubectl apply -f <https://raw.githubusercontent.com/kubernetes/dashboard/v2.0.1/aio/deploy/recommended.yaml>；
   3. cd /opt/publish/rancher/，找到工程中准备好的两个配置文件：
      - dashboard.admin-user.yaml：定义管理员账户；
      - dashboard.admin-user-role.yaml：绑定管理员角色；
   4. 创建管理员账户：kubectl create -f dashboard.admin-user.yml -f dashboard.admin-user-role.yml；
   5. 找到 Bearer Token：kubectl -n kubernetes-dashboard describe secret admin-user-token | grep ^token；
7. Helm 方式安装：
   1. ；
8. 运行：
   1. 启动 Dashboard：kubectl proxy（守护进程）；
   2. 访问：<http://localhost:8001/api/v1/namespaces/kubernetes-dashboard/services/https:kubernetes-dashboard:/proxy/>；
   3. 首次访问需要输入 token，或者指定~/.kube/config 文件，都可以顺利登入；
9. 删除账户：kubectl delete ns kubernetes-dashboard；
10. 升级 Dashboard：先删除账户，然后照着安装和运行方法重做一遍（修改链接中的版本号）；
11. 通过配置 Nginx（见/opt/publish/nginx/nginx.conf），访问<http://company.local/admin/dashboard访问Dashboard>；
