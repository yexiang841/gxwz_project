## 《微服务工作笔记》-- 架构选型（Selection）

<br>

#### 介绍

---

- 项目文档记录了笔者工作积累的云原生微服务技术笔记，以及元宇宙、数字孪生、程序化建模工作学习笔记；
- 项目包含一套元宇宙建模方法论和工程，采用 QGIS -> Blender / Houdini -> Unreal / Unity 的方式实现数字孪生虚拟城市；
- 项目包含一个 Unreal 客户端工程和一个 Unity 客户端工程（建设中）；
- 项目包含一个 Cesium 网页端工程，实现 WebGIS BI 展示；
- 项目包含一个 Vue.js / ElementPlus / TS 网页端工程，实现 Admin 管理后台；
- 项目包含一个 Java / SpringBoot 微服务工程和一个 C# / DotNet 微服务工程（建设中），共用网关和服务注册中心（Nacos），可以协同工作；
- 项目包含一个 Publish 部署工程，采用 Docker / Kubernetes / Jenkins 方式进行服务部署；
- 程序语言涉及 C++ / Java / C# / Python / TS / Shell / VEX；
- 错漏之处恳请读者不吝指正，转载望注明出处：<https://gitee.com/yexiang841/q3d_project/>；
- Copyright 叶湘 ( weixin:yexiang841, email:yexiang841@qq.com ), Shitouren Studio. All Rights Reserved.

<br>

#### 微服务工作学习笔记

---

- [《微服务工作笔记》-- 部署说明（Deploy）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_service/README.Deploy.md)
- `>>>>` [《微服务工作笔记》-- 架构选型（Selection）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_service/README.Selection.md)
- [《微服务工作笔记》-- 数据接口（Interface）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_service/README.Interface.md)
- [《微服务工作笔记》-- 数据持久化（Persistence）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_service/README.Database.md)
- [《微服务工作笔记》-- 分布式组件（Distributed）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_service/README.Distributed.md)
- [《微服务工作笔记》-- 云原生（CloudNative）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_service/README.CloudNative.md)
- [《微服务工作笔记》-- 持续集成部署（DevOps）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_service/README.DevOps.md)
- [《微服务工作笔记》-- 系统安全（Security）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_service/README.Security.md)
- [《微服务工作笔记》-- 数据治理（Governance）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_service/README.Governance.md)
- [《微服务工作笔记》-- Java 微服务（SpringBoot）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_service/README.SpringBoot.md)
- [《微服务工作笔记》-- DotNet 微服务（DotNet）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_service/README.DotNet.md)
- [《微服务工作笔记》-- 网页端工程（Frontend）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_service/README.Frontend.md)

<br>

#### 技术栈选型目标

---

###### 战略目标：

- 为企业选择适配其发展阶段的研发路径，服务其发展战略；
- 在合理的范围内平衡性能与成本；
- 论证技术可行性，预判和规避研发路径上的技术瓶颈，提前为可能遇到的问题规划解决方案；

###### 能够从选型层面解决的常见问题：

- 数据库压力过载导致系统崩溃：
  ```
  原因之一：没有微服务化，数据抽象设计层面的隔离程度不够；
  原因之二：没有采用分表分库的架构；
  原因之三：没有采用缓存技术；
  ```
- 无法排查宕机或者响应延迟的异常：
  ```
  主要原因：监控体系不完善（日志、调用链跟踪、Metrics 监控、告警系统等）；
  ```
- 系统中业务和数据耦合度过大，难以维护和改动：
  ```
  原因之一：没有微服务化，本质是服务的接口化没有做到位；
  原因之二：跨系统的同步读写过多，没有合理使用异步队列和消息订阅模式；
  ```
- 前后端耦合度过大，导致无法兼顾 CS / BS（APP 接口、网站接口、小程序接口统一不起来）：
  ```
  主要原因：滥用 MVC，V 中后端逻辑太多；
  ```
- 缺乏 HA 体系，因此不具备高可用性：
  ```
  主要原因：没有利用微服务化 / 容器化 / K8S 部署等手段支撑 HA 的实现；
  ```
- 项目代码难以被维护和更新：
  ```
  原因之一：缺少架构拓扑图、缺少开发文档和运维文档、备注写得不够详细、代码规范没有统一；
  原因之二：重度依赖人工运维，没有合理设计 DevOps；
  原因之三：没有使用容器化技术连接研发和运维；
  ```
- APP 后端多版本并存的接口无法兼容：
  ```
  原因之一：缺少 BFF 设计；
  原因之二：过度依赖 Rest 类型的接口；
  原因之三：微服务程度不够，本质上是部署结构不够灵活导致无法同时维护多版本接口；
  ```
- 缺乏足够的测试：
  ```
  原因之一：小型团队因人员或条件有限，基于时间成本和重要紧急性的统筹考虑（或许无可厚非）；
  原因之二：没有足够的基础设计来支撑多环境（主要指容器化技术和本地部署）；
  原因之三：没有运用测试用例导向的研发流程；
  原因之四：缺乏性能测试的理念和手段；
  ```
- 测试环境和部署环境存在严重差异：
  ```
  原因之一：没有使用容器化技术；
  原因之二：生产环境的部署架构不适用于开发环境单机部署；
  ```
- 一个功能改动涉及多处代码（导致 bug 频出和维护困难）：
  ```
  主要原因：抽象程度不够（一段代码写了两次一定是结构有问题）；
  ```
- 任何小改动都要更新服务：
  ```
  原因之一：可配置模块设计不良；
  原因之二：没有合理利用微服务架构的配置中心；
  ```
- 不符合开闭原则（对扩展开放，对修改关闭）；
  ```
  案例 1：当系统想要扩展昵称、等级、签到等登录相关功能时，如果涉及对基础数据结构的修改，就是不良设计；
  案例 2：当系统需要作为开放平台对外提供服务时，如果对外提供的信息无法进行权限等级划分，也算不良设计；
  ```
- 无法单点登录；
  ```
  与其它系统存在数据层面耦合；
  缺乏元数据标准和数据治理体系；
  ```
- 容易遭受安全攻击：
  ```
  原因之一：缺少 Ddos 防护流程；
  原因之二：缺少相应的备用手段，如多类型接口并存和切换方案；
  原因之三：缺乏统一的输入校验和转义；
  原因之四：选用的数据库 ORM 不能很好的隔离输入字段；
  ```
- 容易被恶意员工删服暴库：
  ```
  原因之一：缺乏权限分级管理；
  原因之二：root 权限滥用；
  原因之三：备份流程欠缺；
  ```

<br>

#### 技术栈选型原则

---

###### 根据前述目标，技术栈选型首先要为企业技术战略服务，因此需要优先考虑的非技术要素有：

- 根据**项目投资成本**选择匹配的技术栈，避免因技术选型倒逼项目增加额外的投资预算；
- 根据**项目产品形态**选择匹配的技术栈，应充分考虑三五年规划中的产品形态；
- 根据**企业人力资源**选择匹配的技术栈，包括现有研发团队力量、当地人力成本、团队建设计划、人才市场概况、企业市场战略需求等；
- 根据**市场规模预期**选择匹配的技术栈，应综合考虑项目前期中小型架构阶段，也要为后期适合大规模集群做好准备，毕竟大多数项目都要经历从小到大的过程；

###### 基于确定的成本和发展规划，技术栈选型原则上应符合以下技术相关要素：

- 技术趋势
  > 技术选型应充分考虑技术趋势，避免做出与趋势明显不符的选择，信息化技术多年来一直处于快速变革之中，整体趋势是专业细分、自动化、标准化、开源化；
- 专业细分
  > 具体表现在当初很多企业需要自行研发的技术，到最后一定会一款或几款高度专业化的产品出现；
- 自动化
  > 具体表现在代码量的逐步减少，今年来低代码和无代码趋势的流行，本质上也是自动化的表现，即：越来越不需要重复而非标准的代码；
- 标准化
  > IT 行业比较热衷定制标准，无论是硬件还是软件方面都有明显的共识：制定标准的权限优于其它权限，因此标准化是 IT 行业发展的长期趋势；
- 开源化
  > 在数字世界里存在一些关于开源化的共识，在很多人眼中甚至可以称之为信仰，包括一些行业先驱和大拿。正是因为开源化共识的存在，纯技术栈成本（不含规模指数）事实上是逐年递减的。同时，开源产品提供了一个最基本的优势：只要个人技术能力达到要求，任何问题理论上都可以通过阅读源码解决，不存在黑箱故障。
- 市场普及度
  > 被国内外大厂采用的技术，往往能够经历实践，也会有更多文献资料可考，对于遇到问题的情况能，能检索到更多的解决方案；
- 稳定性
  > 任何一种技术，都需要在长年累月的实践中升级与优化，当修复的问题达到足够量时，通常都能覆盖大多数使用场景，因而趋于稳定；
- 性能指标公开
  > 对于技术选型来说，基本的判断依据就是性能指标，因此候选方案必须满足的条件之一，就是性能指标公开，且提供测试样本和过程；
- 跨平台特性/兼容性/扩展性
  > 技术架构通常会对基础平台存在依赖，如操作系统依赖、语言依赖、工具依赖、库依赖等，技术框架能兼容更多的依赖，就意味着更大的扩展性；
- 社区活跃度
  > 通常在评价一个技术框架是否有成为主流的迹象时，尤其对于开源技术来说，社区活跃度是一个重要指标；

<br>

#### 技术架构选型

---

基于以上背景和需求驱动，下面章节对基础选型进行举例；
请读者注意，选型具有一定的客观指标，也具有一定主观性，请按需参考；

<br>

#### 选型：Server 端采用单进程架构还是微服务架构？

---

###### 备选项：

- 单进程架构；
- 微服务架构；

###### 选型分析：

- 一个认知误区：微服务架构比单进程架构更优秀？
  ```
  事实上，抛开应用场景就无法讨论适用性；
  ```
- 不论是基于 MVC 的实践还是基于 DDD 的实践，微服务被事实证明，对基于信息流的应用后端，有较好的表现，能够合理进行逻辑分层，支持敏捷开发和运维，支持线性扩容和弹性部署；
- 但代价很明显：分层架构必然带来冗余的传输损耗，包括响应时间损耗和算力损耗，因此至少有两种场景下微服务表现不佳： 1. 对响应时间要求较高的场景，如游戏服务器，很少使用微服务架构； 2. 对事务一致性要求较高的场景，如财务系统，微服务每增加一个节点，可用性就下降一个数量级，处理分布式事务的回滚逻辑复杂度就会提高一个数量级；

###### 选型建议：

- 基于信息流的应用后端，若对响应时间和事务一致性要求不高，建议采用微服务架构；

<br>

#### 选型：微服务架构使用侵入式架构还是非侵入式架构？

---

###### 备选项：

- 侵入式架构，如 SpringCloud、Dubbo、DotNet 等全家桶组件
- 非侵入式架构，如 SideCar 架构，其主要实现有： - Istio； - Linkerd； - Dapr；

###### 选型分析：

- TODO；

###### 选型建议：

- TODO；

<br>

#### Server 端微服务架构解析

---

###### 语言及 Web 框架：架构师选型第一环也是影响重大的一环，会在团队建设、成本结构、交付周期等企业战略层面产生影响；

###### 敏捷开发：

- IDE：通常认为现代研发的发展方向是全栈工程师，因此掌握多种 IDE，或者掌握多用型的 IDE 是必备的技能；
- SCM：代码版本管理库；
- 系统环境：不同系统在开发时的构建目标都是一样的，只是实现方法有区别；
- 依赖包管理：依赖包管理主要包括中央库和构建工具，解决的是依赖包自动下载、版本管理、依赖重复、包冲突等问题；
- 构建工具：对于编译型的语言来说，构建工具就是配置编译参数的过程，通过配置将依赖包集成到编译参数中；
- 持续集成部署：是一系列工具和流程规范的集合，最终目标是敏捷开发和持续快速交付/迭代；

###### 服务接口：

- 入口代理：第一道工序，用于解析域名，根据域名或者路径映射到服务进程；
- 智能网关：实现 BFF（Backend for Frontend）；
  - 负载均衡：当服务线性扩容时，如何将流量按需分配到各个服务上；
  - 服务降级/熔断限流：当服务压力到达极限时，应如何合理地拒绝服务并避免产生雪崩效应；
  - 统一认证：在微服务架构上，认证可以放在每个微服务，但更好的选择是放在网关；
  - TODO:请求分片：；
  - TODO:动态路由：；
  - TODO:响应缓存：；
  - TODO:重试策略：；
  - TODO:链路追踪：；
  - TODO:协议转换：；
  - TODO:黑白名单：；
- 服务注册中心：让服务可以不理会 IP 和 Port，直接用服务名互相访问，因此可以支持动态部署，实现线性扩容和服务热备；
- 接口协议：从单体进程到微服务，第一要务就是解决服务间通信的问题；
- 接口文档：从手写文档，到自动导出文档，时代一直在变；

###### 分布式组件：

- 配置中心：全局消息总线，可实现不停服更新配置等订阅功能；
- 消息队列/异步通信：将任务事件排入队列中异步执行，或用于通信，或用于分布式事务的实现；
- 分布式事务：当一个业务链路需要跨越多个微服务节点时，出现调用中断怎么办？事务如何回滚？就是需要重点解决的问题；
- ID 生成器：一个号的 ID 有几个指标，数据库自增 ID 和 UUID 都无法完全满足，因此市面上出现了几个流行的 ID 生成器；

###### 持久层模型：

- 数据驱动模型：从行为层到；
- 数据库管理工具：开发测试运维常用工具；
- 数据库：持久层存储，主要是关系型和非关系型，其它类型还有图数据库、时序数据库等；
- 数据持久化模型：ORM（Object Relational Mapping）将存储结构和读写过程映射到框架对象中；
- 数据库连接池：读写数据库的连接管理器，为连接优化和监控等提供入口；
- 分表分库：海量高并发情况下，单表单库很容易达到压测极限，分表分库是解决数据库压力过大的手段之一；
- 缓存：缓存也是解决海量高并发情况下数据库压力过大的手段之一；
- 搜索引擎：搜索引擎通常是采用另一种持久层存储（Lucene 索引文档），目前主流的搜索引擎都基于 Lucene，除了 Apache Solr 之外，一般不使用关系数据库；
- 分布式文件存储：在云平台的助推下，现在已经叫做对象存储了；

###### 微服务治理：

- 日志：无论是否微服务，日志的规范是敏捷运维的基础要素之一；
- 审计日志：随着日志查询被重视和 ELK 的流行，对日志格式规范的要求提升，于是发展处审计日志；
- 集中日志监控：即日志的采集、传输、存储、查询/搜索；
- CI/CD：持续集成和发布，敏捷开发和敏捷运维的基础要素之一；
- 调用链跟踪：SkyWalking；
- Metrics 监控：Prometheus / AlertManager / Grafana；

###### 容器化：

- 容器：为了规避不同系统的环境带来的影响，解耦环境对进程的影响，可以考虑从开发阶段就进行容器化验证；
- 容器网络：在容器之间建立可通信的网络，告别手动配置服务网络节点；
- 容器部署：基于容器，可以实现一键部署，是敏捷开发和持续集成的重要基石；
- 容器治理：如何拉取、配置和部署公共组件，如果管理服务容器，如何监控容器状态，如何动态扩容；

<br>

#### C#/DotNet 技术栈选型案例

---

###### 语言及 Web 框架：C# / DotNet6；

###### 敏捷开发：

- IDE：VS / VSCode；
- SCM：Git / Gitee / Master-Develop 分支模型；
- 系统环境：macOS；
- 语言：C# / DotNet；
- 依赖包管理：NuGet；
- 构建工具：Build.cs；
- 持续集成部署：Jenkins + SonarQube + Linter + UnitTest + Phabricator；

###### 服务接口：

- 入口代理：Nginx / Kong
- 智能网关：Ocelot；
  - TODO:负载均衡：；
  - TODO:服务降级/熔断限流：；
  - TODO:统一认证：；
  - TODO:请求分片：；
  - TODO:动态路由：；
  - TODO:响应缓存：；
  - TODO:重试策略：；
  - TODO:链路追踪：；
  - TODO:协议转换：；
  - TODO:黑白名单：；
- 服务注册中心：Nacos；
- 接口协议：REST / gRPC(ProtoBuf)
- 接口文档：Swagger1；

###### 分布式组件：

- 配置中心：Nacos；
- 消息队列/异步通信：RabbitMQ / RocketMQ；
- 分布式事务：Seata；
- TODO:ID 生成器：；

###### 持久层模型：

- 数据驱动模型：DDD；
- 数据库管理工具：CloudBeaver；
- 数据库：EF Core MySQL / MongoDB / TiDB；
- 数据持久化模型：EF-Core；
- 数据库连接池：DotNet 自带；
- 分表分库：Sharding-Core；
- 分布式缓存：Redis；
- 搜索引擎：Elasticsearch
- 分布式文件存储：云 OSS

###### 微服务治理：

- 日志：DotNet 自带 / Log4Net；
- 审计日志：利用过滤器手动实现；
- 集中日志监控：Filebeat -> Kafka -> Logstash -> ElasticSearch -> Kibana；
- 调用链跟踪：SkyWalking；
- Metrics 监控：Prometheus / AlertManager / Grafana；

###### 容器化：

- 容器：Docker；
- 容器网络：Docker-Swarm / Kubernetes-Istio
- 容器部署：Docker-Compose / Kubernetes / Rancher
- 容器治理：cAdvisor / Heapster / InfluxDB / Prometheus / AlertManager / Grafana；

<br>

#### Java/SpringBoot 技术栈选型案例

---

###### 语言及 Web 框架：Java / OpenJDK11 / SpringBoot / SpringCloud；

###### 敏捷开发：

- IDE：VSCode；
- SCM：Git / Gitee / Master-Develop 分支模型；
- 系统环境：macOS；
- 依赖包管理：Maven；
- 构建工具：Maven / Gradle；
- 持续集成部署：Jenkins + SonarQube + Linter + UnitTest + Phabricator；

###### 服务接口：

- 入口代理：Nginx / Kong
- 服务注册中心：Nacos；
- 智能网关：SpringCloud Gateway；
  - 负载均衡：SpringCloud Ribbon；
  - 服务降级/熔断限流：Sentinel；
  - TODO:统一认证：；
  - TODO:请求分片：；
  - TODO:动态路由：；
  - TODO:响应缓存：；
  - TODO:重试策略：；
  - TODO:链路追踪：；
  - TODO:协议转换：；
  - TODO:黑白名单：；
- 服务注册中心：Nacos；
- 接口协议：REST / Feign / gRPC(ProtoBuf) / Dubbo
- 接口文档：SpringDoc；

###### 分布式组件：

- 配置中心：Nacos；
- 消息队列/异步通信：RabbitMQ / RocketMQ；
- 分布式事务：Seata；
- ID 生成器：Uid-Generator；

###### 持久层模型：

- 数据驱动模型：DDD；
- 数据库管理工具：CloudBeaver；
- 数据库：MySQL / MongoDB / TiDB；
- 数据持久化模型：JPA(Hibernate) / MyBatis(Plus)；
- 数据库连接池：Druid；
- 分表分库：Sharding-JDBC；
- 分布式缓存：Redis；
- 搜索引擎：Elasticsearch
- 分布式文件存储：云 OSS

###### 微服务治理：

- 日志：Slf4j / Logback；
- 审计日志：Logback + Logstash-encode；
- 集中日志监控：Filebeat -> Kafka -> Logstash -> ElasticSearch -> Kibana；
- 调用链跟踪：SkyWalking；
- Metrics 监控：Prometheus / AlertManager / Grafana；

###### 容器化：

- 容器：Docker；
- 容器网络：Docker-Swarm / Kubernetes-Istio
- 容器部署：Docker-Compose / Kubernetes / Rancher
- 容器治理：cAdvisor / Heapster / InfluxDB / Prometheus / AlertManager / Grafana；

<br>

#### 异构系统兼容

---

###### 基于以上三节，笔者的选型原则是尽可能选用跨平台的备选项，以确保 Java/Spring 体系和 C#/DotNet 体系的异构兼容；

###### 下节说明一下笔者为何会选择这两种语言体系；

<br>

#### 选型：服务端语言及 Web 框架

---

###### 备选项：

- **Java**：大厂标配，框架最成熟，支持现有各种微服务架构，开源 crm 多，静态强类型语言，适合高度抽象模型开发，一线城市人才充沛；
- **C#**：微软出品，发展非常迅猛，在面向对象的阐释方面比 Java/C++ 更优，运行效率略与 Java 相当，低于 C++/Go，近几年发展迅猛而健康，资深技术圈非常推荐；
- **C++**：大厂选配，性能可以做到极限，仅一线城市才有较多人才，静态弱类型语言，适合系统底层开发；
- **PHP**：中小公司标配 / 大厂选配，框架相对成熟，扁平简单，开源 crm 多，动态弱类型语言，适合快速网站开发，二线至五线城市人才行情尚可，但国内行情事实是高端人才普遍不在 PHP 领域深耕；
- **Python**：大厂小厂选配，框架很高效，扁平简单，开源框架支持有限，动态强类型语言，适合快速算法验证，不论在哪里都很难招人；
- **Golang**：游戏领域标配，云原生组件标配，Google 在内外部全面推广，达到类似 C 的效率，在国外非常热门，但近几年被 Rust 压制，只有游戏领域在坚守；
- **Kotlin**：号称是 Java 的继任者，笔者简单试用过，语法简洁可读性好，在面向对象思想的阐释方面，暂时无法全面取代 Java，生态建立也不快；
- **Rust**：以复杂性为代价解决安全性的类 C 语言，很多系统核心系统已经在采用，但生态仍在建设，暂时不适合使用在应用系统上；
- **Node.js**：网页端专家力推神器，从出现至今发展迅猛，其基础包管理工具 npm 已成为许多软件（不仅限于网页端软件）的依赖组件；

###### 选型分析：

- 各语言专用的主流 Web 框架：
  - Java：SpringMVC / SpringCloud / SpringBoot；
  - C#：基于 DotNet Framework / DotNet Core，所支持框架已从 ASP.NET 升级到 UWP，算是跨平台的完备体系，在国内普及度稍有欠缺；
  - C++：没有自成体系的 Web 框架，多依赖 RPC，需要自行组合使用各种库（如 fastcgi / libevent / STL / Boost / ICE / Thrift / gRPC / ProtocolBuffer）；
  - PHP：成熟的框架非常多（如 Laravel / Yii2），应用广泛如 ThinkPHP（许多免费开源 crm 的基础框架），在 Swoole、Workerman 等第三方框架的支撑下，也能做出前后分离的架构；
  - Python：Django（定位类似 ThinkPHP 大包大揽自成体系），Tornado（异步非阻塞做得很好，并发高），Flask（做小项目还是很快的）；
  - Golang：Gin / Echo / Beego；
  - Kotlin：Vert.x；
  - Rust：Rocket / Actix Web / Yew；
  - Node.js：Express；
- 各语言及平台运行效率对比：<https://www.techempower.com/benchmarks/>；
- 也可以参考国内顶级算法比赛为各种语言定义的时间倍率：<https://wiki.botzone.org.cn/index.php?title=Bot>：
  - C/C++：1 倍
  - Pascal：1 倍
  - JavaScript：2 倍
  - Java：3 倍
  - C#：6 倍
  - Python：6 倍
- 各语言排名榜单：
  - TIOBE 榜（综合多种权威来源反映热门程度）：官网：<https://www.tiobe.com/tiobe-index/>；
  - RedMonk 榜（Github 上项目数量和 StackOverflow 标签数）：官网：<https://redmonk.com/sogrady/>；
  - PYPL 榜（根据 Google 上对语言教程的搜索）：官网：<http://pypl.github.io/PYPL.html>；

###### 选型建议：

- 首先，就当前流行趋势而言，DDD 设计思想和微服务时间落地，更需要考量的是**技术生态的成熟度**和**基于强类型语义的面向对象特性**。
- Java：其配套生态齐全，解决方案成熟完整，比较适合做微服务；
- C#：自从微软拥抱开源之后，C#/DotNet 发展迅猛，DotNet 自带全家桶非常优秀，类型安全十分讲究，对性能要求高的部分甚至可以用 C++ 无缝补充，个人非常推荐；
- C++：驾驭 C++，对语言基础理论的掌握非常依赖，若想获得良好的品控，对团队要求较高，最终意味着高人力成本；
- PHP：根据资深技术圈的交流总结，PHP 在 DDD 领域驱动设计的落地方面存在诸多短板，根本原因是其动态弱类型语言特性，其语言的设计初衷并非基于 OO（面向对象），而是面向存储过程。因此国内一线互联网大厂通常只用作小规模试水产品或者快速原型，并不用于应对海量高并发特性的生产环境，在类似环境下使用 PHP（及其生态），将面临高并发环境下的扩展性风险；其生态组件在成熟度和稳定性上缺少大厂实践支撑；
- Python：人才缺口是个主要问题。行业内非常少见将 Python 用于后台框架的开发。同样的，在 DDD 领域驱动设计的落地方面和微服务的搭建上，其生态组件在成熟度和稳定性上缺少大厂实践支撑；
- Go：游戏领域的朋友常用于服务器开发，Go 语言的语法规范简单明了，但是除了游戏领域，Golang 用在哪个领域都面临强大的竞争对手，经过多年实践，仅在国外有部分公司用于面向信息流的后台架构，在国内可以说案例稀少；
- Kotlin 和 C++ 一样在面向对象逻辑体系方面并没有比 Java 好太多，Kotlin 的定位跟 C# 是大部分重合的，相比之下，如果在 Java 之外多学一门语言，更多人的选择是 C# 而不是 Kotlin；
- Node.js 在迄今的实践经验中多用于快速验证站点的搭建；

<br>

#### 选型：敏捷开发 - 集成开发工具(IDE)

---

###### 备选项：

- IntelJ Idea；
- JetBrains Fleet；
- VS；
- VSCode；
- Open Sumi；

###### 选型分析：

- 首先 IDE 的选型是基于语言的选型，因笔者推荐 Java/C#，因此主要是寻找一款适合这两种语言对应的 Web 框架适用的 IDE；
- Idea 是目前最好用的 Java IDE，JetBrains 来自捷克，政治风险相对可控，JetBrains 系列工具并不免费，若无正版支持，不建议公司使用；
- VS 是目前最好用的 C#/C++ IDE，社区免费版足够强大，收费的专业版是目前 C 语言系列 IDE 的天花板；
- VSCode 和 Fleet 是新型轻量级 IDE 的代表，其特性有： - 采用插件平台实现定制 IDE 模式，且免费提供； - 组件是根据自己需求，通过插件定制的，因此选择面更广； - 组件按需组合，因此理论上效率会比大包大揽型的 IDE 要高； - 通过市场竞争机制和持续本优化，孵化优秀组件；
- VSCode 和 Fleet 都能兼容其它语言、框架的开发，如网页端框架等，可实现统一工具入口；
- Open Sumi 是阿里研发的国产 IDE，与 VSCode 和 Fleet 相同路线，甚至可以兼容 VSCode 的所有插件，是信创工程首选；
- VSCode 是先行者，Fleet 是 VSCode 的竞争者，Open sumi 是国产竞品；

###### 选型建议：

- 若追求极致体验，可考虑 Idea / VS；
- 若追求统一工具入口和定制化，选择 VSCode / Fleet；

<br>

#### 选型：敏捷开发 - 代码版本管理(SCM)

---

###### 备选项：

- Svn；
- Git；

###### 选型分析：

- Svn 是上一代的 SCM，可以被 Git 完全替代； - 容易学习和掌握（招人容易/新手友好）； - 不容易出错（对于人多的团队来说，控制出错率很重要）； - 被 Linus 嫌弃，所以 Linus 开发了 Git；
- Git 对比 Svn 大概可以类比 Kubernetes 对比 SpringCloud ；
- Git 的概念和层次结构比较完整，因而比 Svn 略微复杂一点，Svn 只实现 Git 的功能子集；

###### 选型建议：

- Svn 已经全面淘汰，除了迁就老员工和旧项目之外，没有其它选用理由；
- 建议选择 Git；

<br>

#### 选型：敏捷开发 - SCM（Git）仓库

---

###### 备选项：

- Github；
- Gitee；
- Coding；
- GitLab；
- Gitea；

###### 选型分析：

- Github 是老牌的国际公有云仓库，发展成熟，资源众多，行业标杆，但存在较大国际政治风险；
- Gitee 是基于 Github 的国际政治风险发展而来的国产替代品，值得赞许的是 Gitee 发展很快很好，完全可以替代 Github，且生态建设也很快；
- Coding 的定位与 Gitee 一致，当前相对 Gitee 弱势，属于竞争者；
- GitLab 是老牌的私有仓库部署工具，因为是私有部署，因此政治风险不会如 Github 那么大；
- Gitea 是 GitLab 的国产替代品；

###### 选型建议：

- 若考虑主流、国际接轨，选择 Github；
- 若考虑国际环境风险（如“脱钩”政策、大防火墙等），选择 Gitee；
- 若大团队自建仓库，选择 GitLab；
- 若小团队自建仓库，选择 Gitea，作为信创试点；

<br>

#### 选型：敏捷开发 - SCM（Git）分支模型

---

###### 备选项：

- 生产/开发模型：master/develop；
- 特性/发布模型：master/develop/feature；
- 开发/发布分离模型：master/develop/feature/release；
- 开发/发布/缺陷分离模型：master/develop/feature/release/hotfix；

###### 选型分析：

- Git Flow： - 简介：<https://cloud.tencent.com/developer/article/1499524>；
  - 两个长期分支 master/developer，master 有两个衍生 hotfix（修 bug）/release（发布并维护的版本），developer 每个功能开发都衍生 feature 分支；
  - feature 开发完之后发布 pull request 请求给上级，合并进 developer 并删除该 feature；
  - release 作为 developer 分支的里程碑，积累要上线的功能，积累到一定程度就合并到 master（发布）；
  - master 中有问题就生成 hotfix 分支修复再提交新的 master 版本；
- Git Flow 的作者进行了反思，建议互联网项目不要使用此模型（更适合多版本并列维护的大型软件，而非互联网项目）
  - 华为也曾进行实践，总结了 merge 的不必要，而且认为多分支对于新人来说出错的概率太大，最终会影响业务进度
- Github Flow：
  - 长期分支只有 master，开发时从 master 衍生 developer 分支，合并时发送 pullrequest 请求主管合并进 master；
  - 容易出现集成时间推移；
  - 适合持续发布，适合小团队和个人；
- Gitlab Flow：
  - 长期分支只有 master，分两种情况：
  - 持续发布：在 master 分支开发之后发布到 pre-production 分支，测试后再发布到 production 分支；
  - 版本发布：每个版本都有一个 stable 分支，要修 bug 的话从 master 分支 cherry-pick 响应的 commit；
  - 容易出现集成时间推移；
  - 适合大多数团队；
- Netflix 模型：
  - 概念：<https://zhuanlan.zhihu.com/p/114452526>；
  - 适合大团队开发
- Facebook 模型：
  - 简介：
  - 用于开发的长期分支只有一个（master），用于发布的分支有多个；
  - 原则上禁止功能分支；
  - 必须 rebase，不能 merge；
  - 理念：
    - 把一个多人项目管理得好像个人项目；
    - 新代码尽快频繁入主干；
    - 主管不花时间做合并；
    - 尽量减少长期分支的存在，以支持 CI/CD；
    - 任何时候都可以集成；
    - 相当于回归到 Git 最基本的用法，但是在一个大团队中使用需要每个人统一贯彻理念，对团队成熟度要求较高；
  - 适合精英团队开发；
- Fork-Merge：
  - 开源项目常用的方式，每个开发者 Fork 主项目，在自己的代码仓中以自己喜欢的方式开发；
  - 开发者向主代码仓提交 Pull Request，由主代码仓审核后合并；
  - 权限管理很简单；
  - 不适用公司内部；

###### 选型建议：

- 作为后端服务而言，建议采用生产/开发模型（master/develop）；
- 对于小型客户端产品而言，开发/发布分离模型（master/develop/feature/release）；

<br>

#### 选型：敏捷开发 - 系统环境

---

###### 备选项：

- Windows；
- MacOS；
- Linux；

###### 选型分析：

- 在软件工具的成熟度上，Windows 无疑是最优的，其次是 MacOS，Linux 排末位；
- 若只考虑服务端开发，笔者会推荐使用 MacOS 和 Linux，因为二者自带 Shell 环境，比较接近主流的 CentOS 服务器环境；
- 近几年由于 Windows 大力发展 WSL2（Windows Subsystem for Linux 2），因此在 Windows 环境下做 Linux 开发已经有比较成熟的环境，已非 Server 开发短板；
- 题外话：考虑到 MacOS 与 Nvidia 决裂的关系，而行业内主要的 GPU 渲染能力都集中在 Nvidia 体系，因此 MacOS 不适合兼用做 3D 建模或者渲染工作站；

###### 选型建议：

- 现阶段更推荐 Windows；

<br>

#### 选型：敏捷开发 - 依赖包管理 - Java/SpringBoot

---

###### 备选项：

- Maven 公共库；
- Maven 自建库；

###### 选型分析：

- 目前可用的 Jar 包管理工具只有 Maven，暂无其它备选；
- Maven 官方的公共库只有一个，Maven Repository ：<https://mvnrepository.com/>，因某些原因访问可能不稳定，因此华为云和阿里云都建立了镜像；
- Maven 自建库只有一种，Nexus Sonatype：<https://www.sonatype.com>；

###### 选型建议：

- 在配置科学上网的前提下使用 Maven 公共库；
- 在团队非常专业的的前提下使用 SonaType 自建库；

<br>

#### 选型：敏捷开发 - 构建工具

---

###### 备选项：

- Maven；
- Gradle；

###### 选型分析：

- Maven：主流依赖包管理工具，稳定可靠简单，目前各知名开源技术栈会将 Maven 作为基础件提供，因此可认为 Maven 是必须掌握的基础；
- Gradle：更专业的构建工具，顺便承担了依赖包管理职责，适合构建复杂过程，比如 Android 应用，目前在 Github 仓库里，较多项目都是采用 Gradle 作为项目依赖包管理工具，因此可认为是趋势；

###### 选型建议：

- Maven 是需要掌握的基础，要求必须掌握，后端服务完全足够；
- Gradle 作为趋势，建议研发人员掌握，鼓励研发人员积极使用；

<br>

#### 选型：敏捷开发 - 持续集成部署

---

- 持续集成部署：Jenkins + SonarQube + Linter + UnitTest + Phabricator；

<br>

#### 选型：服务接口 - 入口代理

---

###### 备选项：

- CGI；
- Servlet（Tomcat/Jetty/Ashx）；
- Nginx；
- Apache；
- OpneResty；
- Kong/OpenResty；
- Envoy；
- Traefik；

###### 选型分析：

- CGI 是早期的监听技术，多用 C/Perl 实现 HTTP/TCP 监听处理，一个请求启动一次处理程序（进程），不仅重，而且对编程要求较高；
- J2EE 体系创造了 Servlet，做了两件重要的事情： - 运行一个守护进程，一个请求只启动一个线程处理； - 用 OO 思想封装了 Http/TCP 请求的各个模块，如 Request/Response/Session 等，令编程 API 变得简单清晰了；
- 之后的 ASP.Net 也是沿袭 Servlet 的做法实现的 Ashx；
- Nginx 和 Apache 都可以设置反向代理，也可运行服务器程序作为网关，监听端口、解析请求、可直接处理静态访问请求因此具备了网关的一部分功能，为统一网关奠定了基础； - Nginx 和 Apache 无法对请求进行动态处理，需要交给后续 Web 框架处理； - Nginx 的作为服务器的优点：反向代理结构更简单、轻量、高并发、低耗； - Apache 的作为服务器优点：便捷的 rewrite，对 PHP 的直接支持；
- Tomcat 和 Jetty 都是 J2EE 体系下的 Servlet 引擎容器，可处理动态接口请求； - Tomcat 更丰富，适合处理非海量的短连接请求； - Jetty 更轻量，对嵌入式更友好，适合处理海量请求，更擅长处理长连接；
- Traefik 与 Nginx 一样主要作为反向代理使用，在云原生时代，因后端服务会动态部署，所以 Nginx 需要通过 Ingress-Controller 才能实现服务发现； - Traefik 能感知云原生服务的动态，自带服务发现能力； - Nginx 适合处理静态代理，适合用在 K8S 集群之前作为转发入口，Traefik 因具备自动实现服务发现，适合在 K8S/K3S 集群中做负载均衡器；
- 一般 GateWay 会包含入口代理的功能，Gateway 一般是一个 Control Plane + Data Plane，比如 Kong 的 Data Plane 就是 OpenResty；
- 使用 Envoy 作为 Data Plane 的 Control Plane 有 Solo.io（Istio 族下） - Envoy：C++ 原生性能优秀，线程模型比 Nginx 还好，水平扩展，所有配置均支持动态接口。WASM 的插件机制逻辑上可编译到原生水平，还有很好的容器属性，只要 push/pull 就能增加插件进行使用； - Traefik 从 2.0 开始支持 L4，与 Swarm，K8S 结合都很好，性能也与 Nginx 不相上下，但配置动态还自带 UI。从 2.3 开始支持 Golang 的动态链接库和 Golang 代码解析执行两种（Dev Mode）； - Kong/OpenResty：基于 Nginx 和 Lua 插件，Kong 增加了控制平面的动态能力，差就差在 Kong 是几个东西组合的，大而全，但不小也不美，配置部署都麻烦。 - Krakend：欧洲公司的小众产品，golang 开发，插件支持 Lua 与 Golang，性能说是比 Kong 高，有技术后发优势，小而美，自带 UI，但社区生态还不够丰富。
- Envoy VS Traefik： - C++ VS Golang； - WASM 插件 VS Golang 插件； - 小而聚(无控制) VS 小而全(自带 UI）；

###### 选型建议：

- 小规模工程考虑简单稳定，建议使用 Nginx；
- 大规模工程考虑充分的自动化运维特性，建议考虑 Kong；
- 云原生容器环境下，可优先考虑 Traefik；

<br>

#### 选型：服务接口 - 服务注册中心

---

###### 备选项：

- SpringCloud Eureka；
- Nacos；
- Apollo；
- Consul；

###### 选型分析：

- 参考：<https://developer.aliyun.com/article/886724>；

###### 选型建议：

- 根据设定的原则：兼容 Java/Spring 和 C#/DotNet，建议使用 Nacos；

<br>

#### 选型：分布式组件 - 智能网关

---

###### 备选项：

- SpringCloud Gateway；
- Nginx-Ingress；
- Kong；
- Traefik；
- HAProxy；
- Voyager；
- Contour；
- Ambassador

###### 选型分析：

- 智能网关的实现逻辑：请求 -> 路由解析器查询路由映射表找到服务（映射表可以是静态或者动态的） -> 拦截器 -> 请求转发器根据服务映射表和 LoadBalance 转发到微服务 -> 微服务响应 -> 拦截器 -> 返回；
- BFF 概念：<https://blog.csdn.net/stackfuture/article/details/121869131>；
- 智能网关是 BFF 的一种实现，BFF 是一种设计理念，目的是解除网页端与后端的耦合，适合放在智能网关中实现的功能逻辑包括：统一认证，动态路由，响应缓存，重试策略，负载均衡，服务降级/熔断限流，链路追踪，协议转换，黑白名单；
- 云原生环境下智能网关的横向对比：<https://blog.didiyun.com/index.php/2018/09/29/kubernetes-ingress-controller-api-gateway/>；

###### 选型建议：

- TODO；
- TODO；

<br>

#### 选型：服务接口 - 智能网关 - 请求分片

---

###### 备选项：

- TODO；
- TODO；

###### 选型分析：

- TODO；
- TODO；

###### 选型建议：

- TODO；
- TODO；

<br>

#### 选型：服务接口 - 智能网关 - 负载均衡

---

###### 备选项：

- TODO；
- TODO；

###### 选型分析：

- TODO；
- TODO；

###### 选型建议：

- TODO；
- TODO；

<br>

#### 选型：服务接口 - 智能网关 - 服务降级/熔断限流

---

###### 备选项：

- TODO；
- TODO；

###### 选型分析：

- TODO；
- TODO；

###### 选型建议：

- TODO；
- TODO；

<br>

#### 选型：服务接口 - 智能网关 - 统一认证

---

###### 备选项：

- TODO；
- TODO；

###### 选型分析：

- TODO；
- TODO；

###### 选型建议：

- TODO；
- TODO；

<br>

#### 选型：服务接口 - 接口协议

---

###### 备选项：

- RPC，包括 gRPC、Thrift 等；
- RESTful，包括标准的 RESTful 方案、以 GET/POST 为主的 RESTful(like) 方案；
- 融合 RESTful 和 RPC 的方案，如 SpringCloud Feign；
- GraphQL；
- DataQL/Hasor；

###### 选型分析：

- RPC 引入 IDL 增加复杂度约束类型，使用 ProtoBuf 等进行高效编解码，代码更规范，效率高；
- RESTful 对测试友好，对语言和 Web 框架解耦，需要手动编解码（Json/Xml）；
- SpringCloud Feign 是一种用 RPC 方式调用 RESTful 接口的方案，没有完全遵照 HTTP 状态码，以降低复杂度。Feign 并不依赖 SpringCloud 其它组件，因此可以单独在 K8S 环境中使用，但无法跨语言和 Web 平台；
- GraphQL：
  - GraphQL 提供的好处：解决了接口数据冗余、接口太多、嵌套数据获取、接口版本兼容问题；
  - GraphQL 引入的问题有：缓存设计困难、舍弃了 HTTP 返回码的作用、schema 增加了编码复杂度等；
  - GraphQL 引入了更大的复杂性，目前阶段还没有公认的最佳实践；
- DataQL
  - Hasor：<https://www.hasor.net/web/index.html>；
  - Dataway：<https://www.hasor.net/web/dataway/about.html>；
  - DataQL：<https://www.hasor.net/web/dataql/whatisdataql.html>；
  - 快速入门：<https://mp.weixin.qq.com/s/FkhKNUm6DTD6bjAjCPbLZQ>；
  - Dataway 在 OSC 上的项目地址：<https://www.oschina.net/p/dataway>；

###### 选型建议：

- 若项目决定只采用 Java 技术栈，可考虑 RESTful(like) + SpringCloud Feign；
- 若项目考虑语言和平台扩展性，可考虑 RESTful + RPC；
- 仅当研发储备充足时才尝试 GraphQL 方案；

<br>

#### 选型：服务接口 - 接口文档

---

###### 备选项：

- Swagger；
- SpringDoc；
- Apifox；
- RAP；
- DOClever；
- Postman；

###### 选型分析：

- Swagger 丰富且独立的各个功能使得它能够被应用在各类需求下，不管是开发仍是测试均可以使用这个工具，来优化本身的开发过程，进行接口文档维护、接口测试等；
- Apifox = Postman + Swagger + mock + Jmeter，是一款集 API 设计，接口文档管理、代码生成、API 调试、API mock,API 自动化为一体的接口一站式协作平台；
- RAP 的应用范围很明确，是一个面向开发人员自测和联调的工具性平台，它更适合以开发为核心对接口进行维护，但目前基本不在维护；
- DOClever 是一款功能比较强大的平台，在国内好评率很高，并且产品彻底免费开源，可线下部署，同时产品更新迭代比较频繁，能够看出他们也是在用心作这个产品；
- Postman 是一个测试向的 API 小工具，能够很是轻量地维护一份“测试记录”，适合小的测试团队本身使用并维护；

###### 选型建议：

- Swagger 成熟稳定，但是在微服务网关的集成中问题比较多，因此不建议在微服务框架下使用 Swagger，且资源消耗较多；
- 当前阶段 SpringDoc 比较轻量级，且对 SpringCloud-Gateway 支持更好，因此比较推荐；
- 可以适当尝试 Apifox；

<br>

#### 选型：分布式组件 - 配置中心

---

###### 备选项：

- Zookeeper；
- SpringCloud Config；
- Nacos；
- Apollo；
- Consul；
- Pilot；

###### 选型分析：

- SpringCloud Config：支持注解，比较简单；
- Apollo：国外主流第三方服务注册中心和配置中心，配置实时推送比较快，UI 支持版本管理和回滚，总体来说略复杂；
- Nacos：与 Apollo 定位相似，阿里发布，简单可靠；
- Consul：与 Apollo 定位相似，对华有敌意，需要政治避坑；
- Pilot：云原生容器时代的服务注册中心和配置中心；

###### 选型建议：

- 根据设定的原则：兼容 Java/Spring 和 C#/DotNet，建议使用 Nacos；

<br>

#### 选型：分布式组件 - 消息队列/异步通信

###### 备选项：

- Kafka；
- RabbitMQ；
- RocketMQ；

###### 选型分析：

- 消息队列是为了解耦/异步/削峰；
- 参考：<https://zhuanlan.zhihu.com/p/60288391>；
- 参考：<https://zhuanlan.zhihu.com/p/84007327>；
- 参考：<https://zhuanlan.zhihu.com/p/115553176>；
- 参考：<https://www.cnblogs.com/aarond/p/queue-compare.html>；
- 参考：<https://baijiahao.baidu.com/s?id=1710441083020088751&wfr=spider&for=pc>；
- 参考：<https://wenku.baidu.com/view/c8574ef1b24e852458fb770bf78a6529647d35fb.html>；
- RabbitMQ 功能特性多，由于是 Erlang 实现，定位问题要求语言基础；
- 优先使用 RabbitMQ 的条件：
  - 高级灵活的路由规则；
  - 控制消息过期或消息延迟；
  - 高级的容错处理能力，在消费者更有可能处理消费不成功的情境中；
  - 更简单的消费者实现；
- 优先使用 Kafka 的条件：
  - 严格的消息顺序；
  - 延长消息留存时间，包括过去消息重放的可能；
  - 传统解决方案无法满足的高伸缩能力；
- 优先使用 RabbitMQ 的条件：
  - 对于可靠性要求很高的场景；
  - 尤其是电商里面的订单扣款，以及业务削峰，在大量交易涌入时，后端可能无法及时处理的情况；

###### 选型建议：

- 考虑到业务中对消息的细粒度特性，可在业务流程中使用 RabbitMQ；
- 考虑吞吐量和简化配置，可在工具依赖中（例如 SpringCloud Bus）使用 Kafka；
- 分布式事务可考虑选择 RocketMQ；

<br>

#### 选型：分布式组件 - 分布式事务

---

###### 备选项：

- Zookeeper；
- RocketMQ；
- Seata；
- Atomikos；

###### 选型分析：

- 理论基础：<https://zhuanlan.zhihu.com/p/41891052>；
- 对应于数据库事务的 ACID 模型，分布式事务有 CAP 模型：C 强一致性，A 可用性，P 分区容错性，理论上三者不可共有（不存在 CA 架构）；
- CP 的案例有 Zookeeper，AP 架构是当下主流，延展为 BASE 架构，允许基本可用，引入中间状态，追求最终一致性；
- 2PC 使用 XA 协议（Mysql5.5 后支持），实现成本较低，但因为靠事务管理器做两阶段操作，所以存在单点宕机/同步阻塞问题；
- TCC（Try-Confirm-Cancel）来自论文实现，先改主业务，然后记录活动日志，通过业务管理器 TCC 从业务，T 做检查和锁定，CC 做幂等执行或幂等取消，适用于要求强一致性和执行时间较短的业务；
- 本地消息表方案（eBay 采用）利用本地队列和扫描器确保最终一致性，配合缓存使用容易实现，RocketMQ 有实现；
- SAGA 模式将事务拆分然后一段段执行，出错后一段段回滚，华为的 Servicecomb 实现了 SAGA 事务的执行序列；
- 蚂蚁金服开源的 Seata 同时支持 AT/TCC/SAGA/XA 模式；
- Spring 的 Atomikos 框架实现了对 XA 和 TCC 的支持，Bitronix 和 Narayana 目前仅支持 XA；

###### 选型建议：

- 行业内 Seata 的应用比较成熟，简单可靠；

<br>

#### 选型：分布式组件 - ID 生成器

---

###### 备选项：

- 数据库自增 ID；
- UUID；
- 分布式 ID：
  - Twitter SnowFlake：；
  - 美团 Leaf：<https://tech.meituan.com/2017/04/21/mt-leaf.html>；
  - 百度 UIDGenertor：[参考]<https://github.com/baidu/uid-generator/blob/master/README.zh_cn.md>；
  - 腾讯 Seqsvr：<https://www.infoq.cn/article/wechat-serial-number-generator-architecture>；

###### 选型分析：

- 参考：<https://segmentfault.com/a/1190000023087471>；
- 美团团队总结的全局 ID 四大需求：
  1.  全局唯一性：不能出现重复的 ID 号，既然是唯一标识，这是最基本的要求；
  2.  趋势递增：在 MySQL InnoDB 引擎中使用的是聚集索引，由于多数 RDBMS 使用 B-tree 的数据结构来存储索引数据，在主键的选择上面我们应该尽量使用有序的主键保证写入性能；
  3.  单调递增：保证下一个 ID 一定大于上一个 ID，例如事务版本号、IM 增量消息、排序等特殊需求；
  4.  信息安全：如果 ID 是连续的，恶意用户的爬取工作就非常容易做了，直接按照顺序下载指定 URL 即可；如果是订单号就更危险了，竞争对手可以直接知道我们一天的单量。所以在一些应用场景下，会需要 ID 无规则、不规则：
- SnowFlake 存在的问题：不依赖数据库，也不依赖内存存储，随时可生成 ID，这也是它如此受欢迎的原因。但因为它在设计时通过时间戳来避免对内存和数据库的依赖，所以它依赖于服务器的时间。当服务器的时间发生了错乱或者回拨，这就直接影响到生成的 ID，有很大概率生成重复的 ID 且一定会打破递增属性。这是一个致命缺点；
- 百度 UIDGenerator：通过借用未来时间和双 Buffer 来解决时间回拨与生成性能等问题，同时结合 MySQL 进行 ID 分配；
- 美团 Leaf：根据业务场景提出了基于号段思想的 Leaf-Segment 方案和基于 Snowflake 的 Leaf-Snowflake 方案。Leaf-Snowflake 通过文件系统缓存降低了对 ZooKeeper 的依赖，同时通过对时间的比对和警报来应对 Snowflake 的时间回拨问题；
- 微信 Seqsvr：微信团队业务特殊，它有一个用 ID 来标记消息的顺序的场景，用来确保我们收到的消息就是有序的。在这里不是全局唯一 ID，而是单个用户全局唯一 ID，只需要保证这个用户发送的消息的 ID 是递增即可。它并没有依赖时间，而是通过自增数和号段来解决生成问题的；

###### 选型建议：

- 自增 ID 容易被爬虫扫库，且存在信息泄露（例如通过自增 ID 了解一天的订单量），不建议使用；
- UUID 不能做到递增，不建议作为主键使用，但是在不要求递增的场景下用作资源索引，还是很好的；
- 考虑到时间回拨问题（易造成重复 ID），不建议直接使用 SnowFlake；
- 百度 UIDGenerator 有 Java 的版本，可以在 Java/SpringBoot 项目中直接使用；

<br>

#### 选型：持久层模型 - 数据驱动模型

---

###### 备选项：

- 数据库驱动设计；
- MVC 分层设计；
- DDD 领域驱动设计；

###### 选型分析：

- 领域驱动设计（DDD - Domain-Driven Design）：
  - 白话解析：<https://zhuanlan.zhihu.com/p/91525839>；
  - DDD 是 Eric Evans 的著作《领域驱动设计》提出的概念，本质上是对 OOAD（面向对象分析和设计）更高层面（业务层）的提炼和延续；
  - DDD 是微服务设计的指导思想，因此优化微服务架构的过程就是对 DDD 的落地实践；
  - DDD 的初级目标之一是数据库解耦，这个思路跟微服务的设计理念是一致的，因此微服务通常遵循 DDD 的思路去实现；
  - DDD 的高级目标之一是读写分离，将指令事件（写）和查询（读）分离出来，就是 CQRS，这也是微服务的实践理念；
  - DDD 推崇根据实体生成表，但个人并不认同此做法，国内大部分企业也不遵循此建议：
- 从实体映射到表的主要论点：
  - 优势：对于不熟悉数据库的人来说很友好，提供了通常连专业 DBA 也做不到的高级 SQL 优化；
  - 反问的声音：这难道不是纵容程序员不好好学数据库吗？
- 从表映射到实体的主要论点：
  - 微服务的设计目标之一就是数据库解耦，都解耦了，理论上不应该存在复杂性很高的 SQL；
  - 数据表设计对字节有更直接的控制，比如 CHAR、VARCHAR、TEXT、TINYTEXT 等，比较直接；
  - 在国内，DBA 对于系统建设通常有深度的介入，贯通前期和后期。先审 schema，再做 coding 的工作流，暂时还是主流习惯；

###### 选型建议：

- 既然做微服务，当然要依据 DDD 设计理念进行实践；
- 但有一点值得探讨：是否要遵从实体生成表的做法？
  - 建议保守一点，先建表，再利用逆向工程生成实体；
  - MyBatis 自带的逆向工程工具要优于 JPA/Hibernate；

<br>

#### 选型：持久层模型 - 数据库管理工具

---

<br>

#### 选型：持久层模型 - 数据库

---

<br>

#### 选型：持久层模型 - 数据持久化模型 - Java/SpringBoot

---

###### 备选项：

- JPA/Hibernate
- MyBatis + mybatis-generator
- MyBatis Plus、Fluent MyBatis 等

###### 选型分析：

- 谣言和认知误区： 1. MyBatis 比 JPA/Hibernate 多了自定义 SQL 的能力； 2. JPA/Hibernate 比 MyBatis 多了规范性； 3. MyBatis 比 JPA/Hibernate 简单易上手； 4. 只有 JPA/Hibernate 才能支撑 DDD； 5. JPA/Hibernate 能提供更好的 SQL 优化；
- 事实是： 1. MyBatis 和 JPA/Hibernate 都能提供自定义 SQL 的能力； 2. JPA/Hibernate 和 MyBatis 的规范性与自身特性无关，只取决于怎么用； 3. MyBatis 和 JPA/Hibernate 都是入门容易，想要精通，需要的是同一套数据库优化的知识； 4. DDD 是一种相对严谨的思路，并不是什么银弹，任何一种 ORM 都能实现 DDD； 5. JPA/Hibernate 也有很好的优化，但却是隐性的，Repository 隐藏了很多细节，需要读源码去理解； 6. MyBatis 有一个逆向工程插件 mybatis-generator 也能提供很好的 SQL 优化，是显性的；
- MyBatis Plus、Fluent MyBatis 其实是在 MyBatis 的基础上添加了很多便捷的功能，比如说： - 分页查询； - 分布式 ID； - 数据库配置加密；
- 个人觉得 MyBatis Plus、Fluent MyBatis 添加的功能，并不完全适用；

###### 选型建议：

- 从数据读写方面看，MyBatis 和 JPA/Hibernate 的效果最终效果几乎等价；
- 从工程方面看： - 若采纳 DDD 的建议（从实体生成表），选择 JPA/Hibernate（更适合自动建表） - 若采用数据库驱动模型（从表生成实体），选择 MyBatis（更适合逆向工程）
- 从低代码工程方面考虑：的方向考虑， - 若采用自动生成 Bean 的低代码策略，选择 JPA/Hibernate（更适合自动建表） - 若采用自动生成 schema 的低代码策略，选择 MyBatis（更适合逆向工程）
- MyBatis Plus、Fluent MyBatis 所添加的功能绑定，事实上违反了最小依赖粒度的原则，不建议使用；

<br>

#### 选型：持久层模型 - 数据库连接池

###### 备选项：

- TODO;

###### 选型分析：

- TODO;

###### 选型建议：

- TODO;

<br>

#### 选型：持久层模型 - 分表分库

---

###### 备选项：

- TODO;

###### 选型分析：

- TODO;

###### 选型建议：

- TODO;

<br>

#### 选型：持久层模型 - 分布式缓存

---

###### 备选项：

- TODO;

###### 选型分析：

- TODO;

###### 选型建议：

- TODO;

###### 备选项：

- Memcached；
- Redis；
- Gemfire；

###### 选型分析：

- Memcached vs Redis - 在早期，Redis 比 Memcached 好在支持更多的存储类型，因此拉开了差距； - 在后期，得益于强大的社区推动，Redis 已经能够完全取代 Memcached；
- Gemfire 是集成程度和成熟度比 Redis 更高的商用分布式缓存，类似于 Oracle，商用不开源；

###### 选型建议：

- 目前阶段还是 Redis 更符合开源互联网需求；

<br>

#### 选型：持久层模型 - 搜索引擎

---

###### 备选项：

- TODO;

###### 选型分析：

- TODO;

###### 选型建议：

- TODO;

<br>

#### 选型：持久层模型 - 分布式文件存储

---

###### 备选项：

- TODO;

###### 选型分析：

- TODO;

###### 选型建议：

- TODO;

<br>

#### 选型：微服务治理 - 日志

---

###### 备选项：

- TODO;

###### 选型分析：

- TODO;

###### 选型建议：

- TODO;

<br>

#### 选型：微服务治理 - 审计日志

---

###### 备选项：

- TODO;

###### 选型分析：

- TODO;

###### 选型建议：

- TODO;

<br>

#### 选型：微服务治理 - 集中日志监控

---

###### 备选项：

- TODO;

###### 选型分析：

- TODO;

###### 选型建议：

- TODO;

<br>

#### 选型：微服务治理 - 调用链跟踪

---

###### 备选项：

- Zipkin；
- CAT；
- Pinpoint；
- SkyWalking；

###### 选型分析：

- 调用链跟踪选型的综合指标包括：调用链可视化、聚合报表、服务依赖图、埋点方式、VM 指标监控、告警支持、多语言支持、存储机制；
- 在 Java 语言中，字节码增强是通过 javaagent 使用 premain 方法，JDK6 以上支持；
- Zipkin：是 Twitter 开源的调用链分析工具，多语言支持非常丰富；
- CAT：是大众点评开源的调用链分析工具，采用字节码增强方式，聚合报表丰富，VM 指标监控不错；
- Pinpoint：韩国团队开源的 APM 产品，运用了字节码增强技术，无侵入。目前支持 Java 和 PHP 语言，底层采用 HBase 来存储数据，探针收集的数据粒度非常细，但性能损耗较大，完成度也很高，文档也较为丰富；
- SkyWalking：国产优秀工具，服务依赖图做得好，埋点方式非侵入而是字节码增强，Java Agent 非常直接，目前主流；
- 综合比较：
  - 参考：<https://www.jianshu.com/p/0fbbf99a236e>；
  - 参考：<https://blog.csdn.net/muriyue6/article/details/122149749>；

###### 选型建议：

- 主要考虑非侵入式埋点、多语言支持和国产化，目前更推荐 SkyWalking；

<br>

#### 选型：微服务治理 - Metrics 监控

---

###### 备选项：

- TODO;

###### 选型分析：

- TODO;

###### 选型建议：

- TODO;

<br>

#### 选型：容器化

---

###### 备选项：

- 微服务非容器化部署；
- 微服务容器化部署；

###### 选型分析：

- 企业技术选型常见的疑问之一：是否需要用到容器化？ - 容器化技术能够解决统一部署环境/测试环境的问题、端口和网络隔离问题、CI/CD 的问题，相对容易掌握，对于优化项目 DevOps 有明显作用； - 容器化能够实现前述选型原则中的**跨平台特性**/**兼容性**/**扩展性**，以使在一个系统内，能够兼容更多语言和 Web 框架研发的微服务；
- 企业技术选型常见的疑问之二：是否需要用到 Kubernetes？ - K8S 是目前主流公认的进化方向，K8S 最大的优势是概念逻辑完整层次分明，因而代价是复杂度，对团队技术层次要求较高。 - K8S 的完整度带来了复杂度，复杂度意味着人才稀缺和人力成本，考虑**企业人力资源**和**项目投资成本**因素，若无足够的人才储备，会容易陷入运维泥沼； - K8S 的发展其实是基于大规模集群部署的经验积累，若海量高并发系统的自动化运维需求和无相关经验，是很难理解 K8S 框架的设计理念；
- 在进化到 K8S 之前，SpringCloud 全家桶和 DotNet 全家桶都是常见的选项，在此选择 SpringCloud 来进行与 Kubernetes 之间的选型对比； - 服务发现和 LB：Eureka + Ribbon **vs** Service + kube-proxy - API 网关：GateWay **vs** Ingres - 配置管理：SpringCloud Config / Apollo **vs** ConfigMaps / Secrets - 限流熔断：Hystrix **vs** HealthCheck / Probe / ServiceMesh - 日志监控：ELK **vs** EFK - Metrics 监控：Actuator / MicroMeter + Prometheus **vs** Heapster + Prometheus - 调用链跟踪：SpringCloud Sleuth / Zipkin **vs** Jaeger / SkyWalking - 应用打包：Uber Jar / War **vs** Docker Imager / Helm - 服务框架： - 发布和调度：NA **vs** Scheduler - 自动伸缩和治愈：NA **vs** Scheduler / AutoScaler - 进程隔离：NA **vs** Docker Pod - 环境管理：NA **vs** Namespace / Authorization - 资源配额：NA **vs** CPU/Mem Limit / Namespace Quotas - 流量治理：NA **vs** ServiceMesh

###### 选型建议：

- 项目早期，可考虑 SpringBoot/DotNet + Docker/Swarm 的容器简化方案；
- 项目后期，可根据集群规模需求，逐步将 Docker/Swarm 替换成 K8S，以使用更多高级特性；

<br>

#### 选型：容器化 - 容器

---

###### 备选项：

- Docker；
- ContainerD；

###### 选型分析：

- TODO；

###### 选型建议：

- TODO；

<br>

#### 选型：容器化 - 容器网络

---

###### 备选项：

- Docker-Swarm；
- Kubernetes-Istio；

###### 选型分析：

- Service Mesh：<https://zhuanlan.zhihu.com/p/61901608>；

###### 选型建议：

- TODO；

<br>

#### 选型：容器化 - 容器部署

---

###### 备选项：

- Docker-Compose；
- Kubernetes；
- Rancher；

###### 选型分析：

- TODO；

###### 选型建议：

- TODO；

<br>

#### 选型：容器化 - 容器治理

---

###### 备选项：

- cAdvisor + InfluxDB；
- Prometheus / AlertManager / Grafana；
- cAdvisor / Heapster / InfluxDB / Prometheus / AlertManager / Grafana；

###### 选型分析：

- 容器治理：<https://www.likecs.com/show-203839482.html>；
- 参考：<https://www.cnblogs.com/st666/p/13029907.html>；
- 参考：<https://www.cnblogs.com/kebibuluan/p/11414366.html>；
- 在 SpringBoot 中，通过 Micrometer 和 Acuator 与 Prometheus 进行对接是比较好的方案；
- 在 Prometheus 之前还可以挡一个 PushGateway，使得 Prometheus 只通过 pull 模式只从 PushGateway 进行，而其它源通过 push 模式发往 PushGateway，这样做可以解决 pull 模式无法穿透目标防火墙等问题；

###### 选型建议：

- 笔者推荐 cAdvisor + InfluxDB
- 笔者推荐 Prometheus + AlertManager + Grafana；

<br>

#### 选型：分布式组件 - 异构系统集成(HSI)

---

###### 备选项：

- 基于 WebService 服务进行数据共享互通，网络传输采用 Soap 协议，数据包采用 XML 格式。
- 基于现代数据中台方案进行数据共享互通，网络传输采用 HTTP/HTTPS 协议，数据包采用 JSON 格式。
- 基于网络中间件和 SDK 进行数据共享互通，网络传输采用 TCP 层协议，数据包进行二进制加密和解密。
- 基于中间数据存储的数据共享互通，各系统通过 ETL 工具，将共享数据统一存储到指定数据仓库，对外开放只读权限。

###### 选型分析：

- 基于 WebService 服务进行数据共享互通，网络传输采用 Soap 协议，数据包采用 XML 格式。 - 优点：实时共享，方案成熟稳定，行业应用较多，很多老系统早已实现这种共享方式，无需重复建设。 - 缺点：需要另建安全体系，XML 格式数据并非加密传输且带宽使用率不高。 - 参考：<https://www.runoob.com/wsdl/wsdl-tutorial.html>；
- 基于现代数据中台方案进行数据共享互通，网络传输采用 HTTP/HTTPS 协议，数据包采用 JSON 格式。 - 优点：实时共享，HTTP/HTTPS 协议无需任何客户端或者依赖包支持，与微服务集群交换数据通常不需要中间转换格式（因为微服务通常也是 JSON 格式），另外 JSON 格式的带宽使用率较高。 - 缺点：HTTP/HTTPS 协议容易受到多种网络攻击，需另建安全体系。
- 基于网络中间件和 SDK 进行数据共享互通，网络传输采用 TCP 层协议，数据包进行二进制加密和解密。 - 优点：实时共享，数据非明文传输安全性极高，SDK 可自带安全防护措施。 - 缺点：为各种语言定制开发 SDK 工作量大，且需要管理 SDK 的版本兼容问题。
- 基于中间数据存储的数据共享互通，各系统通过 ETL 工具，将共享数据统一存储到指定数据仓库，对外开放只读权限。 - 优点：不会因为数据共享影响到各系统本身（因为攻击者只能影响中间数据库） - 缺点：非实时，安全性依赖于操作系统（而操作系统爆出漏洞的几率较高）

###### 选型建议：

- 若和已有系统集成，则需要考察对方系统的集成方案，许多成熟大系统很可能是采用 WebService 方案；
- 若是完全新建的系统，可优先考虑数据中台共享方案；

<br>

#### 选型：网页端框架

---

###### 备选项：

- AngularJS
- React
- Vue.js

###### 选型分析：

###### 选型建议：
