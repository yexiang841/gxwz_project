## 《微服务工作笔记》-- 网页端工程（Frontend）

<br>

#### 介绍

---

- 项目文档记录了笔者工作积累的云原生微服务技术笔记，以及元宇宙、数字孪生、程序化建模工作学习笔记；
- 项目包含一套元宇宙建模方法论和工程，采用 QGIS -> Blender / Houdini -> Unreal / Unity 的方式实现数字孪生虚拟城市；
- 项目包含一个 Unreal 客户端工程和一个 Unity 客户端工程（建设中）；
- 项目包含一个 Cesium 网页端工程，实现 WebGIS BI 展示；
- 项目包含一个 Vue.js / ElementPlus / TS 网页端工程，实现 Admin 管理后台；
- 项目包含一个 Java / SpringBoot 微服务工程和一个 C# / DotNet 微服务工程（建设中），共用网关和服务注册中心（Nacos），可以协同工作；
- 项目包含一个 Publish 部署工程，采用 Docker / Kubernetes / Jenkins 方式进行服务部署；
- 程序语言涉及 C++ / Java / C# / Python / TS / Shell / VEX；
- 错漏之处恳请读者不吝指正，转载望注明出处：<https://gitee.com/yexiang841/q3d_project/>；
- Copyright 叶湘 ( weixin:yexiang841, email:yexiang841@qq.com ), Shitouren Studio. All Rights Reserved.

<br>

#### 微服务工作学习笔记

---

- [《微服务工作笔记》-- 项目介绍（Prologue）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_service/README.Prologue.md)
- [《微服务工作笔记》-- 架构选型（Selection）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_service/README.Selection.md)
- [《微服务工作笔记》-- 数据接口（Interface）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_service/README.Interface.md)
- [《微服务工作笔记》-- 数据持久化（Persistence）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_service/README.Database.md)
- [《微服务工作笔记》-- 分布式组件（Distributed）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_service/README.Distributed.md)
- [《微服务工作笔记》-- 云原生（CloudNative）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_service/README.CloudNative.md)
- [《微服务工作笔记》-- 持续集成部署（DevOps）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_service/README.DevOps.md)
- [《微服务工作笔记》-- 系统安全（Security）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_service/README.Security.md)
- [《微服务工作笔记》-- 数据治理（Governance）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_service/README.Governance.md)
- [《微服务工作笔记》-- Java 微服务（SpringBoot）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_service/README.SpringBoot.md)
- [《微服务工作笔记》-- DotNet 微服务（DotNet）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_service/README.DotNet.md)
- `>>>>` [《微服务工作笔记》-- 网页端工程（Frontend）](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_service/README.Frontend.md)

<br>

#### 本章摘要

---

1.  更新状态：持续整理上传中；
2.  最后更新：2021.09.12；
3.  本章记录网页端的开发技术，主要专注 Vue.js 和 React.js 两种框架；

<br>

#### ECMAScript

---

1.  ECMAScript 是 JavaScript 发展中的形成的标准化；
2.  ECMAScript 当前的主要竞争对手是微软的 TypeScript；
3.  ECMAScript 的发展历程：
    - ES3：初始版本；
    - ES4：内测版，未发布；
    - ES5：里程碑，全浏览器达成了一致，将在新版本中予以支持（IE9 及以上）；
    - ES6：当前的主流版本，在 ES5 基础上进行了优化，但部分旧浏览器还需要时间淘汰；
    - ES7：React 系列所需要的版本；
    - ES8：过渡版本；
    - ES9：研发中的版本；
4.  Promise 语法：
    - 实现了异步和多线程；
    - 参考：<https://blog.csdn.net/qq_34645412/article/details/81170576>；
5.  async / await：
    - 相当于简易实现 Promise 的语法糖
    - 参考：<https://blog.csdn.net/qq_42941302/article/details/109245356>；

<br>

#### TypeScript

---

1.  TypeScript 是微软版本的 ECMAScript；
2.  TypeScript 有很多好特性，但是没有被纳入标准；
3.  因为语言特性优秀，因此很多开发者还是会使用 TypeScript，但会用工具翻译成符合 ECMAScript 标准的 JS；
4.  总体来说，TypeScriipt > ESMAScript > JavaScript；
5.  安装：
    ```
    npm install -g typescript # 然后就可以全局使用 tsc xxx.ts 进行编译
    npm i @types/node -D # 工程中的 TypeScript 支持
    ```
6.  类型：
    ```
    let x01:bool = true/false; // 布尔
    let x02:number = 6; // 数值
    let x03:string = 'hello' 或者 "hello"; // 字符串
    let x04:number[] = [1,2,3]; // 数组
    let x05:Array<number> = [1,2,3]; // 数组(泛型写法)
    let x06:[number,string] = [1,'2']; // 元组
    enum X07 {Red,Green,Blue}; // 定义枚举
    let x07:X07 = Red; // 枚举变量
    enum X08 {Red=1,Green,Blue}; // 定义带序号的枚举
    let x08:string = X08[2]; // 获取枚举名，x8 = 'Green'
    let x09:number|string = 5; // 联合类型，可以使几种类型之一
    x09 = 'hello'
    let x10:(number|string)[] = [1,'2',3]; // 联合类型数组
    let x11:any = 'hello world'; // 任意类型
    let x12:number = (x10 as string).length; // 显示转化
    let x13:void; // 空类型，用于表示函数无返回的返回值
    ```
7.  对象、接口：
    ```
    // 定义接口
    interface IPerson {
      name: string;   // 必选属性
      age ?: number;  // 可选属性
      [propName: string]: any; // 任意属性，[propName: string]表示属性名，any表示任意类型
    }
    // 定义对象
    let tom:IPerson = {
      name: 'tome'; // 注意这里省略了类型
      age: 18;      // 注意这一行可有可无
      id: 1;        // 注意，因为接口中任意属性的存在，这一句才合规
    }
    ```
8.  函数、接口、泛型：
    ```
    // 普通函数
    let f01 = function(p01:number,p02:number):boolean{
      return p01==p02;
    }
    // 泛型函数
    let f02 = function<T>(p01:T,p02:T):boolean{
      return p01==p02;
    }
    // 函数接口
    interface IFun03 {
      <T>(p01:T,p02:T):boolean
    }
    // 用函数接口定义函数
    let f03:IFun03 = function(p01,p02){
      return p01==p02;
    }
    // 泛型函数接口
    interface IFun04<T> {
      (p01:T,p02:T):boolean
    }
    // 用泛型函数接口定义函数
    let f04:IFun04<number> = function(p01,p02){
      return p01==p02;
    }
    ```

<br>

#### Node.js / npm

---

1.  官网：<https://nodejs.org/zh-cn/>；
2.  React.js 和其它很多网页端引擎依赖 npm 这个基础工具；
3.  Homebrew 方式安装：
    ```
    brew install node
    ```
    - 在 MacOS 上用这种方式安装，npm 全局安装的默认位置是/usr/local/lib/node_modules
4.  验证：
    ```
    node -v
    npm -v
    ```
5.  使用淘宝镜像 cnpm（不推荐）：
    ```
    npm install -g cnpm --registry=https://registry.npm.taobao.org
    ```
    - 之后就可以直接使用 cnpm 了；
    - 建议还是科学使用 npm；
6.  npm 常用操作：
    - 安装：
      ```
      npm install -g [package] # 全局安装，将框架安装在全局默认位置/usr/local/lib/node_modules
      npm install [package]@x.x.x # 全局安装指定版本
      npm install [package] # 当前目录安装，将框架安装在当前运行指令的./node_modules
      npm install --save [package] # 当前目录安装，并在 package.json 的 dependencies 节点写入依赖：
      npm install --save-dev [package] # 当前目录安装，并在 package.json 的 devDependencies 节点写入依赖：
      npm i # 基本等同于 npm install
      npm i -S # 基本等同于 npm install --save
      npm i -D # 基本等同于 npm install --save-dev
      ```
      - 问：什么情况下需要全局安装？
        - 答：只有某些工具需要全局安装，例如 vue-cli，vue2 项目需要用 vue-cli 来生成，但 vue3 就不用了；
      - `npm i` vs `npm install`
        1. 用 npm i 安装的模块无法用 npm uninstall 删除，用 npm uninstall i 才卸载掉；
        2. npm i 会帮助检测与当前 node 版本最匹配的 npm 包版本号，并匹配出来相互依赖的 npm 包应该提升的版本号；
        3. 部分 npm 包在当前 node 版本下无法使用，必须使用建议版本；
        4. 安装报错时 intall 肯定会出现 npm-debug.log 文件，npm i 不一定；
    - 列出已安装模块：
      ```
      npm list # 列出本地安装的模块
      npm list -g # 列出全局安装的模块
      npm list -g -depth 0 # 控制显示深度
      ```
    - 如果安装总是报错，可以清一次缓存：
      ```
      npm cache clean
      ```
    - 卸载：
      ```
      npm uninstall [package] -g # 卸载全局模块
      npm uninstall [package] # 卸载当前目录模块
      npm uninstall [package] --save # 卸载当前目录模块，并清理 package.json
      ```
    - 查看远程版本：
      ```
      npm view [package] version # 查看模块远程最新版本
      npm view [package] versions # 查看模块远程所有版本
      ```
    - npm 指令不需要科学上网也比较快；
7.  可能遇到的安装问题：
    - 个人遭遇的案例：`npm i vite-plugin-imagemin -D` 时会出现安装依赖失败问题，其中有两个依赖出现了如下错误：
      - 安装前置依赖 gifsicle 组件时出现 autoconf 指令错误，需要通过重新安装 autoconf 的方式解决；
        ```
        brew install libtool automake autoconf nasm
        ```
      - 安装前置依赖 jpegtran 组件时出现安装脚本检查错误，可以通过绕过脚本检查来解决；
        ```
        npm install jpegtran-bin --ignore-scripts
        ```

<br>

#### Webpack

---

1.  Webpack 是一个 js 打包器，通过 Webpack 打包的项目能够解决服务器兼容至 ES5 的问题；
2.  网页端模块化打包发展史：
    - Script 标签
      ```
      <script src="module.js"></script>
      ```
      - 弊端：变量冲突、按顺序加载、自行解决依赖关系、不适用大工程；
    - CommonsJS
      ```
      requir("module");
      requir("../module.js");
      export.xxxFunction = function(){};
      module.exports = xxxValue;
      ```
      - 实现：服务端是 Node.js，客户端是 Browserify；
      - 弊端：不支持异步加载；
    - AMD
      - 解决了异步加载；
      - 弊端：过于复杂；
    - CMD
      - 解决了复杂性；
      - 弊端：依赖 SPM 打包；
    - Babel（ES6 标准）
      ```
      import "module";
      export function xxxFunction(){};
      module "xxxModule";
      ```
      - 一举解决所有问题；
      - 弊端：旧浏览器不支持；
3.  安装 Webpack：
    ```
    npm install -g webpack
    npm install -g webpack-cli
    ```
4.  为了能将 css 打包，还需要安装：
    ```
    npm install -g css-loader style-loader
    ```
5.  打包：
    - 在 webpack.config.js 配置文件所在目录之下：
    ```
    webpack
    ```

<br>

#### Axios

---

1.  JQuery 是基于 JS 的语法糖，优化了很多用法，并主要用于 Ajax 请求；
    - JQuery 太重了，除了网络请求还担负了其它 dom 操作相关的能力；
    - 因此在使用了 MVVM 框架之后，JQuery 多余的部分变得不必要；
2.  Axios 是在 Vue 时代替代 JQuery 的选项；
3.  菜鸟教程：<https://www.runoob.com/vue2/vuejs-ajax-axios.html>
4.  使用方法 1 - 引入 cdn 地址:
    - 无版本号：`<script src="https://unpkg.com/axios/dist/axios.min.js"></script>`
    - 有版本号：`<script src="https://cdn.staticfile.org/axios/0.18.0/axios.min.js"></script>`
5.  使用方法 2 - 本地安装:
    ```
    npm install axios
    ```
6.  TODO:Axios 在 Vue 中的使用

<br>

#### CSS 样式

---

1.  基本选择器：
    - div {...} // 元素选择器
    - #id {...} // ID 选择器
    - .class {...} // 类选择器
    - - {...} // 通配符选择器
2.  复合选择器：
    - 并集选择器：
      - .class, span, #id {...} // 表示满足任意一个条件都被选择
    - 后代选择器可以组合以上几种基本选择器，举例：
      - .class span {...} // 后代选择器的随意组合
      - #id span .class {...} // 后代选择器的随意组合
    - 子代选择器跟后代选择器一样，但是要求“后代”必须是“子辈”（不能是孙辈）
      - .class > a {...} // 子代选择器的随意组合
3.  伪类选择器：
    - a:link // 普通链接样式
    - a:visited // 访问过的链接样式
    - a:hover // 鼠标悬浮时的链接样式
    - a:active // 鼠标点击（未松开）时的链接样式
    - .class span:nth-child(n): // 表示 class 类下的第 n 个 span
4.  盒子模型：
    - margin -> boder -> padding -> content
5.  浮动和清除浮动：
    - 建议跳过浮动模型，直接使用 flex 布局
6.  display:flex 布局
    - display 有 block、inline、inline-block、flex 几种；
    - 前面几种模型比较传统复杂，需要处理浮动等问题，建议重点使用 flex 布局；
    - flex 布局不能在 IE11 以下的浏览器正常使用；
    - 父元素属性：
      - flex-direction: // 子元素对齐方向
        - row // 横向右（默认）
        - row-reverse // 横向左
        - column // 纵向下
        - column-reverse // 纵向上
      - justify-content: // 子元素在主轴上的对齐方式
        - flex-start // 主轴向前对齐（默认）
        - flex-end // 主轴向后对齐
        - center //主轴居中
        - space-around // 平均分配剩余空间（两端也会分配空间）
        - space-between // 平均分配剩余空间（两端对齐）
      - flex-wrap: // 子元素在主轴上超出容器尺寸时是否换行
        - nowrap // 不换行（默认）
        - wrap // 换行，会忽略子元素的主轴尺寸，自动压缩子元素（硬塞）；
      - align-items: // 子元素在侧轴上的对齐方式（多行）
        - flex-start // 侧轴向前对齐（默认）
        - flex-end // 侧轴向后对齐
        - center // 侧轴居中
        - stretch // 侧轴拉伸（仅当子元素没有设置侧轴尺寸时）
      - align-content: // 子元素在侧轴上的对齐方式（用于多行，单行下无效）
        - flex-start // 侧轴向前对齐（默认）
        - flex-end // 侧轴向后对齐
        - center //侧轴居中
        - space-around // 平均分配剩余空间（两端也会分配空间）
        - space-between // 平均分配剩余空间（两端对齐）
        - stretch // 侧轴拉伸（仅当子元素没有设置侧轴尺寸时）
      - flex-flow: // 复合属性，相当于同时设置了 flex-direction 和 flex-wrap；
    - 子元素属性：
      - flex:1 // 占用剩余空间的份数；
      - align-self: // 子元素自己在侧轴上的对齐，允许单个项目拥有跟父元素定义的 align-items 不同的属性
        - auto // 默认值，与父元素的 align-items 一致，如果没有父元素，则等于 stretch
        - flex-start / flex-end / center / stretch // 跟 align-items 的定义一样
      - order:n // 动态改变元素在父元素中的排序，n 越小越靠前

<br>

#### CSS 预处理器

---

1.  CSS 预处理器可使 css 可编程化；
2.  TODO:SASS(SCSS) vs LESS vs STYLUS

<br>

#### Vue

---

1.  核心架构：
    - Dom Listeners 实现与后端的数据绑定；
    - Data Binding 实现与页面表现层的数据绑定；
2.  浏览器支持：
    - 不支持 IE8 以下浏览器（因为 IE8 不支持 ECMAScript5）；
3.  浏览器插件：
    - 科学访问 chrome 网上应用商店，找到Vue.js devtools：<https://chrome.google.com/webstore/detail/vuejs-devtools>，添加插件到浏览器
    - 在 chrome 的 Vue DevTools 扩展设置中打开“允许访问文件网址”;
    - Vue.js devtools 的 V 图标会在检测到页面包含 Vue.js 时自动变成亮色，然后可以直接在页面进行调试；
4.  Vue 生命周期：
    - 简述：
      1.  beforeCreate(创建前)
      2.  created(创建后)
      3.  beforeMount(载入前)
      4.  mounted(载入后)
      5.  beforeUpdate(更新前)
      6.  updated(更新后)
      7.  beforeDestroy(销毁前)
      8.  destroyed(销毁后)
    - 具体：
      1.  newVue(）实例化一个 vue 实例，然后 mnit 初始化 event 和 lit ecycle，其实这个过程中分别调用 73 个初始化函数（initLitecycleO, initEventsO, initRenderO)，分别初始化了生命周期，事件以及定义 createElement 函数，初始化生命周期时，定义了一些属性， 比如表示当前状态生命周期状态得＿isMounted,\_isDestroyed,\_isBeingDestroyed，表示 keep-alive 中组件状态的＿inactive, 而初始化 event 时，实际上就是定义 7$once、 $otf、 $emit、 $on 几个函数。而 createElement 函数是在初始化 render 时定义的（调 用 7initRender 函数）
      2.  执行 betoreCreate 生命周期函数
      3.  beforeCreate 执行完后，会开始进行数据初始化，这个过程，会定义 data 数据方法以及事件，并且完成数据劫持。bserve 以及给组 件实例配置 watcher 观察者实例。这样，后续当数据发生变化时，才能感知到数据的变化并完成页面的渲染
      4.  执行 created 生命周期函数，所以，当这个函数执行的时候，我们已经可以拿到 data 下的数据以及 methods 下的方法了，所以在这 里，我们可以开始调用方法进行数据请求了
      5.  created 执行完后，我们可以看到，这里有个判断，判断当前是否有 el 参数（这里为什么需要判断，是因为我们后面的操作是会依赖这 个 el 的，后面会详细说），如果有，我们再看是否有 template 参数。如果没有 el，那么我们会等待调用＄ mount(el）方法（后面会详细 说）。
      6.  确保有了 el 后，继续往下走，判断当有 template 参数时，我们会选择去将 template 模板转换成 render 函数（其实在这前面是还有一个 判断的，判断当前是否有 render 函数，如果有的话，则会直接去渲染当前的 render 函数，如果没有那么我们才开始去查找是否有 template 模板），如果没有 template，那么我们就会直接将获取到的 el（也就是我们常见的＃app, #app 里面可能还会有其他标签） 编译成 templae，然后在将这个 template 转换成 render 函数。
      7.  之后再调用 betorMount，也就是说实际从 crated 到 betoreMount 之间，最主要的工作就是将模板或者 el 转换为 render 函数。并且 我们可以看出一点就是你不管是用 el，还是用 template，或者是用我们最常用的 vue 文件（如果是 vue 文件，他其实是会先编译成为 template)，最终他都是会被转换为 render 函数的。
      8.  beforeMount 调用后，我们是不是要开始渲染 render 函数了，首先我们会先生产一个虚拟 dom（用于后续数据发生变化时，新老虚 拟 dom 对比计算），进行保存，然后再开始将 render 渲染成为真实的 dom0 渲染成真实 dom 后，会将渲染出来的真实 dom 替换掉原 来的 vm.$el（这一步我们可能不理解，请耐心往下看，后面我会举例说明）然后再将替换后的＄ el append 到我们的页面内。整个初 步流程就算是走完了
      9.  之后再调用 mounted，并将标识生命周期的一个属性＿isMounted 置为 true。所以 mounted 函数内，我们是可以操作 dom 的，因为 这个时候 dom 已经渲染完成了。
      10. 再之后，只有当我们状态数据发生变化时，我们在触发 betoreUpdate,要开始将我们变化后的数据渲染到页面上了（实际上这里是有 个判断的，判断当前的＿isMounted 是不是为 ture 并且 jsDestroyed 是不是为 false，也就是说，保证 dom 已经被挂载的情况下，且当 前组件并未被销毁，才会走 update 流程）
      11. beforeUpdate 调用之后，我们又会重新生成一个新的虚拟 dom(Vnode),然后会拿这个最新的 Vnode 和原来的 Vnode 去做一个 ditf 算，这里就涉及到一系列的计算，算出最小的更新范围，从而更新 render 函数中的最新数据，再将更新后的 render 函数渲染成真实 dom。也就完成了我们的数据更新
      12. 然后再执行 updated，所以 updated 里面也可以操作 dom，并拿到最新更新后的 dom。不过这里我要插一句话了，mouted 和 updated 的执行，并不会等待所有子组件都被挂载完成后再执行，所以如果你希望所有视图都更新完毕后再做些什么事情，那么你最 好在 mouted 或者 updated 中加一个＄ nextTick()，然后把要做的事情放在＄ netTick()中去做（至于为什么，以后讲到＄ nextTick 再 说吧）
      13. 再之后 betoreDestroy 没啥说的，实例销毁前，也就是说在这个函数内，你还是可以操作实例的
      14. 之后会做一系列的销毁动作，解除各种数据引用移除事件监听，删除组件一 atcher，删除子实例，删除自身 self 等。同时将实例属 」l 生＿isDestroyed 置为 true
      15. 销毁完成后，再执行 destroyed
5.  TODO:VueRouter：
6.  Vuex：
    - 作用：状态存储中心（避免传参）
    - 三大组件：
      - State：被监控的数据；
      - Mutations：数据的变化；
      - Actions：数据的变化方式（例如异步请求后台）

<br>

#### Vue2 (Deprecated)

---

1.  官网：<https://cn.vuejs.org>
2.  文档：<https://cn.vuejs.org/v2/guide/installation.html>
3.  菜鸟教学：<https://www.runoob.com/vue2/vue-tutorial.html>
4.  安装 Vue2：
    ```
    npm install -g vue-cli # 然后就可以使用 vue、vue-init、vue-list 指令；
    ```
5.  安装工具库：
    ```
    npm install axios # 网络工具库
    npm install lodash # 容器库
    ```
6.  安装样式模块：
    ```
    npm install sass-loader node-sass --save-dev # 如果使用 SASS 样式
    npm install less less-loader --save-dev # 如果使用 LESS 样式
    npm install stylus stylus-loader --save-dev # 如果使用 STYLUS 样式
    ```
7.  网页直接引入：
    - 开发环境（包含完整的警告和调试模式）：
      - 下载：https://cn.vuejs.org/js/vue.js
      - 或在线引用：`<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>`
      - 可指定版本：`<script src="https://cdn.jsdelivr.net/npm/vue@2.6.10/dist/vue.js"></script>`
    - 生产环境（删除了警告）：
      - 下载：https://cn.vuejs.org/js/vue.min.js
      - 或在线引用：`<script src="https://cdn.jsdelivr.net/npm/vue@2.6.10"></script>`
      - 生产环境建议带上版本号；
8.  工程化(webpack)
    - 前面都是手动完成单个页面测试，真正使用上需要工程化
    - 建立工程：
      ```
      vue init webpack [project_dir]
      ```
      - 首先会新建 project_dir 工程目录；
      - 指令会先下模板，再填项目元信息和预设，然后完成工程初识化；
      - 项目元信息中的 project_name 不能包含大写，也可以和 project_dir 不同；
      - 项目预设中的 eslint 设置会从 github 取预设，因此需要科学上网；
      - 项目预设中的 test unit、e2e test 建议选 no，其余可以保持默认；
      - 这个指令通常需要科学上网才比较快；
    - 工程结构：
      ```
      .
      ├── build                 # 项目构建(webpack)相关代码
      ├── config                # 配置目录，包括端口号等。
      ├── src                   # 主要源码都在这里
      ├── static                # 静态资源目录
      ├── node_modules          # npm 加载的项目依赖模块
      ├── index.html            # 起始页
      ├── package.json          # npm 打包配置文件，记录当前项目所依赖模块的版本信息，锁定模块的大版本号（并不能锁定后面的小版本）
      ├── package-lock.json     # 锁定安装时依赖包的具体版本号，以保证其他人在 `npm install` 时大家的依赖能保证一致
      └── README.md             # 项目说明文档
      ```
    - 开发/运行：
      ```
      npm install # 如果依赖有改动先执行 install
      npm run dev # 正常编译通过会启动一个服务进程监听本地 8080 端口，浏览器访问<http://localhost:8080/>
      ```
    - 打包/发布：
      ```
      npm install # 如果依赖有改动先执行 install
      npm run build
      ```
      - 完成之后，项目文件夹中会出现一个 dist 文件夹，里面是打包之后的内容，可以直接部署；
      - 打开 dist/index.html 文件看到 css/js 的引用路径是绝对路径，去掉斜杠改为相对路径即可正常打开 dist/index.html；
9.  Vue2 语法
    - 代码见测试工程；
    - Vue2 使用了 Options API，写法与 Vue3 不同；

<br>

#### ElementUI (Deprecated)

---

1.  官网：<https://element.eleme.cn/#/zh-CN>；
2.  文档：<https://element.eleme.cn/#/zh-CN/component/installation>；
3.  Github：<https://github.com/ElementUI>；
4.  Gitee：<https://gitee.com/mirrors/elementui.git>；
5.  安装 ElementUI 模块：
    ```
    npm i element-ui -S
    ```
6.  2020 年之后，建议使用 Element-Plus

<br>

#### Vue-Element-Admin (Deprecated)

---

1.  官网：<https://panjiachen.github.io/vue-element-admin-site/zh/guide/>；
2.  Github：<https://github.com/PanJiaChen/vue-element-admin>；
3.  Gitee：<https://gitee.com/mirrors/vue-element-admin>；
4.  POC：<https://panjiachen.github.io/vue-element-admin/#/login?redirect=%2Fdashboard>；
5.  2020 年之后，建议使用 Vue3 + Element-Plus

<br>

#### Vite1

---

1.  Vite 相对于 Webpack 的优势就是按需加载比较快；
2.  但 Vite1 的 Bug 相对较多，因此到了 Vite2 才真正大规模使用；

<br>

#### Vite2

---

1.  官网：<https://vitejs.cn>
2.  文档：<https://vitejs.cn/guide/>
3.  Vite2 是针对 Vue3 ，同时兼容 React 的一个构建工具，特点就是快，比基于 Webpack 的 vue-init 更快；
4.  Vite2 提供一个开发服务器，基于原生 ES 模块 提供了丰富的内建功能，如速度快到惊人的模块热更新（HMR）；
5.  Vite2 提供了一套构建指令，它使用 Rollup 打包你的代码，并且它是预配置的，可输出用于生产环境的高度优化过的静态资源；
6.  Vite2 要求 Node.js 12.0 以上；

<br>

#### Pinia

---

1.  官网：<https://pinia.vuejs.org/>；
2.  参考：<https://blog.csdn.net/weixin_46873254/article/details/123304854>；
3.  Pinia 是可替代 Vuex 的工具，可视为 Vuex 的精简版；
4.  对比 Vuex@4 的话，Pinia 对 TypeScript 更加友好，而且允许建立多个 Store，更适合工程化。
5.  Vue 官方现阶段推荐使用 Pinia，同时也表示 Vuex@5 会参考借鉴 Pinia；
6.  安装：
    ```
    npm install pinia
    ```
7.  引入：
    ```
    import App from './App.vue'
    import { createPinia } from 'pinia'
    const pinia = createPinia();
    const app = createApp( App );
    app.use( pinia );
    app.mount( '#app' );
    ```
8.  Pinia 对比 Vuex：
    - 参考：<https://developer.51cto.com/article/672649.html>；

<br>

#### Vue3

---

1.  官网：<https://v3.cn.vuejs.org/>
2.  文档：<https://v3.cn.vuejs.org/guide/introduction.html>
3.  菜鸟教学：<https://www.runoob.com/vue3/vue3-install.html>
4.  参考：<https://blog.csdn.net/qq_33323469/article/details/123245587>
5.  安装 Vue3.2：
    ```
    npm install -g @vue/cli @vue/cli-init # 然后就可以使用 vue、vue-init、vue-list 指令；
    ```
6.  网页直接引入：
    - 开发环境（包含完整的警告和调试模式）：
      - 下载：https://cn.vuejs.org/js/vue.js
      - 或在线引用：`<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>`
      - 可指定版本：`<script src="https://cdn.jsdelivr.net/npm/vue@2.6.10/dist/vue.js"></script>`
    - 生产环境（删除了警告）：
      - 下载：https://cn.vuejs.org/js/vue.min.js
      - 或在线引用：`<script src="https://cdn.jsdelivr.net/npm/vue@2.6.10"></script>`
      - 生产环境建议带上版本号；
7.  工程化(Vite)
    - 前面都是手动完成单个页面测试，真正使用上需要工程化
    - 建立工程：
      ```
      npm init @vitejs/app [project_dir] # 会自动安装 @vitejs/create-app 工具用来创建工程
      ```
      - Vue3.2 已经不推荐使用 `create-vite-app` 工具或者 `vue init / create` 了，推荐使用 Vite；
      - 首先会新建 project_dir 工程目录；
      - 指令会先下模板，再填项目元信息和预设，然后完成工程初识化；
      - 项目元信息中的 project_name 不能包含大写，也可以和 project_dir 不同；
      - 需要选择 vue 工程（因为 Vite 也支持 React 等其它框架）；
    - 安装组件：
      ```
      # 生产环境组件
      npm i @element-plus/icons-vue -S #
      npm i @vueuse/core -S #
      npm i axios -S # 网络组件
      npm i echarts -S # 表单组件
      npm i echarts-liquidfill -S #
      npm i element-plus -S # ElementPlus 组件
      npm i js-md5 -S #
      npm i nprogress -S #
      npm i pinia -S # Pinia 替代 Vuex
      npm i pinia-plugin-persist -S #
      npm i qs -S # 字符串解析和序列化组件
      npm i vue-i18n -S # 国际化组件
      npm i vue-router@4 -S # 安装路由( Vue3 匹配 Vue-Router4 )
      npm i vue3-seamless-scroll -S #
      # 开发环境组件
      npm i rollup-plugin-visualizer -D #
      npm i sass -D # SASS 预处理器
      npm i unplugin-auto-import -D # 自动 import 组件
      npm i unplugin-vue-components -D # Component 按需导入组件
      npm i vite-plugin-cdn-import -D #
      npm i vite-plugin-compression -D #
      npm i vite-plugin-html -D #
      npm i vite-plugin-imagemin -D #
        # 这个组件安装依赖较多且易出错，可能要按如下方式解决 autoconf 和 jpegtran-bin 的问题：
        # brew install libtool automake autoconf nasm
        # npm install jpegtran-bin --ignore-scripts
      npm i vite-plugin-vue-setup-extend -D #
      npm i @types/node -D # 支持引入 path 中的 resolve
      # 编译
      npm install
      ```
    - 工程结构：
      ```
      .
      ├── build                 # 项目构建(webpack)相关代码
      ├── config                # 配置目录，包括端口号等。
      ├── src                   # 主要源码都在这里
      ├── static                # 静态资源目录
      ├── node_modules          # npm 加载的项目依赖模块
      ├── index.html            # 起始页
      ├── package.json          # npm 打包配置文件，记录当前项目所依赖模块的版本信息，锁定模块的大版本号（并不能锁定后面的小版本）
      ├── package-lock.json     # 锁定安装时依赖包的具体版本号，以保证其他人在 `npm install` 时大家的依赖能保证一致
      └── README.md             # 项目说明文档
      ```
    - 开发/运行：
      ```
      npm run dev # 正常编译通过会启动一个服务进程监听本地 3000 端口，浏览器访问<http://localhost:3000/>
      ```
    - 打包/发布：
      ```
      npm run build
      ```
      - 完成之后，项目文件夹中会出现一个 dist 文件夹，里面是打包之后的内容，可以直接部署；
      - 打开 dist/index.html 文件看到 css/js 的引用路径是绝对路径，去掉斜杠改为相对路径即可正常打开 dist/index.html；
8.  Vue3 语法
    - 代码见测试工程；
    - Vue3 使用了 Composition API，写法与 Vue2 不同；
    - Vue3.2 之后新增了 setup 语法糖，真正让 Vue3 的写法简洁明了；

<br>

#### Vue-i18n

---

1.  官网：<https://kazupon.github.io/vue-i18n/zh/started.html>；

<br>

#### ElementPlus

---

1.  官网：<https://element-plus.org/zh-CN/>；
2.  文档：<https://element-plus.org/zh-CN/guide/design.html>；
3.  组件：<https://element-plus.org/zh-CN/component/button.html>；
4.  图标：<https://element-plus.org/zh-CN/component/icon.html>；
5.  安装 ElementPlus 模块：
    ```
    npm install element-plus --save
    ```
6.  ElementPlus 完整导入：
    ```
    import ElementPlus from 'element-plus'
    import 'element-plus/dist/index.css'
    const app = createApp(App)
    app.use(ElementPlus)
    app.mount('#app')
    ```
    - 全部导入在启动时会引入全部组件，没必要且导致缓慢，因此推荐按需导入；
7.  ElementPlus 按需导入：
    - 安装按需自动导入模块：
    ```
    npm install unplugin-vue-components
    ```
    - 修改 vite.config.js/ts(如果使用 vite)
    ```
    import Components from 'unplugin-vue-components/vite'
    import { ElementPlusResolver } from 'unplugin-vue-components/resolvers'
    export default {
      plugins: [
        Components({
          resolvers: [ElementPlusResolver()],
        }),
      ],
    }
    ```
    - 或者修改 webpack.config.js(如果使用 webpack)
    ```
    const Components = require('unplugin-vue-components/webpack')
    const { ElementPlusResolver } = require('unplugin-vue-components/resolvers')
    module.export = {
      plugins: [
        Components({
          resolvers: [ElementPlusResolver()],
        }),
      ],
    }
    ```
8.  引入资源库：
    - 从 unpkg 站引入：
    ```
    <head>
      <!-- 导入样式 -->
      <link rel="stylesheet" href="//unpkg.com/element-plus/dist/index.css" />
      <!-- 导入 Vue 3 -->
      <script src="//unpkg.com/vue@next"></script>
      <!-- 导入组件库 -->
      <script src="//unpkg.com/element-plus"></script>
    </head>
    ```
    - 从 jsDelivr 站引入：
    ```
    <head>
      <!-- 导入样式 -->
      <link
        rel="stylesheet"
        href="//cdn.jsdelivr.net/npm/element-plus/dist/index.css"
      />
      <!-- 导入 Vue 3 -->
      <script src="//cdn.jsdelivr.net/npm/vue@next"></script>
      <!-- 导入组件库 -->
      <script src="//cdn.jsdelivr.net/npm/element-plus"></script>
    </head>
    ```

<br>

#### 微信小程序

---

1.  API：<https://mp.weixin.qq.com/debug/wxadoc/dev/api/>；

<br>

#### Uni-App

---

1.  官网：<https://uniapp.dcloud.io>；
2.  文档：<https://uniapp.dcloud.io/resource.html>；
3.  版本选择：；

<br>

#### Vue2 迁移到 Vue3

---

1.  参考：<https://juejin.cn/post/7086454715109769253>；
