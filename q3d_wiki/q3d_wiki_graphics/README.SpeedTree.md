## 《元宇宙工作学习笔记》-- SpeedTree

<br>

#### 介绍

---

- 项目文档记录了笔者工作积累的云原生微服务技术笔记，以及元宇宙、数字孪生、程序化建模工作学习笔记；
- 项目包含一套元宇宙建模方法论和工程，采用 QGIS -> Blender / Houdini -> Unreal / Unity 的方式实现数字孪生虚拟城市；
- 项目包含一个 Unreal 客户端工程和一个 Unity 客户端工程（建设中）；
- 项目包含一个 Cesium 网页端工程，实现 WebGIS BI 展示；
- 项目包含一个 Vue.js / ElementPlus / TS 网页端工程，实现 Admin 管理后台；
- 项目包含一个 Java / SpringBoot 微服务工程和一个 C# / DotNet 微服务工程（建设中），共用网关和服务注册中心（Nacos），可以协同工作；
- 项目包含一个 Publish 部署工程，采用 Docker / Kubernetes / Jenkins 方式进行服务部署；
- 程序语言涉及 C++ / Java / C# / Python / TS / Shell / VEX；
- 错漏之处恳请读者不吝指正，转载望注明出处：<https://gitee.com/yexiang841/q3d_project/>；
- Copyright 叶湘 ( weixin:yexiang841, email:yexiang841@qq.com ), Shitouren Studio. All Rights Reserved.

<br>

#### 元宇宙、数字孪生、程序化建模工作学习笔记

---

- [《元宇宙工作学习笔记》-- 数字孪生虚拟城市建模方法](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_graphics/README.Meta.md)
- [《元宇宙工作学习笔记》-- 图形学基础](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_graphics/README.Graphics.md)
- [《元宇宙工作学习笔记》-- QGIS](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_graphics/README.QGIS.md)
- [《元宇宙工作学习笔记》-- Blender](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_graphics/README.Blender.md)
- [《元宇宙工作学习笔记》-- C4D](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_graphics/README.C4D.md)
- [《元宇宙工作学习笔记》-- Houdini](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_graphics/README.Houdini.md)
- `>>>>` [《元宇宙工作学习笔记》-- SpeedTree](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_graphics/README.SpeedTree.md)
- [《元宇宙工作学习笔记》-- Illustrator](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_graphics/README.Illustrator.md)

## 学习笔记

---

1.  为什么选择 SpeedTree：
    1. GameReady；
    2. 建模架构良好；
    3. 支持所有类型树木；
2.  软件简介：
    1. 官网：；
    2. SpeedTree 以节点操作为主；
3.  教程
    1.  官方入门操作：<https://www.bilibili.com/video/BV1Ws411F7Yk>；
    2.  深入案例教程：<https://www.bilibili.com/video/BV1st411h7jH>；
    3.  中文详解教程：<https://www.bilibili.com/video/BV1R741157mD>；
4.  案例
    1.  ；
5.  资源
    1.  ；
6.  视图：
    1.  X：逐级隐藏；
    2.  C：逐级取消隐藏；
    3.  F：聚焦某一个树干或者叶子；
    4.  cmd+F：取消聚焦；
7.  树 Tree：
    1.  ；
8.  枝干：
    1.  节点包括：
        1. Trunk：
        2. Big Branch：
        3. Branch：
        4. Little Branch：
        5. Twigs：
    2.  Mode：模式
        1. Claasic：兼容 8 以前版本的；
        2. Absolute：不管 Trunk 如何改变，分支都是一个绝对值，【适合 Trunk】；
        3. Absolute Steps：固定间隔；
        4. Interval：类似固定间隔，360 度平均分布。【适合 BigBranch 或者 Twigs】；
        5. Phyllotaxy：对称排列，不是分布在 360 度，而是分布在 N 个面上，每个面都有差不多的分布；
        6. Bifurcation：只要父节点有分支，这里就会分叉（根据敏感度），【适合 Branch 或者 Little Branch】；
    3.  Gen:
        1.
    4.  Sphine:
        1. Length：主干长度；
        2. Orientation：枝干的朝向（上下）；
        3. Shape：
           1. Gravity：受重力作用产生的弯曲；
           2. ：；
        4. Noise：
           1. Late：给枝干加弯曲噪声；
           2. Noise：主干上的 S 型；
    5.  Segment：调整分段数；
    6.  Skin：
        1. Radius：枝干半径；
        2. Welding：焊接部位的处理；
        3. Squash：把圆形枝干压扁；
    7.  Displacement：
        1. Shape：枝干表面的茎状凹凸；
        2. Flares：根部发散；
        3.
9.  树叶：
    1.  Gen：
        1. Philotaxy：叶序（对称、轮生）；
        2. Node：叶子节点排布
           1. Internode：分布间隔；
        3. Shared：
           1. Boundaries：叶子生长的边界（距离枝头和根的距离）；
           2. Knockout：叶子总数控制器；
        4.
    2.  Orientation：
        1. Local Orientation：
           1. Sky influence：朝向天空的角度；
           2. Fold：从树叶根部往上翘的角度；
           3. Align：从树叶根部往枝头弯的角度；
           4. Face：感觉跟 Skey influence 差不多，但不以天空为标准；
        2. Deformation：
           1. Fold：树叶左右两边向中间弯曲；
           2. Curl：树叶从根部到顶部向地面弯曲；
           3. Twist：树叶沿着主茎左右扭动；
           4. Scale：长短宽窄拉伸；
    3.  Skin：
        1. Size：叶片的大小；
        2. Leaves：
           1. Weld：焊接（调整叶子和树干的连接处）；
10. 茎叶：
    1. Geometry：
       1. Shape：
          1. Fold：茎叶以茎为中心对称折叠；
          2. Curl：茎叶自身的卷曲；
          3. Boundary：茎叶长在茎上的位置；
          4. Scale：制作类似茎叶两头窄中间宽的形状，可以通过 Curve 直接调整；
11. 棕榈树的制作：
    1. 制作树干：通过 Skin.Radius 的曲线刻画出树干的曲率；
    2. 制作树干上的肿囊：添加 BigBranches，加盖缩短，优化剖面，变密集作为树干突起；
    3.
12. 树叶贴图制作（Photoshop）：
    1. 获取干净平整素材；
    2. 制作蒙版图：框选叶子外部涂黑，叶子内部涂白（或者用蒙版操作）；
    3. 制作彩色图：原图层背景变成近似色，修剪边缘，然后可以合并为一个图层；
    4. 制作法线图：复制出一个图层，用 Photoshop 滤镜中的 3D 功能烘焙法线；
    5. 制作置换图：复制出一个图层，变成灰度图，然后调整对比度曲线将黑白分离（黑色是叶茎）；
    6.
