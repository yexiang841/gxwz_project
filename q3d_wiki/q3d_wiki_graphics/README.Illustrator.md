## 《元宇宙工作学习笔记》-- Illustrator

<br>

#### 介绍

---

- 项目文档记录了笔者工作积累的云原生微服务技术笔记，以及元宇宙、数字孪生、程序化建模工作学习笔记；
- 项目包含一套元宇宙建模方法论和工程，采用 QGIS -> Blender / Houdini -> Unreal / Unity 的方式实现数字孪生虚拟城市；
- 项目包含一个 Unreal 客户端工程和一个 Unity 客户端工程（建设中）；
- 项目包含一个 Cesium 网页端工程，实现 WebGIS BI 展示；
- 项目包含一个 Vue.js / ElementPlus / TS 网页端工程，实现 Admin 管理后台；
- 项目包含一个 Java / SpringBoot 微服务工程和一个 C# / DotNet 微服务工程（建设中），共用网关和服务注册中心（Nacos），可以协同工作；
- 项目包含一个 Publish 部署工程，采用 Docker / Kubernetes / Jenkins 方式进行服务部署；
- 程序语言涉及 C++ / Java / C# / Python / TS / Shell / VEX；
- 错漏之处恳请读者不吝指正，转载望注明出处：<https://gitee.com/yexiang841/q3d_project/>；
- Copyright 叶湘 ( weixin:yexiang841, email:yexiang841@qq.com ), Shitouren Studio. All Rights Reserved.

<br>

#### 元宇宙、数字孪生、程序化建模工作学习笔记

---

- [《元宇宙工作学习笔记》-- 数字孪生虚拟城市建模方法](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_graphics/README.Meta.md)
- [《元宇宙工作学习笔记》-- 图形学基础](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_graphics/README.Graphics.md)
- [《元宇宙工作学习笔记》-- QGIS](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_graphics/README.QGIS.md)
- [《元宇宙工作学习笔记》-- Blender](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_graphics/README.Blender.md)
- [《元宇宙工作学习笔记》-- C4D](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_graphics/README.C4D.md)
- [《元宇宙工作学习笔记》-- Houdini](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_graphics/README.Houdini.md)
- [《元宇宙工作学习笔记》-- SpeedTree](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_graphics/README.SpeedTree.md)
- `>>>>` [《元宇宙工作学习笔记》-- Illustrator](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_graphics/README.Illustrator.md)

<br>

#### 学习笔记

---

1.  2019 要切换为传统基本功能面板；
2.  工具栏
    1.  v：选择工具，选择；
    2.  a：直接选择工具，选择锚点；
    3.  y：魔棒工具，双击设置，可以按描边选也可以按填充色选；
    4.  v：套索工具，选择锚点；
    5.  p：钢笔工具，单点是画直线，点击拖动之后是画曲线，添加删除锚点，锚点转换；
    6.  曲线钢笔工具，根据上一个锚点自动计算曲率；
    7.  t：文字工具，单击出一行，手动换行，双击出个框，会自动换行
        1. 在非闭合路径上单击就会跟着路径排字；
        2. 在闭合路径上单击就会框在图形里排字；
           1. 想在闭合路径边上排字需要按 alt；
    8.  m：矩形工具；
    9.  \：直线工具；
    10. k：实时上色工具，需要先选定
    11. b：画笔工具，路径画笔，会自动生成路径，[ 和 ] 键可以放大缩小笔触；
    12. shift+b：斑点画笔工具，填充画笔，会自动生成形状；
    13. shift+n：形状工具，随手画个形状，自动生成规则图形；
    14. 连接工具：根据圈起来的点，连接两条路径，路径会合并，以先圈到的路径样式为准；
    15. shift+e：橡皮擦工具，给形状挖坑用的；
    16. s：比例缩放工具
        1.  选中形状按住 shift 键直接缩放：对形状进行比例缩放；
        2.  选中形状按住~键进行缩放：对填充背景进行比例缩放；
    17. g：渐变工具，针对填色；
    18. i：吸管工具，针对填色；
        1.  按快捷键 i：当油漆桶用；
    19. w：混合工具，连接两个形状并混合颜色；
        1.  双击混合工具，可以设置混合模式为渐变或者步阶；
    20. shift+o：画板工具，针对画板进行尺寸调整等操作，双击画板可以详细设置；
3.  图层和组
    1.  选择多个形状，cmd+g 进行编组，cmd+shift+g 取消编组；
    2.  图层右上角的设置里包括一个“粘贴时记住图层”的选项，可以在图层分组复制粘贴时选择是否包括图层分组信息；
4.  图像描摹：将位图转换为矢量图，此功能需要从窗口菜单下选开；
5.  路径查找器：形状的布尔操作，此功能需要从窗口菜单下选开；
6.  效果：
    1. 3D：用平面矢量模拟 3D 效果；
    2.
