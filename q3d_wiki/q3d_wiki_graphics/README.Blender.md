## 《元宇宙工作学习笔记》-- Blender

<br>

#### 介绍

---

- 项目文档记录了笔者工作积累的云原生微服务技术笔记，以及元宇宙、数字孪生、程序化建模工作学习笔记；
- 项目包含一套元宇宙建模方法论和工程，采用 QGIS -> Blender / Houdini -> Unreal / Unity 的方式实现数字孪生虚拟城市；
- 项目包含一个 Unreal 客户端工程和一个 Unity 客户端工程（建设中）；
- 项目包含一个 Cesium 网页端工程，实现 WebGIS BI 展示；
- 项目包含一个 Vue.js / ElementPlus / TS 网页端工程，实现 Admin 管理后台；
- 项目包含一个 Java / SpringBoot 微服务工程和一个 C# / DotNet 微服务工程（建设中），共用网关和服务注册中心（Nacos），可以协同工作；
- 项目包含一个 Publish 部署工程，采用 Docker / Kubernetes / Jenkins 方式进行服务部署；
- 程序语言涉及 C++ / Java / C# / Python / TS / Shell / VEX；
- 错漏之处恳请读者不吝指正，转载望注明出处：<https://gitee.com/yexiang841/q3d_project/>；
- Copyright 叶湘 ( weixin:yexiang841, email:yexiang841@qq.com ), Shitouren Studio. All Rights Reserved.

<br>

#### 元宇宙、数字孪生、程序化建模工作学习笔记

---

- [《元宇宙工作学习笔记》-- 数字孪生虚拟城市建模方法](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_graphics/README.Meta.md)
- [《元宇宙工作学习笔记》-- 图形学基础](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_graphics/README.Graphics.md)
- [《元宇宙工作学习笔记》-- QGIS](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_graphics/README.QGIS.md)
- `>>>>` [《元宇宙工作学习笔记》-- Blender](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_graphics/README.Blender.md)
- [《元宇宙工作学习笔记》-- C4D](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_graphics/README.C4D.md)
- [《元宇宙工作学习笔记》-- Houdini](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_graphics/README.Houdini.md)
- [《元宇宙工作学习笔记》-- SpeedTree](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_graphics/README.SpeedTree.md)
- [《元宇宙工作学习笔记》-- Illustrator](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_graphics/README.Illustrator.md)

<br>

#### Blender 简介

---

官网：<https://www.Blender.org>；

优势：

- 免费开源；
- 更新快、社区热度高；
- 跨平台；
- 全流程支持；

版本选择：

- 首先确保使用 LTS 版本；
- 需要考虑各种插件的适配性，目前 3.x 版本上 BlenderGIS 插件无法正常使用，因此推荐 2.93 LTS 版本；

教程：

- https://www.bilibili.com/video/BV1p54y1y7Gx（顺子老师零基础入门）
- https://www.bilibili.com/video/av44803831（顺子老师入门）
- https://www.bilibili.com/video/av28455284（顺子老师进阶）
- https://space.bilibili.com/164094818/channel/detail?cid=56637（Blender 2.8x 中文基础）
- https://www.bilibili.com/video/av909518/（台湾大神）待刷
- https://www.bilibili.com/video/av17379560（物理系统详细教程）待刷
- https://www.bilibili.com/video/av60721343（材质节点教程快速版）
- https://www.bilibili.com/video/av25081413（材质节点教程详细版）
- https://search.bilibili.com/all?keyword=blender%202.80%20中文基础&from_source=nav_search_new（官网视频教程）
- https://space.bilibili.com/35723238?from=search&seid=11499331111341161006（技巧检索教程）

案例：

- https://tutorial.blendercn.org（官网图文教程，其实案例更有意义）
- https://tutorial.blendercn.org/?page_id=4227（官网最全案例）
- https://www.bilibili.com/video/av79165741（迷雾森林）
- https://www.bilibili.com/video/av40593848（自然场景）
- https://www.bilibili.com/video/av42757884（日出场景）
- https://www.bilibili.com/video/av57695441（树）
- https://www.bilibili.com/video/av27252576（流水）
- https://www.bilibili.com/video/av76574938（法线贴图造鹅卵石+流水）
- https://www.bilibili.com/video/av78920142?p=3（约束）
- https://www.bilibili.com/video/av89537826?from=search&seid=17557707107846711401（一座城市的材质渐变效果）
- https://zhuanlan.zhihu.com/p/68940371（37个 Blender 小贴士）
- https://www.bilibili.com/video/av63116873/?spm_id_from=333.788.videocard.0（申公豹长毛）

资源：

- https://jpsmile.com/extreme-pbr-combo-v3-2/（extreme pbr combo 3.2）
- https://item.taobao.com/item.htm?id=599147163323（飞扬先生）
- https://pan.baidu.com/s/1QWd1eayk1hu_lYmOt-Fgkw 提取码：78yc
- 只剩一瓶辣椒酱的 Blender 材质
  - https://www.blendermagic.cn
  - https://item.taobao.com/item.htm
  - 订单 895011395616243462 持续更新 921515788
  - https://pan.baidu.com/s/1pJmG4Fi0vDWlDJ5ADTzbpQ 提取码：05cf

其它：

- 参考：<http://www.bgteach.com/article/125>；
- 参考：<https://www.zhihu.com/question/267059879/answer/580735644>；
- 参考：<https://www.zhihu.com/question/288361117/answer/464794185>；

<br>

#### 配置

---

- 把渲染器设定为 Cycles + Rendered 能提升视图操作的流畅度；

- 建议把需要用到的字体放在独立的地方（因为适合放在 3D 场景中的字体是少部分），并在 File Path 中指定；

- 保存外部数据，把相关的素材（音频、视频、材质贴图等，全打包在一个 .blend 文件中）；

<br>

#### 视图

---

视图布局：

- 建议在大纲视图中把可选择性（箭头）、可见性（眼睛）、开启渲染（照相机）打开，以方便后续操作；
- Blender 把布局分门别类，不同的工作流使用不同的布局；
- 修改视图布局之后要记得在 File -> Default 中保存启动文件；

视角操作：

- 滑动 = 旋转，shift 滑动 = 移动，ctrl 滑动/触摸板缩放 = 缩放
- 在 Gizmo 上可直接做旋转操作，Gizmo 下面的小手、放大镜分别是视角平移操作和缩放操作，可以左键点住持续操作；

ASDW 模式：

- shift+` 进入 asdw 操作模式，类似 Unreal，鼠标左键在当前视角返回编辑模式，鼠标右键返回操作前的视角；

视角切换：

- ` 键可以直接呼出视角菜单；
- 在 Gizmo 上点 X、Y、Z 可直接做正视视角切换操作

相机视图：

- Gizmo 下面的摄像机可以进入相机视图；
- ` 键呼出的视角菜单也可以进入相机视图；
- 相机视图随便动一下就会退出，但是在 n 工具栏 View 中锁定 Camera To View 之后，就能根据视角动态调整相机方位；
- 若相机设置为正交视图，则锁定 Camera To View 失去缩放效果，需要在属性窗口调整；

正交和透视：

- Gizmo 下面的网格键可以切换透视视图（近大远小）和正交视图（常用于 X、Y、Z 视图精确建模）；

相机视图的正交和透视：

- 场景切换为正交视图之后，相机还是透视，相机的切换在属性窗口设置；

单独显示：

- h 键可隐藏选中物体，shift + h 可隐藏所有未选中物体，alt + h 取消隐藏；
- 大纲视图中显示眼睛图标之后，也可手动操作。注意，大纲中的电脑图标是开启禁用，比隐藏显示优先级更高；

居中显示：

- 小键盘 . 可居中显示所选物体，没有小键盘可以用 ` 键呼出视角菜单选择居中显示；

裁剪范围：

- n 工具栏中视图选项可选择裁切起点和终点；

相机视图的裁剪范围：

- 相机视图的范围需要在属性栏单独设置；

<br>

#### 视图模式

---

物体模式只对物体进行移动旋转缩放操作，编辑模式才能改变物体本身：

- [切换视图模式] ctrl + tab 呼出模式菜单；

<br>

#### 坐标系

---

- 正上方选项卡可以切换全局和局部坐标系；
- , 键呼出坐标系切换选项；

<br>

#### 游标

---

游标是 Blender 中使用的对齐方式，思路就是先把游标对齐到 A，再把 B 对齐到游标；

- [对齐] 选中 B 物体，使用 shift + s 将游标放置到对齐点，选中 A 物体，shift + s 将物体对齐到游标；
- 手动放置游标的机会比较少，更多的是吸附操作；
- 选中物体，shift + s 呼出吸附带单，然后选择游标吸附到选中项，甚至在编辑模式也能进行这项操作；
- shift + c 游标位置归零；

<br>

#### 着色模式

---

四个着色模式分别是：

- 线框（没有面着色）；
- 平面着色（Solid）；
- 材质预览（Eevee 实时渲染）；
- 渲染（Cycles）；

z 键呼出着色模式菜单；

<br>

#### 集合（大纲中的分组）

---

指的是大纲视图中的分组，作用是归类，在大纲中便于管理；

- [建立集合] 右键 -> Move To Collection 可以实现物体变换集合以及新建集合；

<br>

#### 父子级（打包）

---

指的是大纲视图中的分组，作用是打包物体，方便整体变换；

- [建立父级] 选中多个物体，按住 shift 选中其中一个作为父级，然后 ctrl + p 打包；
- [清空父级] 物体模式下选中多物体后 ctrl+p 建立父子级，alt+p 清空父子级；

<br>

#### 合并和分离

---

与集合分组不同，物体的合并是真正意义上的合并，点线面都会归入一个 Object，再要分离就得进入编辑模式选取：

- [合并] 物体模式下选中两个物体，ctrl + j 将其合并；
- [分离] 编辑模式下用 l 选择松散网格，也可以 ctrl + l 扩大选择到松散物体，然后 p 将其分离（按选中项、松散块、材质分离）；

<br>

#### 添加删除复制

---

- [添加] shift + a，添加完之后，左下角会有一个快速编辑面板，可以对基础数据进行快速编辑；
- [删除] 选中后 x（要回车确认），选中后 del（不需确认）；
- [复制] shift + d，再按 x/y/z 可以沿着轴摆放，按住 ctrl 可以进行吸附移动；
- [重复操作] shift + r；

<br>

#### 关联复制

---

关联复制是指物体使用相同的数据属性，会同时变换，能节省空间。做头发、做地编等场景常用；

- 关联复制的时候，物体的变换（移动旋转缩放）是可以不同的，但物体的编辑是同时的；
- [关联复制] alt + d，再按 x/y/z 可以沿着轴摆放，按住 ctrl 可以进行吸附移动；
- 在右侧数据面板可以看到物体内部的数据集，还有关联了多少物体，以及解除关联；
- 要注意关联操作会跟物体命名脱节，因此建议养成良好的命名习惯；

<br>

#### 选择

---

- [全选] a ；
- [框选] 物体模式下鼠标直接框；
- [刷选] c 进入刷选，此时滚轮变成笔刷大小（而不是视图操作），回车结束选择；

<br>

#### 变换

---

基础变换：

- [非精确变换] 移动/旋转/缩放 = g/r/s；
- [精确变换] 使用 g/r/s 之后，再按 x/y/z 可以延轴变换，按住 shift 缓慢变换，按住 ctrl 吸附变换；
- [精确变换] t 工具栏呼出 Gizmo 然后按轴线变换；
- [精确变换] n 工具栏 item 页可以直接调整变换数值；
- [精确变换] 右侧物体属性栏也可以直接调整变换数值；

变换一个不在原点的物体，或者变换一组物体时，变换的轴心点就很重要，变换的轴心点可以设置为：

- 游标；
- 质心；
- 各自的原点；

变换轴心：

- . 键呼出轴心点切换选项；

变换吸附：

- 可以用 . 键呼出轴心点切换选项；
- 按住 ctrl 键变换的同时可吸附增量；
- 还可以在顶部打开磁铁开关，除了吸附增量之外，还可以吸附点线面；

随机变换，常用于毛发、地编等制作：

- 在物体 -> 变换中可选择随机变换，然后可调参数；

<br>

#### 设置原点

---

有一些情况下物体的中心会没设置好，比如分离出来的物体，或者外部导入的素材，需要调整以方便变换操作；

- 右键可将原点设置到几何中心；
- 可先移动游标，然后再将原点移动到游标；
- 也可以在基础变换的时候，右上角选项选择仅移动原点；

<br>

#### 编辑模式

---

编辑模式下的修改都是破坏性的，如果想要不破坏的修改，建议使用修改器；

- [切换为编辑模式] tab
- [反选] 编辑模式下 l 可以选择松散物体部分，ctrl + i 可以反选；
- [环选] 编辑模式下 alt + 选择；
- [切割] 编辑模式下 ctrl + r 环切；
- [挤出] 编辑模式下 e，然后按 ctrl 沿法线挤出；
- [挤入] 编辑模式下 i；
- [倒角] 编辑模式下 ctrl + b 对线倒角（破坏性），或者使用倒角修改器（非破坏性）；
- [扫描] 在曲线的右侧物体属性菜单中倒角或者物体倒角；

- [编辑模式] ctrl+e(边选框) f(面选框) v(点选框)；
- [编辑模式] shift+e 边线折痕；

<br>

#### 样条

---

- [样条.生成] e 往两头加点，右键细分往中间加点，v 改点模式，f 封闭 y 分离（两点），p 拆分（单点）；
- [样条.属性] 路径曲线很好用，shift+n 重置法线，右侧物体数据属性 2D 化强行拍平至局部坐标系；
- [样条.平面] 建立平面物体删除“仅面”然后右键转化为曲线，就可以建立平面样条线；

- [物体模式] ctrl+数字 添加表面细分修改器（数字是视图细分值）；

<br>

#### 网格

---

细分：

- 重构网格修改器的作用是将变形之后的不均匀网格进行重新布线；

法向：

- 编辑模式下 alt + n 呼出法向菜单，里边有关于法线的全部操作；

<br>

#### 修改器

---

在 apply 之前，修改器的修改都是非破坏性的；

- 陈列
- 倒角
- 布尔
- 精简
- 镜像
- 实体化
- 表面细分
- 线框
- 置换
- 晶格
- 缩裹

<br>

#### 材质

---

- [节点颜色] 绿色=着色器，黄色=颜色，灰色=值，紫色=向量；
- [添加纹理] 打开 Node Wrangler 之后快捷键 ctr+t 可快速添加纹理 UV；
- [材质关联] 选中两个物体后 ctrl+l 可统一到活动项材质，在右侧材质列表中点击关联数可独立出来；
- [常用 01.塑料] 菲涅尔+漫射+光泽->混合着色器->表面材质输出；
- [常用 02.玻璃] 玻璃->表面材质输出；
- [常用 03.造型灯] 黑体+光线衰减->自发光->表面材质输出；
- [常用 04.单面发光] 几何数据背面+自发光+透明 BSDF->混合着色器；
- [常用 04.石头] voronoi->法线贴图->置换->置换材质输出（cycles）；
- [常用 05.云雾] 体积散射->体积材质输出 + [圆形石头]；
- [常用 06.金属] 原理化 BSDF->表面材质输出，杂色纹理/波浪纹理->凹凸/置换可制造不规则纹理；
- [常用 07.蜡烛] 次表面散射->表面材质输出；
- [常用 08.纸/树叶] 半透明 BSDF->表面材质输出；
- [常用 09.凹凸贴图] 菲涅尔+(纹理->凹凸->漫射)+(纹理->凹凸->光泽)->混合着色器->表面材质输出；
- [常用 10.色温渐变] 渐变纹理+黑体->混合 RGB->自发光；
- [常用 11.透明发光体] 自发光+透明 BSDF->混合着色器；
- [常用 12.颜色分离] 图像纹理->分离 RGB->混合着色器的系数；
- [保存方案] b 加框框，然后 ctrl+j 保存；
- [展开/收起] h；
- [禁用/启用] m；
- [删除线] ctrl+右键滑动；
- [快速连接] alt+右键滑动；
- [添加转折点] shift+右键滑动；
- [替换节点] 选中节点之后 shift+s；
- [快速预览] ctrl+shift+点击一个节点；

<br>

#### 动画

---

- [镜头 1.生成] 直接生成相机=在 3D 游标生成相机；当前视角生成相机；
- [镜头 2.进入] 点击视图右侧相机=进入镜头视角，切换到默认摄像机/解除锁定到视图之后转动=退出镜头视角；
- [镜头 3.调整] n 呼出锁定相机到视图，然后直接移动=镜头调整（不勾为调整显示比例）；
- [镜头 4.目标] 建议以空物体作为相机目标，调整相机的标准跟随时需要打开局部坐标轴调整坐标；
- [镜头 5.切换] 动画中切换相机需要在动画模式下添加标记然后设定相机；
- [动画 1] 空格=播放，shift+←=回到头帧；
- [动画 2] ？调整点类型，t 调整插值类型；
- [动画 3] 无限循环：在通道的外插模式里边选择线性外推；
- [动画 4] 切换镜头：在切换点添加标记，选中新相机，将相机绑定到标记；
- [动画 5] 选中曲线后 n 可以呼出修改器，为曲线添加变化；

<br>

#### 骨骼

---

- 编辑模式下，n 键呼出的工具菜单中有快速镜像功能；
- 编辑模式下，骨骼可以挤出和细分；
- 骨骼绑定方法：
  1. 先选物体，再选骨骼；
  2. ctrl+tab 进入姿态模式；
  3. ctrl+p 绑定骨骼父级；
- 在姿态模式下可以选择骨骼然后在 n 工具菜单中锁定轴，以实现固定方向旋转；
- 编辑模式下选中骨头 ctrl + p 将骨头设定为相连项；
- 编辑模式下 ctrl+p 保持偏移量相当于移动约束；
- 在编辑下建立骨骼才会是一套骨骼；

<br>

#### 渲染器

---

- Cycles 渲染（光线跟踪）需要调整的参数有：
  - 光程调整反弹次数（用于调整暗部光泽，大概 4~8 次就好）；
  - 反射焦散和折射焦散（玻璃材质把光折射出去的场合才用，否则去掉）；
  - 采样影响（主要减少噪点，根据视图效果来调整就好）；
  - 降噪（勾上之后就可以适当降低采样数）；
  - X/Y 拼接（CPU 用 16/32，GPU 用 256/512）；
  - 渲染框（勾上之后只渲染框框中的部分）；
- Eevee 渲染（实时渲染）需要调整的参数有：
  - 环境光遮蔽 AO（物体靠的越近阴影越黑，建议打开）；
  - 次表面散射（需要光透视的时候打开）；
  - 屏幕空间反射/折射（用于玻璃材质透视和反射，需要配合材质中打开屏幕空间反射/折射）；
  - 接触阴影（在光属性中调）；
  - bloom（可选，辉光滤镜风格）；
  - 在需要镜面反射的地方添加光照探头反射平面（光栅通用方法）；
  - 在需要漫反射的地方添加光照探头辐射体积（C4D 中的全局光照 GI，Unity 中的 LP）；
  - ctrl+b 局部渲染框（尤其作用于 cycles）；
- 贴图烘焙：<https://www.bilibili.com/read/cv6051841/>；

<br>

#### 插件

---

- Image Export：在建模的时候添加图片，包括高程图等；
- Extra Object：各种参数化模型，如参数化石头、参数化齿轮等；
- A.N.T landscape：景观生成器；
- Rigify：添加父级菜单；
- LoopTools：阵列工具；
- MirrorTools：镜像工具；
- Node Wrangler：在使用节点的时候支持快捷键；
- Cell Fracture：破碎与分裂；
- Material Library / Material Utilities：预设材质库；
- Tri-lighting：三点打光插件；
- easyHDRi：hdri 预览插件；
- extreme pbr combo：1100 多种 PBR 节点式材质；
- 万物有灵：有一些不错的打光方案的预设；

<br>
