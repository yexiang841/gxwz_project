## 《元宇宙工作学习笔记》-- 数字孪生虚拟城市建模方法（Prologue）

<br>

#### 介绍

---

- 项目文档记录了笔者工作积累的云原生微服务技术笔记，以及元宇宙、数字孪生、程序化建模工作学习笔记；
- 项目包含一套元宇宙建模方法论和工程，采用 QGIS -> Blender / Houdini -> Unreal / Unity 的方式实现数字孪生虚拟城市；
- 项目包含一个 Unreal 客户端工程和一个 Unity 客户端工程（建设中）；
- 项目包含一个 Cesium 网页端工程，实现 WebGIS BI 展示；
- 项目包含一个 Vue.js / ElementPlus / TS 网页端工程，实现 Admin 管理后台；
- 项目包含一个 Java / SpringBoot 微服务工程和一个 C# / DotNet 微服务工程（建设中），共用网关和服务注册中心（Nacos），可以协同工作；
- 项目包含一个 Publish 部署工程，采用 Docker / Kubernetes / Jenkins 方式进行服务部署；
- 程序语言涉及 C++ / Java / C# / Python / TS / Shell / VEX；
- 错漏之处恳请读者不吝指正，转载望注明出处：<https://gitee.com/yexiang841/q3d_project/>；
- Copyright 叶湘 ( weixin:yexiang841, email:yexiang841@qq.com ), Shitouren Studio. All Rights Reserved.

<br>

#### 元宇宙、数字孪生、程序化建模工作学习笔记

---

- `>>>>` [《元宇宙工作学习笔记》-- 数字孪生虚拟城市建模方法](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_graphics/README.Meta.md)
- [《元宇宙工作学习笔记》-- 图形学基础](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_graphics/README.Graphics.md)
- [《元宇宙工作学习笔记》-- QGIS](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_graphics/README.QGIS.md)
- [《元宇宙工作学习笔记》-- Blender](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_graphics/README.Blender.md)
- [《元宇宙工作学习笔记》-- C4D](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_graphics/README.C4D.md)
- [《元宇宙工作学习笔记》-- Houdini](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_graphics/README.Houdini.md)
- [《元宇宙工作学习笔记》-- SpeedTree](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_graphics/README.SpeedTree.md)
- [《元宇宙工作学习笔记》-- Illustrator](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_graphics/README.Illustrator.md)

<br>

#### 软件要求

---

- GIS 软件：QGIS / ArcGIS / Arcsence / GlobalMapper；
- 空三建模软件：MetaShape(PhotoScan) / ContextCapture(Smart3D) / 大疆智图 / Pix4Dcapture；
- 3D 建模：Blender / 3DsMax / Maya / C4D；
- 模型雕刻：ZBrush；
- 材质贴图：Substance Designer；
- 地形建模：Houdini / Gaea / WorldMachine / Unreal / Unity；
- 植被建模：SpeedTree / Forester / VueXstream / LaubWerk / TreeStorm；
- 流水：Houdini / Realflow / Unreal / Unity；
- 云：Houdini / Unreal / Unity；
- 火焰 / 烟雾 ：Houdini / Unreal / X-Particles / TurbulenceFD；
- 粒子：Houdini / X-Particles / Unreal / Unity；
- 骨骼：Maya / Blender / Unreal；
- 网络资源转换 C4D / 3DsMax / Blender；
- 视频合成：Premiere / Nuke / AfterEffects / Hitfilm；

<br>

#### 硬件要求

---

- CPU：AMD 3990x，建议 AMD 系列，多核架构适合渲染解算，推高性价比；
- 显卡：Nvidia：3080t 以上，一定要是 N 卡，因为主流渲染器对 N 卡支撑得更好；
- 系统：Windows / Linux，如果做渲染，还是尽早放弃 Mac 吧，苹果公司跟 N 家断交了，而各软件对 A 家支持还没跟上，更别提苹果自己的 M 系列；

<br>

#### 平台与架构

---

1.  C/S 架构：
    - Unreal 体系：跨平台支持；
    - Unity 体系：跨平台支持；
2.  B/S 架构：
    - Three.js 体系：；
    - Cesium 体系：；

<br>

#### 虚拟城市数字孪生建模理论方法汇总

---

1.  全手工建模
    - 工作流：想要效果好，人工不能少；
    - 优点：想要多精细就有多精细；
    - 缺点：周期长、人力成本高；
2.  程序化建模（TA 向 -> AI 向）
    - 工作流：以图形学、数学为支撑、使用 Houdini、Unreal、Unity 等程序语言进行自动规划建模，预测应该是未来 AI 建模的基础；
    - 优点：降本增效真科技；
    - 缺点：目前人才紧缺，缺乏各类不同品类建模方法和程序的沉淀；
3.  空三建模
    - 工作流：倾斜摄影 -> 空三建模软件自动建模（点云、Mesh、TiledMesh、Texture）
    - 优点：全程自动化；
    - 缺点：精细度严重不够，即使打点也不够；
    - 缺点：模型全都是表层模型，无法优化，很难拆解，也缺少完美契合的应用场景；
4.  依托 GIS 数字资产的半自动化建模
    - 工作流：自行寻找 OSM/DEM/POI 等 GIS 数字资产 -> GIS 软件处理素材 -> 3D 建模软件做建筑建模 -> 3D 效果软件做环境建模 -> Unreal/Unity 交互
    - 优点：定制程度最高；
    - 缺点：需要学习的内容非常多，需要掌握的软件也不少；
    - 缺点：Blender 的建筑建模不够精细；
    - 数字高程模型（DEM）简介：知乎数据禾：<https://zhuanlan.zhihu.com/p/106722117>；

<br>

#### GIS 数字资产获取方式

---

1.  OpenStreetMap
    - 官网：<https://www.openstreetmap.org>；
    - 资源列表：<http://download.openstreetmap.fr/extracts/asia/china/>；
    - 德国镜像资源列表：<https://download.geofabrik.de>；
    - 数据资产：
      - 全球地表人造物分类 OSM，免费（osm_geofabrik_de）
    - 备注：
      - 下载 shp 包解压就是 .shp/shx/sbx/dbf/kmz 文件，大部分 gis 软件都能使用，文件占存储较小（china.shp 目录大约 5G）；
      - 下载 osm.bz2 直接解压就是 .osm 文件，占用较大存储（china.osm 一个文件就 21G），一些插件会需要 .osm 格式，比如 sketchup 插件；
      - 下载 osm.pbf 经过了压缩，需要转换成 .osm 文件或者.shp 文件才能使用
      - 将 pbf 转换成 .osm 使用工具 Osmconvert：<https://wiki.openstreetmap.org/wiki/Osmconvert>；
      - 在线将 osm/osm.pbf 转换为 shp 的网站：<https://geoconverter.hsr.ch/vector>；
    - 优点：免费开源、长期稳定、数据更新也算及时；
    - 缺点：精细度达不到城市建模之用；
2.  全球地理信息公共产品（GlobeLand）
    - 官网：<http://www.globallandcover.com/>；
    - 数据资产：
      - 30 米精度地表覆盖物（osm_webmap_globeland_30m）：<http://www.globallandcover.com/defaults.html?type=data&src=/Scripts/map/defaults/browse.html&head=browse&type=data>；
    - 优点：具有对 osm_webmap_globeland_30m 数据资源的解析和相关工具；
    - 优点：基础地理信息数据，一到三年更新一次，免费；
3.  中国工程科技知识中心（CKCEST）：
    - 官网：<https://www.ckcest.cn/entry/>；
    - 资源列表：<https://kmap.ckcest.cn/>；
    - 测绘资源列表：<https://kmap.ckcest.cn/otherSearch/searchCgmlPage>；
    - 数据资产：
      - （GlobeLand 资源镜像）30 米精度地表覆盖物（osm_webmap_globeland_30m）：<https://kmap.ckcest.cn/resource/search/normal?words=&dataCode=global30>；
      - 1:25 万全国基础地理数据/矢量地图数据（DLG），免费（osm_webmap_25w）：<https://kmap.ckcest.cn/nationwideMap/index>；
      - 1:100 万全国基础地理数据/矢量地图数据（DLG），免费（osm_webmap_100w）：<https://kmap.ckcest.cn/nationwideMap/index>；
      - 数字高程模型（DEM），只能在线查看不能下载；
      - 分幅正射影像数据（DOM），只能在线查看不能下载；
      - 数字栅格地图（DRG），只能在线查看不能下载；
      - 航空摄影图像，只能在线查看不能下载；
      - 航天摄影图像，只能在线查看不能下载；
4.  全国地理信息资源目录服务系统（WebMap）
    - 官网：<https://www.webmap.cn>；
    - 资源列表：<https://www.webmap.cn/commres.do?method=dataDownload>；
    - 数据资产：
      - （GlobeLand 资源镜像）30 米精度地表覆盖物（osm_webmap_globeland_30m）：<https://www.webmap.cn/commres.do?method=globeIndex>；
      - （CKCEST 资源镜像）1:25 万全国基础地理数据/矢量地图数据（DLG），免费（osm_webmap_25w）：<https://www.webmap.cn/commres.do?method=result25W>；
      - （CKCEST 资源镜像）1:100 万全国基础地理数据/矢量地图数据（DLG），免费（osm_webmap_100w）：<https://www.webmap.cn/commres.do?method=result100W>；
5.  地理国情监测云平台（DSAC）：
    - 官网：<http://www.dsac.cn>；
    - 资源列表：<http://www.dsac.cn/DataProduct/Search?>；
    - 数据资产：
      - 全国乡镇行政区划（），在资源列表中搜索；
      - 全国各省市电子地图矢量数据（），在资源列表中搜索；
      - 全国人口密度数据，在资源列表中搜索；
      - 全国土地覆盖数据，在资源列表中搜索；
      - 全国土地利用数据，在资源列表中搜索；
      - 全国县级医疗资源分布数据，在资源列表中搜索；
      - 全国 GDP 公里格网数据，在资源列表中搜索；
    - 优点：数据类型非常丰富，适合用于地理学/社会学研究分析；
    - 缺点：下载需要提交审核；
6.  空间地理数据云（GSCloud）：
    - 官网：<http://www.gscloud.cn/>；
    - 公开数据：<http://www.gscloud.cn/sources/index?pid=1&rootid=1>；
      - 数字高程数据（DEM）免费资源列表：<http://www.gscloud.cn/sources/index?pid=302&ptitle=DEM%20%E6%95%B0%E5%AD%97%E9%AB%98%E7%A8%8B%E6%95%B0%E6%8D%AE&rootid=1>；
      - 各外国卫星公开数据产品；
    - 高分辨率数据：<http://www.gscloud.cn/sources/index?pid=2&rootid=2>；
      - 国产高分一到三号卫星数据产品；
      - 国产资源一号、三号卫星数据产品；
    - 优点：最全的国产卫星功能数据介绍（虽然不一定有得下）；
    - 优点：汇聚了可以下载的 30M 分辨率以上的公开 DEM 数据（精度最高的是 GDEMV3）；
7.  规划云：
    - 官网：<http://guihuayun.com/>；
    - 参考：B 站：<https://www.bilibili.com/video/BV1j441177n7>；
    - 数据资产：
      - 多风格地图底图：<http://guihuayun.com/maps/index.php>；
      - 行政区划 JSON 格式数据下载：<http://guihuayun.com/maps/region.php>；
      - VIP 入会需要 299；
    - 优点：提供各种资源指引、网站导航、方法论等，还有很多在线实用工具；
    - 优点：学术氛围浓厚，拥有爱分享的 B 站 Up 主国匠城（B 站上最好的 GIS 软件教学之一）；
    - 优点：拥有活跃度很高的社区，社区分享的数据价值都很高；
    - 缺点：规划云底图工具并不具有地图下载功能，因此只能截图或使用浏览器下载图片的方式获取；
8.  天地图（国家地理信息公共服务平台）：
    - 官网：<https://www.tianditu.gov.cn/>；
    - 数据资产：
      - 在线地图：<https://map.tianditu.gov.cn/>；
      - 地图 API（要申请 key）：<http://lbs.tianditu.gov.cn/server/MapService.html>；
      - 公开数据资源 WFS（不需要申请 key）：<http://gisserver.tianditu.gov.cn/TDTService/wfs>；
    - 优点：资源质量高；
    - 缺点：接口变个不停；
9.  高德地图 LBS：
    - 官网：<https://lbs.amap.com/>；
    - 数据资产：
      - 在线 POI（要申请 key）：<https://lbs.amap.com/api/webservice/summary>；
      - POI 资产分类编码：<https://lbs.amap.com/api/webservice/guide/api/search>；
    - 优点：国内最靠谱的 POI 数据 API 之一，免费的，体验很好；
    - 参考：B 站：<https://www.bilibili.com/video/BV1SK41157KQ>；
    - 参考：B 站：<https://www.bilibili.com/video/BV1RV411k7Gs>；
10. 百度地图 LBS：
    - 官网：<https://lbsyun.baidu.com/>；
    - 坐标拾取接口：<http://api.map.baidu.com/lbsapi/getpoint/>；
    - 数据类型：POI；
    - 优点：国内最靠谱的 POI 数据 API 之一，免费的，体验很好；
11. MapBox：
    - 官网：<https://www.mapbox.com/>；
    - 在线地图：<https://map.tianditu.gov.cn/>；
    - 地图 API（要申请 key）：<http://lbs.tianditu.gov.cn/server/MapService.html>；
    - 优点：资源质量高，稳定；
    - 优点：提供良好的在线接口；
    - 缺点：注册需要验证 VISA 卡；
12. 中科图新云 GIS：
    - 官网：<http://www.tuxingis.com/index.html>；
    - 数据资产：
      - 5 米、12.5 米、30 米高程图 DEM：<http://www.tuxingis.com/activity/544.html?trackingLinkId=4bd44e84a5e34494bad6b3c12f2a2ca8&source=baidu&plan=DEMshuju-neirongye&unit=DEMhexinci&keyword=%7Bdem%7D%E6%95%B0%E6%8D%AE&e_creative=50727699109&e_keywordid=171619051416&e_keywordid2=171619051416>；
    - 优点：精度高；
    - 缺点：数百块一个城市；
13. NextGIS：
    - 官网：<https://data.nextgis.com/>；
    - 资源列表：<https://data.nextgis.com/en/catalog/subdivisions?country=CN>；
    - 数据资产：
      - OSM 资产，收费；
    - 优点：数据资产整理得不错；
14. 超图(SuperMap)：
    - 官网：<https://www.supermap.com/>；
    - 数据资产：OSM / DEM / POI，收费；
    - 优点：数据类型很全，商业网站相对稳定；
    - 优点：实力雄厚，生态丰富，有各种桌面应用、私服、Unreal/Unity 插件等，下载地图（收起费来也）方便；
15. BigeMap：
    - 官网：<http://www.bigemap.com/>；
    - 数据资产：OSM / DEM / POI，收费；
    - 优点：实力大概夹在超图和水经注之间吧，也有一些不错的收费数据；
16. 数据禾：
    - 官网：<https://www.databox.store>；
    - 数据资产：OSM / DEM / POI，收费；
    - 优点：数据类型很全，商业网站相对稳定；
17. 水经注(RiverMap)：
    - 官网：<http://www.rivermap.cn/index.html>；
    - 数据资产：OSM / DEM / POI，收费；
    - 优点：数据类型很全，商业网站相对稳定；
    - 优点：专注于地图下载产品（万能地图下载器），数据类型多，有高精度 DEM；
    - 缺点：实力不强，感觉随时要挂；
18. NASA：
    - 官网：<https://search.asf.alaska.edu/#/>；
    - 数据资产：
      - 12.5 米高精度数字高程图（dem_alos-palsar-rtc_12.5m），免费；
    - 优点：精度高；
    - 缺点：国内城市刻意不提供；
19. 谷歌地图（API）：
    - 官网：<https://developers.google.com/maps>；
    - 数据类型：OSM / DEM / POI / 其它；
    - 优点：一家包揽天下（除了中国），要软件有软件要生态有生态要数据有数据（除了中国）；
    - 缺点：404；
20. 谷歌地球（GoogleEarth）：
    - 将 DEM 数据分块索引(kmz)导入 Google Earth。输出项目区域索引代号，如：北海涠洲岛(N21E109)；

<br>

#### GIS 软件加工 GIS 数字资产方法汇总：

---

1.  ArcGIS 体系
    - 工作流：ArcSence 加工 OSM/DEM/POI 等 GIS 数字资产 -> ?；
    - 参考：B 站：<https://www.bilibili.com/video/BV1Lf4y1U7zo>；
    - 优点：体系和工作流非常成熟；
    - 缺点：收费软件，破解费劲；
    - 缺点：软件比较重，学习成本高；
2.  超图体系
    - 工作流：；
    - 参考：；
    - 优点：体系逐渐成熟；
    - 缺点：闭源产品，各种收费割菜，格局小；
3.  QGIS
    - 工作流：；
    - 参考：；
    - 优点：开源免费跨平台；
    - 优点：可与 ArcGis 匹敌；
    - 优点：一个工具能替代 ArcGis、GlobalMapper、全能地图下载器、WorldMachine；

<br>

#### 3D 软件加载 GIS 资产加工方法汇总

---

1.  Blender + BlenderGIS：
    - 参考：B 站：<https://www.bilibili.com/video/BV1bA411M7d6>；
    - 参考：B 站：<https://www.bilibili.com/video/BV1g541157kP>；
    - 参考：B 站：<https://www.bilibili.com/video/BV1sg4y1z7Dh>；
2.  Blender + Google + MapModelImporter：
    - 参考：B 站：<https://www.bilibili.com/video/BV1AJ411p7yF>；
3.  Blender + OSM：
    - 参考：B 站：<https://www.bilibili.com/video/BV1bA411M7d6>；
    - 参考：B 站：<https://www.bilibili.com/video/BV1g541157kP>；
    - 参考：B 站：<https://www.bilibili.com/video/BV1sg4y1z7Dh>；
4.  CityEngine
    - 参考：；
5.  51World + 51SuperAPI
    - 工作流：；
    - 官网：<https://www.51aes.com/>；
    - 参考：；
6.  PS + 3dMapGeneratorAtlas
    - 工作流：在 PS 里边安装 3dMapGeneratorAtlas 插件，导入 DEM 资产；
    - 参考：B 站：<https://www.bilibili.com/video/BV1ZE411Q7NX>；
7.  SketchUp：
    - 工作流：GoogleEarth 下载 OSM，直接导入 SU；
    - 参考：B 站搬运：<https://www.bilibili.com/video/BV1ft41187uf>；
8.  Rhino + Grasshopper
    - 工作流：；
    - 参考：；
9.  Unreal + ArchvizExplorer
    - 工作流：；
    - 参考：B 站万物唯有凋零：<https://www.bilibili.com/video/BV1x54y1E7YN>；

<br>

#### 程序化建模 - 基础地形（Terrain）

---

资产特性：

- ；

技术栈：

- 方案一：WorldCreator / Blender 做地形生成，Unreal 做地形导入、分片、加载；
- 方案二：Houdini 做地形生成，Unreal 做地形导入、分片、加载；

依赖数据：

- Dem；

参考资料：

- Blender 工作流：<https://www.zhihu.com/question/287885383/answer/2452333523>；
- <https://zhuanlan.zhihu.com/p/135881819>；
- <https://zhuanlan.zhihu.com/p/135881819>；
- <https://zhuanlan.zhihu.com/p/136431110>；
- <https://zhuanlan.zhihu.com/p/89475951>；
- <https://zhuanlan.zhihu.com/c_1172179336513175552>；

工作流：

1.  导入 Dem；
2.  楞状处理（celular noise）；
3.  腐蚀出大的沟壑；
4.  地形拔高；
5.  增加梯田效果；
6.  细节腐蚀；
7.  热腐蚀+沉积（slump node）；
8.  提取坡度较大的部分；
9.  对坡度较大的部分进行处理；

<br>

#### 程序化建模 - 山体（Moutain）

---

资产特性：

- 山体的垂直平面细节，需要解决法线以及 UV 拉伸的问题；

技术栈：

- ；

依赖数据：

- Dem；

参考资料：

- 悬崖制作：<https://www.bilibili.com/video/BV1sf4y1v7RG>；

工作流：

1.  ；
2.  ；
3.  ；

<br>

#### 程序化建模 - 岩石（Rock）

---

资产特性：

- 岩石（Rock）：分为山体中的岩石、水流中的岩石、以及砂砾地貌中的岩石；
- 在需要石头的位置绘制 density；

依赖数据：

- 暂无；

参考资料：

- <>；

技术栈：

- Houdini+Unreal+QuixelMegascans；

工作流：

1.  ；
2.  ；
3.  ；

<br>

#### 程序化建模 - 海洋（Sea）

---

资产特性：

技术栈：

- Houdini + Unreal + QuixelMegascans + 基于 FFT；

依赖数据：Dem；

参考资料：

- 教程 1：<https://www.bilibili.com/video/BV1XJ411Y7YF>；
- 教程 2：<https://www.bilibili.com/video/BV1P7411J7nJ>；
- 官方教程：<https://vimeo.com/261901539>；
- 鲨鱼出水案例：<https://www.bilibili.com/video/BV1Mx411o7Bs?from=search&seid=2782300825587372472>；

工作流：

1.  平面缩放（Grid Size）；
2.  重力场（Gravity）：海浪汹涌程度；
3.  随机种子（Sead）：波浪随机形态；
4.  风力（Wind.Speed）：决定涟漪大小；
5.  浪花对比度（Wind.Direction Bias）：调小会令细节增多，调大会联结成片；
6.  浪头移动（Wind.Direction Movement）：这一项决定浪头是否移动（而不是原地起伏）；
7.  浪头掉落（Wind.Chop）：决定了浪头要不要翻折掉落；
8.  浪的高度（Amplitude.Scale）；

<br>

#### 程序化建模 - 河流湖泊水库瀑布（Water）

---

资产特性：

- 首先是解决地形标注精准度的问题，其次是是水流特效的制作；

技术栈：

- ；

依赖数据：Dem；

参考资料：

- GAT 案例：；
- <https://www.bilibili.com/video/BV1fb411S7CE/?spm_id_from=333.788.videocard.4>；
- 来自杨超的总结分析：<https://zhuanlan.zhihu.com/p/108608470>；

工作流：

1.  生成河面 mesh；
2.  投射到地面并挖沟；
3.  放置石头；
4.  放置粒子发射器（生成浪花用）；
5.  生成阶梯瀑布；

<br>

#### 程序化建模 - 沙滩（Beach）

---

资产特性：

技术栈：

- ；

依赖数据：Dem；

参考资料：

- <https://www.bilibili.com/video/av457683104>；

工作流：

1.  ；
2.  ；
3.  ；

<br>

#### 程序化建模 - 草地（Grass）

---

资产特性：

技术栈：

- ；

依赖数据：

- Dem；

参考资料：

工作流：

1.  ；
2.  ；
3.  ；

<br>

#### 程序化建模 - 灌木（Bush）

---

资产特性：

技术栈：

- ProceduralFoliageSpawner（程序化植物生成器）；

依赖数据：

- 经过 PS 加工的 GIS 数据；

参考资料：

- <https://www.linecg.com/video/play29960.html>；
- <https://tieba.baidu.com/p/6484121116>；
- <https://www.bilibili.com/video/BV1Ab411F7Dd?p=1>；

工作流：

1.  ；
2.  ；
3.  ；

<br>

#### 程序化建模 - 森林（Forest）

---

资产特性：

- 山谷树密、山顶树稀；
- 每棵树有自己的势力范围；
- 用 Houdini 生成树木点，用 Unreal 生成树木；

技术栈：

- ProceduralFoliageSpawner（程序化植物生成器）；

依赖数据：

- 经过 PS 加工的 GIS 数据；

参考资料：

- <https://www.linecg.com/video/play29960.html>；
- <https://tieba.baidu.com/p/6484121116>；
- <https://www.bilibili.com/video/BV1Ab411F7Dd?p=1>；

工作流：

1.  ；
2.  ；
3.  ；

<br>

#### 程序化建模 - 农田（Farm）

---

资产特性：

技术栈：

- Houdini 圆角多边形程序化建模；

依赖数据：

- 经过 PS 加工的 GIS 数据；

参考资料：

- <>；

工作流：

1.  ；
2.  ；
3.  ；

<br>

#### 程序化建模 - 村庄（Village）

---

资产特性：

- 村庄通常包括无序低矮建筑、独栋/联排别墅、无序道路、农场、鱼塘等。；

技术栈：

- ；
- 白模 + 随机材质；

依赖数据：

- Dem；

参考资料：

- AI 随机拼接：<https://www.artstation.com/artwork/oOW8NB>；

工作流：

1.  ；
2.  ；
3.  ；

<br>

#### 程序化建模 - 路网（Road）

---

资产特性：

- 道路主要包括铁路、公路和土路、路网交界处的路桥以及道路周边的绿化植被；

技术栈：

- ；

依赖数据：

- OSM；

参考资料：

- <https://zhuanlan.zhihu.com/p/73570541>；
- <https://zhuanlan.zhihu.com/p/81061083?from_voters_page=true>；

工作流：

1.  ；
    - 沥青；
    1. 补丁；
    2. 裂纹（裂缝中的草）；
    - 车痕；
    - 条纹；
    - 边缘沙土；
    - 交叉口；
2.  ；
3.  ；

<br>

#### 程序化建模 - 城镇（Urban）

---

资产特性：

- 从航空图上来看，城镇中面积较大部分包括区域式建筑群、地标式建筑群、公路网、绿地、工地；

技术栈：

- ；

依赖数据：

- OSM；

参考资料：

- 城市生成：<https://www.bilibili.com/video/BV1RW411p7Qx?p=1>；
- 随机建筑：<https://www.bilibili.com/video/BV1J7411i7nz/?spm_id_from=333.788.recommend_more_video.0>；
- 建筑生成：<https://zhuanlan.zhihu.com/p/508224675>；

工作流：

1.  ；
2.  ；
3.  ；

<br>

#### 程序化建模 - 天空球（Sky）

---

资产特性：

技术栈：

- ；

依赖数据：暂无；

参考资料：

- <>；

工作流：

1.  ；
2.  ；
3.  ；

<br>

#### 程序化建模 - 光照/定向光源（Lighting）

---

资产特性：

技术栈：

- ；

依赖数据：暂无；

参考资料：

- <>；

工作流：

1.  ；
    - 亮度（Intensity）：亮度；
    - 颜色（Light Color）：主要是冷暖，要考虑天空球以及太阳角度；
    - 阴影细节（Shadow Bias）：待实践；
    - 体积雾（Volumetric Fog）：可以实现夜间的光雾效果；
    - 光束（Light Shaft）：可以实现穿过树林的光束效果；
    - 动态间接光照（Dynamic Indirect Lighting）：在较暗的地方补充光线；
    - 阴影清晰度（Num Dynamic Shadow ）：可以调整阴影形状的清晰度；
2.  ；
3.  ；

<br>

#### 程序化建模 - 全局光照（GI）

---

资产特性：

技术栈：

- Unreal 可直接放置和调节大气光照/天空光源；

依赖数据：暂无；

参考资料：

- <>；

工作流：

1.  ；
2.  ；
3.  ；

<br>

#### 程序化建模 - 云（Cloud）

---

资产特性：

技术栈：

- ；

依赖数据：暂无；

参考资料：

- <>；

工作流：

1.  ；
2.  ；
3.  ；

<br>

#### 程序化建模 - 雾（Frog/Haze）

---

资产特性：

技术栈：

- Unreal 可直接放置高度雾（Frog）和大气雾（Haze）；

依赖数据：暂无：

参考资料：

- <>；

工作流 - 高度雾：

1.  高度：衰减起始值；
2.  浓度（Density）：浓度；
3.  颜色：事实上会影响雾后场景的颜色；
4.  透明度（Opacity）：能穿过雾看多远；

工作流 - 大气雾：

1.  ；
2.  ；
3.  ；

<br>

#### 程序化建模 - 后期效果（PostProcessVolume）

---

资产特性：

技术栈：

- Unreal 的后期盒子可实现景深（Focal）调节；

依赖数据：暂无；

参考资料：

- <>；

工作流：

1.  ；
2.  ；
3.  ；

<br>

#### 程序化建模 - 点云（PointCloud）

---

资产特性：

-

技术栈：

- Unreal 插件 Lidar Point Cloud；
- Houdini 点云导入 Unreal；

依赖数据：暂无；

参考资料：

- <https://www.cnblogs.com/smallhand/p/16351186.html>；
- <https://www.cnblogs.com/smallhand/p/16351186.html>；
- Houdini 点云导入 Unreal：<https://www.bilibili.com/video/BV1Z44y127av>；

工作流：

1.  ；
2.  ；
3.  ；

<br>

#### 网络 3D 资产汇总：

---

- https://hdrihaven.com（HDR）
- https://cc0textures.com（cc0材质）
- http://texturehaven.com（材质02）
- https://www.textures.com（已下载2018合集）
- 贴图素材解析：col(颜色)、disp(置换)、met(金属)、nrm(法线)、rgh(糙度)
- http://gongbao.court.gov.cn/details/4f8bf81a2ed381e52fac05e910846c.html（autodesk收版权费案例）
- https://www.bilibili.com/video/av60380451/?spm_id_from=333.788.videocard.14（动作捕捉）
- https://space.bilibili.com/200451213（天逸地宝一堆教程）
- 【自然景观制作】
- https://www.bilibili.com/video/av67563541/（一个可参照的flowscape效果软件）
- https://www.cgmodel.com/essay-12227.html（CGModel免费资产）
- https://quixel.com（Quixel免费资产）
- https://www.zhihu.com/question/355826682（地形制作软件汇总）
- https://zhuanlan.zhihu.com/p/35787545（Houdini+Unreal）
- https://zhuanlan.zhihu.com/p/56206638（c4d+vue+worldmachine）
- http://dy.163.com/v2/article/detail/EK4CVBOU0516BJGJ.html（Houdini程序化建模）
- https://www.bilibili.com/video/av88181045/?spm_id_from=333.788.videocard.3（blendergis(blender+google earth）
- https://zhuanlan.zhihu.com/p/79388299（Houdini+SD+Unreal）
- https://www42.zippyshare.com/d/uy9aR8JL/50017/ExtPBRCmbEdBL2.8_DownloadPirate.com.part02.rar
- https://www93.zippyshare.com/d/1vNkuvlA/27125/ExtPBRCmbEdBL2.8_DownloadPirate.com.part24.rar

<br>

#### 推荐在 3D 场景中使用的字体

---

很多字体，例如草书，会经常在 3D 场景中出现破面的问题，因此推荐一些

- 华康雅宋（免费商用）；
- 华康龙门石碑（免费商用）；
- 华康布丁（免费商用）；
- 陈继世行楷（版权不明）；
- 鲁迅体（版权不明）；
- 方圆孙中山行书（需授权）；
- 华文新魏（需授权）；
- 华文隶书（需授权）；
- 华文行楷（需授权）；
- 方正毡笔黑（需授权）；
- 方正细珊瑚（需授权）；
- 方正水柱（需授权）；
- 方正水黑（需授权）；
- 方正康体（需授权）；

<br>
