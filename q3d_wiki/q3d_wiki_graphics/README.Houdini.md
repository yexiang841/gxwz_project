## 《元宇宙工作学习笔记》-- Houdini

<br>

#### 介绍

---

- 项目文档记录了笔者工作积累的云原生微服务技术笔记，以及元宇宙、数字孪生、程序化建模工作学习笔记；
- 项目包含一套元宇宙建模方法论和工程，采用 QGIS -> Blender / Houdini -> Unreal / Unity 的方式实现数字孪生虚拟城市；
- 项目包含一个 Unreal 客户端工程和一个 Unity 客户端工程（建设中）；
- 项目包含一个 Cesium 网页端工程，实现 WebGIS BI 展示；
- 项目包含一个 Vue.js / ElementPlus / TS 网页端工程，实现 Admin 管理后台；
- 项目包含一个 Java / SpringBoot 微服务工程和一个 C# / DotNet 微服务工程（建设中），共用网关和服务注册中心（Nacos），可以协同工作；
- 项目包含一个 Publish 部署工程，采用 Docker / Kubernetes / Jenkins 方式进行服务部署；
- 程序语言涉及 C++ / Java / C# / Python / TS / Shell / VEX；
- 错漏之处恳请读者不吝指正，转载望注明出处：<https://gitee.com/yexiang841/q3d_project/>；
- Copyright 叶湘 ( weixin:yexiang841, email:yexiang841@qq.com ), Shitouren Studio. All Rights Reserved.

<br>

#### 元宇宙、数字孪生、程序化建模工作学习笔记

---

- [《元宇宙工作学习笔记》-- 数字孪生虚拟城市建模方法](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_graphics/README.Meta.md)
- [《元宇宙工作学习笔记》-- 图形学基础](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_graphics/README.Graphics.md)
- [《元宇宙工作学习笔记》-- QGIS](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_graphics/README.QGIS.md)
- [《元宇宙工作学习笔记》-- Blender](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_graphics/README.Blender.md)
- [《元宇宙工作学习笔记》-- C4D](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_graphics/README.C4D.md)
- `>>>>` [《元宇宙工作学习笔记》-- Houdini](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_graphics/README.Houdini.md)
- [《元宇宙工作学习笔记》-- SpeedTree](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_graphics/README.SpeedTree.md)
- [《元宇宙工作学习笔记》-- Illustrator](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_graphics/README.Illustrator.md)

<br>

#### 先学点英文

---

spray 喷雾 platonic 理想的 combine 结合 extract 提取 pelt 剥离 pivot 轴心 revolve 车削(循环旋转) loft 放样 rails 轨道? sweep 扫描? creep 爬行蔓延? crease 折痕 ray 射线 topobuild 拓扑建模? carve 切开? refine 提炼? sculpt 雕刻 extrude 挤出 bevel 倒角 split 切割/分离 reduce 减面 expand 扩张 collapse 坍塌 cusp 尖锐/转折点 flip 弹击 clip 回形针/修剪/别附 subdivide 细分 facet 小平面 stroke 敲击 fuse 焊接 snap 折断 twist 扭曲 bend 弯曲 taper 锥化 squash 挤压 lattice 晶格 shatter 破碎 fractal 破碎分形 fracture 破裂 decomposition 分解 ripple 涟漪 morph 图像变换 flattern 平展 unwrap 打开 shade 阴影 rigging 传动装置 capture 捕获 geometry 几何学 blend 混合 delta_mush 一种平滑变形 pose-space-deform 姿势空间变形 pin 大头针/钉住 comb 梳理 tissue 纸巾 biped 两足动物 constraint 约束 lag 延迟 jiggle 轻摇 jitter 紧张/晃动 gamepad 游戏手柄 toggle 切换 fur 毛皮 groom 梳毛? simulate 模拟 isolate 使孤立 advect 水平输送 frizz 卷发 part_hair 分边发型(中分) signed_distance_field 距离场 particle 粒子 dynamics 动态的/动力学的 snapping 吸附 adjust 调整 stitch 缝合 seam 缝接 inspect 检视 ortho 黑白的/灰度的 normals 法线 primitive 原始简单粗糙的 hull 外壳 marker 标记 visualization 形象化 palette 调色板 performance_monitor 性能监视器 caustic 焦散 portal 入口 ambient 周遭环境 stereo 立体声的 switcher 交换机/转换器 collision 碰撞 collider 碰撞体 emitter 发射器 axis 轴 attract 吸引 drag 拖曳 flock 聚集 interact 互相影响/相互作用 detect 检测 instance 实例 sprite 精灵 vortex 旋涡/旋风 vorticle 旋涡的/旋转的 filament 细丝/纤维 grain 颗粒 rigid_body 刚体 rbg_grain 晶粒 upres_sand 高精度沙子 granular 颗粒状 strand 串 slice 分片 fillet 切片 fluid 流体 vellum 牛皮纸 strut 支撑 tetrahedral 四面体的 rigid 坚硬的/刚体的 glued 胶合的 rag 布屑的 convex 凸面的 breakable 易碎的 impacts 影响 debris 碎片/残骸 adjacent 毗邻的 cone 圆锥 slider 滑块 hinge 铰链 spring 弹簧 angular 有棱角的 tank 水槽/池塘 crown 皇冠/冠状 sink 下沉/埋入 drip 滴下 slump 骤降 precipitation 沉淀 condensation 冷凝 suction 吸/吸力 viscous 黏的 clay 黏土 crag 峭壁/岩石块 whitewater 白浪 dissolve 溶解 melt 熔化 lava 火山岩浆 steam 蒸汽 flat 平的 pyro 纵火 populate 移民/殖民 pump 抽吸 fuel 燃料 temperature 温度 explosion 爆炸 flame 火焰 billowy 汹涌的 wispy 细微的 candle 蜡烛 volcano 火山 trail 尾迹 hybrid 混合的 organic 有机的 finite 有限的 highres 高分辨率 initial 初始的 crowd 人群/聚合/群组 clump 一簇簇/丛 obstacle 障碍 stadium 体育场 formation 编队 fuzzy 模糊的/失真的 uniform 统一的/均衡的 shelves 架子 planar 平面的 cast 投掷/投射/计算 composite 复合的/混合的 expression 表现/表达 fade 褪色/平淡/渐消 interpolate 插入/篡改 promote 提升/促进 orient 适应 swap 交换 wrangle 争议 blast 爆炸/冲击 evaluate 评估 foam 泡沫 spectrum 光谱/频谱/范围 peak 山峰/尖顶 unwrap 打开 profile 轮廓/概要 trim 切除/削减 prune 删除/修剪 spline 锯齿 remesh 重置网格 starburst 光芒四射 triangulate 变成三角 wireframe 线框 crop 裁剪 erode 腐蚀/侵蚀 hydro 水电 straiten 束紧/变窄 shrink 收缩 shrinkwrap 收缩包裹 bulge 膨胀 inflate 使充气/膨胀 patch 补丁/斑点/小块地 resample 重新取样 rubber 橡胶 shader 着色器 squab 雏鸟 detangle 理顺 reverse 反转 convert 转换 deform 变形 transition 转变 transport 运输 transfer 转移/传输 intersection 交叉/交叉点 conform 遵照/使一致 solver 解算器 stash 贮藏 tet 测试设备组 warp 歪曲 drape 褶皱

<br>

#### Houdini 简介

---

官网：<https://www.sidefx.com>；

优势：

- 无可替换的解算能力；
- 全节点操作；

教程：

- 基础概念：<https://zhuanlan.zhihu.com/p/78841856>；
- <https://zhuanlan.zhihu.com/p/80298267>；
- 导入 quixel megascan 的资产：<https://www.bilibili.com/video/av78328073>；
- <https://www.bilibili.com/video/BV1Z54y1i7PU>；
- vex 小技巧：<https://andreasbabo93.wixsite.com/sbabovfx/useful-vex-and-expressions>；
- 关键帧操作：<https://jiaocheng.hxsd.com/course/content/9239>；
- <https://www.zcool.com.cn/u/2346925>；
- <https://www.bilibili.com/video/av23667036>；
- <https://www.bilibili.com/video/av85557786>；
- <https://www.bilibili.com/video/av33359267>；
- <https://www.51zxw.net/list.aspx?cid=531#!fenye=2>；
- <http://www.zf3d.com/Products.asp?id=1073>；

案例：

- <https://www.bilibili.com/video/BV1Gt41137Jq/?spm_id_from=333.788.videocard.5（程序化石头）>；
- <https://zhuanlan.zhihu.com/p/89475951（地形制作基础入门）>；
- <https://zhuanlan.zhihu.com/p/68111850（）>；
- <https://zhuanlan.zhihu.com/p/98701539（）>；
- <https://zhuanlan.zhihu.com/p/40596375（）>；
- <https://zhuanlan.zhihu.com/p/108608470（）>；
- <https://www.bilibili.com/video/av58705794/（用houdini制作地形之一）>；
- <https://www.bilibili.com/video/av76618719/（用houdini制作地形之二）>；
- 种田：<https://zhuanlan.zhihu.com/p/75224948>；
- 拱门制作：<https://zhuanlan.zhihu.com/p/77533772>；
- 生命游戏：<https://zhuanlan.zhihu.com/p/54396666>；
- 五星红旗：<https://zhuanlan.zhihu.com/p/85227078>；

资源：

- houdini 资源下载：<https://www.orbolt.com/>；

其它：

- <https://www.zhihu.com/question/324191016/answer/829380407>；

<br>

#### 安装和破解

---

- TODO:Windows 环境安装：；
- TODO:Linux 环境安装：；
- Mac 环境安装：理论上讲，houdini for Mac 的安装破解分为以下步骤：

1.  安装原版镜像：Mac 版甚至不需要断网；
2.  添加环境变量：
    ```
    cd /Applications/Houdini/Houdini18.5.532/Frameworks/Houdini.framework/Versions/Current/Resources/
    source houdini_setup
    ```
3.  检查授权服务：
    ```
    ps aux | grep sesinetd；（应存在：/Library/Preferences/sesi/sesinetd -D -l /Library/Logs/sesinetd.log -V 2）
    md5 /Library/Preferences/sesi/sesinetd
    ```
4.  停掉授权服务：
    ```
    sudo launchctl unload /Library/LaunchDaemons/com.sidefx.sesinetd.plist；
    ```
    或者
    ```
    sudo sesictrl -q（亲测未必有效，按步骤 3 去检查）；
    ```
5.  删除旧授权信息：
    ```
    sudo rm -rf /Library/Preferences/sesi/\*；
    ```
6.  覆盖 sesi 文件：
    ```
    sudo cp ./New/sesinetd /Applications/Houdini/Houdini18.5.532/Frameworks/Houdini.framework/Versions/Current/Resources/houdini/sbin/；
    sudo cp ./New/sesinetd /Library/Preferences/sesi/；
    ```
    - 如果是通过破解程序完成的替换，可以通过 md5 校验文件是否一样：
      ```
      md5 /Library/Preferences/sesi/sesinetd # 跟之前步骤 3 对比应不同，应和如下文件相同
      md5 /Volumes/WhiteBox/\[Q3D\]/\[Q3D\]\[Tool\]/SideFX\ Houdini\ FX\ 18.0.532\ Mac/sesinetd
      ```
7.  打开 macOS 相关权限：
    1. 关闭 sip 服务
       1. 重启按 cmd+R 进入修复模式；
       2. 打开实用工具->终端输入 csrutil disable；
       3. 再重启，终端输入 csrutil status 验证一下 disable；
    2. 打开 sesi 时在系统设置的安全里边同意打开不明开发者（或者 sudo spctl --master-disable）；
8.  重启授权服务：
    ```
    sudo launchctl load /Library/LaunchDaemons/com.sidefx.sesinetd.plist；
    ```
9.  检查授权服务：（同步骤 3）
10. 赋予一些文件夹权限：
    ```
    sudo chown -R aoaoao:staff /Users/aoaoao/Library/Preferences/houdini/
    ls -l /Users/aoaoao/Library/Preferences/houdini/
    ```
11. 查看服务器名：
    ```
    sesictrl -n
    ```
    要注意的是当网络环境变化时，这里查看到的服务名会有变化，服务名变化就需要重新破解授权；
    需要经常变换网络的情况下，多个服务器名可以录入多组秘钥；
12. 启动授权客户端：
    1. （在步骤 2 的前提下）hkey，如果环境变量正确会打开 Lisence Administrator；
    2. 或者直接在应用程序中那你跟打开（Houdini 目录中钥匙符号的那个 app）；
13. 修正服务器地址：
    1. 根据 sesictrl -n 的结果更新服务器地址；
    2. SERVER [ServerName] [ServerCode]；
    3. 在 hkey 运行起来的 Lisence Administrator -> File -> Change Lisence Server 中填入 ServerName；
    4. ServerCode 在下一步使用；
14. 破解：
    1. 运行破解器
    2. 通过菜单选择手动添加 License，然后复制前 5 行进去；
15. 两组秘钥示例：
    - 某些网络下，server 名为 192.168.2.17
    ```
    SERVER 192.168.2.17 dbf63d10 9796E1F9385B9DB022578A2A8146A0C66E3B804F154B7
    LICENSE Generic Houdini-Master 18.5 200 25-dec-2028 *.*.*.* 192.168.2.17 b3f26ab3 e8PB5bz2f+LsNWAh7pHF3bDvbGgr5NraHprCfL6bqD3uDIy4MFjAjSw5R2BM@bGvUWtUiTCHhazuq8
    LICENSE Generic Render 18.5 200 25-dec-2028 *.*.*.* 192.168.2.17 5c05d345 2aOmV4Da2isgMQdVZFlRsPX2YZO5nkzcH7VYIz1F0NYTSfSqY@OeqWPZvlerAb5PpmIbmAN8cG9X00
    LICENSE Generic Karma-Render 18.5 200 25-dec-2028 *.*.*.* 192.168.2.17 ef036173 o+zyRlOSS4sLa6RBfH4mYbJWVDwy84Y5Rdx9n@1285hSbdbtZ+15g+zSW7HknhvqP2D@ddM+gHK1yB
    LICENSE Generic Houdini-Engine 18.5 200 25-dec-2028 *.*.*.* 192.168.2.17 4c6be2ee PdZFmbs1Qj33BqdEIaJAEoKw173enBzBayhbU03V7rbaTmQ7XNJd0spnHH6iMvtMV9K4HCkL3V+s15
    LICENSE Generic PDG 18.5 200 25-dec-2028 *.*.*.* 192.168.2.17 8dd30800 eFzILSCCmd2tEOrqzPyXcdLR@FKinCQJzesXeJ8NyopJ@JWSm7jUeS9u5Lwk7PV1g1M+lMQbyP5OEA
    LICENSE Generic Beta 18.5 200 25-dec-2028 *.*.*.* 192.168.2.17 652c33e3 9Xlt0ufHBAWS3q592X+aSW3RaOW3TnbNXU2V@1Bf1bdx@ZU1ibWUI5f4hsjdKB0TuNtM0ngu6RO6m4
    LICENSE Generic Houdini-Escape 18.5 200 25-dec-2028 *.*.*.* 192.168.2.17 f8236545 25R5R04Gu5xp7BhPEGdV+DmNthOfiQMFSlTzWE2+bmx4Aa5DjsIgW659nvec7ml4TGxeVNQz2Hh9q8
    LICENSE Generic Houdini-Master-Experimental 18.5 200 25-dec-2028 *.*.*.* 192.168.2.17 139c38df bJ3LysPucd1zUkilKDBFFmi4KDFYeAsFG8Ti1n7t1r+I6N8HghQoIKfHf8ricDsZ2sR0A6rgTBLmw3
    ```
    - 某些网络下，server 名为 aoaoaodeiMac.local
    ```
    SERVER aoaoaodeiMac.local dbf63d10 B3A8F64549A260978C4086A7FF70A65FFF21D62A74DF31
    LICENSE Generic Houdini-Master 18.5 200 25-dec-2028 *.*.*.* aoaoaodeiMac.local c5724e58 QcBpKU+Ow9FRkdcnebGoxe6XYxrVbnly@MIXYS72dSk0qHz7JxpaLQUtoaBLPgxJFwAsCsNdrXzlx6
    LICENSE Generic Render 18.5 200 25-dec-2028 *.*.*.* aoaoaodeiMac.local f1d4106a eSBpAN483NKu0F7PPKrw@O4@wXMeGtNVZV@zgAC+pxEMA6eWYr+eNyMvSzequNHHLJwCOgDbf9PtSD
    LICENSE Generic Karma-Render 18.5 200 25-dec-2028 *.*.*.* aoaoaodeiMac.local bfba395b VNEBAjv3VOricT7wfKBH2F1+682HTFoIlysaJ67che8fO8RNb9fqmgSc02pCWpQ1TRcyC+OnyYVZL3
    LICENSE Generic Houdini-Engine 18.5 200 25-dec-2028 *.*.*.* aoaoaodeiMac.local 19ca0b5b SG39iGi@i8rDdBGymNUOXSv0ObVxMxW0lWDS2c0tyE2+Xl2YpB6OQQvDdeLtx4sbAbHodpXWErrksD
    LICENSE Generic PDG 18.5 200 25-dec-2028 *.*.*.* aoaoaodeiMac.local 161f8a15 KTjkUSXf02vqzwGYCOHOyKmK7y1rs2l4@ZbamY6VH2wm8Iu1XTLuLugRVnjS986jIEyF+j1IYCnl06
    LICENSE Generic Beta 18.5 200 25-dec-2028 *.*.*.* aoaoaodeiMac.local bc7f8738 JNYIK4O@7h8adMA1UhkEO7dko2ZOfmJZGeChaO36B6aitdxD3@8JKQ17IW2czxBl3cuHfGWbp+fdOB
    LICENSE Generic Houdini-Escape 18.5 200 25-dec-2028 *.*.*.* aoaoaodeiMac.local bad450c5 @LM8qxCB2OZf76FHYBoRd9wAi7HBdGXsAb4oJh0u3vQOoTBrq3D88uppBaJtmy1ygXrgD3F9SnP+N8
    LICENSE Generic Houdini-Master-Experimental 18.5 200 25-dec-2028 *.*.*.* aoaoaodeiMac.local 22deb1a8 kI1HOBGTAqQ5LEtxCOnb9D@8lTOs599Q0s026xD45u00W3lGACWNO+5Bd@u3vUnQZ2xdXqlLvTtgq3
    ```

<br>

#### 配置

---

首先应该设定 JOB 和 HIPNAME 环境变量

- 在 Edit.Aliases / Variables.Variables 中设定 HIPNAME 和 JOB = HIP

<br>

#### 编辑视图

---

- [视角] 单击滑动 = 旋转，三指滑动 = 移动（需要软件配合），双触摸滑动/双击滑动 = 缩放，ctrl+单击 = 倾斜视图；
- （推荐）要么使用多个屏幕，用空格+b 切换视图独显；
- （不推荐）要么使用一个屏幕，用空格+1 到 5；
- spcae + 5 进入 uv 视图，cmd+b 切换全屏显示；
- 调整显示配置：d；

<br>

#### 节点视图

---

- 居中显示： h；
- 自动排布：l；
- 颜色设置：c；
- 节点检索 ：o；
- 进入/退出节点： i / u；
- 节点连线切断：y；
- 节点向下或者向右排列：a 之后拖向下或者 a 之后拖向右；

<br>

#### 节点视图

---

- 扩充选择（扩散到相邻）：shift + g；

<br>

#### 网络

---

- 名词解释：OP => Operation；
- SOP（Surface）：几何体；
- POP NetWork（Particle）：粒子节点网络，用于发射粒子；
- DOP Network（Dynamics）：用来描述动态模拟的数据，例如刚体，软体、流体等；
- VOP Network（Vex）：通过使用 VEX 操作符构建各种类型的节点的地方，包括 shader 节点；
- ROP Network（Render）：存放渲染相关的节点；
- TOP Network（Task）：构建任务流程的网络，也就是创建 PDG 的地方；
- CHOP Network（Channel）：用来组织 Channel 相关的节点；
- Constraints Network：用来组织几何体的刚体约束；
- COP2 Network（Compositing）：用来处理 2D 图片或者 2D 形式的数据（例如深度图），常配合渲染器使用，类似程序化版 Photoshop。
- Material Network：用来存材质的网络，在 Material Palette 中创建的材质可以在这里看到并修改逻辑；
- SHOP Network（Shader）：老版本（<16）的构建 shader 的网络，已经被官方声明废弃；

<br>

#### 属性、面板参数

---

- 往节点上添加属性，有以下方法（需要选择属性添加的层级：Point / Primary / Detail ）：
  1. 添加一个属性：AttributeCreate；
  2. 添加一个可视化编辑属性：使用 Wrangle，如：v@v = chv(“velocity”)；然后点加号添加可视化参数；
  3. 添加一个属性并使数值随机化：AttributeRandomize；
- 参数面板引用本节点上的属性：
  1. TODO；
  2. 替代方案：增加一个 wrangle 节点，把属性变成 wrangle 节点的参数，然后再引用到当前参数面板；
- 参数面板引用其它节点上的属性：
  1. point/detail/primative(“{节点路径/节点编号}”,”{属性名}”,{数字编号})；（数字编号用于多维向量）
  2. 读取 copy2stamp 产生的属性：stamp(“{copy 节点路径}”,”{属性名}”,{默认值})；
- 参数面板读取“隐藏属性”：
  1. 读取 SOP 的 boundingbox 尺寸：bbox；
  2. 读取 SOP 的 center：centroid；
- 参数面板引用本节点上的参数变量：
  1. chi/chf/chv/chs(“{参数名}”)；根据参数类型有多个版本；
- 参数面板引用其它节点上的参数变量：
  1. chi/chf/chv/chs(“{节点路径}”,“{参数名}”)；根据参数类型有多个版本；
- Wrangle 引用本节点上的属性：
  1. 直接@；
- Wrangle 引用输入节点的属性：
  1. point/detail/primative({端口号},”{属性名}”,{数字编号})；（数字编号用于多维向量）
- Wrangle 引用输入节点的属性：
  1. 读取 copy2stamp 产生的属性：stamp(“{copy 节点路径}”,”{属性名}”,{默认值})；
- Wrangle 中生成并引用面板参数：
  1. ch(“{参数名}”)，然后点右边加号；
- Vop 导入属性：
  1. Bind 节点；
- Vop 中生成并引用面板参数：
  1. 在单个属性上鼠标中键 -> PromoteParameter；
  2. 在整个节点上右键 -> Vex/VopOptions -> CreateInputParameters；
- Vop 导入输出属性：
  1. BindExport 节点；
- 常用全局属性：
  - i@Frame / $F：整数帧；
  - f@Frame / $FF：浮点帧（开启子步幅的话，需要用这个）；
  - @Time / $T：时间（以FPS为1个单位的时间，可取代$FF）；
  - $HIP：工作路径；
  - $HIPNAME：.hip 文件的名字；
- 常用局部属性：
  - i@ptnum / $PT：点 ID；
  - i@numpt / $NPTS：点的数量；
  - i@primnum / $PR：线 ID；
  - i@numprim / $NPRIMS：线的数量；
  - v@Cd / ：颜色（向量，下同）
    . @Cd.r / @Cd.x / $CR：红色；
    . @Cd.g / @Cd.y / $CB：蓝色；
    . @Cd.b / @Cd.z / $CG：绿色；
  - v@v / $V：velocity，加速度；
  - v@N / $N：法线；
  - f@pscale：拉伸；
  - $TX/$TY/$TZ：点的绝对位置；
  - $BBX/$BBY/$BBZ：点在 BoundingBox 中的位置；
- 属性节点：
  - AttributeRandom：对属性进行打乱，可以是完全随机，也可以是范围打乱（比如在一个方向上略微打乱）；

<br>

#### 变量

---

- 全局数值变量：
  - $FF：帧数（浮点数，要配合 PlayBack 中关闭 Integer Frame Values 才有效果）；
  - $FPS：帧率；
  - $E：欧拉数（自然常数，2.71828）；
  - $PI：圆周率（3.14159）；
  - $TSTART：动画第一帧的时间（秒）；
  - $TEND：动画最后一帧的时间（秒）；
  - $TLENGTH：$TEND - $TSTART（动画总时间时间）（秒）；
  - $FSTART：动画时间线的第一帧；
  - $FEND：动画最后一帧；
  - $NFRAMES：$FEND - $FSTART + 1（动画总帧数）；
  - $RSTART：播放栏中的第一帧；
  - $REND：播放栏中的最后一帧；
  - $HIP/$JOB：工程目录；
  1. 在 Edit->AliasesAndVariables->Variables 中可编辑；
     . $HIPNAME：当前场景的名字；
     . $HFS：Houdini 安装目录；
- 局部变量：
  - $PR：面的 ID 编号；
  - $NPR：面的数量；
  - $：；

<br>

#### 建模

---

1.  多边形建模：
    - Color：赋予基本颜色；
    - Transform：基础线性变化；
    - Merge：合并显示；
    - Add：大体有三种类型的作用：
      1.  新增点或线；
      2.  把点连成线、把线合成面；
      3.  删除线，只留点；
    - Resample：重新采样，其中有一个重要属性 curveu，是从 0 到 1 的点属性；
    - PolyBevel：倒角；
    - PolyWire：扫描；
    - PolySplit：环切；
    - PolyExtrude：挤出/挤入；
    - PolyFrame：重新计算法线和切线；
      1.  如果需要法线指向切线方向，可随便定义一个 NormalName，然后将 TangentName 设为 N；
      2.  可在 PolyFrame 之前使用 Reverse 进行方向反转；
    - ExtrudeVolume：挤出规整体；
    - Fuse：焊接；
    - Blast：删除；
    - IsoOffset：体积化；
    - Subdivide：对面上的线进行细分；
    - EdgeFracture：对面进行破碎；
    - SoftTransform：软选择；
2.  布尔：
    - Boolean：
      1. 注意，当布尔对象是复杂多边形时，如果多边形不规则（点面交叉比较多），Boolean 操作并不保险；
      2. 可以把复杂多边形用 VdbFromPolygons 变为 vdb，再变回 polygon，从而形成无交叉的规则多边形，然后再做 Boolean；
      3. 组合物体可以单独连接 Boolean 节点将内部融合；
    - VdbCombine：直接对两个 vdb 进行布尔操作；
3.  体积：
    - Houdini 的体积分为两种：VDB 和 Volume；
    - VDB（SDF）：
      1. 通过 VdbFromPolygon 生成；
      2. 内存效率较高，只在必要位置（体素：Voxels）存储信息（到表面的距离）；
         1. 外部距离：ExteriorBandVoxels；
         2. 内部距离：InteriorBandVoxels；
      3. 视口中显示比较粗糙，可以通过 vdbvisualizetree 节点进行观察；
      4. 在流体碰撞时，碰撞体通常做成 VDB；
      5. 分为 DistanceVDB（如流体）和 FogVDB（如云），撒点方式和材质使用都有区别；
    - Volume；
      1. 通常 IsoOffset 生成；
      2. 每个体素都存数据，较费空间；
      3. 在某些动力学模拟中会需要 Volume；
    - 体积属性传递：AttributeFromVolume；
    - ；
4.  阵列：
    - CopyStamp：在散布点一侧（右侧）：可添加或修改每个点的属性，控制散布之后的变化；
    - CopyToPoint：类似 CopyStamp 复制到点上；
5.  对齐：
    - 放大对齐底部公式：ty1 - (1/2) _ sizey1 = ty2 - (1/2) _ sizey2 + y
      1. y = (1/2) \* (sizey2-sizey1) - (ty2-ty1)
      2. y = (1/2) _ (scaley2-1) _ sizey1 - (scaley2-1) \* ty1
      3. y = (scaley2-1) _ ( (1/2) _ sizey1 - ty1 )
6.  Vop：
    - 程序员还是建议使用 wrangle；
    - CurlNoise：对线条进行扰乱；
    - AntiAliasedNoise：对 P 进行扰乱；
    - RampParameter：对属性进行重映射（作用类似于 Fit 系列，但可以进行颜色匹配）；
    - Random：；
    - TwoWaySwitch：；
    - Compare：；
7.  Wrangle：
    - primpoint()：找出线两端的点 ID；
    - removepoint()：删除点；
    - removeprim()：删除线；
8.  循环：
    - 对每一根线的循环使用 ForEachPrimative；
    - 对每一个点的循环使用 ForEachPoint；
    - CreateMetaImportNode：生成一个循环属性节点，节点会为循环操作提供：
      - iteration：；
      - numiteration：；
      - value：；
      - ivalue：；
9.  地形：
    - <https://zhuanlan.zhihu.com/p/135881819>；
    - <https://zhuanlan.zhihu.com/p/136431110>；
    - 咖喱味教程：<https://www.bilibili.com/video/BV1Rb411L7tH?from=search&seid=6094988674404216853>；
    - 很好的教程：<https://www.bilibili.com/video/BV1DA411x7fL?from=search&seid=29468064484173615>；
    - HDA：<https://www.bilibili.com/video/BV1Go4y1R75j?p=6>；
    - heightfield：生成基础地形；
    - heightfield_noise：对 mask 区域做地形起伏，有多种噪音形态（Noise Type），如果在 Noise Layer 中填 mask，那么就会生成 mask；
    - heightfield_paint：手动生成一个 mask 区域；
    - heightfield_maskblur：让 mask 的边缘平滑过渡；
    - heightfield_drawmask：另一种手动画 mask 的方式，通过多边形节点画 mask；
    - heightfield_remap：对高程的最大最小值进行重新映射（也就是拉伸）；
    - heightfield_project：模型投射到地表，既能做 bool 操作，也能生成 mask；
    - heightfield_maskbyfeature：按地形中的各种条件做 mask，如斜率、高度、尖顶/峡谷（曲率）、方向（法线）、等；
    - heightfield_distort_by_noise：在地形有变化的地方增加噪音（而不是全局噪音）；
    - heightfield_distort_by_layer：对某个 layer 增加噪音；
    - heightfield_resample：重新采样，需要更精细时可以做一下；
    - heightfield_layer：混合多个效果层；
    - heightfield_masknoise：给 mask 添加 noise；
    - heightfield_copylayer：复制一个层到另一个层；
    - heightfield_maskbyobject：通过 object 做 mask；
    - heightfield_visualize：单独显示某一层；
    - heightfield_slump：滑流堆积效果；
    - heightfield_maskclear：清除 mask；
      - 临时使用的 mask 应该清除掉，就好比 attributedelete 一样；
    - heightfield_copylayer：；
    - heightfield_pattern：在地形上增加图案；
      - 手动制作山脉可以使用多个 heightfield_pattern+heightfield_distort_by_noise+heightfield_layer 组合而成；
    - heightfield_terrace：制作梯田；
    - heightfield_slump：制作山体滑坡，通常可以配合 heightfield_maskbyfeature（slope）一起使用；
    - heightfield_output：输出高度图；
    - heightfield_erode：地形腐蚀；
      - erode 是一个时间过程，但是可以选择 Freeze at Frame 来选择当前显示哪一帧的效果；
      - 在经过 erode 之后，地形会多出很多 layer，如基石 bedrock、沉积 sediment、岩屑 debris，可以在 layer 标签下管理；
      - 可以在 visualization 中管理每个层的颜色；
      - 通常可以用 heightfield_layer 混合原地形和 erode 之后的地形，通过 blend 调整腐蚀度；
      - erode 之后加一个 resample 有助于下一步操作；
    - heightfield_scatter：重要的散布节点，参数暴露到 HDA；
      - 第一个参数是地形，第二个参数是 mask，第三个是散布的实体，比如树；
      - 使用 mas 有 3 种散布形态：平均、按密度、固定总数；
      - 不适用 mask 有 1 种散布形态：按点属性；
      - intersection：散布点的交叉属性，用半径 outer redius 和衰减 falloff 来控制；
      - 参考：<https://vimeo.com/351872461>；
    - convertheightfield：将地形转化为 mesh；
      - 转化之后最好加一个 attributedelete，把不必要的属性去掉；
10. 分组：
    - group：直接选择点线面进行编组；
    - group create：根据法线、体积、边长等各种条件，将物体中的点线面构造出分组；
    - group expression：通过 vex 表达式进行分组；
    - group combine：组的布尔；
    - group copy：把一个物体上的组，复制给另一个物体；
    - group paint：通过画笔选择组；
    - group promote：把分组在点线面之间转化；
11. 材质节点：
    - mosaic：将特效转化为 uv 图，参考：<https://www.youtube.com/watch?v=m80SL5kOcwo>；
12. 动画：
    - 设置关键帧：alt+单击；
    - 删除关键帧或者表达式：ctl+shift+单击；
    - Motion FX（运动函数）
      1. wave：正弦波函数；
      2. noise：随机噪声函数；
    - timeshift：设定动画的开始时间；
13. 骨骼：
    - ：；
14. 拍屏：
    - 拍屏可以实现精确的时间播放（如果是在播放条点击的话无法保证时间精确）；
    - 点右上角可以保留拍屏记录；
15. 灯光：
    - 太阳光；
    - 天光；
    - 环境光；
      1. 在环境光中取消 SkyEnvironmentMap.EnableSkyEnvironmentMap 可以关掉默认天光；
      2. 然后就可以在 EnvironmentMap 中选择 HDR 贴图；
16. 渲染
    - 将需要渲染的 Sop 以 Geo 节点（建议命名为 Render_Xxx）导出；
    - 新建太阳光、天光、环境光等；
    - 新建相机；
    - 新建 out.Mantra 节点；
      1. 引入相机；
      2. 修改 Object 为 ForceObject（TODO）；
      3. 设定 Rendering.RenderingEngine 为 PBR；
      4. 调试时可渲染单帧，输出时渲染动画；
      5. 渲染时预览：Preview，调试阶段可提前查看效果；
      6. 噪点水平：NoiseLevel，调试阶段可提高；
      7. 采样精度：Rendering.Sampling.PixelSamples，调试阶段可保持默认值；
      8. 动态采样精度范围：Rendering.Sampling.MinRaySample/MaxRaySample，调试阶段可降低 Max 值；
      9. 穿透性：Rendering.Sampling.StockasticSamples；
    - 运动模糊：
      1. 要在运动的 geo.Render.Sampling.GeometryVelocityBlur 设置为 VelocityBlur 以支持运动模糊；
      2. 打开 out.Mantra.Rendering.AllowMotionBlur；
      3. 需要运动模糊的物体，其 sop 上一定要带着 v 属性，如果没有的话，可添加 Trail 节点生成 v 属性；
    - 如果相机不动，可以打开 SampleLock 选项；
17. 任务：
    - 可以在 TaskList 中添加不同 Task，通过 Include Take 单独调整属性；
18. HDA（HoudiniDigitalAsset）
    - HDA 文件是 Houdini 专用的资产文件，可应用于别的软件。OTL 是旧版本 Houdini 使用的格式；
    - HoudiniEngine：
      1. HoudiniEngine 是其它软件用于加载 HDA（HoudiniDigitalAsset）的插件；
      2. HoudiniEngineForUE4 的源码地址：
         1. 旧版：<https://github.com/sideeffects/HoudiniEngineForUnreal>；
         2. 新版：<https://github.com/sideeffects/HoudiniEngineForUnreal-v2>；
         3. 推荐使用新版；
         4. HoudiniEngine 要求跟 Houdini 版本一一对应，但是对 UE4 的版本要求不高；
         5. 实测 UE4.26/UE4.27 都可以使用 HoudiniEngine2.0.1 去连接 Houdini18.5.532（Mac 系统）；
         6. 但是 UE4.26/UE4.27 虽然能顺利安装 HoudiniEngine2.0.4，却只能连 Houdini18.5.696；
         7. 版本详情在此：<https://github.com/sideeffects/HoudiniEngineForUnreal-v2/tags>；
         8. 如果版本对不上，导入资产就只能显示为一个 3D 的 Houdini 的 LOGO；
         9. UE4 和 HoudiniEngine 版本是否能对上，看最后能不能在 UE4 的菜单栏出现 HoudiniEngine；
         10. HoudiniEngine 和 Houdini 版本是否能对上，看最后 HoudiniEngin 能不能连接到 Houdini；
      3. HoudiniEnginForUE4 图文教程：
         1. <https://blog.csdn.net/qq_31788759/article/details/103467847>；
         2. <https://zhuanlan.zhihu.com/p/35787545>；
      4. HoudiniEnginForUE4 视频教程：
         1. <https://www.bilibili.com/video/BV1Jv411i7uR>；（较详细）
         2. <https://www.bilibili.com/video/BV1YK4y1o797>；（全程操作无解说）
      5. 教程都说把插件拖到引擎源码的 4.xx/Engine/Plugins/Runtime 目录之下；
      6. 实测其实也可以放在工程目录的 Plugins 底下，不影响源码和别的工程，似乎更干净；
    - 创建 HDA:
      1. 节点封装
         1. 要用 Subnet 对 Houdini 节点进行封装；
         2. 在 Subnet 上一级右键 Subnet 然后导出 HDA 资产；
         3. 在弹出的页面中将需要暴露的参数拖入参数列表；
    - 类型声明：<https://www.sidefx.com/docs/unreal/_attributes.html#AttributesInput>；
      1. ；
      2. ；
      3. ；
    - ；

<br>

#### 实操案例 - 流体解算

---

1. [流体] 原理：
   1. SPH：为每个粒子添加一个力，然后计算相互作用；
   2. Volume；
   3. Flip：对场域中的点计算压力值（根据粒子加速度和环境），然后作用到粒子；
      1. 利用 SurfaceVolume 进行辅助解算（?）；
2. [流体] 新建 Null 节点 Flip_Config 作为参数控制器：
   1. 新建浮点参数 ParticleSeperation，设为 0.1（后期通过减小这个值增加粒子数）；
   2. 新建浮点参数 VoxellSize，设为 ParticleSeperation \* FlipSolver.GridSize；
3. [流体] 制作或者导入 Geo 节点 Pyro_Sop；
   1. 通常需要分别制作发射源 Sop、背景 Sop、碰撞体 Sop；
4. [流体] 新建 Geo 节点 Flip_Source 制作流体发射源 Sop；
   1. 可以用多种 Sop 手段进行发射体建模；
      1. 如果需要发射体不停改变形态，可以添加 Moutain 节点，用$T 参数对发射源形态做变化；
      2. 如果需要碰撞体变化，可以用 Motion FX 函数控制，但是要将运动同步到碰撞体配置中；
   2. 发射源连接 FlipSource，新建流体相关属性；
      1. 建立 VDB：引入 Config.VoxelSize；
      2. 建立粒子场：引入 Config.ParticleSeperation；
      3. 勾选 Velocity，调整向量，给粒子场一个初速度；
      4. 如果需要发射源随机，则调整 JitterSeed 控制每一帧源粒子初始位置随机分布，可以设为$T；
   3. 提炼粒子场，有两种方法：
      1. 用 Blast 节点，删除 Surface 组就可以得到粒子场；
      2. 用 Primitive 节点，勾上 AdjustVisualization，将 Volume.DisplayMode 改成 Invisible 可以单独得到粒子场；
   4. 输出；
5. [流体] 新建 Geo 节点 Flip_Collider 制作碰撞体：
   1. 在需要碰撞的物体上添加 CollisionSource；
      1. 引入 Config.VoxelSize；
      2. 勾上 FillInterior，关闭内部（液体碰撞时不会进入内部）；
   2. 输出：分别输出 Polygon 和 Volume；
6. [流体] 新建 DOPNetwork 动力学网络节点 Flip_Dop 作为解算器：
   1. 新建 FlipObject 作为发射源属性配置，连接 FlipSolver 的第一输入端；
      1. 着色：
         1. 改为粒子显示（否则会卡死）：Guides.Particles.Visualization 改为 Particles；
         2. 若需要对运动碰撞体进行 debug，可以选择在 Guides.Visualization 勾上 Collision；
      2. 关闭边界（如果需要封闭液体空间）：勾选 CloseBoundary；
      3. 引入 Config.ParticleSeperation；
      4. 调整 GridSize 控制场密度；
      5. 液体粘性：Physical.Viscosity 调到 100 以上；
   2. 引入外部发射源，有两种方式：
      1. 直接在 FlipObject 中引入：
         1. InitialData.InputType 设置为 ParticleField；
         2. InitialData.SOPPath 指向发射源的输出，不需要默认源；
      2. 从 VolumeSource 引入：
         1. FlipObject 的 InitialData.SOPPath 设置为空，不需要默认源；
         2. 新建 VolumeSource 作为 FlipSolver 的第四输入；
         3. Initialize 改为 SourceFlip；
         4. SopPath 指向发射源的输出；
         5. 通过 Activation 控制发射源是否发射，这一点在动画时常用于打关键帧；
   3. 新建 FlipSolver 作为解算器：
      1. 修改 VolumeMotion.VolumeLimits.BoxSize（解算边界）；
      2. 如果显卡好的话，记得开启 Solver.MaxCellsToExTrapolate.UseOpenCL；
      3. 为了解决【速度小的区域粒子堆积浪费计算量】的情况，应开启 ParticleMotion.Seperation.ApplyParticleSeperation；
      4. 为了解决【速度大的区域出现粒子不足】的情况，待定；
      5. 开启 VolumeMotion.Collision.StickOnCollision，让液体碰撞时产生摩擦阻力；
      6. 开启子步幅（Substeps）可以带来更好的模拟效果，也可以改善体积感等小问题，对于高速运动的流体尤其合适；
      7. 可以选择修改 VolumeMotion.VelocityTransfer：
         1. SplashyKernel 较激进；
            1. 将 VelocitySmoothing 调大也可以达到黏性和顺滑的效果；
         2. SwirlyKernel 较顺滑；
      8. 液体粘性：
         1. 勾选 VolumeMotion.Viscosity 和 ViscosityByAttribute；
         2. 提高 VolumeMotion.ViscosityScale，此系数与 FlipObject.Physical.Viscosity 的关系，由 VolumeMotion.MaxMethod 确定，常选 Copy/Multiple；
         3. 为简单起见，这里可不勾选 ViscosityByAttribute，直接由 FlipObject 确定；
      9. 设置发射源头和粒子出口：
         1. 原理：
            1. 可以用 VolumeSource 简单解决源头出口问题，但会是一个从空到填充的过程；
            2. 如果需要从第一帧就填充空间，并且一头不停输入，一头不停输出，则需要在设置空间流体的同时，在源头和出口都填充带 velocity 属性的粒子场；
            3. 如果用 VolumeSource 来做这件事情，就会出现叠加，并且两端的动力场不统一；
            4. FlipSolver 中的 BoundaryLayer 就是设计用替换来解决这个问题；
         2. 在 FlipSolver 中开启边界层：
            1. 开启 VolumeMotion.VolumeLimit.UseBoundaryLayer；
            2. 开启 VolumeMotion.VolumeLimit.ApplyBoundaryVelocities；
            3. 给源头和出口留出空间（设置 LowerPadding 和 UpperPadding）；
         3. 在发射源 Flip_Source 中设置源头和出口：
            1. 用 Box 引入 FlipSolver 的 BoxSize 和 BoxCenter；
            2. 用 Bound 引入 FlipSolver 的 Boundary 的 LowerPadding 和 UpperPadding（取负数）；
            3. 用 VdbFromPolygon 将这个段变成 VDB，然后用原来的源减去这一段，从而得到源头和出口 VDB；
            4. VDB 转为 Polygon 然后加 FlipSource 输出作为 BoundarySurfaceVolume；
            5. VDB 转为 Polygon 之后加 PointFromVolume，然后加 Velocity 再输出作为 BoundaryVelocityVolume；
               1. PointFromVolume 中的 ParticleSeperation 可以单独控制；
               2. 因为后面要将点变成 Volume，所以需要打开 AddScaleAttribute，相当于添加 pscale 属性；
               3. 用 AttributeCreate 或者 AttributeWrangle 添加 Velocity 属性，并从 FlipSource 节点中引入 Velocity；
               4. 用 VolumeRasterizeAttribute 将粒子变为 Volume（使用 v 属性）；
   4. 新建 StaticObject 设置碰撞体；
      1. 设置碰撞体 POLY：
         1. SopPath 引入碰撞体 Flip_Collider 的 Polygon 输出；
         2. 在外部物体都隐藏的情况下，DisplayGeometry 可以显示出 POLY；
      2. 设置碰撞体 VDB：
         1. Collision.RDBSolver.Volume.Mode 改为 VolumeSample；
         2. Collision.RDBSolver.Volume.ProxyVolume 引入碰撞体 Pyro_Collider 的 VDB 输出；
         3. 引入 Flip_Config.DivisionSize 给 Collision.RDBSolver.Volume.DivisionSize，控制碰撞体精度；
         4. 若开启 Collision.RDBSolver.Volume.CollisionGuide，则会显示碰撞 VDB；
      3. 新建 Merge 将被 StaticObject 和 FlipSolver 组合在一起；
         1. 需要调整 AffectorRelationship 调整为 Mutual（互相作用模式）；
   5. 新建 Gravity 节点为结算器添加重力：
      1. 如果没有碰撞体，可以接在 FlipSolver 上；
      2. 如果有碰撞体，可以接在 Merge 节点上；
7. [流体] 新建 Geo 节点 Flip_Cache 作为缓存输出：
   1. 新建 DOPImportField 作为导出节点；
      1. 分别引入 DOPNetwork 和 FlipObject 节点；
      2. 预设选 FlipFluid，会自动生成三种导出：
         1. Geometry - 粒子 sop；
         2. surface - 用于辅助液体压缩；
         3. vel - 速度场，用于制作运动模糊；
   2. 新建 AttributeDelete 节点删除除了 id 之外的不必要属性（\* ^id）；
   3. 新建 GroupDelete 节点删除不必要的组（\*）；
   4. 新建 FluidCompress 做液体压缩（流体必备环节 TODO）
      1. ParticalSeperation 可以从 FlipObject 中引用；
      2. Cull Bandwidth 指的是距离 surface 多少个 voxel 之内的粒子会被保留；
   5. 新建 FileCache 节点做缓存，勾上 LoadFromDisk，尝试 SaveToDisk；
   6. 新建 ParticleFluidSurface 对缓存进行解压还原显示：
      1. 还原粒子：
         1. 将 ConvertTo 设置为 Particles；
         2. 将 Visualize 设置为 Velocity；
         3. 注意此时还原出来的是 surface 附近的粒子，其余部分后面将用多边形填充；
      2. 还原 surface：
         1. 引入 FlipObject 中的 ParticleSeperation 参数；
         2. 打开 Regions.BoundingBox.UseBoundingBox 封闭边界；
         3. 引入 FlipSolver 的 VolumeMotion.VolumeLimit.BoxSize 和 BoxCenter 作为边界；
         4. 勾上 Regions.BoundingBox.ClosedBoundaries；
         5. 勾上 Regions.Collision.SubtractCollisionVolumes，用以减去碰撞体中的体积，注意，这里要在第二输入引入碰撞体；
         6. 打开 Filter 中的 Dilate、Smooth、Erode，消除表面零碎的破洞；
      3. 区分源：如果 FlipSolver 的 VolumeSource 有多个（用 Merge 组合在一起），那么在此可以通过 Blast 删除分组将两者区别输出；
      4. 再次缓存：还原表面之后可以再次进行 FileCache 的缓存；
      5. 将 Flip_Cache 分为内部和外部，分别以 Geo 节点导出（命名为 Flip_Render_Water_Interior/Exterior）；
8. [流体] 材质：
   1. 外部（Render_Flilp_Exterior 节点）使用 BasicLiquid 材质；
      1. 较低的 diffuse；
      2. 较高的反射；
      3. 较高的折射；
   2. 外部（Render_Flilp_Interior 节点）使用 UniformVolume 材质；
      1. 修改颜色；
   3. 新建其它物体材质，指定给 Render\_其它节点；
9. [流体] 渲染：略；

<br>

#### 实操案例 - 火焰解算

---

1. [火焰] 原理：TODO；
2. [火焰] 新建 Null 节点 Pyro_Config 作为参数控制器：
   1. 建立参数：NoiseFrequency、NoiseThreshold、ParticleSeperation（粒子间距）、VoxelSize（体积间距）；
   2. 新建 box 节点，通过 bbox 和 centroid 函数获取发射源输出节点的 size 和 center，通过 transform 节点进行一定调整，然后作为参数输出给发射源属性配置节点；
3. [火焰] 制作或者导入 Geo 节点 Pyro_Sop；
   1. 通常需要分别制作发射源 Sop、背景 Sop、碰撞体 Sop；
4. [火焰] 新建 Geo 节点 Pyro_Source 制作燃烧发射源 Sop；
   1. 将燃烧发射源破碎化
      1. 方法一：手动随机删除（不推荐）；
      2. 方法二：按一定频率（破碎尺寸）添加噪波到颜色，设置阈值之后删除点以使面破碎；
   2. 发射源连接 PyroSource，新建燃烧相关属性；
      1. 注意目前 PyroSource 还有一个 bug：需要切换并切回一次 Initialize 才能将参数选项显示出来；
      2. 可删掉 density 参数，Pyro 解算并不需要，只需要 fuel 和 temperature；
      3. Mode 可以选择保持输入、表面撒点或者体积撒点，此例选择表面撒点；
   3. 添加 AttributeNoise，扰乱 fuel 和 temperature 属性；
      1. 勾上 Animated，使火焰强度和位置随机；
      2. 可以在属性中点击查看着色之后的属性变化效果；
      3. 通过 Remap 属性调整 Noise 的偏移值大小；
      4. 通过 Distribution 调整分布函数；
   4. 添加 PointVelocity 节点，添加 v 属性作为火焰的发射方向；
      1. 显示速度属性方法一：在界面上打开显示尾迹（PointTrail）；
      2. 显示速度属性方法二：在 DisplayOption.Visualize.SceneVisualizers 中添加 v 属性的 Marker；
      3. 火焰一般向上燃烧，所以将 Initialization 设置为 SetToValue，value 值设为 0,1,0（y 值可调）
      4. CurlNoise 和 Animated 都打开，使火焰喷射方向随机；
         1. Scale 调整扰乱强度；
         2. SwirlSize 调整扰乱的粒度；
         3. Turbulence 调整扰乱层级；
   5. 用 VolumeRasterizeAttribute 将粒子变为 Volume（使用 fuel、temperature、Alpha、v 属性）；
      1. VoxelSize 用于控制体积雾精细度；
5. [火焰] 新建 Geo 节点 Pyro_Collider 制作碰撞体：（TODO）
6. [火焰] 新建 DOPNetwork 动力学网络节点 Pyro_Dop 作为解算器：
   1. 新建 SmokeObject 作为发射源属性配置，连接 PyroSolver 的第一输入端；
      1. 流体框大小：将 Config 中的 Size 和 Center 数据引入 SmokeObject 的 Properties.Size 和 Center；
      2. 着色：
         1. 可选 1：在 Guides.Particles.Visualization 中直接勾选 Density、Temperature、Fuel 等；
         2. 可选 2（推荐）：在 Guides.Particles.Visualization 中只勾选 MultiField；
            1. 勾选 Multi.BoundingBox 和 BoundingBoxHash（TODO）；
            2. 设定 Multi.Smoke.DensityField 为 Density；
            3. 设定 Multi.Emission.EmissionField 为 Temperature，EmissionScale 设为 8；
            4. 设定 Multi.Emission.EmissionColorField 为 heat；
            5. Emission.Mode 选择任意一种 Preset 比如 BlackBody；
            6. 可以调整颜色点，也可以调整属性映射值，比如讲 EmitRange 调整为 0~2；
      3. 精度：设定 DivisionSize 为 0.01，电影级的精度建议 0.001；
   2. 引入外部发射源，有两种方式：
      1. 直接在 SmokeObject 中引入：
         1. 在 InitialData 中指定 Density、Temperature、Fuel、Velocity 的外部 SOPPath 路径；
      2. 从 VolumeSource 引入：
         1. SmokeObject 的 InitialData 全都设置为空；
         2. 新建 VolumeSource 作为 FlipSolver 的第五输入端；
         3. Initialize 改为 SourceFuel；
         4. SopPath 指向发射源的输出；
         5. 可调整各个属性的 Scale 以缩放着色比例；
   3. 解算边界：
      1. 新建 GasResizeFluidDynamic 作为 PyroSolver 的第二输入端；
   4. 新建 PyroSolver 作为解算器：
      1. 温度扩散：Simulator.TemperatureDiffuse，越小越容易扩散，建议 0.08；
      2. 冷却率：Simulator.CoolingRate，越大冷却越快，从而让火焰边缘快速冷却收缩，建议 0.8~0.9；
      3. 浮力：Simulator.BuoyancyLift，越大火焰上升越快，建议 1~2；
      4. 燃烧率：Combustion.BurnRate，越大烧得越旺，建议 0.8；
      5. 燃料利用率：Combustion.FuelInefficiency，燃料被消耗的比例，建议 0.2（燃料会剩余 20%）；
      6. 火焰膨胀率（向四周爆燃的效果）：Combustion.GasRelease，小火焰建议 2，爆炸建议 10 以上；
      7. 火焰维持的高度：Combustion.Flames.FlameHeight，越大越高，是个缩放值，小火苗建议 1 以下；
      8. 火焰小扰乱：Shape.Disturbance，越大扰乱越激烈，建议 0.5；
         1. 小扰乱的粒度：Shape.Disturbance.DIsturbance.BlockSize，根据火焰整体尺寸调整；
      9. 火焰大扰乱：Shape.Turbulence，越大扰乱越激烈，建议 0.1；
         1. 大扰乱的粒度：Shape.Turbulence.Turbulence.SwirlSize，根据火焰整体尺寸调整；
         2. 大扰乱的迭代次数：Shape.Turbulence.Turbulence.Turbulence，越多越乱；
      10. 锐利度：Shape.Sharpening，越大约锐利，建议 0.2；
      11. 火焰涡流细节：Shape.Confinement，越大细节越多，建议 0.2；
      12. 开启子步幅（Substeps）可以带来更好的模拟效果，也可以改善体积感等小问题，火焰渲染建议开到 2~3；
   5. 新建 StaticObject 设置碰撞体；
      1. 设置碰撞体 POLY：
         1. SopPath 引入碰撞体 Pyro_Collider 的 Polygon 输出；
         2. 在外部物体都隐藏的情况下，DisplayGeometry 可以显示出 POLY；
      2. 设置碰撞体 VDB：
         1. Collision.RDBSolver.Volume.Mode 改为 VolumeSample；
         2. Collision.RDBSolver.Volume.ProxyVolume 引入碰撞体 Pyro_Collider 的 VDB 输出；
         3. 若开启 Collision.RDBSolver.Volume.CollisionGuide，则会显示碰撞 VDB；
      3. 新建 Merge 将被碰撞对象和 PyroSolver 组合在一起；
         1. 需要调整 AffectorRelationship 调整为 Mutual（互相作用模式）；
7. [火焰] 新建 Geo 节点 Pyro_Cache 作为缓存输出：
   1. 新建 DopI/O 作为输出节点；
      1. 分别引入 DOPNetwork 和 SmokeObject 节点：
      2. 预设选 Pyro，会自动添加很多属性，主要使用 density，temperature，heat，fuel；
      3. DopI/O 对属性和组的操作方法是按需导入，所以不必要进行属性和组删除；
   2. 新建 FileCache 节点做缓存，勾上 LoadFromDisk，尝试 SaveToDisk；
   3. 新建 VolumeVisualization 对缓存进行解压还原着色显示（基本上设定为跟 Pyro_Dop.SmokeObject 一致）：
      1. 设定 Smoke.DensityField 为 Density；
      2. 设定 Emission.EmissionField 为 Temperature，EmisionScale 设为 8；
      3. 设定 Emission.EmissionColorField 为 heat；
      4. Emission.Mode 选择任意一种 Preset 比如 BlackBody；
      5. 可以调整颜色点，也可以调整属性映射值，比如讲 EmitRange 调整为 0~2；
8. [火焰] 材质：
   1. 新建 mat.Flame 材质，指定给 Render_Fire 节点；
   2. 新建其它物体材质，指定给 Render\_其它节点；
9. [火焰] 渲染：略；

<br>

#### 实操案例 - 粒子解算（传送门案例）

---

1. 原理：TODO；
2. 新建 Null 节点 Particles_Config 作为参数控制器：
   1. Particles_Num：细分粒子数；
   2. N_Noise_TanG：发射源沿着切线方向的最大扰动角度；
   3. Sub_Step：解算步幅（将决定最后粒子连线的长度）；
3. 建立发射源模型：
   1. 用 Circle 节点建立圆圈；
   2. 添加 Resample 进行点细分；
      1. 分 1000 个点；
      2. 同时打开 TangentAttribute 得到切线属性 tangentu，把名字修改为 N 可以直接赋予法线；
   3. 将法线沿切线方向扰乱（随时间）：
      1. ；
   4. 将法线长度进行扰乱（随时间）：
      1. ；
   5. 将法线沿 z 轴方向扰乱（随时间）：
      1. ；
4. 新建 DopNetwork 动力学网络节点 Particles_Dop 作为解算器：
   1. 从 Particles_Config 中引入子步幅值 Sub_Step（设置在整个 DopNetwork 上）；
   2. 新建 PopObject 作为发射源属性配置，连接 PopSolver 的第一输入端；
      1. 着色；
      2. 碰撞；
   3. 引入外部发射源，有两种方式：
      1. 直接在 PopObject 中引入：
         1. 在 InitialGeometry 中指定 Sop；
      2. 从 PopSource 引入：
         1. PopObject 的 InitialGeometry 设置为空；
         2. 新建 PopSource 作为 PopSolver 的第三输入端；
         3. Source.Sop 指向发射源 Sop；
         4. 发射数量有两种控制方式：
            1. 固定频率发射粒子：Birth.Activation/Count；
            2. 数量固定，时间随机：Birth.Const/BirthRate；
         5. 粒子生存时间/噪波：Birth.LifeExpectancy/Variance；
         6. 可以在此处用 Merge 组合多种发射源，营造更好效果；
   4. 新建 PopWind 作为风场：
      1. 如果需要风，就调节 Activation 为 1；
      2. 修改 WindVelocity 以调节风向；
      3. 如果需要空气阻力，就调节 AirResistance；
      4. 调节 Noise.Amplitude 等，以调节风力扰动；
   5. 新建 PopSolver 作为解算器；
   6. 新建 Gravity 为粒子添加重力；
   7. 新建 StaticObject 设置碰撞体；
      1. SopPath 引入碰撞体 Particles_Sop_Ground 的输出；
      2. 新建 Merge 将被碰撞对象和 Gravity 组合在一起；
         1. 需要调整 AffectorRelationship 调整为左影响右；
5. 新建 Geo 节点 Particles_Cache 作为缓存输出：
   1. 导出粒子，有两种方式：
      1. 新建 DOPImport 作为导出节点；
         1. 分别引入 DOPNetwork 和 PopObject 节点；
         2. 新建 AttributeDelete 节点删除除了 id 之外的不必要属性（\* ^id）；
         3. 新建 GroupDelete 节点删除不必要的组；
      2. 新建 DopI/O 作为导出节点；
         1. DopI/O 对属性和组的操作方法是按需导入，所以不必要进行属性和组删除；
   2. 新建 FileCache 节点做缓存：
      1. 从 Particles_Config 中引入子步幅值 Sub_Step；
      2. 开启了子步幅的话，浮点帧也应该开启，需要将$F改为$FF，才能够写入浮点帧；
      3. 勾上 LoadFromDisk，尝试 SaveToDisk；
   3. 对粒子进行差时连线，有三种方法：
      1. 使用 Trail 节点；
      2. 用多个 TimeShift 分别 shift 不同的时间，然后 Merge 起来，然后用 Add 节点将相同 id 的点连接成线；
         1. 因为开启了子步幅，所以注意把 IntegerFrames 勾掉；
      3. 新建 ForLoopWithFeedBack 对方法二进行程序化改造：
         1. 这个节点跟 ForEach 的区别是可以对每次循环的结果进行保留；
         2. 循环中接入 TimeShift，
      4. 再次缓存：还原表面之后可以再次进行 FileCache 的缓存；
6. 材质：mat.Glow 材质
7. 渲染：略；

<br>

#### 实操案例 - 粒子解算（模型沙化案例）

---

1. 新建 Null 节点 Sand_Config 作为参数控制器：
   1. ；
2. 制作沙化模型：
   1. 创建一个 Test 系列的模型；
   2. 沙化：可以使用 IsoOffset+Scatter 撒点，但这里建议使用 VellumConstraints_Grain 节点；
3. 把整个模型拆分为一块一块：
   1. 创建 PointVop，使用 Voronoise 节点，设定好 Frequency，就能把所有点分成不同块，从 seed 中输出一个 0~1 之间的值；
   2. 如果想要看到这个值的效果，可以把 seed 继续接入 Random 节点（1D 输入 3D 输出），连入 GeometryOutput.Cd 属性就能显示出来；
   3. 这个值通过另一个 Random 节点（1D 输入 3D 输出），可以输出一个 Cluster_Class 属性，可以用来识别块；
4. 选取一半的块连接成簇：
   1. Cluster_Class 还可以加一个 Compare 和 TwoWaySwitch 切换为 1.0 和 0.0，用另一个 Cluster_AB 属性输出；
   2. 使用 ConnectAdjacentPieces 节点将所有点与相邻点连线（相邻 5 个左右就好）；
   3. 使用 Blast，根据 Cluster_AB 将一半的 Point 删去，再反转删除，用 Merge 合并；
   4. 使用 PointWrangle，判断每一条线，如果两端的 Cluster_Class 属性不同，就删去这条线；
5. 创建约束和沙化属性传递：
   1. 新建一个属性传递平面 Grid，用 Moutain 将平面增加噪音；
   2. 使用 AttributeTransfer 节点用作属性传递（从右边传到左边），两边都用 AttributeCreate 或者 Wrangle 生成同一个属性 Active；
   3. 使用
6. 新建 DopNetwork 动力学网络节点 Sand_Dop 作为解算器：
   1. 新建 VellumObject 引入；
   2. 新建 VellumSolver；
      1. 修改 Advanced.GrainCollisions.RepulsionWeight 为 0，以使粒子间不排斥，沙子就能堆积（否则就会像沙子般弹开）；
      2.
   3. 新建 Gravity；
   4. 新建 StaticObject；
      1. 提升 Physical.Friction（静态摩擦力）和 Physical.DynamicFrictionScale（滑动摩擦力）到 4 左右，以使沙子带粘性，就如沾了水一样；
      2.
7. 材质：mat.Cloud 材质；
8. 渲染：略；

<br>

#### 实操案例 - 布料解算（旗杆+细绳+红旗案例）

---

1. 新建 Null 节点 Particles_Config 作为参数控制器：
   1. Resample_Lenth：控制细绳分段长度，建议 0.04；
2. 分别制作模型：
   1. 用 Tube 节点创建旗杆，选 Polygon 类型，分段加到 100；
   2. 用 Grid 节点创建红旗；
      1. 为了使碎片不均匀使用 Remesh 节点重新对面片进行排布；
         1. 调整 Meshing.Iterations 为 1（越小越不规则）；
         2. 调整 ElementSizing.TargetSize 为合适的值（碎片大小）；
      2. 使用 EdgeFracture 对平面进行破碎处理（之后再用约束粘起来）；
         1. 勾上 ShowGuideGeometry 才能看到分裂线；
         2. 调整 InitialPieces 得到合适的碎片数量；
   3. 用 Curve 节点画线，分为主干线和分支线
      1. 用 Resample 节点进行细分；
      2. 为了让曲线像细绳，TreatPolygonAs 选用 SubdivisionCurves；
3. 制作约束：
   1. 新建 Geo 节点，分别引入以上 Sop，每个 Sop 建立自己的点分组；
   2. VellumConstraits 基本使用：
      1. 被约束物连左边，约束物连右边；
      2. ConstraitType：
         1. DistanceAloneEdges：；
         2. BendAcrossTriangles：；
         3. Cloth：布料；
         4. Hair：毛发；
         5. String：绳；
         6. PinToTarget：图钉；
         7. AttachToGeometry：吸附到几何体；
         8. StitchPoints：；
         9. WeldPoints：点吸附（做撕裂用）；
         10. Glue：胶水粘贴；
   3. 新建 VellumConstraits 连接粗绳和旗杆；
      1. 选择正确的点组使用 ConstraitsType.AttachToGeometry 约束；
      2. 勾上 ClosestPoints.MaxDistance，让约束只在一定距离内产生（如连接处）；
   4. 新建 VellumConstraits 连接细绳和粗绳；
      1. 选择正确的点组使用 ConstraitsType.Glue 约束；
      2. 调整 GlueSearch 中的 MaxSearchPoints 和 ConstaitsPerPoint 让每个点与周围更多点连接；
      3. 调整 GlueSearch 中的 MaxSearchDist 让每个点能搜索到更远的点；
   5. 新建 VellumConstraits 连接旗和细绳；
      1. 同上；
   6. 定义约束材质类型：
      1. 同样是使用 VellumConstraits 节点；
      2. 新建 VellumConstraits 定义旗面，对旗的点组使用 ConstraitsType.Cloth 约束；
      3. 新建 VellumConstraits 定义绳子，对绳的点组使用 ConstraitsType.String 约束，将 Stretch.Stiffness 调大（让绳子拉硬度变大）；
   7. 新建 VellumConstraits 缝合裂开的旗面；
      1. 选择正确的点组使用 ConstraitsType.WeldPoints 约束；
      2. 这种约束会在点层级生成 weld 属性，没有缝合的值为-1，断开但缝合上的会是一个正整数，后面会用此属性进行撕裂；
   8. 新建 DopNetwork 动力学网络节点 Cloth_Dop 作为解算器：
      1. 从 Cloth_Config 中引入子步幅值 Sub_Step（设置在整个 DopNetwork 上）；
      2. 新建 VellumObject 作为发射源属性配置，连接 PopSolver 的第一输入端；
         1. 着色；
         2. 碰撞；
      3. 引入外部发射源，有两种方式：
         1. 直接在 PopObject 中引入：
            1. 在 InitialGeometry 中指定 Sop；
         2. 从 PopSource 引入：
            1. PopObject 的 InitialGeometry 设置为空；
            2. 新建 PopSource 作为 PopSolver 的第三输入端；
            3. Source.Sop 指向发射源 Sop；
            4. 发射数量有两种控制方式：
               1. 固定频率发射粒子：Birth.Activation/Count；
               2. 数量固定，时间随机：Birth.Const/BirthRate；
            5. 粒子生存时间/噪波：Birth.LifeExpectancy/Variance；
            6. 可以在此处用 Merge 组合多种发射源，营造更好效果；
      4. 新建 PopWind 作为风场：
         1. 如果需要风，就调节 Activation 为 1；
         2. 修改 WindVelocity 以调节风向；
         3. 如果需要空气阻力，就调节 AirResistance；
         4. 调节 Noise.Amplitude 等，以调节风力扰动；
      5. 新建 PopSolver 作为解算器；
         1. 需要特别注意：对于布料绳子模拟，如果不加大 Iterations.Substeps 的话（至少 3 以上），经常会得不到正确结算；
      6. 新建 Gravity 为粒子添加重力；
      7. 关于碰撞体：因为 VellumContraits 的第三输入端有碰撞体设置，所以的 Dopnet 中并不必需设定 StaticObject；
   9. 新建 Geo 节点 Particles_Cache 作为缓存输出：
      1. 新建 DOPImport 作为导出节点；
         1. 分别引入 DOPNetwork 和 VellumObject 节点；
         2. ImportStype 设定为 FetchGeometryFromDOPNetwork；
      2. 使用 VellumPostProcess 对布料模拟进行优化：
         1. 第一输入引入 DOPImport 节点，第二输入引入 Cloth_Constraits 输出的 OUT_Constraits 节点；
         2. 调节 Operations.Smooth.Subdivision 为 Catmull-Clark，SubdivisionDepth 为 1 或者 2 都能看到更优化的布料模拟；
      3. 新建 AttributeDelete 节点删除除了 id 之外的不必要属性（\* ^id）；
      4. 新建 GroupDelete 节点删除不必要的组（\*）；
      5. 新建 FileCache 节点做缓存：
         1. 从 Cloth_Config 中引入子步幅值 Sub_Step；
         2. 开启了子步幅的话，浮点帧也应该开启，需要将$F改为$FF，才能够写入浮点帧；
         3. 勾上 LoadFromDisk，尝试 SaveToDisk；
   10. 新建 Geo 节点 Cloth_Sop_DragPoint 作为拉扯点：
       1. 用 Add 节点建立点，用 Transfer 节点调整到合适的相对位置；
       2. 对 Transfer 节点 K 帧，制作靠近动画；
   11. 在 Cloth_Dop 中新建 PopWrangle 作为 Solver 的第三个输入（每一步结算后被调用）；
       1. 在 PopWrangle 中设定第一个 Input 为 DragPoint 输出的 Sop；
       2. 在 Code 中实现算法：
          1. 计算 DragPoint 的位置向量；
          2. 取旗面组对@P 和 DragPoint 相减获得距离向量；
          3. 计算距离向量的长度（也就是旗面每个点到拉扯点的距离）；
          4. 设定某个阈值，当距离小于某个值时，让 weld 属性变成-1，即可实现断裂；

<br>

#### 实操案例 - 闪电制作

---

1. 新建 Null 节点 Lightening_Config 作为参数控制器：
   1. ；
2. 制作主干：
   1. 新建直线作为主干；
   2. 添加 Resample 进行点细分；
      1. 分 20 个点；
      2. 同时打开 CurveUAttribute 得到递增属性 curveu（有必要时可用 Reverse 倒置 curveu 属性）；
   3. 制作前小后大的弯折效果；
      1. 添加 Wrangle 或者 Vop，将 P 加上 CurlNoise 属性，以实现弯折；
      2. 为了实现前小后大，CurlNoise 属性再接入一个振幅 amp，值为 curveu 乘以常数；
      3. 为了实现时间随机，CurlNoise 属性再接入一个偏移 offset，值为 Time；
   4. 制作前大后小的粗细效果；
      1. 添加 Wrangle 或者 Vop，将 pscale 乘以 curveu 属性，并额外以 Trans_pscale 输出，以备传递给分支；
   5. 制作前实后透的透明效果；
      1. 添加 Wrangle 或者 Vop，将 Alpha 乘以 curveu 属性，并额外以 Trans_Alpha 输出，以备传递给分支；
3. 制作分支：
   1. 设定好主干上的分支生长点位置及法线：
      1. 添加 PolyFrame 重建法线方向；
         1. NormalName 随便设定一个替代变量；
         2. TangentName 设定为 N；
      2. 通过 Delete 节点中的表达式删除靠近开端的点（比如@curve < 0.4）；
      3. 通过 ForEache 循环删除一些生长点，留下另一些；
         1. 可以接入 iteration 作为参数，为每个点建立编号，然后建立随机数，再删去一半；
   2. 新建直线作为分支；
   3. 使用 CopyStamp 将直线复制到主干的点上；
      1. 从主干传递 Trans_pscale 和 Trans_Alpha 属性给分支；
   4. 制作前小后大的弯折效果；
      1. 添加 Wrangle 或者 Vop，将 P 加上 CurlNoise 属性，以实现弯折；
      2. 为了实现前小后大，CurlNoise 再接入一个振幅 amp 属性，值为 curveu 乘以常数；
      3. 为了实现时间随机，CurlNoise 再接入一个偏移 offset 属性，值为 Time；
   5. 制作前大后小的粗细效果（并且开端对应主干的 pscale）；
      1. 添加 Wrangle 或者 Vop，将传递过来的 Trans_pscale 乘以 curveu 属性，输出到 pscale 属性；
   6. 制作前实后透的透明效果（并且开端对应主干的 Alpha）；
      1. 添加 Wrangle 或者 Vop，将传递过来的 Trans_Alpha 乘以 curveu 属性，输出到 Alpha 属性；
4. 材质：mat.Glow 材质；
5. 渲染：略；

<br>

#### 实操案例 - 云雾解算

---

1. 新建 Null 节点 Cloud_Config 作为参数控制器：
   1. ；
2. 制作分布区域：
   1. 制作多边形覆盖想要有云的区域，使用 Polygon，加大 Frequency；
   2. 添加扰乱：
      1. 可以用 Moutain 节点；
      2. 也可以用 PointVop 添加 aanoise 到 P 属性；
   3. 如果精度不够，用 subdivide 进行细分；
3. 撒点；
   1. 用 VdbFromPolygon 转化为 VDB，用 Density 类型而不是表面类型，以进行内部撒点；
   2. 用 Scatter 撒点，撒点数引用自 Config 节点；
   3. 用 PointWrangle 对点的@pscale 做一个 Rand(@ptnum)随机操作；
   4. 用 AttributeRandom 对@N 做随机操作；
      1. Distribution.Distribution 设定为 DirectionOrOrientation；
   5. 对 Sphere 加上 Moutain 之后撒到上述分布点上；
4. 制作云：
   1. 如果想要制作底部平整的云，用 Clip 进行底部裁切，然后用 PolyFill 缝合（缝合类型取决于后续细分需求）；
   2. 添加 VdbActivate 调整结算边界尺寸，有两种方法：
      1. 直接调整 Expand.VoxelsToExpand；
      2. 在 Config 中进行调整，然后引入 Position.Center/Size；
   3. 对云添加簇扰乱（否则会是明显一簇一簇的样子）：
      1. 添加 VolumeVop；
      2. 用 UnifiledNoise 节点对云进行多层簇扰乱，需要设置为 3DInput3DNoise，类型建议 F1；
      3. 用 VolumeSample 对 density 属性进行重新采样并赋予 density 属性；
   4. 对云添加边缘打薄扰乱（会出现特别薄的部分）：
      1. 添加 VolumeVop；
      2. 用 AntiAliasedNoise 节点对云进行多层簇扰乱，需要设置为 3DInput3DNoise；
      3. 用 VolumeSample 对 density 属性进行重新采样并赋予 density 属性；
   5. 对云添加黏连效果（模拟被风吹在一起的感觉）：
      1. 添加 VolumeVop；
      2. 用 AntiAliasedFlowNoise 节点对云进行多层簇扰乱，需要设置为 3DInput3DNoise；
      3. 用 VolumeSample 对 density 属性进行重新采样并赋予 density 属性；
5. 材质：mat.Cloud 材质；
6. 渲染：略；

<br>

#### 实操案例 - 河床中的石头

---

1.  建立地形：
    1.  新建平地：[Grid]；
    2.  添加地面噪音：[Moutain]；
    3.  挤出平整体积：[ExtrudeVolume]；
2.  添加河床：
    1.  新建 Boolean 体：[Box]；
    2.  修改上宽下窄：选中面之后拉伸；
    3.  对 Box 添加噪音：[AttributeNoise]，作用于 P；
    4.  修正点线面：
        1. 将 Boolean 体转为 VDB：[VolumeFromPolygon]；
        2. 将 VDB 转回 Polygon：[ConvertVDB]；
    5.  用地形对河床体做 Boolean 减运算：Boolean 节点；
    6.  添加/修正法线：[Normal]；
    7.  输出：[Null]；
3.  设置 DopNetwork；
4.  设置石头：
    1.  通过 AttributePaint 节点直接绘制区域（或者程序生成区域）：
    2.  撒点：[Scatter]；
    3.  添加随机大小：[AttributeRandomize]
        1. 对 pscale 做随机；
        2. 调整映射：可将 Distribution 修改为 Custom Ramp，手动调整；
    4.  调整间距：[PointRelax]：
        1. PointRediusAttribute 设为 pscale 属性，可以正好避免所有碰撞，但会造成分布范围扩大；
        2. 为了解决分布范围扩大的问题，第二输入连接到分布体（河床），可以实现强制吸附；
5.  材质：；
6.  渲染：略；

<br>

#### 实操案例 - 毛发解算

---
