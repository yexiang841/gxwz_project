## 《元宇宙工作学习笔记》-- QGIS

<br>

#### 介绍

---

- 项目文档记录了笔者工作积累的云原生微服务技术笔记，以及元宇宙、数字孪生、程序化建模工作学习笔记；
- 项目包含一套元宇宙建模方法论和工程，采用 QGIS -> Blender / Houdini -> Unreal / Unity 的方式实现数字孪生虚拟城市；
- 项目包含一个 Unreal 客户端工程和一个 Unity 客户端工程（建设中）；
- 项目包含一个 Cesium 网页端工程，实现 WebGIS BI 展示；
- 项目包含一个 Vue.js / ElementPlus / TS 网页端工程，实现 Admin 管理后台；
- 项目包含一个 Java / SpringBoot 微服务工程和一个 C# / DotNet 微服务工程（建设中），共用网关和服务注册中心（Nacos），可以协同工作；
- 项目包含一个 Publish 部署工程，采用 Docker / Kubernetes / Jenkins 方式进行服务部署；
- 程序语言涉及 C++ / Java / C# / Python / TS / Shell / VEX；
- 错漏之处恳请读者不吝指正，转载望注明出处：<https://gitee.com/yexiang841/q3d_project/>；
- Copyright 叶湘 ( weixin:yexiang841, email:yexiang841@qq.com ), Shitouren Studio. All Rights Reserved.

<br>

#### 元宇宙、数字孪生、程序化建模工作学习笔记

---

- [《元宇宙工作学习笔记》-- 数字孪生虚拟城市建模方法](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_graphics/README.Meta.md)
- [《元宇宙工作学习笔记》-- 图形学基础](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_graphics/README.Graphics.md)
- `>>>>` [《元宇宙工作学习笔记》-- QGIS](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_graphics/README.QGIS.md)
- [《元宇宙工作学习笔记》-- Blender](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_graphics/README.Blender.md)
- [《元宇宙工作学习笔记》-- C4D](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_graphics/README.C4D.md)
- [《元宇宙工作学习笔记》-- Houdini](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_graphics/README.Houdini.md)
- [《元宇宙工作学习笔记》-- SpeedTree](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_graphics/README.SpeedTree.md)
- [《元宇宙工作学习笔记》-- Illustrator](https://gitee.com/yexiang841/q3d_project/blob/master/q3d_wiki/q3d_wiki_graphics/README.Illustrator.md)

<br>

#### 学习笔记

---

1.  为什么选择 QGIS：
    - 轻量级系统，基本功能可替换 ArcGIS，学习曲线比较平顺；
    - 一个工具能替代 ArcGis、GlobalMapper、全能地图下载器、WorldMachine；
    - 开源免费跨平台；
    - 支持直接下载 GIS 数字资产；
2.  软件简介：
    - 官网：<https://www.qgis.org>；
    - 可以从官网下载插件离线安装，插件：<https://plugins.qgis.org/>；
3.  教程
    - B 站国匠城：<https://www.bilibili.com/video/BV1vg4y1B7Wa>；
    - B 站四季留歌：<https://www.bilibili.com/video/BV1PW411N7vH>；
4.  案例
    - ；
5.  资源
    - ；
6.  界面：
    - ；
7.  视图：
    - QGIS 有两种操作模式，一种是视图模式，一种是编辑模式；
    - 与 3D 软件稍微有点区别，QGIS 软件的视图操作，主要是移动和缩放操作；
    - QGIS 软件经常需要调整视口尺寸，因此建立视口书签能显著提升工作效率；
8.  工作流
    - 从浏览器窗口的工程主目录将资源导入到窗口，会自动新建图层；
    - 最佳实践是保持图层命名与主工程目录资源命名的类似或关联；
    - QGIS 是在视图区域按需加载，区域外的资源不会占用算力；
    - 右键图层打开属性列表可查看原始数据信息，右键图层显示标注可将其中的 Name 字段显示出来；
    - 对图层的操作不会影响原始资产；
    - 视图菜单中的新建三维视图可根据高程字段自动生成三维影像，但需要将坐标系设定为投影坐标系；
    - 导出范围；
9.  坐标系
    - 坐标系有两种：
      - 地理坐标：使用经纬度，基于球体，方便精确寻址；
      - 投影坐标：使用投影到平面之后的坐标，方便测算，投射方式会有很多种，主要是拉伸和展开的方式不同，最常用的是圆柱体投影；
    - WGS:84 是最常用的地理坐标系，WGS:84 / UTM 是常用的投影坐标系；
    - EPSG 是欧洲的地理机构，对坐标系进行了统一编码，例如：
      - EPSG:4326 就是 WGS:84
      - EPSG:32xxx 就是 WGS:84 / UTM
10. 矢量
    - 矢量图层可以在矢量菜单选项中重投影到不同的坐标系；
    - 矢量图层右键打开属性列表之后，还可以像操作数表一样对数据进行增删改查；
    - 在矢量->研究工具选项中创建网格，是常用的建模选取手段，通常选用投影坐标；
    - 除了菜单栏矢量中的工具之外，菜单栏处理中的矢量工具还有很多扩展功能
11. 符号化
    - 双击图层或者右键最后一项是打开图层属性，符号化是常用选项，用以修改显示效果；
    - 单位：调整显示尺寸，例如像素显示不受缩放影响；
    - 简单标记：调整显示效果，如形状、描边、填充、透明度等；
    - 图层渲染：调整不同图层之间混合显示的效果；
    - 显示方式：
      - 单一显示：通常是原始的选项；
      - 分类：按属性分类显示；
      - 渐进：按属性渐进显示；
      - 点聚类：随缩放自动将点聚集；
      - 热图：用颜色描述密集度；
12. 加载在线栅格底图的方法
    - 参考：<https://www.bilibili.com/video/BV1yg4y1B7FY>；
    - QuickMapServices 插件
      - 提供免费栅格底图，没有矢量图；
      - AutoNavi：高德地图，丰富的 POI，做导航推荐；
      - bing 卫星图：比较快，精度高，做实景推荐；
      - ESRI：种类多，有各种纯色底图、地形图等，精度不够，做 BI 推荐；
      - CatoDB：黑白两色底图，精度较高，做 BI 推荐；
      - Stamen：有很不错的高程图，局部精度高，做建模推荐；
    - HCMGIS 插件
      - 与 QuickMapServices 大同小异，也是提供免费栅格底图；
    - XYZtiles 工具；
      - 手动添加在线栅格底图的工具；
      - 作为 QuickMapServices 的补充，主要作用是添加天地图；
    - WMS/WMTS 接入；
      - 与 XYZtiles 工具差不多，但是 WMS/WMTS 比 XYZTiles 的优势是数据格式更规范，如内容分层；
13. 插件
    - GeoHey
      - 仅针对矢量图层；
      - 解决地图的偏移问题，比如高德地图；
      - 安装插件后，调出数据处理工具箱，找到 GeoHey，选择双击 WGS to GCJ02，选择高德的矢量图，完成转换输出到新图层；
14. 导出图片工作流
    - 导出分为视图导出和裁切导出；
    - 卫星图：；
    - 矢量图：；
14. 导出图片设置
    - DPI
      - 网络传输显示：96 dpi；
      - 纸品打印：300 dpi；
    - 范围（宽度和高度）
      - 设定印品内页尺寸（120 克胶版纸）：
        - 标准尺寸：52 cm x 38 cm（1.3684 ：1）
        - 标准尺寸三折：17.5 cm x 19 cm
        - 标准尺寸带出血：52.6 cm x 38.6 cm（1.3627 ：1）
        - PS 工程尺寸：像素 6213 x 4559 （1.3628 ：1），分辨率 300，颜色模式 CMYK
      - 设定印品封套尺寸（200 克铜版纸）：
        - 标准尺寸：22 cm x 19 cm
        - 标准尺寸带出血：
      - QGIS 软件设计比例参考：
        - View：16km x 11.6923km（1.3684：1）
        - Frame：20km x 17.5385km（1.3684：1）
        - Bound：32km x 23.3846km（1.3684：1）
15. 导出 3D 地图设置
    - 需要新建导出布局视图，拖入 3D 地图，然后才能导出；
