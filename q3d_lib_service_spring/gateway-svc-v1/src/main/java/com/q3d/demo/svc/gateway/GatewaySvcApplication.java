package com.q3d.demo.svc.gateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

// import springfox.documentation.oas.annotations.EnableOpenApi;

// @EnableOpenApi /** 开启 Swagger3，访问地址：http://{host}:{port}/{context-path}/swagger-ui/index.html */
@EnableDiscoveryClient /** 开启服务发现，使 Eureka 或者 Nacos 能按照服务名寻址 */
@EnableAspectJAutoProxy /** 让切面生效，目前作用于：日志切面 */
@EnableConfigurationProperties /** 开启 @ConfigurationProperties，支持从配置文件或远程配置读取属性注入对象 */
@ComponentScan(basePackages = { /** Bean 扫描路径，对依赖顺序也有要求 */
		"com.q3d.demo.common.lib.logger.runner.**", /** 引入 LogRunner 在日志中自动加服务名和端口号前缀 */
		// "com.q3d.demo.common.lib.logger.aspect.**", /** 切面所依赖的 Web 服务与 Gateway 冲突 */
		"com.q3d.demo.svc.gateway.**.**", /** Gateway-Svc 包扫描路径 */
		"com.q3d.demo.svc.gateway.**.**.**", /** Gateway-Svc 包扫描路径 */
})
@SpringBootApplication
/**
 * @author 叶湘 ( weixin:yexiang841, email:yexiang841@qq.com )
 */
public class GatewaySvcApplication {

	public static void main(String[] args) {
		SpringApplication.run(GatewaySvcApplication.class, args);
	}

}
