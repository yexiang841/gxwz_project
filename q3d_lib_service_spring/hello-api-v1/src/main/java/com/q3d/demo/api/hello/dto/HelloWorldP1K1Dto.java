package com.q3d.demo.api.hello.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import io.swagger.v3.oas.annotations.media.Schema;

import lombok.*;
import lombok.experimental.SuperBuilder;

@Data /**
       * Lombok @Data =
       * 
       * @Getter +
       * @Setter +
       * @ToString +
       * @EqualsAndHashCode +
       * @RequiredArgsConstruct
       */
@AllArgsConstructor /** Lombok 全参构造方法 */
@NoArgsConstructor /** Lombok 无参构造方法 */
@ToString(callSuper = true) /** 当基类和子类都用 @Data 时，子类须声明此项 */
@EqualsAndHashCode(callSuper = true) /** 当基类和子类都用 @Data 时，子类须声明此项 */
@SuperBuilder /** Lombok 链式 Builder 构造方法，当基类和子类都用 @Data 时，两者都要从 @Builder 改成 @SuperBuilder */
@Schema(description = "按 UUID 选择性修改 HelloWorld 请求结构体，带全局唯一 UUID") /** SpringDoc 注解 */
/**
 * @author 叶湘 ( weixin:yexiang841, email:yexiang841@qq.com )
 */
public class HelloWorldP1K1Dto extends HelloWorldP1Dto {

    @Schema(description = "全局唯一 UUID", example = "774e1f502f704a5d810aa729c8544a07")
    @NotNull(message = "全局唯一 UUID 是必填项")
    @NotBlank(message = "全局唯一 UUID 不能为空")
    @Pattern(regexp = "^[a-f\\d]{32}$", message = "UUID 必须是去掉 4 位横杠的 32 位十六进制数")
    protected String uuid;

}
