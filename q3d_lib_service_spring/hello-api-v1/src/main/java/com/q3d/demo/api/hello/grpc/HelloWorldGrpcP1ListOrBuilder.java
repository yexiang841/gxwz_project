// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: HelloWorld.proto

package com.q3d.demo.api.hello.grpc;

public interface HelloWorldGrpcP1ListOrBuilder extends
    // @@protoc_insertion_point(interface_extends:HelloWorldGrpcP1List)
    com.google.protobuf.MessageOrBuilder {

  /**
   * <code>repeated .HelloWorldGrpcP1 grpc = 1;</code>
   */
  java.util.List<com.q3d.demo.api.hello.grpc.HelloWorldGrpcP1> 
      getGrpcList();
  /**
   * <code>repeated .HelloWorldGrpcP1 grpc = 1;</code>
   */
  com.q3d.demo.api.hello.grpc.HelloWorldGrpcP1 getGrpc(int index);
  /**
   * <code>repeated .HelloWorldGrpcP1 grpc = 1;</code>
   */
  int getGrpcCount();
  /**
   * <code>repeated .HelloWorldGrpcP1 grpc = 1;</code>
   */
  java.util.List<? extends com.q3d.demo.api.hello.grpc.HelloWorldGrpcP1OrBuilder> 
      getGrpcOrBuilderList();
  /**
   * <code>repeated .HelloWorldGrpcP1 grpc = 1;</code>
   */
  com.q3d.demo.api.hello.grpc.HelloWorldGrpcP1OrBuilder getGrpcOrBuilder(
      int index);
}
