package com.q3d.demo.api.hello.grpc;

import static io.grpc.MethodDescriptor.generateFullMethodName;
import static io.grpc.stub.ClientCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ClientCalls.asyncClientStreamingCall;
import static io.grpc.stub.ClientCalls.asyncServerStreamingCall;
import static io.grpc.stub.ClientCalls.asyncUnaryCall;
import static io.grpc.stub.ClientCalls.blockingServerStreamingCall;
import static io.grpc.stub.ClientCalls.blockingUnaryCall;
import static io.grpc.stub.ClientCalls.futureUnaryCall;
import static io.grpc.stub.ServerCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ServerCalls.asyncClientStreamingCall;
import static io.grpc.stub.ServerCalls.asyncServerStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnaryCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall;

/**
 * <pre>
 * 定义服务
 * 无参数或者无返回值使用(google.protobuf.Empty)
 * </pre>
 */
@javax.annotation.Generated(
    value = "by gRPC proto compiler (version 1.15.0)",
    comments = "Source: HelloWorld.proto")
public final class HelloWorldServiceGrpc {

  private HelloWorldServiceGrpc() {}

  public static final String SERVICE_NAME = "HelloWorldService";

  // Static method descriptors that strictly reflect the proto.
  private static volatile io.grpc.MethodDescriptor<com.q3d.demo.api.hello.grpc.StringGrpc,
      com.q3d.demo.api.hello.grpc.StringGrpc> getHelloMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "Hello",
      requestType = com.q3d.demo.api.hello.grpc.StringGrpc.class,
      responseType = com.q3d.demo.api.hello.grpc.StringGrpc.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<com.q3d.demo.api.hello.grpc.StringGrpc,
      com.q3d.demo.api.hello.grpc.StringGrpc> getHelloMethod() {
    io.grpc.MethodDescriptor<com.q3d.demo.api.hello.grpc.StringGrpc, com.q3d.demo.api.hello.grpc.StringGrpc> getHelloMethod;
    if ((getHelloMethod = HelloWorldServiceGrpc.getHelloMethod) == null) {
      synchronized (HelloWorldServiceGrpc.class) {
        if ((getHelloMethod = HelloWorldServiceGrpc.getHelloMethod) == null) {
          HelloWorldServiceGrpc.getHelloMethod = getHelloMethod = 
              io.grpc.MethodDescriptor.<com.q3d.demo.api.hello.grpc.StringGrpc, com.q3d.demo.api.hello.grpc.StringGrpc>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(
                  "HelloWorldService", "Hello"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.q3d.demo.api.hello.grpc.StringGrpc.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.q3d.demo.api.hello.grpc.StringGrpc.getDefaultInstance()))
                  .setSchemaDescriptor(new HelloWorldServiceMethodDescriptorSupplier("Hello"))
                  .build();
          }
        }
     }
     return getHelloMethod;
  }

  private static volatile io.grpc.MethodDescriptor<com.q3d.demo.api.hello.grpc.HelloWorldGrpcP1,
      com.q3d.demo.api.hello.grpc.UuidGrpc> getAddOneMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "AddOne",
      requestType = com.q3d.demo.api.hello.grpc.HelloWorldGrpcP1.class,
      responseType = com.q3d.demo.api.hello.grpc.UuidGrpc.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<com.q3d.demo.api.hello.grpc.HelloWorldGrpcP1,
      com.q3d.demo.api.hello.grpc.UuidGrpc> getAddOneMethod() {
    io.grpc.MethodDescriptor<com.q3d.demo.api.hello.grpc.HelloWorldGrpcP1, com.q3d.demo.api.hello.grpc.UuidGrpc> getAddOneMethod;
    if ((getAddOneMethod = HelloWorldServiceGrpc.getAddOneMethod) == null) {
      synchronized (HelloWorldServiceGrpc.class) {
        if ((getAddOneMethod = HelloWorldServiceGrpc.getAddOneMethod) == null) {
          HelloWorldServiceGrpc.getAddOneMethod = getAddOneMethod = 
              io.grpc.MethodDescriptor.<com.q3d.demo.api.hello.grpc.HelloWorldGrpcP1, com.q3d.demo.api.hello.grpc.UuidGrpc>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(
                  "HelloWorldService", "AddOne"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.q3d.demo.api.hello.grpc.HelloWorldGrpcP1.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.q3d.demo.api.hello.grpc.UuidGrpc.getDefaultInstance()))
                  .setSchemaDescriptor(new HelloWorldServiceMethodDescriptorSupplier("AddOne"))
                  .build();
          }
        }
     }
     return getAddOneMethod;
  }

  private static volatile io.grpc.MethodDescriptor<com.q3d.demo.api.hello.grpc.HelloWorldGrpcP1List,
      com.q3d.demo.api.hello.grpc.UuidListGrpc> getAddBatchMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "AddBatch",
      requestType = com.q3d.demo.api.hello.grpc.HelloWorldGrpcP1List.class,
      responseType = com.q3d.demo.api.hello.grpc.UuidListGrpc.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<com.q3d.demo.api.hello.grpc.HelloWorldGrpcP1List,
      com.q3d.demo.api.hello.grpc.UuidListGrpc> getAddBatchMethod() {
    io.grpc.MethodDescriptor<com.q3d.demo.api.hello.grpc.HelloWorldGrpcP1List, com.q3d.demo.api.hello.grpc.UuidListGrpc> getAddBatchMethod;
    if ((getAddBatchMethod = HelloWorldServiceGrpc.getAddBatchMethod) == null) {
      synchronized (HelloWorldServiceGrpc.class) {
        if ((getAddBatchMethod = HelloWorldServiceGrpc.getAddBatchMethod) == null) {
          HelloWorldServiceGrpc.getAddBatchMethod = getAddBatchMethod = 
              io.grpc.MethodDescriptor.<com.q3d.demo.api.hello.grpc.HelloWorldGrpcP1List, com.q3d.demo.api.hello.grpc.UuidListGrpc>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(
                  "HelloWorldService", "AddBatch"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.q3d.demo.api.hello.grpc.HelloWorldGrpcP1List.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.q3d.demo.api.hello.grpc.UuidListGrpc.getDefaultInstance()))
                  .setSchemaDescriptor(new HelloWorldServiceMethodDescriptorSupplier("AddBatch"))
                  .build();
          }
        }
     }
     return getAddBatchMethod;
  }

  private static volatile io.grpc.MethodDescriptor<com.q3d.demo.api.hello.grpc.UuidGrpc,
      com.q3d.demo.api.hello.grpc.OpResultGrpc> getRemoveOneMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "RemoveOne",
      requestType = com.q3d.demo.api.hello.grpc.UuidGrpc.class,
      responseType = com.q3d.demo.api.hello.grpc.OpResultGrpc.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<com.q3d.demo.api.hello.grpc.UuidGrpc,
      com.q3d.demo.api.hello.grpc.OpResultGrpc> getRemoveOneMethod() {
    io.grpc.MethodDescriptor<com.q3d.demo.api.hello.grpc.UuidGrpc, com.q3d.demo.api.hello.grpc.OpResultGrpc> getRemoveOneMethod;
    if ((getRemoveOneMethod = HelloWorldServiceGrpc.getRemoveOneMethod) == null) {
      synchronized (HelloWorldServiceGrpc.class) {
        if ((getRemoveOneMethod = HelloWorldServiceGrpc.getRemoveOneMethod) == null) {
          HelloWorldServiceGrpc.getRemoveOneMethod = getRemoveOneMethod = 
              io.grpc.MethodDescriptor.<com.q3d.demo.api.hello.grpc.UuidGrpc, com.q3d.demo.api.hello.grpc.OpResultGrpc>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(
                  "HelloWorldService", "RemoveOne"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.q3d.demo.api.hello.grpc.UuidGrpc.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.q3d.demo.api.hello.grpc.OpResultGrpc.getDefaultInstance()))
                  .setSchemaDescriptor(new HelloWorldServiceMethodDescriptorSupplier("RemoveOne"))
                  .build();
          }
        }
     }
     return getRemoveOneMethod;
  }

  private static volatile io.grpc.MethodDescriptor<com.q3d.demo.api.hello.grpc.UuidListGrpc,
      com.q3d.demo.api.hello.grpc.OpResultGrpc> getRemoveBatchMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "RemoveBatch",
      requestType = com.q3d.demo.api.hello.grpc.UuidListGrpc.class,
      responseType = com.q3d.demo.api.hello.grpc.OpResultGrpc.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<com.q3d.demo.api.hello.grpc.UuidListGrpc,
      com.q3d.demo.api.hello.grpc.OpResultGrpc> getRemoveBatchMethod() {
    io.grpc.MethodDescriptor<com.q3d.demo.api.hello.grpc.UuidListGrpc, com.q3d.demo.api.hello.grpc.OpResultGrpc> getRemoveBatchMethod;
    if ((getRemoveBatchMethod = HelloWorldServiceGrpc.getRemoveBatchMethod) == null) {
      synchronized (HelloWorldServiceGrpc.class) {
        if ((getRemoveBatchMethod = HelloWorldServiceGrpc.getRemoveBatchMethod) == null) {
          HelloWorldServiceGrpc.getRemoveBatchMethod = getRemoveBatchMethod = 
              io.grpc.MethodDescriptor.<com.q3d.demo.api.hello.grpc.UuidListGrpc, com.q3d.demo.api.hello.grpc.OpResultGrpc>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(
                  "HelloWorldService", "RemoveBatch"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.q3d.demo.api.hello.grpc.UuidListGrpc.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.q3d.demo.api.hello.grpc.OpResultGrpc.getDefaultInstance()))
                  .setSchemaDescriptor(new HelloWorldServiceMethodDescriptorSupplier("RemoveBatch"))
                  .build();
          }
        }
     }
     return getRemoveBatchMethod;
  }

  private static volatile io.grpc.MethodDescriptor<com.q3d.demo.api.hello.grpc.IdExtGrpc,
      com.q3d.demo.api.hello.grpc.OpResultGrpc> getDeleteOneMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "DeleteOne",
      requestType = com.q3d.demo.api.hello.grpc.IdExtGrpc.class,
      responseType = com.q3d.demo.api.hello.grpc.OpResultGrpc.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<com.q3d.demo.api.hello.grpc.IdExtGrpc,
      com.q3d.demo.api.hello.grpc.OpResultGrpc> getDeleteOneMethod() {
    io.grpc.MethodDescriptor<com.q3d.demo.api.hello.grpc.IdExtGrpc, com.q3d.demo.api.hello.grpc.OpResultGrpc> getDeleteOneMethod;
    if ((getDeleteOneMethod = HelloWorldServiceGrpc.getDeleteOneMethod) == null) {
      synchronized (HelloWorldServiceGrpc.class) {
        if ((getDeleteOneMethod = HelloWorldServiceGrpc.getDeleteOneMethod) == null) {
          HelloWorldServiceGrpc.getDeleteOneMethod = getDeleteOneMethod = 
              io.grpc.MethodDescriptor.<com.q3d.demo.api.hello.grpc.IdExtGrpc, com.q3d.demo.api.hello.grpc.OpResultGrpc>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(
                  "HelloWorldService", "DeleteOne"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.q3d.demo.api.hello.grpc.IdExtGrpc.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.q3d.demo.api.hello.grpc.OpResultGrpc.getDefaultInstance()))
                  .setSchemaDescriptor(new HelloWorldServiceMethodDescriptorSupplier("DeleteOne"))
                  .build();
          }
        }
     }
     return getDeleteOneMethod;
  }

  private static volatile io.grpc.MethodDescriptor<com.q3d.demo.api.hello.grpc.IdExtListGrpc,
      com.q3d.demo.api.hello.grpc.OpResultGrpc> getDeleteBatchMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "DeleteBatch",
      requestType = com.q3d.demo.api.hello.grpc.IdExtListGrpc.class,
      responseType = com.q3d.demo.api.hello.grpc.OpResultGrpc.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<com.q3d.demo.api.hello.grpc.IdExtListGrpc,
      com.q3d.demo.api.hello.grpc.OpResultGrpc> getDeleteBatchMethod() {
    io.grpc.MethodDescriptor<com.q3d.demo.api.hello.grpc.IdExtListGrpc, com.q3d.demo.api.hello.grpc.OpResultGrpc> getDeleteBatchMethod;
    if ((getDeleteBatchMethod = HelloWorldServiceGrpc.getDeleteBatchMethod) == null) {
      synchronized (HelloWorldServiceGrpc.class) {
        if ((getDeleteBatchMethod = HelloWorldServiceGrpc.getDeleteBatchMethod) == null) {
          HelloWorldServiceGrpc.getDeleteBatchMethod = getDeleteBatchMethod = 
              io.grpc.MethodDescriptor.<com.q3d.demo.api.hello.grpc.IdExtListGrpc, com.q3d.demo.api.hello.grpc.OpResultGrpc>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(
                  "HelloWorldService", "DeleteBatch"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.q3d.demo.api.hello.grpc.IdExtListGrpc.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.q3d.demo.api.hello.grpc.OpResultGrpc.getDefaultInstance()))
                  .setSchemaDescriptor(new HelloWorldServiceMethodDescriptorSupplier("DeleteBatch"))
                  .build();
          }
        }
     }
     return getDeleteBatchMethod;
  }

  private static volatile io.grpc.MethodDescriptor<com.q3d.demo.api.hello.grpc.IdExtGrpc,
      com.q3d.demo.api.hello.grpc.OpResultGrpc> getReviveOneMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "ReviveOne",
      requestType = com.q3d.demo.api.hello.grpc.IdExtGrpc.class,
      responseType = com.q3d.demo.api.hello.grpc.OpResultGrpc.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<com.q3d.demo.api.hello.grpc.IdExtGrpc,
      com.q3d.demo.api.hello.grpc.OpResultGrpc> getReviveOneMethod() {
    io.grpc.MethodDescriptor<com.q3d.demo.api.hello.grpc.IdExtGrpc, com.q3d.demo.api.hello.grpc.OpResultGrpc> getReviveOneMethod;
    if ((getReviveOneMethod = HelloWorldServiceGrpc.getReviveOneMethod) == null) {
      synchronized (HelloWorldServiceGrpc.class) {
        if ((getReviveOneMethod = HelloWorldServiceGrpc.getReviveOneMethod) == null) {
          HelloWorldServiceGrpc.getReviveOneMethod = getReviveOneMethod = 
              io.grpc.MethodDescriptor.<com.q3d.demo.api.hello.grpc.IdExtGrpc, com.q3d.demo.api.hello.grpc.OpResultGrpc>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(
                  "HelloWorldService", "ReviveOne"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.q3d.demo.api.hello.grpc.IdExtGrpc.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.q3d.demo.api.hello.grpc.OpResultGrpc.getDefaultInstance()))
                  .setSchemaDescriptor(new HelloWorldServiceMethodDescriptorSupplier("ReviveOne"))
                  .build();
          }
        }
     }
     return getReviveOneMethod;
  }

  private static volatile io.grpc.MethodDescriptor<com.q3d.demo.api.hello.grpc.IdExtListGrpc,
      com.q3d.demo.api.hello.grpc.OpResultGrpc> getReviveBatchMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "ReviveBatch",
      requestType = com.q3d.demo.api.hello.grpc.IdExtListGrpc.class,
      responseType = com.q3d.demo.api.hello.grpc.OpResultGrpc.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<com.q3d.demo.api.hello.grpc.IdExtListGrpc,
      com.q3d.demo.api.hello.grpc.OpResultGrpc> getReviveBatchMethod() {
    io.grpc.MethodDescriptor<com.q3d.demo.api.hello.grpc.IdExtListGrpc, com.q3d.demo.api.hello.grpc.OpResultGrpc> getReviveBatchMethod;
    if ((getReviveBatchMethod = HelloWorldServiceGrpc.getReviveBatchMethod) == null) {
      synchronized (HelloWorldServiceGrpc.class) {
        if ((getReviveBatchMethod = HelloWorldServiceGrpc.getReviveBatchMethod) == null) {
          HelloWorldServiceGrpc.getReviveBatchMethod = getReviveBatchMethod = 
              io.grpc.MethodDescriptor.<com.q3d.demo.api.hello.grpc.IdExtListGrpc, com.q3d.demo.api.hello.grpc.OpResultGrpc>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(
                  "HelloWorldService", "ReviveBatch"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.q3d.demo.api.hello.grpc.IdExtListGrpc.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.q3d.demo.api.hello.grpc.OpResultGrpc.getDefaultInstance()))
                  .setSchemaDescriptor(new HelloWorldServiceMethodDescriptorSupplier("ReviveBatch"))
                  .build();
          }
        }
     }
     return getReviveBatchMethod;
  }

  private static volatile io.grpc.MethodDescriptor<com.q3d.demo.api.hello.grpc.HelloWorldGrpcP1K1,
      com.q3d.demo.api.hello.grpc.OpResultGrpc> getEditSelectiveMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "EditSelective",
      requestType = com.q3d.demo.api.hello.grpc.HelloWorldGrpcP1K1.class,
      responseType = com.q3d.demo.api.hello.grpc.OpResultGrpc.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<com.q3d.demo.api.hello.grpc.HelloWorldGrpcP1K1,
      com.q3d.demo.api.hello.grpc.OpResultGrpc> getEditSelectiveMethod() {
    io.grpc.MethodDescriptor<com.q3d.demo.api.hello.grpc.HelloWorldGrpcP1K1, com.q3d.demo.api.hello.grpc.OpResultGrpc> getEditSelectiveMethod;
    if ((getEditSelectiveMethod = HelloWorldServiceGrpc.getEditSelectiveMethod) == null) {
      synchronized (HelloWorldServiceGrpc.class) {
        if ((getEditSelectiveMethod = HelloWorldServiceGrpc.getEditSelectiveMethod) == null) {
          HelloWorldServiceGrpc.getEditSelectiveMethod = getEditSelectiveMethod = 
              io.grpc.MethodDescriptor.<com.q3d.demo.api.hello.grpc.HelloWorldGrpcP1K1, com.q3d.demo.api.hello.grpc.OpResultGrpc>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(
                  "HelloWorldService", "EditSelective"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.q3d.demo.api.hello.grpc.HelloWorldGrpcP1K1.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.q3d.demo.api.hello.grpc.OpResultGrpc.getDefaultInstance()))
                  .setSchemaDescriptor(new HelloWorldServiceMethodDescriptorSupplier("EditSelective"))
                  .build();
          }
        }
     }
     return getEditSelectiveMethod;
  }

  private static volatile io.grpc.MethodDescriptor<com.q3d.demo.api.hello.grpc.HelloWorldGrpcP1K2,
      com.q3d.demo.api.hello.grpc.OpResultGrpc> getUpdateSelectiveMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "UpdateSelective",
      requestType = com.q3d.demo.api.hello.grpc.HelloWorldGrpcP1K2.class,
      responseType = com.q3d.demo.api.hello.grpc.OpResultGrpc.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<com.q3d.demo.api.hello.grpc.HelloWorldGrpcP1K2,
      com.q3d.demo.api.hello.grpc.OpResultGrpc> getUpdateSelectiveMethod() {
    io.grpc.MethodDescriptor<com.q3d.demo.api.hello.grpc.HelloWorldGrpcP1K2, com.q3d.demo.api.hello.grpc.OpResultGrpc> getUpdateSelectiveMethod;
    if ((getUpdateSelectiveMethod = HelloWorldServiceGrpc.getUpdateSelectiveMethod) == null) {
      synchronized (HelloWorldServiceGrpc.class) {
        if ((getUpdateSelectiveMethod = HelloWorldServiceGrpc.getUpdateSelectiveMethod) == null) {
          HelloWorldServiceGrpc.getUpdateSelectiveMethod = getUpdateSelectiveMethod = 
              io.grpc.MethodDescriptor.<com.q3d.demo.api.hello.grpc.HelloWorldGrpcP1K2, com.q3d.demo.api.hello.grpc.OpResultGrpc>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(
                  "HelloWorldService", "UpdateSelective"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.q3d.demo.api.hello.grpc.HelloWorldGrpcP1K2.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.q3d.demo.api.hello.grpc.OpResultGrpc.getDefaultInstance()))
                  .setSchemaDescriptor(new HelloWorldServiceMethodDescriptorSupplier("UpdateSelective"))
                  .build();
          }
        }
     }
     return getUpdateSelectiveMethod;
  }

  private static volatile io.grpc.MethodDescriptor<com.google.protobuf.Empty,
      com.q3d.demo.api.hello.grpc.CountResultGrpc> getGetCountMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "GetCount",
      requestType = com.google.protobuf.Empty.class,
      responseType = com.q3d.demo.api.hello.grpc.CountResultGrpc.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<com.google.protobuf.Empty,
      com.q3d.demo.api.hello.grpc.CountResultGrpc> getGetCountMethod() {
    io.grpc.MethodDescriptor<com.google.protobuf.Empty, com.q3d.demo.api.hello.grpc.CountResultGrpc> getGetCountMethod;
    if ((getGetCountMethod = HelloWorldServiceGrpc.getGetCountMethod) == null) {
      synchronized (HelloWorldServiceGrpc.class) {
        if ((getGetCountMethod = HelloWorldServiceGrpc.getGetCountMethod) == null) {
          HelloWorldServiceGrpc.getGetCountMethod = getGetCountMethod = 
              io.grpc.MethodDescriptor.<com.google.protobuf.Empty, com.q3d.demo.api.hello.grpc.CountResultGrpc>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(
                  "HelloWorldService", "GetCount"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.google.protobuf.Empty.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.q3d.demo.api.hello.grpc.CountResultGrpc.getDefaultInstance()))
                  .setSchemaDescriptor(new HelloWorldServiceMethodDescriptorSupplier("GetCount"))
                  .build();
          }
        }
     }
     return getGetCountMethod;
  }

  private static volatile io.grpc.MethodDescriptor<com.q3d.demo.api.hello.grpc.UuidGrpc,
      com.q3d.demo.api.hello.grpc.HelloWorldGrpcR1K1> getGetOneMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "GetOne",
      requestType = com.q3d.demo.api.hello.grpc.UuidGrpc.class,
      responseType = com.q3d.demo.api.hello.grpc.HelloWorldGrpcR1K1.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<com.q3d.demo.api.hello.grpc.UuidGrpc,
      com.q3d.demo.api.hello.grpc.HelloWorldGrpcR1K1> getGetOneMethod() {
    io.grpc.MethodDescriptor<com.q3d.demo.api.hello.grpc.UuidGrpc, com.q3d.demo.api.hello.grpc.HelloWorldGrpcR1K1> getGetOneMethod;
    if ((getGetOneMethod = HelloWorldServiceGrpc.getGetOneMethod) == null) {
      synchronized (HelloWorldServiceGrpc.class) {
        if ((getGetOneMethod = HelloWorldServiceGrpc.getGetOneMethod) == null) {
          HelloWorldServiceGrpc.getGetOneMethod = getGetOneMethod = 
              io.grpc.MethodDescriptor.<com.q3d.demo.api.hello.grpc.UuidGrpc, com.q3d.demo.api.hello.grpc.HelloWorldGrpcR1K1>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(
                  "HelloWorldService", "GetOne"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.q3d.demo.api.hello.grpc.UuidGrpc.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.q3d.demo.api.hello.grpc.HelloWorldGrpcR1K1.getDefaultInstance()))
                  .setSchemaDescriptor(new HelloWorldServiceMethodDescriptorSupplier("GetOne"))
                  .build();
          }
        }
     }
     return getGetOneMethod;
  }

  private static volatile io.grpc.MethodDescriptor<com.q3d.demo.api.hello.grpc.UuidListGrpc,
      com.q3d.demo.api.hello.grpc.HelloWorldGrpcR1K1List> getGetBatchMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "GetBatch",
      requestType = com.q3d.demo.api.hello.grpc.UuidListGrpc.class,
      responseType = com.q3d.demo.api.hello.grpc.HelloWorldGrpcR1K1List.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<com.q3d.demo.api.hello.grpc.UuidListGrpc,
      com.q3d.demo.api.hello.grpc.HelloWorldGrpcR1K1List> getGetBatchMethod() {
    io.grpc.MethodDescriptor<com.q3d.demo.api.hello.grpc.UuidListGrpc, com.q3d.demo.api.hello.grpc.HelloWorldGrpcR1K1List> getGetBatchMethod;
    if ((getGetBatchMethod = HelloWorldServiceGrpc.getGetBatchMethod) == null) {
      synchronized (HelloWorldServiceGrpc.class) {
        if ((getGetBatchMethod = HelloWorldServiceGrpc.getGetBatchMethod) == null) {
          HelloWorldServiceGrpc.getGetBatchMethod = getGetBatchMethod = 
              io.grpc.MethodDescriptor.<com.q3d.demo.api.hello.grpc.UuidListGrpc, com.q3d.demo.api.hello.grpc.HelloWorldGrpcR1K1List>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(
                  "HelloWorldService", "GetBatch"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.q3d.demo.api.hello.grpc.UuidListGrpc.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.q3d.demo.api.hello.grpc.HelloWorldGrpcR1K1List.getDefaultInstance()))
                  .setSchemaDescriptor(new HelloWorldServiceMethodDescriptorSupplier("GetBatch"))
                  .build();
          }
        }
     }
     return getGetBatchMethod;
  }

  private static volatile io.grpc.MethodDescriptor<com.q3d.demo.api.hello.grpc.PageRequestGrpc,
      com.q3d.demo.api.hello.grpc.HelloWorldGrpcR1K1List> getGetByPageMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "GetByPage",
      requestType = com.q3d.demo.api.hello.grpc.PageRequestGrpc.class,
      responseType = com.q3d.demo.api.hello.grpc.HelloWorldGrpcR1K1List.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<com.q3d.demo.api.hello.grpc.PageRequestGrpc,
      com.q3d.demo.api.hello.grpc.HelloWorldGrpcR1K1List> getGetByPageMethod() {
    io.grpc.MethodDescriptor<com.q3d.demo.api.hello.grpc.PageRequestGrpc, com.q3d.demo.api.hello.grpc.HelloWorldGrpcR1K1List> getGetByPageMethod;
    if ((getGetByPageMethod = HelloWorldServiceGrpc.getGetByPageMethod) == null) {
      synchronized (HelloWorldServiceGrpc.class) {
        if ((getGetByPageMethod = HelloWorldServiceGrpc.getGetByPageMethod) == null) {
          HelloWorldServiceGrpc.getGetByPageMethod = getGetByPageMethod = 
              io.grpc.MethodDescriptor.<com.q3d.demo.api.hello.grpc.PageRequestGrpc, com.q3d.demo.api.hello.grpc.HelloWorldGrpcR1K1List>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(
                  "HelloWorldService", "GetByPage"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.q3d.demo.api.hello.grpc.PageRequestGrpc.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.q3d.demo.api.hello.grpc.HelloWorldGrpcR1K1List.getDefaultInstance()))
                  .setSchemaDescriptor(new HelloWorldServiceMethodDescriptorSupplier("GetByPage"))
                  .build();
          }
        }
     }
     return getGetByPageMethod;
  }

  private static volatile io.grpc.MethodDescriptor<com.q3d.demo.api.hello.grpc.PageRequestGrpc,
      com.q3d.demo.api.hello.grpc.UuidListGrpc> getGetUuidByPageMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "GetUuidByPage",
      requestType = com.q3d.demo.api.hello.grpc.PageRequestGrpc.class,
      responseType = com.q3d.demo.api.hello.grpc.UuidListGrpc.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<com.q3d.demo.api.hello.grpc.PageRequestGrpc,
      com.q3d.demo.api.hello.grpc.UuidListGrpc> getGetUuidByPageMethod() {
    io.grpc.MethodDescriptor<com.q3d.demo.api.hello.grpc.PageRequestGrpc, com.q3d.demo.api.hello.grpc.UuidListGrpc> getGetUuidByPageMethod;
    if ((getGetUuidByPageMethod = HelloWorldServiceGrpc.getGetUuidByPageMethod) == null) {
      synchronized (HelloWorldServiceGrpc.class) {
        if ((getGetUuidByPageMethod = HelloWorldServiceGrpc.getGetUuidByPageMethod) == null) {
          HelloWorldServiceGrpc.getGetUuidByPageMethod = getGetUuidByPageMethod = 
              io.grpc.MethodDescriptor.<com.q3d.demo.api.hello.grpc.PageRequestGrpc, com.q3d.demo.api.hello.grpc.UuidListGrpc>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(
                  "HelloWorldService", "GetUuidByPage"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.q3d.demo.api.hello.grpc.PageRequestGrpc.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.q3d.demo.api.hello.grpc.UuidListGrpc.getDefaultInstance()))
                  .setSchemaDescriptor(new HelloWorldServiceMethodDescriptorSupplier("GetUuidByPage"))
                  .build();
          }
        }
     }
     return getGetUuidByPageMethod;
  }

  private static volatile io.grpc.MethodDescriptor<com.google.protobuf.Empty,
      com.q3d.demo.api.hello.grpc.CountResultGrpc> getSelectCountMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "SelectCount",
      requestType = com.google.protobuf.Empty.class,
      responseType = com.q3d.demo.api.hello.grpc.CountResultGrpc.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<com.google.protobuf.Empty,
      com.q3d.demo.api.hello.grpc.CountResultGrpc> getSelectCountMethod() {
    io.grpc.MethodDescriptor<com.google.protobuf.Empty, com.q3d.demo.api.hello.grpc.CountResultGrpc> getSelectCountMethod;
    if ((getSelectCountMethod = HelloWorldServiceGrpc.getSelectCountMethod) == null) {
      synchronized (HelloWorldServiceGrpc.class) {
        if ((getSelectCountMethod = HelloWorldServiceGrpc.getSelectCountMethod) == null) {
          HelloWorldServiceGrpc.getSelectCountMethod = getSelectCountMethod = 
              io.grpc.MethodDescriptor.<com.google.protobuf.Empty, com.q3d.demo.api.hello.grpc.CountResultGrpc>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(
                  "HelloWorldService", "SelectCount"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.google.protobuf.Empty.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.q3d.demo.api.hello.grpc.CountResultGrpc.getDefaultInstance()))
                  .setSchemaDescriptor(new HelloWorldServiceMethodDescriptorSupplier("SelectCount"))
                  .build();
          }
        }
     }
     return getSelectCountMethod;
  }

  private static volatile io.grpc.MethodDescriptor<com.google.protobuf.Empty,
      com.q3d.demo.api.hello.grpc.CountResultGrpc> getSelectCountRemovedMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "SelectCountRemoved",
      requestType = com.google.protobuf.Empty.class,
      responseType = com.q3d.demo.api.hello.grpc.CountResultGrpc.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<com.google.protobuf.Empty,
      com.q3d.demo.api.hello.grpc.CountResultGrpc> getSelectCountRemovedMethod() {
    io.grpc.MethodDescriptor<com.google.protobuf.Empty, com.q3d.demo.api.hello.grpc.CountResultGrpc> getSelectCountRemovedMethod;
    if ((getSelectCountRemovedMethod = HelloWorldServiceGrpc.getSelectCountRemovedMethod) == null) {
      synchronized (HelloWorldServiceGrpc.class) {
        if ((getSelectCountRemovedMethod = HelloWorldServiceGrpc.getSelectCountRemovedMethod) == null) {
          HelloWorldServiceGrpc.getSelectCountRemovedMethod = getSelectCountRemovedMethod = 
              io.grpc.MethodDescriptor.<com.google.protobuf.Empty, com.q3d.demo.api.hello.grpc.CountResultGrpc>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(
                  "HelloWorldService", "SelectCountRemoved"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.google.protobuf.Empty.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.q3d.demo.api.hello.grpc.CountResultGrpc.getDefaultInstance()))
                  .setSchemaDescriptor(new HelloWorldServiceMethodDescriptorSupplier("SelectCountRemoved"))
                  .build();
          }
        }
     }
     return getSelectCountRemovedMethod;
  }

  private static volatile io.grpc.MethodDescriptor<com.q3d.demo.api.hello.grpc.IdExtGrpc,
      com.q3d.demo.api.hello.grpc.HelloWorldGrpcR1K2> getSelectOneMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "SelectOne",
      requestType = com.q3d.demo.api.hello.grpc.IdExtGrpc.class,
      responseType = com.q3d.demo.api.hello.grpc.HelloWorldGrpcR1K2.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<com.q3d.demo.api.hello.grpc.IdExtGrpc,
      com.q3d.demo.api.hello.grpc.HelloWorldGrpcR1K2> getSelectOneMethod() {
    io.grpc.MethodDescriptor<com.q3d.demo.api.hello.grpc.IdExtGrpc, com.q3d.demo.api.hello.grpc.HelloWorldGrpcR1K2> getSelectOneMethod;
    if ((getSelectOneMethod = HelloWorldServiceGrpc.getSelectOneMethod) == null) {
      synchronized (HelloWorldServiceGrpc.class) {
        if ((getSelectOneMethod = HelloWorldServiceGrpc.getSelectOneMethod) == null) {
          HelloWorldServiceGrpc.getSelectOneMethod = getSelectOneMethod = 
              io.grpc.MethodDescriptor.<com.q3d.demo.api.hello.grpc.IdExtGrpc, com.q3d.demo.api.hello.grpc.HelloWorldGrpcR1K2>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(
                  "HelloWorldService", "SelectOne"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.q3d.demo.api.hello.grpc.IdExtGrpc.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.q3d.demo.api.hello.grpc.HelloWorldGrpcR1K2.getDefaultInstance()))
                  .setSchemaDescriptor(new HelloWorldServiceMethodDescriptorSupplier("SelectOne"))
                  .build();
          }
        }
     }
     return getSelectOneMethod;
  }

  private static volatile io.grpc.MethodDescriptor<com.q3d.demo.api.hello.grpc.IdExtListGrpc,
      com.q3d.demo.api.hello.grpc.HelloWorldGrpcR1K2List> getSelectBatchMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "SelectBatch",
      requestType = com.q3d.demo.api.hello.grpc.IdExtListGrpc.class,
      responseType = com.q3d.demo.api.hello.grpc.HelloWorldGrpcR1K2List.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<com.q3d.demo.api.hello.grpc.IdExtListGrpc,
      com.q3d.demo.api.hello.grpc.HelloWorldGrpcR1K2List> getSelectBatchMethod() {
    io.grpc.MethodDescriptor<com.q3d.demo.api.hello.grpc.IdExtListGrpc, com.q3d.demo.api.hello.grpc.HelloWorldGrpcR1K2List> getSelectBatchMethod;
    if ((getSelectBatchMethod = HelloWorldServiceGrpc.getSelectBatchMethod) == null) {
      synchronized (HelloWorldServiceGrpc.class) {
        if ((getSelectBatchMethod = HelloWorldServiceGrpc.getSelectBatchMethod) == null) {
          HelloWorldServiceGrpc.getSelectBatchMethod = getSelectBatchMethod = 
              io.grpc.MethodDescriptor.<com.q3d.demo.api.hello.grpc.IdExtListGrpc, com.q3d.demo.api.hello.grpc.HelloWorldGrpcR1K2List>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(
                  "HelloWorldService", "SelectBatch"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.q3d.demo.api.hello.grpc.IdExtListGrpc.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.q3d.demo.api.hello.grpc.HelloWorldGrpcR1K2List.getDefaultInstance()))
                  .setSchemaDescriptor(new HelloWorldServiceMethodDescriptorSupplier("SelectBatch"))
                  .build();
          }
        }
     }
     return getSelectBatchMethod;
  }

  private static volatile io.grpc.MethodDescriptor<com.q3d.demo.api.hello.grpc.PageRequestGrpc,
      com.q3d.demo.api.hello.grpc.HelloWorldGrpcR1K2List> getSelectByPageMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "SelectByPage",
      requestType = com.q3d.demo.api.hello.grpc.PageRequestGrpc.class,
      responseType = com.q3d.demo.api.hello.grpc.HelloWorldGrpcR1K2List.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<com.q3d.demo.api.hello.grpc.PageRequestGrpc,
      com.q3d.demo.api.hello.grpc.HelloWorldGrpcR1K2List> getSelectByPageMethod() {
    io.grpc.MethodDescriptor<com.q3d.demo.api.hello.grpc.PageRequestGrpc, com.q3d.demo.api.hello.grpc.HelloWorldGrpcR1K2List> getSelectByPageMethod;
    if ((getSelectByPageMethod = HelloWorldServiceGrpc.getSelectByPageMethod) == null) {
      synchronized (HelloWorldServiceGrpc.class) {
        if ((getSelectByPageMethod = HelloWorldServiceGrpc.getSelectByPageMethod) == null) {
          HelloWorldServiceGrpc.getSelectByPageMethod = getSelectByPageMethod = 
              io.grpc.MethodDescriptor.<com.q3d.demo.api.hello.grpc.PageRequestGrpc, com.q3d.demo.api.hello.grpc.HelloWorldGrpcR1K2List>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(
                  "HelloWorldService", "SelectByPage"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.q3d.demo.api.hello.grpc.PageRequestGrpc.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.q3d.demo.api.hello.grpc.HelloWorldGrpcR1K2List.getDefaultInstance()))
                  .setSchemaDescriptor(new HelloWorldServiceMethodDescriptorSupplier("SelectByPage"))
                  .build();
          }
        }
     }
     return getSelectByPageMethod;
  }

  private static volatile io.grpc.MethodDescriptor<com.q3d.demo.api.hello.grpc.PageRequestGrpc,
      com.q3d.demo.api.hello.grpc.IdExtListGrpc> getSelectIdExtByPageMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "SelectIdExtByPage",
      requestType = com.q3d.demo.api.hello.grpc.PageRequestGrpc.class,
      responseType = com.q3d.demo.api.hello.grpc.IdExtListGrpc.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<com.q3d.demo.api.hello.grpc.PageRequestGrpc,
      com.q3d.demo.api.hello.grpc.IdExtListGrpc> getSelectIdExtByPageMethod() {
    io.grpc.MethodDescriptor<com.q3d.demo.api.hello.grpc.PageRequestGrpc, com.q3d.demo.api.hello.grpc.IdExtListGrpc> getSelectIdExtByPageMethod;
    if ((getSelectIdExtByPageMethod = HelloWorldServiceGrpc.getSelectIdExtByPageMethod) == null) {
      synchronized (HelloWorldServiceGrpc.class) {
        if ((getSelectIdExtByPageMethod = HelloWorldServiceGrpc.getSelectIdExtByPageMethod) == null) {
          HelloWorldServiceGrpc.getSelectIdExtByPageMethod = getSelectIdExtByPageMethod = 
              io.grpc.MethodDescriptor.<com.q3d.demo.api.hello.grpc.PageRequestGrpc, com.q3d.demo.api.hello.grpc.IdExtListGrpc>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(
                  "HelloWorldService", "SelectIdExtByPage"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.q3d.demo.api.hello.grpc.PageRequestGrpc.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.q3d.demo.api.hello.grpc.IdExtListGrpc.getDefaultInstance()))
                  .setSchemaDescriptor(new HelloWorldServiceMethodDescriptorSupplier("SelectIdExtByPage"))
                  .build();
          }
        }
     }
     return getSelectIdExtByPageMethod;
  }

  private static volatile io.grpc.MethodDescriptor<com.google.protobuf.Empty,
      com.q3d.demo.api.hello.grpc.IdExtListGrpc> getSelectIdExtRemovedMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "SelectIdExtRemoved",
      requestType = com.google.protobuf.Empty.class,
      responseType = com.q3d.demo.api.hello.grpc.IdExtListGrpc.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<com.google.protobuf.Empty,
      com.q3d.demo.api.hello.grpc.IdExtListGrpc> getSelectIdExtRemovedMethod() {
    io.grpc.MethodDescriptor<com.google.protobuf.Empty, com.q3d.demo.api.hello.grpc.IdExtListGrpc> getSelectIdExtRemovedMethod;
    if ((getSelectIdExtRemovedMethod = HelloWorldServiceGrpc.getSelectIdExtRemovedMethod) == null) {
      synchronized (HelloWorldServiceGrpc.class) {
        if ((getSelectIdExtRemovedMethod = HelloWorldServiceGrpc.getSelectIdExtRemovedMethod) == null) {
          HelloWorldServiceGrpc.getSelectIdExtRemovedMethod = getSelectIdExtRemovedMethod = 
              io.grpc.MethodDescriptor.<com.google.protobuf.Empty, com.q3d.demo.api.hello.grpc.IdExtListGrpc>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(
                  "HelloWorldService", "SelectIdExtRemoved"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.google.protobuf.Empty.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.q3d.demo.api.hello.grpc.IdExtListGrpc.getDefaultInstance()))
                  .setSchemaDescriptor(new HelloWorldServiceMethodDescriptorSupplier("SelectIdExtRemoved"))
                  .build();
          }
        }
     }
     return getSelectIdExtRemovedMethod;
  }

  /**
   * Creates a new async stub that supports all call types for the service
   */
  public static HelloWorldServiceStub newStub(io.grpc.Channel channel) {
    return new HelloWorldServiceStub(channel);
  }

  /**
   * Creates a new blocking-style stub that supports unary and streaming output calls on the service
   */
  public static HelloWorldServiceBlockingStub newBlockingStub(
      io.grpc.Channel channel) {
    return new HelloWorldServiceBlockingStub(channel);
  }

  /**
   * Creates a new ListenableFuture-style stub that supports unary calls on the service
   */
  public static HelloWorldServiceFutureStub newFutureStub(
      io.grpc.Channel channel) {
    return new HelloWorldServiceFutureStub(channel);
  }

  /**
   * <pre>
   * 定义服务
   * 无参数或者无返回值使用(google.protobuf.Empty)
   * </pre>
   */
  public static abstract class HelloWorldServiceImplBase implements io.grpc.BindableService {

    /**
     * <pre>
     **
     * 字符串测试
     * 
     * &#64;param grpcString
     * &#64;return grpcString
     * </pre>
     */
    public void hello(com.q3d.demo.api.hello.grpc.StringGrpc request,
        io.grpc.stub.StreamObserver<com.q3d.demo.api.hello.grpc.StringGrpc> responseObserver) {
      asyncUnimplementedUnaryCall(getHelloMethod(), responseObserver);
    }

    /**
     * <pre>
     **
     * 新增一条记录；
     * 如果字段未被设置，会使用 Model 类的默认值；
     * 如果 Model 默认值为 Null 而数据库字段设置为 Not Null，则报错；
     * 除非在 Mapper 中重写 SQL 语句；
     * 适用于业务调用；
     * 
     * &#64;param grpcP1
     * &#64;return grpcUuid
     * </pre>
     */
    public void addOne(com.q3d.demo.api.hello.grpc.HelloWorldGrpcP1 request,
        io.grpc.stub.StreamObserver<com.q3d.demo.api.hello.grpc.UuidGrpc> responseObserver) {
      asyncUnimplementedUnaryCall(getAddOneMethod(), responseObserver);
    }

    /**
     * <pre>
     **
     * 批量新增记录；
     * 如果字段未被设置，会使用数据库默认值；
     * 除非在 Mapper 中重写 SQL 语句；
     * 适用于业务调用；
     * 
     * &#64;param grpcP1List
     * &#64;return grpcUuidList
     * </pre>
     */
    public void addBatch(com.q3d.demo.api.hello.grpc.HelloWorldGrpcP1List request,
        io.grpc.stub.StreamObserver<com.q3d.demo.api.hello.grpc.UuidListGrpc> responseObserver) {
      asyncUnimplementedUnaryCall(getAddBatchMethod(), responseObserver);
    }

    /**
     * <pre>
     **
     * 按 UUID 删除一条记录(只修改标志位，不真实删除)；
     * 适用于业务调用；
     * 
     * &#64;param grpcUuid
     * &#64;return grpcOpResult
     * </pre>
     */
    public void removeOne(com.q3d.demo.api.hello.grpc.UuidGrpc request,
        io.grpc.stub.StreamObserver<com.q3d.demo.api.hello.grpc.OpResultGrpc> responseObserver) {
      asyncUnimplementedUnaryCall(getRemoveOneMethod(), responseObserver);
    }

    /**
     * <pre>
     **
     * 按 UUID 列表批量删除记录(只修改标志位，不真实删除)；
     * 适用于业务调用；
     * 
     * &#64;param grpcUuidList
     * &#64;return grpcOpResult
     * </pre>
     */
    public void removeBatch(com.q3d.demo.api.hello.grpc.UuidListGrpc request,
        io.grpc.stub.StreamObserver<com.q3d.demo.api.hello.grpc.OpResultGrpc> responseObserver) {
      asyncUnimplementedUnaryCall(getRemoveBatchMethod(), responseObserver);
    }

    /**
     * <pre>
     **
     * 按 ID 彻底删除一条记录(真实删除，找不回来的那种)；
     * 适用于 Admin 调用；
     * 
     * &#64;param grpcIdExt
     * &#64;return grpcOpResult
     * </pre>
     */
    public void deleteOne(com.q3d.demo.api.hello.grpc.IdExtGrpc request,
        io.grpc.stub.StreamObserver<com.q3d.demo.api.hello.grpc.OpResultGrpc> responseObserver) {
      asyncUnimplementedUnaryCall(getDeleteOneMethod(), responseObserver);
    }

    /**
     * <pre>
     **
     * 按 ID 列表批量彻底删除记录(真实删除，找不回来的那种)；
     * 适用于 Admin 调用；
     * 
     * &#64;param grpcIdExtList
     * &#64;return grpcOpResult
     * </pre>
     */
    public void deleteBatch(com.q3d.demo.api.hello.grpc.IdExtListGrpc request,
        io.grpc.stub.StreamObserver<com.q3d.demo.api.hello.grpc.OpResultGrpc> responseObserver) {
      asyncUnimplementedUnaryCall(getDeleteBatchMethod(), responseObserver);
    }

    /**
     * <pre>
     **
     * 按 ID 恢复一条被标记删除的记录；
     * 适用于 Admin 调用；
     * 
     * &#64;param grpcIdExt
     * &#64;return grpcOpResult
     * </pre>
     */
    public void reviveOne(com.q3d.demo.api.hello.grpc.IdExtGrpc request,
        io.grpc.stub.StreamObserver<com.q3d.demo.api.hello.grpc.OpResultGrpc> responseObserver) {
      asyncUnimplementedUnaryCall(getReviveOneMethod(), responseObserver);
    }

    /**
     * <pre>
     **
     * 按 ID 列表批量恢复被标记删除的记录；
     * 适用于 Admin 调用；
     * 
     * &#64;param grpcIdExtList
     * &#64;return grpcOpResult
     * </pre>
     */
    public void reviveBatch(com.q3d.demo.api.hello.grpc.IdExtListGrpc request,
        io.grpc.stub.StreamObserver<com.q3d.demo.api.hello.grpc.OpResultGrpc> responseObserver) {
      asyncUnimplementedUnaryCall(getReviveBatchMethod(), responseObserver);
    }

    /**
     * <pre>
     **
     * 按 UUID 选择性修改一条记录；
     * 只更新 Model 中的非 Null 字段；
     * 适用于业务调用；
     * 
     * &#64;param grpcP1K1
     * &#64;return grpcOpResult
     * </pre>
     */
    public void editSelective(com.q3d.demo.api.hello.grpc.HelloWorldGrpcP1K1 request,
        io.grpc.stub.StreamObserver<com.q3d.demo.api.hello.grpc.OpResultGrpc> responseObserver) {
      asyncUnimplementedUnaryCall(getEditSelectiveMethod(), responseObserver);
    }

    /**
     * <pre>
     **
     * 按 ID 选择性修改一条记录；
     * 只更新 Model 中的非 Null 字段；
     * 适用于 Admin 调用；
     * 
     * &#64;param grpcP1K2
     * &#64;return grpcOpResult
     * </pre>
     */
    public void updateSelective(com.q3d.demo.api.hello.grpc.HelloWorldGrpcP1K2 request,
        io.grpc.stub.StreamObserver<com.q3d.demo.api.hello.grpc.OpResultGrpc> responseObserver) {
      asyncUnimplementedUnaryCall(getUpdateSelectiveMethod(), responseObserver);
    }

    /**
     * <pre>
     **
     * 查询记录总数；
     * 只匹配 active 为 1 的记录；
     * 适用于业务调用；
     * 
     * &#64;return grpcCountResult
     * </pre>
     */
    public void getCount(com.google.protobuf.Empty request,
        io.grpc.stub.StreamObserver<com.q3d.demo.api.hello.grpc.CountResultGrpc> responseObserver) {
      asyncUnimplementedUnaryCall(getGetCountMethod(), responseObserver);
    }

    /**
     * <pre>
     **
     * 按 UUID 查询一条记录；
     * 只匹配 active 为 1 的记录；
     * 适用于业务调用；
     * 
     * &#64;param grpcUuid
     * &#64;return grpcR1K1
     * </pre>
     */
    public void getOne(com.q3d.demo.api.hello.grpc.UuidGrpc request,
        io.grpc.stub.StreamObserver<com.q3d.demo.api.hello.grpc.HelloWorldGrpcR1K1> responseObserver) {
      asyncUnimplementedUnaryCall(getGetOneMethod(), responseObserver);
    }

    /**
     * <pre>
     **
     * 按 UUID 列表批量查询记录；
     * 只匹配 active 为 1 的记录；
     * 适用于业务调用；
     * 
     * &#64;param grpcUuidList
     * &#64;return grpcR1K1List
     * </pre>
     */
    public void getBatch(com.q3d.demo.api.hello.grpc.UuidListGrpc request,
        io.grpc.stub.StreamObserver<com.q3d.demo.api.hello.grpc.HelloWorldGrpcR1K1List> responseObserver) {
      asyncUnimplementedUnaryCall(getGetBatchMethod(), responseObserver);
    }

    /**
     * <pre>
     **
     * 分页查询记录；
     * 只匹配 active 为 1 的记录；
     * 适用于业务调用；
     * 
     * &#64;param grpcPageRequest
     * &#64;return grpcR1K1List
     * </pre>
     */
    public void getByPage(com.q3d.demo.api.hello.grpc.PageRequestGrpc request,
        io.grpc.stub.StreamObserver<com.q3d.demo.api.hello.grpc.HelloWorldGrpcR1K1List> responseObserver) {
      asyncUnimplementedUnaryCall(getGetByPageMethod(), responseObserver);
    }

    /**
     * <pre>
     **
     * 分页查询 UUID 列表；
     * 只匹配 active 为 1 的记录；
     * 适用于业务调用；
     * 
     * &#64;param grpcPageRequest
     * &#64;return grpcUuidList
     * </pre>
     */
    public void getUuidByPage(com.q3d.demo.api.hello.grpc.PageRequestGrpc request,
        io.grpc.stub.StreamObserver<com.q3d.demo.api.hello.grpc.UuidListGrpc> responseObserver) {
      asyncUnimplementedUnaryCall(getGetUuidByPageMethod(), responseObserver);
    }

    /**
     * <pre>
     **
     * 查询记录总数；
     * 即使 active 为 0 也会匹配；
     * 适用于 Admin 调用；
     * 
     * &#64;return grpcCountResult
     * </pre>
     */
    public void selectCount(com.google.protobuf.Empty request,
        io.grpc.stub.StreamObserver<com.q3d.demo.api.hello.grpc.CountResultGrpc> responseObserver) {
      asyncUnimplementedUnaryCall(getSelectCountMethod(), responseObserver);
    }

    /**
     * <pre>
     **
     * 查询被标记删除的记录总数；
     * 即 active 为 0 的记录；
     * 适用于 Admin 调用；
     * 
     * &#64;return grpcCountResult
     * </pre>
     */
    public void selectCountRemoved(com.google.protobuf.Empty request,
        io.grpc.stub.StreamObserver<com.q3d.demo.api.hello.grpc.CountResultGrpc> responseObserver) {
      asyncUnimplementedUnaryCall(getSelectCountRemovedMethod(), responseObserver);
    }

    /**
     * <pre>
     **
     * 按 ID 查询一条记录；
     * 即使 active 为 0 也会匹配；
     * 适用于 Admin 调用；
     * 
     * &#64;param grpcIdExt
     * &#64;return grpcR1K2
     * </pre>
     */
    public void selectOne(com.q3d.demo.api.hello.grpc.IdExtGrpc request,
        io.grpc.stub.StreamObserver<com.q3d.demo.api.hello.grpc.HelloWorldGrpcR1K2> responseObserver) {
      asyncUnimplementedUnaryCall(getSelectOneMethod(), responseObserver);
    }

    /**
     * <pre>
     **
     * 按 ID 列表批量查询记录；
     * 即使 active 为 0 也会匹配；
     * 适用于 Admin 调用；
     * 
     * &#64;param grpcIdExtList
     * &#64;return grpcR1K2List
     * </pre>
     */
    public void selectBatch(com.q3d.demo.api.hello.grpc.IdExtListGrpc request,
        io.grpc.stub.StreamObserver<com.q3d.demo.api.hello.grpc.HelloWorldGrpcR1K2List> responseObserver) {
      asyncUnimplementedUnaryCall(getSelectBatchMethod(), responseObserver);
    }

    /**
     * <pre>
     **
     * 分页查询记录；
     * 即使 active 为 0 也会匹配；
     * 适用于 Admin 调用；
     * 
     * &#64;param grpcPageRequest
     * &#64;return grpcR1K2List
     * </pre>
     */
    public void selectByPage(com.q3d.demo.api.hello.grpc.PageRequestGrpc request,
        io.grpc.stub.StreamObserver<com.q3d.demo.api.hello.grpc.HelloWorldGrpcR1K2List> responseObserver) {
      asyncUnimplementedUnaryCall(getSelectByPageMethod(), responseObserver);
    }

    /**
     * <pre>
     **
     * 分页查询 ID 列表；
     * 即使 active 为 0 也会匹配；
     * 适用于 Admin 调用；
     * 
     * &#64;param grpcPageRequest
     * &#64;return grpcIdExtList
     * </pre>
     */
    public void selectIdExtByPage(com.q3d.demo.api.hello.grpc.PageRequestGrpc request,
        io.grpc.stub.StreamObserver<com.q3d.demo.api.hello.grpc.IdExtListGrpc> responseObserver) {
      asyncUnimplementedUnaryCall(getSelectIdExtByPageMethod(), responseObserver);
    }

    /**
     * <pre>
     **
     * 查询被标记删除的全部记录的 ID 列表；
     * 即 active 为 0 的记录；
     * 适用于 Admin 调用；
     * 
     * &#64;return grpcIdExtList
     * </pre>
     */
    public void selectIdExtRemoved(com.google.protobuf.Empty request,
        io.grpc.stub.StreamObserver<com.q3d.demo.api.hello.grpc.IdExtListGrpc> responseObserver) {
      asyncUnimplementedUnaryCall(getSelectIdExtRemovedMethod(), responseObserver);
    }

    @java.lang.Override public final io.grpc.ServerServiceDefinition bindService() {
      return io.grpc.ServerServiceDefinition.builder(getServiceDescriptor())
          .addMethod(
            getHelloMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                com.q3d.demo.api.hello.grpc.StringGrpc,
                com.q3d.demo.api.hello.grpc.StringGrpc>(
                  this, METHODID_HELLO)))
          .addMethod(
            getAddOneMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                com.q3d.demo.api.hello.grpc.HelloWorldGrpcP1,
                com.q3d.demo.api.hello.grpc.UuidGrpc>(
                  this, METHODID_ADD_ONE)))
          .addMethod(
            getAddBatchMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                com.q3d.demo.api.hello.grpc.HelloWorldGrpcP1List,
                com.q3d.demo.api.hello.grpc.UuidListGrpc>(
                  this, METHODID_ADD_BATCH)))
          .addMethod(
            getRemoveOneMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                com.q3d.demo.api.hello.grpc.UuidGrpc,
                com.q3d.demo.api.hello.grpc.OpResultGrpc>(
                  this, METHODID_REMOVE_ONE)))
          .addMethod(
            getRemoveBatchMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                com.q3d.demo.api.hello.grpc.UuidListGrpc,
                com.q3d.demo.api.hello.grpc.OpResultGrpc>(
                  this, METHODID_REMOVE_BATCH)))
          .addMethod(
            getDeleteOneMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                com.q3d.demo.api.hello.grpc.IdExtGrpc,
                com.q3d.demo.api.hello.grpc.OpResultGrpc>(
                  this, METHODID_DELETE_ONE)))
          .addMethod(
            getDeleteBatchMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                com.q3d.demo.api.hello.grpc.IdExtListGrpc,
                com.q3d.demo.api.hello.grpc.OpResultGrpc>(
                  this, METHODID_DELETE_BATCH)))
          .addMethod(
            getReviveOneMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                com.q3d.demo.api.hello.grpc.IdExtGrpc,
                com.q3d.demo.api.hello.grpc.OpResultGrpc>(
                  this, METHODID_REVIVE_ONE)))
          .addMethod(
            getReviveBatchMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                com.q3d.demo.api.hello.grpc.IdExtListGrpc,
                com.q3d.demo.api.hello.grpc.OpResultGrpc>(
                  this, METHODID_REVIVE_BATCH)))
          .addMethod(
            getEditSelectiveMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                com.q3d.demo.api.hello.grpc.HelloWorldGrpcP1K1,
                com.q3d.demo.api.hello.grpc.OpResultGrpc>(
                  this, METHODID_EDIT_SELECTIVE)))
          .addMethod(
            getUpdateSelectiveMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                com.q3d.demo.api.hello.grpc.HelloWorldGrpcP1K2,
                com.q3d.demo.api.hello.grpc.OpResultGrpc>(
                  this, METHODID_UPDATE_SELECTIVE)))
          .addMethod(
            getGetCountMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                com.google.protobuf.Empty,
                com.q3d.demo.api.hello.grpc.CountResultGrpc>(
                  this, METHODID_GET_COUNT)))
          .addMethod(
            getGetOneMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                com.q3d.demo.api.hello.grpc.UuidGrpc,
                com.q3d.demo.api.hello.grpc.HelloWorldGrpcR1K1>(
                  this, METHODID_GET_ONE)))
          .addMethod(
            getGetBatchMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                com.q3d.demo.api.hello.grpc.UuidListGrpc,
                com.q3d.demo.api.hello.grpc.HelloWorldGrpcR1K1List>(
                  this, METHODID_GET_BATCH)))
          .addMethod(
            getGetByPageMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                com.q3d.demo.api.hello.grpc.PageRequestGrpc,
                com.q3d.demo.api.hello.grpc.HelloWorldGrpcR1K1List>(
                  this, METHODID_GET_BY_PAGE)))
          .addMethod(
            getGetUuidByPageMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                com.q3d.demo.api.hello.grpc.PageRequestGrpc,
                com.q3d.demo.api.hello.grpc.UuidListGrpc>(
                  this, METHODID_GET_UUID_BY_PAGE)))
          .addMethod(
            getSelectCountMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                com.google.protobuf.Empty,
                com.q3d.demo.api.hello.grpc.CountResultGrpc>(
                  this, METHODID_SELECT_COUNT)))
          .addMethod(
            getSelectCountRemovedMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                com.google.protobuf.Empty,
                com.q3d.demo.api.hello.grpc.CountResultGrpc>(
                  this, METHODID_SELECT_COUNT_REMOVED)))
          .addMethod(
            getSelectOneMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                com.q3d.demo.api.hello.grpc.IdExtGrpc,
                com.q3d.demo.api.hello.grpc.HelloWorldGrpcR1K2>(
                  this, METHODID_SELECT_ONE)))
          .addMethod(
            getSelectBatchMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                com.q3d.demo.api.hello.grpc.IdExtListGrpc,
                com.q3d.demo.api.hello.grpc.HelloWorldGrpcR1K2List>(
                  this, METHODID_SELECT_BATCH)))
          .addMethod(
            getSelectByPageMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                com.q3d.demo.api.hello.grpc.PageRequestGrpc,
                com.q3d.demo.api.hello.grpc.HelloWorldGrpcR1K2List>(
                  this, METHODID_SELECT_BY_PAGE)))
          .addMethod(
            getSelectIdExtByPageMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                com.q3d.demo.api.hello.grpc.PageRequestGrpc,
                com.q3d.demo.api.hello.grpc.IdExtListGrpc>(
                  this, METHODID_SELECT_ID_EXT_BY_PAGE)))
          .addMethod(
            getSelectIdExtRemovedMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                com.google.protobuf.Empty,
                com.q3d.demo.api.hello.grpc.IdExtListGrpc>(
                  this, METHODID_SELECT_ID_EXT_REMOVED)))
          .build();
    }
  }

  /**
   * <pre>
   * 定义服务
   * 无参数或者无返回值使用(google.protobuf.Empty)
   * </pre>
   */
  public static final class HelloWorldServiceStub extends io.grpc.stub.AbstractStub<HelloWorldServiceStub> {
    private HelloWorldServiceStub(io.grpc.Channel channel) {
      super(channel);
    }

    private HelloWorldServiceStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected HelloWorldServiceStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new HelloWorldServiceStub(channel, callOptions);
    }

    /**
     * <pre>
     **
     * 字符串测试
     * 
     * &#64;param grpcString
     * &#64;return grpcString
     * </pre>
     */
    public void hello(com.q3d.demo.api.hello.grpc.StringGrpc request,
        io.grpc.stub.StreamObserver<com.q3d.demo.api.hello.grpc.StringGrpc> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getHelloMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     * <pre>
     **
     * 新增一条记录；
     * 如果字段未被设置，会使用 Model 类的默认值；
     * 如果 Model 默认值为 Null 而数据库字段设置为 Not Null，则报错；
     * 除非在 Mapper 中重写 SQL 语句；
     * 适用于业务调用；
     * 
     * &#64;param grpcP1
     * &#64;return grpcUuid
     * </pre>
     */
    public void addOne(com.q3d.demo.api.hello.grpc.HelloWorldGrpcP1 request,
        io.grpc.stub.StreamObserver<com.q3d.demo.api.hello.grpc.UuidGrpc> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getAddOneMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     * <pre>
     **
     * 批量新增记录；
     * 如果字段未被设置，会使用数据库默认值；
     * 除非在 Mapper 中重写 SQL 语句；
     * 适用于业务调用；
     * 
     * &#64;param grpcP1List
     * &#64;return grpcUuidList
     * </pre>
     */
    public void addBatch(com.q3d.demo.api.hello.grpc.HelloWorldGrpcP1List request,
        io.grpc.stub.StreamObserver<com.q3d.demo.api.hello.grpc.UuidListGrpc> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getAddBatchMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     * <pre>
     **
     * 按 UUID 删除一条记录(只修改标志位，不真实删除)；
     * 适用于业务调用；
     * 
     * &#64;param grpcUuid
     * &#64;return grpcOpResult
     * </pre>
     */
    public void removeOne(com.q3d.demo.api.hello.grpc.UuidGrpc request,
        io.grpc.stub.StreamObserver<com.q3d.demo.api.hello.grpc.OpResultGrpc> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getRemoveOneMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     * <pre>
     **
     * 按 UUID 列表批量删除记录(只修改标志位，不真实删除)；
     * 适用于业务调用；
     * 
     * &#64;param grpcUuidList
     * &#64;return grpcOpResult
     * </pre>
     */
    public void removeBatch(com.q3d.demo.api.hello.grpc.UuidListGrpc request,
        io.grpc.stub.StreamObserver<com.q3d.demo.api.hello.grpc.OpResultGrpc> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getRemoveBatchMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     * <pre>
     **
     * 按 ID 彻底删除一条记录(真实删除，找不回来的那种)；
     * 适用于 Admin 调用；
     * 
     * &#64;param grpcIdExt
     * &#64;return grpcOpResult
     * </pre>
     */
    public void deleteOne(com.q3d.demo.api.hello.grpc.IdExtGrpc request,
        io.grpc.stub.StreamObserver<com.q3d.demo.api.hello.grpc.OpResultGrpc> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getDeleteOneMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     * <pre>
     **
     * 按 ID 列表批量彻底删除记录(真实删除，找不回来的那种)；
     * 适用于 Admin 调用；
     * 
     * &#64;param grpcIdExtList
     * &#64;return grpcOpResult
     * </pre>
     */
    public void deleteBatch(com.q3d.demo.api.hello.grpc.IdExtListGrpc request,
        io.grpc.stub.StreamObserver<com.q3d.demo.api.hello.grpc.OpResultGrpc> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getDeleteBatchMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     * <pre>
     **
     * 按 ID 恢复一条被标记删除的记录；
     * 适用于 Admin 调用；
     * 
     * &#64;param grpcIdExt
     * &#64;return grpcOpResult
     * </pre>
     */
    public void reviveOne(com.q3d.demo.api.hello.grpc.IdExtGrpc request,
        io.grpc.stub.StreamObserver<com.q3d.demo.api.hello.grpc.OpResultGrpc> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getReviveOneMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     * <pre>
     **
     * 按 ID 列表批量恢复被标记删除的记录；
     * 适用于 Admin 调用；
     * 
     * &#64;param grpcIdExtList
     * &#64;return grpcOpResult
     * </pre>
     */
    public void reviveBatch(com.q3d.demo.api.hello.grpc.IdExtListGrpc request,
        io.grpc.stub.StreamObserver<com.q3d.demo.api.hello.grpc.OpResultGrpc> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getReviveBatchMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     * <pre>
     **
     * 按 UUID 选择性修改一条记录；
     * 只更新 Model 中的非 Null 字段；
     * 适用于业务调用；
     * 
     * &#64;param grpcP1K1
     * &#64;return grpcOpResult
     * </pre>
     */
    public void editSelective(com.q3d.demo.api.hello.grpc.HelloWorldGrpcP1K1 request,
        io.grpc.stub.StreamObserver<com.q3d.demo.api.hello.grpc.OpResultGrpc> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getEditSelectiveMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     * <pre>
     **
     * 按 ID 选择性修改一条记录；
     * 只更新 Model 中的非 Null 字段；
     * 适用于 Admin 调用；
     * 
     * &#64;param grpcP1K2
     * &#64;return grpcOpResult
     * </pre>
     */
    public void updateSelective(com.q3d.demo.api.hello.grpc.HelloWorldGrpcP1K2 request,
        io.grpc.stub.StreamObserver<com.q3d.demo.api.hello.grpc.OpResultGrpc> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getUpdateSelectiveMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     * <pre>
     **
     * 查询记录总数；
     * 只匹配 active 为 1 的记录；
     * 适用于业务调用；
     * 
     * &#64;return grpcCountResult
     * </pre>
     */
    public void getCount(com.google.protobuf.Empty request,
        io.grpc.stub.StreamObserver<com.q3d.demo.api.hello.grpc.CountResultGrpc> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getGetCountMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     * <pre>
     **
     * 按 UUID 查询一条记录；
     * 只匹配 active 为 1 的记录；
     * 适用于业务调用；
     * 
     * &#64;param grpcUuid
     * &#64;return grpcR1K1
     * </pre>
     */
    public void getOne(com.q3d.demo.api.hello.grpc.UuidGrpc request,
        io.grpc.stub.StreamObserver<com.q3d.demo.api.hello.grpc.HelloWorldGrpcR1K1> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getGetOneMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     * <pre>
     **
     * 按 UUID 列表批量查询记录；
     * 只匹配 active 为 1 的记录；
     * 适用于业务调用；
     * 
     * &#64;param grpcUuidList
     * &#64;return grpcR1K1List
     * </pre>
     */
    public void getBatch(com.q3d.demo.api.hello.grpc.UuidListGrpc request,
        io.grpc.stub.StreamObserver<com.q3d.demo.api.hello.grpc.HelloWorldGrpcR1K1List> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getGetBatchMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     * <pre>
     **
     * 分页查询记录；
     * 只匹配 active 为 1 的记录；
     * 适用于业务调用；
     * 
     * &#64;param grpcPageRequest
     * &#64;return grpcR1K1List
     * </pre>
     */
    public void getByPage(com.q3d.demo.api.hello.grpc.PageRequestGrpc request,
        io.grpc.stub.StreamObserver<com.q3d.demo.api.hello.grpc.HelloWorldGrpcR1K1List> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getGetByPageMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     * <pre>
     **
     * 分页查询 UUID 列表；
     * 只匹配 active 为 1 的记录；
     * 适用于业务调用；
     * 
     * &#64;param grpcPageRequest
     * &#64;return grpcUuidList
     * </pre>
     */
    public void getUuidByPage(com.q3d.demo.api.hello.grpc.PageRequestGrpc request,
        io.grpc.stub.StreamObserver<com.q3d.demo.api.hello.grpc.UuidListGrpc> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getGetUuidByPageMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     * <pre>
     **
     * 查询记录总数；
     * 即使 active 为 0 也会匹配；
     * 适用于 Admin 调用；
     * 
     * &#64;return grpcCountResult
     * </pre>
     */
    public void selectCount(com.google.protobuf.Empty request,
        io.grpc.stub.StreamObserver<com.q3d.demo.api.hello.grpc.CountResultGrpc> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getSelectCountMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     * <pre>
     **
     * 查询被标记删除的记录总数；
     * 即 active 为 0 的记录；
     * 适用于 Admin 调用；
     * 
     * &#64;return grpcCountResult
     * </pre>
     */
    public void selectCountRemoved(com.google.protobuf.Empty request,
        io.grpc.stub.StreamObserver<com.q3d.demo.api.hello.grpc.CountResultGrpc> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getSelectCountRemovedMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     * <pre>
     **
     * 按 ID 查询一条记录；
     * 即使 active 为 0 也会匹配；
     * 适用于 Admin 调用；
     * 
     * &#64;param grpcIdExt
     * &#64;return grpcR1K2
     * </pre>
     */
    public void selectOne(com.q3d.demo.api.hello.grpc.IdExtGrpc request,
        io.grpc.stub.StreamObserver<com.q3d.demo.api.hello.grpc.HelloWorldGrpcR1K2> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getSelectOneMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     * <pre>
     **
     * 按 ID 列表批量查询记录；
     * 即使 active 为 0 也会匹配；
     * 适用于 Admin 调用；
     * 
     * &#64;param grpcIdExtList
     * &#64;return grpcR1K2List
     * </pre>
     */
    public void selectBatch(com.q3d.demo.api.hello.grpc.IdExtListGrpc request,
        io.grpc.stub.StreamObserver<com.q3d.demo.api.hello.grpc.HelloWorldGrpcR1K2List> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getSelectBatchMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     * <pre>
     **
     * 分页查询记录；
     * 即使 active 为 0 也会匹配；
     * 适用于 Admin 调用；
     * 
     * &#64;param grpcPageRequest
     * &#64;return grpcR1K2List
     * </pre>
     */
    public void selectByPage(com.q3d.demo.api.hello.grpc.PageRequestGrpc request,
        io.grpc.stub.StreamObserver<com.q3d.demo.api.hello.grpc.HelloWorldGrpcR1K2List> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getSelectByPageMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     * <pre>
     **
     * 分页查询 ID 列表；
     * 即使 active 为 0 也会匹配；
     * 适用于 Admin 调用；
     * 
     * &#64;param grpcPageRequest
     * &#64;return grpcIdExtList
     * </pre>
     */
    public void selectIdExtByPage(com.q3d.demo.api.hello.grpc.PageRequestGrpc request,
        io.grpc.stub.StreamObserver<com.q3d.demo.api.hello.grpc.IdExtListGrpc> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getSelectIdExtByPageMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     * <pre>
     **
     * 查询被标记删除的全部记录的 ID 列表；
     * 即 active 为 0 的记录；
     * 适用于 Admin 调用；
     * 
     * &#64;return grpcIdExtList
     * </pre>
     */
    public void selectIdExtRemoved(com.google.protobuf.Empty request,
        io.grpc.stub.StreamObserver<com.q3d.demo.api.hello.grpc.IdExtListGrpc> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getSelectIdExtRemovedMethod(), getCallOptions()), request, responseObserver);
    }
  }

  /**
   * <pre>
   * 定义服务
   * 无参数或者无返回值使用(google.protobuf.Empty)
   * </pre>
   */
  public static final class HelloWorldServiceBlockingStub extends io.grpc.stub.AbstractStub<HelloWorldServiceBlockingStub> {
    private HelloWorldServiceBlockingStub(io.grpc.Channel channel) {
      super(channel);
    }

    private HelloWorldServiceBlockingStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected HelloWorldServiceBlockingStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new HelloWorldServiceBlockingStub(channel, callOptions);
    }

    /**
     * <pre>
     **
     * 字符串测试
     * 
     * &#64;param grpcString
     * &#64;return grpcString
     * </pre>
     */
    public com.q3d.demo.api.hello.grpc.StringGrpc hello(com.q3d.demo.api.hello.grpc.StringGrpc request) {
      return blockingUnaryCall(
          getChannel(), getHelloMethod(), getCallOptions(), request);
    }

    /**
     * <pre>
     **
     * 新增一条记录；
     * 如果字段未被设置，会使用 Model 类的默认值；
     * 如果 Model 默认值为 Null 而数据库字段设置为 Not Null，则报错；
     * 除非在 Mapper 中重写 SQL 语句；
     * 适用于业务调用；
     * 
     * &#64;param grpcP1
     * &#64;return grpcUuid
     * </pre>
     */
    public com.q3d.demo.api.hello.grpc.UuidGrpc addOne(com.q3d.demo.api.hello.grpc.HelloWorldGrpcP1 request) {
      return blockingUnaryCall(
          getChannel(), getAddOneMethod(), getCallOptions(), request);
    }

    /**
     * <pre>
     **
     * 批量新增记录；
     * 如果字段未被设置，会使用数据库默认值；
     * 除非在 Mapper 中重写 SQL 语句；
     * 适用于业务调用；
     * 
     * &#64;param grpcP1List
     * &#64;return grpcUuidList
     * </pre>
     */
    public com.q3d.demo.api.hello.grpc.UuidListGrpc addBatch(com.q3d.demo.api.hello.grpc.HelloWorldGrpcP1List request) {
      return blockingUnaryCall(
          getChannel(), getAddBatchMethod(), getCallOptions(), request);
    }

    /**
     * <pre>
     **
     * 按 UUID 删除一条记录(只修改标志位，不真实删除)；
     * 适用于业务调用；
     * 
     * &#64;param grpcUuid
     * &#64;return grpcOpResult
     * </pre>
     */
    public com.q3d.demo.api.hello.grpc.OpResultGrpc removeOne(com.q3d.demo.api.hello.grpc.UuidGrpc request) {
      return blockingUnaryCall(
          getChannel(), getRemoveOneMethod(), getCallOptions(), request);
    }

    /**
     * <pre>
     **
     * 按 UUID 列表批量删除记录(只修改标志位，不真实删除)；
     * 适用于业务调用；
     * 
     * &#64;param grpcUuidList
     * &#64;return grpcOpResult
     * </pre>
     */
    public com.q3d.demo.api.hello.grpc.OpResultGrpc removeBatch(com.q3d.demo.api.hello.grpc.UuidListGrpc request) {
      return blockingUnaryCall(
          getChannel(), getRemoveBatchMethod(), getCallOptions(), request);
    }

    /**
     * <pre>
     **
     * 按 ID 彻底删除一条记录(真实删除，找不回来的那种)；
     * 适用于 Admin 调用；
     * 
     * &#64;param grpcIdExt
     * &#64;return grpcOpResult
     * </pre>
     */
    public com.q3d.demo.api.hello.grpc.OpResultGrpc deleteOne(com.q3d.demo.api.hello.grpc.IdExtGrpc request) {
      return blockingUnaryCall(
          getChannel(), getDeleteOneMethod(), getCallOptions(), request);
    }

    /**
     * <pre>
     **
     * 按 ID 列表批量彻底删除记录(真实删除，找不回来的那种)；
     * 适用于 Admin 调用；
     * 
     * &#64;param grpcIdExtList
     * &#64;return grpcOpResult
     * </pre>
     */
    public com.q3d.demo.api.hello.grpc.OpResultGrpc deleteBatch(com.q3d.demo.api.hello.grpc.IdExtListGrpc request) {
      return blockingUnaryCall(
          getChannel(), getDeleteBatchMethod(), getCallOptions(), request);
    }

    /**
     * <pre>
     **
     * 按 ID 恢复一条被标记删除的记录；
     * 适用于 Admin 调用；
     * 
     * &#64;param grpcIdExt
     * &#64;return grpcOpResult
     * </pre>
     */
    public com.q3d.demo.api.hello.grpc.OpResultGrpc reviveOne(com.q3d.demo.api.hello.grpc.IdExtGrpc request) {
      return blockingUnaryCall(
          getChannel(), getReviveOneMethod(), getCallOptions(), request);
    }

    /**
     * <pre>
     **
     * 按 ID 列表批量恢复被标记删除的记录；
     * 适用于 Admin 调用；
     * 
     * &#64;param grpcIdExtList
     * &#64;return grpcOpResult
     * </pre>
     */
    public com.q3d.demo.api.hello.grpc.OpResultGrpc reviveBatch(com.q3d.demo.api.hello.grpc.IdExtListGrpc request) {
      return blockingUnaryCall(
          getChannel(), getReviveBatchMethod(), getCallOptions(), request);
    }

    /**
     * <pre>
     **
     * 按 UUID 选择性修改一条记录；
     * 只更新 Model 中的非 Null 字段；
     * 适用于业务调用；
     * 
     * &#64;param grpcP1K1
     * &#64;return grpcOpResult
     * </pre>
     */
    public com.q3d.demo.api.hello.grpc.OpResultGrpc editSelective(com.q3d.demo.api.hello.grpc.HelloWorldGrpcP1K1 request) {
      return blockingUnaryCall(
          getChannel(), getEditSelectiveMethod(), getCallOptions(), request);
    }

    /**
     * <pre>
     **
     * 按 ID 选择性修改一条记录；
     * 只更新 Model 中的非 Null 字段；
     * 适用于 Admin 调用；
     * 
     * &#64;param grpcP1K2
     * &#64;return grpcOpResult
     * </pre>
     */
    public com.q3d.demo.api.hello.grpc.OpResultGrpc updateSelective(com.q3d.demo.api.hello.grpc.HelloWorldGrpcP1K2 request) {
      return blockingUnaryCall(
          getChannel(), getUpdateSelectiveMethod(), getCallOptions(), request);
    }

    /**
     * <pre>
     **
     * 查询记录总数；
     * 只匹配 active 为 1 的记录；
     * 适用于业务调用；
     * 
     * &#64;return grpcCountResult
     * </pre>
     */
    public com.q3d.demo.api.hello.grpc.CountResultGrpc getCount(com.google.protobuf.Empty request) {
      return blockingUnaryCall(
          getChannel(), getGetCountMethod(), getCallOptions(), request);
    }

    /**
     * <pre>
     **
     * 按 UUID 查询一条记录；
     * 只匹配 active 为 1 的记录；
     * 适用于业务调用；
     * 
     * &#64;param grpcUuid
     * &#64;return grpcR1K1
     * </pre>
     */
    public com.q3d.demo.api.hello.grpc.HelloWorldGrpcR1K1 getOne(com.q3d.demo.api.hello.grpc.UuidGrpc request) {
      return blockingUnaryCall(
          getChannel(), getGetOneMethod(), getCallOptions(), request);
    }

    /**
     * <pre>
     **
     * 按 UUID 列表批量查询记录；
     * 只匹配 active 为 1 的记录；
     * 适用于业务调用；
     * 
     * &#64;param grpcUuidList
     * &#64;return grpcR1K1List
     * </pre>
     */
    public com.q3d.demo.api.hello.grpc.HelloWorldGrpcR1K1List getBatch(com.q3d.demo.api.hello.grpc.UuidListGrpc request) {
      return blockingUnaryCall(
          getChannel(), getGetBatchMethod(), getCallOptions(), request);
    }

    /**
     * <pre>
     **
     * 分页查询记录；
     * 只匹配 active 为 1 的记录；
     * 适用于业务调用；
     * 
     * &#64;param grpcPageRequest
     * &#64;return grpcR1K1List
     * </pre>
     */
    public com.q3d.demo.api.hello.grpc.HelloWorldGrpcR1K1List getByPage(com.q3d.demo.api.hello.grpc.PageRequestGrpc request) {
      return blockingUnaryCall(
          getChannel(), getGetByPageMethod(), getCallOptions(), request);
    }

    /**
     * <pre>
     **
     * 分页查询 UUID 列表；
     * 只匹配 active 为 1 的记录；
     * 适用于业务调用；
     * 
     * &#64;param grpcPageRequest
     * &#64;return grpcUuidList
     * </pre>
     */
    public com.q3d.demo.api.hello.grpc.UuidListGrpc getUuidByPage(com.q3d.demo.api.hello.grpc.PageRequestGrpc request) {
      return blockingUnaryCall(
          getChannel(), getGetUuidByPageMethod(), getCallOptions(), request);
    }

    /**
     * <pre>
     **
     * 查询记录总数；
     * 即使 active 为 0 也会匹配；
     * 适用于 Admin 调用；
     * 
     * &#64;return grpcCountResult
     * </pre>
     */
    public com.q3d.demo.api.hello.grpc.CountResultGrpc selectCount(com.google.protobuf.Empty request) {
      return blockingUnaryCall(
          getChannel(), getSelectCountMethod(), getCallOptions(), request);
    }

    /**
     * <pre>
     **
     * 查询被标记删除的记录总数；
     * 即 active 为 0 的记录；
     * 适用于 Admin 调用；
     * 
     * &#64;return grpcCountResult
     * </pre>
     */
    public com.q3d.demo.api.hello.grpc.CountResultGrpc selectCountRemoved(com.google.protobuf.Empty request) {
      return blockingUnaryCall(
          getChannel(), getSelectCountRemovedMethod(), getCallOptions(), request);
    }

    /**
     * <pre>
     **
     * 按 ID 查询一条记录；
     * 即使 active 为 0 也会匹配；
     * 适用于 Admin 调用；
     * 
     * &#64;param grpcIdExt
     * &#64;return grpcR1K2
     * </pre>
     */
    public com.q3d.demo.api.hello.grpc.HelloWorldGrpcR1K2 selectOne(com.q3d.demo.api.hello.grpc.IdExtGrpc request) {
      return blockingUnaryCall(
          getChannel(), getSelectOneMethod(), getCallOptions(), request);
    }

    /**
     * <pre>
     **
     * 按 ID 列表批量查询记录；
     * 即使 active 为 0 也会匹配；
     * 适用于 Admin 调用；
     * 
     * &#64;param grpcIdExtList
     * &#64;return grpcR1K2List
     * </pre>
     */
    public com.q3d.demo.api.hello.grpc.HelloWorldGrpcR1K2List selectBatch(com.q3d.demo.api.hello.grpc.IdExtListGrpc request) {
      return blockingUnaryCall(
          getChannel(), getSelectBatchMethod(), getCallOptions(), request);
    }

    /**
     * <pre>
     **
     * 分页查询记录；
     * 即使 active 为 0 也会匹配；
     * 适用于 Admin 调用；
     * 
     * &#64;param grpcPageRequest
     * &#64;return grpcR1K2List
     * </pre>
     */
    public com.q3d.demo.api.hello.grpc.HelloWorldGrpcR1K2List selectByPage(com.q3d.demo.api.hello.grpc.PageRequestGrpc request) {
      return blockingUnaryCall(
          getChannel(), getSelectByPageMethod(), getCallOptions(), request);
    }

    /**
     * <pre>
     **
     * 分页查询 ID 列表；
     * 即使 active 为 0 也会匹配；
     * 适用于 Admin 调用；
     * 
     * &#64;param grpcPageRequest
     * &#64;return grpcIdExtList
     * </pre>
     */
    public com.q3d.demo.api.hello.grpc.IdExtListGrpc selectIdExtByPage(com.q3d.demo.api.hello.grpc.PageRequestGrpc request) {
      return blockingUnaryCall(
          getChannel(), getSelectIdExtByPageMethod(), getCallOptions(), request);
    }

    /**
     * <pre>
     **
     * 查询被标记删除的全部记录的 ID 列表；
     * 即 active 为 0 的记录；
     * 适用于 Admin 调用；
     * 
     * &#64;return grpcIdExtList
     * </pre>
     */
    public com.q3d.demo.api.hello.grpc.IdExtListGrpc selectIdExtRemoved(com.google.protobuf.Empty request) {
      return blockingUnaryCall(
          getChannel(), getSelectIdExtRemovedMethod(), getCallOptions(), request);
    }
  }

  /**
   * <pre>
   * 定义服务
   * 无参数或者无返回值使用(google.protobuf.Empty)
   * </pre>
   */
  public static final class HelloWorldServiceFutureStub extends io.grpc.stub.AbstractStub<HelloWorldServiceFutureStub> {
    private HelloWorldServiceFutureStub(io.grpc.Channel channel) {
      super(channel);
    }

    private HelloWorldServiceFutureStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected HelloWorldServiceFutureStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new HelloWorldServiceFutureStub(channel, callOptions);
    }

    /**
     * <pre>
     **
     * 字符串测试
     * 
     * &#64;param grpcString
     * &#64;return grpcString
     * </pre>
     */
    public com.google.common.util.concurrent.ListenableFuture<com.q3d.demo.api.hello.grpc.StringGrpc> hello(
        com.q3d.demo.api.hello.grpc.StringGrpc request) {
      return futureUnaryCall(
          getChannel().newCall(getHelloMethod(), getCallOptions()), request);
    }

    /**
     * <pre>
     **
     * 新增一条记录；
     * 如果字段未被设置，会使用 Model 类的默认值；
     * 如果 Model 默认值为 Null 而数据库字段设置为 Not Null，则报错；
     * 除非在 Mapper 中重写 SQL 语句；
     * 适用于业务调用；
     * 
     * &#64;param grpcP1
     * &#64;return grpcUuid
     * </pre>
     */
    public com.google.common.util.concurrent.ListenableFuture<com.q3d.demo.api.hello.grpc.UuidGrpc> addOne(
        com.q3d.demo.api.hello.grpc.HelloWorldGrpcP1 request) {
      return futureUnaryCall(
          getChannel().newCall(getAddOneMethod(), getCallOptions()), request);
    }

    /**
     * <pre>
     **
     * 批量新增记录；
     * 如果字段未被设置，会使用数据库默认值；
     * 除非在 Mapper 中重写 SQL 语句；
     * 适用于业务调用；
     * 
     * &#64;param grpcP1List
     * &#64;return grpcUuidList
     * </pre>
     */
    public com.google.common.util.concurrent.ListenableFuture<com.q3d.demo.api.hello.grpc.UuidListGrpc> addBatch(
        com.q3d.demo.api.hello.grpc.HelloWorldGrpcP1List request) {
      return futureUnaryCall(
          getChannel().newCall(getAddBatchMethod(), getCallOptions()), request);
    }

    /**
     * <pre>
     **
     * 按 UUID 删除一条记录(只修改标志位，不真实删除)；
     * 适用于业务调用；
     * 
     * &#64;param grpcUuid
     * &#64;return grpcOpResult
     * </pre>
     */
    public com.google.common.util.concurrent.ListenableFuture<com.q3d.demo.api.hello.grpc.OpResultGrpc> removeOne(
        com.q3d.demo.api.hello.grpc.UuidGrpc request) {
      return futureUnaryCall(
          getChannel().newCall(getRemoveOneMethod(), getCallOptions()), request);
    }

    /**
     * <pre>
     **
     * 按 UUID 列表批量删除记录(只修改标志位，不真实删除)；
     * 适用于业务调用；
     * 
     * &#64;param grpcUuidList
     * &#64;return grpcOpResult
     * </pre>
     */
    public com.google.common.util.concurrent.ListenableFuture<com.q3d.demo.api.hello.grpc.OpResultGrpc> removeBatch(
        com.q3d.demo.api.hello.grpc.UuidListGrpc request) {
      return futureUnaryCall(
          getChannel().newCall(getRemoveBatchMethod(), getCallOptions()), request);
    }

    /**
     * <pre>
     **
     * 按 ID 彻底删除一条记录(真实删除，找不回来的那种)；
     * 适用于 Admin 调用；
     * 
     * &#64;param grpcIdExt
     * &#64;return grpcOpResult
     * </pre>
     */
    public com.google.common.util.concurrent.ListenableFuture<com.q3d.demo.api.hello.grpc.OpResultGrpc> deleteOne(
        com.q3d.demo.api.hello.grpc.IdExtGrpc request) {
      return futureUnaryCall(
          getChannel().newCall(getDeleteOneMethod(), getCallOptions()), request);
    }

    /**
     * <pre>
     **
     * 按 ID 列表批量彻底删除记录(真实删除，找不回来的那种)；
     * 适用于 Admin 调用；
     * 
     * &#64;param grpcIdExtList
     * &#64;return grpcOpResult
     * </pre>
     */
    public com.google.common.util.concurrent.ListenableFuture<com.q3d.demo.api.hello.grpc.OpResultGrpc> deleteBatch(
        com.q3d.demo.api.hello.grpc.IdExtListGrpc request) {
      return futureUnaryCall(
          getChannel().newCall(getDeleteBatchMethod(), getCallOptions()), request);
    }

    /**
     * <pre>
     **
     * 按 ID 恢复一条被标记删除的记录；
     * 适用于 Admin 调用；
     * 
     * &#64;param grpcIdExt
     * &#64;return grpcOpResult
     * </pre>
     */
    public com.google.common.util.concurrent.ListenableFuture<com.q3d.demo.api.hello.grpc.OpResultGrpc> reviveOne(
        com.q3d.demo.api.hello.grpc.IdExtGrpc request) {
      return futureUnaryCall(
          getChannel().newCall(getReviveOneMethod(), getCallOptions()), request);
    }

    /**
     * <pre>
     **
     * 按 ID 列表批量恢复被标记删除的记录；
     * 适用于 Admin 调用；
     * 
     * &#64;param grpcIdExtList
     * &#64;return grpcOpResult
     * </pre>
     */
    public com.google.common.util.concurrent.ListenableFuture<com.q3d.demo.api.hello.grpc.OpResultGrpc> reviveBatch(
        com.q3d.demo.api.hello.grpc.IdExtListGrpc request) {
      return futureUnaryCall(
          getChannel().newCall(getReviveBatchMethod(), getCallOptions()), request);
    }

    /**
     * <pre>
     **
     * 按 UUID 选择性修改一条记录；
     * 只更新 Model 中的非 Null 字段；
     * 适用于业务调用；
     * 
     * &#64;param grpcP1K1
     * &#64;return grpcOpResult
     * </pre>
     */
    public com.google.common.util.concurrent.ListenableFuture<com.q3d.demo.api.hello.grpc.OpResultGrpc> editSelective(
        com.q3d.demo.api.hello.grpc.HelloWorldGrpcP1K1 request) {
      return futureUnaryCall(
          getChannel().newCall(getEditSelectiveMethod(), getCallOptions()), request);
    }

    /**
     * <pre>
     **
     * 按 ID 选择性修改一条记录；
     * 只更新 Model 中的非 Null 字段；
     * 适用于 Admin 调用；
     * 
     * &#64;param grpcP1K2
     * &#64;return grpcOpResult
     * </pre>
     */
    public com.google.common.util.concurrent.ListenableFuture<com.q3d.demo.api.hello.grpc.OpResultGrpc> updateSelective(
        com.q3d.demo.api.hello.grpc.HelloWorldGrpcP1K2 request) {
      return futureUnaryCall(
          getChannel().newCall(getUpdateSelectiveMethod(), getCallOptions()), request);
    }

    /**
     * <pre>
     **
     * 查询记录总数；
     * 只匹配 active 为 1 的记录；
     * 适用于业务调用；
     * 
     * &#64;return grpcCountResult
     * </pre>
     */
    public com.google.common.util.concurrent.ListenableFuture<com.q3d.demo.api.hello.grpc.CountResultGrpc> getCount(
        com.google.protobuf.Empty request) {
      return futureUnaryCall(
          getChannel().newCall(getGetCountMethod(), getCallOptions()), request);
    }

    /**
     * <pre>
     **
     * 按 UUID 查询一条记录；
     * 只匹配 active 为 1 的记录；
     * 适用于业务调用；
     * 
     * &#64;param grpcUuid
     * &#64;return grpcR1K1
     * </pre>
     */
    public com.google.common.util.concurrent.ListenableFuture<com.q3d.demo.api.hello.grpc.HelloWorldGrpcR1K1> getOne(
        com.q3d.demo.api.hello.grpc.UuidGrpc request) {
      return futureUnaryCall(
          getChannel().newCall(getGetOneMethod(), getCallOptions()), request);
    }

    /**
     * <pre>
     **
     * 按 UUID 列表批量查询记录；
     * 只匹配 active 为 1 的记录；
     * 适用于业务调用；
     * 
     * &#64;param grpcUuidList
     * &#64;return grpcR1K1List
     * </pre>
     */
    public com.google.common.util.concurrent.ListenableFuture<com.q3d.demo.api.hello.grpc.HelloWorldGrpcR1K1List> getBatch(
        com.q3d.demo.api.hello.grpc.UuidListGrpc request) {
      return futureUnaryCall(
          getChannel().newCall(getGetBatchMethod(), getCallOptions()), request);
    }

    /**
     * <pre>
     **
     * 分页查询记录；
     * 只匹配 active 为 1 的记录；
     * 适用于业务调用；
     * 
     * &#64;param grpcPageRequest
     * &#64;return grpcR1K1List
     * </pre>
     */
    public com.google.common.util.concurrent.ListenableFuture<com.q3d.demo.api.hello.grpc.HelloWorldGrpcR1K1List> getByPage(
        com.q3d.demo.api.hello.grpc.PageRequestGrpc request) {
      return futureUnaryCall(
          getChannel().newCall(getGetByPageMethod(), getCallOptions()), request);
    }

    /**
     * <pre>
     **
     * 分页查询 UUID 列表；
     * 只匹配 active 为 1 的记录；
     * 适用于业务调用；
     * 
     * &#64;param grpcPageRequest
     * &#64;return grpcUuidList
     * </pre>
     */
    public com.google.common.util.concurrent.ListenableFuture<com.q3d.demo.api.hello.grpc.UuidListGrpc> getUuidByPage(
        com.q3d.demo.api.hello.grpc.PageRequestGrpc request) {
      return futureUnaryCall(
          getChannel().newCall(getGetUuidByPageMethod(), getCallOptions()), request);
    }

    /**
     * <pre>
     **
     * 查询记录总数；
     * 即使 active 为 0 也会匹配；
     * 适用于 Admin 调用；
     * 
     * &#64;return grpcCountResult
     * </pre>
     */
    public com.google.common.util.concurrent.ListenableFuture<com.q3d.demo.api.hello.grpc.CountResultGrpc> selectCount(
        com.google.protobuf.Empty request) {
      return futureUnaryCall(
          getChannel().newCall(getSelectCountMethod(), getCallOptions()), request);
    }

    /**
     * <pre>
     **
     * 查询被标记删除的记录总数；
     * 即 active 为 0 的记录；
     * 适用于 Admin 调用；
     * 
     * &#64;return grpcCountResult
     * </pre>
     */
    public com.google.common.util.concurrent.ListenableFuture<com.q3d.demo.api.hello.grpc.CountResultGrpc> selectCountRemoved(
        com.google.protobuf.Empty request) {
      return futureUnaryCall(
          getChannel().newCall(getSelectCountRemovedMethod(), getCallOptions()), request);
    }

    /**
     * <pre>
     **
     * 按 ID 查询一条记录；
     * 即使 active 为 0 也会匹配；
     * 适用于 Admin 调用；
     * 
     * &#64;param grpcIdExt
     * &#64;return grpcR1K2
     * </pre>
     */
    public com.google.common.util.concurrent.ListenableFuture<com.q3d.demo.api.hello.grpc.HelloWorldGrpcR1K2> selectOne(
        com.q3d.demo.api.hello.grpc.IdExtGrpc request) {
      return futureUnaryCall(
          getChannel().newCall(getSelectOneMethod(), getCallOptions()), request);
    }

    /**
     * <pre>
     **
     * 按 ID 列表批量查询记录；
     * 即使 active 为 0 也会匹配；
     * 适用于 Admin 调用；
     * 
     * &#64;param grpcIdExtList
     * &#64;return grpcR1K2List
     * </pre>
     */
    public com.google.common.util.concurrent.ListenableFuture<com.q3d.demo.api.hello.grpc.HelloWorldGrpcR1K2List> selectBatch(
        com.q3d.demo.api.hello.grpc.IdExtListGrpc request) {
      return futureUnaryCall(
          getChannel().newCall(getSelectBatchMethod(), getCallOptions()), request);
    }

    /**
     * <pre>
     **
     * 分页查询记录；
     * 即使 active 为 0 也会匹配；
     * 适用于 Admin 调用；
     * 
     * &#64;param grpcPageRequest
     * &#64;return grpcR1K2List
     * </pre>
     */
    public com.google.common.util.concurrent.ListenableFuture<com.q3d.demo.api.hello.grpc.HelloWorldGrpcR1K2List> selectByPage(
        com.q3d.demo.api.hello.grpc.PageRequestGrpc request) {
      return futureUnaryCall(
          getChannel().newCall(getSelectByPageMethod(), getCallOptions()), request);
    }

    /**
     * <pre>
     **
     * 分页查询 ID 列表；
     * 即使 active 为 0 也会匹配；
     * 适用于 Admin 调用；
     * 
     * &#64;param grpcPageRequest
     * &#64;return grpcIdExtList
     * </pre>
     */
    public com.google.common.util.concurrent.ListenableFuture<com.q3d.demo.api.hello.grpc.IdExtListGrpc> selectIdExtByPage(
        com.q3d.demo.api.hello.grpc.PageRequestGrpc request) {
      return futureUnaryCall(
          getChannel().newCall(getSelectIdExtByPageMethod(), getCallOptions()), request);
    }

    /**
     * <pre>
     **
     * 查询被标记删除的全部记录的 ID 列表；
     * 即 active 为 0 的记录；
     * 适用于 Admin 调用；
     * 
     * &#64;return grpcIdExtList
     * </pre>
     */
    public com.google.common.util.concurrent.ListenableFuture<com.q3d.demo.api.hello.grpc.IdExtListGrpc> selectIdExtRemoved(
        com.google.protobuf.Empty request) {
      return futureUnaryCall(
          getChannel().newCall(getSelectIdExtRemovedMethod(), getCallOptions()), request);
    }
  }

  private static final int METHODID_HELLO = 0;
  private static final int METHODID_ADD_ONE = 1;
  private static final int METHODID_ADD_BATCH = 2;
  private static final int METHODID_REMOVE_ONE = 3;
  private static final int METHODID_REMOVE_BATCH = 4;
  private static final int METHODID_DELETE_ONE = 5;
  private static final int METHODID_DELETE_BATCH = 6;
  private static final int METHODID_REVIVE_ONE = 7;
  private static final int METHODID_REVIVE_BATCH = 8;
  private static final int METHODID_EDIT_SELECTIVE = 9;
  private static final int METHODID_UPDATE_SELECTIVE = 10;
  private static final int METHODID_GET_COUNT = 11;
  private static final int METHODID_GET_ONE = 12;
  private static final int METHODID_GET_BATCH = 13;
  private static final int METHODID_GET_BY_PAGE = 14;
  private static final int METHODID_GET_UUID_BY_PAGE = 15;
  private static final int METHODID_SELECT_COUNT = 16;
  private static final int METHODID_SELECT_COUNT_REMOVED = 17;
  private static final int METHODID_SELECT_ONE = 18;
  private static final int METHODID_SELECT_BATCH = 19;
  private static final int METHODID_SELECT_BY_PAGE = 20;
  private static final int METHODID_SELECT_ID_EXT_BY_PAGE = 21;
  private static final int METHODID_SELECT_ID_EXT_REMOVED = 22;

  private static final class MethodHandlers<Req, Resp> implements
      io.grpc.stub.ServerCalls.UnaryMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ServerStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ClientStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.BidiStreamingMethod<Req, Resp> {
    private final HelloWorldServiceImplBase serviceImpl;
    private final int methodId;

    MethodHandlers(HelloWorldServiceImplBase serviceImpl, int methodId) {
      this.serviceImpl = serviceImpl;
      this.methodId = methodId;
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public void invoke(Req request, io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        case METHODID_HELLO:
          serviceImpl.hello((com.q3d.demo.api.hello.grpc.StringGrpc) request,
              (io.grpc.stub.StreamObserver<com.q3d.demo.api.hello.grpc.StringGrpc>) responseObserver);
          break;
        case METHODID_ADD_ONE:
          serviceImpl.addOne((com.q3d.demo.api.hello.grpc.HelloWorldGrpcP1) request,
              (io.grpc.stub.StreamObserver<com.q3d.demo.api.hello.grpc.UuidGrpc>) responseObserver);
          break;
        case METHODID_ADD_BATCH:
          serviceImpl.addBatch((com.q3d.demo.api.hello.grpc.HelloWorldGrpcP1List) request,
              (io.grpc.stub.StreamObserver<com.q3d.demo.api.hello.grpc.UuidListGrpc>) responseObserver);
          break;
        case METHODID_REMOVE_ONE:
          serviceImpl.removeOne((com.q3d.demo.api.hello.grpc.UuidGrpc) request,
              (io.grpc.stub.StreamObserver<com.q3d.demo.api.hello.grpc.OpResultGrpc>) responseObserver);
          break;
        case METHODID_REMOVE_BATCH:
          serviceImpl.removeBatch((com.q3d.demo.api.hello.grpc.UuidListGrpc) request,
              (io.grpc.stub.StreamObserver<com.q3d.demo.api.hello.grpc.OpResultGrpc>) responseObserver);
          break;
        case METHODID_DELETE_ONE:
          serviceImpl.deleteOne((com.q3d.demo.api.hello.grpc.IdExtGrpc) request,
              (io.grpc.stub.StreamObserver<com.q3d.demo.api.hello.grpc.OpResultGrpc>) responseObserver);
          break;
        case METHODID_DELETE_BATCH:
          serviceImpl.deleteBatch((com.q3d.demo.api.hello.grpc.IdExtListGrpc) request,
              (io.grpc.stub.StreamObserver<com.q3d.demo.api.hello.grpc.OpResultGrpc>) responseObserver);
          break;
        case METHODID_REVIVE_ONE:
          serviceImpl.reviveOne((com.q3d.demo.api.hello.grpc.IdExtGrpc) request,
              (io.grpc.stub.StreamObserver<com.q3d.demo.api.hello.grpc.OpResultGrpc>) responseObserver);
          break;
        case METHODID_REVIVE_BATCH:
          serviceImpl.reviveBatch((com.q3d.demo.api.hello.grpc.IdExtListGrpc) request,
              (io.grpc.stub.StreamObserver<com.q3d.demo.api.hello.grpc.OpResultGrpc>) responseObserver);
          break;
        case METHODID_EDIT_SELECTIVE:
          serviceImpl.editSelective((com.q3d.demo.api.hello.grpc.HelloWorldGrpcP1K1) request,
              (io.grpc.stub.StreamObserver<com.q3d.demo.api.hello.grpc.OpResultGrpc>) responseObserver);
          break;
        case METHODID_UPDATE_SELECTIVE:
          serviceImpl.updateSelective((com.q3d.demo.api.hello.grpc.HelloWorldGrpcP1K2) request,
              (io.grpc.stub.StreamObserver<com.q3d.demo.api.hello.grpc.OpResultGrpc>) responseObserver);
          break;
        case METHODID_GET_COUNT:
          serviceImpl.getCount((com.google.protobuf.Empty) request,
              (io.grpc.stub.StreamObserver<com.q3d.demo.api.hello.grpc.CountResultGrpc>) responseObserver);
          break;
        case METHODID_GET_ONE:
          serviceImpl.getOne((com.q3d.demo.api.hello.grpc.UuidGrpc) request,
              (io.grpc.stub.StreamObserver<com.q3d.demo.api.hello.grpc.HelloWorldGrpcR1K1>) responseObserver);
          break;
        case METHODID_GET_BATCH:
          serviceImpl.getBatch((com.q3d.demo.api.hello.grpc.UuidListGrpc) request,
              (io.grpc.stub.StreamObserver<com.q3d.demo.api.hello.grpc.HelloWorldGrpcR1K1List>) responseObserver);
          break;
        case METHODID_GET_BY_PAGE:
          serviceImpl.getByPage((com.q3d.demo.api.hello.grpc.PageRequestGrpc) request,
              (io.grpc.stub.StreamObserver<com.q3d.demo.api.hello.grpc.HelloWorldGrpcR1K1List>) responseObserver);
          break;
        case METHODID_GET_UUID_BY_PAGE:
          serviceImpl.getUuidByPage((com.q3d.demo.api.hello.grpc.PageRequestGrpc) request,
              (io.grpc.stub.StreamObserver<com.q3d.demo.api.hello.grpc.UuidListGrpc>) responseObserver);
          break;
        case METHODID_SELECT_COUNT:
          serviceImpl.selectCount((com.google.protobuf.Empty) request,
              (io.grpc.stub.StreamObserver<com.q3d.demo.api.hello.grpc.CountResultGrpc>) responseObserver);
          break;
        case METHODID_SELECT_COUNT_REMOVED:
          serviceImpl.selectCountRemoved((com.google.protobuf.Empty) request,
              (io.grpc.stub.StreamObserver<com.q3d.demo.api.hello.grpc.CountResultGrpc>) responseObserver);
          break;
        case METHODID_SELECT_ONE:
          serviceImpl.selectOne((com.q3d.demo.api.hello.grpc.IdExtGrpc) request,
              (io.grpc.stub.StreamObserver<com.q3d.demo.api.hello.grpc.HelloWorldGrpcR1K2>) responseObserver);
          break;
        case METHODID_SELECT_BATCH:
          serviceImpl.selectBatch((com.q3d.demo.api.hello.grpc.IdExtListGrpc) request,
              (io.grpc.stub.StreamObserver<com.q3d.demo.api.hello.grpc.HelloWorldGrpcR1K2List>) responseObserver);
          break;
        case METHODID_SELECT_BY_PAGE:
          serviceImpl.selectByPage((com.q3d.demo.api.hello.grpc.PageRequestGrpc) request,
              (io.grpc.stub.StreamObserver<com.q3d.demo.api.hello.grpc.HelloWorldGrpcR1K2List>) responseObserver);
          break;
        case METHODID_SELECT_ID_EXT_BY_PAGE:
          serviceImpl.selectIdExtByPage((com.q3d.demo.api.hello.grpc.PageRequestGrpc) request,
              (io.grpc.stub.StreamObserver<com.q3d.demo.api.hello.grpc.IdExtListGrpc>) responseObserver);
          break;
        case METHODID_SELECT_ID_EXT_REMOVED:
          serviceImpl.selectIdExtRemoved((com.google.protobuf.Empty) request,
              (io.grpc.stub.StreamObserver<com.q3d.demo.api.hello.grpc.IdExtListGrpc>) responseObserver);
          break;
        default:
          throw new AssertionError();
      }
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public io.grpc.stub.StreamObserver<Req> invoke(
        io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        default:
          throw new AssertionError();
      }
    }
  }

  private static abstract class HelloWorldServiceBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoFileDescriptorSupplier, io.grpc.protobuf.ProtoServiceDescriptorSupplier {
    HelloWorldServiceBaseDescriptorSupplier() {}

    @java.lang.Override
    public com.google.protobuf.Descriptors.FileDescriptor getFileDescriptor() {
      return com.q3d.demo.api.hello.grpc.HelloWorld.getDescriptor();
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.ServiceDescriptor getServiceDescriptor() {
      return getFileDescriptor().findServiceByName("HelloWorldService");
    }
  }

  private static final class HelloWorldServiceFileDescriptorSupplier
      extends HelloWorldServiceBaseDescriptorSupplier {
    HelloWorldServiceFileDescriptorSupplier() {}
  }

  private static final class HelloWorldServiceMethodDescriptorSupplier
      extends HelloWorldServiceBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoMethodDescriptorSupplier {
    private final String methodName;

    HelloWorldServiceMethodDescriptorSupplier(String methodName) {
      this.methodName = methodName;
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.MethodDescriptor getMethodDescriptor() {
      return getServiceDescriptor().findMethodByName(methodName);
    }
  }

  private static volatile io.grpc.ServiceDescriptor serviceDescriptor;

  public static io.grpc.ServiceDescriptor getServiceDescriptor() {
    io.grpc.ServiceDescriptor result = serviceDescriptor;
    if (result == null) {
      synchronized (HelloWorldServiceGrpc.class) {
        result = serviceDescriptor;
        if (result == null) {
          serviceDescriptor = result = io.grpc.ServiceDescriptor.newBuilder(SERVICE_NAME)
              .setSchemaDescriptor(new HelloWorldServiceFileDescriptorSupplier())
              .addMethod(getHelloMethod())
              .addMethod(getAddOneMethod())
              .addMethod(getAddBatchMethod())
              .addMethod(getRemoveOneMethod())
              .addMethod(getRemoveBatchMethod())
              .addMethod(getDeleteOneMethod())
              .addMethod(getDeleteBatchMethod())
              .addMethod(getReviveOneMethod())
              .addMethod(getReviveBatchMethod())
              .addMethod(getEditSelectiveMethod())
              .addMethod(getUpdateSelectiveMethod())
              .addMethod(getGetCountMethod())
              .addMethod(getGetOneMethod())
              .addMethod(getGetBatchMethod())
              .addMethod(getGetByPageMethod())
              .addMethod(getGetUuidByPageMethod())
              .addMethod(getSelectCountMethod())
              .addMethod(getSelectCountRemovedMethod())
              .addMethod(getSelectOneMethod())
              .addMethod(getSelectBatchMethod())
              .addMethod(getSelectByPageMethod())
              .addMethod(getSelectIdExtByPageMethod())
              .addMethod(getSelectIdExtRemovedMethod())
              .build();
        }
      }
    }
    return result;
  }
}
