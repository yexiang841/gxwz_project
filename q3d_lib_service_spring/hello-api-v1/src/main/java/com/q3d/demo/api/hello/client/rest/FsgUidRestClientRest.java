package com.q3d.demo.api.hello.client.rest;

import com.alibaba.fastjson2.JSONObject;
import com.q3d.demo.common.lib.fsguid.dto.FsgUidDto;
import com.q3d.demo.common.lib.service.response.ResponseUtil;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

@Component /** 注入组件 */
/**
 * @author 叶湘 ( weixin:yexiang841, email:yexiang841@qq.com )
 */
public class FsgUidRestClientRest {

        @Autowired
        private RestTemplate restTemplate;

        private String remoteSvcName = "hello-svc-v1";

        /**
         * 调用 hello-svc-v1 服务的 uid 接口获取一个全局唯一的 uid
         * 
         * @return FsgUidDto
         */
        public FsgUidDto next() {
                // 自动寻址：获取 Eureka-Server 中注册的实例列表，代替具体 {host}:{port}
                // 并根据 @LoadBalance 配置自动选择 HA 实例
                String url = "http://" + remoteSvcName + "/api/fsguid/next";
                UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromUriString(url);
                // 定义 requestHeaders
                HttpHeaders requestHeaders = new HttpHeaders();
                requestHeaders.setContentType(MediaType.APPLICATION_JSON);
                // 定义 requestBody
                MultiValueMap<String, String> requestBody = new LinkedMultiValueMap<>();
                // 定义 request = requestHeaders + requestBody
                HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<MultiValueMap<String, String>>(
                                requestBody,
                                requestHeaders);
                // 根据 ResponseBase 的结构自动映射响应结构体
                ResponseEntity<JSONObject> response = restTemplate.exchange(uriBuilder.build().encode().toUri(),
                                HttpMethod.POST, request,
                                JSONObject.class);
                FsgUidDto dto = ResponseUtil.parseResponse(FsgUidDto.class, response, remoteSvcName);
                return dto;
        }

        /**
         * 调用 hello-svc-v1 服务的 uid 接口解析一个 uid
         *
         * @param uid
         * @return JSONObject
         */
        public FsgUidDto parse(Long uid) {
                // 以服务名 hello-svc-v1 代替具体 {host}:{port}，实现 Eureka 自动寻址
                String url = "http://" + remoteSvcName + "/api/fsguid/parse/" + uid;
                UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromUriString(url);
                // 定义 requestHeaders
                HttpHeaders requestHeaders = new HttpHeaders();
                requestHeaders.setContentType(MediaType.APPLICATION_JSON);
                // 定义 requestBody
                MultiValueMap<String, String> requestBody = new LinkedMultiValueMap<>();
                // 定义 request = requestHeaders + requestBody
                HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<MultiValueMap<String, String>>(
                                requestBody,
                                requestHeaders);
                // 根据 ResponseBase 的结构自动映射响应结构体
                ResponseEntity<JSONObject> response = restTemplate.exchange(uriBuilder.build().encode().toUri(),
                                HttpMethod.GET, request,
                                JSONObject.class);
                FsgUidDto dto = ResponseUtil.parseResponse(FsgUidDto.class, response, remoteSvcName);
                return dto;
        }

}
