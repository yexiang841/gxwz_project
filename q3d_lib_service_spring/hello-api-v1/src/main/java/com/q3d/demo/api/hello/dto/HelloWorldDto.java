package com.q3d.demo.api.hello.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PositiveOrZero;

import io.swagger.v3.oas.annotations.media.Schema;

import lombok.*;
import lombok.experimental.SuperBuilder;

@Data /**
       * Lombok @Data =
       * 
       * @Getter +
       * @Setter +
       * @ToString +
       * @EqualsAndHashCode +
       * @RequiredArgsConstruct
       */
@AllArgsConstructor /** Lombok 全参构造方法 */
@NoArgsConstructor /** Lombok 无参构造方法 */
@SuperBuilder /** Lombok 链式 Builder 构造方法，当基类和子类都用 @Data 时，两者都要从 @Builder 改成 @SuperBuilder */
/**
 * @author 叶湘 ( weixin:yexiang841, email:yexiang841@qq.com )
 */
public class HelloWorldDto implements java.io.Serializable { // 若 RPC 需要传输 DTO 则要求序列化

    @Schema(description = "元宇宙名称", example = "元宇宙二号")
    @NotNull(message = "元宇宙名称不能为空")
    @NotBlank(message = "元宇宙名称不能为空")
    protected String worldName;

    @Schema(description = "元宇宙描述", example = "元宇宙二号是个好宇宙")
    @Builder.Default /** 设置默认值，允许客户端不赋值 */
    protected String worldDesc = "";

    @Schema(description = "元宇宙年龄", example = "42")
    @PositiveOrZero(message = "元宇宙年龄不能为负值")
    @Builder.Default /** 设置默认值，允许客户端不赋值 */
    protected Integer worldAge = 0;

    @Schema(description = "元宇宙半径", example = "3.14")
    @PositiveOrZero(message = "元宇宙半径不能为负值")
    @Builder.Default /** 设置默认值，允许客户端不赋值 */
    protected Float worldRadius = 0.0f;

    @Schema(description = "元宇宙质量", example = "2.71828")
    @PositiveOrZero(message = "元宇宙质量不能为负值")
    @Builder.Default /** 设置默认值，允许客户端不赋值 */
    protected Double worldWeight = 0.0d;

}
