package com.q3d.demo.api.hello.service;

import javax.validation.Valid;

import com.q3d.demo.api.hello.dto.HelloWorldP1Dto;
import com.q3d.demo.api.hello.dto.HelloWorldP1K1Dto;
import com.q3d.demo.api.hello.dto.HelloWorldP1K2Dto;
import com.q3d.demo.api.hello.dto.HelloWorldP1ListDto;
import com.q3d.demo.api.hello.dto.HelloWorldR1K1Dto;
import com.q3d.demo.api.hello.dto.HelloWorldR1K1ListDto;
import com.q3d.demo.api.hello.dto.HelloWorldR1K2Dto;
import com.q3d.demo.api.hello.dto.HelloWorldR1K2ListDto;
import com.q3d.demo.common.lib.service.dto.CountResultDto;
import com.q3d.demo.common.lib.service.dto.IdExtDto;
import com.q3d.demo.common.lib.service.dto.IdExtListDto;
import com.q3d.demo.common.lib.service.dto.OpResultDto;
import com.q3d.demo.common.lib.service.dto.PageRequestDto;
import com.q3d.demo.common.lib.service.dto.StringDto;
import com.q3d.demo.common.lib.service.dto.UuidDto;
import com.q3d.demo.common.lib.service.dto.UuidListDto;

/**
 * HelloWorldService 服务接口
 * 
 * @author 叶湘 ( weixin:yexiang841, email:yexiang841@qq.com )
 */
public interface HelloWorldServiceInterface {

    /**
     * 字符串测试
     * 
     * @param dtoStringRequest
     * @return StringDto
     */
    public StringDto hello(@Valid StringDto dtoStringRequest);

    /**
     * 新增一条记录；
     * 如果字段未被设置，会使用 Model 类的默认值；
     * 如果 Model 默认值为 Null 而数据库字段设置为 Not Null，则报错；
     * 除非在 Mapper 中重写 SQL 语句；
     * 适用于业务调用；
     * 
     * @param dtoP1
     * @return dtoOpResult
     */
    public UuidDto addOne(@Valid HelloWorldP1Dto dtoP1);

    /**
     * 批量新增记录；
     * 如果字段未被设置，会使用数据库默认值；
     * 除非在 Mapper 中重写 SQL 语句；
     * 适用于业务调用；
     * 
     * @param dtoP1List
     * @return dtoOpResult
     */
    public UuidListDto addBatch(@Valid HelloWorldP1ListDto dtoP1List);

    /**
     * 按 UUID 删除一条记录(只修改标志位，不真实删除)；
     * 适用于业务调用；
     * 
     * @param dtoUuid
     * @return dtoOpResult
     */
    public OpResultDto removeOne(@Valid UuidDto dtoUuid);

    /**
     * 按 UUID 列表批量删除记录(只修改标志位，不真实删除)；
     * 适用于业务调用；
     * 
     * @param dtoUuidList
     * @return dtoOpResult
     */
    public OpResultDto removeBatch(@Valid UuidListDto dtoUuidList);

    /**
     * 按 ID 彻底删除一条记录(真实删除，找不回来的那种)；
     * 适用于 Admin 调用；
     * 
     * @param dtoIdExtExt
     * @return dtoOpResult
     */
    public OpResultDto deleteOne(@Valid IdExtDto dtoIdExt);

    /**
     * 按 ID 列表批量彻底删除记录(真实删除，找不回来的那种)；
     * 适用于 Admin 调用；
     * 
     * @param dtoIdExtList
     * @return dtoOpResult
     */
    public OpResultDto deleteBatch(@Valid IdExtListDto dtoIdExtList);

    /**
     * 按 ID 恢复一条被标记删除的记录；
     * 适用于 Admin 调用；
     * 
     * @param dtoIdExtExt
     * @return dtoOpResult
     */
    public OpResultDto reviveOne(@Valid IdExtDto dtoIdExt);

    /**
     * 按 ID 列表批量恢复被标记删除的记录；
     * 适用于 Admin 调用；
     * 
     * @param dtoIdExtList
     * @return dtoOpResult
     */
    public OpResultDto reviveBatch(@Valid IdExtListDto dtoIdExtList);

    /**
     * 按 UUID 选择性修改一条记录；
     * 只更新 Model 中的非 Null 字段；
     * 适用于业务调用；
     * 
     * @param dtoP1K1
     * @return dtoOpResult
     */
    public OpResultDto editSelective(@Valid HelloWorldP1K1Dto dtoP1K1);

    /**
     * 按 ID 选择性修改一条记录；
     * 只更新 Model 中的非 Null 字段；
     * 适用于 Admin 调用；
     * 
     * @param dtoP1K2
     * @return dtoOpResult
     */
    public OpResultDto updateSelective(@Valid HelloWorldP1K2Dto dtoP1K2);

    /**
     * 查询记录总数；
     * 只匹配 active 为 1 的记录；
     * 适用于业务调用；
     * 
     * @return dtoCountResult
     */
    public CountResultDto getCount();

    /**
     * 按 UUID 查询一条记录；
     * 只匹配 active 为 1 的记录；
     * 适用于业务调用；
     * 
     * @param dtoUuid
     * @return dtoR1K1
     */
    public HelloWorldR1K1Dto getOne(@Valid UuidDto dtoUuid);

    /**
     * 按 UUID 列表批量查询记录；
     * 只匹配 active 为 1 的记录；
     * 适用于业务调用；
     * 
     * @param dtoUuidList
     * @return dtoR1K1List
     */
    public HelloWorldR1K1ListDto getBatch(@Valid UuidListDto dtoUuidList);

    /**
     * 分页查询记录；
     * 只匹配 active 为 1 的记录；
     * 适用于业务调用；
     * 
     * @param dtoPageRequest
     * @return dtoR1K1List
     */
    public HelloWorldR1K1ListDto getByPage(@Valid PageRequestDto dtoPageRequest);

    /**
     * 分页查询 UUID 列表；
     * 只匹配 active 为 1 的记录；
     * 适用于业务调用；
     * 
     * @param dtoPageRequest
     * @return dtoUuidList
     */
    public UuidListDto getUuidByPage(@Valid PageRequestDto dtoPageRequest);

    /**
     * 查询记录总数；
     * 即使 active 为 0 也会匹配；
     * 适用于 Admin 调用；
     * 
     * @return dtoCountResult
     */
    public CountResultDto selectCount();

    /**
     * 查询被标记删除的记录总数；
     * 即 active 为 0 的记录；
     * 适用于 Admin 调用；
     * 
     * @return dtoCountResult
     */
    public CountResultDto selectCountRemoved();

    /**
     * 按 ID 查询一条记录；
     * 即使 active 为 0 也会匹配；
     * 适用于 Admin 调用；
     * 
     * @param dtoIdExt
     * @return dtoR1K2
     */
    public HelloWorldR1K2Dto selectOne(@Valid IdExtDto dtoIdExt);

    /**
     * 按 ID 列表批量查询记录；
     * 即使 active 为 0 也会匹配；
     * 适用于 Admin 调用；
     * 
     * @param dtoIdExtList
     * @return dtoR1K2List
     */
    public HelloWorldR1K2ListDto selectBatch(@Valid IdExtListDto dtoIdExtList);

    /**
     * 分页查询记录；
     * 即使 active 为 0 也会匹配；
     * 适用于 Admin 调用；
     * 
     * @param dtoPageRequest
     * @return dtoR1K2List
     */
    public HelloWorldR1K2ListDto selectByPage(@Valid PageRequestDto dtoPageRequest);

    /**
     * 分页查询 ID 列表；
     * 即使 active 为 0 也会匹配；
     * 适用于 Admin 调用；
     * 
     * @param dtoPageRequest
     * @return dtoIdExtList
     */
    public IdExtListDto selectIdExtByPage(@Valid PageRequestDto dtoPageRequest);

    /**
     * 查询被标记删除的全部记录的 ID 列表；
     * 即 active 为 0 的记录；
     * 适用于 Admin 调用；
     * 
     * @return dtoIdExtList
     */
    public IdExtListDto selectIdExtRemoved();

}