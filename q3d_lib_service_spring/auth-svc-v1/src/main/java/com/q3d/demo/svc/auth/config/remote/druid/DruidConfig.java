package com.q3d.demo.svc.auth.config.remote.druid;

import com.alibaba.druid.pool.DruidDataSource;
import com.q3d.demo.common.lib.druid.model.ConfigDruidModel;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import lombok.extern.slf4j.Slf4j;

@Slf4j /** Lombok 的 Logger 注解 */
@Configuration /** 作用于 Bean 类的属性配置 */
/**
 * 数据源获取配置（配置中心）
 * 
 * @author 叶湘 ( weixin:yexiang841, email:yexiang841@qq.com )
 */
public class DruidConfig {

    @Autowired
    ConfigDruidModel druidConfigModel;

    @Bean
    @RefreshScope /**
                   * ConfigDruidModel 因其 @ConfigurationProperties 注解接收到动态配置更新时，
                   * 通过 @RefreshScope 注解触发本方法，动态刷新 DruidDataSource
                   */
    public DruidDataSource dataSource() {
        log.info("dataSource : " + druidConfigModel.toString());
        DruidDataSource druidDataSource = new DruidDataSource();
        druidDataSource.setUrl(druidConfigModel.getUrl());
        druidDataSource.setUsername(druidConfigModel.getUsername());
        druidDataSource.setPassword(druidConfigModel.getPassword());
        druidDataSource.setDriverClassName(druidConfigModel.getDriverClassName());
        return druidDataSource;
    }

}