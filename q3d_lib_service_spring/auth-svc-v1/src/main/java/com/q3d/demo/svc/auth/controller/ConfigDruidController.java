package com.q3d.demo.svc.auth.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.druid.stat.DruidStatManagerFacade;
import com.q3d.demo.common.lib.druid.dto.ConfigDruidDto;
import com.q3d.demo.common.lib.druid.manager.ConfigDruidManager;
import com.q3d.demo.common.lib.service.response.ResponseBase;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;

@RequestMapping("/config/druid") /** 匹配路径 */
@Tag(name = "动态配置（Druid 组）查询接口") /** SpringDoc 类注解 */
@RestController
/**
 * 代码来自 Druid 官方 Github
 * 
 * @author 叶湘 ( weixin:yexiang841, email:yexiang841@qq.com )
 */
public class ConfigDruidController {

    @Autowired
    private ConfigDruidManager druidConfigManager;

    @GetMapping("/stat")
    @Operation(summary = "获取所有数据源的监控数据")
    public Object druidStat() {
        // DruidStatManagerFacade#getDataSourceStatDataList
        // DruidStatManagerFacade 还提供了一些其他方法，你可以按需选择使用。
        return DruidStatManagerFacade.getInstance().getDataSourceStatDataList();
    }

    @GetMapping(value = "/all")
    @Operation(summary = "获取配置中心所有字段的值，存储在配置中心的 {application-name}-druid.yml 中")
    public ResponseBase<ConfigDruidDto> all() {
        // 数据操作
        ConfigDruidDto dtoConfigDruid = druidConfigManager.getAll();
        // 结构化返回
        return ResponseBase.success(dtoConfigDruid );
    }

}