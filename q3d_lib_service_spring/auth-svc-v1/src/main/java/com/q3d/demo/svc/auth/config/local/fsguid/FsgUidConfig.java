package com.q3d.demo.svc.auth.config.local.fsguid;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.baidu.fsg.uid.impl.DefaultUidGenerator;
import com.baidu.fsg.uid.worker.DisposableWorkerIdAssigner;

@Configuration /** 作用于 Bean 类的属性配置 */
/**
 * UidGenerator 官方建议高性能要求可配置 cached-uid-spring.xml
 * 
 * @author 叶湘 ( weixin:yexiang841, email:yexiang841@qq.com )
 */
public class FsgUidConfig {

	@Bean
	public DefaultUidGenerator defaultUidGenerator(DisposableWorkerIdAssigner disposableWorkerIdAssigner) {
		DefaultUidGenerator defaultUidGenerator = new DefaultUidGenerator();
		defaultUidGenerator.setWorkerIdAssigner(disposableWorkerIdAssigner);
		defaultUidGenerator.setTimeBits(29);
		defaultUidGenerator.setWorkerBits(21);
		defaultUidGenerator.setSeqBits(13);
		defaultUidGenerator.setEpochStr("2016-09-20");
		return defaultUidGenerator;
	}

	@Bean
	public DisposableWorkerIdAssigner disposableWorkerIdAssigner() {
		return new DisposableWorkerIdAssigner();
	}

}
