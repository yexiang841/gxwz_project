package com.q3d.demo.common.lib.druid.dto;

import io.swagger.v3.oas.annotations.media.Schema;

import lombok.*;

@Data /**
       * Lombok @Data =
       * 
       * @Getter +
       * @Setter +
       * @ToString +
       * @EqualsAndHashCode +
       * @RequiredArgsConstruct
       */
@AllArgsConstructor /** Lombok 全参构造方法 */
@NoArgsConstructor /** Lombok 无参构造方法 */
@Builder /** Lombok 链式 Builder 构造方法 */
@Schema(description = "Druid 配置响应结构体") /** SpringDoc 注解 */
public class ConfigDruidDto implements java.io.Serializable { // 若 RPC 需要传输 DTO 则要求序列化

       @Schema(description = "来自配置中心的 url 字段")
       private String url;

       @Schema(description = "来自配置中心的 username 字段")
       private String username;

       @Schema(description = "来自配置中心的 password 字段")
       private String password;

       @Schema(description = "来自配置中心的 driverClassName 字段")
       private String driverClassName;

}
