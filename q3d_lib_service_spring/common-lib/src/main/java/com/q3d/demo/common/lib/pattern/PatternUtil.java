package com.q3d.demo.common.lib.pattern;

import java.util.regex.Pattern;

/**
 * @author 叶湘 ( weixin:yexiang841, email:yexiang841@qq.com )
 */
public class PatternUtil {

    public static boolean validateUuid32Bit(String uuid) {
        String patternUuid = "^[a-f\\d]{32}$";
        if (!Pattern.matches(patternUuid, uuid)) {
            return false;
        }
        return true;
    }

    public static boolean validateIdExt19Bit(String id) {
        String patternIdExt = "^[1-9]{1}[\\d]{18}$";
        if (!Pattern.matches(patternIdExt, id)) {
            return false;
        }
        return true;
    }
}
