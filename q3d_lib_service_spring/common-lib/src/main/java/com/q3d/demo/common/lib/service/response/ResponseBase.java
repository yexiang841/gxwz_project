package com.q3d.demo.common.lib.service.response;

import io.swagger.v3.oas.annotations.media.Schema;

import lombok.*;

@Data /**
       * Lombok @Data =
       * 
       * @Getter +
       * @Setter +
       * @ToString +
       * @EqualsAndHashCode +
       * @RequiredArgsConstruct
       */
@Builder /** Lombok 链式 Builder 构造方法 */
@AllArgsConstructor /** Lombok 全参构造方法 */
@NoArgsConstructor /** Lombok 无参构造方法 */
@Schema(description = "响应结构体") /** SpringDoc 注解 */
/**
 * @author 叶湘 ( weixin:yexiang841, email:yexiang841@qq.com )
 */
public class ResponseBase<DTO> {

    @Builder.Default /** new 出的对象 code 为 null，build 出的对象 code 可赋值，也可使用缺省值 */
    @Schema(name = "HTTP 返回码", description = "直接引用 HttpServletResponse 字段")
    private int code = ResponseStatus.OK.code;

    @Builder.Default /** new 出的对象 code_desc 为 null，build 出的对象 code 可赋值，也可使用缺省值 */
    @Schema(name = "HTTP 返回码描述", description = "对 HTTP 返回码的解析，省的查百度")
    private String answer = ResponseStatus.OK.answer;

    @Schema(name = "响应结构体数据对象", description = "通常是 DTO")
    private DTO data;

    @Schema(name = "响应结构体描述", description = "注意这个是用户自定义信息，不是 ResultCode 中的返回码描述")
    private String message;

    // @Builder.Default /** new 出的对象 code 为 null，build 出的对象 code 可赋值，也可使用缺省值 */
    // private ResponseStatus status = ResponseStatus.OK;

    /**
     * 写入自定义返回状态
     *
     * @param status
     * @return ResponseBase<DTO>
     */
    public ResponseBase<DTO> status(ResponseStatus status) {
        this.code = status.code;
        this.answer = status.answer;
        return this;
    }

    /**
     * 生成返回结构，写入数据并返回
     *
     * @param data
     * @return ResponseBase<DTO>
     */
    public static <DTO> ResponseBase<DTO> success(DTO data) {
        ResponseBase<DTO> response = new ResponseBase<DTO>();
        response.setData(data);
        return response;
    }

}
