package com.q3d.demo.common.lib.auth;

import com.q3d.demo.common.lib.service.response.ResponseStatus;

import lombok.*;;

/**
 * 自定义未授权异常
 * 
 * @author 叶湘 ( weixin:yexiang841, email:yexiang841@qq.com )
 */
public class UnAuthorizedException extends RuntimeException {

    @Getter
    private final ResponseStatus responseStatus;

    public UnAuthorizedException(String message) {
        super(message);
        this.responseStatus = ResponseStatus.UNAUTHORIZED;
    }

    public UnAuthorizedException(ResponseStatus responseStatus) {
        super(responseStatus.getAnswer());
        this.responseStatus = responseStatus;
    }

    public UnAuthorizedException(ResponseStatus responseStatus, Throwable cause) {
        super(cause);
        this.responseStatus = responseStatus;
    }

    /**
     * For better performance
     *
     * @return Throwable
     */
    @Override
    public Throwable fillInStackTrace() {
        return this;
    }

    public Throwable doFillInStackTrace() {
        return super.fillInStackTrace();
    }
}
