package com.q3d.demo.common.lib.fsguid.dto;

import io.swagger.v3.oas.annotations.media.Schema;

import lombok.*;

@Data /**
       * Lombok @Data =
       * 
       * @Getter +
       * @Setter +
       * @ToString +
       * @EqualsAndHashCode +
       * @RequiredArgsConstruct
       */
@AllArgsConstructor /** Lombok 全参构造方法 */
@NoArgsConstructor /** Lombok 无参构造方法 */
@Builder /** Lombok 链式 Builder 构造方法 */
@Schema(description = "解析 UID 响应结构体") /** SpringDoc 注解 */
public class FsgUidDto implements java.io.Serializable { // 若 RPC 需要传输 DTO 则要求序列化

    @Schema(description = "全局唯一UID", example = "3246174490346037000")
    private Long uid;

    @Schema(description = "时间戳", example = "2022-09-15 22:43:44")
    private String timestamp;

    @Schema(description = "机器码", example = "20")
    private Integer workerId;

    @Schema(description = "序列号", example = "7944")
    private Integer sequence;
}
