package com.q3d.demo.common.lib.service.dto;

import javax.validation.constraints.NotNull;

import io.swagger.v3.oas.annotations.media.Schema;

import lombok.*;

@Data /**
       * Lombok @Data =
       * 
       * @Getter +
       * @Setter +
       * @ToString +
       * @EqualsAndHashCode +
       * @RequiredArgsConstruct
       */
@AllArgsConstructor /** Lombok 全参构造方法 */
@NoArgsConstructor /** Lombok 无参构造方法 */
@Builder /** Lombok 链式 Builder 构造方法 */
@Schema(description = "字符串响应结构体") /** SpringDoc 注解 */
/**
 * @author 叶湘 ( weixin:yexiang841, email:yexiang841@qq.com )
 */
public class StringDto implements java.io.Serializable { // 若 RPC 需要传输 DTO 则要求序列化

    @Schema(description = "字符串响应结构体", example = "Hello World")
    @NotNull(message = "字符串响应结构体不能为空")
    protected String text;

}
