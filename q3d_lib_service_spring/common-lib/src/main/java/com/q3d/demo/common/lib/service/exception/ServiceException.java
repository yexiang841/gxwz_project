package com.q3d.demo.common.lib.service.exception;

import com.q3d.demo.common.lib.service.response.ResponseStatus;

import lombok.*;

/**
 * 自定义服务端异常类
 * 
 * @author 叶湘 ( weixin:yexiang841, email:yexiang841@qq.com )
 */
public class ServiceException extends RuntimeException {

    @Getter
    private final ResponseStatus responseStatus;

    public ServiceException(String msg) {
        super(msg);
        this.responseStatus = ResponseStatus.BAD_REQUEST_FAILURE;
    }

    public ServiceException(ResponseStatus responseStatus) {
        super(responseStatus.getAnswer());
        this.responseStatus = responseStatus;
    }

    public ServiceException(ResponseStatus responseStatus, String msg) {
        super(msg);
        this.responseStatus = responseStatus;
    }

    public ServiceException(ResponseStatus responseStatus, Throwable cause) {
        super(cause);
        this.responseStatus = responseStatus;
    }

    public ServiceException(String msg, Throwable cause) {
        super(msg, cause);
        this.responseStatus = ResponseStatus.BAD_REQUEST_FAILURE;
    }

    /**
     * For better performance
     *
     * @return Throwable
     */
    @Override
    public Throwable fillInStackTrace() {
        return this;
    }

    public Throwable doFillInStackTrace() {
        return super.fillInStackTrace();
    }
}
