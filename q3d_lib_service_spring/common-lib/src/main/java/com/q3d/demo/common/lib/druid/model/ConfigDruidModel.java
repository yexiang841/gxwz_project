package com.q3d.demo.common.lib.druid.model;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import lombok.*;

@Data /**
       * Lombok @Data =
       * 
       * @Getter +
       * @Setter +
       * @ToString +
       * @EqualsAndHashCode +
       * @RequiredArgsConstruct
       */
@Builder /** Lombok 链式 Builder 构造方法 */
@AllArgsConstructor /** Lombok 全参构造方法 */
@NoArgsConstructor /** Lombok 无参构造方法 */
@ConfigurationProperties(prefix = ConfigDruidModel.PREFIX)
@Component /** 注入组件 */
/**
 * 数据源获取配置（配置中心）
 * 
 * @author 叶湘 ( weixin:yexiang841, email:yexiang841@qq.com )
 */
public class ConfigDruidModel {

    public static final String PREFIX = "spring.datasource.druid";

    // @Builder.Default /** application.yml 或 bootstrap.yml 应定义该字段，因此不需要默认值 */
    private String url;

    // @Builder.Default /** application.yml 或 bootstrap.yml 应定义该字段，因此不需要默认值 */
    private String username;

    // @Builder.Default /** application.yml 或 bootstrap.yml 应定义该字段，因此不需要默认值 */
    private String password;

    // @Builder.Default /** application.yml 或 bootstrap.yml 应定义该字段，因此不需要默认值 */
    private String driverClassName;

}
