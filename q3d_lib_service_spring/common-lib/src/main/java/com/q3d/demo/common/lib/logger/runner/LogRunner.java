package com.q3d.demo.common.lib.logger.runner;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component /** 注入组件 */
@Order(value = 1) /** Order 越小执行越早 */
/**
 * 为 Logger 添加服务名前缀
 * ApplicationRunner 与 CommandLineRunner 作用一样，即在 SpringBoot 启动后执行自定义方法
 * Order 配置为 1，因此日志配置会较早执行
 * 
 * @author 叶湘 ( weixin:yexiang841, email:yexiang841@qq.com )
 */
public class LogRunner implements ApplicationRunner {

    @Value(value = "${spring.application.name}") /** 读取配置文件（或远程配置）数据 */
    public String applicationName;

    @Value(value = "${server.port}") /** 读取配置文件（或远程配置）数据 */
    public String serverPort;

    /** 用线程缓存装服务名 */
    public static ThreadLocal<String> threadLocalName = new ThreadLocal<String>();

    /** 用线程缓存装日志时间序号 */
    public static ThreadLocal<Long> threadLocalTime = new ThreadLocal<Long>();

    @Override
    public void run(ApplicationArguments args) throws Exception {
        // 将服务名+端口写入线程缓存
        LogRunner.threadLocalName.set(applicationName + "-" + serverPort);
    }

}