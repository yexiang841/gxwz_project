package com.q3d.demo.common.lib.service.dto;

import javax.validation.constraints.Max;
import javax.validation.constraints.Positive;
import javax.validation.constraints.PositiveOrZero;

import io.swagger.v3.oas.annotations.media.Schema;

import lombok.*;

@Data /**
       * Lombok @Data =
       * 
       * @Getter +
       * @Setter +
       * @ToString +
       * @EqualsAndHashCode +
       * @RequiredArgsConstruct
       */
@AllArgsConstructor /** Lombok 全参构造方法 */
@NoArgsConstructor /** Lombok 无参构造方法 */
@Builder /** Lombok 链式 Builder 构造方法 */
@Schema(description = "分页查询请求结构体") /** SpringDoc 注解 */
/**
 * @author 叶湘 ( weixin:yexiang841, email:yexiang841@qq.com )
 */
public class PageRequestDto implements java.io.Serializable { // 若 RPC 需要传输 DTO 则要求序列化

    @Schema(description = "查询页数", example = "0")
    @PositiveOrZero(message = "查询页数不能为负值")
    @Builder.Default /** 设置默认值，允许客户端不赋值 */
    protected Integer page = 0;

    @Schema(description = "查询条目数(上限 100)", example = "10")
    @Positive(message = "查询条目数不能为 0 或负值")
    @Max(value = 100, message = "单次查询条目数不能超过 100")
    @Builder.Default /** 设置默认值，允许客户端不赋值 */
    protected Integer limit = 10;

}
