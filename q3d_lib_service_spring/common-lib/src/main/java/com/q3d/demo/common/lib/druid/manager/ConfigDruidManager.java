package com.q3d.demo.common.lib.druid.manager;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.q3d.demo.common.lib.druid.dto.ConfigDruidDto;
import com.q3d.demo.common.lib.druid.model.ConfigDruidModel;

@Component /** 注入组件 */
/**
 * @author 叶湘 ( weixin:yexiang841, email:yexiang841@qq.com )
 */
public class ConfigDruidManager {

        @Autowired
        private ModelMapper modelMapper;

        @Autowired
        private ConfigDruidModel configDruidModel;

        /**
         * 获取配置中心所有字段的值
         *
         * @return ConfigHelloDto
         */
        public ConfigDruidDto getAll() {
                return this.convertToDto(configDruidModel);
        }

        /**
         * 从 Model 转换成 DTO
         *
         * @param model
         * @return DTO
         */
        ConfigDruidDto convertToDto(ConfigDruidModel model) {
                return modelMapper.map(model, ConfigDruidDto.class);
        }

        /**
         * 从 DTO 转换成 Model
         *
         * @param dto
         * @return Model
         */
        ConfigDruidModel convertToModel(ConfigDruidDto dto) {
                return modelMapper.map(dto, ConfigDruidModel.class);
        }

}
