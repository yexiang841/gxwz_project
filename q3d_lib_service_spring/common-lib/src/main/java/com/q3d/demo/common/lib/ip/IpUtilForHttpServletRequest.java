package com.q3d.demo.common.lib.ip;

import java.net.InetAddress;

import javax.servlet.http.HttpServletRequest;

import lombok.extern.slf4j.Slf4j;

@Slf4j /** Lombok 的 Logger 注解 */
public class IpUtilForHttpServletRequest {

    private static final String IP_UTILS_FLAG = ",";
    private static final String UNKNOWN = "unknown";
    private static final String LOCALHOST_IPv6 = "0:0:0:0:0:0:0:1";
    private static final String LOCALHOST_IPv4 = "127.0.0.1";

    /**
     * 获取真实 IP 的方法
     */
    public static String getRemoteIPv4(HttpServletRequest request) {
        String ip = null;
        try {
            /**
             * 以下两个获取在 k8s 中，将真实的客户端 IP，放到了 x-Original-Forwarded-For。
             * 而将WAF的回源地址放到了 x-Forwarded-For 了。
             */
            ip = request.getHeader("X-Original-Forwarded-For");
            if (ip == null || ip.length() == 0 || UNKNOWN.equalsIgnoreCase(ip)) {
                ip = request.getHeader("X-Forwarded-For");
            }
            // 获取 Nginx 等代理的 IP
            if (ip == null || ip.length() == 0 || UNKNOWN.equalsIgnoreCase(ip)) {
                ip = request.getHeader("x-forwarded-for");
            }
            if (ip == null || ip.length() == 0 || UNKNOWN.equalsIgnoreCase(ip)) {
                ip = request.getHeader("Proxy-Client-IP");
            }
            if (ip == null || ip.length() == 0 || ip.length() == 0 || UNKNOWN.equalsIgnoreCase(ip)) {
                ip = request.getHeader("WL-Proxy-Client-IP");
            }
            if (ip == null || ip.length() == 0 || UNKNOWN.equalsIgnoreCase(ip)) {
                ip = request.getHeader("HTTP_CLIENT_IP");
            }
            if (ip == null || ip.length() == 0 || UNKNOWN.equalsIgnoreCase(ip)) {
                ip = request.getHeader("HTTP_X_FORWARDED_FOR");
            }
            // 兼容k8s集群获取ip
            if (ip == null || ip.length() == 0 || UNKNOWN.equalsIgnoreCase(ip)) {
                ip = request.getRemoteAddr();
                if (LOCALHOST_IPv4.equalsIgnoreCase(ip) || LOCALHOST_IPv6.equalsIgnoreCase(ip)) {
                    // 根据网卡取本机配置的IP
                    InetAddress iNet = null;
                    iNet = InetAddress.getLocalHost();
                    ip = iNet.getHostAddress();
                }
            }
        } catch (Exception e) {
            log.error("GetClientIp_Failed: ", e);
        }
        // 使用代理，则获取第一个IP地址
        if (ip != null && ip.length() > 0 && ip.indexOf(IP_UTILS_FLAG) > 0) {
            ip = ip.substring(0, ip.indexOf(IP_UTILS_FLAG));
        }
        return ip.equals(LOCALHOST_IPv6) ? LOCALHOST_IPv4 : ip;
    }

}
