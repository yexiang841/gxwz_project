package com.q3d.demo.common.lib.service.response;

import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.annotation.JsonFormat;

import io.swagger.v3.oas.annotations.media.Schema;

import lombok.*;

@Schema(description = "响应状态") /** SpringDoc 注解 */
@Getter /** Lombok Getter */
@AllArgsConstructor /** Lombok 全参构造方法 */
@JsonFormat(shape = JsonFormat.Shape.OBJECT) /** 状态码序列化为 {code:int,desc:string} */
/**
 * @author 叶湘 ( weixin:yexiang841, email:yexiang841@qq.com )
 */
public enum ResponseStatus {

    /** OK */
    OK(HttpServletResponse.SC_OK, "OK"),

    /** UNAUTHORIZED */
    UNAUTHORIZED(HttpServletResponse.SC_UNAUTHORIZED, "UNAUTHORIZED"),

    /** NOT_FOUND */
    NOT_FOUND(HttpServletResponse.SC_NOT_FOUND, "NOT_FOUND"),

    /** METHOD_NOT_ALLOWED */
    METHOD_NOT_ALLOWED(HttpServletResponse.SC_METHOD_NOT_ALLOWED, "METHOD_NOT_ALLOWED"),

    /** UNSUPPORTED_MEDIA_TYPE */
    UNSUPPORTED_MEDIA_TYPE(HttpServletResponse.SC_UNSUPPORTED_MEDIA_TYPE, "UNSUPPORTED_MEDIA_TYPE"),

    /** FORBIDDEN */
    FORBIDDEN(HttpServletResponse.SC_FORBIDDEN, "FORBIDDEN"),

    /** INTERNAL_SERVER_ERROR */
    INTERNAL_SERVER_ERROR(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "INTERNAL_SERVER_ERROR"),

    /** BAD_REQUEST_FAILURE */
    BAD_REQUEST_FAILURE(HttpServletResponse.SC_BAD_REQUEST, "BAD_REQUEST_FAILURE"),

    /** BAD_REQUEST_MSG_NOT_READABLE */
    BAD_REQUEST_MSG_NOT_READABLE(HttpServletResponse.SC_BAD_REQUEST, "BAD_REQUEST_MSG_NOT_READABLE"),

    /** BAD_REQUEST_PARAM_MISS */
    BAD_REQUEST_PARAM_MISS(HttpServletResponse.SC_BAD_REQUEST, "BAD_REQUEST_PARAM_MISS"),

    /** BAD_REQUEST_PARAM_TYPE_ERROR */
    BAD_REQUEST_PARAM_TYPE_ERROR(HttpServletResponse.SC_BAD_REQUEST, "BAD_REQUEST_PARAM_TYPE_ERROR"),

    /** BAD_REQUEST_PARAM_BIND_ERROR */
    BAD_REQUEST_PARAM_BIND_ERROR(HttpServletResponse.SC_BAD_REQUEST, "BAD_REQUEST_PARAM_BIND_ERROR"),

    /** BAD_REQUEST_PARAM_VALID_ERROR */
    BAD_REQUEST_PARAM_VALID_ERROR(HttpServletResponse.SC_BAD_REQUEST, "BAD_REQUEST_PARAM_VALID_ERROR"),

    /** BAD_REQUEST_PARAM_DATA_ERROR */
    BAD_REQUEST_PARAM_DATA_ERROR(HttpServletResponse.SC_BAD_REQUEST, "BAD_REQUEST_PARAM_DATA_ERROR"),

    /** SERVICE_UNAVAILABLE */
    SERVICE_UNAVAILABLE(HttpServletResponse.SC_SERVICE_UNAVAILABLE, "SERVICE_UNAVAILABLE");

    @Schema(name = "HTTP 返回码", description = "直接引用 HttpServletResponse 字段")
    final int code;

    @Schema(description = "HTTP 返回码描述")
    final String answer;

}