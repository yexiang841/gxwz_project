package com.q3d.demo.common.lib.fsguid.model;

import lombok.*;

@Data /**
       * Lombok @Data =
       * 
       * @Getter +
       * @Setter +
       * @ToString +
       * @EqualsAndHashCode +
       * @RequiredArgsConstruct
       */
@Builder /** Lombok 链式 Builder 构造方法 */
/**
 * @author 叶湘 ( weixin:yexiang841, email:yexiang841@qq.com )
 */
public class FsgUidModel {
    private Long UID;
    private String timestamp;
    private Integer workerId;
    private Integer sequence;
}
