package com.q3d.demo.svc.hello.config.remote.helloworld;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

@Data /**
       * Lombok @Data =
       * 
       * @Getter +
       * @Setter +
       * @ToString +
       * @EqualsAndHashCode +
       * @RequiredArgsConstruct
       */
@AllArgsConstructor /** Lombok 全参构造方法 */
@NoArgsConstructor /** Lombok 无参构造方法 */
@Builder /** Lombok 链式 Builder 构造方法 */
@Schema(description = "HelloWorld 配置响应结构体") /** SpringDoc 注解 */
public class ConfigHelloWorldDto implements java.io.Serializable { // 若 RPC 需要传输 DTO 则要求序列化

       @Schema(description = "来自配置中心的 source 字段")
       private String source;

       @Schema(description = "来自配置中心的 text 字段")
       private String text;

       @Schema(description = "来自配置中心的 num 字段")
       private Integer num;

}
