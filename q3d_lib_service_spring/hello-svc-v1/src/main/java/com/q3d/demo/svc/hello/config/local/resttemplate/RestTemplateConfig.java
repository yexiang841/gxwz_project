package com.q3d.demo.svc.hello.config.local.resttemplate;

import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

@Configuration /** 作用于 Bean 类的属性配置 */
/**
 * 将 RestTemplate 的相关配置注入为 Bean
 * 注意：RestTemplate 默认已注册为 @Bean，在此重复定义，需要在 application.yml 中配置：
 * spring.main.allowBeanDefinitionOverriding: true
 * 否则会抛出运行时错误
 * 
 * @author 叶湘 ( weixin:yexiang841, email:yexiang841@qq.com )
 */
public class RestTemplateConfig {

    @Bean
	@LoadBalanced /** 远程服务多热备情况下的负载均衡策略 */
    public RestTemplate restTemplate() {
        HttpComponentsClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory();
        // 统一设置连接超时
        requestFactory.setConnectTimeout(5 * 1000);
        // 统一设置读超时
        requestFactory.setReadTimeout(5 * 1000);
        return new RestTemplate(requestFactory);
    }

}
