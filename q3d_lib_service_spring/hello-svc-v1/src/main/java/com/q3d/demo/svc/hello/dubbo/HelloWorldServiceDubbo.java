package com.q3d.demo.svc.hello.dubbo;

import javax.validation.Valid;

// import org.apache.dubbo.config.annotation.DubboService;
import org.springframework.beans.factory.annotation.Autowired;

import com.q3d.demo.api.hello.dto.HelloWorldP1Dto;
import com.q3d.demo.api.hello.dto.HelloWorldP1K1Dto;
import com.q3d.demo.api.hello.dto.HelloWorldP1K2Dto;
import com.q3d.demo.api.hello.dto.HelloWorldP1ListDto;
import com.q3d.demo.api.hello.dto.HelloWorldR1K1Dto;
import com.q3d.demo.api.hello.dto.HelloWorldR1K1ListDto;
import com.q3d.demo.api.hello.dto.HelloWorldR1K2Dto;
import com.q3d.demo.api.hello.dto.HelloWorldR1K2ListDto;
import com.q3d.demo.api.hello.service.HelloWorldServiceInterface;
import com.q3d.demo.common.lib.service.dto.CountResultDto;
import com.q3d.demo.common.lib.service.dto.IdExtDto;
import com.q3d.demo.common.lib.service.dto.IdExtListDto;
import com.q3d.demo.common.lib.service.dto.OpResultDto;
import com.q3d.demo.common.lib.service.dto.PageRequestDto;
import com.q3d.demo.common.lib.service.dto.StringDto;
import com.q3d.demo.common.lib.service.dto.UuidDto;
import com.q3d.demo.common.lib.service.dto.UuidListDto;
import com.q3d.demo.svc.hello.mybatis.service.HelloWorldServiceBatis;

// @DubboService /** Dubbo 服务端 */
/**
 * Dubbo 服务端，实现具体业务
 * HelloWorldServiceBatis / HelloWorldServiceJpa 都实现
 * 了 HelloWorldServiceInterface，因此可从中任选一个作为具体实现
 * 
 * @author 叶湘 ( weixin:yexiang841, email:yexiang841@qq.com )
 */
public class HelloWorldServiceDubbo implements HelloWorldServiceInterface {

    /**
     * 也可以等效替换为 HelloWorldServiceJpa
     */
    @Autowired
    private HelloWorldServiceBatis helloWorldV1ServicePo;

    @Override
    public StringDto hello(@Valid StringDto dtoStringRequest) {
        return helloWorldV1ServicePo.hello(dtoStringRequest);
    }

    @Override
    public UuidDto addOne(@Valid HelloWorldP1Dto dtoP1) {
        return helloWorldV1ServicePo.addOne(dtoP1);
    }

    @Override
    public UuidListDto addBatch(@Valid HelloWorldP1ListDto dtoP1List) {
        return helloWorldV1ServicePo.addBatch(dtoP1List);
    }

    @Override
    public OpResultDto removeOne(@Valid UuidDto dtoUuid) {
        return helloWorldV1ServicePo.removeOne(dtoUuid);
    }

    @Override
    public OpResultDto removeBatch(@Valid UuidListDto dtoUuidList) {
        return helloWorldV1ServicePo.removeBatch(dtoUuidList);
    }

    @Override
    public OpResultDto deleteOne(@Valid IdExtDto dtoIdExt) {
        return helloWorldV1ServicePo.deleteOne(dtoIdExt);
    }

    @Override
    public OpResultDto deleteBatch(@Valid IdExtListDto dtoIdExtList) {
        return helloWorldV1ServicePo.deleteBatch(dtoIdExtList);
    }

    @Override
    public OpResultDto reviveOne(@Valid IdExtDto dtoIdExt) {
        return helloWorldV1ServicePo.reviveOne(dtoIdExt);
    }

    @Override
    public OpResultDto reviveBatch(@Valid IdExtListDto dtoIdExtList) {
        return helloWorldV1ServicePo.reviveBatch(dtoIdExtList);
    }

    @Override
    public OpResultDto editSelective(@Valid HelloWorldP1K1Dto dtoP1K1) {
        return helloWorldV1ServicePo.editSelective(dtoP1K1);
    }

    @Override
    public OpResultDto updateSelective(@Valid HelloWorldP1K2Dto dtoP1K2) {
        return helloWorldV1ServicePo.updateSelective(dtoP1K2);
    }

    @Override
    public CountResultDto getCount() {
        return helloWorldV1ServicePo.getCount();
    }

    @Override
    public HelloWorldR1K1Dto getOne(@Valid UuidDto dtoUuid) {
        return helloWorldV1ServicePo.getOne(dtoUuid);
    }

    @Override
    public HelloWorldR1K1ListDto getBatch(@Valid UuidListDto dtoUuidList) {
        return helloWorldV1ServicePo.getBatch(dtoUuidList);
    }

    @Override
    public HelloWorldR1K1ListDto getByPage(@Valid PageRequestDto dtoPageRequest) {
        return helloWorldV1ServicePo.getByPage(dtoPageRequest);
    }

    @Override
    public UuidListDto getUuidByPage(@Valid PageRequestDto dtoPageRequest) {
        return helloWorldV1ServicePo.getUuidByPage(dtoPageRequest);
    }

    @Override
    public CountResultDto selectCount() {
        return helloWorldV1ServicePo.selectCount();
    }

    @Override
    public CountResultDto selectCountRemoved() {
        return helloWorldV1ServicePo.selectCountRemoved();
    }

    @Override
    public HelloWorldR1K2Dto selectOne(@Valid IdExtDto dtoIdExt) {
        return helloWorldV1ServicePo.selectOne(dtoIdExt);
    }

    @Override
    public HelloWorldR1K2ListDto selectBatch(@Valid IdExtListDto dtoIdExtList) {
        return helloWorldV1ServicePo.selectBatch(dtoIdExtList);
    }

    @Override
    public HelloWorldR1K2ListDto selectByPage(@Valid PageRequestDto dtoPageRequest) {
        return helloWorldV1ServicePo.selectByPage(dtoPageRequest);
    }

    @Override
    public IdExtListDto selectIdExtByPage(@Valid PageRequestDto dtoPageRequest) {
        return helloWorldV1ServicePo.selectIdExtByPage(dtoPageRequest);
    }

    @Override
    public IdExtListDto selectIdExtRemoved() {
        return helloWorldV1ServicePo.selectIdExtRemoved();
    }

}
