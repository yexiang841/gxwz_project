package com.q3d.demo.svc.hello.config.local.swagger;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.actuate.autoconfigure.endpoint.web.CorsEndpointProperties;
import org.springframework.boot.actuate.autoconfigure.endpoint.web.WebEndpointProperties;
import org.springframework.boot.actuate.autoconfigure.web.server.ManagementPortType;
import org.springframework.boot.actuate.endpoint.ExposableEndpoint;
import org.springframework.boot.actuate.endpoint.web.EndpointLinksResolver;
import org.springframework.boot.actuate.endpoint.web.EndpointMapping;
import org.springframework.boot.actuate.endpoint.web.EndpointMediaTypes;
import org.springframework.boot.actuate.endpoint.web.ExposableWebEndpoint;
import org.springframework.boot.actuate.endpoint.web.WebEndpointsSupplier;
import org.springframework.boot.actuate.endpoint.web.annotation.ControllerEndpointsSupplier;
import org.springframework.boot.actuate.endpoint.web.annotation.ServletEndpointsSupplier;
import org.springframework.boot.actuate.endpoint.web.servlet.WebMvcEndpointHandlerMapping;
import org.springframework.context.annotation.Bean;
import org.springframework.core.env.Environment;
import org.springframework.util.StringUtils;
// import org.springframework.context.annotation.Configuration;
// import org.springframework.web.bind.annotation.RestController;

// import springfox.documentation.builders.ApiInfoBuilder;
// import springfox.documentation.builders.PathSelectors;
// import springfox.documentation.builders.RequestHandlerSelectors;
// import springfox.documentation.service.ApiInfo;
// import springfox.documentation.service.Contact;
// import springfox.documentation.spi.DocumentationType;
// import springfox.documentation.spring.web.plugins.Docket;

@SuppressWarnings(value = "all") /** 废弃代码忽略所有警告 */
// @Configuration /** 作用于 Bean 类的属性配置 */
/**
 * 手动配置 Swagger 的方式已废弃，现改用 SpringDoc
 * 
 * @author 叶湘 ( weixin:yexiang841, email:yexiang841@qq.com )
 */
public class SwaggerConfig {

        @Value(value = "${spring.application.name}") /** 读取配置文件（或远程配置）数据 */
        public String applicationName;

        @Value(value = "${spring.profiles.active}") /** 读取配置文件（或远程配置）数据 */
        public String profilesActive;

        /**
         * 配置基本信息
         * 
         * @return ApiInfo
         */
        // @Bean
        // public ApiInfo apiInfo() {
        // return new ApiInfoBuilder().title("Q3D Service API Docs")
        // .description("Module：" + applicationName)
        // .contact(new Contact("叶湘", "weixin:yexiang841", "yexiang841@qq.com"))
        // .version("1.0.0").build();
        // }

        /**
         * 配置文档生成最佳实践
         * 
         * @param apiInfo
         * @return Docket
         */
        // @Bean
        // public Docket createRestApi(ApiInfo apiInfo) {
        // return new Docket(DocumentationType.OAS_30)
        // .apiInfo(apiInfo)
        // .groupName(profilesActive)
        // .select()
        // .apis(RequestHandlerSelectors.withClassAnnotation(RestController.class))
        // .paths(PathSelectors.any())
        // .build();
        // }

        /**
         * 增加如下配置以解决SpringBoot 2.6.x 与Swagger 3.0.0 不兼容问题
         **/
        @Bean
        public WebMvcEndpointHandlerMapping webEndpointServletHandlerMapping(WebEndpointsSupplier webEndpointsSupplier,
                        ServletEndpointsSupplier servletEndpointsSupplier,
                        ControllerEndpointsSupplier controllerEndpointsSupplier,
                        EndpointMediaTypes endpointMediaTypes, CorsEndpointProperties corsProperties,
                        WebEndpointProperties webEndpointProperties, Environment environment) {
                List<ExposableEndpoint<?>> allEndpoints = new ArrayList<ExposableEndpoint<?>>();
                Collection<ExposableWebEndpoint> webEndpoints = webEndpointsSupplier.getEndpoints();
                allEndpoints.addAll(webEndpoints);
                allEndpoints.addAll(servletEndpointsSupplier.getEndpoints());
                allEndpoints.addAll(controllerEndpointsSupplier.getEndpoints());
                String basePath = webEndpointProperties.getBasePath();
                EndpointMapping endpointMapping = new EndpointMapping(basePath);
                boolean shouldRegisterLinksMapping = this.shouldRegisterLinksMapping(webEndpointProperties, environment,
                                basePath);
                return new WebMvcEndpointHandlerMapping(endpointMapping, webEndpoints, endpointMediaTypes,
                                corsProperties.toCorsConfiguration(), new EndpointLinksResolver(allEndpoints, basePath),
                                shouldRegisterLinksMapping, null);
        }

        private boolean shouldRegisterLinksMapping(WebEndpointProperties webEndpointProperties, Environment environment,
                        String basePath) {
                return webEndpointProperties.getDiscovery().isEnabled() && (StringUtils.hasText(basePath)
                                || ManagementPortType.get(environment).equals(ManagementPortType.DIFFERENT));
        }
}
