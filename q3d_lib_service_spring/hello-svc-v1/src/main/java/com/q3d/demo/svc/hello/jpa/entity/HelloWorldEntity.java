package com.q3d.demo.svc.hello.jpa.entity;

import java.util.Date;

import javax.persistence.*;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.springframework.data.annotation.CreatedDate;

import lombok.*;

@Data /**
       * Lombok @Data =
       * 
       * @Getter +
       * @Setter +
       * @ToString +
       * @EqualsAndHashCode +
       * @RequiredArgsConstruct
       */
@AllArgsConstructor /** Lombok 全参构造方法 */
@NoArgsConstructor /** Lombok 无参构造方法 */
@Builder /** Lombok 链式 Builder 构造方法，当基类和子类都用 @Data 时，两者都要从 @Builder 改成 @SuperBuilder */
@Entity /** 表示这是一个 JPA 实体类 */
@Table(name = "t_hello_world", uniqueConstraints = @UniqueConstraint(columnNames = "uuid"))
@DynamicInsert /** 插入字段为 null 是使用数据库默认值 */
@DynamicUpdate /** 更新字段为 null 是使用数据库默认值 */
/**
 * @author 叶湘 ( weixin:yexiang841, email:yexiang841@qq.com )
 */
public class HelloWorldEntity {

    // @GeneratedValue(strategy = GenerationType.IDENTITY) /** ID 生成策略：自增 ID */
    // @GeneratedValue(strategy = GenerationType.TABLE) /** ID 生成策略：使用专门的 ID 表生成 */
    // @GeneratedValue(strategy = GenerationType.SEQUENCE) /** ID 生成策略：由的库序列生成 */
    // @GeneratedValue(strategy = GenerationType.AUTO, generator = "uid_generator")
    // /** ID 生成策略：自定义 ID */
    // @GenericGenerator(name = "uid_generator", strategy =
    // "com.q3d.demo.svc.hello.jpa.generator.JpaUidGenerator")
    @Id /** 此属性为主键 */
    @Column(name = "id", columnDefinition = "bigint(20) NOT NULL COMMENT '全局唯一 ID'")
    protected Long id;

    /** guid 对应：MySQL 的 uuid()，ORACLE 的 rawtohex(sys_guid()) */
    @Column(name = "uuid", columnDefinition = "char(32) CHARACTER SET ascii NOT NULL COMMENT '全局唯一 UUID，去掉 4 位横杠的 32 位十六进制数'")
    protected String uuid;

    @Column(name = "active", columnDefinition = "smallint(4) NOT NULL DEFAULT 1 COMMENT '可用性标记，0：已删除，1：正常数据'")
    protected Short active;

    @Column(name = "world_name", columnDefinition = "varchar(64) NOT NULL COMMENT '元宇宙名字'")
    protected String worldName;

    @Column(name = "world_desc", columnDefinition = "int(10) NOT NULL DEFAULT 0 COMMENT '元宇宙年龄'")
    protected String worldDesc;

    @Column(name = "world_age", columnDefinition = "varchar(255) DEFAULT NULL COMMENT '元宇宙描述'")
    protected Integer worldAge;

    @Column(name = "world_radius", columnDefinition = "float(12,2) NOT NULL DEFAULT 0 COMMENT '元宇宙半径'")
    protected Float worldRadius;

    @Column(name = "world_weight", columnDefinition = "double(22,4) NOT NULL DEFAULT 0 COMMENT '元宇宙质量'")
    protected Double worldWeight;

    @Column(name = "create_time", columnDefinition = "datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间'")
    @CreationTimestamp
    @CreatedDate
    protected Date createTime;

}