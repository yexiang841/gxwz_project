package com.q3d.demo.svc.hello.controller;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.q3d.demo.api.hello.controller.HelloWorldControllerBase;
import com.q3d.demo.svc.hello.jpa.service.HelloWorldServiceJpa;

import io.swagger.v3.oas.annotations.tags.Tag;

@RequestMapping("/api/helloworld/jpa") /** 匹配路径 */
@Tag(name = "HelloWorld 接口：JPA读写，HTTP 调用，简化 RESTful 规范，不使用响应码，也不遵循名词规范") /** SpringDoc 类注解 */
@RestController
/**
 * @author 叶湘 ( weixin:yexiang841, email:yexiang841@qq.com )
 */
public class HelloWorldControllerJpa extends HelloWorldControllerBase {

  @Autowired
  private HelloWorldServiceJpa helloWorldServiceJpa;

  @PostConstruct /** 在构造方法之后自动执行 */
  public void initialize() {
    this.helloWorldServiceInterface = helloWorldServiceJpa;
  }

}
