package com.q3d.demo.svc.hello.controller;

import com.q3d.demo.common.lib.fsguid.dto.FsgUidDto;
import com.q3d.demo.common.lib.service.response.ResponseBase;
import com.q3d.demo.svc.hello.fsguid.manager.FsgUidManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;

@RequestMapping("/api/fsguid") /** 匹配路径 */
@Tag(name = "百度分布式 ID 生成器接口") /** SpringDoc 类注解 */
@RestController /** 注解为 RESTful 接口控制器 */
/**
 * @author 叶湘 ( weixin:yexiang841, email:yexiang841@qq.com )
 */
public class FsgUidController {

    @Autowired
    private FsgUidManager fsgUidManager;

    @PostMapping(value = "/next")
    @Operation(summary = "获取一个 UID", description = "生成一个具有时序性的 UID，该 UID 可以通过 parse 接口反解")
    public ResponseBase<FsgUidDto> next() {
        // 数据操作
        FsgUidDto dtoFsgUid = fsgUidManager.getUidR1Dto();
        // 结构化返回
        return ResponseBase.success(dtoFsgUid);
    }

    @GetMapping(value = "/parse/{uid}")
    @Operation(summary = "解析一个 UID", description = "会得到时间戳、机器码、序列号")
    public ResponseBase<FsgUidDto> parseByPath(@PathVariable("uid") Long uid) {
        // 数据操作
        FsgUidDto dtoFsgUid = fsgUidManager.parse(uid);
        // 结构化返回
        return ResponseBase.success(dtoFsgUid);
    }

}
