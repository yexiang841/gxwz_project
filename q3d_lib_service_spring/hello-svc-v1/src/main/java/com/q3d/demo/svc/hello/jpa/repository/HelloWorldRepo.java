package com.q3d.demo.svc.hello.jpa.repository;

import com.q3d.demo.svc.hello.jpa.entity.HelloWorldEntity;

import java.math.BigInteger;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
/**
 * @author 叶湘 ( weixin:yexiang841, email:yexiang841@qq.com )
 */
public interface HelloWorldRepo extends JpaRepository<HelloWorldEntity, Long> {

    @Modifying(clearAutomatically = true, flushAutomatically = true) /** 修改前 Flush，修改后清缓存 */
    @Query(value = "UPDATE t_hello_world helloWorld SET helloWorld.active = :active WHERE helloWorld.uuid = :uuid", nativeQuery = true)
    @Transactional(rollbackFor = Exception.class) /** 利用 JDBC 的机制实现事务 */
    /**
     * 按 UUID 修改删除标志位
     * 
     * @param uuid   全局唯一 UUID
     * @param active 可用性标志
     * @return int
     */
    int updateActiveByUuid(@Param("uuid") String uuid, @Param("active") Short active);

    @Modifying
    @Query(value = "UPDATE t_hello_world helloWorld SET helloWorld.active = :active WHERE helloWorld.uuid IN (:uuidList)", nativeQuery = true)
    @Transactional(rollbackFor = Exception.class) /** 利用 JDBC 的机制实现事务 */
    /**
     * 按 UUID 列表批量修改标志位
     * 
     * @param uuidList UUID 列表
     * @param active   可用性标志
     * @return int
     */
    int updateActiveByUuidBatch(@Param("uuidList") List<String> uuidList, @Param("active") Short active);

    @Modifying(clearAutomatically = true, flushAutomatically = true) /** 修改前 Flush，修改后清缓存 */
    @Query(value = "UPDATE t_hello_world helloWorld SET helloWorld.active = :active WHERE helloWorld.id = :id", nativeQuery = true)
    @Transactional(rollbackFor = Exception.class) /** 利用 JDBC 的机制实现事务 */
    /**
     * 按 UUID 修改标志位
     * 
     * @param id     全局唯一 ID
     * @param active 可用性标志
     * @return int
     */
    int updateActiveById(@Param("id") Long id, @Param("active") Short active);

    @Modifying
    @Query(value = "UPDATE t_hello_world helloWorld SET helloWorld.active = :active WHERE helloWorld.id IN (:idList)", nativeQuery = true)
    @Transactional(rollbackFor = Exception.class) /** 利用 JDBC 的机制实现事务 */
    /**
     * 按 ID 列表批量修改标志位
     * 
     * @param idList ID 列表
     * @param active 可用性标志
     * @return int
     */
    int updateActiveByIdBatch(@Param("idList") List<Long> idList, @Param("active") Short active);

    /**
     * 按 UUID 查询一条记录
     * 
     * @param uuid 全局唯一 UUID
     * @return HelloWorldEntity
     */
    HelloWorldEntity findByUuid(String uuid);

    /**
     * 分页查询记录
     * 即 active 为 0 的记录；
     * 
     * @param pageable 分页参数
     * @return List<HelloWorldEntity>
     */
    Page<HelloWorldEntity> findByActiveFalse(Pageable pageable);

    /**
     * 按 UUID 查询一条记录
     * 即 active 为 0 的记录；
     * 
     * @param uuid 全局唯一 UUID
     * @return HelloWorldEntity
     */
    HelloWorldEntity findByActiveFalseAndUuidIs(String uuid);

    /**
     * 按 UUID 列表批量查询记录
     * 
     * @param uuidList 全局唯一 UUID 列表
     * @param sort     排序方式
     * @return List<HelloWorldEntity>
     */
    List<HelloWorldEntity> findByActiveFalseAndUuidIn(List<String> uuidList, Sort sort);

    /**
     * 按 ID 列表批量查询记录
     * 
     * @param idList 全局唯一 ID 列表
     * @param sort   排序方式
     * @return List<HelloWorldEntity>
     */
    List<HelloWorldEntity> findByIdIn(Iterable<Long> idList, Sort sort);

    /**
     * 分页查询记录
     * 
     * @param pageable 分页参数
     * @return List<HelloWorldEntity>
     */
    Page<HelloWorldEntity> findAll(Pageable pageable);

    @Query(value = "SELECT helloWorld.uuid FROM t_hello_world helloWorld WHERE helloWorld.active = 1", countQuery = "SELECT count(*) FROM t_hello_world WHERE active = 1", nativeQuery = true)
    /**
     * 分页查询 UUID 列表；
     * 
     * @param pageable 分页参数
     * @return List<String>
     */
    Page<String> findUuidByActiveFalse(Pageable pageable);

    @Query(value = "SELECT helloWorld.id FROM t_hello_world helloWorld", countQuery = "SELECT count(*) FROM t_hello_world", nativeQuery = true)
    /**
     * 分页查询 ID 列表；
     * 
     * @param pageable 分页参数
     * @return List<Long>
     */
    Page<BigInteger> findId(Pageable pageable);

    @Query(value = "SELECT helloWorld.id FROM t_hello_world helloWorld WHERE helloWorld.active = 0 ORDER BY helloWorld.id asc", nativeQuery = true)
    /**
     * 查询被标记删除的全部记录的 ID 列表；
     * 
     * @return List<Long>
     */
    List<Long> findIdByActiveFalse();

}
