package com.q3d.demo.svc.hello.mybatis.mapperext;

import org.apache.ibatis.jdbc.SQL;

import com.q3d.demo.svc.hello.mybatis.mapper.HelloWorldModelSqlProvider;
import com.q3d.demo.svc.hello.mybatis.model.HelloWorldModel;
import com.q3d.demo.svc.hello.mybatis.model.HelloWorldModelExample;

/**
 * HelloWorldModelSqlProvider 是由 MyBatis-Generator 生成的，对于需要订制的部分，在这里继承和重写
 * 
 * @author 叶湘 ( weixin:yexiang841, email:yexiang841@qq.com )
 */
public class HelloWorldModelSqlProviderExt extends HelloWorldModelSqlProvider {

    /**
     * 为了修改 uuid 和 create_time 为数据库自动生成而重写.
     *
     * @param record
     * @return int
     */
    @Override
    public String insertSelective(HelloWorldModel record) {
        SQL sql = new SQL();
        sql.INSERT_INTO("t_hello_world");

        if (record.getId() != null) {
            sql.VALUES("id", "#{id,jdbcType=BIGINT}");
        }

        sql.VALUES("uuid", "replace(uuid(),'-','')");

        sql.VALUES("active", "1");

        if (record.getWorldName() != null) {
            sql.VALUES("world_name", "#{worldName,jdbcType=VARCHAR}");
        }

        if (record.getWorldDesc() != null) {
            sql.VALUES("world_desc", "#{worldDesc,jdbcType=VARCHAR}");
        }

        if (record.getWorldAge() != null) {
            sql.VALUES("world_age", "#{worldAge,jdbcType=INTEGER}");
        }

        if (record.getWorldRadius() != null) {
            sql.VALUES("world_radius", "#{worldRadius,jdbcType=REAL}");
        }

        if (record.getWorldWeight() != null) {
            sql.VALUES("world_weight", "#{worldWeight,jdbcType=DOUBLE}");
        }

        sql.VALUES("create_time", "current_timestamp()");

        return sql.toString();
    }

    /**
     * 只查询符合条件的 UUID 的 SQL 语句
     *
     * @param example
     * @return int
     */
    public String selectUuidByExample(HelloWorldModelExample example) {
        SQL sql = new SQL();
        if (example != null && example.isDistinct()) {
            sql.SELECT_DISTINCT("uuid");
        } else {
            sql.SELECT("uuid");
        }
        sql.FROM("t_hello_world");
        applyWhere(sql, example, false);

        if (example != null && example.getOrderByClause() != null) {
            sql.ORDER_BY(example.getOrderByClause());
        }

        return sql.toString();
    }

    /**
     * 只查询符合条件的 ID 的 SQL 语句
     *
     * @param example
     * @return int
     */
    public String selectIdExtByExample(HelloWorldModelExample example) {
        SQL sql = new SQL();
        if (example != null && example.isDistinct()) {
            sql.SELECT_DISTINCT("id");
        } else {
            sql.SELECT("id");
        }
        sql.FROM("t_hello_world");
        applyWhere(sql, example, false);

        if (example != null && example.getOrderByClause() != null) {
            sql.ORDER_BY(example.getOrderByClause());
        }

        return sql.toString();
    }

}
