package com.q3d.demo.svc.hello.jpa.service;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

import com.q3d.demo.common.lib.service.dto.CountResultDto;
import com.q3d.demo.common.lib.service.dto.IdExtDto;
import com.q3d.demo.common.lib.service.dto.IdExtListDto;
import com.q3d.demo.common.lib.service.dto.OpResultDto;
import com.q3d.demo.common.lib.service.dto.PageRequestDto;
import com.q3d.demo.common.lib.service.dto.StringDto;
import com.q3d.demo.common.lib.service.dto.UuidDto;
import com.q3d.demo.common.lib.service.dto.UuidListDto;
import com.q3d.demo.common.lib.service.exception.ServiceException;
import com.q3d.demo.common.lib.service.response.ResponseStatus;
import com.q3d.demo.api.hello.service.HelloWorldServiceInterface;
import com.q3d.demo.api.hello.dto.HelloWorldP1Dto;
import com.q3d.demo.api.hello.dto.HelloWorldP1K1Dto;
import com.q3d.demo.api.hello.dto.HelloWorldP1K2Dto;
import com.q3d.demo.api.hello.dto.HelloWorldP1ListDto;
import com.q3d.demo.api.hello.dto.HelloWorldR1K1Dto;
import com.q3d.demo.api.hello.dto.HelloWorldR1K1ListDto;
import com.q3d.demo.api.hello.dto.HelloWorldR1K2Dto;
import com.q3d.demo.api.hello.dto.HelloWorldR1K2ListDto;
import com.q3d.demo.svc.hello.fsguid.manager.FsgUidManager;
import com.q3d.demo.svc.hello.jpa.entity.HelloWorldEntity;
import com.q3d.demo.svc.hello.jpa.repository.HelloWorldRepo;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j /** Lombok 的 Logger 注解 */
@Validated /** Controller 中某些接口的入参可能没有封装 DTO 进行校验，因此在 Service 层再做一次校验 */
/**
 * @author 叶湘 ( weixin:yexiang841, email:yexiang841@qq.com )
 */
public class HelloWorldServiceJpa implements HelloWorldServiceInterface {

    @Autowired
    private HelloWorldRepo helloWorldRepo;

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private FsgUidManager fsgUidManager;

    @Override
    public StringDto hello(@Valid StringDto dtoStringRequest) {
        StringDto dtoStringResponse = StringDto.builder().text(">>>> Hello " + dtoStringRequest.getText() + " <<<<")
                .build();
        return dtoStringResponse;
    }

    @Override
    public UuidDto addOne(@Valid HelloWorldP1Dto dtoP1) {
        HelloWorldEntity entityRequest = null;
        HelloWorldEntity entityResponse = null;
        try {
            // 使用 ModelMapper 工具将 DTO 转换成 Entity
            entityRequest = modelMapper.map(dtoP1, HelloWorldEntity.class);
            // 生成全局唯一 ID
            Long uid = fsgUidManager.getUid();
            entityRequest.setId(uid);
            // 生成全局唯一 UUID
            String uuid = UUID.randomUUID().toString().replaceAll("\\-", "");
            entityRequest.setUuid(uuid);
            // 执行数据库写操作
            entityResponse = helloWorldRepo.saveAndFlush(entityRequest);
            log.debug(
                    "addOne_dto: " + dtoP1.toString() + " opResult: " + (entityResponse.getUuid() != null) + " model: "
                            + entityRequest);
        } catch (DataIntegrityViolationException e) {
            log.warn(e.getMessage());
            throw new ServiceException(ResponseStatus.BAD_REQUEST_PARAM_DATA_ERROR, e.getCause().getMessage());
        }
        // 组装 DTO 响应结构体
        UuidDto dtoUuid = UuidDto.builder().uuid(entityResponse.getUuid()).build();
        return dtoUuid;
    }

    @Override
    public UuidListDto addBatch(@Valid HelloWorldP1ListDto dtoP1List) {
        // 组装 List
        List<HelloWorldEntity> entityP1List = new ArrayList<HelloWorldEntity>(dtoP1List.getDtoList().size());
        for (HelloWorldP1Dto dtoP1 : dtoP1List.getDtoList()) {
            try {
                // 使用 ModelMapper 工具将 DTO 转换成 Entity
                HelloWorldEntity entityP1 = modelMapper.map(dtoP1, HelloWorldEntity.class);
                // 生成全局唯一 ID
                Long uid = fsgUidManager.getUid();
                entityP1.setId(uid);
                // 生成全局唯一 ID
                String uuid = UUID.randomUUID().toString().replaceAll("\\-", "");
                entityP1.setUuid(uuid);
                entityP1List.add(entityP1);
            } catch (DataIntegrityViolationException e) {
                log.warn(e.getMessage());
                // 只要有一个参数不对，就会抛出异常，不会写入任何一个
                throw new ServiceException(ResponseStatus.BAD_REQUEST_PARAM_DATA_ERROR, e.getCause().getMessage());
            }
        }
        // 组装 DTO 响应结构体
        UuidListDto dtoUuidList = UuidListDto.builder().dtoList(new ArrayList<UuidDto>()).build();
        for (HelloWorldEntity entityP1 : entityP1List) {
            try {
                // 执行数据库写操作
                HelloWorldEntity entityR1 = helloWorldRepo.saveAndFlush(entityP1);
                if (entityR1.getUuid() != null) {
                    UuidDto dtoUuid = UuidDto.builder().uuid(entityR1.getUuid()).build();
                    dtoUuidList.getDtoList().add(dtoUuid);
                }
                log.debug("addBatch_model: " + entityP1.toString() + " opResult: " + (entityR1.getUuid() != null));
            } catch (DataAccessException e) {
                // 这里不再抛出异常，成功一个算一个，不成功的跳过
                log.warn(e.getMessage());
            }
        }
        return dtoUuidList;
    }

    @Override
    public OpResultDto removeOne(@Valid UuidDto dtoUuid) {
        // 执行数据库写操作
        int opResult = helloWorldRepo.updateActiveByUuid(dtoUuid.getUuid(), (short) 0);
        log.debug("removeOne_by_uuid: " + dtoUuid.getUuid() + " opResult: " + opResult);
        // 组装 DTO 响应结构体
        OpResultDto dtoOpResult = OpResultDto.builder().opResult(opResult).build();
        return dtoOpResult;
    }

    @Override
    public OpResultDto removeBatch(@Valid UuidListDto dtoUuidList) {
        // 组装 List
        List<String> uuidList = new ArrayList<String>(dtoUuidList.getDtoList().size());
        for (UuidDto dtoUuid : dtoUuidList.getDtoList()) {
            uuidList.add(dtoUuid.getUuid());
        }
        // 执行数据库写操作，只修改标志位
        int opResult = helloWorldRepo.updateActiveByUuidBatch(uuidList, (short) 0);
        log.debug("removeBatch_by_uuid_list: " + uuidList + " opResult: " + opResult);
        // 组装 dto 响应结构体
        OpResultDto dtoOpResult = OpResultDto.builder().opResult(opResult).build();
        return dtoOpResult;
    }

    @Override
    public OpResultDto deleteOne(@Valid IdExtDto dtoIdExt) {
        Long id = Long.parseLong(dtoIdExt.getIdExt());
        // 执行数据库写操作，按 ID 删除
        helloWorldRepo.deleteById(id);
        log.debug("deleteOne_by_id: " + id + " opResult: 1");
        // 组装 DTO 响应结构体
        OpResultDto dtoOpResult = OpResultDto.builder().opResult(1).build();
        return dtoOpResult;
    }

    @Override
    public OpResultDto deleteBatch(@Valid IdExtListDto dtoIdExtList) {
        // 组装 List
        List<Long> idList = new ArrayList<Long>(dtoIdExtList.getDtoList().size());
        for (IdExtDto dtoIdExt : dtoIdExtList.getDtoList()) {
            idList.add(Long.parseLong(dtoIdExt.getIdExt()));
        }
        // 执行数据库写操作，按 ID 列表删除
        helloWorldRepo.deleteAllById(idList);
        log.debug("deleteBatch_by_id_list: " + idList + " opResult: " + idList.size());
        // 组装 DTO 响应结构体
        OpResultDto dtoOpResult = OpResultDto.builder().opResult(idList.size()).build();
        return dtoOpResult;
    }

    @Override
    public OpResultDto reviveOne(@Valid IdExtDto dtoIdExt) {
        // 执行数据库写操作
        int opResult = helloWorldRepo.updateActiveById(Long.valueOf(dtoIdExt.getIdExt()), (short) 1);
        log.debug("reviveOne_by_uuid: " + dtoIdExt.getIdExt() + " opResult: " + opResult);
        // 组装 DTO 响应结构体
        OpResultDto dtoOpResult = OpResultDto.builder().opResult(opResult).build();
        return dtoOpResult;
    }

    @Override
    public OpResultDto reviveBatch(@Valid IdExtListDto dtoIdExtList) {
        // 组装 List
        List<Long> idList = new ArrayList<Long>(dtoIdExtList.getDtoList().size());
        for (IdExtDto dtoidExt : dtoIdExtList.getDtoList()) {
            idList.add(Long.valueOf(dtoidExt.getIdExt()));
        }
        // 执行数据库写操作，只修改标志位
        int opResult = helloWorldRepo.updateActiveByIdBatch(idList, (short) 1);
        log.debug("reviveBatch_by_id_list: " + idList + " opResult: " + opResult);
        // 组装 dto 响应结构体
        OpResultDto dtoOpResult = OpResultDto.builder().opResult(opResult).build();
        return dtoOpResult;
    }

    @Override
    public OpResultDto editSelective(@Valid HelloWorldP1K1Dto dtoP1K1) {
        // 读出原 Entity
        HelloWorldEntity entityRequest = helloWorldRepo.findByUuid(dtoP1K1.getUuid());
        // 使用 ModelMapper 工具将 DTO 的属性写入原 Entity
        modelMapper.map(dtoP1K1, entityRequest);
        // 执行数据库写操作
        helloWorldRepo.save(entityRequest);
        log.debug("editSelective_dto: " + dtoP1K1.toString() + " opResult: 1");
        // 组装 DTO 响应结构体
        OpResultDto dtoOpResult = OpResultDto.builder().opResult(1).build();
        return dtoOpResult;
    }

    @Override
    public OpResultDto updateSelective(@Valid HelloWorldP1K2Dto dtoP1K2) {
        // 读出原 Entity
        HelloWorldEntity entityRequest = helloWorldRepo.findById(Long.valueOf(dtoP1K2.getIdExt())).orElseThrow();
        // 使用 ModelMapper 工具将 DTO 的属性写入原 Entity
        modelMapper.map(dtoP1K2, entityRequest);
        // 执行数据库写操作
        helloWorldRepo.save(entityRequest);
        log.debug("updateSelective_dto: " + dtoP1K2.toString() + " opResult: 1");
        // 组装 DTO 响应结构体
        OpResultDto dtoOpResult = OpResultDto.builder().opResult(1).build();
        return dtoOpResult;
    }

    @Override
    public CountResultDto getCount() {
        // 使用 Example 构造查询条件
        HelloWorldEntity entityExample = HelloWorldEntity.builder().active(Short.valueOf((short) 1)).build();
        Example<HelloWorldEntity> example = Example.of(entityExample);
        // 执行数据库读操作
        long countResult = helloWorldRepo.count(example);
        // 组装 DTO 响应结构体
        CountResultDto dtoCountResult = CountResultDto.builder().countResult(Long.valueOf(countResult)).build();
        log.debug("getCount_size: " + dtoCountResult.getCountResult());
        return dtoCountResult;
    }

    @Override
    public HelloWorldR1K1Dto getOne(@Valid UuidDto dtoUuid) {
        // 执行数据库读操作，按 UUID 查询
        HelloWorldEntity entityResponse = helloWorldRepo.findByActiveFalseAndUuidIs(dtoUuid.getUuid());
        // 有可能查询不到，或者 active 为 0
        if (entityResponse == null) {
            log.warn("getOne_by_uuid: " + dtoUuid.getUuid() + " model: 0");
            return null;
        }
        // 使用 ModelMapper 工具将 Entity 转换成 Dto
        HelloWorldR1K1Dto dtoR1K1 = modelMapper.map(entityResponse, HelloWorldR1K1Dto.class);
        // DTO 中使用 Long 类型时间，手动转换
        dtoR1K1.setCreateTimeExt(entityResponse.getCreateTime().getTime());
        log.debug("getOne_by_uuid: " + dtoUuid.getUuid() + " model: " + entityResponse);
        return dtoR1K1;
    }

    @Override
    public HelloWorldR1K1ListDto getBatch(@Valid UuidListDto dtoUuidList) {
        // 组装 List
        List<String> uuidList = new ArrayList<String>(dtoUuidList.getDtoList().size());
        for (UuidDto dtoUuid : dtoUuidList.getDtoList()) {
            uuidList.add(dtoUuid.getUuid());
        }
        // 执行数据库读操作，按 UUID 列表查询
        List<HelloWorldEntity> entityList = helloWorldRepo.findByActiveFalseAndUuidIn(
                uuidList,
                Sort.by(Sort.Direction.ASC, "id"));
        // 组装 DTO 响应结构体
        HelloWorldR1K1ListDto dtoR1K1List = HelloWorldR1K1ListDto.builder()
                .dtoList(new ArrayList<HelloWorldR1K1Dto>(entityList.size())).build();
        for (HelloWorldEntity entity : entityList) {
            // 使用 ModelMapper 工具将 Entity 转换成 Dto
            HelloWorldR1K1Dto dtoR1K1 = modelMapper.map(entity, HelloWorldR1K1Dto.class);
            // DTO 中使用 Long 类型时间，手动转换
            dtoR1K1.setCreateTimeExt(entity.getCreateTime().getTime());
            dtoR1K1List.getDtoList().add(dtoR1K1);
        }
        log.debug("getBatch_by_uuid_list: " + uuidList + " dtoList.size: " + dtoR1K1List.getDtoList().size());
        return dtoR1K1List;
    }

    @Override
    public HelloWorldR1K1ListDto getByPage(@Valid PageRequestDto dtoPageRequest) {
        int page = dtoPageRequest.getPage();
        int limit = dtoPageRequest.getLimit();
        PageRequest pageRequest = PageRequest.of(page, limit, Sort.by(Sort.Direction.ASC, "id"));
        // 执行数据库读操作，分页查询
        Page<HelloWorldEntity> entityPage = helloWorldRepo.findByActiveFalse(pageRequest);
        // 组装 DTO 响应结构体
        HelloWorldR1K1ListDto dtoR1K1List = HelloWorldR1K1ListDto.builder()
                .dtoList(new ArrayList<HelloWorldR1K1Dto>(entityPage.getContent().size()))
                .build();
        for (HelloWorldEntity entity : entityPage.getContent()) {
            // 使用 ModelMapper 工具将 Entity 转换成 Dto
            HelloWorldR1K1Dto dtoR1K1 = modelMapper.map(entity, HelloWorldR1K1Dto.class);
            // DTO 中使用 Long 类型时间，手动转换
            dtoR1K1.setCreateTimeExt(entity.getCreateTime().getTime());
            dtoR1K1List.getDtoList().add(dtoR1K1);
        }
        log.debug("getByPage_page: " + dtoPageRequest.getPage() + " limit: " +
                dtoPageRequest.getLimit()
                + " dtoList.size: "
                + dtoR1K1List.getDtoList().size());
        return dtoR1K1List;
    }

    @Override
    public UuidListDto getUuidByPage(@Valid PageRequestDto dtoPageRequest) {
        int page = dtoPageRequest.getPage();
        int limit = dtoPageRequest.getLimit();
        PageRequest pageRequest = PageRequest.of(page, limit, Sort.by(Sort.Direction.ASC, "id"));
        // 执行数据库读操作，分页查询
        Page<String> uuidPage = helloWorldRepo.findUuidByActiveFalse(pageRequest);
        // 组装 DTO 响应结构体
        UuidListDto dtoUuidList = UuidListDto.builder()
                .dtoList(new ArrayList<UuidDto>(uuidPage.getContent().size())).build();
        for (String uuid : uuidPage.getContent()) {
            UuidDto dtoUuid = UuidDto.builder().uuid(uuid).build();
            dtoUuidList.getDtoList().add(dtoUuid);
        }
        log.debug("getUuidByPage_page: " + dtoPageRequest.getPage() + " limit: "
                +
                dtoPageRequest.getLimit()
                + " dtoList.size: "
                + dtoUuidList.getDtoList().size());
        return dtoUuidList;
    }

    @Override
    public CountResultDto selectCount() {
        // 执行数据库读操作
        long countResult = helloWorldRepo.count();
        // 组装 DTO 响应结构体
        CountResultDto dtoCountResult = CountResultDto.builder().countResult(Long.valueOf(countResult)).build();
        log.debug("selectCount_size: " + dtoCountResult.getCountResult());
        return dtoCountResult;
    }

    @Override
    public CountResultDto selectCountRemoved() {
        // 使用 Example 构造查询条件
        HelloWorldEntity entityExample = HelloWorldEntity.builder().active(Short.valueOf((short) 0)).build();
        Example<HelloWorldEntity> example = Example.of(entityExample);
        // 执行数据库读操作
        long countResult = helloWorldRepo.count(example);
        // 组装 DTO 响应结构体
        CountResultDto dtoCountResult = CountResultDto.builder().countResult(Long.valueOf(countResult)).build();
        log.debug("selectCountRemoved_size: " + dtoCountResult.getCountResult());
        return dtoCountResult;
    }

    @Override
    public HelloWorldR1K2Dto selectOne(@Valid IdExtDto dtoIdExt) {
        Long id = Long.parseLong(dtoIdExt.getIdExt());
        // 执行数据库读操作，按 ID 查询
        Optional<HelloWorldEntity> optional = helloWorldRepo.findById(id);
        if (optional.isEmpty()) {
            log.warn("selectOne_by_id: " + id + " model: 0");
            return null;
        }
        HelloWorldEntity entity = optional.get();
        // 使用 ModelMapper 工具将 Entity 转换成 Dto
        HelloWorldR1K2Dto dtoR1K2 = modelMapper.map(entity, HelloWorldR1K2Dto.class);
        // DTO 中使用 String 类型 ID，手动转换
        dtoR1K2.setIdExt(entity.getId().toString());
        // DTO 中使用 Long 类型时间，手动转换
        dtoR1K2.setCreateTimeExt(entity.getCreateTime().getTime());
        log.debug("selectOne_by_id: " + id + " model: 1");
        return dtoR1K2;
    }

    @Override
    public HelloWorldR1K2ListDto selectBatch(@Valid IdExtListDto dtoIdExtList) {
        // 组装 List
        List<Long> idList = new ArrayList<Long>(dtoIdExtList.getDtoList().size());
        for (IdExtDto dtoIdExt : dtoIdExtList.getDtoList()) {
            idList.add(Long.parseLong(dtoIdExt.getIdExt()));
        }
        // 执行数据库读操作，按 ID 列表查询
        List<HelloWorldEntity> entityList = helloWorldRepo.findByIdIn(idList, Sort.by(Sort.Direction.ASC, "id"));
        // 组装 DTO 响应结构体
        HelloWorldR1K2ListDto dtoR1K2List = HelloWorldR1K2ListDto.builder()
                .dtoList(new ArrayList<HelloWorldR1K2Dto>(entityList.size())).build();
        for (HelloWorldEntity entity : entityList) {
            // 使用 ModelMapper 工具将 Entity 转换成 Dto
            HelloWorldR1K2Dto dtoR1K2 = modelMapper.map(entity,
                    HelloWorldR1K2Dto.class);
            // DTO 中使用 String 类型 ID，手动转换
            dtoR1K2.setIdExt(entity.getId().toString());
            // DTO 中使用 Long 类型时间，手动转换
            dtoR1K2.setCreateTimeExt(entity.getCreateTime().getTime());
            dtoR1K2List.getDtoList().add(dtoR1K2);
        }
        log.debug("selectBatch_by_id_list: " + idList + " dtoList.size: "
                + dtoR1K2List.getDtoList().size());
        return dtoR1K2List;
    }

    @Override
    public HelloWorldR1K2ListDto selectByPage(@Valid PageRequestDto dtoPageRequest) {
        int page = dtoPageRequest.getPage();
        int limit = dtoPageRequest.getLimit();
        PageRequest pageRequest = PageRequest.of(page, limit, Sort.by(Sort.Direction.ASC, "id"));
        // 执行数据库读操作，分页查询
        Page<HelloWorldEntity> entityPage = helloWorldRepo.findAll(pageRequest);
        // 组装 DTO 响应结构体
        HelloWorldR1K2ListDto dtoR1K2List = HelloWorldR1K2ListDto.builder()
                .dtoList(new ArrayList<HelloWorldR1K2Dto>(entityPage.getContent().size()))
                .build();
        for (HelloWorldEntity entity : entityPage.getContent()) {
            // 使用 ModelMapper 工具将 Entity 转换成 Dto
            HelloWorldR1K2Dto dtoR1K2 = modelMapper.map(entity, HelloWorldR1K2Dto.class);
            // DTO 中使用 String 类型 ID，手动转换
            dtoR1K2.setIdExt(entity.getId().toString());
            // DTO 中使用 Long 类型时间，手动转换
            dtoR1K2.setCreateTimeExt(entity.getCreateTime().getTime());
            dtoR1K2List.getDtoList().add(dtoR1K2);
        }
        log.debug("selectByPage_page: " + dtoPageRequest.getPage() + " limit: "
                +
                dtoPageRequest.getLimit()
                + " dtoList.size: "
                + dtoR1K2List.getDtoList().size());
        return dtoR1K2List;
    }

    @Override
    public IdExtListDto selectIdExtByPage(@Valid PageRequestDto dtoPageRequest) {
        int page = dtoPageRequest.getPage();
        int limit = dtoPageRequest.getLimit();
        PageRequest pageRequest = PageRequest.of(page, limit, Sort.by(Sort.Direction.ASC, "id"));
        // 执行数据库读操作，分页查询
        Page<BigInteger> idExtPage = helloWorldRepo.findId(pageRequest);
        // 组装 DTO 响应结构体
        IdExtListDto dtoIdExtList = IdExtListDto.builder()
                .dtoList(new ArrayList<IdExtDto>(idExtPage.getContent().size())).build();
        for (BigInteger id : idExtPage.getContent()) {
            IdExtDto dtoIdExt = IdExtDto.builder().idExt(id.toString())
                    .build();
            dtoIdExtList.getDtoList().add(dtoIdExt);
        }
        log.debug("selectIdExtByPage_page: " + dtoPageRequest.getPage() + " limit: " + dtoPageRequest.getLimit()
                + " dtoList.size: " + dtoIdExtList.getDtoList().size());
        return dtoIdExtList;
    }

    @Override
    public IdExtListDto selectIdExtRemoved() {
        // 执行数据库读操作
        List<Long> entityList = helloWorldRepo.findIdByActiveFalse();
        // 组装 DTO 响应结构体
        IdExtListDto dtoIdExtList = IdExtListDto.builder()
                .dtoList(new ArrayList<IdExtDto>(entityList.size())).build();
        for (Long id : entityList) {
            IdExtDto dtoIdExt = IdExtDto.builder().idExt(id.toString())
                    .build();
            dtoIdExtList.getDtoList().add(dtoIdExt);
        }
        log.debug("selectIdExtRemoved_dtoList.size: " +
                dtoIdExtList.getDtoList().size());
        return dtoIdExtList;
    }

}