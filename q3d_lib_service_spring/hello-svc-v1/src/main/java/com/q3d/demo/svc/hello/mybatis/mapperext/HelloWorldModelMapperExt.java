package com.q3d.demo.svc.hello.mybatis.mapperext;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.InsertProvider;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.SelectKey;
import org.apache.ibatis.annotations.SelectProvider;
import org.apache.ibatis.type.JdbcType;

import com.q3d.demo.common.lib.service.dto.UuidDto;
import com.q3d.demo.svc.hello.mybatis.mapper.HelloWorldModelMapper;
import com.q3d.demo.svc.hello.mybatis.model.HelloWorldModel;
import com.q3d.demo.svc.hello.mybatis.model.HelloWorldModelExample;

/**
 * HelloWorldModelMapper 是由 MyBatis-Generator 生成的，对于需要订制的部分，在这里继承和重写
 * 
 * @author 叶湘 ( weixin:yexiang841, email:yexiang841@qq.com )
 */
public interface HelloWorldModelMapperExt extends HelloWorldModelMapper {

        /**
         * 重写部分：
         * 1、修改 uuid 和 create_time 为数据库自动生成；
         * 2、修改返回值 uuid；
         *
         * @param record
         * @return int
         */
        @Override
        @Insert({
                        "insert into t_hello_world (id, uuid, active, ",
                        "world_name, world_desc, ",
                        "world_age, world_radius, ",
                        "world_weight, create_time)",
                        "values (#{id,jdbcType=BIGINT},replace(uuid(),'-',''), 1, ",
                        "#{worldName,jdbcType=VARCHAR}, #{worldDesc,jdbcType=VARCHAR}, ",
                        "#{worldAge,jdbcType=INTEGER}, #{worldRadius,jdbcType=REAL}, ",
                        "#{worldWeight,jdbcType=DOUBLE}, current_timestamp())"
        })
        @SelectKey(statement = "SELECT uuid FROM t_hello_world WHERE id = #{id,jdbcType=BIGINT}", keyProperty = "uuid", before = false, resultType = String.class)
        int insert(HelloWorldModel record);

        /**
         * 重写部分：
         * 1、修改 uuid 和 create_time 为数据库自动生成；
         * 2、修改返回值 uuid；
         *
         * @param record
         * @return int
         */
        @Override
        @InsertProvider(type = HelloWorldModelSqlProviderExt.class, method = "insertSelective")
        @SelectKey(statement = "SELECT uuid FROM t_hello_world WHERE id = #{id,jdbcType=BIGINT}", keyProperty = "uuid", before = false, resultType = String.class)
        int insertSelective(HelloWorldModel record);

        /**
         * 只查询符合条件的 UUID
         *
         * @param example
         * @return List<HelloWorldK1Dto>
         */
        @SelectProvider(type = HelloWorldModelSqlProviderExt.class, method = "selectUuidByExample")
        @Results({
                        @Result(column = "uuid", property = "uuid", jdbcType = JdbcType.CHAR),
        })
        List<UuidDto> selectUuidByExample(HelloWorldModelExample example);

        /**
         * 只查询符合条件的 ID
         *
         * @param example
         * @return List<HelloWorldK2Dto>
         */
        @SelectProvider(type = HelloWorldModelSqlProviderExt.class, method = "selectIdExtByExample")
        @Results({
                        @Result(column = "id", property = "id", jdbcType = JdbcType.BIGINT, id = true),
        })
        List<Map<String, Long>> selectIdExtByExample(HelloWorldModelExample example);

}
