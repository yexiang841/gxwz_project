package com.q3d.demo.svc.sba;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import de.codecentric.boot.admin.server.config.EnableAdminServer;

@EnableAdminServer /** 开启 SpringBoot Admin，访问地址：http://{host}:{port}/{context-path}/ */
@SpringBootApplication
/**
 * @author 叶湘 ( weixin:yexiang841, email:yexiang841@qq.com )
 */
public class SbaSvcApplication {

	public static void main(String[] args) {
		SpringApplication.run(SbaSvcApplication.class, args);
	}

}
