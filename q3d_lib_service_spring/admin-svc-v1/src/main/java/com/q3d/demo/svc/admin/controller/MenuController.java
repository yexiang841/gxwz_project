package com.q3d.demo.svc.admin.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;

@RequestMapping("/menu/v1") /** 匹配路径 */
@Tag(name = "q3d_admin 项目获取菜单相关配置的接口") /** SpringDoc 类注解 */
@RestController
/**
 * @author 叶湘 ( weixin:yexiang841, email:yexiang841@qq.com )
 */
public class MenuController {

    @Operation(summary = "获取菜单列表")
    @GetMapping("/list")
    public String list() {
        return "[{haha:hehe}]";
    }

}
