package com.q3d.demo.svc.admin.config.local.springdoc;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.SpecVersion;
import io.swagger.v3.oas.models.info.Contact;
import io.swagger.v3.oas.models.info.Info;

@Configuration /** 作用于 Bean 类的属性配置 */
/**
 * 配置 SpringDoc
 * 开启 Swagger，访问地址：http://{host}:{port}/{context-path}/swagger-ui/index.html
 * 并同时提供 JSON 数据接口：http://{host}:{port}/{context-path}/v3/api-docs/
 * 
 * @author 叶湘 ( weixin:yexiang841, email:yexiang841@qq.com )
 */
public class SpringDocConfig {

    @Value(value = "${spring.application.name}") /** 读取配置文件（或远程配置）数据 */
    public String applicationName;

    @Value(value = "${spring.profiles.active}") /** 读取配置文件（或远程配置）数据 */
    public String profilesActive;

    @Bean
    public OpenAPI openApi() {
        return new OpenAPI().specVersion(SpecVersion.V31).info(new Info().title("Q3D Service API Docs")
                .description("Module：" + applicationName)
                .version("v1.0.0")
                .contact(new Contact()
                        .name("叶湘")
                        .url("weixin:yexiang841")
                        .email("yexiang841@qq.com")));
    }

}