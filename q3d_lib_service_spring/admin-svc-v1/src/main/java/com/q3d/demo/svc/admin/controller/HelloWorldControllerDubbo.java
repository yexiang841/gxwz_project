package com.q3d.demo.svc.admin.controller;

import javax.annotation.PostConstruct;

// import org.apache.dubbo.config.annotation.DubboReference;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.q3d.demo.api.hello.controller.HelloWorldControllerBase;
import com.q3d.demo.api.hello.service.HelloWorldServiceInterface;

import io.swagger.v3.oas.annotations.tags.Tag;

@RequestMapping("/api/helloworld/dubbo") /** 匹配路径 */
@Tag(name = "HelloWorld 远程调用测试接口：Dubbo 调用（RPC）") /** SpringDoc 类注解 */
@RestController
/**
 * @author 叶湘 ( weixin:yexiang841, email:yexiang841@qq.com )
 */
public class HelloWorldControllerDubbo extends HelloWorldControllerBase {

    // @DubboReference
    protected HelloWorldServiceInterface helloWorldV1ClientDubbo;

    @PostConstruct /** 在构造方法之后自动执行 */
    public void initialize() {
        this.helloWorldServiceInterface = helloWorldV1ClientDubbo;
    }

}
