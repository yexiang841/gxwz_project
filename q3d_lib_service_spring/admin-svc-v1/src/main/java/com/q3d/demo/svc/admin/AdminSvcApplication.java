package com.q3d.demo.svc.admin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;

import com.q3d.demo.api.hello.client.feign.HelloWorldFeignInterface;

// @EnableOpenApi /** 开启 Swagger3，访问地址：http://{host}:{port}/{context-path}/swagger-ui/index.html */
@EnableDiscoveryClient /** 开启服务发现，使 Eureka 或者 Nacos 能按照服务名寻址 */
@EnableConfigurationProperties /** 开启 @ConfigurationProperties，支持从配置文件或远程配置读取属性注入对象 */
@EnableFeignClients( /** 开启 OpenFeign 客户端 */
		basePackages = { "com.q3d.demo.api.hello.**.**" }, /** 方式一：Hello-API 包扫描路径 */
		clients = { HelloWorldFeignInterface.class } /** 方式二：直接指定 Class */
)
@ComponentScan(basePackages = { /** Bean 扫描路径，对依赖顺序也有要求 */
		"com.q3d.demo.common.lib.service.**.**", /** 引入 REST 服务所需数据结构，如封装响应结构体，统一异常处理等 */
		"com.q3d.demo.common.lib.logger.**.**", /** 引入日志增强功能，自动加服务名、端口号、请求线程标识、请求响应时间等 */
		"com.q3d.demo.common.lib.fsguid.**.**", /** 引入百度分布式 ID 所需数据结构 */
		"com.q3d.demo.common.lib.druid.**.**", /** 引入动态配置数据库线程池所需数据结构 */
		"com.q3d.demo.api.hello.**.**", /** 引入 Hello-API 中的数据结构 */
		"com.q3d.demo.svc.admin.**.**", /** 本服务扫描路径 */
		"com.q3d.demo.svc.admin.**.**.**", /** 本服务包扫描路径 */
})
// @MapperScan(basePackages = { /** MyBatis Mapper 扫描路径 */
// "com.q3d.demo.svc.admin.mybatis.mapper",
// "com.q3d.demo.svc.admin.mybatis.mapperext",
// "com.q3d.demo.common.lib.fsguid.mapper",
// "com.baidu.fsg.uid",
// })
@SpringBootApplication(exclude = DataSourceAutoConfiguration.class) /** 微服务入口，禁用自动 DataSource 是为了从 Nacos 获取新配置 */
/**
 * @author 叶湘 ( weixin:yexiang841, email:yexiang841@qq.com )
 */
public class AdminSvcApplication {

	public static void main(String[] args) {
		SpringApplication.run(AdminSvcApplication.class, args);
	}

}
