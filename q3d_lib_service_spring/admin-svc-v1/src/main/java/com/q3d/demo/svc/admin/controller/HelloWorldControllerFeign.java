package com.q3d.demo.svc.admin.controller;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.q3d.demo.api.hello.client.feign.HelloWorldClientFeign;
import com.q3d.demo.api.hello.controller.HelloWorldControllerBase;

import io.swagger.v3.oas.annotations.tags.Tag;

@RequestMapping("/api/helloworld/feign") /** 匹配路径 */
@Tag(name = "HelloWorld 远程调用测试接口：Feign 调用（形似 RPC 的 HTTP）") /** SpringDoc 类注解 */
@RestController
/**
 * @author 叶湘 ( weixin:yexiang841, email:yexiang841@qq.com )
 */
public class HelloWorldControllerFeign extends HelloWorldControllerBase {

    @Autowired
    protected HelloWorldClientFeign helloWorldV1ClientFeign;

    @PostConstruct /** 在构造方法之后自动执行 */
    public void initialize() {
        this.helloWorldServiceInterface = helloWorldV1ClientFeign;
    }

}
