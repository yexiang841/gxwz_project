package com.q3d.demo.svc.admin.controller;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.q3d.demo.api.hello.client.rest.HelloWorldClientRest;
import com.q3d.demo.api.hello.controller.HelloWorldControllerBase;

import io.swagger.v3.oas.annotations.tags.Tag;

@RequestMapping("/api/helloworld/rest") /** 匹配路径 */
@Tag(name = "HelloWorld 远程调用测试接口：REST 调用（HTTP），简化 RESTful 规范，不使用响应码，也不遵循名词规范") /** SpringDoc 类注解 */
@RestController
/**
 * @author 叶湘 ( weixin:yexiang841, email:yexiang841@qq.com )
 */
public class HelloWorldControllerRest extends HelloWorldControllerBase {

    @Autowired
    protected HelloWorldClientRest helloWorldV1ClientRest;

    @PostConstruct /** 在构造方法之后自动执行 */
    public void initialize() {
        this.helloWorldServiceInterface = helloWorldV1ClientRest;
    }

}
