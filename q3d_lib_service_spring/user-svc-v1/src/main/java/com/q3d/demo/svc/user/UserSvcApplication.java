package com.q3d.demo.svc.user;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

import com.q3d.demo.api.user.client.feign.UserBaseFeignInterface;

@EnableDiscoveryClient /** 开启服务发现，使 Eureka 或者 Nacos 能按照服务名寻址 */
@EnableConfigurationProperties /** 开启 @ConfigurationProperties，支持从配置文件或远程配置读取属性注入对象 */
@EnableAspectJAutoProxy /** 让切面生效，目前作用于：日志切面 */
@EnableFeignClients( /** 开启 OpenFeign 客户端 */
		basePackages = { "com.q3d.demo.api.user.**.**" }, /** 方式一：User-API 包扫描路径 */
		clients = { UserBaseFeignInterface.class } /** 方式二：直接指定 Class */
)
@ComponentScan(basePackages = { /** Bean 扫描路径，对依赖顺序也有要求 */
		"com.q3d.demo.common.lib.service.**.**", /** 引入 REST 服务所需数据结构，如封装响应结构体，统一异常处理等 */
		"com.q3d.demo.common.lib.logger.**.**", /** 引入日志增强功能，自动加服务名、端口号、请求线程标识、请求响应时间等 */
		"com.q3d.demo.common.lib.fsguid.**.**", /** 引入百度分布式 ID 所需数据结构 */
		"com.q3d.demo.common.lib.druid.**.**", /** 引入动态配置数据库线程池所需数据结构 */
		"com.q3d.demo.api.user.**.**", /** 引入 User-API 中的数据结构 */
		"com.q3d.demo.svc.user.**.**", /** 本服务扫描路径 */
		"com.q3d.demo.svc.user.**.**.**", /** 本服务包扫描路径 */
})
@MapperScan(basePackages = { /** MyBatis Mapper 扫描路径 */
		"com.q3d.demo.svc.user.mybatis.mapper",
		"com.q3d.demo.svc.user.mybatis.mapperext",
		"com.q3d.demo.common.lib.fsguid.mapper",
		"com.baidu.fsg.uid",
})
@SpringBootApplication(exclude = DataSourceAutoConfiguration.class) /** 微服务入口，禁用自动 DataSource 是为了从 Nacos 获取新配置 */
/**
 * @author 叶湘 ( weixin:yexiang841, email:yexiang841@qq.com )
 */
public class UserSvcApplication {

	public static void main(String[] args) {
		SpringApplication.run(UserSvcApplication.class, args);
	}

}
