package com.q3d.demo.svc.user.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.q3d.demo.common.lib.service.response.ResponseBase;
import com.q3d.demo.svc.user.config.remote.userbase.ConfigUserBaseDto;
import com.q3d.demo.svc.user.config.remote.userbase.ConfigUserBaseManager;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;

@RequestMapping("/config/userbase") /** 匹配路径 */
@Tag(name = "动态配置（UserBase 组）查询接口，配置存储在配置中心的 {application-name}-helloworld.yaml 中") /** SpringDoc 类注解 */
@RestController
/**
 * @author 叶湘 ( weixin:yexiang841, email:yexiang841@qq.com )
 */
public class ConfigUserBaseController {

    @Autowired
    private ConfigUserBaseManager helloConfigManager;

    @GetMapping(value = "/all")
    @Operation(summary = "获取配置中心所有字段的值")
    public ResponseBase<ConfigUserBaseDto> all() {
        // 数据操作
        ConfigUserBaseDto dtoConfigClient = helloConfigManager.getAll();
        // 结构化返回
        return ResponseBase.success(dtoConfigClient);
    }

    @GetMapping(value = "/source")
    @Operation(summary = "获取配置中心 source 字段的值")
    public ResponseBase<String> source() {
        // 数据操作
        String source = helloConfigManager.getSource();
        // 结构化返回
        return ResponseBase.success(source);
    }

    @GetMapping(value = "/text")
    @Operation(summary = "获取配置中心 text 字段的值")
    public ResponseBase<String> text() {
        // 数据操作
        String text = helloConfigManager.getText();
        // 结构化返回
        return ResponseBase.success(text);
    }

    @GetMapping(value = "/num")
    @Operation(summary = "获取配置中心 num 字段的值")
    public ResponseBase<Integer> num() {
        // 数据操作
        Integer num = helloConfigManager.getNum();
        // 结构化返回
        return ResponseBase.success(num);
    }

}
