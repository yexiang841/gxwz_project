package com.q3d.demo.svc.user.controller;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.q3d.demo.api.user.controller.UserBaseControllerBase;
import com.q3d.demo.svc.user.mybatis.service.UserBaseServiceBatis;

import io.swagger.v3.oas.annotations.tags.Tag;

@RequestMapping("/api/userbase") /** 匹配路径 */
@Tag(name = "UserBase 接口：MyBatis 读写，HTTP 调用，简化 RESTful 规范，不使用响应码，也不遵循名词规范") /** SpringDoc 类注解 */
@RestController
/**
 * @author 叶湘 ( weixin:yexiang841, email:yexiang841@qq.com )
 */
public class UserBaseControllerBatis extends UserBaseControllerBase {

  @Autowired
  private UserBaseServiceBatis helloWorldServiceBatis;

  @PostConstruct /** 在构造方法之后自动执行 */
  public void initialize() {
    this.helloWorldServiceInterface = helloWorldServiceBatis;
  }

}
