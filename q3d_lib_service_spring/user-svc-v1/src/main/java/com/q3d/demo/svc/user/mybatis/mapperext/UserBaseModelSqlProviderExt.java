package com.q3d.demo.svc.user.mybatis.mapperext;

import org.apache.ibatis.jdbc.SQL;

import com.q3d.demo.svc.user.mybatis.mapper.UserBaseModelSqlProvider;
import com.q3d.demo.svc.user.mybatis.model.UserBaseModel;
import com.q3d.demo.svc.user.mybatis.model.UserBaseModelExample;

/**
 * UserBaseModelSqlProvider 是由 MyBatis-Generator 生成的，对于需要订制的部分，在这里继承和重写
 * 
 * @author 叶湘 ( weixin:yexiang841, email:yexiang841@qq.com )
 */
public class UserBaseModelSqlProviderExt extends UserBaseModelSqlProvider {

    /**
     * 为了修改 uuid 和 create_time 为数据库自动生成而重写.
     *
     * @param record
     * @return int
     */
    @Override
    public String insertSelective(UserBaseModel row) {
        SQL sql = new SQL();
        sql.INSERT_INTO("t_user_base");
        
        if (row.getId() != null) {
            sql.VALUES("id", "#{id,jdbcType=BIGINT}");
        }

        sql.VALUES("uuid", "replace(uuid(),'-','')");

        sql.VALUES("active", "1");
        
        if (row.getUserid() != null) {
            sql.VALUES("userid", "#{userid,jdbcType=VARCHAR}");
        }
        
        if (row.getNickname() != null) {
            sql.VALUES("nickname", "#{nickname,jdbcType=VARCHAR}");
        }
        
        if (row.getPassword() != null) {
            sql.VALUES("`password`", "#{password,jdbcType=VARCHAR}");
        }
        
        if (row.getPhonenumber() != null) {
            sql.VALUES("phonenumber", "#{phonenumber,jdbcType=VARCHAR}");
        }
        
        if (row.getEmail() != null) {
            sql.VALUES("email", "#{email,jdbcType=VARCHAR}");
        }
        
        if (row.getAvatar() != null) {
            sql.VALUES("avatar", "#{avatar,jdbcType=VARCHAR}");
        }
        
        sql.VALUES("create_time", "current_timestamp()");
        
        return sql.toString();
    }

    /**
     * 只查询符合条件的 UUID 的 SQL 语句
     *
     * @param example
     * @return int
     */
    public String selectUuidByExample(UserBaseModelExample example) {
        SQL sql = new SQL();
        if (example != null && example.isDistinct()) {
            sql.SELECT_DISTINCT("uuid");
        } else {
            sql.SELECT("uuid");
        }
        sql.FROM("t_user_base");
        applyWhere(sql, example, false);

        if (example != null && example.getOrderByClause() != null) {
            sql.ORDER_BY(example.getOrderByClause());
        }

        return sql.toString();
    }

    /**
     * 只查询符合条件的 ID 的 SQL 语句
     *
     * @param example
     * @return int
     */
    public String selectIdExtByExample(UserBaseModelExample example) {
        SQL sql = new SQL();
        if (example != null && example.isDistinct()) {
            sql.SELECT_DISTINCT("id");
        } else {
            sql.SELECT("id");
        }
        sql.FROM("t_user_base");
        applyWhere(sql, example, false);

        if (example != null && example.getOrderByClause() != null) {
            sql.ORDER_BY(example.getOrderByClause());
        }

        return sql.toString();
    }
}