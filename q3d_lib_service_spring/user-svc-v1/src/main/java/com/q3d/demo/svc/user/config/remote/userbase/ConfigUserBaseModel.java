package com.q3d.demo.svc.user.config.remote.userbase;

import org.springframework.stereotype.Component;

import org.springframework.boot.context.properties.ConfigurationProperties;

import lombok.*;

@Data /**
       * Lombok @Data =
       * 
       * @Getter +
       * @Setter +
       * @ToString +
       * @EqualsAndHashCode +
       * @RequiredArgsConstruct
       */
@Builder /** Lombok 链式 Builder 构造方法 */
@AllArgsConstructor /** Lombok 全参构造方法 */
@NoArgsConstructor /** Lombok 无参构造方法 */
@ConfigurationProperties(prefix = ConfigUserBaseModel.PREFIX)
@Component /** 注入组件 */
/**
 * 使用 @ConfigurationProperties 更新配置字段
 * 
 * @author 叶湘 ( weixin:yexiang841, email:yexiang841@qq.com )
 */
public class ConfigUserBaseModel {

       public static final String PREFIX = "config.userbase";

       @Builder.Default /** application.yml 和 bootstrap.yml 中不定义此字段，因此需要加默认值 */
       protected String source = "default";

       @Builder.Default /** application.yml 和 bootstrap.yml 中不定义此字段，因此需要加默认值 */
       protected String text = "default";

       @Builder.Default /** application.yml 和 bootstrap.yml 中不定义此字段，因此需要加默认值 */
       protected Integer num = 0;

}
