package com.q3d.demo.svc.user.mybatis.mapperext;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.InsertProvider;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.SelectKey;
import org.apache.ibatis.annotations.SelectProvider;
import org.apache.ibatis.type.JdbcType;

import com.q3d.demo.common.lib.service.dto.UuidDto;
import com.q3d.demo.svc.user.mybatis.mapper.UserBaseModelMapper;
import com.q3d.demo.svc.user.mybatis.mapper.UserBaseModelSqlProvider;
import com.q3d.demo.svc.user.mybatis.model.UserBaseModel;
import com.q3d.demo.svc.user.mybatis.model.UserBaseModelExample;

/**
 * UserBaseModelMapper 是由 MyBatis-Generator 生成的，对于需要订制的部分，在这里继承和重写
 * 
 * @author 叶湘 ( weixin:yexiang841, email:yexiang841@qq.com )
 */
public interface UserBaseModelMapperExt extends UserBaseModelMapper {
    
        /**
         * 重写部分：
         * 1、修改 uuid 和 create_time 为数据库自动生成；
         * 2、修改返回值 uuid；
         *
         * @param record
         * @return int
         */
        @Override
    @Insert({
        "insert into t_user_base (id, uuid, active, ",
        "userid, nickname, ",
        "`password`, phonenumber, ",
        "email, avatar, create_time)",
        "values (#{id,jdbcType=BIGINT},replace(uuid(),'-',''), 1, ",
        "#{userid,jdbcType=VARCHAR}, #{nickname,jdbcType=VARCHAR}, ",
        "#{password,jdbcType=VARCHAR}, #{phonenumber,jdbcType=VARCHAR}, ",
        "#{email,jdbcType=VARCHAR}, #{avatar,jdbcType=VARCHAR}, current_timestamp())"
    })
    @SelectKey(statement = "SELECT uuid FROM t_user_base WHERE id = #{id,jdbcType=BIGINT}", keyProperty = "uuid", before = false, resultType = String.class)
    int insert(UserBaseModel row);

        /**
         * 重写部分：
         * 1、修改 uuid 和 create_time 为数据库自动生成；
         * 2、修改返回值 uuid；
         *
         * @param record
         * @return int
         */
        @Override
    @InsertProvider(type=UserBaseModelSqlProvider.class, method="insertSelective")
    @SelectKey(statement = "SELECT uuid FROM t_user_base WHERE id = #{id,jdbcType=BIGINT}", keyProperty = "uuid", before = false, resultType = String.class)
    int insertSelective(UserBaseModel row);

        /**
         * 只查询符合条件的 UUID
         *
         * @param example
         * @return List<UserBaseK1Dto>
         */
        @SelectProvider(type = UserBaseModelSqlProviderExt.class, method = "selectUuidByExample")
        @Results({
                        @Result(column = "uuid", property = "uuid", jdbcType = JdbcType.CHAR),
        })
        List<UuidDto> selectUuidByExample(UserBaseModelExample example);

        /**
         * 只查询符合条件的 ID
         *
         * @param example
         * @return List<UserBaseK2Dto>
         */
        @SelectProvider(type = UserBaseModelSqlProviderExt.class, method = "selectIdExtByExample")
        @Results({
                        @Result(column = "id", property = "id", jdbcType = JdbcType.BIGINT, id = true),
        })
        List<Map<String, Long>> selectIdExtByExample(UserBaseModelExample example);

}
