// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: Common.proto

package com.q3d.demo.api.user.grpc;

public interface CountResultGrpcOrBuilder extends
    // @@protoc_insertion_point(interface_extends:CountResultGrpc)
    com.google.protobuf.MessageOrBuilder {

  /**
   * <code>int64 countResult = 1;</code>
   */
  long getCountResult();
}
