package com.q3d.demo.api.user.dto;

import java.util.List;

import javax.validation.Valid;

import io.swagger.v3.oas.annotations.media.Schema;

import lombok.*;

@Data /**
       * Lombok @Data =
       * 
       * @Getter +
       * @Setter +
       * @ToString +
       * @EqualsAndHashCode +
       * @RequiredArgsConstruct
       */
@AllArgsConstructor /** Lombok 全参构造方法 */
@NoArgsConstructor /** Lombok 无参构造方法 */
@Builder /** Lombok 链式 Builder 构造方法 */
@Schema(description = "新增 UserBase 列表请求结构体") /** SpringDoc 注解 */
/**
 * @author 叶湘 ( weixin:yexiang841, email:yexiang841@qq.com )
 */
public class UserBaseP1ListDto implements java.io.Serializable { // 若 RPC 需要传输 DTO 则要求序列化

    @Valid /** 要求对列表中的对象进行字段校验 */
    @Schema(description = "UserBase 业务数据列表")
    protected List<UserBaseP1Dto> dtoList;

}
