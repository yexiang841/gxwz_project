// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: Common.proto

package com.q3d.demo.api.user.grpc;

public interface UuidGrpcOrBuilder extends
    // @@protoc_insertion_point(interface_extends:UuidGrpc)
    com.google.protobuf.MessageOrBuilder {

  /**
   * <code>string uuid = 1;</code>
   */
  java.lang.String getUuid();
  /**
   * <code>string uuid = 1;</code>
   */
  com.google.protobuf.ByteString
      getUuidBytes();
}
