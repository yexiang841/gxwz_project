package com.q3d.demo.api.user.client.feign;

import org.springframework.cloud.openfeign.FeignClient;

import com.q3d.demo.api.user.controller.UserBaseControllerInterface;

@FeignClient( /** value 和 name 互为别名，作用是一样的 */
        // url = "http://localhost:8020" /** 这是直接寻址而不经过注册中心的写法，url 将覆盖 name/value */
        // value = "user-svc-v1", /** 远程服务名，这是通过注册中心寻址而不经过网关的写法 */
        // path = "/api/userbase", /** 具体接口的路径，这是直接寻址或者通过注册中心寻址的写法 */
        value = "gateway-svc-v1", /** 远程服务名，这是通过网关中转的写法 */
        path = "/user-svc-v1/api/userbase", /** 具体接口的路径，这是通过网关中转的写法（网关服务用路径第一段寻址） */
        contextId = "context-user-svc-v1", /** 当一个value/name需要拆开不同的 FeignClient 时，需配置唯一名称以区分注入的 Bean */
        configuration = UserBaseFeignConfig.class /** 用于指定自定义解码器 ResponseFeignDecoder */
)
/**
 * 为 user-svc-v1 定义 UserBase 接口的 FeignClient
 * 
 * @author 叶湘 ( weixin:yexiang841, email:yexiang841@qq.com )
 */
public interface UserBaseFeignInterface extends UserBaseControllerInterface {

}
