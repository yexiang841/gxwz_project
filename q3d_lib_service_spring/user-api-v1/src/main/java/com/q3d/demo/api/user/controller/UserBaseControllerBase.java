package com.q3d.demo.api.user.controller;

import javax.validation.Valid;

import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.CrossOrigin;

import com.q3d.demo.common.lib.service.dto.CountResultDto;
import com.q3d.demo.common.lib.service.dto.IdExtDto;
import com.q3d.demo.common.lib.service.dto.IdExtListDto;
import com.q3d.demo.common.lib.service.dto.OpResultDto;
import com.q3d.demo.common.lib.service.dto.PageRequestDto;
import com.q3d.demo.common.lib.service.dto.StringDto;
import com.q3d.demo.common.lib.service.dto.UuidDto;
import com.q3d.demo.common.lib.service.dto.UuidListDto;
import com.q3d.demo.common.lib.service.response.ResponseBase;
import com.q3d.demo.api.user.dto.UserBaseP1Dto;
import com.q3d.demo.api.user.dto.UserBaseP1K1Dto;
import com.q3d.demo.api.user.dto.UserBaseP1K2Dto;
import com.q3d.demo.api.user.dto.UserBaseP1ListDto;
import com.q3d.demo.api.user.dto.UserBaseR1K2Dto;
import com.q3d.demo.api.user.dto.UserBaseR1K2ListDto;
import com.q3d.demo.api.user.dto.UserBaseR1K1Dto;
import com.q3d.demo.api.user.dto.UserBaseR1K1ListDto;
import com.q3d.demo.api.user.service.UserBaseServiceInterface;

@Validated /** 在 Controller 层面开启校验 */
@CrossOrigin /** 支持跨域请求 */
/**
 * UserBase 接口的真正实现
 * 
 * @author 叶湘 ( weixin:yexiang841, email:yexiang841@qq.com )
 */
public class UserBaseControllerBase implements UserBaseControllerInterface {

    protected UserBaseServiceInterface helloWorldServiceInterface;

    @Override
    public ResponseBase<StringDto> hello(@PathVariable("text") String text) {
        // 数据操作
        StringDto dtoStringResult = helloWorldServiceInterface.hello(StringDto.builder().text(text).build());
        // 结构化返回
        return ResponseBase.success(dtoStringResult);

    }

    @Override
    public ResponseBase<UuidDto> addOne(@RequestBody @Valid UserBaseP1Dto dtoUserBaseP1) {
        // 数据操作
        UuidDto dtoUuid = helloWorldServiceInterface.addOne(dtoUserBaseP1);
        // 结构化返回
        return ResponseBase.success(dtoUuid);
    }

    @Override
    public ResponseBase<UuidListDto> addBatch(@RequestBody @Valid UserBaseP1ListDto dtoP1List) {
        // 数据操作
        UuidListDto dtoUuidList = helloWorldServiceInterface.addBatch(dtoP1List);
        // 结构化返回
        return ResponseBase.success(dtoUuidList);
    }

    @Override
    public ResponseBase<OpResultDto> removeOne(@RequestParam(value = "uuid", required = true) String uuid) {
        // 构造 DTO，由 Service 根据 DTO 的字段注解完成参数检查
        UuidDto dtoUuid = UuidDto.builder().uuid(uuid).build();
        // 数据操作
        OpResultDto dtoOpResult = helloWorldServiceInterface.removeOne(dtoUuid);
        // 结构化返回
        return ResponseBase.success(dtoOpResult);
    }

    @Override
    public ResponseBase<OpResultDto> removeBatch(@RequestBody @Valid UuidListDto dtoUuidList) {
        // 数据操作
        OpResultDto dtoOpResult = helloWorldServiceInterface.removeBatch(dtoUuidList);
        // 结构化返回
        return ResponseBase.success(dtoOpResult);
    }

    @Override
    public ResponseBase<OpResultDto> deleteOne(@RequestParam(value = "id", required = true) String idExt) {
        // 构造 DTO，由 Service 根据 DTO 的字段注解完成参数检查
        IdExtDto dtoIdExt = IdExtDto.builder()
                .idExt(idExt)
                .build();
        // 数据操作
        OpResultDto dtoOpResult = helloWorldServiceInterface.deleteOne(dtoIdExt);
        // 结构化返回
        return ResponseBase.success(dtoOpResult);
    }

    @Override
    public ResponseBase<OpResultDto> deleteBatch(@RequestBody @Valid IdExtListDto dtoIdExtList) {
        // 数据操作
        OpResultDto dtoOpResult = helloWorldServiceInterface.deleteBatch(dtoIdExtList);
        // 结构化返回
        return ResponseBase.success(dtoOpResult);
    }

    @Override
    public ResponseBase<OpResultDto> reviveOne(@RequestParam(value = "id", required = true) String idExt) {
        // 构造 DTO，由 Service 根据 DTO 的字段注解完成参数检查
        IdExtDto dtoIdExt = IdExtDto.builder()
                .idExt(idExt)
                .build();
        // 数据操作
        OpResultDto dtoOpResult = helloWorldServiceInterface.reviveOne(dtoIdExt);
        // 结构化返回
        return ResponseBase.success(dtoOpResult);
    }

    @Override
    public ResponseBase<OpResultDto> reviveBatch(@RequestBody @Valid IdExtListDto dtoIdExtList) {
        // 数据操作
        OpResultDto dtoOpResult = helloWorldServiceInterface.reviveBatch(dtoIdExtList);
        // 结构化返回
        return ResponseBase.success(dtoOpResult);
    }

    @Override
    public ResponseBase<OpResultDto> editSelective(@RequestBody @Valid UserBaseP1K1Dto dtoP1K1) {
        // 数据操作
        OpResultDto dtoOpResult = helloWorldServiceInterface.editSelective(dtoP1K1);
        // 结构化返回
        return ResponseBase.success(dtoOpResult);
    }

    @Override
    public ResponseBase<OpResultDto> updateSelective(@RequestBody @Valid UserBaseP1K2Dto dtoP1K2) {
        // 数据操作
        OpResultDto dtoOpResult = helloWorldServiceInterface.updateSelective(dtoP1K2);
        // 结构化返回
        return ResponseBase.success(dtoOpResult);
    }

    @Override
    public ResponseBase<CountResultDto> getCount() {
        // 数据操作
        CountResultDto dtoCountResult = helloWorldServiceInterface.getCount();
        // 结构化返回
        return ResponseBase.success(dtoCountResult);
    }

    @Override
    public ResponseBase<UserBaseR1K1Dto> getOne(@PathVariable("uuid") String uuid) {
        // 构造 DTO，由 Service 根据 DTO 的字段注解完成参数检查
        UuidDto dtoUuid = UuidDto.builder().uuid(uuid).build();
        // 数据操作
        UserBaseR1K1Dto dtoR1K1 = helloWorldServiceInterface.getOne(dtoUuid);
        // 结构化返回
        return ResponseBase.success(dtoR1K1);
    }

    @Override
    public ResponseBase<UserBaseR1K1ListDto> getBatch(@RequestBody @Valid UuidListDto dtoUuidList) {
        // 数据操作
        UserBaseR1K1ListDto dtoR1List = helloWorldServiceInterface.getBatch(dtoUuidList);
        // 结构化返回
        return ResponseBase.success(dtoR1List);
    }

    @Override
    public ResponseBase<UserBaseR1K1ListDto> getByPage(@ModelAttribute @Valid PageRequestDto dtoPageRequest) {
        // 数据操作
        UserBaseR1K1ListDto dtoR1K1List = helloWorldServiceInterface.getByPage(dtoPageRequest);
        // 结构化返回
        return ResponseBase.success(dtoR1K1List);
    }

    @Override
    public ResponseBase<UuidListDto> getUuidByPage(@ModelAttribute @Valid PageRequestDto dtoPageRequest) {
        // 数据操作
        UuidListDto dtoUuidList = helloWorldServiceInterface.getUuidByPage(dtoPageRequest);
        // 结构化返回
        return ResponseBase.success(dtoUuidList);
    }

    @Override
    public ResponseBase<CountResultDto> selectCount() {
        // 数据操作
        CountResultDto dtoCountResult = helloWorldServiceInterface.selectCount();
        // 结构化返回
        return ResponseBase.success(dtoCountResult);
    }

    @Override
    public ResponseBase<CountResultDto> selectCountRemoved() {
        // 数据操作
        CountResultDto dtoCountResult = helloWorldServiceInterface.selectCountRemoved();
        // 结构化返回
        return ResponseBase.success(dtoCountResult);
    }

    @Override
    public ResponseBase<UserBaseR1K2Dto> selectOne(@PathVariable("id") String idExt) {
        // 构造 DTO，由 Service 根据 DTO 的字段注解完成参数检查
        IdExtDto dtoIdExt = IdExtDto.builder()
                .idExt(idExt)
                .build();
        // 数据操作
        UserBaseR1K2Dto dtoR1K2 = helloWorldServiceInterface.selectOne(dtoIdExt);
        // 结构化返回
        return ResponseBase.success(dtoR1K2);
    }

    @Override
    public ResponseBase<UserBaseR1K2ListDto> selectBatch(@RequestBody @Valid IdExtListDto dtoIdExtList) {
        // 数据操作
        UserBaseR1K2ListDto dtoR1K2List = helloWorldServiceInterface.selectBatch(dtoIdExtList);
        // 结构化返回
        return ResponseBase.success(dtoR1K2List);
    }

    @Override
    public ResponseBase<UserBaseR1K2ListDto> selectByPage(@ModelAttribute @Valid PageRequestDto dtoPageRequest) {
        // 数据操作
        UserBaseR1K2ListDto dtoR1K2List = helloWorldServiceInterface.selectByPage(dtoPageRequest);
        // 结构化返回
        return ResponseBase.success(dtoR1K2List);
    }

    @Override
    public ResponseBase<IdExtListDto> selectIdExtByPage(@ModelAttribute @Valid PageRequestDto dtoPageRequest) {
        // 数据操作
        IdExtListDto dtoIdExtList = helloWorldServiceInterface.selectIdExtByPage(dtoPageRequest);
        // 结构化返回
        return ResponseBase.success(dtoIdExtList);
    }

    @Override
    public ResponseBase<IdExtListDto> selectIdExtRemoved() {
        // 数据操作
        IdExtListDto dtoIdExtList = helloWorldServiceInterface.selectIdExtRemoved();
        // 结构化返回
        return ResponseBase.success(dtoIdExtList);
    }

}
