// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: Common.proto

package com.q3d.demo.api.user.grpc;

/**
 * <pre>
 * 定义操作响应结构体 CountResultGrpc
 * </pre>
 *
 * Protobuf type {@code CountResultGrpc}
 */
public  final class CountResultGrpc extends
    com.google.protobuf.GeneratedMessageV3 implements
    // @@protoc_insertion_point(message_implements:CountResultGrpc)
    CountResultGrpcOrBuilder {
private static final long serialVersionUID = 0L;
  // Use CountResultGrpc.newBuilder() to construct.
  private CountResultGrpc(com.google.protobuf.GeneratedMessageV3.Builder<?> builder) {
    super(builder);
  }
  private CountResultGrpc() {
    countResult_ = 0L;
  }

  @java.lang.Override
  public final com.google.protobuf.UnknownFieldSet
  getUnknownFields() {
    return this.unknownFields;
  }
  private CountResultGrpc(
      com.google.protobuf.CodedInputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    this();
    if (extensionRegistry == null) {
      throw new java.lang.NullPointerException();
    }
    int mutable_bitField0_ = 0;
    com.google.protobuf.UnknownFieldSet.Builder unknownFields =
        com.google.protobuf.UnknownFieldSet.newBuilder();
    try {
      boolean done = false;
      while (!done) {
        int tag = input.readTag();
        switch (tag) {
          case 0:
            done = true;
            break;
          default: {
            if (!parseUnknownFieldProto3(
                input, unknownFields, extensionRegistry, tag)) {
              done = true;
            }
            break;
          }
          case 8: {

            countResult_ = input.readInt64();
            break;
          }
        }
      }
    } catch (com.google.protobuf.InvalidProtocolBufferException e) {
      throw e.setUnfinishedMessage(this);
    } catch (java.io.IOException e) {
      throw new com.google.protobuf.InvalidProtocolBufferException(
          e).setUnfinishedMessage(this);
    } finally {
      this.unknownFields = unknownFields.build();
      makeExtensionsImmutable();
    }
  }
  public static final com.google.protobuf.Descriptors.Descriptor
      getDescriptor() {
    return com.q3d.demo.api.user.grpc.CommonProto.internal_static_CountResultGrpc_descriptor;
  }

  protected com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internalGetFieldAccessorTable() {
    return com.q3d.demo.api.user.grpc.CommonProto.internal_static_CountResultGrpc_fieldAccessorTable
        .ensureFieldAccessorsInitialized(
            com.q3d.demo.api.user.grpc.CountResultGrpc.class, com.q3d.demo.api.user.grpc.CountResultGrpc.Builder.class);
  }

  public static final int COUNTRESULT_FIELD_NUMBER = 1;
  private long countResult_;
  /**
   * <code>int64 countResult = 1;</code>
   */
  public long getCountResult() {
    return countResult_;
  }

  private byte memoizedIsInitialized = -1;
  public final boolean isInitialized() {
    byte isInitialized = memoizedIsInitialized;
    if (isInitialized == 1) return true;
    if (isInitialized == 0) return false;

    memoizedIsInitialized = 1;
    return true;
  }

  public void writeTo(com.google.protobuf.CodedOutputStream output)
                      throws java.io.IOException {
    if (countResult_ != 0L) {
      output.writeInt64(1, countResult_);
    }
    unknownFields.writeTo(output);
  }

  public int getSerializedSize() {
    int size = memoizedSize;
    if (size != -1) return size;

    size = 0;
    if (countResult_ != 0L) {
      size += com.google.protobuf.CodedOutputStream
        .computeInt64Size(1, countResult_);
    }
    size += unknownFields.getSerializedSize();
    memoizedSize = size;
    return size;
  }

  @java.lang.Override
  public boolean equals(final java.lang.Object obj) {
    if (obj == this) {
     return true;
    }
    if (!(obj instanceof com.q3d.demo.api.user.grpc.CountResultGrpc)) {
      return super.equals(obj);
    }
    com.q3d.demo.api.user.grpc.CountResultGrpc other = (com.q3d.demo.api.user.grpc.CountResultGrpc) obj;

    boolean result = true;
    result = result && (getCountResult()
        == other.getCountResult());
    result = result && unknownFields.equals(other.unknownFields);
    return result;
  }

  @java.lang.Override
  public int hashCode() {
    if (memoizedHashCode != 0) {
      return memoizedHashCode;
    }
    int hash = 41;
    hash = (19 * hash) + getDescriptor().hashCode();
    hash = (37 * hash) + COUNTRESULT_FIELD_NUMBER;
    hash = (53 * hash) + com.google.protobuf.Internal.hashLong(
        getCountResult());
    hash = (29 * hash) + unknownFields.hashCode();
    memoizedHashCode = hash;
    return hash;
  }

  public static com.q3d.demo.api.user.grpc.CountResultGrpc parseFrom(
      java.nio.ByteBuffer data)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data);
  }
  public static com.q3d.demo.api.user.grpc.CountResultGrpc parseFrom(
      java.nio.ByteBuffer data,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data, extensionRegistry);
  }
  public static com.q3d.demo.api.user.grpc.CountResultGrpc parseFrom(
      com.google.protobuf.ByteString data)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data);
  }
  public static com.q3d.demo.api.user.grpc.CountResultGrpc parseFrom(
      com.google.protobuf.ByteString data,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data, extensionRegistry);
  }
  public static com.q3d.demo.api.user.grpc.CountResultGrpc parseFrom(byte[] data)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data);
  }
  public static com.q3d.demo.api.user.grpc.CountResultGrpc parseFrom(
      byte[] data,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data, extensionRegistry);
  }
  public static com.q3d.demo.api.user.grpc.CountResultGrpc parseFrom(java.io.InputStream input)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input);
  }
  public static com.q3d.demo.api.user.grpc.CountResultGrpc parseFrom(
      java.io.InputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input, extensionRegistry);
  }
  public static com.q3d.demo.api.user.grpc.CountResultGrpc parseDelimitedFrom(java.io.InputStream input)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseDelimitedWithIOException(PARSER, input);
  }
  public static com.q3d.demo.api.user.grpc.CountResultGrpc parseDelimitedFrom(
      java.io.InputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseDelimitedWithIOException(PARSER, input, extensionRegistry);
  }
  public static com.q3d.demo.api.user.grpc.CountResultGrpc parseFrom(
      com.google.protobuf.CodedInputStream input)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input);
  }
  public static com.q3d.demo.api.user.grpc.CountResultGrpc parseFrom(
      com.google.protobuf.CodedInputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input, extensionRegistry);
  }

  public Builder newBuilderForType() { return newBuilder(); }
  public static Builder newBuilder() {
    return DEFAULT_INSTANCE.toBuilder();
  }
  public static Builder newBuilder(com.q3d.demo.api.user.grpc.CountResultGrpc prototype) {
    return DEFAULT_INSTANCE.toBuilder().mergeFrom(prototype);
  }
  public Builder toBuilder() {
    return this == DEFAULT_INSTANCE
        ? new Builder() : new Builder().mergeFrom(this);
  }

  @java.lang.Override
  protected Builder newBuilderForType(
      com.google.protobuf.GeneratedMessageV3.BuilderParent parent) {
    Builder builder = new Builder(parent);
    return builder;
  }
  /**
   * <pre>
   * 定义操作响应结构体 CountResultGrpc
   * </pre>
   *
   * Protobuf type {@code CountResultGrpc}
   */
  public static final class Builder extends
      com.google.protobuf.GeneratedMessageV3.Builder<Builder> implements
      // @@protoc_insertion_point(builder_implements:CountResultGrpc)
      com.q3d.demo.api.user.grpc.CountResultGrpcOrBuilder {
    public static final com.google.protobuf.Descriptors.Descriptor
        getDescriptor() {
      return com.q3d.demo.api.user.grpc.CommonProto.internal_static_CountResultGrpc_descriptor;
    }

    protected com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
        internalGetFieldAccessorTable() {
      return com.q3d.demo.api.user.grpc.CommonProto.internal_static_CountResultGrpc_fieldAccessorTable
          .ensureFieldAccessorsInitialized(
              com.q3d.demo.api.user.grpc.CountResultGrpc.class, com.q3d.demo.api.user.grpc.CountResultGrpc.Builder.class);
    }

    // Construct using com.q3d.demo.api.user.grpc.CountResultGrpc.newBuilder()
    private Builder() {
      maybeForceBuilderInitialization();
    }

    private Builder(
        com.google.protobuf.GeneratedMessageV3.BuilderParent parent) {
      super(parent);
      maybeForceBuilderInitialization();
    }
    private void maybeForceBuilderInitialization() {
      if (com.google.protobuf.GeneratedMessageV3
              .alwaysUseFieldBuilders) {
      }
    }
    public Builder clear() {
      super.clear();
      countResult_ = 0L;

      return this;
    }

    public com.google.protobuf.Descriptors.Descriptor
        getDescriptorForType() {
      return com.q3d.demo.api.user.grpc.CommonProto.internal_static_CountResultGrpc_descriptor;
    }

    public com.q3d.demo.api.user.grpc.CountResultGrpc getDefaultInstanceForType() {
      return com.q3d.demo.api.user.grpc.CountResultGrpc.getDefaultInstance();
    }

    public com.q3d.demo.api.user.grpc.CountResultGrpc build() {
      com.q3d.demo.api.user.grpc.CountResultGrpc result = buildPartial();
      if (!result.isInitialized()) {
        throw newUninitializedMessageException(result);
      }
      return result;
    }

    public com.q3d.demo.api.user.grpc.CountResultGrpc buildPartial() {
      com.q3d.demo.api.user.grpc.CountResultGrpc result = new com.q3d.demo.api.user.grpc.CountResultGrpc(this);
      result.countResult_ = countResult_;
      onBuilt();
      return result;
    }

    public Builder clone() {
      return (Builder) super.clone();
    }
    public Builder setField(
        com.google.protobuf.Descriptors.FieldDescriptor field,
        java.lang.Object value) {
      return (Builder) super.setField(field, value);
    }
    public Builder clearField(
        com.google.protobuf.Descriptors.FieldDescriptor field) {
      return (Builder) super.clearField(field);
    }
    public Builder clearOneof(
        com.google.protobuf.Descriptors.OneofDescriptor oneof) {
      return (Builder) super.clearOneof(oneof);
    }
    public Builder setRepeatedField(
        com.google.protobuf.Descriptors.FieldDescriptor field,
        int index, java.lang.Object value) {
      return (Builder) super.setRepeatedField(field, index, value);
    }
    public Builder addRepeatedField(
        com.google.protobuf.Descriptors.FieldDescriptor field,
        java.lang.Object value) {
      return (Builder) super.addRepeatedField(field, value);
    }
    public Builder mergeFrom(com.google.protobuf.Message other) {
      if (other instanceof com.q3d.demo.api.user.grpc.CountResultGrpc) {
        return mergeFrom((com.q3d.demo.api.user.grpc.CountResultGrpc)other);
      } else {
        super.mergeFrom(other);
        return this;
      }
    }

    public Builder mergeFrom(com.q3d.demo.api.user.grpc.CountResultGrpc other) {
      if (other == com.q3d.demo.api.user.grpc.CountResultGrpc.getDefaultInstance()) return this;
      if (other.getCountResult() != 0L) {
        setCountResult(other.getCountResult());
      }
      this.mergeUnknownFields(other.unknownFields);
      onChanged();
      return this;
    }

    public final boolean isInitialized() {
      return true;
    }

    public Builder mergeFrom(
        com.google.protobuf.CodedInputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws java.io.IOException {
      com.q3d.demo.api.user.grpc.CountResultGrpc parsedMessage = null;
      try {
        parsedMessage = PARSER.parsePartialFrom(input, extensionRegistry);
      } catch (com.google.protobuf.InvalidProtocolBufferException e) {
        parsedMessage = (com.q3d.demo.api.user.grpc.CountResultGrpc) e.getUnfinishedMessage();
        throw e.unwrapIOException();
      } finally {
        if (parsedMessage != null) {
          mergeFrom(parsedMessage);
        }
      }
      return this;
    }

    private long countResult_ ;
    /**
     * <code>int64 countResult = 1;</code>
     */
    public long getCountResult() {
      return countResult_;
    }
    /**
     * <code>int64 countResult = 1;</code>
     */
    public Builder setCountResult(long value) {
      
      countResult_ = value;
      onChanged();
      return this;
    }
    /**
     * <code>int64 countResult = 1;</code>
     */
    public Builder clearCountResult() {
      
      countResult_ = 0L;
      onChanged();
      return this;
    }
    public final Builder setUnknownFields(
        final com.google.protobuf.UnknownFieldSet unknownFields) {
      return super.setUnknownFieldsProto3(unknownFields);
    }

    public final Builder mergeUnknownFields(
        final com.google.protobuf.UnknownFieldSet unknownFields) {
      return super.mergeUnknownFields(unknownFields);
    }


    // @@protoc_insertion_point(builder_scope:CountResultGrpc)
  }

  // @@protoc_insertion_point(class_scope:CountResultGrpc)
  private static final com.q3d.demo.api.user.grpc.CountResultGrpc DEFAULT_INSTANCE;
  static {
    DEFAULT_INSTANCE = new com.q3d.demo.api.user.grpc.CountResultGrpc();
  }

  public static com.q3d.demo.api.user.grpc.CountResultGrpc getDefaultInstance() {
    return DEFAULT_INSTANCE;
  }

  private static final com.google.protobuf.Parser<CountResultGrpc>
      PARSER = new com.google.protobuf.AbstractParser<CountResultGrpc>() {
    public CountResultGrpc parsePartialFrom(
        com.google.protobuf.CodedInputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws com.google.protobuf.InvalidProtocolBufferException {
      return new CountResultGrpc(input, extensionRegistry);
    }
  };

  public static com.google.protobuf.Parser<CountResultGrpc> parser() {
    return PARSER;
  }

  @java.lang.Override
  public com.google.protobuf.Parser<CountResultGrpc> getParserForType() {
    return PARSER;
  }

  public com.q3d.demo.api.user.grpc.CountResultGrpc getDefaultInstanceForType() {
    return DEFAULT_INSTANCE;
  }

}

