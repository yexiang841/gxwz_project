package com.q3d.demo.api.user.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import io.swagger.v3.oas.annotations.media.Schema;

import lombok.*;
import lombok.experimental.SuperBuilder;

@Data /**
       * Lombok @Data =
       * 
       * @Getter +
       * @Setter +
       * @ToString +
       * @EqualsAndHashCode +
       * @RequiredArgsConstruct
       */
@AllArgsConstructor /** Lombok 全参构造方法 */
@NoArgsConstructor /** Lombok 无参构造方法 */
@SuperBuilder /** Lombok 链式 Builder 构造方法，当基类和子类都用 @Data 时，两者都要从 @Builder 改成 @SuperBuilder */
/**
 * @author 叶湘 ( weixin:yexiang841, email:yexiang841@qq.com )
 */
public class UserBaseDto implements java.io.Serializable { // 若 RPC 需要传输 DTO 则要求序列化

    @Schema(description = "用户 ID，支持英文、数字、下划线", example = "yexiang841")
    // @NotNull(message = "用户 ID 不能为空")
    @NotBlank(message = "用户 ID 不能为空字符串")
    protected String userid;

    @Schema(description = "用户昵称，支持中文、英文、数字", example = "叶湘")
    @NotNull(message = "用户昵称不能为空")
    @NotBlank(message = "用户昵称不能为空字符串")
    protected String nickname;

    @Schema(description = "加密过的密码", example = "abcde")
    @NotNull(message = "密码不能为空")
    @NotBlank(message = "密码不能为空字符串")
    protected String password;

    @Schema(description = "手机号，支持数字、横线", example = "18907791148")
    @Builder.Default /** 设置默认值，允许客户端不赋值 */
    protected String phonenumber = "18907791148";

    @Schema(description = "邮箱", example = "yexiang841@qq.com")
    @Builder.Default /** 设置默认值，允许客户端不赋值 */
    protected String email = "yexiang841@qq.com";

    @Schema(description = "头像", example = "http://abcde")
    @Builder.Default /** 设置默认值，允许客户端不赋值 */
    protected String avatar = "http://abcde";

}
