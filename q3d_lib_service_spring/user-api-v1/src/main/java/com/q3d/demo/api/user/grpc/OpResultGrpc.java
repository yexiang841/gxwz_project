// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: Common.proto

package com.q3d.demo.api.user.grpc;

/**
 * <pre>
 * 定义操作响应结构体 OpResultGrpc
 * </pre>
 *
 * Protobuf type {@code OpResultGrpc}
 */
public  final class OpResultGrpc extends
    com.google.protobuf.GeneratedMessageV3 implements
    // @@protoc_insertion_point(message_implements:OpResultGrpc)
    OpResultGrpcOrBuilder {
private static final long serialVersionUID = 0L;
  // Use OpResultGrpc.newBuilder() to construct.
  private OpResultGrpc(com.google.protobuf.GeneratedMessageV3.Builder<?> builder) {
    super(builder);
  }
  private OpResultGrpc() {
    opResult_ = 0;
  }

  @java.lang.Override
  public final com.google.protobuf.UnknownFieldSet
  getUnknownFields() {
    return this.unknownFields;
  }
  private OpResultGrpc(
      com.google.protobuf.CodedInputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    this();
    if (extensionRegistry == null) {
      throw new java.lang.NullPointerException();
    }
    int mutable_bitField0_ = 0;
    com.google.protobuf.UnknownFieldSet.Builder unknownFields =
        com.google.protobuf.UnknownFieldSet.newBuilder();
    try {
      boolean done = false;
      while (!done) {
        int tag = input.readTag();
        switch (tag) {
          case 0:
            done = true;
            break;
          default: {
            if (!parseUnknownFieldProto3(
                input, unknownFields, extensionRegistry, tag)) {
              done = true;
            }
            break;
          }
          case 8: {

            opResult_ = input.readInt32();
            break;
          }
        }
      }
    } catch (com.google.protobuf.InvalidProtocolBufferException e) {
      throw e.setUnfinishedMessage(this);
    } catch (java.io.IOException e) {
      throw new com.google.protobuf.InvalidProtocolBufferException(
          e).setUnfinishedMessage(this);
    } finally {
      this.unknownFields = unknownFields.build();
      makeExtensionsImmutable();
    }
  }
  public static final com.google.protobuf.Descriptors.Descriptor
      getDescriptor() {
    return com.q3d.demo.api.user.grpc.CommonProto.internal_static_OpResultGrpc_descriptor;
  }

  protected com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internalGetFieldAccessorTable() {
    return com.q3d.demo.api.user.grpc.CommonProto.internal_static_OpResultGrpc_fieldAccessorTable
        .ensureFieldAccessorsInitialized(
            com.q3d.demo.api.user.grpc.OpResultGrpc.class, com.q3d.demo.api.user.grpc.OpResultGrpc.Builder.class);
  }

  public static final int OPRESULT_FIELD_NUMBER = 1;
  private int opResult_;
  /**
   * <code>int32 opResult = 1;</code>
   */
  public int getOpResult() {
    return opResult_;
  }

  private byte memoizedIsInitialized = -1;
  public final boolean isInitialized() {
    byte isInitialized = memoizedIsInitialized;
    if (isInitialized == 1) return true;
    if (isInitialized == 0) return false;

    memoizedIsInitialized = 1;
    return true;
  }

  public void writeTo(com.google.protobuf.CodedOutputStream output)
                      throws java.io.IOException {
    if (opResult_ != 0) {
      output.writeInt32(1, opResult_);
    }
    unknownFields.writeTo(output);
  }

  public int getSerializedSize() {
    int size = memoizedSize;
    if (size != -1) return size;

    size = 0;
    if (opResult_ != 0) {
      size += com.google.protobuf.CodedOutputStream
        .computeInt32Size(1, opResult_);
    }
    size += unknownFields.getSerializedSize();
    memoizedSize = size;
    return size;
  }

  @java.lang.Override
  public boolean equals(final java.lang.Object obj) {
    if (obj == this) {
     return true;
    }
    if (!(obj instanceof com.q3d.demo.api.user.grpc.OpResultGrpc)) {
      return super.equals(obj);
    }
    com.q3d.demo.api.user.grpc.OpResultGrpc other = (com.q3d.demo.api.user.grpc.OpResultGrpc) obj;

    boolean result = true;
    result = result && (getOpResult()
        == other.getOpResult());
    result = result && unknownFields.equals(other.unknownFields);
    return result;
  }

  @java.lang.Override
  public int hashCode() {
    if (memoizedHashCode != 0) {
      return memoizedHashCode;
    }
    int hash = 41;
    hash = (19 * hash) + getDescriptor().hashCode();
    hash = (37 * hash) + OPRESULT_FIELD_NUMBER;
    hash = (53 * hash) + getOpResult();
    hash = (29 * hash) + unknownFields.hashCode();
    memoizedHashCode = hash;
    return hash;
  }

  public static com.q3d.demo.api.user.grpc.OpResultGrpc parseFrom(
      java.nio.ByteBuffer data)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data);
  }
  public static com.q3d.demo.api.user.grpc.OpResultGrpc parseFrom(
      java.nio.ByteBuffer data,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data, extensionRegistry);
  }
  public static com.q3d.demo.api.user.grpc.OpResultGrpc parseFrom(
      com.google.protobuf.ByteString data)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data);
  }
  public static com.q3d.demo.api.user.grpc.OpResultGrpc parseFrom(
      com.google.protobuf.ByteString data,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data, extensionRegistry);
  }
  public static com.q3d.demo.api.user.grpc.OpResultGrpc parseFrom(byte[] data)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data);
  }
  public static com.q3d.demo.api.user.grpc.OpResultGrpc parseFrom(
      byte[] data,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data, extensionRegistry);
  }
  public static com.q3d.demo.api.user.grpc.OpResultGrpc parseFrom(java.io.InputStream input)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input);
  }
  public static com.q3d.demo.api.user.grpc.OpResultGrpc parseFrom(
      java.io.InputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input, extensionRegistry);
  }
  public static com.q3d.demo.api.user.grpc.OpResultGrpc parseDelimitedFrom(java.io.InputStream input)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseDelimitedWithIOException(PARSER, input);
  }
  public static com.q3d.demo.api.user.grpc.OpResultGrpc parseDelimitedFrom(
      java.io.InputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseDelimitedWithIOException(PARSER, input, extensionRegistry);
  }
  public static com.q3d.demo.api.user.grpc.OpResultGrpc parseFrom(
      com.google.protobuf.CodedInputStream input)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input);
  }
  public static com.q3d.demo.api.user.grpc.OpResultGrpc parseFrom(
      com.google.protobuf.CodedInputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input, extensionRegistry);
  }

  public Builder newBuilderForType() { return newBuilder(); }
  public static Builder newBuilder() {
    return DEFAULT_INSTANCE.toBuilder();
  }
  public static Builder newBuilder(com.q3d.demo.api.user.grpc.OpResultGrpc prototype) {
    return DEFAULT_INSTANCE.toBuilder().mergeFrom(prototype);
  }
  public Builder toBuilder() {
    return this == DEFAULT_INSTANCE
        ? new Builder() : new Builder().mergeFrom(this);
  }

  @java.lang.Override
  protected Builder newBuilderForType(
      com.google.protobuf.GeneratedMessageV3.BuilderParent parent) {
    Builder builder = new Builder(parent);
    return builder;
  }
  /**
   * <pre>
   * 定义操作响应结构体 OpResultGrpc
   * </pre>
   *
   * Protobuf type {@code OpResultGrpc}
   */
  public static final class Builder extends
      com.google.protobuf.GeneratedMessageV3.Builder<Builder> implements
      // @@protoc_insertion_point(builder_implements:OpResultGrpc)
      com.q3d.demo.api.user.grpc.OpResultGrpcOrBuilder {
    public static final com.google.protobuf.Descriptors.Descriptor
        getDescriptor() {
      return com.q3d.demo.api.user.grpc.CommonProto.internal_static_OpResultGrpc_descriptor;
    }

    protected com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
        internalGetFieldAccessorTable() {
      return com.q3d.demo.api.user.grpc.CommonProto.internal_static_OpResultGrpc_fieldAccessorTable
          .ensureFieldAccessorsInitialized(
              com.q3d.demo.api.user.grpc.OpResultGrpc.class, com.q3d.demo.api.user.grpc.OpResultGrpc.Builder.class);
    }

    // Construct using com.q3d.demo.api.user.grpc.OpResultGrpc.newBuilder()
    private Builder() {
      maybeForceBuilderInitialization();
    }

    private Builder(
        com.google.protobuf.GeneratedMessageV3.BuilderParent parent) {
      super(parent);
      maybeForceBuilderInitialization();
    }
    private void maybeForceBuilderInitialization() {
      if (com.google.protobuf.GeneratedMessageV3
              .alwaysUseFieldBuilders) {
      }
    }
    public Builder clear() {
      super.clear();
      opResult_ = 0;

      return this;
    }

    public com.google.protobuf.Descriptors.Descriptor
        getDescriptorForType() {
      return com.q3d.demo.api.user.grpc.CommonProto.internal_static_OpResultGrpc_descriptor;
    }

    public com.q3d.demo.api.user.grpc.OpResultGrpc getDefaultInstanceForType() {
      return com.q3d.demo.api.user.grpc.OpResultGrpc.getDefaultInstance();
    }

    public com.q3d.demo.api.user.grpc.OpResultGrpc build() {
      com.q3d.demo.api.user.grpc.OpResultGrpc result = buildPartial();
      if (!result.isInitialized()) {
        throw newUninitializedMessageException(result);
      }
      return result;
    }

    public com.q3d.demo.api.user.grpc.OpResultGrpc buildPartial() {
      com.q3d.demo.api.user.grpc.OpResultGrpc result = new com.q3d.demo.api.user.grpc.OpResultGrpc(this);
      result.opResult_ = opResult_;
      onBuilt();
      return result;
    }

    public Builder clone() {
      return (Builder) super.clone();
    }
    public Builder setField(
        com.google.protobuf.Descriptors.FieldDescriptor field,
        java.lang.Object value) {
      return (Builder) super.setField(field, value);
    }
    public Builder clearField(
        com.google.protobuf.Descriptors.FieldDescriptor field) {
      return (Builder) super.clearField(field);
    }
    public Builder clearOneof(
        com.google.protobuf.Descriptors.OneofDescriptor oneof) {
      return (Builder) super.clearOneof(oneof);
    }
    public Builder setRepeatedField(
        com.google.protobuf.Descriptors.FieldDescriptor field,
        int index, java.lang.Object value) {
      return (Builder) super.setRepeatedField(field, index, value);
    }
    public Builder addRepeatedField(
        com.google.protobuf.Descriptors.FieldDescriptor field,
        java.lang.Object value) {
      return (Builder) super.addRepeatedField(field, value);
    }
    public Builder mergeFrom(com.google.protobuf.Message other) {
      if (other instanceof com.q3d.demo.api.user.grpc.OpResultGrpc) {
        return mergeFrom((com.q3d.demo.api.user.grpc.OpResultGrpc)other);
      } else {
        super.mergeFrom(other);
        return this;
      }
    }

    public Builder mergeFrom(com.q3d.demo.api.user.grpc.OpResultGrpc other) {
      if (other == com.q3d.demo.api.user.grpc.OpResultGrpc.getDefaultInstance()) return this;
      if (other.getOpResult() != 0) {
        setOpResult(other.getOpResult());
      }
      this.mergeUnknownFields(other.unknownFields);
      onChanged();
      return this;
    }

    public final boolean isInitialized() {
      return true;
    }

    public Builder mergeFrom(
        com.google.protobuf.CodedInputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws java.io.IOException {
      com.q3d.demo.api.user.grpc.OpResultGrpc parsedMessage = null;
      try {
        parsedMessage = PARSER.parsePartialFrom(input, extensionRegistry);
      } catch (com.google.protobuf.InvalidProtocolBufferException e) {
        parsedMessage = (com.q3d.demo.api.user.grpc.OpResultGrpc) e.getUnfinishedMessage();
        throw e.unwrapIOException();
      } finally {
        if (parsedMessage != null) {
          mergeFrom(parsedMessage);
        }
      }
      return this;
    }

    private int opResult_ ;
    /**
     * <code>int32 opResult = 1;</code>
     */
    public int getOpResult() {
      return opResult_;
    }
    /**
     * <code>int32 opResult = 1;</code>
     */
    public Builder setOpResult(int value) {
      
      opResult_ = value;
      onChanged();
      return this;
    }
    /**
     * <code>int32 opResult = 1;</code>
     */
    public Builder clearOpResult() {
      
      opResult_ = 0;
      onChanged();
      return this;
    }
    public final Builder setUnknownFields(
        final com.google.protobuf.UnknownFieldSet unknownFields) {
      return super.setUnknownFieldsProto3(unknownFields);
    }

    public final Builder mergeUnknownFields(
        final com.google.protobuf.UnknownFieldSet unknownFields) {
      return super.mergeUnknownFields(unknownFields);
    }


    // @@protoc_insertion_point(builder_scope:OpResultGrpc)
  }

  // @@protoc_insertion_point(class_scope:OpResultGrpc)
  private static final com.q3d.demo.api.user.grpc.OpResultGrpc DEFAULT_INSTANCE;
  static {
    DEFAULT_INSTANCE = new com.q3d.demo.api.user.grpc.OpResultGrpc();
  }

  public static com.q3d.demo.api.user.grpc.OpResultGrpc getDefaultInstance() {
    return DEFAULT_INSTANCE;
  }

  private static final com.google.protobuf.Parser<OpResultGrpc>
      PARSER = new com.google.protobuf.AbstractParser<OpResultGrpc>() {
    public OpResultGrpc parsePartialFrom(
        com.google.protobuf.CodedInputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws com.google.protobuf.InvalidProtocolBufferException {
      return new OpResultGrpc(input, extensionRegistry);
    }
  };

  public static com.google.protobuf.Parser<OpResultGrpc> parser() {
    return PARSER;
  }

  @java.lang.Override
  public com.google.protobuf.Parser<OpResultGrpc> getParserForType() {
    return PARSER;
  }

  public com.q3d.demo.api.user.grpc.OpResultGrpc getDefaultInstanceForType() {
    return DEFAULT_INSTANCE;
  }

}

