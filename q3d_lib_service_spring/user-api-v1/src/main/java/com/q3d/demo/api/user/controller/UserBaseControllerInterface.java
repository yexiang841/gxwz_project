package com.q3d.demo.api.user.controller;

import javax.validation.Valid;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import com.q3d.demo.api.user.dto.UserBaseP1Dto;
import com.q3d.demo.api.user.dto.UserBaseP1K1Dto;
import com.q3d.demo.api.user.dto.UserBaseP1K2Dto;
import com.q3d.demo.api.user.dto.UserBaseP1ListDto;
import com.q3d.demo.api.user.dto.UserBaseR1K1Dto;
import com.q3d.demo.api.user.dto.UserBaseR1K1ListDto;
import com.q3d.demo.api.user.dto.UserBaseR1K2Dto;
import com.q3d.demo.api.user.dto.UserBaseR1K2ListDto;
import com.q3d.demo.common.lib.service.dto.CountResultDto;
import com.q3d.demo.common.lib.service.dto.IdExtListDto;
import com.q3d.demo.common.lib.service.dto.OpResultDto;
import com.q3d.demo.common.lib.service.dto.PageRequestDto;
import com.q3d.demo.common.lib.service.dto.StringDto;
import com.q3d.demo.common.lib.service.dto.UuidDto;
import com.q3d.demo.common.lib.service.dto.UuidListDto;
import com.q3d.demo.common.lib.service.response.ResponseBase;

import io.swagger.v3.oas.annotations.Operation;

@SuppressWarnings(value = "all") /** 已经有 ApiOperation 了，可以忽略方法注释警告 */
@Validated /** 在 Controller 层面开启校验 */
/**
 * 为 user-svc-v1 定义 UserBase 接口
 * Mapping 和 Swagger 写在这一层级是为了 FeignClient 和 CondtrollerBase 类同时使用
 * 
 * @author 叶湘 ( weixin:yexiang841, email:yexiang841@qq.com )
 */
public interface UserBaseControllerInterface {

  @GetMapping(value = "/hello/{text}")
  @Operation(summary = "字符串测试接口，从 Path 读取字符串", description = "")
  public ResponseBase<StringDto> hello(@PathVariable("text") String text);

  @PostMapping(value = "/add/one")
  @Operation(summary = "新增一条记录，从 RequestBody 读取字段参数", description = "")
  public ResponseBase<UuidDto> addOne(@RequestBody @Valid UserBaseP1Dto dtoUserBaseP1);

  @PostMapping(value = "/add/batch")
  @Operation(summary = "批量新增记录，从 RequestBody 读取字段列表", description = "")
  public ResponseBase<UuidListDto> addBatch(@RequestBody @Valid UserBaseP1ListDto dtoP1List);

  @DeleteMapping(value = "/remove/one")
  @Operation(summary = "按 UUID 删除一条记录，从 RequestParam 读取 UUID 参数", description = "")
  public ResponseBase<OpResultDto> removeOne(@RequestParam(value = "uuid", required = true) String uuid);

  @DeleteMapping(value = "/remove/batch")
  @Operation(summary = "按 UUID 列表批量删除记录，从 RequestBody 读取 UUID 列表", description = "")
  public ResponseBase<OpResultDto> removeBatch(@RequestBody @Valid UuidListDto dtoUuidList);

  @DeleteMapping(value = "/delete/one")
  @Operation(summary = "按 ID 删除一条记录，从 RequestParam 读取 ID 参数", description = "")
  public ResponseBase<OpResultDto> deleteOne(@RequestParam(value = "id", required = true) String idExt);

  @DeleteMapping(value = "/delete/batch")
  @Operation(summary = "按 ID 列表批量删除记录，从 RequestBody 读取 ID 列表", description = "")
  public ResponseBase<OpResultDto> deleteBatch(@RequestBody @Valid IdExtListDto dtoIdExtList);

  @PutMapping(value = "/revive/one")
  @Operation(summary = "按 ID 恢复一条被标记删除的记录，从 RequestParam 读取 ID 参数", description = "")
  public ResponseBase<OpResultDto> reviveOne(@RequestParam(value = "id", required = true) String idExt);

  @PutMapping(value = "/revive/batch")
  @Operation(summary = "按 ID 列表批量恢复被标记删除的记录，从 RequestBody 读取 ID 列表", description = "")
  public ResponseBase<OpResultDto> reviveBatch(@RequestBody @Valid IdExtListDto dtoIdExtList);

  @PutMapping(value = "/edit/selective")
  @Operation(summary = "按 UUID 选择性修改一条记录，从 RequestBody 读取字段参数，只更新非空字段", description = "")
  public ResponseBase<OpResultDto> editSelective(@RequestBody @Valid UserBaseP1K1Dto dtoP1K1);

  @PutMapping(value = "/update/selective")
  @Operation(summary = "按 ID 选择性修改一条记录，从 RequestBody 读取字段参数，只更新非空字段", description = "")
  public ResponseBase<OpResultDto> updateSelective(@RequestBody @Valid UserBaseP1K2Dto dtoP1K1);

  @GetMapping(value = "/get/count")
  @Operation(summary = "查询记录总数", description = "")
  public ResponseBase<CountResultDto> getCount();

  @GetMapping(value = "/get/one/{uuid}")
  @Operation(summary = "按 UUID 查询一条记录，从 Path 读取 UUID 参数", description = "")
  public ResponseBase<UserBaseR1K1Dto> getOne(@PathVariable("uuid") String uuid);

  @PostMapping(value = "/get/batch")
  @Operation(summary = "按 UUID 列表批量查询记录，从 RequestBody 读取 UUID 列表", description = "")
  public ResponseBase<UserBaseR1K1ListDto> getBatch(@RequestBody @Valid UuidListDto dtoUuidList);

  @GetMapping(value = "/get/page")
  @Operation(summary = "分页批量查询记录，从 Form 读取 page 和 limit", description = "")
  public ResponseBase<UserBaseR1K1ListDto> getByPage(@ModelAttribute @Valid PageRequestDto dtoPageRequest);

  @GetMapping(value = "/get/uuid/page")
  @Operation(summary = "分页批量查询记录，只返回 UUID 列表，从 Form 读取 page 和 limit", description = "")
  public ResponseBase<UuidListDto> getUuidByPage(@ModelAttribute @Valid PageRequestDto dtoPageRequest);

  @GetMapping(value = "/select/count")
  @Operation(summary = "查询记录总数，包括标记为删除的记录", description = "")
  public ResponseBase<CountResultDto> selectCount();

  @GetMapping(value = "/select/count/removed")
  @Operation(summary = "查询记录总数，包括标记为删除的记录", description = "")
  public ResponseBase<CountResultDto> selectCountRemoved();

  @GetMapping(value = "/select/one/{id}")
  @Operation(summary = "按 ID 查询一条记录，从 Path 读取 ID 参数", description = "")
  public ResponseBase<UserBaseR1K2Dto> selectOne(@PathVariable("id") String idExt);

  @PostMapping(value = "/select/batch")
  @Operation(summary = "按 ID 列表批量查询记录，从 RequestBody 读取 ID 列表", description = "")
  public ResponseBase<UserBaseR1K2ListDto> selectBatch(@RequestBody @Valid IdExtListDto dtoIdExtList);

  @GetMapping(value = "/select/page")
  @Operation(summary = "按 ID 列表批量查询记录，从 Form 读取 page 和 limit", description = "")
  public ResponseBase<UserBaseR1K2ListDto> selectByPage(@ModelAttribute @Valid PageRequestDto dtoPageRequest);

  @GetMapping(value = "/select/id/page")
  @Operation(summary = "分页批量查询记录，只返回 UUID 列表，从 Form 读取 page 和 limit", description = "")
  public ResponseBase<IdExtListDto> selectIdExtByPage(@ModelAttribute @Valid PageRequestDto dtoPageRequest);

  @GetMapping(value = "/select/id/removed")
  @Operation(summary = "分页批量查询记录，只返回 UUID 列表，从 RequestParam 读取 page 和 limit", description = "")
  public ResponseBase<IdExtListDto> selectIdExtRemoved();

}