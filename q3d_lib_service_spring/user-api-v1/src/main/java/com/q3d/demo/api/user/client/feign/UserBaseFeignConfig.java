package com.q3d.demo.api.user.client.feign;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;

import com.q3d.demo.common.lib.service.response.ResponseFeignDecoder;

import feign.Feign;
import feign.codec.Decoder;

/**
 * 自定义 FeignClient 相关配置，如基础配置或者自定义解码器等
 * 此类不要注入 Spring 容器, 否则系统内所有 Feign 调用都会使用该配置
 * 
 * @author 叶湘 ( weixin:yexiang841, email:yexiang841@qq.com )
 */
public class UserBaseFeignConfig {

    /**
     * 自定义 Decoder 在此注入才能生效
     */
    @Bean
    public Decoder feignDecoder() {
        return new ResponseFeignDecoder();
    }

    @Bean
    @Scope("prototype")
    public Feign.Builder feignBuilder() {
        return Feign.builder();
        /** 指定 Http 底层使用 OkHttpClient，需要引入 feign-okhttp 包 */
        // .client(new OkHttpClient())
        /** 指定 Http 底层使用 ApacheHttpClient，需要引入 feign-okhttp 包 */
        // .client(new ApacheHttpClient())
        /** 指定底层日志工具 */
        // .logger(new Slf4jLogger())
        /** 自定义编码器，实践证明放在这里没啥用，要放在 Encoder 单独的 @Bean 里 */
        // .encoder(encoder)
        /** 自定义解码器，实践证明放在这里没啥用，要放在 Decoder 单独的 @Bean 里 */
        // .decoder(new ResponseFeignDecoder());
        /** 不知道是啥 */
        // .contract(null);
        /** 自定义拦截器，验证要用到 */
        // .requestInterceptor(new BasicAuthRequestInterceptor("user", "user"))
        /** 指定目标，这里作为公共配置文件没有必要 */
        // .target(UserBaseFeignInterface.class, "http://user-svc-v1");
    }
}