package com.q3d.demo.svc.config.controller;

import com.q3d.demo.common.lib.service.response.ResponseBase;
import com.q3d.demo.svc.config.service.PublishV1Service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;

@RequestMapping("/publish/v1") /** 匹配路径 */
@Tag(name = "更新配置接口v1") /** SpringDoc 类注解 */
@RestController
/**
 * @author 叶湘 ( weixin:yexiang841, email:yexiang841@qq.com )
 */
public class PublishV1Controller {

    @Autowired
    private PublishV1Service publishService;

    @PostMapping(value = "/refresh")
    @Operation(summary = "刷新配置", description = "更改配置之后进行刷新以使之生效")
    public ResponseBase<Void> refresh() {
        publishService.refresh();
        ResponseBase<Void> response = new ResponseBase<Void>();
        return response;
    }

}
