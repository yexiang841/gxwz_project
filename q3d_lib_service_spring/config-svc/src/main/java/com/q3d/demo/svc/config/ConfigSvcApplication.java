package com.q3d.demo.svc.config;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.config.server.EnableConfigServer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.client.RestTemplate;

import springfox.documentation.oas.annotations.EnableOpenApi;

@EnableOpenApi /** 开启 Swagger，访问地址：http://{host}:{port}/{context-path}/swagger-ui/index.html */
@EnableConfigServer /** 声明自己为 Config Server */
@EnableDiscoveryClient /** 开启服务发现，使 Eureka 或者 Nacos 能按照服务名寻址 */
@EnableConfigurationProperties /** 开启 @ConfigurationProperties，支持从配置文件或远程配置读取属性注入对象 */
@ComponentScan(basePackages = { /** Bean 扫描路径，对依赖顺序也有要求 */
		"com.q3d.demo.svc.config.**.**", /** Hello-Svc 包扫描路径 */
		"com.q3d.demo.svc.config.**.**.**", /** Hello-Svc 包扫描路径 */
		"com.q3d.demo.common.lib.**.**", /** Common-Lib 包扫描路径 */
})
@SpringBootApplication() /** 微服务入口 */
/**
 * @author 叶湘 ( weixin:yexiang841, email:yexiang841@qq.com )
 */
public class ConfigSvcApplication {

	public static void main(String[] args) {
		SpringApplication.run(ConfigSvcApplication.class, args);
	}

	@Bean /** 将 RestTemplate 注入为 Bean，否则无法在使用时 @Autowired */
	public RestTemplate restTemplate() {
		return new RestTemplate();

	}

}
