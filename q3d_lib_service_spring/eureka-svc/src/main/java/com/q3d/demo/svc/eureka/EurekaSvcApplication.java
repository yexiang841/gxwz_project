package com.q3d.demo.svc.eureka;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;


@EnableEurekaServer /** 声明自己为 Eureka-Server */
@SpringBootApplication /** 微服务入口 */
/**
 * @author 叶湘 ( weixin:yexiang841, email:yexiang841@qq.com )
 */
public class EurekaSvcApplication {

	public static void main(String[] args) {
		SpringApplication.run(EurekaSvcApplication.class, args);
	}

}
