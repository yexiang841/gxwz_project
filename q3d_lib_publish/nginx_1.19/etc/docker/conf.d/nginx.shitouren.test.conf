    # MicroService HTTP server
    server {
        listen       80;
        server_name  svc.shitouren.test;
        charset      utf-8;
        access_log   /opt/q3d_lib_publish/nginx_1.19/log/docker/access.http.svc.shitouren.test.log;
        
        location /nacos/ {
            proxy_pass http://c-nacos:8848/nacos/;  # 容器配置的环境变量 : 默认端口
            proxy_redirect                   off;
            proxy_pass_header                Server;
            proxy_pass_header                Set-Cookie;
            proxy_set_header Host            $http_host;
            proxy_set_header X-Forwarded_For $proxy_add_x_forwarded_for;
            proxy_set_header X-Real-IP       $remote_addr;
            proxy_set_header X-Scheme        $scheme;                                           
            add_header Access-Control-Allow-Origin *;
        }
    }

    # ES-Head HTTP server
    server {
        listen       80;
        server_name  es-head.shitouren.test;
        charset      utf-8;
        access_log   /opt/q3d_lib_publish/nginx_1.19/log/docker/access.http.es-head.shitouren.test.log;

        location / {
            proxy_pass http://c-es-head:9100/; # 容器配置的环境变量 : 默认端口 
            proxy_redirect                   off;
            proxy_pass_header                Server;
            proxy_pass_header                Set-Cookie;
            proxy_set_header Host            $http_host;
            proxy_set_header X-Forwarded_For $proxy_add_x_forwarded_for;
            proxy_set_header X-Real-IP       $remote_addr;
            proxy_set_header X-Scheme        $scheme;
            add_header Access-Control-Allow-Origin *;
        }
    }

    # 日志系统 Elasticsearch HTTP server
    server {
        listen       80;
        server_name  es.log.shitouren.test;
        charset      utf-8;
        access_log   /opt/q3d_lib_publish/nginx_1.19/log/docker/access.http.es.log.shitouren.test.log;

        location / {
            proxy_pass http://c-elk-log:9200/; # 容器配置的环境变量 : 默认端口 
            proxy_redirect                   off;
            proxy_pass_header                Server;
            proxy_pass_header                Set-Cookie;
            proxy_set_header Host            $http_host;
            proxy_set_header X-Forwarded_For $proxy_add_x_forwarded_for;
            proxy_set_header X-Real-IP       $remote_addr;
            proxy_set_header X-Scheme        $scheme;
            # add_header Access-Control-Allow-Origin *; # 这个配置需要关掉，然后在 elasticsearch.yml 中配置
        }
    }

    # 日志系统 Kibana HTTP server
    server {
        listen       80;
        server_name  kibana.log.shitouren.test;
        charset      utf-8;
        access_log   /opt/q3d_lib_publish/nginx_1.19/log/docker/access.http.kibana.log.shitouren.test.log;

        location / {
            proxy_pass http://c-elk-log:5601/;  # 容器配置的环境变量 : 默认端口
            proxy_redirect                   off;
            proxy_pass_header                Server;
            proxy_pass_header                Set-Cookie;
            proxy_set_header Host            $http_host;
            proxy_set_header X-Forwarded_For $proxy_add_x_forwarded_for;
            proxy_set_header X-Real-IP       $remote_addr;
            proxy_set_header X-Scheme        $scheme;                                           
            add_header Access-Control-Allow-Origin *;
        }
    }

    # 日志系统 Cerebro HTTP server
    server {
        listen       80;
        server_name  cerebro.log.shitouren.test;
        charset      utf-8;
        access_log   /opt/q3d_lib_publish/nginx_1.19/log/docker/access.http.cerebro.log.shitouren.test.log;

        location / {
            proxy_pass http://c-cerebro-log:9000/;  # 容器配置的环境变量 : 默认端口
            proxy_redirect                   off;
            proxy_pass_header                Server;
            proxy_pass_header                Set-Cookie;
            proxy_set_header Host            $http_host;
            proxy_set_header X-Forwarded_For $proxy_add_x_forwarded_for;
            proxy_set_header X-Real-IP       $remote_addr;
            proxy_set_header X-Scheme        $scheme;                                           
            add_header Access-Control-Allow-Origin *;
        }
    }

    # CloudBeaver (DBeaver Web 版) HTTP server
    server {
        listen       80;
        server_name  cloudbeaver.shitouren.test;
        charset      utf-8;
        access_log   /opt/q3d_lib_publish/nginx_1.19/log/docker/access.http.cloudbeaver.shitouren.test.log;

        location / {
            proxy_pass http://c-cloudbeaver:8978/;  # 容器配置的环境变量 : 默认端口
            proxy_redirect                   off;
            proxy_pass_header                Server;
            proxy_pass_header                Set-Cookie;
            proxy_set_header Host            $http_host;
            proxy_set_header X-Forwarded_For $proxy_add_x_forwarded_for;
            proxy_set_header X-Real-IP       $remote_addr;
            proxy_set_header X-Scheme        $scheme;                                           
            add_header Access-Control-Allow-Origin *;
        }
    }

