#!/bin/bash

file_path=$(cd `dirname $0`; pwd) # 当前文件路径
svc_path=$(cd $file_path/..; pwd) # 子项目路径
base_path=$(cd $svc_path/..; pwd) # 部署代码库路径

logstash_etc_path=/usr/local/etc/logstash # logstash 配置目录
logstash_data_path=/usr/local/opt/logstash/data # logstash 数据目录
logstash_plugin_path=/usr/local/opt/logstash/plugins # logstash 插件目录
logstash_log_path=/usr/local/var/log/logstash # mongodb 日志目录

rm -rf $logstash_etc_path
ln -s $svc_path/etc/mac $logstash_etc_path
ls -l  $logstash_etc_path

rm -rf $logstash_data_path
ln -s $svc_path/data/mac $logstash_data_path
ls -l  $logstash_data_path

rm -rf $logstash_plugin_path
ln -s $svc_path/plugin/mac $logstash_plugin_path
ls -l  $logstash_plugin_path

rm -rf $logstash_log_path
ln -s $svc_path/log/mac $logstash_log_path
ls -l  $logstash_log_path

