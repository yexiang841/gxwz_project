#!/bin/bash

file_path=$(cd `dirname $0`; pwd) # 当前文件路径
svc_path=$(cd $file_path/..; pwd) # 子项目路径
base_path=$(cd $svc_path/..; pwd) # 部署代码库路径

cmd="rm -rf $svc_path/data/docker/*"
echo $cmd
eval $cmd

cmd="rm -rf $svc_path/plugin/docker/*"
echo $cmd
eval $cmd

cmd="rm -rf $svc_path/log/docker/*"
echo $cmd
eval $cmd

