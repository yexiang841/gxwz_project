#!/bin/bash

file_path=$(cd `dirname $0`; pwd) # 当前文件路径
svc_path=$(cd $file_path/..; pwd) # 子项目路径
base_path=$(cd $svc_path/..; pwd) # 部署代码库路径

rabbitmq_etc_path=/usr/local/etc/rabbitmq # rabbitmq 配置目录
rabbitmq_data_path=/usr/local/var/lib/rabbitmq # rabbitmq 数据目录
rabbitmq_log_path=/usr/local/var/log/rabbitmq # rabbitmq 日志目录

rm -rf $rabbitmq_etc_path
ln -s $svc_path/etc/mac $rabbitmq_etc_path
ls -l  $rabbitmq_etc_path

rm -rf $rabbitmq_data_path
ln -s $svc_path/data/mac $rabbitmq_data_path
ls -l  $rabbitmq_data_path

rm -rf $rabbitmq_log_path
ln -s $svc_path/log/mac $rabbitmq_log_path
ls -l  $rabbitmq_log_path

