/******************************************/
/*   数据库 = test_nacos_config   */
/*   用户名 = TESTuser */
/******************************************/

/* 确认数据库 */
CREATE DATABASE IF NOT EXISTS test_nacos_config DEFAULT CHARSET utf8 COLLATE utf8_general_ci;

/* 切换数据库 */
USE test_nacos_config;

/* 执行建表脚本 */
SOURCE /opt/q3d_lib_publish/nacos_2.1/etc/nacos-mysql.sql

/*INSERT IGNORE INTO users (username, password, enabled) VALUES ('nacos', '$2a$10$EuWPZHzz32dJN7jexM34MOeYirDdFAZm2kuWj7VEOJhhZkDrxfvUu', TRUE);*/
INSERT IGNORE INTO users (username, password, enabled) VALUES ('TESTuser', '$2a$10$bAipes5TugIix2bN3HPmzevBFfJisfRo1.DVimpHTfU2SiU7ypE2y', TRUE); /* TESTuser@123456 */

/*INSERT IGNORE INTO roles (username, role) VALUES ('nacos', 'ROLE_ADMIN');*/
INSERT IGNORE INTO roles (username, role) VALUES ('TESTuser', 'ROLE_ADMIN');

/* 确认账户 */
CREATE USER IF NOT EXISTS 'TESTuser'@'%' IDENTIFIED by 'TESTuser@123456';

/* 授权 */
GRANT ALL PRIVILEGES ON test_nacos_config.* TO 'TESTuser'@'%';

/* 生效 */
FLUSH PRIVILEGES;
