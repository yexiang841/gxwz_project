/******************************************/
/*   数据库 = dev_nacos_config   */
/*   用户名 = DEVuser */
/******************************************/

/* 确认数据库 */
CREATE DATABASE IF NOT EXISTS dev_nacos_config DEFAULT CHARSET utf8 COLLATE utf8_general_ci;

/* 切换数据库 */
USE dev_nacos_config;

/* 执行建表脚本 */
SOURCE /opt/q3d_lib_publish/nacos_2.1/etc/nacos-mysql.sql

/*INSERT INTO users (username, password, enabled) VALUES ('nacos', '$2a$10$EuWPZHzz32dJN7jexM34MOeYirDdFAZm2kuWj7VEOJhhZkDrxfvUu', TRUE);*/ /* password: nacos */
INSERT INTO users (username, password, enabled) VALUES ('DEVuser', '$2a$10$2IC1Am1pfjUaMQ8jSIZ7z./oJoCfQIq2GvwY/oqgRTZrR4aVneoJm', TRUE); /* password: DEVuser@123456 */

/*INSERT INTO roles (username, role) VALUES ('nacos', 'ROLE_ADMIN');*/
INSERT INTO roles (username, role) VALUES ('DEVuser', 'ROLE_ADMIN'); /* 此处的 DEVuser 是 Nacos 账号*/

/* 确认账户 */
CREATE USER IF NOT EXISTS 'DEVuser'@'%' IDENTIFIED by 'DEVuser@123456';

/* 授权 */
GRANT ALL PRIVILEGES ON dev_nacos_config.* TO 'DEVuser'@'%';

/* 生效 */
FLUSH PRIVILEGES;
