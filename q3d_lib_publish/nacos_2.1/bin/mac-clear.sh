#!/bin/bash

file_path=$(cd `dirname $0`; pwd) # 当前文件路径
svc_path=$(cd $file_path/..; pwd) # 子项目路径
base_path=$(cd $svc_path/..; pwd) # 部署代码库路径

cmd="rm -rf $svc_path/data/*" # 数据路径，Nacos 源码中写死了 /data 路径，难以完全更改
echo $cmd
eval $cmd

cmd="rm -rf $svc_path/logs/*" # 日志路径，Nacos 源码中写死了 /logs 路径，难以完全更改
echo $cmd
eval $cmd
