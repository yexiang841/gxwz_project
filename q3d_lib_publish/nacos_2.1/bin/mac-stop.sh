#!/bin/bash

file_path=$(cd `dirname $0`; pwd) # 当前文件路径
svc_path=$(cd $file_path/..; pwd) # 子项目路径
base_path=$(cd $svc_path/..; pwd) # 部署代码库路径
data_path=$(cd $svc_path/data; pwd) # 数据路径，Nacos 源码中写死了 /data 路径，难以完全更改
log_path=$(cd $svc_path/logs; pwd) # 日志路径，Nacos 源码中写死了 /logs 路径，难以完全更改

cmd="$svc_path/bin/shutdown.sh"
echo $cmd
eval $cmd

cmd="ps aux | grep nacos"
echo $cmd
eval $cmd

