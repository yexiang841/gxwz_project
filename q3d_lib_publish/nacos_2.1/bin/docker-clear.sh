#!/bin/bash

file_path=$(cd `dirname $0`; pwd) # 当前文件路径
svc_path=$(cd $file_path/..; pwd) # 子项目路径
base_path=$(cd $svc_path/..; pwd) # 部署代码库路径
data_path=$(cd $svc_path/data-docker; pwd) # 数据路径，Nacos 源码中写死了 /data 路径，本地测试环境已占用，因此 docker 环境需要避开
log_path=$(cd $svc_path/log-docker; pwd) # 日志路径，Nacos 源码中写死了 /logs 路径，本地测试环境已占用，因此 docker 环境需要避开

cmd="rm -rf $svc_path/data-docker/*"
echo $cmd
eval $cmd

cmd="rm -rf $svc_path/log-docker/*"
echo $cmd
eval $cmd
