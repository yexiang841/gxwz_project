#!/bin/bash

chown -R elasticsearch:elasticsearch /opt/q3d_lib_publish/sebp_elk_7.15/data/docker/elasticsearch
chown -R elasticsearch:elasticsearch /opt/q3d_lib_publish/sebp_elk_7.15/log/docker/elasticsearch
chown -R logstash:logstash /opt/q3d_lib_publish/sebp_elk_7.15/log/docker/logstash
chown -R kibana:kibana /opt/q3d_lib_publish/sebp_elk_7.15/log/docker/kibana

