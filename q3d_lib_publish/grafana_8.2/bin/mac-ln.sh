#!/bin/bash

file_path=$(cd `dirname $0`; pwd) # 当前文件路径
svc_path=$(cd $file_path/..; pwd) # 子项目路径
base_path=$(cd $svc_path/..; pwd) # 部署代码库路径

grafana_etc_path=/usr/local/etc/grafana # grafana 配置目录
grafana_data_path=/usr/local/var/lib/grafana # grafana 数据目录
grafana_plugin_path=/usr/local/var/lib/grafana/plugins # grafana 插件目录
grafana_log_path=/usr/local/var/log/grafana # mongodb 日志目录

rm -rf $grafana_etc_path
ln -s $svc_path/etc/mac/ $grafana_etc_path
ls -l  $grafana_etc_path

rm -rf $grafana_data_path
ln -s $svc_path/data/mac $grafana_data_path
ls -l  $grafana_data_path

rm -rf $grafana_plugin_path
ln -s $svc_path/plugin/mac $grafana_plugin_path
ls -l  $grafana_plugin_path

rm -rf $grafana_log_path
ln -s $svc_path/log/mac $grafana_log_path
ls -l  $grafana_log_path

