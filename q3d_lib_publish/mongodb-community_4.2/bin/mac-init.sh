#!/bin/bash

# brew services start mongodb-community@4.2

# // 登录 MongoDB Shell（无账户密码）
# mongo --host 127.0.0.1 --port 27017
# // 选择管理者库
# use admin
# // 配置 root 账户密码
# db.createUser({user:"root",pwd:"DEVroot@123456", roles:["root"]})
# // 验证效果
# show users
# // 先退出
# quit()

# // 重新登录 MongoDB Shell（使用刚配置的 root 账户密码）
# mongo --host 127.0.0.1 --port 27017 -u root -p DEVroot@123456
# // 选择管理者库
# use admin
# // 配置 DEVuser 账户密码，权限见说明；
# db.createUser({user:"DEVuser",pwd:"DEVuser@123456", roles:["userAdminAnyDatabase","dbAdminAnyDatabase","readWriteAnyDatabase"]})
# // 验证效果
# show users
# // 退出
# quit()

