#!/bin/bash

file_path=$(cd `dirname $0`; pwd) # 当前文件路径
svc_path=$(cd $file_path/..; pwd) # 子项目路径
base_path=$(cd $svc_path/..; pwd) # 部署代码库路径

mongodb_etc_file=/usr/local/etc/mongod.conf # mongodb 配置文件
mongodb_data_path=/usr/local/var/mongodb # mongodb 数据目录
mongodb_log_path=/usr/local/var/log/mongodb # mongodb 日志目录

rm -rf $mongodb_etc_file
ln -s $svc_path/etc/mac/mongod.conf $mongodb_etc_file
ls -l  $mongodb_etc_file

rm -rf $mongodb_data_path
ln -s $svc_path/data/mac $mongodb_data_path
ls -l  $mongodb_data_path

rm -rf $mongodb_log_path
ln -s $svc_path/log/mac $mongodb_log_path
ls -l  $mongodb_log_path

