#!/bin/bash

file_path=$(cd `dirname $0`; pwd) # 当前文件路径
svc_path=$(cd $file_path/..; pwd) # 子项目路径
base_path=$(cd $svc_path/..; pwd) # 部署代码库路径
data_path=$(cd $svc_path/data/docker; pwd) # 数据路径
log_path=$(cd $svc_path/log/docker; pwd) # 日志路径

cmd="rm -rf $data_path/*"
echo $cmd
eval $cmd

cmd="rm -rf $log_path/*.log"
echo $cmd
eval $cmd

cmd="rm -rf $log_path/redolog/*"
echo $cmd
eval $cmd

cmd="rm -rf $log_path/undolog/*"
echo $cmd
eval $cmd

cmd="rm -rf $log_path/binlog/*"
echo $cmd
eval $cmd

