#!/bin/bash

file_path=$(cd `dirname $0`; pwd) # 当前文件路径
svc_path=$(cd $file_path/..; pwd) # 子项目路径
base_path=$(cd $svc_path/..; pwd) # 部署代码库路径
data_path=$(cd $svc_path/data/mac; pwd) # 数据路径
log_path=$(cd $svc_path/log/mac; pwd) # 日志路径

mysql_base_path=/usr/local/opt/mysql\@5.7 # mysql 软件目录
mysql_data_path=/usr/local/var/mysql # mysql 数据目录
mysql_etc_file=/usr/local/etc/my.cnf # mysql 配置文件
mysql_log_path=/usr/local/var/log/mysql # mysql 日志目录

rm -rf $mysql_etc_file
ln -s $svc_path/etc/mac/my.cnf $mysql_etc_file
ls -l  $mysql_etc_file

rm -rf $mysql_data_path
mkdir -p $data_path
ln -s $data_path $mysql_data_path
ls -l  $mysql_data_path

rm -rf $mysql_log_path
mkdir -p $log_path
mkdir -p $log_path/redolog
mkdir -p $log_path/undolog
mkdir -p $log_path/binlog
ln -s $log_path $mysql_log_path
ls -l  $mysql_log_path
ls -l  $mysql_log_path/*

