#!/bin/bash

file_path=$(cd `dirname $0`; pwd) # 当前文件路径
svc_path=$(cd $file_path/..; pwd) # 子项目路径
base_path=$(cd $svc_path/..; pwd) # 部署代码库路径

# 以下初始化脚本仅在非容器部署使用

# 新建非管理员账户/密码
mysql -h127.0.0.1 -P3306 -uroot -pDEVroot@123456 -e "CREATE USER IF NOT EXISTS 'TESTuser'@'%' IDENTIFIED by 'TESTuser@123456'"

# 验证非管理员账户/密码
mysql -h127.0.0.1 -P3306 -uTESTuser -pTESTuser@123456 -e "show databases"

