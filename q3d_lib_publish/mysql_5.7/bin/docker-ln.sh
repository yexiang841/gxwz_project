#!/bin/bash

file_path=$(cd `dirname $0`; pwd) # 当前文件路径
svc_path=$(cd $file_path/..; pwd) # 子项目路径
base_path=$(cd $svc_path/..; pwd) # 部署代码库路径
data_path=$(cd $svc_path/data/docker; pwd) # 数据路径
log_path=$(cd $svc_path/log/docker; pwd) # 日志路径

mkdir -p $data_path
ls -l  $data_path

mkdir -p $log_path
ls -l  $log_path

