#!/bin/bash

file_path=$(cd `dirname $0`; pwd) # 当前文件路径
svc_path=$(cd $file_path/..; pwd) # 子项目路径
base_path=$(cd $svc_path/..; pwd) # 部署代码库路径

# 以下初始化脚本仅在非容器部署使用

mysqld --initialize-insecure --user=$(whoami) --basedir=/usr/local/opt/mysql@5.7 --datadir=/usr/local/var/mysql --tmpdir=/tmp

brew services start mysql@5.7

brew services list

mysql_secure_installation
# 其中包括设定 root 密码和配置等，依据提示操作；
# 如：root 密码：DEVroot@123456
