#!/bin/bash

file_path=$(cd `dirname $0`; pwd) # 当前文件路径
svc_path=$(cd $file_path/..; pwd) # 子项目路径
base_path=$(cd $svc_path/..; pwd) # 部署代码库路径

kibana_etc_path=/usr/local/etc/kibana # kibana 配置目录
kibana_data_path=/usr/local/opt/kibana/data # kibana 数据目录
kibana_log_path=/usr/local/var/log/kibana # mongodb 日志目录
kibana_plugin_path=/usr/local/opt/kibana/plugins # kibana 插件目录

rm -rf $kibana_etc_path
ln -s $svc_path/etc/mac/ $kibana_etc_path
ls -l  $kibana_etc_path

rm -rf $kibana_data_path
ln -s $svc_path/data/mac $kibana_data_path
ls -l  $kibana_data_path

rm -rf $kibana_plugin_path
ln -s $svc_path/plugin/mac $kibana_plugin_path
ls -l  $kibana_plugin_path

rm -rf $kibana_log_path
ln -s $svc_path/log/mac $kibana_log_path
ls -l  $kibana_log_path

