#!/bin/bash

file_path=$(cd `dirname $0`; pwd) # 当前文件路径
svc_path=$(cd $file_path/..; pwd) # 子项目路径
base_path=$(cd $svc_path/..; pwd) # 部署代码库路径

postgresql_data_path=/usr/local/var/postgres # postgresql 数据目录
postgresql_etc_file=/usr/local/var/postgres/postgresql.conf # postgresql 配置文件
postgresql_hba_file=/usr/local/var/postgres/pg_hba.conf # postgresql 配置文件
postgresql_log_path=/usr/local/var/log/postgresql # postgresql 日志目录

rm -rf $postgresql_data_path
ln -s $svc_path/data/mac $postgresql_data_path
ls -l  $postgresql_data_path

rm -rf $postgresql_etc_file
ln -s $svc_path/etc/mac/postgresql.conf $postgresql_etc_file
ls -l  $postgresql_etc_file

rm -rf $postgresql_hba_file
ln -s $svc_path/etc/mac/pg_hba.conf $postgresql_hba_file
ls -l  $postgresql_hba_file

rm -rf $postgresql_log_path
ln -s $svc_path/log/mac $postgresql_log_path
ls -l  $postgresql_log_path

