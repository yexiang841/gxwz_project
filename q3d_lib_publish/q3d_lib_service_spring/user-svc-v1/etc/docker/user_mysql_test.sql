/******************************************/
/*   数据库 = test_q3d_svc */
/*   用户名 = TESTuser */
/******************************************/

/* 确认数据库 */
CREATE DATABASE IF NOT EXISTS test_q3d_svc DEFAULT CHARSET utf8 COLLATE utf8_general_ci;

/* 切换数据库 */
USE test_q3d_svc;

/* 执行建表脚本 */
SOURCE /opt/q3d_lib_publish/q3d_lib_service_spring/user-svc-v1/etc/userbase/schema_userbase.sql

/* 确认账户 */
CREATE USER IF NOT EXISTS 'TESTuser'@'%' IDENTIFIED by 'TESTuser@123456';

/* 授权 */
GRANT ALL PRIVILEGES ON test_q3d_svc.* TO 'TESTuser'@'%';

/* 生效 */
FLUSH PRIVILEGES;

