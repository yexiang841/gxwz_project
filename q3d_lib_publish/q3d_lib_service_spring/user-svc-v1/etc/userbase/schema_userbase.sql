CREATE TABLE IF NOT EXISTS `t_user_base` (
  -- `id` BIGINT(20) NOT NULL AUTO_INCREMENT COMMENT '数据库自增 ID，不建议使用',
  `id` BIGINT(20) NOT NULL COMMENT '全局唯一 ID',
  `uuid` CHAR(32) CHARACTER SET ascii NOT NULL COMMENT '全局唯一 UUID，去掉 4 位横杠的 32 位十六进制数',
  `active` SMALLINT(4) NOT NULL DEFAULT 1 COMMENT '可用性标记，0：已删除，1：正常数据',
  `userid` VARCHAR(64) NOT NULL COMMENT '用户 ID，支持英文、数字、下划线',
  `nickname` VARCHAR(64) NOT NULL DEFAULT 'NULL' COMMENT '用户昵称，支持中文、英文、数字',
  `password` VARCHAR(64) NOT NULL DEFAULT 'NULL' COMMENT '加密过的密码',
  `phonenumber` VARCHAR(32) DEFAULT NULL COMMENT '手机号，支持数字、横线',
  `email` VARCHAR(64) DEFAULT NULL COMMENT '邮箱',
  `avatar` VARCHAR(128) DEFAULT NULL COMMENT '头像',
  `create_time` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY (`uuid`),
  UNIQUE KEY (`userid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='t_user_base';
