#!/bin/bash

file_path=$(cd `dirname $0`; pwd) # 当前文件路径
svc_path=$(cd $file_path/..; pwd) # 子项目路径
base_path=$(cd $svc_path/..; pwd) # 部署代码库路径

mysql -h127.0.0.1 -p3306 -uroot -pDEVroot@123456 -e "source $svc_path/uid-generator/etc/mac/uid-generator_mysql_dev.sql"
mysql -h127.0.0.1 -p3306 -uroot -pDEVroot@123456 -e "source $svc_path/hello-svc-v1/etc/mac/hello_mysql_dev.sql"
mysql -h127.0.0.1 -p3306 -uroot -pDEVroot@123456 -e "source $svc_path/user-svc-v1/etc/mac/user_mysql_dev.sql"
mysql -h127.0.0.1 -P3306 -uDEVuser -pDEVuser@123456 -e "show databases;use dev_q3d_svc;show tables"

