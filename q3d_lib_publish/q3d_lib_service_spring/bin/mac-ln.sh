#!/bin/bash

file_path=$(cd `dirname $0`; pwd) # 当前文件路径
svc_path=$(cd $file_path/..; pwd) # 子项目路径
base_path=$(cd $svc_path/..; pwd) # 部署代码库路径

publish_path=/opt/q3d_lib_publish # 部署目录

rm -rf $publish_path
ln -s $base_path $publish_path
ls -l $publish_path

