CREATE TABLE IF NOT EXISTS `t_hello_world` (
  -- `id` BIGINT(20) NOT NULL AUTO_INCREMENT COMMENT '数据库自增 ID，不建议使用',
  `id` BIGINT(20) NOT NULL COMMENT '全局唯一 ID',
  `uuid` CHAR(32) CHARACTER SET ascii NOT NULL COMMENT '全局唯一 UUID，去掉 4 位横杠的 32 位十六进制数',
  `active` SMALLINT(4) NOT NULL DEFAULT 1 COMMENT '可用性标记，0：已删除，1：正常数据',
  `create_time` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `world_name` VARCHAR(64) NOT NULL COMMENT '元宇宙名字',
  `world_desc` VARCHAR(255) DEFAULT NULL COMMENT '元宇宙描述',
  `world_age` INT(10) NOT NULL DEFAULT 0 COMMENT '元宇宙年龄',
  `world_radius` FLOAT(12,2) NOT NULL DEFAULT 0 COMMENT '元宇宙半径',
  `world_weight` DOUBLE(22,4) NOT NULL DEFAULT 0 COMMENT '元宇宙质量',
  PRIMARY KEY (`id`),
  UNIQUE KEY (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='t_hello_world';
