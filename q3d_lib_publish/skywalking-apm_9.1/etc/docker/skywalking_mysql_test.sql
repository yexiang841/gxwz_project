/******************************************/
/*   数据库 = test_skywalking   */
/*   用户名 = TESTuser */
/******************************************/

/* 确认数据库 */
CREATE DATABASE IF NOT EXISTS test_skywalking DEFAULT CHARSET utf8 COLLATE utf8_general_ci;

/* 确认账户 */
CREATE USER IF NOT EXISTS 'TESTuser'@'%' IDENTIFIED by 'TESTuser@123456';

/* 授权 */
GRANT ALL PRIVILEGES ON test_skywalking.* TO 'TESTuser'@'%';

/* 生效 */
FLUSH PRIVILEGES;
