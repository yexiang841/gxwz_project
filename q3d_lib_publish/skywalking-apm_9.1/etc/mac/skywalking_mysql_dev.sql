/******************************************/
/*   数据库 = dev_skywalking   */
/*   用户名 = DEVuser */
/******************************************/

/* 确认数据库 */
CREATE DATABASE IF NOT EXISTS dev_skywalking DEFAULT CHARSET utf8 COLLATE utf8_general_ci;

/* 确认账户 */
CREATE USER IF NOT EXISTS 'DEVuser'@'%' IDENTIFIED by 'DEVuser@123456';

/* 授权 */
GRANT ALL PRIVILEGES ON dev_skywalking.* TO 'DEVuser'@'%';

/* 生效 */
FLUSH PRIVILEGES;
