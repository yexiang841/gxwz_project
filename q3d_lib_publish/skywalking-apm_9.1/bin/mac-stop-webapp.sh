#!/bin/bash

cd `dirname $0`/../target
target_dir=`pwd`

pid=`ps ax | grep -i 'skywalking-apm' | grep 'webapp' | grep java | grep -v grep | awk '{print $1}'`
if [ -z "$pid" ] ; then
        echo "No SkyWalking-WebApp running."
        exit -1;
fi

echo "The SkyWalking-WebApp(${pid}) is running..."

kill ${pid}

echo "Send shutdown request to SkyWalking-WebApp(${pid}) OK"

