#!/bin/bash

file_path=$(cd `dirname $0`; pwd) # 当前文件路径
svc_path=$(cd $file_path/..; pwd) # 子项目路径
base_path=$(cd $svc_path/..; pwd) # 部署代码库路径

# 改用 Elasticsearch 存储 Skywalking 的数据后，就不必要再初始化 MySQL 数据库了

# mysql -h127.0.0.1 -p3306 -uroot -pDEVroot@123456 -e "source $svc_path/etc/mac/skywalking_mysql_dev.sql"
# mysql -h127.0.0.1 -p3306 -uDEVuser -pDEVuser@123456 -e "show databases;use dev_skywalking;show tables"

