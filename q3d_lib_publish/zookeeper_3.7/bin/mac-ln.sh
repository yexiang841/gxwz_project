#!/bin/bash

file_path=$(cd `dirname $0`; pwd) # 当前文件路径
svc_path=$(cd $file_path/..; pwd) # 子项目路径
base_path=$(cd $svc_path/..; pwd) # 部署代码库路径

zookeeper_etc_path=/usr/local/etc/zookeeper # zookeeper 配置文件
zookeeper_data_path=/usr/local/var/run/zookeeper/data # zookeeper 数据目录
zookeeper_log_path=/usr/local/var/log/zookeeper # zookeeper 日志目录

rm -rf $zookeeper_etc_path
ln -s $svc_path/etc/mac/ $zookeeper_etc_path
ls -l  $zookeeper_etc_path

rm -rf $zookeeper_data_path
ln -s $svc_path/data/mac/ $zookeeper_data_path
ls -l  $zookeeper_data_path

rm -rf $zookeeper_log_path
ln -s $svc_path/log/mac/ $zookeeper_log_path
ls -l  $zookeeper_log_path

