#!/bin/bash

file_path=$(cd `dirname $0`; pwd) # 当前文件路径
svc_path=$(cd $file_path/..; pwd) # 子项目路径
base_path=$(cd $svc_path/..; pwd) # 部署代码库路径
etc_path=$(cd $svc_path/etc/mac; pwd) # 配置路径
data_path=$(cd $svc_path/data/mac; pwd) # 数据路径
plugin_path=$(cd $svc_path/plugin/mac; pwd) # 插件路径
log_path=$(cd $svc_path/log/mac; pwd) # 日志路径

elasticsearch_etc_path=/usr/local/etc/elasticsearch # elasticsearch 配置目录
elasticsearch_data_path=/usr/local/var/lib/elasticsearch # elasticsearch 数据目录
elasticsearch_plugin_path=/usr/local/var/elasticsearch/plugins # elasticsearch 配置目录
elasticsearch_log_path=/usr/local/var/log/elasticsearch # elasticsearch 日志目录

mkdir -p $elasticsearch_etc_path
rm -rf $elasticsearch_etc_path
mkdir -p $etc_path
ln -s $etc_path $elasticsearch_etc_path
ls -l  $elasticsearch_etc_path

mkdir -p $elasticsearch_data_path
rm -rf $elasticsearch_data_path
mkdir -p $data_path
ln -s $data_path $elasticsearch_data_path
ls -l  $elasticsearch_data_path

mkdir -p $elasticsearch_plugin_path
rm -rf $elasticsearch_plugin_path
mkdir -p $plugin_path
ln -s $plugin_path $elasticsearch_plugin_path
ls -l  $elasticsearch_plugin_path

mkdir -p $elasticsearch_log_path
rm -rf $elasticsearch_log_path
mkdir -p $log_path
ln -s $log_path $elasticsearch_log_path
ls -l  $elasticsearch_log_path

