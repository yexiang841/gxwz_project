#!/bin/bash

file_path=$(cd `dirname $0`; pwd) # 当前文件路径
svc_path=$(cd $file_path/..; pwd) # 子项目路径
base_path=$(cd $svc_path/..; pwd) # 部署代码库路径
data_path=$(cd $svc_path/data/mac; pwd) # 数据路径
log_path=$(cd $svc_path/log/mac; pwd) # 日志路径

redis_etc_file=/usr/local/etc/redis.conf # redis 配置文件
redis_etc_sentinel_file=/usr/local/etc/redis-sentinel.conf # redis 集群配置文件
redis_data_path=/usr/local/var/db/redis # redis 数据目录
redis_log_path=/usr/local/var/log/redis # redis 日志目录
redis_log_file=/usr/local/var/log/redis.log # redis 启动日志文件

rm -rf $redis_etc_file
ln -s $svc_path/etc/mac/redis.conf $redis_etc_file
ls -l  $redis_etc_file

rm -rf $redis_etc_sentinel_file
ln -s $svc_path/etc/mac/redis-sentinel.conf $redis_etc_sentinel_file
ls -l  $redis_etc_sentinel_file

rm -rf $redis_data_path
mkdir -p $data_path
ln -s $data_path $redis_data_path
ls -l  $redis_data_path

rm -rf $redis_log_path
mkdir -p $log_path
ln -s $log_path $redis_log_path
ls -l  $redis_log_path

rm -rf $redis_log_file
ln -s $log_path/redis.log $redis_log_file
ls -l  $redis_log_file

