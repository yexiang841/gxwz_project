#!/bin/bash

file_path=$(cd `dirname $0`; pwd) # 当前文件路径
svc_path=$(cd $file_path/..; pwd) # 子项目路径
base_path=$(cd $svc_path/..; pwd) # 部署代码库路径

kafka_etc_path=/usr/local/etc/kafka # kafka 配置目录
kafka_data_path=/usr/local/var/lib/kafka-logs # kafka 数据目录
kafka_log_brew_path=/usr/local/var/log/kafka # kafka 启动日志目录
kafka_log_run_path=/usr/local/opt/kafka/libexec/logs # kafka 运行日志目录

rm -rf $kafka_etc_path
ln -s $svc_path/etc/mac/ $kafka_etc_path
ls -l  $kafka_etc_path

rm -rf $kafka_data_path
ln -s $svc_path/data/mac/ $kafka_data_path
ls -l  $kafka_data_path

rm -rf $kafka_log_brew_path
ln -s $svc_path/log/mac/ $kafka_log_brew_path
ls -l  $kafka_log_brew_path

rm -rf $kafka_log_run_path
ln -s $svc_path/log/mac/ $kafka_log_run_path
ls -l  $kafka_log_run_path

