#!/bin/bash

file_path=$(cd `dirname $0`; pwd) # 当前文件路径
base_path=$(cd $file_path/..; pwd) # 部署代码库路径

# 因为使用了 external_links 字段，因此以下服务的启动是有依赖顺序的
docker-compose -f $base_path/sebp_elk_7.15/docker-compose.yml up -d
docker-compose -f $base_path/sebp_elk_7.15/docker-compose.es-head.yml up -d
docker-compose -f $base_path/mysql_5.7/docker-compose.yml up -d
docker-compose -f $base_path/cloudbeaver_21.2/docker-compose.yml up -d
docker-compose -f $base_path/nacos_2.0/docker-compose.yml up -d
docker-compose -f $base_path/nginx_1.19/docker-compose.yml up -d
#docker-compose -f $base_path/filebeat_7.15/docker-compose.yml up -d
