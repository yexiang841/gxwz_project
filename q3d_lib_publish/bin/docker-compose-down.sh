#!/bin/bash

file_path=$(cd `dirname $0`; pwd) # 当前文件路径
base_path=$(cd $file_path/..; pwd) # 部署代码库路径

docker ps -a
docker stop $(docker ps -aq)
docker container prune

