#!/bin/bash

file_path=$(cd `dirname $0`; pwd) # 当前文件路径
base_path=$(cd $file_path/..; pwd) # 部署代码库路径
project_path=$(cd $svc_path/..; pwd) # 项目库路径
sys_publish_path="/opt/q3d_lib_publish" # 系统部署路径

echo ""
echo "file_path = $file_path"
echo "base_path = $base_path"
echo "project_path = $project_path"
echo "sys_publish_path = $sys_publish_path"

# echo ""
# cmd="sh $base_path/cloudbeaver_21.2/bin/mac-clear.sh"
# echo $cmd
# eval $cmd
echo ""
cmd="sh $base_path/elasticsearch_7.10/bin/mac-clear.sh"
echo $cmd
eval $cmd
# echo ""
# cmd="sh $base_path/filebeat_7.15/bin/mac-clear.sh"
# echo $cmd
# eval $cmd
echo ""
cmd="sh $base_path/grafana_8.2/bin/mac-clear.sh"
echo $cmd
eval $cmd
echo ""
cmd="sh $base_path/kafka_3.2/bin/mac-clear.sh"
echo $cmd
eval $cmd
echo ""
cmd="sh $base_path/kibana_7.10/bin/mac-clear.sh"
echo $cmd
eval $cmd
echo ""
cmd="sh $base_path/logstash_7.10/bin/mac-clear.sh"
echo $cmd
eval $cmd
echo ""
cmd="sh $base_path/mongodb-community_4.2/bin/mac-clear.sh"
echo $cmd
eval $cmd
echo ""
cmd="sh $base_path/mysql_5.7/bin/mac-clear.sh"
echo $cmd
eval $cmd
echo ""
cmd="sh $base_path/nacos_2.1/bin/mac-clear.sh"
echo $cmd
eval $cmd
echo ""
cmd="sh $base_path/nginx_1.19/bin/mac-clear.sh"
echo $cmd
eval $cmd
echo ""
cmd="sh $base_path/postgresql_14.3/bin/mac-clear.sh"
echo $cmd
eval $cmd
echo ""
cmd="sh $base_path/rabbitmq_3.10/bin/mac-clear.sh"
echo $cmd
eval $cmd
echo ""
cmd="sh $base_path/redis_7.0/bin/mac-clear.sh"
echo $cmd
eval $cmd
# echo ""
# cmd="sh $base_path/sebp_elk_7.15/bin/mac-clear.sh"
# echo $cmd
# eval $cmd
echo ""
cmd="sh $base_path/skywalking-agent_8.11/bin/mac-clear.sh"
echo $cmd
eval $cmd
echo ""
cmd="sh $base_path/skywalking-apm_9.1/bin/mac-clear.sh"
echo $cmd
eval $cmd
echo ""
cmd="sh $base_path/q3d_lib_service_spring/bin/mac-clear.sh"
echo $cmd
eval $cmd
echo ""
cmd="sh $base_path/zookeeper_3.7/bin/mac-clear.sh"
echo $cmd
eval $cmd

