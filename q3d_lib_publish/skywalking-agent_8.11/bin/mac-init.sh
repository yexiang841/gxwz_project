#!/bin/bash

file_path=$(cd `dirname $0`; pwd) # 当前文件路径
svc_path=$(cd $file_path/..; pwd) # 子项目路径
base_path=$(cd $svc_path/..; pwd) # 部署代码库路径

# 根据项目需要，复制相应的 jar 包
# 这两个是 SpringCloud Gateway 需要的

cmd="cp -f $svc_path/optional-plugins/apm-spring-webflux-5.x-plugin-8.11.0.jar $svc_path/plugins/"
echo $cmd
eval $cmd

cmd="cp -f $svc_path/optional-plugins/apm-spring-cloud-gateway-3.x-plugin-8.11.0.jar $svc_path/plugins/"
echo $cmd
eval $cmd

